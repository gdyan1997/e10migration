import request from 'axios'

/**
 * 表单字段转换
 * @param data
 * @param isMobile
 */
export const getWorkflowJsCodeByPage = (data?: any) => {
  return request({
    url: `/jscode/getWorkflowJsCodeByPageNew`,
    method: 'POST',
    data: data
  })
}

export const getWorkflowJsCodeByPageNewWithOutTrans = (data?: any) => {
  return request({
    url: `/jscode/getWorkflowJsCodeByPageNewWithOutTrans`,
    method: 'POST',
    data: data
  })
}

export const exportExcelForAll = (data?: any) => {
  return request({
    url: `/jscode/exportExcelForAll`,
    method: 'GET',
    params: data
  })
}
/**
 * 强制获取所有流程模版
 * @param data true false
 */
export const initAll = (data?: any) => {
  return request({
    url: `/jscode/initAll`,
    method: 'GET',
    params: data
  })
}

/**
 * 记录单挑数据转换后的结果
 * @param data
 */
export const initTranfercontent = (data?: any) => {
  return request({
    url: `/jscode/trans`,
    method: 'POST',
    data: data
  })
}

/**
 * 记录多条数据转换后的结果
 * @param datas
 */
export const initTranfercontentBatch = (datas?: any) => {
  return request({
    url: `/jscode/transBatch`,
    method: 'POST',
    data: datas
  })
}

/**
 * 表单字段转换
 * @param data
 * @param isMobile
 */
export const getE10NewId = (data?: any, isMobile?: boolean) => {
  return request({
    url: `/papi/version/upgrade/trans/getNewId`,
    method: 'GET',
    params: data
  })
}