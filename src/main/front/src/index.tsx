import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import 'core-js/es/array/from';
import 'core-js/es/array/map';

import React from 'react';
import ReactDOM from 'react-dom/client';
import 'normalize.css';
import './index.css';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Main from './components/main/main';
import reportWebVitals from './reportWebVitals';
import NotFoundPage from "./NotFoundPage";
import { ConfigProvider } from 'antd';
import { legacyLogicalPropertiesTransformer,StyleProvider } from '@ant-design/cssinjs';

const rootElement = document.getElementById('root');
if (rootElement) {
    ReactDOM.createRoot(rootElement).render(
        <React.StrictMode>
            <ConfigProvider>
                <StyleProvider hashPriority="high"  transformers={[legacyLogicalPropertiesTransformer]}>
                    <Router>
                        <Routes>
                            <Route path="/" element={<Main />} />
                            <Route path="/main/*" element={<Main />} />
                            {/* 其他路由 */}
                            {/* 添加通配符路由 */}
                            <Route path="*" element={<NotFoundPage />} />
                        </Routes>
                    </Router>
                </StyleProvider>
            </ConfigProvider>
        </React.StrictMode>
    );
} else {
    console.error("Root element not found!");
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

