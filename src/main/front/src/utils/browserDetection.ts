export function isIE(): boolean {
    const userAgent = window.navigator.userAgent;
    const isIE11 = userAgent.indexOf('Trident/') > -1 && userAgent.indexOf('rv:11.0') > -1;
    const isOlderIE = userAgent.indexOf('MSIE') > -1 && userAgent.indexOf('Windows NT') > -1;
    return isIE11 || isOlderIE;
}
