import React from 'react';

const BrowserWarning: React.FC = () => {
    return (
        <div style={{ textAlign: 'center', marginTop: '50px' }}>
            <h1>Unsupported Browser</h1>
            <p>For the best experience, please use a modern browser like Google Chrome or Microsoft Edge.</p>
        </div>
    );
};

export default BrowserWarning;
