import './index.css'
import JSZip from 'jszip';
import React, {useEffect, useRef, useState} from 'react';
import {Button, Input, Space, Table, Pagination, Modal, message, Radio, FloatButton, Tooltip, Spin} from 'antd';
import WeaJsCodeShift from "../transJs/WeaJsCodeShift";
import {DownloadOutlined, SearchOutlined, SmileOutlined, InfoCircleOutlined} from '@ant-design/icons';
import {FilterDropdownProps, Key} from "antd/es/table/interface";
import * as XLSX from 'xlsx';
import useConfirm from "../custom/useConfirm";
import {
    exportExcelForAll,
    getWorkflowJsCodeByPage,
    getWorkflowJsCodeByPageNewWithOutTrans,
    initAll,
    initTranfercontent,
    initTranfercontentBatch
} from "../../api/TransCodeApi";

const {Column, ColumnGroup} = Table;

interface JSDataType {
    id: React.Key;
    frommodule: string;
    workflowname: string;
    formname: string;
    nodename: string;
    scriptstr: string;
    tranfercontent: string;
    workflowid: string;
    typename: string;
    layoutname: string;
    tranferstatus: string[];
    hassrcscript: string;
    hascssstyle: string;
    transferflag: string;//无法完全转换，已转换需确认
}

const handleHref = () => {
    const params = new URLSearchParams(window.location.search);
    const oneParam = params.get('one');
    return oneParam === 'true' || oneParam == '1'; // 假设URL中的one参数是布尔值的字符串形式
};

const WorkflowJsList: React.FC = () => {
    const [confirm, ConfirmModal] = useConfirm();

    const iframeRef = useRef<HTMLIFrameElement>(null);

    const [data, setData] = useState<JSDataType[]>([]);
    const [loading, setLoading] = useState(true);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10,
        total: 0
    });
    const [workflowName, setWorkflowName] = useState<string>('');
    // 初始化 scroll 高度为初始值
    const [scrollHeight, setScrollHeight] = useState(800);
    const [exporting, setExporting] = useState(false);
    const [disableButton, setDisableButton] = useState(false);
    const [fromModule, setFromModule] = useState<string>('');
    const [unTransNum, setUnTransNum] = useState<number>(0);
    const [one, setOne] = useState(false);

    // 定义 resize 事件处理函数
    const handleResize = () => {
        // 获取浏览器窗口的可视区域高度
        const viewportHeight = window.innerHeight || document.documentElement.clientHeight;
        // 设置固定的顶部高度（如果有的话，没有则设置为 0）
        const fixedHeight = 120 + 55; // 例如顶部导航栏高度
        // 计算剩余的高度
        const remainingHeight = viewportHeight - fixedHeight;
        // 更新 scroll 高度
        setScrollHeight(remainingHeight);
    };

    // 监听窗口大小变化，动态调整 scroll 高度
    useEffect(() => {
        // 获取URL参数并设置状态
        const oneValue = handleHref();
        setOne(oneValue);
        // 初始化时执行一次
        handleResize();
        // 添加 resize 事件监听器
        window.addEventListener('resize', handleResize);

        // 返回清除函数，在组件卸载时执行
        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []); // 空数组表示只在组件挂载和卸载时执行

    // 组件挂载时加载初始数据
    useEffect(() => {
        try {
            fetchData(1, pagination.pageSize); // 等待fetchData执行完成
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
        }
    }, [workflowName, fromModule]); // 注意这里的空数组表示只在组件挂载时执行一次

    // 处理分页器变化事件
    const handlePaginationChange = (page: any, pageSize: any) => {
        setDisableButton(true); // 按钮不允许点击

        try {
            fetchData(page, pageSize); // 等待fetchData执行完成
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
            setDisableButton(false); // 在异步操作完成后重新启用按钮
        }
    };
    const handlePaginationChangeForTable = (pagination: any) => {
        setPagination(pagination);
        try {
            fetchData(pagination.current, pagination.pageSize); // 等待fetchData执行完成
        } catch (error) {
            console.error('Error fetching data:', error);
        } finally {
        }
    };

    // 发送分页请求
    const fetchData = async (pageNum: number, pageSize: number) => {
        try {
            setLoading(true)
            const response = await getWorkflowJsCodeByPage({
                page: pageNum,
                perPage: pageSize,
                fromModule: fromModule,
                workflowName: workflowName
            });
            setPagination(prevState => ({
                ...prevState,
                current: pageNum,
                pageSize: pageSize,
                total: response.data.total
            }));
            await handleBatchConvertClickForPage(response.data.list, response.data.fields, response.data.detailTables, response.data.cubeButton, true);

        } catch (error) {
            if (window.console) console.error('Failed to fetch data:', error);
        } finally {
            setLoading(false)
        }
    };

    const handleConvertClick = (record: JSDataType, fields: any, detailTables: any, cubeButton: any, isUpdate: boolean = false) => {

        // 修改数据
        const updatedData = data.map(item => {
            if (record.scriptstr && !record.tranfercontent)
                if (item.id === record.id) {
                    // 修改对应行的 tranferContent
                    const tranferContent = WeaJsCodeShift.transform(record.frommodule, record.scriptstr, fields?.[record.frommodule]?.[record.workflowid], detailTables?.[record.frommodule]?.[record.workflowid], cubeButton);
                    const verifyJS = WeaJsCodeShift.verifyJS(tranferContent, record.hassrcscript === '是');
                    item = {
                        ...item,
                        tranfercontent: tranferContent, // 修改为新的内容
                        transferflag: verifyJS?.data,
                        tranferstatus: verifyJS?.detail,
                    };
                    initTranfercontent(item);
                }
            return item;
        });

        // 更新状态
        if (isUpdate)
            setData(updatedData);

        return updatedData;

    };

    /**
     * 页面展示用，存入转换结果是只需要异步操作即可
     * @param records
     * @param fields
     * @param detailTables
     * @param isUpdate
     */
    const handleBatchConvertClickForPage = async (records: JSDataType[], fields: any, detailTables: any, cubeButton: any, isUpdate: boolean = false) => {
        const changeData: JSDataType[] = [];
        const updatedData = records.map(record => {
            if (record.scriptstr && !record.tranfercontent) {
                record.tranfercontent = WeaJsCodeShift.transform(record.frommodule, record.scriptstr, fields?.[record.frommodule]?.[record.workflowid], detailTables?.[record.frommodule]?.[record.workflowid], cubeButton);
                const verifyJS = WeaJsCodeShift.verifyJS(record.tranfercontent, record.hassrcscript === '是');
                record.transferflag = verifyJS?.data;
                record.tranferstatus = verifyJS?.detail;
                changeData.push(record);
            }
            return record;
        })

        //转换的数据记录一下
        if (changeData.length > 0) {
            // if(!one){
            //     // await initTranfercontentBatch(changeData);
            // }else {
                for (const tmp of changeData) {
                    await initTranfercontent(tmp);
                }
            // }
        }

        // 更新状态
        if (isUpdate)
            setData(updatedData);

        return updatedData;
    };

    /**
     * 批量更新数据用，存入转换结果是需要同步操作
     * @param records
     * @param fields
     * @param detailTables
     * @param isUpdate
     */
    const handleBatchConvertClickForDb = async (records: JSDataType[], fields: any, detailTables: any, cubeButton: any, isUpdate: boolean = false) => {
        const changeData: JSDataType[] = [];
        const updatedData = records.map(record => {
            if (record.scriptstr && !record.tranfercontent) {
                record.tranfercontent = WeaJsCodeShift.transform(record.frommodule, record.scriptstr, fields?.[record.frommodule]?.[record.workflowid], detailTables?.[record.frommodule]?.[record.workflowid], cubeButton);
                const verifyJS = WeaJsCodeShift.verifyJS(record.tranfercontent, record.hassrcscript === '是');
                record.transferflag = verifyJS?.data;
                record.tranferstatus = verifyJS?.detail;
                changeData.push(record);
            }
            return record;
        })

        //转换的数据记录一下
        if (changeData.length > 0) {
            if(!one){
                await initTranfercontentBatch(changeData);
            }else {
                for (const tmp of changeData) {
                    await initTranfercontent(tmp);
                }
            }
        }

        // 更新状态
        if (isUpdate)
            setData(updatedData);

        return updatedData;
    };

    const handleExportClick = () => {
        confirm({
            title: '确认导出',
            content: '确定要导出全部数据吗？转换逻辑较为复杂，需要花费很长时间，请耐心等待... ',
            okText: '确认',
            cancelText: '取消',
            onOk() {
                exportAll();
            },
            onCancel() {
                // 取消操作
            },
        });
    };

    const handleInitAllTemplate = () => {
        confirm({
            title: '确认初始化',
            content: '确定要将所有数据初始化吗？初始化会清空中间表所有模版数据，后续需要重新生成所有的模版数据以及转换后代码，转换逻辑较为复杂，需要花费很长时间，请耐心等待... ',
            okText: '确认',
            cancelText: '取消',
            onOk() {
                initAllTemplate(true);
            },
            onCancel() {
                // 取消操作
            },
        });
    };

    /**
     * 初始化所有模版
     */
    const initAllTemplate = async (isForce: boolean = false) => {
        try {
            setLoading(true);
            setDisableButton(true); // 开始导出，设置状态为导出中

            await initAll({
                isForce: isForce,
                fromModule: fromModule,
                workflowName: workflowName
            }).then(res => {
                if (res.data.success) {
                    message.success(res.data.message);
                } else {
                    message.error(res.data.message);
                }
            });
        } catch (error) {
            if (window.console) console.error('initAllTemplate:', error);
        } finally {
            await fetchData(1, pagination.pageSize);
            setLoading(false);
            setDisableButton(false);
        }
    };

    /**
     * 导出数据
     * @param pageNum
     * @param pageSize
     * @param isAll
     */
    const exportToExcel = async (pageNum: number, pageSize: number, isAll: boolean = false) => {
        try {
            setLoading(true);
            setExporting(true); // 开始导出，设置状态为导出中
            const response = await getWorkflowJsCodeByPage({
                page: pageNum,
                perPage: pageSize,
                fromModule: fromModule,
                workflowName: workflowName,
                isAll: isAll,
            });

            let dataTypes = await handleBatchConvertClickForPage(response.data.list, response.data.fields, response.data.detailTables, response.data.cubeButton, false);
            const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
            const fileExtension = '.xlsx';
            const batchSize = 500; // 每个批次包含的数据量

            const chineseDataArray = dataTypes.map(item => [
                item.workflowid,
                item.workflowname,
                item.formname,
                item.typename,
                item.layoutname,
                item.nodename,
                item.scriptstr,
                item.tranfercontent,
                item.transferflag,
                item.hascssstyle,
            ]);

            // 创建标题行数组
            const titleArray = [
                '工作流ID',
                '工作流名称',
                '表单名称',
                '类型名称',
                '布局名称',
                '节点名称',
                '脚本内容',
                '转换后内容',
                '转换情况',
                '包含CSS样式',
            ];

            if (dataTypes.length > 500) {
                const zip = new JSZip();
                for (let i = 0; i < chineseDataArray.length; i += batchSize) {
                    const batchData = chineseDataArray.slice(i, i + batchSize);

                    const ws = XLSX.utils.aoa_to_sheet([titleArray, ...batchData]);

                    // 合并第一行的第一列到第十列单元格
                    // ws['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 9 } }];// 创建样式对象
                    const wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, `data`);
                    const excelBuffer = XLSX.write(wb, {bookType: 'xlsx', type: 'array'});
                    const dataBlob = new Blob([excelBuffer], {type: fileType});
                    zip.file(`第${i + 1}~${i + batchSize}条清单${fileExtension}`, dataBlob);
                }

                // 生成 ZIP 文件
                const zipBlob = await zip.generateAsync({type: 'blob'});
                // 生成下载链接
                const downloadLink = document.createElement('a');
                downloadLink.href = URL.createObjectURL(zipBlob);
                downloadLink.download = 'JS代码替换清单' + (workflowName ? '-' + workflowName : '') + '.zip';
                downloadLink.click();
            } else {

                const ws = XLSX.utils.aoa_to_sheet([titleArray, ...chineseDataArray]);

                // 合并第一行的第一列到第十列单元格——不合并了
                // ws['!merges'] = [{ s: { r: 0, c: 0 }, e: { r: 0, c: 9 } }];// 创建样式对象
                // ws['I1'].c = [{ // 设置注释对象
                //     // t: 's', // 文本类型
                //     t: '转换情况：\n1、存在JS引入--E9的代码块中存在引入的JS，引入的JS是无法通过转换工具进行转换的，此种类型下的代码块均需要手动调整；\n2、代码异常--E9代码块本身存在错误，此种代码块贴入转换工具会直接报错，无法转换；\n3、无法完全转换--E9的代码块转换后，无法直接使用，需要手动调整，此类型代码块框架已全部存在，减少手动设置工作量；\n4、已转换需确认--E9的代码块支持全部转换，转换后需要验证;', // 注释内容
                //     a: 'Cloud', // 作者
                //     // r: 'I1', // 关联的单元格
                //     h: '50', // 注释框的高度
                //     w: '150', // 注释框的宽度
                //     hidden: true,
                // }];
                const wb = {Sheets: {data: ws}, SheetNames: ['data']};
                const excelBuffer = XLSX.write(wb, {bookType: 'xlsx', type: 'array'});
                const dataBlob = new Blob([excelBuffer], {type: fileType});

                // 生成下载链接
                const downloadLink = document.createElement('a');
                downloadLink.href = URL.createObjectURL(dataBlob);
                downloadLink.download = 'JS代码替换清单' + (workflowName ? '-' + workflowName : '') + fileExtension;
                downloadLink.click();
            }
            message.success("导出成功！");

        } catch (error) {
            if (window.console) console.error('exportToExcel:', error);
            message.error("导出失败！");
        } finally {
            setLoading(false);
            setExporting(false);
        }
    };

    /**
     * 全部转换
     * @param pageNum
     * @param pageSize
     * @param isAll
     */
    const exportAll = async () => {
        try {
            setLoading(true);
            setExporting(true); // 开始导出，设置状态为导出中

            let total = 0;
            let currentPageSize = 0;

            do {
                const response = await getWorkflowJsCodeByPageNewWithOutTrans({
                    page: 1,
                    perPage: 10,
                    workflowName: workflowName,
                    fromModule: fromModule,
                    timestamp: new Date().getTime(),
                });

                total = response.data.total;
                currentPageSize = response.data.list.length;
                setUnTransNum(total);
                if (total > 0 && currentPageSize > 0) {
                    await handleBatchConvertClickForDb(response.data.list, response.data.fields, response.data.detailTables, response.data.cubeButton, false);
                } else {
                    break; // 如果返回的数据总数为0，则退出循环
                }
            } while (total > 0 && currentPageSize == 10);//当获取到的数据没有了，就不执行了

            //转换结束，未转换数0
            setUnTransNum(0);

            //处理完成之后调用api去生成excel
            const exportUrl = `/jscode/exportExcelForAll?fromModule=${fromModule}&workflowName=${workflowName}`;
            if (iframeRef.current) {
                iframeRef.current.src = exportUrl;
            }
            message.success("导出成功！");

        } catch (error) {
            if (window.console) console.error('exportAll:', error);
            message.error("导出失败！");
        } finally {
            setLoading(false);
            setExporting(false);
        }
    };

    // @ts-ignore
    const searchInputRef = useRef<Input>(null);

    const getColumnSearchProps = (dataIndex: keyof JSDataType) => ({
        filterDropdown: ({
                             setSelectedKeys,
                             selectedKeys,
                             confirm,
                             clearFilters,
                         }: FilterDropdownProps) => (
            <div style={{padding: 8}}>
                <Input
                    ref={searchInputRef}
                    placeholder={`请输入...`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm)}
                        icon={<SearchOutlined/>}
                        size="small"
                        style={{width: 90}}
                    >
                        查询
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{width: 90}}>
                        重置
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered: boolean) => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
        onFilter: (value: boolean | Key, record: JSDataType) => // 更改此处的参数类型
            record[dataIndex].toString().toLowerCase().includes((value as string).toLowerCase()), // 或者根据实际情况进行类型转换
        onFilterDropdownVisibleChange: (visible: boolean) => {
            if (visible) {
                setTimeout(() => searchInputRef.current?.select(), 100);
            }
        },
        render: (text: string) => text,
    });

    const handleSearch = (selectedKeys: React.Key[], confirm: () => void) => {
        setWorkflowName(selectedKeys?.[0] as string || '');
        confirm();
    };

    const handleReset = (clearFilters: (() => void) | undefined) => {
        if (clearFilters) {
            clearFilters();
        }
    };

    return (
        <div>
            <div
                style={{
                    margin: '8px 20px 8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center'
                }}>
                <FloatButton className='blinking-button' tooltip={<div>E9迁移E10Js代码转换相关资源</div>}
                             onClick={() => {
                                 window.open('https://www.e-cology.com.cn/sp/doc/docDetail/2001445336352200948');
                             }}/>
                <div>
                    <Button type="primary" danger shape="round" icon={<SmileOutlined/>} style={{marginRight: 16}}
                            onClick={handleInitAllTemplate}
                            disabled={exporting || disableButton} // 如果正在导出，则禁用按钮
                    >
                        强制模版初始化
                    </Button>
                    <Button type="primary" shape="round" icon={<SmileOutlined/>} style={{marginRight: 16}}
                            onClick={() => initAllTemplate(false)}
                            disabled={exporting || disableButton} // 如果正在导出，则禁用按钮
                    >
                        新增模版初始化
                    </Button>
                    {/*<Button type="primary" shape="round" icon={<DownloadOutlined/>} style={{marginRight: 16}}*/}
                    {/*        onClick={() => exportToExcel(pagination.current, pagination.pageSize)}*/}
                    {/*        disabled={exporting} // 如果正在导出，则禁用按钮*/}
                    {/*>*/}
                    {/*    导出本页*/}
                    {/*</Button>*/}
                    <Button type="primary" shape="round" icon={<DownloadOutlined/>} style={{marginRight: 16}}
                            onClick={handleExportClick}
                            disabled={exporting || disableButton} // 如果正在导出，则禁用按钮
                    >
                        导出全部
                    </Button>
                    <Radio.Group onChange={e => {
                        setFromModule(e.target.value);
                    }} value={fromModule} disabled={exporting || disableButton}>
                        <Radio value={''}>全部</Radio>
                        <Radio value={'workflow'}>流程</Radio>
                        <Radio value={'cube'}>建模</Radio>
                    </Radio.Group>
                </div>
                <Pagination
                    current={pagination.current}
                    pageSize={pagination.pageSize}
                    total={pagination.total}
                    onChange={handlePaginationChange}
                    showSizeChanger={true}
                    showTotal={(total, range) => `总共 ${total} 条数据`}
                />
            </div>
            <div className="router-content-table">
                <Table
                    dataSource={data}
                    loading={loading && !exporting}
                    pagination={false}
                    // pagination={pagination} // 设置分页器
                    scroll={{x: 'false', y: scrollHeight}} // 添加 scroll 属性以支持列宽度设置
                >
                    <Column title="所属模块" dataIndex="frommodule" key="frommodule"
                            width='6%' className="custom-column-min"
                            render={(text) => {
                                // 根据不同的值返回不同的显示内容
                                if (text === 'workflow') {
                                    return '流程';
                                } else if (text === 'cube') {
                                    return '建模';
                                } else {
                                    return text; // 如果不是 'workflow' 或 'cube'，则原样返回
                                }
                            }}/>
                    <Column title="流程/模块名称" dataIndex="workflowname" key="workflowname"
                            width='8%' {...getColumnSearchProps('workflowname')} className="custom-column-min"/>
                    <Column title="节点/应用名称" dataIndex="nodename" key="nodename" width='7%'
                            className="custom-column-min"/>
                    <Column title="模版名称" dataIndex="layoutname" key="layoutname" width='7%'
                            className="custom-column-min"/>
                    <Column title="模版类型" dataIndex="typename" key="typename" width='7%'
                            className="custom-column-min"/>
                    <Column title="代码段内容" dataIndex="scriptstr" key="scriptstr" width='28%'
                            className="custom-column-max"
                            render={(_: any, record: JSDataType) => (
                                <div style={{
                                    overflowY: 'auto',
                                    textOverflow: 'ellipsis',
                                    whiteSpace: 'pre-wrap',
                                    wordWrap: 'break-word',
                                    maxWidth: '100%',
                                    maxHeight: '400px'
                                }} className=" scrollbar">
                                    <pre>{record.scriptstr}</pre>
                                </div>
                            )}/>
                    <Column title="转换后内容" dataIndex="tranfercontent" key="tranfercontent" width='30%'
                            className="custom-column-max"
                            render={(_: any, record: JSDataType) => (
                                <div style={{
                                    overflowY: 'auto',
                                    textOverflow: 'ellipsis',
                                    whiteSpace: 'pre-wrap',
                                    wordWrap: 'break-word',
                                    maxWidth: '100%',
                                    maxHeight: '400px'
                                }} className=" scrollbar">
                                    <pre>{record.tranfercontent}</pre>
                                </div>
                            )}/>
                    <Column
                        title={
                            <Tooltip title={
                                <div style={{maxWidth: 500, margin: '10px 5px'}}>
                                    转换情况说明：<br/>
                                    1、存在JS引入--E9的代码块中存在引入的JS，引入的JS是无法通过转换工具进行转换的，此种类型下的代码块均需要手动调整；<br/>
                                    2、代码异常--E9代码块本身存在错误，此种代码块贴入转换工具会直接报错，无法转换；<br/>
                                    3、无法完全转换--E9的代码块转换后，代码框架已经替换，部分内容无法替换需要手动调整；<br/>
                                    4、已转换需确认--E9的代码块支持全部转换，转换后需要验证;
                                </div>
                            }>
                                <span style={{color: "red"}}>转换情况<InfoCircleOutlined
                                    style={{marginLeft: 5}}/></span>
                            </Tooltip>
                        }
                        dataIndex="transferflag" key="transferflag"
                        width='7%'
                        className="custom-column-tool"
                        render={(_: any, record: JSDataType) => {
                            const tranferstatusContent = record.tranferstatus?.length
                                ? (
                                    <div style={{maxWidth: 500, margin: '10px 5px'}}>
                                        {record.tranferstatus.map((status, index) => (
                                            <div key={index}>{index+1}. {status}</div>
                                        ))}
                                    </div>
                                )
                                : null;

                            return tranferstatusContent ? (
                                <Tooltip title={tranferstatusContent}>
                                    <span>{record.transferflag}</span>
                                </Tooltip>
                            ) : (
                                <span>{record.transferflag}</span>
                            );

                        }}/>
                </Table>
            </div>
            <iframe ref={iframeRef} style={{display: 'none'}}/>
            {exporting && (
                <div style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%',
                    height: '100%',
                    backgroundColor: 'rgba(255, 255, 255, 0.7)',
                    zIndex: 9999,
                }}>
                    <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                        <Spin tip={`正在导出，请稍候... (剩余模版:${unTransNum})`}>
                            <span
                                style={{visibility: "hidden"}}>{`正在导出，请稍候... (剩余待转换模版:${unTransNum})`}</span>
                        </Spin>
                    </div>
                </div>
            )}

            <ConfirmModal />
        </div>

    );

};

export default WorkflowJsList;