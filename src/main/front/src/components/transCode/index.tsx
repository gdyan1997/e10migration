import React from 'react';
import {Layout} from 'antd';
import WorkflowJsList from './list';

class TransCode extends React.Component<any, any> {

    render() {
        return (
            <Layout>
                <Layout.Content  style={{background: '#fff'}}>
                    <WorkflowJsList/>
                </Layout.Content>
            </Layout>
        );
    }
}

export default TransCode;