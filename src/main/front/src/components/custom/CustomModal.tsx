import React from 'react';
import ReactDOM from 'react-dom';
import './CustomModal.css';

interface CustomModalProps {
    title: string;
    content: React.ReactNode;
    onOk: () => void;
    onCancel: () => void;
    visible: boolean;
    okText?: string;
    cancelText?: string;
}

const CustomModal: React.FC<CustomModalProps> = ({ title, content, onOk, onCancel, visible, okText = 'OK', cancelText = 'Cancel' }) => {
    if (!visible) return null;

    return ReactDOM.createPortal(
        <div className="custom-modal-overlay">
            <div className="custom-modal">
                <div className="custom-modal-header">
                    <h2>{title}</h2>
                </div>
                <div className="custom-modal-body">
                    {content}
                </div>
                <div className="custom-modal-footer">
                    <button className="custom-modal-button" onClick={onCancel}>{cancelText}</button>
                    <button className="custom-modal-button custom-modal-button-ok" onClick={onOk}>{okText}</button>
                </div>
            </div>
        </div>,
        document.body
    );
};

export default CustomModal;
