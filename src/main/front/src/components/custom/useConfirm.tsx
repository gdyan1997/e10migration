import React, { useState } from 'react';
import CustomModal from './CustomModal';

interface ConfirmProps {
    title: string;
    content: React.ReactNode;
    onOk?: () => void;
    onCancel?: () => void;
    okText?: string;
    cancelText?: string;
}

const useConfirm = (): [(props: ConfirmProps) => void, React.FC] => {
    const [confirmProps, setConfirmProps] = useState<ConfirmProps | null>(null);
    const [visible, setVisible] = useState(false);

    const confirm = (props: ConfirmProps) => {
        setConfirmProps(props);
        setVisible(true);
    };

    const ConfirmModal: React.FC = () => {
        if (!confirmProps) return null;
        return (
            <CustomModal
                {...confirmProps}
                visible={visible}
                onOk={() => {
                    setVisible(false);
                    confirmProps.onOk && confirmProps.onOk();
                }}
                onCancel={() => {
                    setVisible(false);
                    confirmProps.onCancel && confirmProps.onCancel();
                }}
            />
        );
    };

    return [confirm, ConfirmModal];
};

export default useConfirm;
