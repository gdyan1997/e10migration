import './index.css';
import React from 'react';
import {Layout, Menu} from 'antd'
import {AuditOutlined, SignatureOutlined} from '@ant-design/icons';
import {Link, Route, Routes,useLocation } from "react-router-dom";
import FunctionList from '../functionList';
import TransCode from '../transCode'

class Main extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        // 获取当前路径
        const { pathname } = this.props;
        // 根据当前路径匹配菜单项的 key
        const matchedItem = Main.items?.find(item => item?.link === pathname);
        let defaultSelectedKeys:string[] = ['diff'];
        if (matchedItem) {
            defaultSelectedKeys = [matchedItem.key];
        }
        this.state = {
            defaultSelectedKeys: defaultSelectedKeys, // 初始化 defaultSelectedKeys 属性
        };
    }

    componentDidMount() {
    }

    //注意路由必须以/main开头，作为前端的访问路径
    static items: Array<{ label: React.ReactNode, key: string, icon: React.ReactNode, link: string }> = [
        {
            label: (<Link to="/main/functionList">拉取清单页面</Link>),
            key: 'diff',
            icon: <AuditOutlined/>,
            link: '/main/functionList'
        },
        {
            label: (<Link to="/main/transCode">JS转换页面</Link>),
            key: 'js',
            icon: <SignatureOutlined/>,
            link: '/main/transCode'
        },
    ];

    // 防止循环滚动
    isSyncingSourceEditor = false
    isSyncingTargetEditor = false

    sourceEditorRef: any = React.createRef()
    targetEditorRef: any = React.createRef()
    // 初始化ref
    sourceEditorDidMount = (editor: any, monaco: any) => {
        this.sourceEditorRef = editor
        editor.focus()
        // 订阅滚动事件
        this.sourceEditorRef.onDidScrollChange((e: any) => {
            if (!this.isSyncingTargetEditor) {
                this.isSyncingSourceEditor = true
                this.targetEditorRef.setScrollTop(e.scrollTop)
                this.targetEditorRef.setScrollLeft(e.scrollLeft)
                this.isSyncingSourceEditor = false
            }
        })
    }

    targetEditorDidMount = (editor: any, monaco: any) => {
        this.targetEditorRef = editor
        this.targetEditorRef.onDidScrollChange((e: any) => {
            if (!this.isSyncingSourceEditor) {
                this.isSyncingTargetEditor = true
                this.sourceEditorRef.setScrollTop(e.scrollTop)
                this.sourceEditorRef.setScrollLeft(e.scrollLeft)
                this.isSyncingTargetEditor = false
            }
        })
    }

    render() {
        return (
            <Layout style={{minHeight: '100vh'}}>
                <Layout.Header className="header"
                               style={{background: '#fff', padding: 0, borderBottom: '1px solid rgba(5, 5, 5, 0.06)',}}>
                    <div style={{display: 'flex', alignItems: 'center'}}>
                        <div className="logo" style={{padding: '0 24px', fontSize: '24px', fontWeight: 'bold'}}>
                            <Link to="/">低版本升级E10配置清单获取工具</Link>
                        </div>
                        <Menu theme="light" mode="horizontal" defaultSelectedKeys={this.state.defaultSelectedKeys}
                              style={{width: '50%', lineHeight: '64px'}}
                              items={Main.items}/>
                    </div>
                </Layout.Header>

                <Layout.Content className="router-content">
                    <Routes>
                        <Route path="/" element={<FunctionList/>}/>
                        <Route path="/functionList" element={<FunctionList/>}/>
                        <Route path="/transCode" element={<TransCode/>}/>
                    </Routes>
                </Layout.Content>

            </Layout>
        );
    }
}

const MyMain = () => {
    const location = useLocation();
    const { pathname } = location;
    return <Main pathname={pathname} />;
}

export default MyMain;