import React from 'react';

class IframeComponent extends React.Component<any, any> {
    private iframeRef: React.RefObject<HTMLIFrameElement>;

    constructor(props: any) {
        super(props);
        this.iframeRef = React.createRef();
    }

    componentDidMount() {
        this.adjustIframeHeight();
        window.addEventListener('resize', this.adjustIframeHeight);
        if (this.iframeRef.current) {
            // this.iframeRef.current.addEventListener('load', this.adjustIframeHeight);
            // this.iframeRef.current.addEventListener('resize', this.adjustIframeHeight);
        }
    }

    componentWillUnmount() {
        window.addEventListener('resize', this.adjustIframeHeight);
        if (this.iframeRef.current) {
            // this.iframeRef.current.removeEventListener('load', this.adjustIframeHeight);
            // this.iframeRef.current.addEventListener('resize', this.adjustIframeHeight);
        }
    }

    // adjustIframeHeight = () => {
    //     if (this.iframeRef.current && this.iframeRef.current.contentWindow) {
    //         const contentWindow = this.iframeRef.current.contentWindow;
    //         const body = contentWindow.document.body;
    //         if (body) {
    //             this.iframeRef.current.style.height = (body.scrollHeight+20) + 'px';
    //         }
    //     }
    // };

    adjustIframeHeight = () => {
        if (this.iframeRef.current && this.iframeRef.current.contentWindow) {
            const contentWindow = this.iframeRef.current.contentWindow;
            const body = contentWindow.document.body;
            if (body) {
                const browserHeight = window.innerHeight > 70? window.innerHeight-70 : 0; // 获取浏览器窗口的高度
                this.iframeRef.current.style.height = browserHeight + 'px'; // 将 iframe 的高度设置为浏览器窗口高度加上一些额外高度
            }
        }
    };


    render() {
        return (
            <div className="iframe-container">
                <iframe ref={this.iframeRef} src="/e10Migration/migration.jsp" title="iframe"/>
            </div>
        );
    }
}

export default IframeComponent;
