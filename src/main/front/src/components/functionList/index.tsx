import './index.css';
import React from 'react';
import {Layout} from 'antd';
import IframeComponent from './iframeContent';


class FunctionList extends React.Component<any, any> {
    render() {
        return (
            <Layout>
               <Layout.Content>
                   <IframeComponent />
               </Layout.Content>
            </Layout>
        );
    }
}

export default FunctionList;