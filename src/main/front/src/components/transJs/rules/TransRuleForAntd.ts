import jscodeshift from 'jscodeshift';
import * as API_LIST from "../../../api/TransCodeApi";

export class TransRuleForAntd {

    transform = (fromModule: string, code: string) => {

        try {
            code = this.antdModalWarningTran(code);
            code = this.antdMessageTran(code);
        } catch (e) {
            if (window.console) console.log('TransRuleForAntd-transform-e', e);
        }
        return code;
    }

    /**
     * antd.Modal.warning({
     *    title: "客户核查提醒",
     *    content: "提醒内容",
     *    onOk() { },
     *    okText: "确定",
     *    onCancel() { }
     * });
     *
     * 改造成：
     *
     * window.weappUi.Dialog.confirm({
     *   title:'',
     *   content: '确认删除xxx吗？',
     *   onOk: () => {
     *     console.log('OK');
     *   },
     *   onCancel: () => {
     *     console.log('Cancel');
     *   },
     *   okText:'确定'
     * });
     */
    antdModalWarningTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {
                        type: "MemberExpression",
                        object: {type: "Identifier", name: "antd"},
                        property: {type: "Identifier", name: "Modal"}
                    },
                    property: {type: "Identifier", name: "warning"}
                },
                arguments: []
            };

            const antdModalWarningComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (antdModalWarningComs.length > 0) {
                antdModalWarningComs.forEach((nodePath: any) => {
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "window.weappUi"},
                                property: {type: "Identifier", name: "Dialog"}
                            },
                            property: {type: "Identifier", name: "confirm"}
                        },
                        arguments: nodePath.node.arguments
                    })
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch
            (e) {
            if (window.console) console.log('antdModalWarningTran-e:', e);
        }
        return code;
    }

    /**
     * antd.message.success("客户千里聆SaaS ID核查通过");
     * antd.message.warning("请输入客户名称！");
     * antd.message.error("请输入客户名称！");
     *
     * 改造成：
     *
     * type有info | error | success | custom
     *
     * window.weappUi.Dialog.message({
     *   type: 'success',
     *   content: '客户千里聆SaaS ID核查通过',
     * });
     */
    antdMessageTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            let calleeNames = ['success', 'warning', 'error'];
            for (let calleeName of calleeNames) {
                const sourceAst: jscodeshift.CallExpression = {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {
                            type: 'MemberExpression',
                            object: {
                                type: 'Identifier',
                                name: 'antd'
                            },
                            property: {
                                type: 'Identifier',
                                name: 'message'
                            }

                        },
                        property: {
                            type: 'Identifier',
                            name: calleeName
                        }
                    },
                    arguments: []
                };

                const antdModalWarningComs = root
                    .find(jscodeshift.CallExpression, sourceAst);

                if (antdModalWarningComs.length > 0) {
                    antdModalWarningComs.forEach((nodePath: any) => {
                        nodePath.replace({
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {
                                    type: 'MemberExpression',
                                    object: {
                                        type: 'MemberExpression',
                                        object: {
                                            type: 'Identifier',
                                            name: 'window'
                                        },
                                        property: {
                                            type: 'Identifier',
                                            name: 'weappUi'
                                        }

                                    },
                                    property: {
                                        type: 'Identifier',
                                        name: 'Dialog'
                                    }

                                },
                                property: {
                                    type: 'Identifier',
                                    name: 'message'
                                }
                            },
                            arguments: [
                                {
                                    type: 'ObjectExpression',
                                    properties: [
                                        {
                                            type:'ObjectProperty',
                                            key: {
                                                type: 'Identifier',
                                                name: 'type'
                                            },
                                            value: {
                                                type: 'Literal',
                                                value: calleeName
                                            }
                                        },{
                                            type:'ObjectProperty',
                                            key: {
                                                type: 'Identifier',
                                                name: 'content'
                                            },
                                            value: nodePath.node.arguments?.[0]
                                        }
                                    ],
                                }
                            ]
                        })
                    });
                }
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch
            (e) {
            if (window.console) console.log('antdModalWarningTran-e:', e);
        }
        return code;
    }
}

const
    instance = new TransRuleForAntd();

export default instance;