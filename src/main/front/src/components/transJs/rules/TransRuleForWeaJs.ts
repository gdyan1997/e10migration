import jscodeshift from 'jscodeshift';

export class TransRuleForWeaJs {

    transform = (code: string) => {

        try {
            code = this.weaJsAlertTran(code);
        } catch (e) {
            if (window.console) console.log('TransRuleForWeaJs-transform-e', e);
        }
        return code;
    }

    /**
     * weaJs.alert("aaaa");
     * window.weaJs.alert("aaaa");
     *
     * 转换成：
     *
     * Dialog.alert("11111111111111");
     * @param code
     */
    weaJsAlertTran = (code: string) => {
        try {
            const root = jscodeshift(code);

            root.find(jscodeshift.CallExpression, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "weaJs"},
                    property: {type: "Identifier", name: "alert"}
                },
                arguments: []
            }).forEach((nodePath: any) => {
                nodePath.node.callee.object.name = 'window.Dialog'
            });

            root.find(jscodeshift.CallExpression, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {
                        type: "MemberExpression",
                        object: {type: "Identifier", name: "window"},
                        property: {type: "Identifier", name: "weaJs"}
                    },
                    property: {type: "Identifier", name: "alert"}
                },
                arguments: []
            }).forEach((nodePath: any) => {
                nodePath.node.callee.object.property.name = 'Dialog'
            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('weaJsAlertTran-e', e);
        }
        return code;
    }
}

const instance = new TransRuleForWeaJs();

export default instance;