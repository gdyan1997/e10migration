import jscodeshift from 'jscodeshift';
import bigInt from 'big-integer';

export class TransRuleForFormmode {

    transform = (fromModule: string, code: string, fieldMap: any, detailTables: any, cubeButton: string = '') => {

        if (fromModule != 'cube') return code;
        try {
            code = this.cubeBindFieldChangeEventTran(code);
            code = this.cubeBindDetailFieldChangeEventTran(code);
            code = this.cubeGetDetailRowSerailNumTran('cube', code, fieldMap, detailTables);
            code = this.cubeBrowserObjectTran(code);
            code = this.cubeGetDetailRowKeyTran('cube', code, fieldMap, detailTables);
            code = this.cubeGetDetailCheckedRowKeyTran(code);
            code = this.cubeGetFieldCurViewAttrTran(code);
            code = this.cubeReloadCardTran(code);
            code = this.cubeShowMessageTran(code);
            //code = this.cubeShowModalMsgTran(code);
            code = this.cubeShowLoadingTran(code);
            code = this.cubeCustomAddOrDelFunTran('cube', code, fieldMap, detailTables);
            code = this.cubeGetCurrentUserInfoTran(code);
            code = this.cubeSetTextFieldEmptyShowContentTran('cube', code, fieldMap, detailTables);
            code = this.cubeDoRightBtnEventTran(code,cubeButton);
            code = this.cubeRegisterCheckEventTran('cube', code, fieldMap, detailTables);
            code = this.cubeRegisterActionTran('cube', code, fieldMap, detailTables);
            code = this.cubeBindFieldActionTran(code);
            code = this.cubeDoCardSubmitTran(code,cubeButton);
            code = this.cubeOpenCustomDialogTran(code);
            code = this.cubeCloseCustomDialogTran(code);
            code = this.cubeSlideOpenModalTran(code);
            code = this.cubeChangeFieldAttrTran(code);
            code = this.cubeChangeSingleFieldTrans(code);

            //明细表行号转成rowid
            code = this.cubeDelDetailRowTrans(code);

            code = this.cubeModeFormEcodeTran(code);

            code = this.cubeUndefinedTran(code);
        } catch (e) {
            if (window.console) console.log("TransRuleForFormmode.transform", e);
        }

        return code;
    }

    /**
     * 将E9流程表单 ModeForm.bindFieldChangeEvent（表单字段值变化触发事件） 转换成E10的写法
     * ModeForm.bindFieldChangeEvent("field27555,field27556",function(obj,id,value){
     *   console.log("ModeForm.bindFieldChangeEvent--",obj,id,value);
     * });
     *
     * 转换成：
     * // 获取表单实例
     *     const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *     // 获取主表字段fieldId
     *     const textFieldMark = weFormSdk.convertFieldNameToId("wbk");
     *     const selectFieldMark = weFormSdk.convertFieldNameToId("xlk");
     *     // 绑定事件，对主表字段和明细表的某一行绑定
     *     weFormSdk.bindFieldChangeEvent(`${textFieldMark},${selectFieldMark}`, (data) => {
     *         // 取字段标识
     *         const fieldMark = data.id;
     *         // 取字段修改的值
     *         const value = data.value;
     *         console.log(data);
     *     });
     * @param code
     */
    cubeBindFieldChangeEventTran = (code: string) => {
        try {
            const root = jscodeshift(code);
            const sourceAstStrings = [
                "{\"type\": \"CallExpression\"," +
                "\"callee\": {\"type\": \"MemberExpression\"," +
                "\"object\": { \"type\": \"Identifier\", \"name\": \"ModeForm\" }," +
                "\"property\": { \"type\": \"Identifier\", \"name\": \"bindFieldChangeEvent\" }}," +
                "\"arguments\": [{},{ \"type\": \"FunctionExpression\" }]}"];

            sourceAstStrings.forEach(sourceAstString => {

                const sourceAst = JSON.parse(sourceAstString);

                //处理 ModeForm.bindDetailFieldChangeEvent
                const bindDetailFieldChangeEventCom = root
                    .find(jscodeshift.Node, sourceAst);

                if (bindDetailFieldChangeEventCom.length > 0) {
                    bindDetailFieldChangeEventCom.forEach((nodePath: any) => {

                        if (nodePath['node']['arguments'] && nodePath['node']['arguments'].length == 2) {

                            //直接去替换模版的方式
                            let $replace1$ = nodePath['node']['arguments'][0]
                            let $replace2$ = nodePath['node']['arguments'][1]['params']?.[0]?.['name'] || 'obj_upgrade'
                            let $replace3$ = nodePath['node']['arguments'][1]['params']?.[1]?.['name'] || 'id_upgrade'
                            let $replace4$ = nodePath['node']['arguments'][1]['params']?.[2]?.['name'] || 'value_upgrade'
                            let $replace5$ = nodePath['node']['arguments'][1]['body']['body']


                            const replacementAst = {
                                type: 'CallExpression',
                                callee: {
                                    type: 'MemberExpression',
                                    object: {type: 'Identifier', name: 'ModeForm'},
                                    property: {type: 'Identifier', name: 'bindFieldChangeEvent'},
                                },
                                arguments: [
                                    $replace1$,
                                    {
                                        type: 'FunctionExpression',
                                        params: [{type: 'Identifier', name: 'weaFormSdkData'}],
                                        body: {
                                            type: 'BlockStatement',
                                            body: [
                                                {
                                                    type: 'VariableDeclaration',
                                                    kind: 'var',
                                                    declarations: [{
                                                        type: 'VariableDeclarator',
                                                        id: {type: 'Identifier', name: $replace2$},
                                                        init: {type: 'Identifier', name: 'weaFormSdkData'}
                                                    }]
                                                },
                                                {
                                                    type: 'VariableDeclaration',
                                                    kind: 'var',
                                                    declarations: [{
                                                        type: 'VariableDeclarator',
                                                        id: {type: 'Identifier', name: $replace3$},
                                                        init: {
                                                            type: 'MemberExpression',
                                                            object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                            property: {type: 'Identifier', name: 'id'}
                                                        }
                                                    }]
                                                },
                                                {
                                                    type: 'VariableDeclaration',
                                                    kind: 'var',
                                                    declarations: [{
                                                        type: 'VariableDeclarator',
                                                        id: {type: 'Identifier', name: $replace4$},
                                                        init: {
                                                            type: 'MemberExpression',
                                                            object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                            property: {type: 'Identifier', name: 'value'}
                                                        }
                                                    }]
                                                },
                                                ...$replace5$
                                            ]
                                        }
                                    },
                                ],
                            };

                            nodePath.replace(replacementAst);

                        }
                    });
                }
            })
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeBindFieldChangeEventTran-e:', e);
        }
        return code;
    }

    /**
     * 将E9流程表单 ModeForm.bindDetailFieldChangeEvent（明细字段值变化触发事件） 转换成E10的写法
     * @param code
     */
    cubeBindDetailFieldChangeEventTran = (code: string) => {

        try {
            const root = jscodeshift(code);
            //处理 ModeForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.CallExpression)
                .filter((path: any) => {
                    return path.node['callee'].property && path.node['callee'].property.name == "bindDetailFieldChangeEvent";
                })
                .filter((path: any) => {
                    return path.node['callee'].object && path.node['callee'].object.name == "ModeForm";
                });

            if (bindDetailFieldChangeEventCom.length > 0) {
                bindDetailFieldChangeEventCom.forEach((nodePath: any) => {
                    if (nodePath.node.arguments && nodePath.node.arguments.length == 2) {
                        const argumentsTemp = nodePath.node.arguments[1];
                        const argumentsFunctionTemp = argumentsTemp.body.body;
                        const params = argumentsTemp.params;
                        var weFormSdkData = "";
                        for (let i = 0; i < params.length; i++) {
                            if (i == 0) weFormSdkData = weFormSdkData + " var " + params[i].name + " = weFormSdkData.id;\r\n";
                            if (i == 1) weFormSdkData = weFormSdkData + " var " + params[i].name + " = weFormSdkData.rowId;\r\n";
                            if (i == 2) weFormSdkData = weFormSdkData + " var " + params[i].name + " = weFormSdkData.value;\r\n";
                        }
                        const defaultVariables = jscodeshift(weFormSdkData).paths()[0].value.program.body;
                        for (let i1 = defaultVariables.length - 1; i1 >= 0; i1--) {
                            argumentsFunctionTemp.unshift(defaultVariables[i1]);
                        }

                        params.splice(0, params.length);
                        params.push(jscodeshift.identifier("weFormSdkData"));
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeBindDetailFieldChangeEventTran-e:', e);
        }
        return code;
    }

    /**
     * 获取明细已有行的数据库主键
     * ModeForm.getDetailRowKey("field112_3");   //获取明细第四行主键
     * 转换后：
     *  // 获取表单实例
     *  const ModeForm = window.WeFormSDK.getWeFormInstance();
     *  // 获取主表字段fieldId
     *  const detailMark = ModeForm.convertFieldNameToId("field112所属明细表名");
     *  // 获取第四行的rowId
     *  const oneRowId = ModeForm.getDetailRowIdByIndex(detailMark, 4);
     * @param code
     */
    cubeGetDetailRowKeyTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "getDetailRowKey"}
                },
                arguments: []
            };

            const wfGetDetailRowKeyComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetDetailRowKeyComs.length > 0) {
                // 定义正则表达式来匹配函数名
                const fieldNameRegex = /^field\d*$/;
                wfGetDetailRowKeyComs.forEach((nodePath) => {

                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 1
                        && nodePath['node']['arguments'][0].type === 'Literal'
                        && nodePath['node']['arguments'][0]['value']
                        && typeof nodePath['node']['arguments'][0]['value'] === 'string'
                        && nodePath['node']['arguments'][0]['value'].split('_').length === 2) {

                        // 直接去替换模版的方式
                        let $replace1$ = '字段' + nodePath.node.arguments[0]['value'].split('_')[0] + '所属明细表id';
                        if (fieldNameRegex.test(nodePath.node.arguments[0]['value'].split('_')[0])
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')]
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id) {
                            $replace1$ = fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id
                        }
                        let $replace2$ = parseInt(nodePath.node.arguments[0]['value'].split('_')[1]) + 1;
                        const targetAST: jscodeshift.CallExpression = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "ModeForm"},
                                property: {type: "Identifier", name: "getDetailRowIdByIndex"}
                            },
                            arguments: [{type: 'Literal', value: $replace1$}, {type: 'Literal', value: $replace2$}]
                        }
                        nodePath.replace(targetAST);
                    }

                });
                //检测代码转换后的代码是否正常
                if (jscodeshift(root.toSource()))
                    return root.toSource();
            }


        } catch (e) {
            if (window.console) console.log('cubeGetDetailRowKeyTran-e:', e);
        }

        return code;
    }

    /**
     * 获取明细选中行主键ID
     * ModeForm.getDetailCheckedRowKey("detail_1"); //输出选中行101，102...等等
     *
     * 转换成：
     *
     * // 获取表单实例
     * const ModeForm = window.WeFormSDK.getWeFormInstance();
     * // 明细表id
     * const detailMark = ModeForm.convertFieldNameToId('shm_glsj_mxb1');
     * // 获取选中行的rowId;result: 167954015280618401,167954015280624841
     * ModeForm.getDetailCheckedRowIndexStr(detailMark)
     *
     * detail_1 需要转换成明细表id
     */
    cubeGetDetailCheckedRowKeyTran = (code: string) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "getDetailCheckedRowKey"}
                }
            };

            //处理 ModeForm.bindDetailFieldChangeEvent
            const getDetailCheckedRowKeyComs = root
                .find(jscodeshift.Node, sourceAst);

            if (getDetailCheckedRowKeyComs.length > 0) {
                getDetailCheckedRowKeyComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['property']['name'] = 'getDetailCheckedRowIndexStr';
                })
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeGetDetailCheckedRowKeyTran-e:', e);
        }

        return code;
    }

    /**
     * 针对于浏览按钮的数据值对象进行转换
     * ModeForm.changeFieldValue("field11_2", {
     *     value: "2,3",
     *     specialobj:[
     *         {id:"2",name:"张三"},
     *         {id:"3",name:"李四"}
     *     ]
     * });
     *
     * 注意：
     * 1、value需要去掉
     * 2、specialobj由改成specialObj
     * 3、修改后其中的id还是需要手动调整的
     * @param code
     */
    cubeBrowserObjectTran = (code: string) => {

        try {
            const root = jscodeshift(code);

            //处理 ModeForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.ObjectExpression).filter(path => {
                    const hasValue = path.node.properties.some(
                        (prop: any) => prop.key?.name === 'value'
                    );
                    const hasSpecialObj = path.node.properties.some(
                        (prop: any) => prop.key?.name === 'specialobj'
                    );
                    return hasValue && hasSpecialObj;
                });

            if (bindDetailFieldChangeEventCom.length > 0) {
                // 遍历匹配的对象并处理
                bindDetailFieldChangeEventCom.forEach(path => {
                    // 移除 value 属性
                    path.node.properties = path.node.properties.filter(
                        (prop: any) => prop.key?.name !== 'value'
                    );

                    //console.log("path.node.properties", path.node.properties);

                    // 将 specialobj 改成 specialObj
                    path.node.properties = path.node.properties.map((prop: any) => {
                        if (prop.key?.name === 'specialobj') {
                            return jscodeshift.property(
                                'init',
                                jscodeshift.identifier('specialObj'),
                                prop['value']
                            );
                        }
                        return prop;
                    });
                    //console.log("path.node.properties", path.node.properties);
                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch
            (e) {
            if (window.console) console.log('cubeBrowserObjectTran-e:', e);
        }

        return code;
    }

    /**
     * 根据明细行标识获取序号(第几行)
     * 有两种写法，需要区分。
     * 1. 传入的参数是字段名，需要转换为字段id
     * 2. 传入的参数是字段id
     * ModeForm.getDetailRowSerailNum("detail_1", 3);    //获取明细1下标为3的行序号
     * ModeForm.getDetailRowSerailNum("field222_3");    //获取字段222对应明细表下标为3的行序号
     * 转换后：
     *  // 获取表单实例
     *  const ModeForm = window.WeFormSDK.getWeFormInstance();
     *  // 获取主表字段fieldId
     *  const detailMark = ModeForm.convertFieldNameToId("mxbo16sr2");
     *  // 这是结果的所在行下标：3
     *  const detailRowStr = ModeForm.getDetailRowSerailNum(detailMark,ModeForm.getDetailRowIdByIndex(detailMark, 4));
     * @param code
     */
    cubeGetDetailRowSerailNumTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "getDetailRowSerailNum"}
                },
                arguments: []
            };

            const wfGetDetailRowKeyComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetDetailRowKeyComs.length > 0) {
                // 定义正则表达式来匹配函数名
                const fieldNameRegex = /^field\d*$/;
                // 定义正则表达式来匹配函数名
                const detailTableRegex = /^detail_\d*$/;

                wfGetDetailRowKeyComs.forEach((nodePath) => {

                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 1
                        && nodePath['node']['arguments'][0].type === 'Literal'
                        && nodePath['node']['arguments'][0]['value']
                        && typeof nodePath['node']['arguments'][0]['value'] === 'string'
                        && nodePath['node']['arguments'][0]['value'].split('_').length === 2) {

                        // 直接去替换模版的方式
                        let $replace1$ = '字段' + nodePath.node.arguments[0]['value'].split('_')[0] + '所属明细表id';
                        if (fieldNameRegex.test(nodePath.node.arguments[0]['value'].split('_')[0])
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')]
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id) {
                            $replace1$ = fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id
                        }
                        let $replace2$ = parseInt(nodePath.node.arguments[0]['value'].split('_')[1]) + 1;
                        const targetAST: jscodeshift.CallExpression = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "ModeForm"},
                                property: {type: "Identifier", name: "getDetailRowSerailNum"}
                            },
                            arguments: [
                                {type: 'Literal', value: $replace1$},
                                {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "ModeForm"},
                                        property: {type: "Identifier", name: "getDetailRowIdByIndex"}
                                    },
                                    arguments: [{type: 'Literal', value: $replace1$}, {
                                        type: 'Literal',
                                        value: $replace2$
                                    }]
                                }
                            ]
                        }
                        nodePath.replace(targetAST);
                    } else if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 2
                        && nodePath['node']['arguments'][0].type === 'Literal'
                        && nodePath['node']['arguments'][0]['value']
                        && typeof nodePath['node']['arguments'][0]['value'] === 'string'
                        && nodePath['node']['arguments'][1].type === 'Literal'
                        && nodePath['node']['arguments'][1]['value']
                        && typeof nodePath['node']['arguments'][1]['value'] === 'number') {

                        // 直接去替换模版的方式
                        let $replace1$ = nodePath.node.arguments[0]['value'];
                        if (detailTableRegex.test($replace1$) && detailTables[$replace1$.replace('detail_', '')]) {
                            $replace1$ = detailTables[$replace1$.replace('detail_', '')]
                        }
                        let $replace2$ = nodePath.node.arguments[1]['value'] + 1;
                        const targetAST: jscodeshift.CallExpression = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "ModeForm"},
                                property: {type: "Identifier", name: "getDetailRowSerailNum"}
                            },
                            arguments: [
                                {type: 'Literal', value: $replace1$},
                                {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "ModeForm"},
                                        property: {type: "Identifier", name: "getDetailRowIdByIndex"}
                                    },
                                    arguments: [{type: 'Literal', value: $replace1$}, {
                                        type: 'Literal',
                                        value: $replace2$
                                    }]
                                }
                            ]
                        }
                        nodePath.replace(targetAST);
                    }

                });
                //检测代码转换后的代码是否正常
                if (jscodeshift(root.toSource()))
                    return root.toSource();
            }


        } catch (e) {
            if (window.console) console.log('wfGetDetailRowSerailNumTran-e:', e);
        }
        return code;
    }

    /**
     * 获取字段当前的只读/必填属性
     * ModeForm.getFieldCurViewAttr("field110_2");   //获取明细字段属性，1：只读、2：可编辑、3：必填；已办全部为只读；
     *
     * 转换后：
     *  // 获取表单实例
     *     const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *     // 获取主表字段
     *     const fieldMark = weFormSdk.convertFieldNameToId("wbk");
     *     // 获取该字段的读写属性
     *     weFormSdk.getFieldAttr(fieldMark);
     *
     * @param code
     */
    cubeGetFieldCurViewAttrTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "getFieldCurViewAttr"}
                },
                arguments: []
            };

            const wfGetFieldCurViewAttrComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetFieldCurViewAttrComs.length > 0) {
                wfGetFieldCurViewAttrComs.forEach((nodePath: any) => {
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {type: "Identifier", name: "ModeForm"},
                            property: {type: "Identifier", name: "getFieldAttr"}
                        },
                        arguments: nodePath.node.arguments
                    })
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeGetFieldCurViewAttrTran-e:', e);
        }
        return code;
    }

    /**
     * 此方法用于仅刷新当前卡片页面
     * ModeForm.reloadCard();
     *
     * 转换后：
     *  //全卡片刷新
     * var ebdFormSdk = window.ebdfpageSDK.getCardSDK();
     * ebdFormSdk.getCardStore().init(ebdFormSdk.getBaseInfo())
     *
     * @param code
     */
    cubeReloadCardTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "reloadCard"}
                },
                arguments: []
            };

            const wfGetFieldCurViewAttrComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetFieldCurViewAttrComs.length > 0) {
                wfGetFieldCurViewAttrComs.forEach((nodePath: any) => {
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {
                                type: "CallExpression",
                                callee: {
                                    type: "MemberExpression",
                                    object: {type: "Identifier", name: "ebdFormSdk"},
                                    property: {type: "Identifier", name: "getCardStore"}
                                },
                                arguments: []
                            },
                            property: {type: "Identifier", name: "init"}
                        },
                        arguments: [{
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "ebdFormSdk"},
                                property: {type: "Identifier", name: "getBaseInfo"}
                            },
                            arguments: []
                        }]
                    })
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeReloadCardTran-e:', e);
        }
        return code;
    }

    /**
     * 可控制显示时间的message信息
     * ModeForm.showMessage("结束时间需大于开始时间");   //警告信息，1.5s后自动消失
     * ModeForm.showMessage("运算错误", 2, 10);  //错误信息，10s后消失
     * 轉換後
     * // 普通提示
     * window.WeFormSDK.showMessage("This is a success prompt", 3, 2);
     * @param code
     */
    cubeShowMessageTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAsts: jscodeshift.CallExpression[] = [{
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "showMessage"}
                },
                arguments: []
            }, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "showConfirm"}
                },
                arguments: []
            }];
            sourceAsts.forEach((sourceAst) => {
                const wfConvertFieldNameToIdComs = root
                    .find(jscodeshift.CallExpression, sourceAst);

                if (wfConvertFieldNameToIdComs.length > 0) {
                    wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                        nodePath['node']['callee']['object']['name'] = 'window.WeFormSDK';
                    });
                }
            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeShowMessageTran-e:', e);
        }
        return code;
    }

    /**
     * 系统样式的Modal弹出框：
     *
     * ModeForm.showModalMsg("系统提示","提示的信息内容",3) //成功提示信息
     * ModeForm.showModalMsg("系统提示","<div style='color:red;'>提示的信息内容</div>",2)  //错误提示信息
     *
     * 轉換後
     *
     * ModeFormEcode.showModalMsg("系统提示","提示的信息内容",3) //成功提示信息
     * ModeFormEcode.showModalMsg("系统提示","<div style='color:red;'>提示的信息内容</div>",2)  //错误提示信息
     *
     *
     * @param code
     */
    cubeShowModalMsgTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "showModalMsg"}
                },
                arguments: []
            };

            const cubeShowModalMsgComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (cubeShowModalMsgComs.length > 0) {
                cubeShowModalMsgComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['object']['name'] = 'ModeFormEcode';
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeShowModalMsgTran-e:', e);
        }
        return code;
    }

    /**
     * 控制全局loading的显示/隐藏：——需要更具参数进行判断
     *
     * ModeForm.showLoading(true,'large','模块加载中...');
     *
     * setTimeout(function(){
     *     //...
     *     ModeForm.showLoading(false);
     * },2000)
     *
     * 轉換後
     *
     *  const { globalSpin } = window.weappUi.Spin;
     *     //打开
     *     globalSpin.start({spinning:true,text:'loading...'})
     *     //关闭
     *     globalSpin.end()
     *     globalSpin.destroy()
     *
     * @param code
     */
    cubeShowLoadingTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "showLoading"}
                },
                arguments: []
            };

            const cubeShowModalMsgComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (cubeShowModalMsgComs.length > 0) {
                cubeShowModalMsgComs.forEach((nodePath: any) => {
                    const {node} = nodePath;
                    // console.log(node);
                    if (node?.arguments?.length > 0) {
                        let params1 = node.arguments?.[0]?.value || false;
                        let params2 = node.arguments?.[1]?.value;
                        let params3 = node.arguments?.[2]?.value || '';
                        if (params1 && params1 === true) {
                            nodePath.replace({
                                type: "CallExpression",
                                callee: {
                                    type: "MemberExpression",
                                    object: {
                                        type: "MemberExpression",
                                        object: {
                                            type: "MemberExpression",
                                            object: {
                                                type: "MemberExpression",
                                                object: {type: "Identifier", name: "window"},
                                                property: {type: "Identifier", name: "weappUi"},
                                            },
                                            property: {type: "Identifier", name: "Spin"},
                                        },
                                        property: {type: "Identifier", name: "globalSpin"},
                                    },
                                    property: {type: "Identifier", name: "start"},
                                },
                                arguments: [{
                                    type: "ObjectExpression",
                                    properties: [{
                                        type: "ObjectProperty",
                                        key: {type: "Identifier", name: "spinning"},
                                        value: {type: "Literal", value: true}
                                    }, {
                                        type: "ObjectProperty",
                                        key: {type: "Identifier", name: "text"},
                                        value: {type: "Literal", value: params3}
                                    }]
                                }]
                            });
                        } else {
                            nodePath.replace(jscodeshift("window.weappUi.Spin.globalSpin.end();\n" +
                                "window.weappUi.Spin.globalSpin.destroy()").find(jscodeshift.Program).get(0).node);
                        }
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeShowLoadingTran-e:', e);
        }
        return code;
    }

    /**
     * 明细新增行渲染后触发事件
     * 说明：重载_customAddFun"+groupid+"函数，groupid从1开始递增，1代表明细1；不论手动添加、联动添 加、接口添加都会触发
     * function _customAddFun1(addIndexStr){      //明细2新增成功后触发事件，addIndexStr即刚新增的行标示，添加多行为(1,2,3)
     *     console.log("新增的行标示："+addIndexStr);
     * }
     * 转换后：
     *     const ModeForm = window.WeFormSDK.getWeFormInstance();
     *     const detailMark = ModeForm.convertFieldNameToId("formtable_main_508_dt1");
     *     // 明细表添加行事件，index为添加行下标，从0开始
     *     ModeForm.registerAction(`${window.WeFormSDK.ACTION_ADDROW}${detailMark}`, (index, data)=>{
     *     alert("添加行下标是"+index);
     *     });
     * @param code
     */
    cubeCustomAddOrDelFunTran = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);
            // 定义正则表达式来匹配函数名
            const functionNameRegex = [/^_customAddFun\d*$/, /^_customDelFun\d*$/];

            functionNameRegex.forEach((functionNameRegex: any) => {
                //处理 ModeForm.bindDetailFieldChangeEvent
                const bindDetailFieldChangeEventCom = root
                    .find(jscodeshift.FunctionDeclaration).filter((nodePath: any) => {
                        const functionName = nodePath.node.id?.name;
                        return functionNameRegex.test(functionName);
                    });

                if (bindDetailFieldChangeEventCom.length > 0) {

                    bindDetailFieldChangeEventCom.forEach((nodePath: any) => {
                        const comments = nodePath?.['node']?.['comments'];
                        let addFlag = true;
                        if (nodePath['node']['id']['name'].indexOf('_customDelFun') === 0) {
                            addFlag = false;
                        }
                        const detailNum = parseInt(nodePath['node']['id']['name'].replace('_customAddFun', '').replace('_customDelFun', ''));//不需要加1，建模的是从1开始
                        let detailMark = detailTables && detailTables[`${detailNum}`] ? detailTables[`${detailNum}`] : `明细表${detailNum}id`;
                        const replacementAst: jscodeshift.ExpressionStatement = {
                            type: 'ExpressionStatement',
                            expression: {
                                type: 'CallExpression',
                                callee: {
                                    type: 'MemberExpression',
                                    object: {type: 'Identifier', name: 'ModeForm'},
                                    property: {type: 'Identifier', name: 'registerAction'},
                                },
                                arguments: [
                                    {
                                        type: 'BinaryExpression',
                                        left: {
                                            type: 'MemberExpression',
                                            object: {
                                                type: 'MemberExpression',
                                                object: {type: 'Identifier', name: 'window'},
                                                property: {
                                                    type: 'Identifier',
                                                    name: 'WeFormSDK'
                                                }
                                            },
                                            property: {
                                                type: 'Identifier',
                                                name: addFlag ? 'ACTION_ADDROW' : 'ACTION_DELROW'
                                            }
                                        },
                                        right: {
                                            type: 'Literal',
                                            value: detailMark
                                        },
                                        operator: '+'
                                    },
                                    {
                                        type: 'FunctionExpression',
                                        body: {
                                            type: 'BlockStatement',
                                            body: nodePath['node']['body']['body']
                                        },
                                        params: [{
                                            type: 'Identifier',
                                            name: nodePath['node']['params'][0] && nodePath['node']['params'][0]['name'] ? nodePath['node']['params'][0]['name'] : 'index_e10'
                                        }, {
                                            type: 'Identifier',
                                            name: 'data_e10'
                                        }]
                                    }
                                ]
                            }
                        };

                        nodePath.replace(replacementAst);
                        if (comments)
                            nodePath.node.comments = comments;

                    });

                }
            });
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeCustomAddOrDelFunTran-e:', e);
        }

        return code;
    }

    /**
     * 获取当前用户信息
     * var userinfo = ModeForm.getCurrentUserInfo();  //当前用户信息
     * 返回的结构：{
     *     "subcompanyid": "4",
     *     "loginid": "zs1",
     *     "jobtitlesname": "default1",
     *     "departmentname": "zs部门1",
     *     "departmentid": "23",
     *     "icon": "/messager/images/icon_m_wev8.jpg",
     *     "subcompanyname": "zs分部",
     *     "jobtitleid": "45",
     *     "userid": "90",
     *     "email": "desensitization____random__F489D0B0F7B78406E2935650E61F7641",
     *     "username": "zs1"
     * }
     * 转换后：
     * var userinfo = window.ebuilderSDK.getCurrUser() //当前用户信息——和E9的信息基本都不一致，无法使用
     * var username = userinfo.name;
     *
     * 对应关系：
     * "subcompanyid": "department.subCompanyId",
     * "loginid": 暂无
     * "jobtitlesname": 暂无
     * "departmentname":  "department.name",
     * "departmentid":  "department.id"
     * "icon": 暂无
     * "subcompanyname":暂无
     * "jobtitleid": 暂无
     * "userid": "userId",
     * "email":  暂无
     * "username": "name"
     * @param code
     */
    cubeGetCurrentUserInfoTran = (code: string) => {
        try {
            let root = jscodeshift(code);

            const e9TypeToE10Type: Record<string, string> = {
                username: "name",
                userid: "userId",
                departmentid: "department.id",
                departmentname: "department.name",
                subcompanyid: "department.subCompanyId",
                // loginid: "",
                // jobtitlesname: "",
                // icon: "",
                // subcompanyname: "",
                // jobtitleid: "",
                // email: ""
            }

            //第一步： 找到定义 var user = ModeForm.getCurrentUserInfo() 的地方，处理定义引用的情况
            const getCurrentUserInfoComs2Ref = root.find(jscodeshift.VariableDeclarator,
                {
                    init: {
                        type: 'CallExpression',
                        callee: {
                            type: 'MemberExpression',
                            object: {name: 'ModeForm'},
                            property: {name: 'getCurrentUserInfo'}
                        }
                    }
                });

            // 遍历每个定义 userinfo 的地方，找到所有引用
            getCurrentUserInfoComs2Ref.forEach((declaration: any) => {
                const userinfoName = declaration.node.id.name;

                // 找到当前定义的变量 userinfo 的范围
                const scope = jscodeshift(declaration).closestScope();

                // 在当前范围内查找所有引用 userinfo 的地方
                const userinfoReferences = scope
                    .find(jscodeshift.MemberExpression, {
                        object: {
                            type: 'Identifier',
                            name: userinfoName
                        }
                    }).forEach((reference: any) => {
                        if (e9TypeToE10Type[reference["node"]["property"]["name"]]) {
                            reference["node"]["property"]["name"] = e9TypeToE10Type[reference["node"]["property"]["name"]];
                        } else {
                            reference.node.comments = [
                                jscodeshift.commentLine(`E10用户信息${userinfoName}中不存在属性 ${reference["node"]["property"]["name"]}!`)
                            ];
                        }
                    });

                // 输出每个引用的位置
                userinfoReferences.forEach((reference: any) => {
                });
            });

            //第二步： 找到定义 ModeForm.getCurrentUserInfo().username 的形式，处理非引用的情况
            const getCurrentUserInfoComs2Property = root.find(jscodeshift.MemberExpression,
                {
                    object: {
                        type: 'CallExpression',
                        callee: {
                            type: 'MemberExpression',
                            object: {name: 'ModeForm'},
                            property: {name: 'getCurrentUserInfo'}
                        }
                    }
                });
            getCurrentUserInfoComs2Property.forEach((declaration: any) => {
                const perpertyName = declaration.node.property?.name;
                if (e9TypeToE10Type[perpertyName]) {
                    declaration.node.property.name = e9TypeToE10Type[perpertyName];
                } else {
                    declaration.node.comments = [
                        jscodeshift.commentLine(`E10用户信息中不存在属性 ${perpertyName}!`)
                    ];
                }
            });

            //第三步： 处理ModeForm.getCurrentUserInfo()本身表达式的替换
            const getCurrentUserInfoComs = root.find(jscodeshift.CallExpression,
                {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {name: 'ModeForm'},
                        property: {name: 'getCurrentUserInfo'}
                    }
                });
            getCurrentUserInfoComs.forEach((declaration: any) => {
                declaration.replace({
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {
                            type: 'MemberExpression',
                            object: {type: 'Identifier', name: 'window'},
                            property: {type: 'Identifier', name: 'ebuilderSDK'}
                        },
                        property: {type: 'Identifier', name: 'getCurrUser'}
                    },
                    arguments: []
                });
            });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeGetCurrentUserInfoTran-e:', e);
        }

        return code;
    }

    /**
     * 系统样式的Modal弹出框：
     *
     * 文本字段可编辑状态，当值为空显示默认灰色提示信息，鼠标点击输入时 提示消失
     * 限定条件：仅支持单行文本、整数、浮点数、千分位、多行文本字段(非html)字段类型；支持主字段及明细字 段
     *
     * setTextFieldEmptyShowContent:function(ﬁeldMark,showContent)
     *
     * 轉換後
     *
     * //fieldId 字段id；detailId 明细表id；detailNum 明细行 例：1、2、3；content 提示内容
     * CommonFormEcode.setTextFieldEmptyShowContent('973131124361216001','973130840859820033', 1,'显示内容')
     * @param code
     */
    cubeSetTextFieldEmptyShowContentTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "setTextFieldEmptyShowContent"}
                },
                arguments: []
            };

            const cubeShowModalMsgComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (cubeShowModalMsgComs.length > 0) {
                // 定义正则表达式来匹配函数名
                const fieldNameRegex = /^field\d*$/;
                cubeShowModalMsgComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['object']['name'] = 'CommonFormEcode';
                    if (nodePath['node']['arguments'].length == 2 && nodePath['node']['arguments'][0]?.value?.indexOf('field') >= 0) {
                        let fieldName = nodePath['node']['arguments'][0]?.value?.replace('field', '');
                        let fieldId = fieldName.split('_')[0];
                        let detailId = "";
                        let detailNum = 1;
                        let content = nodePath['node']['arguments'][1];
                        if (fieldName.indexOf("_") > 0) {
                            fieldId = fieldMap[fieldName.split('_')[0]]?.e10_field_id || '字段ID：' + fieldId + '需转成E10字段ID';
                            detailId = fieldMap[fieldName.split('_')[0]]?.e10_sub_form_id || "";
                            detailNum = parseInt(fieldName.split('_')[1]) + 1;
                        } else {
                            fieldId = fieldMap[fieldName]?.e10_field_id || fieldId;
                        }

                        nodePath['node']['arguments'] = [
                            {
                                type: 'StringLiteral',
                                value: fieldId
                            },
                            {
                                type: 'StringLiteral',
                                value: detailId
                            },
                            {
                                type: 'NumericLiteral',
                                value: detailNum
                            },
                            content
                        ]

                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeSetTextFieldEmptyShowContentTran-e:', e);
        }
        return code;
    }

    /**
     * 调用右键按钮事件
     * //系统自带的可以直接在控制台查看按钮方法名
     * ModeForm.doRightBtnEvent("viewLog");  //查看日志
     *
     * 版本支持： 900200401
     * //针对自定义的页面扩展按钮，type= doCustomFunction;需要指定对应的expendid来执行
     * //或者传第三个参数扩展名称，支持对语言解析
     * eg:
     *    ModeForm.doRightBtnEvent('doCustomFunction',"62358");//传扩展id调用
     *    ModeForm.doRightBtnEvent('doCustomFunction','','test')；//传name
     *    ModeForm.doRightBtnEvent('doCustomFunction','','姓名~~name~~姓名');//多语言直接用  ~~ 分割
     *
     * 转换后：
     *
     * //系统自带的可以在接口中的按钮名称或直接在控制台查看按钮data-id
     * const ebdFormSdk = window.ebdfpageSDK.getCardSDK();
     * ebdFormSdk.triggerTopButton('762820349675487278');//系统或自定义按钮id
     * ebdFormSdk.triggerTopButton('edit');//只有系统按钮支持，传入按钮名称
     *
     * @param code
     */
    cubeDoRightBtnEventTran = (code: string, cubeButton: string = '') => {
        try {
            let root = jscodeshift(code);

            const e9TypeToE10Type: Record<string, string> = {
                doSubmit: "save,editSave",
                doSubNew: "saveAndCreate",
                doSubmitDraft: "tempSave",
                toEdit: "edit",
                doSubCopy: "saveAndCopy",
                toDel: "delete",
                doPrint: "print",
                doShare: "share",
            }

            //第三步： 处理ModeForm.getCurrentUserInfo()本身表达式的替换
            const doRightBtnEventComs = root.find(jscodeshift.CallExpression,
                {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {name: 'ModeForm'},
                        property: {name: 'doRightBtnEvent'}
                    },
                    arguments: []
                });
            const doRightBtnEventComsArray = doRightBtnEventComs.paths().map(path => path);
            const numberPattern = /^\d+$/;//判断是否为数字
            doRightBtnEventComsArray.map((declaration: any) => {
                let doRightBtnEvent = declaration.node.arguments[0]?.value;
                let expendId = declaration.node.arguments[1]?.value;
                let expendName = declaration.node.arguments[2]?.value;
                //处理doCustomFunction，
                if (doRightBtnEvent == 'doCustomFunction') {
                    if (numberPattern.test(expendId)) {
                        expendId = bigInt(cubeButton).plus(bigInt(expendId)).toString();
                    } else {
                        if (expendId === '' && expendName != '')
                            expendId = "扩展名称[" + expendName + "]需要转成E10EB按钮ID"
                        else
                            expendId = "页面扩展ID[" + expendId + "]需要转成E10EB按钮ID"
                    }
                } else {
                    if (e9TypeToE10Type[doRightBtnEvent]) {
                        expendId = e9TypeToE10Type[doRightBtnEvent];
                    } else {
                        expendId = "按钮方法名[" + doRightBtnEvent + "]需要转成E10EB按钮ID"
                    }
                }
                //特殊处理需要将save和editSave合并
                //E9中新建保存、编辑保存都是相同的function名称，需要转换为E10中新建保存+编辑保存
                if (expendId === "save,editSave") {
                    // 替换当前节点为第一个新节点
                    declaration.replace({
                        type: 'CallExpression',
                        callee: {
                            type: 'MemberExpression',
                            object: {type: 'Identifier', name: 'ebdFormSdk'},
                            property: {type: 'Identifier', name: 'triggerTopButton'}
                        },
                        arguments: [{type: 'Literal', value: 'save'}]
                    });
                    const secondNewNode = jscodeshift.expressionStatement(
                        jscodeshift.callExpression(
                            jscodeshift.memberExpression(
                                jscodeshift.identifier('ebdFormSdk'),
                                jscodeshift.identifier('triggerTopButton')
                            ),
                            [jscodeshift.literal('editSave')]
                        )
                    );
                    const expressionBody = jscodeshift(declaration).closest(jscodeshift.ExpressionStatement);
                    // const lastStatement = functionBody.find(jscodeshift.Statement).at(-1).get();

                    // 在当前节点后面插入新节点，注意这个expressionBody的外层要是一个数组，不然会报错的
                    //如果是是用declaration 会发现他的外层是一个对象ExpressionStatement，就会报错了！！！
                    expressionBody.insertAfter([secondNewNode]);
                } else {
                    declaration.replace({
                        type: 'CallExpression',
                        callee: {
                            type: 'MemberExpression',
                            object: {type: 'Identifier', name: 'ebdFormSdk'},
                            property: {type: 'Identifier', name: 'triggerTopButton'}
                        },
                        arguments: [{type: 'Literal', value: expendId}]
                    })
                }
            });
            // 等待所有的异步操作完成
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeDoRightBtnEventTran-e:', e);
        }

        return code;
    }

    /**
     * 将E9流程表单 注册拦截事件，指定动作执行前触发，并可阻断/放行后续操作
     * jQuery().ready(function(){
     *      ModeForm.registerCheckEvent(ModeForm.OPER_ADDROW+"1", function(callback){
     *           alert("添加明细1前执行逻辑，明细1则是OPER_ADDROW+1，依次类推")
     *           callback(); //允许继续添加行调用callback，不调用代表阻断添加
     *      });
     *       ModeForm.registerCheckEvent(ModeForm.OPER_DELROW+"2", function(callback){
     *          alert("删除明细2前执行逻辑")
     *          callback(); //允许继续删除行调用callback，不调用代表阻断删除
     *       });
     * })
     * 转成：
     *     const ModeForm = window.WeFormSDK.getWeFormInstance();
     *     const detailMark = ModeForm.convertFieldNameToId("formtable_main_508_dt1");
     *     // 注册保存事件
     *     ModeForm.registerCheckEvent(`${window.WeFormSDK.OPER_ADDROW}${detailMark}`, (callback: Function, failFn: Function)=>{
     *         // ...执行定义逻辑
     *         callback();
     *     });
     *
     * @param code
     */
    cubeRegisterCheckEventTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "registerCheckEvent"}
                }
            };

            //处理 ModeForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.Node, sourceAst);

            if (bindDetailFieldChangeEventCom.length > 0) {
                const registerCheckEventTypeForDetail: Record<string, string> = {
                    OPER_ADDROW: 'OPER_ADDROW',
                    OPER_DELROW: 'OPER_DELROW'
                };
                const numberPattern = /^\d+$/;//判断是否为数字
                const numberEndWithDotPattern = /\d+(,|$)/;//数字加逗号结尾
                bindDetailFieldChangeEventCom.forEach((nodePath: any) => {

                    if (nodePath.node.arguments && nodePath.node.arguments.length === 2) {

                        //节点直接替换
                        if (nodePath.node.arguments[0]) {

                            //处理本身就是表达是的情况
                            if (nodePath.node.arguments[0].type === 'BinaryExpression') {
                                let tempNodePath = nodePath.node.arguments[0];
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.left.type === 'MemberExpression' || tempNodePath.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.left);
                                }
                                if (tempNodePath.right.type === 'MemberExpression' || tempNodePath.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // 处理ModeForm.OPER_ADDROW 和 ModeForm.OPER_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'ModeForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventTypeForDetail[expression.property.name]) {
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventTypeForDetail[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });

                            }

                            //节点是表达式需要找到所有的子节点都进行替换
                            //filter 包含当前节点
                            //find   不包括当前节点
                            let argumentsTemp = jscodeshift(nodePath.node.arguments[0])
                                .find(jscodeshift.BinaryExpression);
                            argumentsTemp.forEach((tempNodePath: any) => {
                                // console.log('tempNodePath', tempNodePath.node);
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.node.left.type === 'MemberExpression' || tempNodePath.node.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.left);
                                }
                                if (tempNodePath.node.right.type === 'MemberExpression' || tempNodePath.node.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // 处理ModeForm.OPER_ADDROW 和 ModeForm.OPER_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'ModeForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventTypeForDetail[expression.property.name]) {
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventTypeForDetail[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });
                            });
                        }

                        //是方法需要加一个参数
                        if (nodePath.node.arguments[1] && nodePath.node.arguments[1].type === 'FunctionExpression') {
                            nodePath.node.arguments[1] = jscodeshift.functionExpression(
                                null,
                                [...nodePath.node.arguments[1].params
                                    , jscodeshift.identifier('failFn')],
                                nodePath.node.arguments[1].body
                            );
                        }
                        const replacementAstForDetail = {
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {type: 'Identifier', name: 'ModeForm'},
                                property: {type: 'Identifier', name: 'registerCheckEvent'},
                            },
                            arguments: [
                                nodePath.node.arguments[0],
                                nodePath.node.arguments[1]
                            ]
                        };

                        nodePath.replace(replacementAstForDetail);
                    }

                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeRegisterCheckEventTran-e:', e);
        }

        return code;
    }


    /**
     * 注册钩子事件，指定动作完成后触发
     * 支持多次调用注册，按注册的先后顺序依次执行
     * ModeForm.registerAction(ModeForm.ACTION_ADDROW+"1", function(index){
     *     alert("添加行下标是"+index);
     * });     //下标从1开始，明细1添加行触发事件，注册函数入参为新添加行下标
     *
     * ModeForm.registerAction(ModeForm.ACTION_DELROW+"2", function(arg){
     *    alert("删除行下标集合是"+arg.join(","));
     * });     //下标从1开始，明细2删除行触发事件
     *
     * 转换后：
     *
     *     const ModeForm = window.WeFormSDK.getWeFormInstance();
     *     const detailMark = ModeForm.convertFieldNameToId("formtable_main_508_dt1");
     *     // 明细表添加行事件，index为添加行下标，从0开始
     *     ModeForm.registerAction(`${window.WeFormSDK.ACTION_ADDROW}${detailMark}`, (index, data)=>{
     *     alert("添加行下标是"+index);
     *     });
     *
     * @param code
     */
    cubeRegisterActionTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "registerAction"}
                }
            };

            //处理 ModeForm.registerCheckEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.Node, sourceAst);

            if (bindDetailFieldChangeEventCom.length > 0) {
                const numberPattern = /^\d+$/;//判断是否为数字
                const numberEndWithDotPattern = /\d+(,|$)/;//数字加逗号结尾
                const registerCheckEventType: Record<string, string> = {
                    ACTION_ADDROW: "ACTION_ADDROW",
                    ACTION_DELROW: "ACTION_DELROW"
                };
                bindDetailFieldChangeEventCom.forEach((nodePath: any) => {

                    if (nodePath.node.arguments && nodePath.node.arguments.length === 2) {

                        //节点直接替换
                        if (nodePath.node.arguments[0]) {

                            //处理本身就是表达是的情况
                            if (nodePath.node.arguments[0].type === 'BinaryExpression') {
                                let tempNodePath = nodePath.node.arguments[0];
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.left.type === 'MemberExpression' || tempNodePath.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.left);
                                }
                                if (tempNodePath.right.type === 'MemberExpression' || tempNodePath.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // 处理ModeForm.OPER_ADDROW 和 ModeForm.OPER_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'ModeForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventType[expression.property.name]) {
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventType[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });

                            }

                            //节点是表达式需要找到所有的子节点都进行替换
                            //filter 包含当前节点
                            //find   不包括当前节点
                            let argumentsTemp = jscodeshift(nodePath.node.arguments[0])
                                .find(jscodeshift.BinaryExpression);
                            argumentsTemp.forEach((tempNodePath: any) => {
                                // console.log('tempNodePath', tempNodePath.node);
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.node.left.type === 'MemberExpression' || tempNodePath.node.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.left);
                                }
                                if (tempNodePath.node.right.type === 'MemberExpression' || tempNodePath.node.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // 处理ModeForm.OPER_ADDROW 和 ModeForm.OPER_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'ModeForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventType[expression.property.name]) {
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventType[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });
                            });
                        }

                        //是方法需要加一个参数
                        //防止参数存在data的问题
                        if (nodePath.node.arguments[1] && nodePath.node.arguments[1].type === 'FunctionExpression') {
                            nodePath.node.arguments[1] = jscodeshift.functionExpression(
                                null,
                                [...nodePath.node.arguments[1].params
                                    , jscodeshift.identifier('data')],
                                nodePath.node.arguments[1].body
                            );
                        }

                        const replacementAst = {
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {type: 'Identifier', name: 'ModeForm'},
                                property: {type: 'Identifier', name: 'registerAction'},
                            },
                            arguments: [
                                nodePath.node.arguments[0],
                                nodePath.node.arguments[1]
                            ]
                        };

                        nodePath.replace(replacementAst);
                    }

                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeRegisterActionTran-e:', e);
        }

        return code;
    }

    /**
     * 字段区域绑定动作事件
     * onblur 失去焦点事件，仅支持单行文本类型
     * onfocus 获取焦点事件，仅支持单行文本字段类型
     * onclick 单击事件，字段所在单元格区域单击触发
     * ondbclick 双击事件，字段所在单元格区域双击触发
     * mouseover 鼠标移入事件，鼠标移入字段所在单元格区域触发
     * mouseout 鼠标移出事件，鼠标移出字段所在单元格区域触发
     *
     * 场景一：
     * ModeForm.bindFieldAction("onfocus", "field111,field222", function(fieldid,rowIndex){
     *       console.log("单行文本字段111获取焦点触发事件");
     *       console.log("明细第"+rowIndex+"行字段fieldid222获取焦点触发事件");
     * });
     *
     * ModeForm.bindFieldAction("onclick", "field333", function(){
     *    console.log("浏览按钮字段单击触发事件，不是指点放大镜选择，是整个字段所在单元格区域单击都会触发");
     * });
     *
     * 转成
     * ModeForm.bindFieldAction("onfocus", "field111,field222", function(data){
     *         // 取字段标识
     *         const fieldid = data.id;
     *         // 取字段修改的值
     *         const value = data.value;
     *         // 修改行号
     *         const rowIndex = data.rowId;
     * });
     *
     * 场景二：
     * blur => onblur
     * focus => onfocus
     * click => onclick
     * dblclick => ondbclick
     * mouseover => mouseover
     * mouseout => mouseout
     *
     * $("#field127245").bind("blur",function(){})
     * 转成
     * ModeForm.bindFieldAction("onblur", "field127245", function(data){
     * });
     *
     * 场景三：
     * $("#field30899").blur();
     * $("#field30899").focus();
     * $("#field30899").click();
     * $("#field30899").dblclick();
     * 转成
     * WfForm.bindFieldAction("onblur", "field30899", function(data){
     * });
     * WfForm.bindFieldAction("onfocus", "field30899", function(data){
     * });
     * WfForm.bindFieldAction("onclick", "field30899", function(data){
     * });
     * WfForm.bindFieldAction("ondbclick", "field30899", function(data){
     * });
     */
    cubeBindFieldActionTran = (code: string) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "bindFieldAction"}
                }
            };

            //处理 ModeForm.bindFieldAction
            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.Node, sourceAst);

            if (wfConvertFieldNameToIdComs.length > 0) {
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 3
                        && nodePath.node.arguments[2].params
                        && nodePath.node.arguments[2].type === 'FunctionExpression') {

                        // 直接去替换模版的方式
                        let $replace1$ = nodePath.node.arguments[0]
                        let $replace2$ = nodePath.node.arguments[1]
                        // let $replace3$ = nodePath.node.arguments[2]?.params?.[0]?.name
                        // let $replace4$ = nodePath.node.arguments[2]?.params?.[1]?.name
                        let $replace5$ = nodePath.node.arguments[2].body.body

                        let bodyComs = [...$replace5$];

                        let paramsNum = nodePath.node.arguments[2]?.params?.length || 0;
                        // 使用 for 循环遍历 paramsNum，从paramsNum-1开始
                        //只有 i == 0 i == 1时有意义，其他默认都是id
                        for (let i = paramsNum - 1; i > -1; i--) {
                            if (nodePath.node.arguments[2]?.params?.[i]?.name) {
                                bodyComs.unshift({
                                    type: 'VariableDeclaration',
                                    kind: 'var',
                                    declarations: [{
                                        type: 'VariableDeclarator',
                                        id: {type: 'Identifier', name: nodePath.node.arguments[2]?.params?.[i]?.name},
                                        init: {
                                            type: 'MemberExpression',
                                            object: {type: 'Identifier', name: 'weaFormSdkData'},
                                            property: {
                                                type: 'Identifier',
                                                name: i == 0 ? 'id' : (i == 1 ? 'rowId' : 'id')
                                            }
                                        }
                                    }]
                                })

                            }
                        }

                        const targetAst = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "ModeForm"},
                                property: {type: "Identifier", name: "bindFieldAction"}
                            },
                            arguments: [
                                $replace1$,
                                $replace2$,
                                {
                                    type: "FunctionExpression",
                                    params: [{type: 'Identifier', name: 'weaFormSdkData'}],
                                    body: {
                                        type: 'BlockStatement',
                                        body: bodyComs
                                    }
                                }
                            ]
                        };

                        nodePath.replace(targetAst);

                    }
                })
            }


            //第二步处理：$("#field127245").bind("blur",function(){})
            const fieldNameRegex = /^#field\d*$/;
            // 找到所有使用 bind() 方法的调用表达式
            root.find(jscodeshift.CallExpression, {
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'CallExpression',
                        callee: {
                            type: 'Identifier',
                            name: 'jQuery'
                        }
                    },
                    property: {
                        type: 'Identifier',
                        name: 'bind'
                    }
                }
            }).forEach((path: any) => {
                const callExpression = path.value.callee.object;
                const selector = callExpression.arguments[0].value;
                const args = path.value.arguments;

                if (fieldNameRegex.test(selector)) {

                    if (args.length === 2 && args[0].type === 'Literal' && args[1].type === 'FunctionExpression') {
                        const event = args[0].value;
                        const handler = args[1];

                        if (typeof event === 'string') {
                            // 将事件名转换为对应的属性名
                            let eventName;
                            switch (event) {
                                case 'blur':
                                    eventName = 'onblur';
                                    break;
                                case 'focus':
                                    eventName = 'onfocus';
                                    break;
                                case 'click':
                                    eventName = 'onclick';
                                    break;
                                case 'dblclick':
                                    eventName = 'ondbclick';
                                    break;
                                case 'mouseover':
                                    eventName = 'onmouseover';
                                    break;
                                case 'mouseout':
                                    eventName = 'onmouseout';
                                    break;
                                default:
                                    eventName = event;
                            }

                            // 构造新的调用表达式
                            const newCallExpression = jscodeshift.callExpression(
                                jscodeshift.memberExpression(
                                    jscodeshift.identifier('ModeForm'),
                                    jscodeshift.identifier('bindFieldAction')
                                ), [
                                    jscodeshift.literal(eventName),
                                    jscodeshift.literal(selector), // 字段 ID
                                    handler // 事件处理函数
                                ]
                            );

                            // 用新的调用表达式替换原有的调用表达式
                            path.replace(newCallExpression);
                        }
                    }
                }
            });

            //第三步处理：$("#field30899").blur(); $("#field30899").focus(); $("#field30899").click(); $("#field30899").dblclick();
            const eventMappings = {
                blur: 'onblur',
                focus: 'onfocus',
                click: 'onclick',
                dblclick: 'ondbclick'
            };

            Object.entries(eventMappings).forEach(([originalEvent, newEvent]) => {
                root.find(jscodeshift.CallExpression, {
                    callee: {
                        type: 'MemberExpression',
                        object: {
                            type: 'CallExpression',
                            callee: {
                                type: 'Identifier',
                                name: 'jQuery'
                            }
                        },
                        property: {
                            type: 'Identifier',
                            name: originalEvent
                        }
                    }
                }).forEach((path: any) => {
                    const selector = path.value.callee.object.arguments[0].value;
                    const functionExpression = path.value.arguments[0];

                    if (functionExpression && fieldNameRegex.test(selector)) {
                        const newCallExpression = jscodeshift.callExpression(
                            jscodeshift.memberExpression(
                                jscodeshift.identifier('ModeForm'),
                                jscodeshift.identifier('bindFieldAction')
                            ), [
                                jscodeshift.literal(newEvent),
                                jscodeshift.literal(selector),
                                functionExpression
                            ]
                        );
                        path.replace(newCallExpression);
                    }
                });
            });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeBindFieldActionTran-e:', e);
        }
        return code;
    }

    /**
     * 外部调用卡片保存方法
     * 注意：此方法主要用于页面扩展按钮
     *
     * 此方法支持外部调用,添加了两个控制参数，1、控制页面保存后是否跳转显示页面；2、支持保存后函数回调，返回billid
     *
     * ModeForm.doCardSubmit('14169','0','',false,function(billid){
     *         console.log("===billid===",billid)
     *     });
     *     //保存方法，保存后不跳转显示页面，打印出billid
     *
     *     转换后：
     *     ModeFormEcode.doCardSubmit('14169','0','',false,function(billid){
     *         console.log("===billid===",billid)
     *     });
     *
     *     ModeFormEcode.doCardSubmit在ecode中进行了封装。
     *
     * @param code
     */
    cubeDoCardSubmitTran = (code: string, cubeButton: string = '') => {
        try {
            let root = jscodeshift(code);

            //第三步： 处理ModeForm.getCurrentUserInfo()本身表达式的替换
            const doRightBtnEventComs = root.find(jscodeshift.CallExpression,
                {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {name: 'ModeForm'},
                        property: {name: 'doCardSubmit'}
                    },
                    arguments: []
                });
            const doRightBtnEventComsArray = doRightBtnEventComs.paths().map(path => path);
            const numberPattern = /^\d+$/;//判断是否为数字
            doRightBtnEventComsArray.map(async (declaration: any) => {
                let argumentsTemp = declaration.node.arguments;
                let expendId = argumentsTemp[0]?.value;
                argumentsTemp.shift();
                if (numberPattern.test(expendId)) {
                    expendId = bigInt(cubeButton).plus(bigInt(expendId)).toString();
                } else {
                    expendId = "页面扩展ID[" + expendId + "]需要转成E10EB按钮ID"
                }

                declaration.replace({
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {type: 'Identifier', name: 'ModeFormEcode'},
                        property: {type: 'Identifier', name: 'doCardSubmit'}
                    },
                    arguments: [{type: 'Literal', value: expendId}, ...argumentsTemp]
                })
            });
            // 等待所有的异步操作完成
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeDoCardSubmitTran-e:', e);
        }

        return code;
    }

    /**
     * 打开自定义对话框
     * 此方法用来打开一个自定义对话框；
     * //如果是内部iframe里定义的方法,直接写方法名,外部添加代码块方式添加的方法,需要在方法名前面加上base.
     *   let buttons=[{btnname:'保存',callfun:'saveDialog'},
     *               {btnname:'新建',callfun:'base.add'},
     *               {btnname:'关闭',callfun:'closeDialog'}];
     *   let style={width:300,height:600};
     *   let prop={title:'测试jsp',url:'/formmode/test.jsp',style:style};
     *   ModeForm.openCustomDialog(prop,buttons)
     *
     *   转换后：
     *   注意标准这边需要将  btnname 改成 content（注意这个我们没法修改，只能给出提示）：
     * let buttons=[{content:'保存',callfun:'saveDialog'},
     *               {content:'新建',callfun:'base.add'},
     *               {content:'关闭',callfun:'closeDialog'}];
     *   let style={width:300,height:600};
     *   let prop={title:'测试jsp',url:'/formmode/test.jsp',style:style};
     * window.ebdfpageSDK.openCustomDialog(prop,buttons)
     * @param code
     */
    cubeOpenCustomDialogTran = (code: string) => {
        try {
            let root = jscodeshift(code);

            //第三步： 处理ModeForm.getCurrentUserInfo()本身表达式的替换
            const openCustomDialogComs = root.find(jscodeshift.CallExpression,
                {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {name: 'ModeForm'},
                        property: {name: 'openCustomDialog'}
                    },
                    arguments: []
                });

            if (openCustomDialogComs.length > 0) {
                openCustomDialogComs.forEach((declaration: any) => {
                    declaration.node.callee.object.name = 'window.ebdfpageSDK';
                    const buttonsArg = declaration.node.arguments[1];

                    if (buttonsArg.type === 'ArrayExpression') {
                        buttonsArg.elements.forEach((prop: any) => {
                            if (prop.type === 'ObjectExpression') {
                                prop.properties.forEach((innerProp: any) => {
                                    if (innerProp.key.name === 'btnname') {
                                        innerProp.key.name = 'content';
                                    }
                                });
                            }
                        });
                    }

                    declaration.node.comments = [
                        jscodeshift.commentLine("注意第二参数的按钮的btnname属性需要改成content！")
                    ]
                })
            }

            // 等待所有的异步操作完成
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeOpenCustomDialogTran-e:', e);
        }

        return code;
    }

    /**
     * 关闭自定义对话框
     * 此方法用来关闭一个自定义对话框；
     *
     * //该方法在嵌入的外部页面中自行调用
     *   parent.ModeForm.closeCustomDialog();
     *
     * 转换后：
     * //该方法在嵌入的外部页面中自行调用
     *   parent.ModeForm.closeCustomDialog();
     *
     * @param code
     */
    cubeCloseCustomDialogTran = (code: string) => {
        try {

            code = code.replaceAll('ModeForm.closeCustomDialog', 'window.ebdfpageSDK.closeCustomDialog');
            let root = jscodeshift(code);

            //第三步： 处理ModeForm.getCurrentUserInfo()本身表达式的替换
            const openCustomDialogComs = root.find(jscodeshift.CallExpression,
                {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {name: 'ModeForm'},
                        property: {name: 'closeCustomDialog'}
                    },
                    arguments: []
                });

            if (openCustomDialogComs.length > 0) {
                openCustomDialogComs.forEach((declaration: any) => {
                    declaration.node.callee.object.name = 'window.ebdfpageSDK'
                })
            }

            // 等待所有的异步操作完成
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeCloseCustomDialogTran-e:', e);
        }

        return code;
    }

    /**
     * 侧滑打开页面
     *
     * ModeForm.slideOpenModal(
     *   true,
     *  '/spa/cube/index.html#/main/cube/card?billid=4&type=2&modeId=8888&formId=-1041&guid=card',
     *     70
     * )
     *
     * 转换后：
     * 如果：true
     * window.ebdfpageSDK.slideOpenModal({url: '/spa/cube/index.html#/main/cube/card?billid=4&type=2&modeId=8888&formId=-1041&guid=card'});
     *
     * 如果：false
     * window.ebdfpageSDK.closeCustomDialog();
     *
     * @param code
     */
    cubeSlideOpenModalTran = (code: string) => {
        try {

            code = code.replaceAll('ModeForm.closeCustomDialog', 'window.ebdfpageSDK.closeCustomDialog');
            let root = jscodeshift(code);

            //第三步： 处理ModeForm.getCurrentUserInfo()本身表达式的替换
            const openCustomDialogComs = root.find(jscodeshift.CallExpression,
                {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {name: 'ModeForm'},
                        property: {name: 'slideOpenModal'}
                    },
                    arguments: []
                });

            if (openCustomDialogComs.length > 0) {
                openCustomDialogComs.forEach((declaration: any) => {
                    let params1 = declaration.node.arguments[0]?.value || false;
                    let params2 = declaration.node.arguments[1]?.value || '';
                    if (!params1) {
                        declaration.replace({
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {
                                    type: 'MemberExpression',
                                    object: {type: 'Identifier', name: 'window'},
                                    property: {type: 'Identifier', name: 'ebdfpageSDK'}
                                },
                                property: {type: 'Identifier', name: 'closeCustomDialog'}
                            },
                            arguments: []
                        })
                    } else {
                        declaration.node.callee.object.name = 'window.ebdfpageSDK'
                        declaration.node.arguments = [jscodeshift.objectExpression([
                            jscodeshift.objectProperty(
                                jscodeshift.identifier('url'),
                                jscodeshift.stringLiteral(params2)
                            )
                        ])]
                    }

                })
            }

            // 等待所有的异步操作完成
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('cubeSlideOpenModalTran-e:', e);
        }

        return code;
    }

    /**
     * 改变字段属性
     * 处理系统字段部分！！！
     * ModeForm.changeFieldAttr("field110", 1);
     * ModeForm.changeFieldAttr("field-1", 1);
     *
     * 转换后：
     * 表单字段：
     * const weFormSdk = window.WeFormSDK.getWeFormInstance();
     * // 修改文本型-单行文本字段
     * const textFieldMark = weFormSdk.convertFieldNameToId('字段名');
     * weFormSdk.changeFieldAttr(textFieldMark,1);
     *
     * @param code
     */
    cubeChangeFieldAttrTran = (code: string) => {

        try {

            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "changeFieldAttr"}
                },
                arguments: []
            };

            //处理 WfForm.changeFieldAttr
            const changeFieldAttrCom = root
                .find(jscodeshift.CallExpression, sourceAst);

            const fieldRegex = /^\d+$/;
            if (changeFieldAttrCom.length > 0) {
                // 遍历匹配的对象并处理
                changeFieldAttrCom.forEach(path => {
                    const argumentsTmp = path.value.arguments;
                    if (argumentsTmp.length >= 2
                        && argumentsTmp[1] && argumentsTmp[1].type === 'Literal'
                        && typeof argumentsTmp[1].value === 'string'
                        && fieldRegex.test(argumentsTmp[1].value)) {
                        argumentsTmp[1].value = Number(argumentsTmp[1].value);
                    }
                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch
            (e) {
            if (window.console) console.log('wFcustomAddFunTran-e:', e);
        }

        return code;
    }

    /**
     * 同时修改字段的值及显示属性
     * ModeForm.changeSingleField("field110", {value:"修改的值"}, {viewAttr:"1"});   //修改值同时置为只读
     *
     * E10需要特殊处理，将viewAttr的属性改成 1
     * E10不支持显示属性是字符串
     *
     * @param code
     */
    cubeChangeSingleFieldTrans = (code: string)=>{

        try {
            let root = jscodeshift(code);

            const sourceAst:any = {
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'ModeForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'changeSingleField'
                    }
                }
            }

            //处理 ModeForm.changeSingleField
            const changeSingleFieldCom = root
                .find(jscodeshift.CallExpression, sourceAst);

            changeSingleFieldCom.forEach(path => {
                const args = path.value.arguments;
                if (
                    args.length === 3 &&
                    args[2].type === 'ObjectExpression' &&
                    'properties' in args[2]
                ) {
                    args[2].properties.forEach((prop: any) => {
                        if (
                            prop.type === 'Property' &&
                            prop.key.type === 'Identifier' &&
                            prop.key.name === 'viewAttr' &&
                            prop.value.type === 'Literal' &&
                            typeof prop.value.value === 'string' &&
                            !isNaN(Number(prop.value.value))
                        ) {
                            prop.value = jscodeshift.literal(parseInt(prop.value.value));
                        }
                    });
                }
            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeChangeSingleFieldTrans-e:', e);
        }
        return code;
    }
    
    /**
     * 1、同时修改字段的值及显示属性
     * ModeForm.delDetailRow("detail_1", "all");     //删除明细1所有行
     * ModeForm.delDetailRow("detail_1", "3,6");     //删除明细1行标为3,6的行
     *
     * 2、同时修改字段的值及显示属性
     * ModeForm.checkDetailRow("detail_2", "all"); //勾选明细2所有行
     * ModeForm.checkDetailRow("detail_2", "", true);//清除明细2所有已选
     * ModeForm.checkDetailRow("detail_2", "3,6", true); //清除明细2全部已选，再勾选行标为3,6的行
     * ModeForm.checkDetailRow("detail_2", "7", false);
     * //保持已选记录，追加选中行标为7的行
     *
     * 3、控制明细行check框是否禁用勾选
     * ModeForm.controlDetailRowDisableCheck("detail_1", "all", true); //明细1所有行check框置灰禁止选中
     * ModeForm.controlDetailRowDisableCheck("detail_1", "1,2", false);
     * //明细1行标为1,2的行释放置灰，允许勾选
     *
     * 4、控制明细数据行的显示及隐藏
     * ModeForm.controlDetailRowDisplay("detail_1", "3,5", true); //明细1行标为3,5的隐藏不显示
     * ModeForm.controlDetailRowDisplay("detail_1", "all", false);
     * //明细1所有行不隐藏都显示
     *
     * 以上方法都是去处理第二个参数。
     * 改成：ModeForm.getDetailRowIdByIndex(detailMark, 1);方式
     * @param code
     */
    cubeDelDetailRowTrans = (code: string) => {

        try {
            let root = jscodeshift(code);

            // 定义正则表达式来匹配函数名
            const detailNumRegex = /^detail_\d*$/;
            const numberRegex = /^\d+$/;

            const sourceAsts: any[] = [{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'ModeForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'delDetailRow'
                    }
                }
            },{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'ModeForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'checkDetailRow'
                    }
                }
            },{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'ModeForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'controlDetailRowDisableCheck'
                    }
                }
            },{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'ModeForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'controlDetailRowDisplay'
                    }
                }
            }]

            sourceAsts.forEach(sourceAst =>{

                //处理 WfForm.changeFieldAttr
                const delDetailRowCom = root
                    .find(jscodeshift.CallExpression, sourceAst);

                delDetailRowCom.forEach(path => {
                    const args = path.value.arguments;
                    if (args.length >= 2 &&
                        args[0].type === 'Literal' &&
                        typeof args[0].value == 'string' &&
                        detailNumRegex.test(args[0].value) &&
                        args[1].type === 'Literal' &&
                        typeof args[1].value == 'string'
                    ) {
                        let detailNumber = args[0].value;
                        let rownumber = args[1].value;
                        let rownumbers = rownumber.split(",");
                        let quasis = [];
                        let expressions:any[] = [];
                        let isFirst = true;
                        rownumbers.forEach(rowTemp => {
                            if(numberRegex.test(rowTemp)) {
                                quasis.push(jscodeshift.templateElement.from({
                                    value: {cooked: isFirst?'':',', raw: isFirst?'':','},
                                    tail: false
                                }));
                                isFirst = false;
                                expressions.push(jscodeshift.callExpression.from({
                                    arguments: [
                                        jscodeshift.literal(detailNumber),
                                        jscodeshift.literal(Number(rowTemp)+1)
                                    ],
                                    callee: jscodeshift.memberExpression(
                                        jscodeshift.identifier('ModeForm'),
                                        jscodeshift.identifier('getDetailRowIdByIndex')
                                    )
                                }));
                            }
                        })
                        quasis.push(jscodeshift.templateElement.from({
                            value: {cooked: '', raw: ''},
                            tail: true
                        }));

                        if(expressions.length>1)
                            args[1] = jscodeshift.templateLiteral(quasis, expressions);
                        else if (expressions.length==1)
                            args[1] = expressions[0]

                    }
                })

            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfDelDetailRowTrans-e:', e);
        }
        return code;
    }

    /**
     * 将ModeForm.转换成ModeFormEcode.
     * @param code
     */
    cubeModeFormEcodeTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            // 映射关系表
            const expressionMap:Record<any, any> = {
                // 'ModeForm.showModalMsg': 'ModeFormEcode.showModalMsg',
                'ModeForm.addDetailRow': 'ModeFormEcode.addDetailRow',
                'ModeForm.batchAddDetailRow': 'ModeFormEcode.batchAddDetailRow',
                'ModeForm.controlBtnDisabled': 'ebdFormSdk.controlBtnDisabled',
                // 添加更多的映射关系...
            };

            root.find(jscodeshift.CallExpression)
                .forEach((path:any) => {
                    const callee = path.node.callee;
                    if(callee.object && callee.property) {
                        const calleeName = `${callee.object.name}.${callee.property.name}`;

                        if (expressionMap[calleeName]) {
                            const [newObject, newProperty] = expressionMap[calleeName].split('.');
                            callee.object.name = newObject;
                            callee.property.name = newProperty;
                        }
                    }
                });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeModeFormEcodeTran-e:', e);
        }
        return code;
    }

    /**
     * 将无法替换的一些接口代码注释掉
     * 1、ModeForm.getFieldCurViewAttr("field110_2");   //获取明细字段属性，1：只读、2：可编辑、3：必填；已办全部为只读；--已支持
     * 2、ModeForm.controlDetailRowDisableCheck("detail_1", "all", true);     //明细1所有行check框置灰禁止选中--已支持
     * 3、ModeForm.controlBtnDisabled(true);    //操作按钮置灰--已支持
     * 4、ModeForm.getCardUrlInfo();     //获取卡片的url参数 此方法实时获取顶部url里面的参数及卡片内部的一些参数；
     * 5、ModeForm.forceRenderField("field28214");     //E10不支持
     *
     * @param code
     */
    cubeUndefinedTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAsts: jscodeshift.CallExpression[] = [{
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "getCardUrlInfo"}
                },
                arguments: []
            },{
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "ModeForm"},
                    property: {type: "Identifier", name: "forceRenderField"}
                },
                arguments: []
            }];

            sourceAsts.forEach(sourceAst => {
                const wfUndefinedComs = root
                    .find(jscodeshift.CallExpression, sourceAst);

                if (wfUndefinedComs.length > 0) {
                    wfUndefinedComs.forEach((nodePath) => {
                        // nodePath.replace(jscodeshift.commentBlock(` 无法替换部分：${jscodeshift(nodePath).toSource()} `))
                        nodePath.node.comments = [
                            jscodeshift.commentLine(`无法替换部分：${jscodeshift(nodePath).toSource()},需要手动调整！`)
                        ]
                    });
                }
            });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('cubeUndefinedTran-e:', e);
        }
        return code;
    }
}

const instance = new TransRuleForFormmode();

export default instance;