import jscodeshift from 'jscodeshift';
import * as API_LIST from "../../../api/TransCodeApi";

export class TransRuleForJQuery {

    transform = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            if(fromModule === 'workflow' || fromModule === 'cube' ){
                code = this.jQueryReadyTran(code);
                code = this.jQueryValTran(fromModule, code);
                code = this.jQueryIndexnumTran(fromModule, code, detailTables);
            }
            code = this.jQueryAjaxTran(code);
        } catch (e) {
            if (window.console) console.log('TransRuleForJQuery-transform-e', e);
        }
        return code;
    }

    /**
     * 处理 jQuery.ready(function(){})
     * jQuery.ready(function(){}) => window.ebuilderSDK.getPageSDK().on("formReady"，function(){})
     *
     * 这边实际尝试后发现需要改造成pageSdk.on('formReady', (args) => {console.log(args)});
     * 第一种的方法不能保证表单字段完全加载完
     *
     * jQuery(function () {})
     * pageSdk.on('formReady', (args) => {console.log(args)});
     * @param code
     */
    jQueryReadyTran = (code: string) => {
        try {
            const root = jscodeshift(code);

            //处理 jQuery.ready(function(){}) => window.ebuilderSDK.getPageSDK().on("formReady"，function(){})
            // const readyComs = root.find(jscodeshift.CallExpression, {
            //     callee: {property: {name: "ready"}, object: {callee: {name: "jQuery"}}}
            // });
            const readyComs = root.find(jscodeshift.CallExpression)
                .filter((nodePath: any) => {
                    return (
                        nodePath.node &&
                        nodePath.node['callee'] &&
                        nodePath.node['callee'].property &&
                        nodePath.node['callee'].object &&
                        nodePath.node['callee'].object['callee'] &&
                        nodePath.node['callee'].property.name === "ready" &&
                        nodePath.node['callee'].object['callee'].name === "jQuery"
                    );
                });

            if (readyComs.length > 0) {
                readyComs.forEach((nodePath: any) => {
                    const {node} = nodePath;
                    let arguments1 = node.arguments;
                    let param1 = jscodeshift.stringLiteral("formReady");
                    arguments1.unshift(param1);

                    let jQuerycalleeArguments = node['callee'].object.arguments;
                    delete jQuerycalleeArguments[0];

                    let jQuerycalleeProperty = node['callee'].property;
                    jQuerycalleeProperty.name = "on";
                    jQuerycalleeProperty.loc.identifierName = "on";

                    node['callee'].object = jscodeshift.identifier("pageSdk");
                });
            }

            //处理 jQuery(function () {｝)
            const readyComs1 = root.find(jscodeshift.CallExpression, {
                callee: {
                    type: 'Identifier',
                    name: 'jQuery'
                },
                arguments: [{
                    type: 'FunctionExpression'
                }]
            }).forEach(path => {
                const argument = path.value.arguments[0];
                if (argument.type === 'FunctionExpression') {
                    const body = argument.body;

                    // 替换函数体为新的函数体
                    if (body.type === 'BlockStatement') {
                        path.replace(
                            jscodeshift.callExpression(
                                jscodeshift.memberExpression(
                                    jscodeshift.identifier('pageSdk'),
                                    jscodeshift.identifier('on')
                                ),
                                [
                                    jscodeshift.literal('formReady'),
                                    jscodeshift.arrowFunctionExpression(
                                        [jscodeshift.identifier('args')],
                                        body
                                    )
                                ]
                            )
                        )
                    }
                }
            });


            return root.toSource();
        } catch (e) {
            if (window.console) console.log('jQueryReadyTran-e', e);
        }
        return code;
    }

    /**
     * 处理jQuery.ajax({}) || $.ajax({})
     * jQuery.ajax({})  =>  request({}).then(_json => {});
     * $.ajax({})  =>  request({}).then(_json => {});
     * @param code
     */
    jQueryAjaxTran = (code: string) => {
        try {
            // const importCode = "import { request } from '@weapp/utils';\r\n";
            const importCode = "";
            const root = jscodeshift(code);

            //处理jQuery.ajax({})  =>  request({}).then(_json => {});
            const jQueryAjaxComs = root.find(jscodeshift.CallExpression)
                .filter((nodePath: any) => {
                    return (
                        nodePath.node &&
                        nodePath.node['callee'] &&
                        nodePath.node['callee'].property &&
                        nodePath.node['callee'].object &&
                        nodePath.node['callee'].property.name === "ajax" &&
                        nodePath.node['callee'].object.name === "jQuery"
                    );
                });

            if (jQueryAjaxComs.length > 0) {
                jQueryAjaxComs.forEach((nodePath: any) => {
                    if (nodePath.node && nodePath.node.arguments[0] && nodePath.node.arguments[0].properties) {
                        let urlArgument: any[] = [];
                        let functionArgument;
                        let functionArgumentError;
                        if (nodePath.node['callee'].object) {
                            let ajaxType = 'post';
                            nodePath.node.arguments[0].properties.forEach((propertiesTemp: any) => {
                                if (propertiesTemp.key.name == "type") {
                                    ajaxType = propertiesTemp.value?.value?.toLowerCase();
                                }
                            });
                            nodePath.node.arguments[0].properties.forEach((propertiesTemp: any) => {
                                if (propertiesTemp.key) {
                                    if (propertiesTemp.key.name == "url" || propertiesTemp.key.name == "dataType") {
                                        urlArgument.push(propertiesTemp);
                                    }
                                    if (propertiesTemp.key.name == "data") {
                                        if (ajaxType === 'get') {
                                            propertiesTemp.key = jscodeshift.identifier("params");
                                            urlArgument.push(propertiesTemp);
                                        } else {
                                            urlArgument.push(propertiesTemp);
                                        }
                                    }
                                    if (propertiesTemp.key.name == "type") {
                                        propertiesTemp.key = jscodeshift.identifier("method");
                                        urlArgument.push(propertiesTemp);
                                    }
                                    if (propertiesTemp.key.name == "success") {
                                        if (propertiesTemp.value.params && propertiesTemp.value.body) {
                                            functionArgument = jscodeshift.arrowFunctionExpression(propertiesTemp.value.params, propertiesTemp.value.body);
                                        } else {
                                            functionArgument = jscodeshift.arrowFunctionExpression([jscodeshift.identifier("_data")], propertiesTemp.value);
                                        }
                                    }
                                    if (propertiesTemp.key.name == "error") {
                                        if (propertiesTemp.value.params && propertiesTemp.value.body) {
                                            functionArgumentError = jscodeshift.arrowFunctionExpression(propertiesTemp.value.params, propertiesTemp.value.body);
                                        } else {
                                            functionArgumentError = jscodeshift.arrowFunctionExpression([jscodeshift.identifier("_data")], propertiesTemp.value);
                                        }
                                    }
                                }
                            });
                        }

                        let requestComponent = jscodeshift.callExpression(jscodeshift.identifier("weappUtils.request"), [jscodeshift.objectExpression(urlArgument)]);
                        requestComponent.comments = [jscodeshift.commentLine("请求失败的情况下返回的参数和ajax不同个，需要调整！")];

                        if (functionArgument) {
                            let thenMemberComponent = jscodeshift.memberExpression(requestComponent, jscodeshift.identifier("then"), false);
                            let thenCallComponent = jscodeshift.callExpression(thenMemberComponent, [functionArgument]);
                            if (functionArgumentError) {
                                let catchMemberComponent = jscodeshift.memberExpression(thenCallComponent, jscodeshift.identifier("catch"), false);
                                let catchCallComponent = jscodeshift.callExpression(catchMemberComponent, [functionArgumentError]);
                                nodePath.replace(catchCallComponent);
                            } else {
                                nodePath.replace(thenCallComponent);
                            }
                        } else {
                            nodePath.replace(requestComponent);
                        }
                    }
                });

                return importCode + root.toSource();
            }

            return root.toSource();
        } catch (e) {
            if (window.console) console.log('jQueryAjaxTran-e', e);
        }
        return code;
    }

    /**
     * 处理jQuery("#field30666").val();           ——取值
     * 改造成：
     * WfForm.getFieldValue("field30666");
     * 处理jQuery("#field30666").val("1111");     ——赋值
     * 改造成：
     * WfForm.changeFieldValue("field123", {value:"1.234"});
     */
    jQueryValTran = (fromModule: string, code: string) => {
        try {
            const root = jscodeshift(code);

            const readyComs = root.find(jscodeshift.CallExpression)
                .filter((nodePath: any) => {
                    return (
                        nodePath.node && nodePath.node['callee']
                        && nodePath.node['callee'].property && nodePath.node['callee'].property.name === "val"
                        && nodePath.node['callee'].object && nodePath.node['callee'].object['callee']
                        && nodePath.node['callee'].object['callee'].name === "jQuery"
                        && nodePath.node['callee'].object.arguments
                        && nodePath.node['callee'].object.arguments[0]
                        && (
                            (nodePath.node['callee'].object.arguments[0].value
                                && nodePath.node['callee'].object.arguments[0].value.indexOf("#field") === 0)
                            ||
                            (nodePath.node['callee'].object.arguments[0].left
                                && ((nodePath.node['callee'].object.arguments[0].left.value
                                        && nodePath.node['callee'].object.arguments[0].left.value.indexOf("#field") === 0)
                                    ||
                                    (nodePath.node['callee'].object.arguments[0].left.left
                                        && nodePath.node['callee'].object.arguments[0].left.left.value
                                        && nodePath.node['callee'].object.arguments[0].left.left.value.indexOf("#field") === 0)))
                        )
                    );
                });

            if (readyComs.length > 0) {
                readyComs.forEach((nodePath: any) => {
                    const {node} = nodePath;
                    if (node['callee'].object.arguments[0]) {
                        if (node['callee'].object.arguments[0].value) {
                            node['callee'].object.arguments[0].value = node['callee'].object.arguments[0].value.replace("#", "");
                        }
                        if (node['callee'].object.arguments[0].left && node['callee'].object.arguments[0].left.value) {
                            node['callee'].object.arguments[0].left.value = node['callee'].object.arguments[0].left.value.replace("#", "");
                        }
                        if (node['callee'].object.arguments[0].left && node['callee'].object.arguments[0].left.left && node['callee'].object.arguments[0].left.left.value) {
                            node['callee'].object.arguments[0].left.left.value = node['callee'].object.arguments[0].left.left.value.replace("#", "");
                        }
                    }
                    if (node.arguments) {
                        if (node.arguments.length === 0) {
                            nodePath.replace(jscodeshift.callExpression(
                                jscodeshift.memberExpression(
                                    jscodeshift.identifier('cube' === fromModule ? 'ModeForm' : 'WfForm')
                                    , jscodeshift.identifier('getFieldValue')
                                    , false),
                                [node['callee'].object.arguments[0]]
                            ));
                        } else if (node.arguments.length === 1) {
                            nodePath.replace(jscodeshift.callExpression(
                                jscodeshift.memberExpression(
                                    jscodeshift.identifier('cube' === fromModule ? 'ModeForm' : 'WfForm')
                                    , jscodeshift.identifier('changeFieldValue')
                                    , false)
                                , [node['callee'].object.arguments[0]
                                    , jscodeshift.objectExpression(
                                        [jscodeshift.objectProperty(
                                            jscodeshift.identifier('value')
                                            , node.arguments[0])])
                                ]
                            ));
                        }
                    }
                });
            }

            return root.toSource();
        } catch (e) {
            if (window.console) console.log('jQueryValTran-e', e);
        }
        return code;
    }

    /**
     * 处理jQuery("#indexnum0").val();           ——获取明细行序号数据
     * 改造成：
     * //indexnum0需要替换成对应明细表的表名称例如：formtable_main_283_dt1
     * WfForm.getDetailAllRowIndexStr(WfForm.convertFieldNameToId("formtable_main_283_dt1"));
     */
    jQueryIndexnumTran = (fromModule: string, code: string, detailTables: any) => {
        try {
            const root = jscodeshift(code);

            // 定义正则表达式来匹配函数名
            const functionNameRegex = /^#indexnum\d*$/;
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {
                        type: "CallExpression",
                        callee: {
                            type: "Identifier",
                            name: "jQuery"
                        },
                        arguments: []
                    },
                    property: {type: "Identifier", name: "val"}
                },
                arguments: []
            }

            const indexnumComs = root.find(jscodeshift.CallExpression, sourceAst)
                .filter((nodePath: any) => {
                    return functionNameRegex.test(nodePath?.node?.callee?.object?.arguments?.[0]?.value);
                });

            if (indexnumComs.length > 0) {

                indexnumComs.forEach((nodePath: any) => {
                    var detailNum = nodePath?.node?.['callee']?.object?.arguments?.[0]?.value;//#indexnum0
                    detailNum = detailNum.replace("#indexnum", "");
                    if (detailTables[parseInt(detailNum)+1]) {
                        detailNum = detailTables[parseInt(detailNum)+1];
                    } else {
                        detailNum = detailNum + "(替换成明细表id)";
                    }
                    const deatailAllRowIndexStr = jscodeshift(('cube' === fromModule ? 'ModeForm' : 'WfForm') + ".getDetailAllRowIndexStr(\"" +
                        detailNum + "\").split(\",\")").paths()[0].value.program.body[0].expression;
                    nodePath.replace(deatailAllRowIndexStr);
                    nodePath.node.comments = [
                        jscodeshift.commentLine('以var indexnum0 = jQuery(\"#indexnum0\").val();为例，'),
                        jscodeshift.commentLine('引用的地方需要改造成 indexnum0.length; 改造后如下：for (var i = 0; i <= indexnum0.length; i++)'),
                        jscodeshift.commentLine('循环内部调用 i 的地方需要改成 indexnum0[i] 改造后如下：' + ('cube' === fromModule ? 'ModeForm' : 'WfForm') + '.getDetailRowKey(\"field****_\" + indexnum0[i])')]
                });
            }

            return root.toSource();
        } catch (e) {
            if (window.console) console.log('jQueryIndexnumTran-e', e);
        }
        return code;
    }
}

const instance = new TransRuleForJQuery();

export default instance;