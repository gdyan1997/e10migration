import jscodeshift from 'jscodeshift';
import * as babel from 'babel-core';
import stage0 from "babel-preset-stage-0";
import flowStripTypes from "babel-plugin-transform-flow-strip-types";


export class TransRulePre {

    transform = (fromModule: string, code: string) => {

        try {
            code = this.codeFormat(code);
            if(fromModule === 'workflow' || fromModule === 'cube' ){
                code = this.checkJsFrom(fromModule, code);
                code = this.WfFormFormat(code);
            }
            code = this.jQueryFormat(code);
            code = this.windowFormat(code);//本身想去批量处理掉带window的数据，但是会导致很多方法调用不到，屏蔽掉

        } catch (e) {
            if (window.console) console.log('TransRulePre-transform-e', e);
        }

        return code;
    }

    /**
     * 验证代码时候是正常的格式
     * @param code
     */
    checkNormalCode = (code: string) => {
        try {
            const options = {
                presets: [stage0],
                plugins: [flowStripTypes],
                ast: false,
                babelrc: false,
                highlightCode: false,
            };
            let es5Code = babel.transform(code, options).code;

        } catch (e) {
            if (window.console) console.log('e', e);
            return false;
        }
        return true;
    }

    /**
     * 代码格式化
     * @param code
     */
    codeFormat = (code: string) => {
        try {
            const options = {
                presets: [stage0],
                plugins: [flowStripTypes],
                ast: false,
                babelrc: false,
                highlightCode: false,
            };
            let es5Code = babel.transform(code, options).code;
            code = es5Code ? es5Code : code;

        } catch (e) {
            if (window.console) console.log('e', e);
        }
        return code;
    }

    /**
     * 1、判断是否存在WfForm，用于定义默认的const WfForm = window.WeFormSDK.getWeFormInstance();
     * 2、判断是否存在ModeForm，用于定义默认的const ModeForm = window.WeFormSDK.getWeFormInstance();
     * @param code
     */
    checkJsFrom = (fromModule: string, code: string) => {
        try {
            let headAppend = "";
            const root = jscodeshift(code);

            //处理 WfForm
            const WfFormComs = root
                .find(jscodeshift.CallExpression)
                .filter((path: any) => {
                    return path.node['callee'].object && path.node['callee'].object.name == "WfForm";
                });

            if (WfFormComs.length > 0 || "workflow" === fromModule) {
                headAppend = headAppend +
                    "const WfForm = window.WeFormSDK.getWeFormInstance();//表单引擎SDK\r\n" +
                    "const wffpSdk = window.weappWorkflow.getFlowPageSDK();//流程详情页面实例\r\n";
            }
            //处理 ModeForm

            const ModeFormComs = root
                .find(jscodeshift.CallExpression)
                .filter((path: any) => {
                    return path.node['callee'].object && path.node['callee'].object.name == "ModeForm";
                });
            if (ModeFormComs.length > 0 || "cube" === fromModule) {

                headAppend = headAppend +
                    "const ModeForm = window.WeFormSDK.getWeFormInstance();//表单引擎SDK\r\n"+
                    "const ebdFormSdk = window.ebdfpageSDK.getCardSDK();//EB表单视图SDK\r\n";
            }

            return headAppend + code;
        } catch (e) {
            if (window.console) console.log('e', e);
        }
        return code;
    }

    /**
     * 预处理：
     * 1、将所有的wfform 都替换为 WfForm
     * 2、将所有的$ 都替换为jQuery
     */
    WfFormFormat = (code: string) => {
        try {
            const root = jscodeshift(code);
            const sourceAst:jscodeshift.Identifier = {type: "Identifier", name: "wfform"};
            const WfFormComs = root
                .find(jscodeshift.Identifier, sourceAst);

            WfFormComs.forEach(path => {
                path.replace(jscodeshift.identifier("WfForm"));
            });
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        }catch (e){
            if (window.console) console.log('e', e);
        }
        return code;
    }

    jQueryFormat = (code: string) => {
        try {
            const root = jscodeshift(code);
            const sourceAst:jscodeshift.Identifier = {type: "Identifier", name: "$"};
            const WfFormComs = root
                .find(jscodeshift.Identifier, sourceAst);

            WfFormComs.forEach(path => {
                path.replace(jscodeshift.identifier("jQuery"));
            });
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        }catch (e){
            if (window.console) console.log('e', e);
        }
        return code;
    }

    windowFormat = (code: string) => {
        try {
            const root = jscodeshift(code);

            // 定义一个映射关系，将 A 替换为 A1
            const expressionMap: any = {
                'CallExpression': {
                    // 添加更多的映射关系...
                },
                'MemberExpression': {
                    'window.WfForm': 'WfForm',
                    'window.ModeForm': 'ModeForm',
                    // 添加更多的映射关系...
                }
            };

            for (let expressionMapKey in expressionMap) {
                // 遍历 AST，查找并替换表达式
                root.find(jscodeshift[expressionMapKey]).forEach(path => {
                    // 没办法了不知能先让typescript忽略错误，不然没法进行了开发了
                    // @ts-ignore
                    const expression = expressionMap[expressionMapKey][jscodeshift(path.node).toSource()];
                    if (expression) {
                        path.replace(jscodeshift.identifier(expression)); // 替换表达式
                    }
                });
            }


            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        }catch (e){
            if (window.console) console.log('windowFormat-e', e);
        }
        return code;
    }
}

const instance = new TransRulePre();

export default instance;