import jscodeshift from 'jscodeshift';
import * as API_LIST from "../../../api/TransCodeApi";

export class TransRuleForEccom {

    transform = (fromModule: string, code: string) => {

        try {
            code = this.ecComWeaToolsCallApiTran(code);
        } catch (e) {
            if (window.console) console.log('TransRuleForAntd-transform-e', e);
        }
        return code;
    }

    /**
     * ecCom.WeaTools.callApi('', 'GET', params).then((resp) => {});
     *
     * 改造成：
     *
     * weappUtils.request({
     *   method: 'GET',
     *   url: '第一个地址参数',
     *   data: params}).then((resp) => {})
     */
    ecComWeaToolsCallApiTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {
                        type: "MemberExpression",
                        object: {type: "Identifier", name: "ecCom"},
                        property: {type: "Identifier", name: "WeaTools"}
                    },
                    property: {type: "Identifier", name: "callApi"}
                },
                arguments: []
            };

            const antdModalWarningComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (antdModalWarningComs.length > 0) {
                antdModalWarningComs.forEach((nodePath: any) => {
                    if(nodePath.node.arguments.length >= 2) {
                        let urlTemp = nodePath.node.arguments[0];
                        let methodTemp = nodePath.node.arguments[1]?.value.toUpperCase() || 'GET';
                        let dataTemp = nodePath.node.arguments[2];
                        nodePath.replace({
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "weappUtils"},
                                property: {type: "Identifier", name: "request"}
                            },
                            arguments: [jscodeshift.objectExpression([
                                jscodeshift.objectProperty(
                                    jscodeshift.identifier('method'),
                                    jscodeshift.stringLiteral(methodTemp)
                                ),
                                jscodeshift.objectProperty(
                                    jscodeshift.identifier('url'),
                                    urlTemp
                                ),
                                jscodeshift.objectProperty(
                                    jscodeshift.identifier(methodTemp === 'GET' ? 'params':'data'),
                                    dataTemp
                                )
                            ])]
                        })
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch
            (e) {
            if (window.console) console.log('ecComWeaToolsCallApiTran-e:', e);
        }
        return code;
    }
}

const
    instance = new TransRuleForEccom();

export default instance;