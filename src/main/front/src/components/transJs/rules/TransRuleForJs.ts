import jscodeshift from 'jscodeshift';
import * as API_LIST from "../../../api/TransCodeApi";

export class TransRuleForJs {

    transform = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            if (fromModule === 'workflow' || fromModule === 'cube') {
                code = this.jsValTran(fromModule, code);
                code = this.jsIndexnumTran(fromModule, code, detailTables);
            }

        } catch (e) {
            if (window.console) console.log('TransRuleForJs-transform-e', e);
        }
        return code;
    }

    /**
     处理var sum = document.getElementById('field8231').value;              ——取值
     * 改造成：
     * WfForm.getFieldValue("field30666");
     * 处理document.getElementById('field8231').value = '11111';              ——赋值
     * 改造成：
     * WfForm.changeFieldValue("field123", {value:"1.234"});
     */
    jsValTran = (fromModule: string, code: string) => {
        try {
            const root = jscodeshift(code);

            const setValueComs = root.find(jscodeshift.AssignmentExpression)
                .filter((nodePath: any) => {
                    return (
                        nodePath.node && nodePath.node.operator === '=' && nodePath.node.left
                        && nodePath.node.left.object
                        && nodePath.node.left.object['callee']
                        && nodePath.node.left.object['callee'].object && nodePath.node.left.object['callee'].object.name === 'document'
                        && nodePath.node.left.object['callee'].property && nodePath.node.left.object['callee'].property.name === 'getElementById'
                        && nodePath.node.left.property && nodePath.node.left.property.name === 'value'
                        && nodePath.node.left.object.arguments
                        && nodePath.node.left.object.arguments.length > 0
                        && nodePath.node.left.object.arguments[0]
                        && (
                            (nodePath.node.left.object.arguments[0].value
                                && nodePath.node.left.object.arguments[0].value.indexOf("field") === 0)
                            ||
                            (nodePath.node.left.object.arguments[0].left
                                && ((nodePath.node.left.object.arguments[0].left.value
                                        && nodePath.node.left.object.arguments[0].left.value.indexOf("field") === 0)
                                    ||
                                    (nodePath.node.left.object.arguments[0].left.left
                                        && nodePath.node.left.object.arguments[0].left.left.value
                                        && nodePath.node.left.object.arguments[0].left.left.value.indexOf("field") === 0)))
                        )
                    );
                });

            if (setValueComs.length > 0) {
                setValueComs.forEach((nodePath: any) => {
                    const {node} = nodePath;
                    if (node.right) {
                        nodePath.replace(jscodeshift.expressionStatement(
                            jscodeshift.callExpression(
                                jscodeshift.memberExpression(
                                    jscodeshift.identifier('cube' === fromModule ? 'ModeForm' : 'WfForm')
                                    , jscodeshift.identifier('changeFieldValue')
                                    , false)
                                , [nodePath.node.left.object.arguments[0]
                                    , jscodeshift.objectExpression(
                                        [jscodeshift.objectProperty(
                                            jscodeshift.identifier('value')
                                            , node.right)])
                                ]
                            )));
                    }
                });
            }

            const getValueComs = root.find(jscodeshift.MemberExpression)
                .filter((nodePath: any) => {
                    return (
                        nodePath.node && nodePath.node.object && nodePath.node.object['callee']
                        && nodePath.node.object['callee'].property && nodePath.node.object['callee'].property.name === 'getElementById'
                        && nodePath.node.object['callee'].object && nodePath.node.object['callee'].object.name === 'document'
                        && nodePath.node.property && nodePath.node.property.name === 'value'
                        && nodePath.node.object.arguments && nodePath.node.object.arguments.length > 0
                        && nodePath.node.object.arguments[0]
                        && (
                            (nodePath.node.object.arguments[0].value
                                && nodePath.node.object.arguments[0].value.indexOf("field") === 0)
                            ||
                            (nodePath.node.object.arguments[0].left
                                && ((nodePath.node.object.arguments[0].left.value
                                        && nodePath.node.object.arguments[0].left.value.indexOf("field") === 0)
                                    ||
                                    (nodePath.node.object.arguments[0].left.left
                                        && nodePath.node.object.arguments[0].left.left.value
                                        && nodePath.node.object.arguments[0].left.left.value.indexOf("field") === 0)))
                        )
                    );
                });

            if (getValueComs.length > 0) {
                getValueComs.forEach((nodePath: any) => {
                    const {node} = nodePath;
                    nodePath.replace(jscodeshift.callExpression(
                        jscodeshift.memberExpression(
                            jscodeshift.identifier('cube' === fromModule ? 'ModeForm' : 'WfForm')
                            , jscodeshift.identifier('getFieldValue')
                            , false),
                        [node.object.arguments[0]]
                    ));
                });
            }

            return root.toSource();
        } catch (e) {
            if (window.console) console.log("jsValTran-e:", e);
        }
        return code;
    }

    /**
     * document.getElementById("indexnum0").value;——获取明细行序号数据
     * 改造成：
     * //indexnum0需要替换成对应明细表的表名称例如：formtable_main_283_dt1
     * WfForm.getDetailAllRowIndexStr(WfForm.convertFieldNameToId("indexnum0"));
     */
    jsIndexnumTran = (fromModule: string, code: string, detailTables: any) => {
        try {
            const root = jscodeshift(code);

            const sourceAst: jscodeshift.MemberExpression = {
                type: 'MemberExpression',
                object: {
                    type: 'CallExpression',
                    callee: {
                        type: 'MemberExpression',
                        object: {type: 'Identifier', name: 'document'},
                        property: {type: 'Identifier', name: 'getElementById'}
                    },
                    arguments: []
                },
                property: {type: 'Identifier', name: 'value'}
            };

            // 定义正则表达式来匹配函数名
            const functionNameRegex = /^indexnum\d*$/;

            const indexnumComs = root.find(jscodeshift.MemberExpression, sourceAst)
                .filter((nodePath: any) => {
                    return nodePath.node?.object?.arguments?.length > 0 && functionNameRegex.test(nodePath.node?.object?.arguments[0]?.value);
                });

            if (indexnumComs.length > 0) {

                indexnumComs.forEach((nodePath: any) => {
                    var detailNum = nodePath.node?.object?.arguments[0]?.value//#indexnum0
                    detailNum = detailNum.replace("indexnum", "");
                    if (detailTables[parseInt(detailNum) + 1]) {
                        detailNum = detailTables[parseInt(detailNum) + 1];
                    } else {
                        detailNum = detailNum + "(替换成明细表名)";
                    }
                    const deatailAllRowIndexStr = jscodeshift(('cube' === fromModule ? 'ModeForm' : 'WfForm') + ".getDetailAllRowIndexStr(\"" +
                        detailNum + "\").split(\",\")").paths()[0].value.program.body[0].expression;
                    nodePath.replace(deatailAllRowIndexStr);
                    nodePath.node.comments = [
                        jscodeshift.commentLine('以var indexnum0 = jQuery(\"#indexnum0\").val();为例，'),
                        jscodeshift.commentLine('引用的地方需要改造成 indexnum0.length; 改造后如下：for (var i = 0; i <= indexnum0.length; i++)'),
                        jscodeshift.commentLine('循环内部调用 i 的地方需要改成 indexnum0[i] 改造后如下：' + ('cube' === fromModule ? 'ModeForm' : 'WfForm') + '.getDetailRowKey(\"field****_\" + indexnum0[i])')]
                });
            }

            return root.toSource();
        } catch (e) {
            if (window.console) console.log("jsIndexnumTran-e:", e);
        }
        return code;
    }
}

const instance = new TransRuleForJs();

export default instance;