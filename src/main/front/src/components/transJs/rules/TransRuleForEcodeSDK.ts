import jscodeshift from 'jscodeshift';

export class TransRuleForEcodeSDK {

    transform = (code: string) => {

        try {
            code = this.ecodeSDKLoadJsTran(code);
        } catch (e) {
            if (window.console) console.log('TransRuleForEcodeSDK-transform-e', e);
        }
        return code;
    }

    /**
     * ecodeSDK.loadjs("/profiler/reportjs/xlsx.full.min.js");
     *
     * 转换成：
     * window.loadjs("/profiler/reportjs/xlsx.full.min.js");
     *
     * @param code
     */
    ecodeSDKLoadJsTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object:  {type: "Identifier", name: "ecodeSDK"},
                    property: {type: "Identifier", name: "loadjs"}
                },
                arguments: []
            };

            const ecodeSDKLoadJsComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (ecodeSDKLoadJsComs.length > 0) {
                ecodeSDKLoadJsComs.forEach((nodePath: any) => {
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {type: "Identifier", name: "window"},
                            property: {type: "Identifier", name: "loadjs"}
                        },
                        arguments: nodePath.node.arguments
                    })
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch
            (e) {
            if (window.console) console.log('ecodeSDKLoadJsTran-e:', e);
        }
        return code;
    }
}

const
    instance = new TransRuleForEcodeSDK();

export default instance;