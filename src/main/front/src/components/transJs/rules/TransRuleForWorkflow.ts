import jscodeshift from 'jscodeshift';

export class TransRuleForWorkflow {

    transform = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        if (fromModule != 'workflow') return code;
        try {
            code = this.wFbindFieldChangeEventTran(code);
            code = this.wFBindDetailFieldChangeEventTran(code);
            code = this.wFRegisterCheckEventTran('workflow', code, fieldMap, detailTables);
            code = this.wFRegisterActionTran('workflow', code, fieldMap, detailTables);
            // code = this.wfConvertFieldNameToIdTran(code);
            code = this.wfBindFieldActionTran(code);
            code = this.wfGetDetailRowKeyTran('workflow', code, fieldMap, detailTables);
            code = this.wfGetDetailRowSerailNumTran('workflow', code, fieldMap, detailTables);
            code = this.wfGetBaseInfoTran(code);
            code = this.wfShowMessageTran(code);
            code = this.wfDoRightBtnEventTran(code);
            code = this.wfShowHoverWindowTran(code);
            code = this.wfGetSignRemarkTran(code);
            code = this.wfSetSignRemarkTran(code);
            code = this.wfGetFieldCurViewAttrTran(code);
            code = this.wfGetFieldValueTran(code);//处理系统表单获取值
            code = this.wfGetFieldValueObjTran(code);
            code = this.wfBindPropertyChangeTran(code);
            code = this.wFCustomAddOrDelFunTran('workflow', code, fieldMap, detailTables);
            code = this.wFChangeFieldValueTran(code);
            code = this.wFBrowserObjectTran(code);
            // code = this.wfWwriteBackDataTran(code);——通过Ecode去实现
            code = this.wfSpecialExpTran(code);
            code = this.wfSetTextFieldEmptyShowContentTran('workflow', code, fieldMap, detailTables);
            code = this.wfControlBtnDisabledTran(code);
            code = this.wFChangeFieldAttrTran(code);
            code = this.wfChangeSingleFieldTrans(code);

            //明细表行号转成rowid
            code = this.wfDelDetailRowTrans(code);

            code = this.wfUndefinedTran(code);
        } catch (e) {
            if (window.console) console.log("TransRuleForWorkflow.transform", e);
        }

        return code;
    }

    /**
     * 将E9流程表单 WfForm.bindFieldChangeEvent（表单字段值变化触发事件） 转换成E10的写法
     * bindFieldChangeEvent: function(fieldMarkStr,funobj)
     *
     * WfForm.bindFieldChangeEvent("field-1", function(obj,id,value){
     *   WfForm.changeFieldValue("field91516", {value:value});	//修改"分享标题"字段值
     * });
     *
     * 系统字段：
     * wffpSdk.registerHookEvent('ChangeSysFieldEvent', (params)=>{
     *    const { fieldType,data } = params;
     *   if(fieldType === 'REQ_REQUEST_NAME'){
     *   }
     * })
     * 名称	含义
     * REQ_REQUEST_NAME	流程标题 field-1
     * REQUEST_NO	流程编号
     * REQUEST_LEVEL	紧急程度 field-2
     * REQUEST_SECRET_LEVEL	密级 field-10
     * EXPIRY_TIME	到期时间
     *
     * 表单字段：
     * // 获取表单实例
     *     const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *     // 获取主表字段fieldId
     *     const textFieldMark = weFormSdk.convertFieldNameToId("wbk");
     *     const selectFieldMark = weFormSdk.convertFieldNameToId("xlk");
     *     // 绑定事件，对主表字段和明细表的某一行绑定
     *     weFormSdk.bindFieldChangeEvent(`${textFieldMark},${selectFieldMark}`, (data) => {
     *         // 取字段标识
     *         const fieldMark = data.id;
     *         // 取字段修改的值
     *         const value = data.value;
     *         console.log(data);
     *     });
     * @param code
     */
    wFbindFieldChangeEventTran = (code: string) => {
        try {
            const root = jscodeshift(code);
            // const targetAst = {
            //     type: 'CallExpression',
            //     callee: {
            //         type: 'MemberExpression',
            //         object: {type: 'Identifier', name: 'WfForm'},
            //         property: {type: 'Identifier', name: 'bindFieldChangeEvent'}
            //     },
            //     arguments: [{type: 'Literal'}, {type: 'FunctionExpression'}]
            // };
            const fieldMap: Record<string, string> = {
                "field-1": "REQ_REQUEST_NAME",
                "field-2": "REQUEST_LEVEL",
                "field-10": "REQUEST_SECRET_LEVEL"
            };

            //处理系统字段
            const sourceAstForSys = {
                "type": "CallExpression",
                "callee": {
                    "type": "MemberExpression",
                    "object": {
                        "type": "Identifier",
                        "name": "WfForm"
                    },
                    "property": {
                        "type": "Identifier",
                        "name": "bindFieldChangeEvent"
                    }
                },
                "arguments": [
                    {},
                    {
                        "type": "FunctionExpression"
                    }
                ]
            };
            const bindFieldChangeEventForSysCom = root
                .find(jscodeshift.Node, sourceAstForSys);

            bindFieldChangeEventForSysCom.forEach((nodePath: any) => {

                // let excuteText = "let $replace1$ = nodePath['node']['arguments'][0]\n" +
                //     "                    let $replace2$ = nodePath['node']['arguments'][1]?.['params']?.[0]?.['name']\n" +
                //     "                    let $replace3$ = nodePath['node']['arguments'][1]?.['params']?.[1]?.['name']\n" +
                //     "                    let $replace4$ = nodePath['node']['arguments'][1]?.['params']?.[2]?.['name']\n" +
                //     "                    let $replace5$ = nodePath['node']['arguments'][1]?.['body']?.['body'] || []\n" +
                //     "\n" +
                //     "\n" +
                //     "                    const replacementAst = {\n" +
                //     "                        type: 'CallExpression',\n" +
                //     "                        callee: {\n" +
                //     "                            type: 'MemberExpression',\n" +
                //     "                            object: { type: 'Identifier', name: 'WfForm' },\n" +
                //     "                            property: { type: 'Identifier', name: 'bindFieldChangeEvent' },\n" +
                //     "                        },\n" +
                //     "                        arguments: [\n" +
                //     "                            $replace1$ ,\n" +
                //     "                            { type: 'FunctionExpression',\n" +
                //     "                                params: [{type:'Identifier',name:'weaFormSdkData'}],\n" +
                //     "                                body: { type: 'BlockStatement',\n" +
                //     "                                    body: [\n" +
                //     "                                        {type:'VariableDeclaration',kind:'var',declarations:[{type:'VariableDeclarator',id:{type:'Identifier',name:$replace2$?$replace2$:'e9_obj'},init:{type:'Identifier',name:'weaFormSdkData'}}]},\n" +
                //     "                                        {type:'VariableDeclaration',kind:'var',declarations:[{type:'VariableDeclarator',id:{type:'Identifier',name:$replace3$?$replace3$:'e9_id'},init:{type:'MemberExpression',object:{type:'Identifier',name:'weaFormSdkData'},property:{type:'Identifier',name:'id'}}}]},\n" +
                //     "                                        {type:'VariableDeclaration',kind:'var',declarations:[{type:'VariableDeclarator',id:{type:'Identifier',name:$replace4$?$replace4$:'e9_value'},init:{type:'MemberExpression',object:{type:'Identifier',name:'weaFormSdkData'},property:{type:'Identifier',name:'value'}}}]},\n" +
                //     "                                        ...$replace5$\n" +
                //     "                                    ] }\n" +
                //     "                            },\n" +
                //     "                        ],\n" +
                //     "                    };\n" +
                //     "                    \n" +
                //     "                    nodePath.replace(replacementAst);";
                // eval(excuteText);

                //直接去替换模版的方式

                if (nodePath['node']['arguments'] && nodePath['node']['arguments'].length == 2) {

                    let $replace1$ = nodePath['node']['arguments'][0]

                    let $replace2$ = nodePath['node']['arguments'][1]['params']?.[0]?.['name'] || 'obj_upgrade'
                    let $replace3$ = nodePath['node']['arguments'][1]['params']?.[1]?.['name'] || 'id_upgrade'
                    let $replace4$ = nodePath['node']['arguments'][1]['params']?.[2]?.['name'] || 'value_upgrade'
                    let $replace5$ = nodePath['node']['arguments'][1]['body']['body']


                    let replacementAst = {
                        type: 'CallExpression',
                        callee: {
                            type: 'MemberExpression',
                            object: {type: 'Identifier', name: 'WfForm'},
                            property: {type: 'Identifier', name: 'bindFieldChangeEvent'},
                        },
                        arguments: [
                            $replace1$,
                            {
                                type: 'FunctionExpression',
                                params: [{type: 'Identifier', name: 'weaFormSdkData'}],
                                body: {
                                    type: 'BlockStatement',
                                    body: [
                                        {
                                            type: 'VariableDeclaration',
                                            kind: 'var',
                                            declarations: [{
                                                type: 'VariableDeclarator',
                                                id: {type: 'Identifier', name: $replace2$},
                                                init: {type: 'Identifier', name: 'weaFormSdkData'}
                                            }]
                                        },
                                        {
                                            type: 'VariableDeclaration',
                                            kind: 'var',
                                            declarations: [{
                                                type: 'VariableDeclarator',
                                                id: {type: 'Identifier', name: $replace3$},
                                                init: {
                                                    type: 'MemberExpression',
                                                    object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                    property: {type: 'Identifier', name: 'id'}
                                                }
                                            }]
                                        },
                                        {
                                            type: 'VariableDeclaration',
                                            kind: 'var',
                                            declarations: [{
                                                type: 'VariableDeclarator',
                                                id: {type: 'Identifier', name: $replace4$},
                                                init: {
                                                    type: 'MemberExpression',
                                                    object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                    property: {type: 'Identifier', name: 'value'}
                                                }
                                            }]
                                        },
                                        ...$replace5$
                                    ]
                                }
                            },
                        ],
                    };

                    if ($replace1$.type === 'Literal') {
                        var fieldNames = $replace1$.value;
                        if (fieldMap[fieldNames]) {//REQ_REQUEST_NAME	流程标题 field-1
                            replacementAst = {
                                type: 'CallExpression',
                                callee: {
                                    type: 'MemberExpression',
                                    object: {type: 'Identifier', name: 'wffpSdk'},
                                    property: {type: 'Identifier', name: 'registerHookEvent'},
                                },
                                arguments: [
                                    {
                                        type: "Literal",
                                        value: "ChangeSysFieldEvent"
                                    },
                                    {
                                        type: 'FunctionExpression',
                                        params: [{type: 'Identifier', name: 'weaFormSdkData'}],
                                        body: {
                                            type: 'BlockStatement',
                                            body: [
                                                {
                                                    type: 'VariableDeclaration',
                                                    kind: 'var',
                                                    declarations: [{
                                                        type: 'VariableDeclarator',
                                                        id: {type: 'Identifier', name: $replace2$}
                                                    }]
                                                },
                                                {
                                                    type: 'VariableDeclaration',
                                                    kind: 'var',
                                                    declarations: [{
                                                        type: 'VariableDeclarator',
                                                        id: {type: 'Identifier', name: $replace3$},
                                                        init: {
                                                            type: 'MemberExpression',
                                                            object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                            property: {type: 'Identifier', name: 'fieldType'}
                                                        }
                                                    }]
                                                },
                                                {
                                                    type: 'VariableDeclaration',
                                                    kind: 'var',
                                                    declarations: [{
                                                        type: 'VariableDeclarator',
                                                        id: {type: 'Identifier', name: $replace4$},
                                                        init: {
                                                            type: 'MemberExpression',
                                                            object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                            property: {type: 'Identifier', name: 'data'}
                                                        }
                                                    }]
                                                },
                                                {
                                                    type: "IfStatement",
                                                    test: {
                                                        type: "BinaryExpression",
                                                        operator: "===",
                                                        left: {
                                                            type: "Identifier",
                                                            name: $replace3$
                                                        },
                                                        right: {
                                                            type: "Literal",
                                                            value: fieldMap[fieldNames],
                                                            raw: `'${fieldMap[fieldNames]}'`
                                                        }
                                                    },
                                                    consequent: {
                                                        type: "BlockStatement",
                                                        body: [
                                                            ...$replace5$
                                                        ]
                                                    },
                                                    "alternate": null
                                                },

                                            ]
                                        }
                                    },
                                ],
                            };

                        }
                    }

                    nodePath.replace(replacementAst);

                }

                // 最原始的直接去替换节点中属性的方法
                // if (nodePath.node.arguments && nodePath.node.arguments.length == 2) {
                //     const argumentsTemp = nodePath.node.arguments[1];
                //     const argumentsFunctionTemp = argumentsTemp.body.body;
                //     const params = argumentsTemp.params;
                //     var weaFormSdkData = "";
                //     for (let i = 0; i < params.length; i++) {
                //         if (i == 0) weaFormSdkData = weaFormSdkData + "// 数据对象 \r\n var " + params[i].name + " = weaFormSdkData;\r\n";
                //         if (i == 1) weaFormSdkData = weaFormSdkData + "// 取字段标识 \r\n var " + params[i].name + " = weaFormSdkData.id;\r\n";
                //         if (i == 2) weaFormSdkData = weaFormSdkData + "// 取字段修改的值 \r\n var " + params[i].name + " = weaFormSdkData.value;\r\n";
                //     }
                //     const defaultVariables = jscodeshift(weaFormSdkData).paths()[0].value.program.body;
                //     for (let i1 = defaultVariables.length - 1; i1 >= 0; i1--) {
                //         argumentsFunctionTemp.unshift(defaultVariables[i1]);
                //     }
                //
                //     params.splice(0, params.length);
                //     params.push(jscodeshift.identifier("weaFormSdkData"));
                // }
            });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('wFbindFieldChangeEventTran-e:', e);
        }
        return code;
    }

    /**
     * 将E9流程表单 WfForm.bindDetailFieldChangeEvent（明细字段值变化触发事件） 转换成E10的写法
     * @param code
     */
    wFBindDetailFieldChangeEventTran = (code: string) => {

        try {
            const root = jscodeshift(code);
            //处理 WfForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.CallExpression)
                .filter((path: any) => {
                    return path.node['callee'].property && path.node['callee'].property.name == "bindDetailFieldChangeEvent";
                })
                .filter((path: any) => {
                    return path.node['callee'].object && path.node['callee'].object.name == "WfForm";
                });

            if (bindDetailFieldChangeEventCom.length > 0) {
                bindDetailFieldChangeEventCom.forEach((nodePath: any) => {
                    if (nodePath.node.arguments && nodePath.node.arguments.length == 2) {
                        const argumentsTemp = nodePath.node.arguments[1];
                        const argumentsFunctionTemp = argumentsTemp.body.body;
                        const params = argumentsTemp.params;
                        var weFormSdkData = "";
                        for (let i = 0; i < params.length; i++) {
                            if (i == 0) weFormSdkData = weFormSdkData + " var " + params[i].name + " = weFormSdkData.id;\r\n";
                            if (i == 1) weFormSdkData = weFormSdkData + " var " + params[i].name + " = weFormSdkData.rowId;\r\n";
                            if (i == 2) weFormSdkData = weFormSdkData + " var " + params[i].name + " = weFormSdkData.value;\r\n";
                        }
                        const defaultVariables = jscodeshift(weFormSdkData).paths()[0].value.program.body;
                        for (let i1 = defaultVariables.length - 1; i1 >= 0; i1--) {
                            argumentsFunctionTemp.unshift(defaultVariables[i1]);
                        }

                        params.splice(0, params.length);
                        params.push(jscodeshift.identifier("weFormSdkData"));
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('wFBindDetailFieldChangeEventTran-e:', e);
        }
        return code;
    }

    /**
     * 将E9流程表单 注册拦截事件，指定动作执行前触发，并可阻断/放行后续操作
     * 情况一：
     * WfForm.registerCheckEvent(WfForm.OPER_ADDROW+"1", function(callback){
     *         callback();    //允许继续添加行调用callback，不调用代表阻断添加
     * });
     * 转成：
     *     const WfForm = window.WeFormSDK.getWeFormInstance();
     *     const detailMark = WfForm.convertFieldNameToId("formtable_main_508_dt1");
     *     // 注册保存事件
     *     WfForm.registerCheckEvent(`${window.WeFormSDK.OPER_ADDROW}${detailMark}`, (callback: Function, failFn: Function)=>{
     *         // ...执行定义逻辑
     *         callback();
     *     });
     *
     *     情况二：
     *     //获取当前活动页面(对弹窗就是最上层)流程详情页面实例
     * const wffpSdk = window.weappWorkflow.getFlowPageSDK();
     * //举例：保存动作前执行自定义逻辑校验
     * wffpSdk.registerInterceptEvent("SaveData", (successFn,failFn)=>{
     *     //允许继续执行保存动作则
     *     successFn();
     *     //阻断保存则
     *     failFn();
     * });
     * WfForm.registerCheckEvent(WfForm.OPER_SAVE, function(callback){
     *         callback();    //允许继续添加行调用callback，不调用代表阻断添加
     * });
     *
     * @param code
     */
    wFRegisterCheckEventTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "registerCheckEvent"}
                }
            };

            //处理 WfForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.Node, sourceAst);

            if (bindDetailFieldChangeEventCom.length > 0) {
                const registerCheckEventType: Record<string, string> = {
                    OPER_SUBMIT: "BeforeClickOperBtn|SUBMIT,BeforeClickOperBtn|AGREE",
                    OPER_SUBMITCONFIRM: "BeforeClickOperBtn|VERIFY",
                    OPER_REJECT: "BeforeClickOperBtn|REJECT",
                    //OPER_REMARK	批注提交	,
                    //OPER_INTERVENE	干预	,
                    OPER_FORWARD: "BeforeClickOperBtn|FORWARD",
                    OPER_TAKEBACK: "BeforeClickOperBtn|TAKE_BACK",
                    OPER_DELETE: "BeforeClickOperBtn|DELETE",
                    //OPER_ADDROW	添加明细行，需拼明细表序号	,——表单的
                    //OPER_DELROW	删除明细行，需拼明细表序号	,——表单的
                    OPER_PRINTPREVIEW: "BeforeClickOperBtn|PRINT",//	打印预览
                    //OPER_EDITDETAILROW	移动端-编辑明细	,——表单的——未对应——标准后续支持
                    //OPER_BEFOREVERIFY	校验必填前触发事件	,——表单的
                    OPER_TURNHANDLE: "BeforeClickOperBtn|TURNTODO",
                    OPER_ASKOPINION: "BeforeClickOperBtn|REMARKADVICE",
                    OPER_TAKFROWARD: "BeforeClickOperBtn|REMARKADVICE_TURNTODO",
                    OPER_TURNREAD: "BeforeClickOperBtn|CIRCULATE",
                    OPER_FORCEOVER: "BeforeClickOperBtn|ARICHIVE",
                    //OPER_BEFORECLICKBTN	点右键按钮前	,
                    //OPER_SAVECOMPLETE	保存后页面跳转前	,
                    OPER_WITHDRAW: "BeforeClickOperBtn|WITHDRAW",
                    OPER_CLOSE: "ClosePageIntercept"	//页面关闭
                };
                const registerCheckEventTypeForDetail: Record<string, string> = {
                    OPER_SAVE: 'OPER_SAVE',
                    OPER_ADDROW: 'OPER_ADDROW',
                    OPER_DELROW: 'OPER_DELROW',
                    OPER_BEFOREVERIFY: 'OPER_BEFOREVERIFY'
                };

                const numberPattern = /^\d+$/;//判断是否为数字
                const numberEndWithDotPattern = /\d+(,|$)/;//数字加逗号结尾

                bindDetailFieldChangeEventCom.forEach((nodePath: any) => {
                    let detailFlag = false;

                    if (nodePath.node.arguments && nodePath.node.arguments.length === 2) {

                        //节点直接替换
                        if (nodePath.node.arguments[0]) {

                            //处理本身就是表达是的情况 WfForm.OPER_ADDROW+"1"+WfForm.OPER_REJECT
                            if (nodePath.node.arguments[0].type === 'BinaryExpression') {
                                let tempNodePath = nodePath.node.arguments[0];
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.left.type === 'MemberExpression' || tempNodePath.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.left);
                                }
                                if (tempNodePath.right.type === 'MemberExpression' || tempNodePath.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'WfForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventType[expression.property.name]) {
                                            expression.type = 'Literal';
                                            expression.value = registerCheckEventType[expression.property.name];
                                            // nodePath.node.arguments[0] = jscodeshift.literal(registerCheckEventType[nodePath.node.arguments[0]['property']['name']])
                                        } else if (registerCheckEventTypeForDetail[expression.property.name]) {
                                            detailFlag = true;
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventTypeForDetail[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });

                            }

                            //处理WfForm.OPER_REJECT 单个表达是的情况
                            if (nodePath.node.arguments[0].type == "MemberExpression"
                                && nodePath.node.arguments[0]['object'] && nodePath.node.arguments[0]['object']['name'] === 'WfForm'
                                && nodePath.node.arguments[0]['property']['name']) {
                                if (registerCheckEventType[nodePath.node.arguments[0]['property']['name']]) {
                                    nodePath.node.arguments[0] = jscodeshift.literal(registerCheckEventType[nodePath.node.arguments[0]['property']['name']])
                                } else if (registerCheckEventTypeForDetail[nodePath.node.arguments[0]['property']['name']]) {
                                    detailFlag = true;
                                    nodePath.node.arguments[0] = {
                                        type: 'MemberExpression',
                                        object: {
                                            type: 'MemberExpression',
                                            object: {type: 'Identifier', name: 'window'},
                                            property: {
                                                type: 'Identifier',
                                                name: 'WeFormSDK'
                                            }
                                        },
                                        property: {
                                            type: 'Identifier',
                                            name: registerCheckEventTypeForDetail[nodePath.node.arguments[0]['property']['name']]
                                        }
                                    };
                                }

                            }

                            //节点是表达式需要找到所有的子节点都进行替换
                            //filter 包含当前节点
                            //find   不包括当前节点
                            let argumentsTemp = jscodeshift(nodePath.node.arguments[0])
                                .find(jscodeshift.BinaryExpression);
                            argumentsTemp.forEach((tempNodePath: any) => {
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.node.left.type === 'MemberExpression' || tempNodePath.node.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.left);
                                }
                                if (tempNodePath.node.right.type === 'MemberExpression' || tempNodePath.node.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // WfForm.OPER_ADDROW 和 WfForm.OPER_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'WfForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventType[expression.property.name]) {
                                            expression.type = 'Literal';
                                            expression.value = registerCheckEventType[expression.property.name];
                                            // tempNodePath.replace(jscodeshift.literal(registerCheckEventType[tempNodePath['node']['property']['name']]))
                                        } else if (registerCheckEventTypeForDetail[expression.property.name]) {
                                            detailFlag = true;
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventTypeForDetail[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });
                            });
                        }

                        //是方法需要加一个参数
                        if (nodePath.node.arguments[1] && nodePath.node.arguments[1].type === 'FunctionExpression') {
                            nodePath.node.arguments[1] = jscodeshift.functionExpression(
                                null,
                                [...nodePath.node.arguments[1].params
                                    , jscodeshift.identifier('failFn')],
                                nodePath.node.arguments[1].body
                            );
                        }

                        const replacementAstForEcode = {
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {type: 'Identifier', name: 'window.WfFormEcode'},
                                property: {type: 'Identifier', name: 'registerCheckEvent'},
                            },
                            arguments: [
                                nodePath.node.arguments[0],
                                nodePath.node.arguments[1]
                            ]
                        };
                        nodePath.replace(replacementAstForEcode);
                    }

                    // eval(excuteText);
                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('wFRegisterCheckEventTran-e:', e);
        }

        return code;
    }


    /**
     * 处理WfForm.registerAction
     * WfForm.registerAction(WfForm.ACTION_ADDROW+"1", function(index){
     *     alert("添加行下标是"+index);
     * });     //下标从1开始，明细1添加行触发事件，注册函数入参为新添加行下标
     * 转换后：
     *  const WfForm = window.WeFormSDK.getWeFormInstance();
     *     const detailMark = WfForm.convertFieldNameToId("formtable_main_508_dt1");
     *     // 明细表添加行事件，index为添加行下标，从0开始
     *     WfForm.registerAction(`${window.WeFormSDK.ACTION_ADDROW}${detailMark}`, (index, data)=>{
     *     alert("添加行下标是"+index);
     *     });
     * @param code
     */
    wFRegisterActionTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "registerAction"}
                }
            };

            //处理 WfForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.Node, sourceAst);

            if (bindDetailFieldChangeEventCom.length > 0) {
                const numberPattern = /^\d+$/;//判断是否为数字
                const numberEndWithDotPattern = /\d+(,|$)/;//数字加逗号结尾
                const registerCheckEventType: Record<string, string> = {
                    ACTION_ADDROW: "ACTION_ADDROW",
                    ACTION_DELROW: "ACTION_DELROW",
                    ACTION_EDITDETAILROW: "ACTION_EDITDETAILROW", //移动端-编辑明细行，需拼明细表序号
                    //ACTION_SWITCHDETAILPAGING 切换明细分页
                    //ACTION_SWITCHTABLAYOUT 切换模板布局标签页
                };
                bindDetailFieldChangeEventCom.forEach((nodePath: any) => {

                    if (nodePath.node.arguments && nodePath.node.arguments.length === 2) {

                        //节点直接替换
                        if (nodePath.node.arguments[0]) {

                            //处理本身就是表达是的情况
                            if (nodePath.node.arguments[0].type === 'BinaryExpression') {
                                let tempNodePath = nodePath.node.arguments[0];
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.left.type === 'MemberExpression' || tempNodePath.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.left);
                                }
                                if (tempNodePath.right.type === 'MemberExpression' || tempNodePath.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // 处理WfForm.ACTION_ADDROW 和 WfForm.ACTION_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'WfForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventType[expression.property.name]) {
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventType[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });

                            }

                            //节点是表达式需要找到所有的子节点都进行替换
                            //filter 包含当前节点
                            //find   不包括当前节点
                            let argumentsTemp = jscodeshift(nodePath.node.arguments[0])
                                .find(jscodeshift.BinaryExpression);
                            argumentsTemp.forEach((tempNodePath: any) => {
                                let needTransExpression: jscodeshift.Expression[] = [];
                                if (tempNodePath.node.left.type === 'MemberExpression' || tempNodePath.node.left.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.left);
                                }
                                if (tempNodePath.node.right.type === 'MemberExpression' || tempNodePath.node.right.type === 'Literal') {
                                    needTransExpression.push(tempNodePath.node.right);
                                }
                                //你在循环体内修改了 expression，但这并不会影响原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 在 JavaScript 中，基本类型的传递是按值传递的，而不是按引用传递的，所以在循环体内对 expression 的修改并不会影响到原始的 tempNodePath.node.left 和 tempNodePath.node.right。
                                // 你需要修改原始的节点，而不仅仅是修改循环中的临时变量
                                needTransExpression.forEach((expression: any) => {
                                    // 处理WfForm.OPER_ADDROW 和 WfForm.OPER_DELROW 表达式
                                    if (expression
                                        && expression.type === 'MemberExpression'
                                        && expression.object
                                        && expression.object.name === 'WfForm'
                                        && expression.property
                                        && expression.property.name) {
                                        if (registerCheckEventType[expression.property.name]) {
                                            expression.object = jscodeshift.memberExpression(
                                                jscodeshift.identifier('window'),
                                                jscodeshift.identifier('WeFormSDK')
                                            );
                                            expression.property = jscodeshift.identifier(registerCheckEventType[expression.property.name]);
                                        }
                                    } else if (expression
                                        && expression.type === 'Literal'
                                        && expression.value) {
                                        if (numberPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value] || expression.value;
                                        }
                                        if (numberEndWithDotPattern.test(expression.value)) {
                                            expression.value = detailTables[expression.value.replace(',', '')] ? (detailTables[expression.value.replace(',', '')] + ',') : expression.value;
                                        }
                                    }
                                });
                            });
                        }

                        //是方法需要加一个参数
                        //防止参数存在data的问题
                        if (nodePath.node.arguments[1] && nodePath.node.arguments[1].type === 'FunctionExpression') {
                            nodePath.node.arguments[1] = jscodeshift.functionExpression(
                                null,
                                [...nodePath.node.arguments[1].params
                                    , jscodeshift.identifier('data')],
                                nodePath.node.arguments[1].body
                            );
                        }

                        const replacementAst = {
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {type: 'Identifier', name: 'WfForm'},
                                property: {type: 'Identifier', name: 'registerAction'},
                            },
                            arguments: [
                                nodePath.node.arguments[0],
                                nodePath.node.arguments[1]
                            ]
                        };

                        nodePath.replace(replacementAst);
                        nodePath.node.comments = [
                            jscodeshift.commentLine('注意明细表的1、2、3...需要替换成明细表的id，可以通过WfForm.convertFieldNameToId("明细表名")获取到明细表id')
                        ]
                    }

                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('wFRegisterActionTran-e:', e);
        }

        return code;
    }

    /**
     * 暂时无需调用，后边会统一处理detail_*这种格式
     * 转换字段名称为id
     * var fieldid = WfForm.convertFieldNameToId("zs");
     * var fieldid = WfForm.convertFieldNameToId("zs_mx", "detail_1");
     * var fieldid = WfForm.convertFieldNameToId("zs_mx", "detail_1", false);
     *
     * detail_1 需要转换成明细表id
     */
    wfConvertFieldNameToIdTran = (code: string) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "convertFieldNameToId"}
                }
            };

            //处理 WfForm.bindDetailFieldChangeEvent
            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.Node, sourceAst);

            if (wfConvertFieldNameToIdComs.length > 0) {
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length >= 2) {
                        nodePath.node.comments = [
                            jscodeshift.commentLine('注意明细表的detail_1、detail_2、detail_3...需要替换成明细表的id，可以通过WfForm.convertFieldNameToId("明细表名")获取到明细表id')
                        ]
                    }
                })
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfConvertFieldNameToIdTran-e:', e);
        }
        return code;
    }

    /**
     * 字段区域绑定动作事件
     * onblur 失去焦点事件，仅支持单行文本类型
     * onfocus 获取焦点事件，仅支持单行文本字段类型
     * onclick 单击事件，字段所在单元格区域单击触发
     * ondbclick 双击事件，字段所在单元格区域双击触发
     * mouseover 鼠标移入事件，鼠标移入字段所在单元格区域触发
     * mouseout 鼠标移出事件，鼠标移出字段所在单元格区域触发
     *
     * 场景一：
     * WfForm.bindFieldAction("onfocus", "field111,field222", function(fieldid,rowIndex){
     * });
     * 转成
     * WfForm.bindFieldAction("onfocus", "field111,field222", function(data){
     *         // 取字段标识
     *         const fieldid = data.id;
     *         // 取字段修改的值
     *         const value = data.value;
     *         // 修改行号
     *         const rowIndex = data.rowId;
     * });
     *
     * 场景二：
     * blur => onblur
     * focus => onfocus
     * click => onclick
     * dblclick => ondbclick
     * mouseover => mouseover
     * mouseout => mouseout
     *
     * $("#field127245").bind("blur",function(){})
     * 转成
     * WfForm.bindFieldAction("onblur", "field127245", function(data){
     * });
     *
     * 场景三:
     * $("#field30899").blur();
     * $("#field30899").focus();
     * $("#field30899").click();
     * $("#field30899").dblclick();
     * 转成
     * WfForm.bindFieldAction("onblur", "field30899", function(data){
     * });
     * WfForm.bindFieldAction("onfocus", "field30899", function(data){
     * });
     * WfForm.bindFieldAction("onclick", "field30899", function(data){
     * });
     * WfForm.bindFieldAction("ondbclick", "field30899", function(data){
     * });
     */
    wfBindFieldActionTran = (code: string) => {

        try {
            const root = jscodeshift(code);
            const sourceAst = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "bindFieldAction"}
                }
            };

            //处理 WfForm.bindDetailFieldChangeEvent
            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.Node, sourceAst);

            if (wfConvertFieldNameToIdComs.length > 0) {
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 3
                        && nodePath.node.arguments[2].params
                        && nodePath.node.arguments[2].type === 'FunctionExpression') {

                        // 直接去替换模版的方式
                        let $replace1$ = nodePath.node.arguments[0]
                        let $replace2$ = nodePath.node.arguments[1]
                        // let $replace3$ = nodePath.node.arguments[2]?.params?.[0]?.name
                        // let $replace4$ = nodePath.node.arguments[2]?.params?.[1]?.name
                        let $replace5$ = nodePath.node.arguments[2].body.body

                        let bodyComs = [...$replace5$];

                        let paramsNum = nodePath.node.arguments[2]?.params?.length || 0;
                        // 使用 for 循环遍历 paramsNum，从paramsNum-1开始
                        //只有 i == 0 i == 1时有意义，其他默认都是id
                        for (let i = paramsNum - 1; i > -1; i--) {
                            if (nodePath.node.arguments[2]?.params?.[i]?.name) {
                                bodyComs.unshift({
                                    type: 'VariableDeclaration',
                                    kind: 'var',
                                    declarations: [{
                                        type: 'VariableDeclarator',
                                        id: {type: 'Identifier', name: nodePath.node.arguments[2]?.params?.[i]?.name},
                                        init: {
                                            type: 'MemberExpression',
                                            object: {type: 'Identifier', name: 'weaFormSdkData'},
                                            property: {
                                                type: 'Identifier',
                                                name: i == 0 ? 'id' : (i == 1 ? 'rowId' : 'id')
                                            }
                                        }
                                    }]
                                })

                            }
                        }

                        const targetAst = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "WfForm"},
                                property: {type: "Identifier", name: "bindFieldAction"}
                            },
                            arguments: [
                                $replace1$,
                                $replace2$,
                                {
                                    type: "FunctionExpression",
                                    params: [{type: 'Identifier', name: 'weaFormSdkData'}],
                                    body: {
                                        type: 'BlockStatement',
                                        body: bodyComs
                                    }
                                }
                            ]
                        };

                        nodePath.replace(targetAst);

                    }
                })
            }

            //第二步处理：$("#field127245").bind("blur",function(){})
            const fieldNameRegex = /^#field\d*$/;
            // 找到所有使用 bind() 方法的调用表达式
            root.find(jscodeshift.CallExpression, {
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'CallExpression',
                        callee: {
                            type: 'Identifier',
                            name: 'jQuery'
                        }
                    },
                    property: {
                        type: 'Identifier',
                        name: 'bind'
                    }
                }
            }).forEach((path: any) => {
                const callExpression = path.value.callee.object;
                const selector = callExpression.arguments[0].value;
                const args = path.value.arguments;

                if (fieldNameRegex.test(selector)) {

                    if (args.length === 2 && args[0].type === 'Literal' && args[1].type === 'FunctionExpression') {
                        const event = args[0].value;
                        const handler = args[1];

                        if (typeof event === 'string') {
                            // 将事件名转换为对应的属性名
                            let eventName;
                            switch (event) {
                                case 'blur':
                                    eventName = 'onblur';
                                    break;
                                case 'focus':
                                    eventName = 'onfocus';
                                    break;
                                case 'click':
                                    eventName = 'onclick';
                                    break;
                                case 'dblclick':
                                    eventName = 'ondbclick';
                                    break;
                                case 'mouseover':
                                    eventName = 'onmouseover';
                                    break;
                                case 'mouseout':
                                    eventName = 'onmouseout';
                                    break;
                                default:
                                    eventName = event;
                            }

                            // 构造新的调用表达式
                            const newCallExpression = jscodeshift.callExpression(
                                jscodeshift.memberExpression(
                                    jscodeshift.identifier('WfForm'),
                                    jscodeshift.identifier('bindFieldAction')
                                ), [
                                    jscodeshift.literal(eventName),
                                    jscodeshift.literal(selector.replace("#","")), // 字段 ID
                                    handler // 事件处理函数
                                ]
                            );

                            // 用新的调用表达式替换原有的调用表达式
                            path.replace(newCallExpression);
                        }
                    }
                }
            });

            //第三步处理：$("#field30899").blur(); $("#field30899").focus(); $("#field30899").click(); $("#field30899").dblclick();
            const eventMappings = {
                blur: 'onblur',
                focus: 'onfocus',
                click: 'onclick',
                dblclick: 'ondbclick'
            };

            Object.entries(eventMappings).forEach(([originalEvent, newEvent]) => {
                root.find(jscodeshift.CallExpression, {
                    callee: {
                        type: 'MemberExpression',
                        object: {
                            type: 'CallExpression',
                            callee: {
                                type: 'Identifier',
                                name: 'jQuery'
                            }
                        },
                        property: {
                            type: 'Identifier',
                            name: originalEvent
                        }
                    }
                }).forEach((path: any) => {
                    const selector = path.value.callee.object.arguments[0].value;
                    const functionExpression = path.value.arguments[0];

                    if (functionExpression && fieldNameRegex.test(selector)) {
                        const newCallExpression = jscodeshift.callExpression(
                            jscodeshift.memberExpression(
                                jscodeshift.identifier('WfForm'),
                                jscodeshift.identifier('bindFieldAction')
                            ), [
                                jscodeshift.literal(newEvent),
                                jscodeshift.literal(selector.replace("#","")),
                                functionExpression
                            ]
                        );
                        path.replace(newCallExpression);
                    }
                });
            });
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfBindFieldActionTran-e:', e);
        }
        return code;
    }
    /**
     * 获取明细已有行的数据库主键
     * WfForm.getDetailRowKey("field112_3");   //获取明细第四行主键
     * 转换后：
     *  // 获取表单实例
     *  const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *  // 获取主表字段fieldId
     *  const detailMark = weFormSdk.convertFieldNameToId("field112所属明细表名");
     *  // 获取第四行的rowId
     *  const oneRowId = weFormSdk.getDetailRowIdByIndex(detailMark, 4);
     * @param code
     */
    wfGetDetailRowKeyTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "getDetailRowKey"}
                },
                arguments: []
            };

            const wfGetDetailRowKeyComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetDetailRowKeyComs.length > 0) {
                // 定义正则表达式来匹配函数名
                const fieldNameRegex = /^field\d*$/;
                wfGetDetailRowKeyComs.forEach((nodePath) => {

                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 1
                        && nodePath['node']['arguments'][0].type === 'Literal'
                        && nodePath['node']['arguments'][0]['value']
                        && typeof nodePath['node']['arguments'][0]['value'] === 'string'
                        && nodePath['node']['arguments'][0]['value'].split('_').length === 2) {

                        // 直接去替换模版的方式
                        let $replace1$ = '字段' + nodePath.node.arguments[0]['value'].split('_')[0] + '所属明细表id';
                        if (fieldNameRegex.test(nodePath.node.arguments[0]['value'].split('_')[0])
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')]
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id) {
                            $replace1$ = fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id
                        }
                        let $replace2$ = parseInt(nodePath.node.arguments[0]['value'].split('_')[1]) + 1;
                        const targetAST: jscodeshift.CallExpression = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "WfForm"},
                                property: {type: "Identifier", name: "getDetailRowIdByIndex"}
                            },
                            arguments: [{type: 'Literal', value: $replace1$}, {type: 'Literal', value: $replace2$}]
                        }
                        nodePath.replace(targetAST);
                    }

                });
                //检测代码转换后的代码是否正常
                if (jscodeshift(root.toSource()))
                    return root.toSource();
            }


        } catch (e) {
            if (window.console) console.log('wfGetDetailRowKeyTran-e:', e);
        }

        return code;
    }

    /**
     * 根据明细行标识获取序号(第几行)
     * 有两种写法，需要区分。
     * 1. 传入的参数是字段名，需要转换为字段id
     * 2. 传入的参数是字段id
     * WfForm.getDetailRowSerailNum("detail_1", 3);    //获取明细1下标为3的行序号
     * WfForm.getDetailRowSerailNum("field222_3");    //获取字段222对应明细表下标为3的行序号
     * 转换后：
     *  // 获取表单实例
     *  const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *  // 获取主表字段fieldId
     *  const detailMark = weFormSdk.convertFieldNameToId("mxbo16sr2");
     *  // 这是结果的所在行下标：3
     *  const detailRowStr = weFormSdk.getDetailRowSerailNum(detailMark,weFormSdk.getDetailRowIdByIndex(detailMark, 4));
     * @param code
     */
    wfGetDetailRowSerailNumTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "getDetailRowSerailNum"}
                },
                arguments: []
            };

            const wfGetDetailRowKeyComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetDetailRowKeyComs.length > 0) {
                // 定义正则表达式来匹配函数名
                const fieldNameRegex = /^field\d*$/;
                wfGetDetailRowKeyComs.forEach((nodePath) => {

                    if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 1
                        && nodePath['node']['arguments'][0].type === 'Literal'
                        && nodePath['node']['arguments'][0]['value']
                        && typeof nodePath['node']['arguments'][0]['value'] === 'string'
                        && nodePath['node']['arguments'][0]['value'].split('_').length === 2) {

                        // 直接去替换模版的方式
                        let $replace1$ = '字段' + nodePath.node.arguments[0]['value'].split('_')[0] + '所属明细表id';
                        if (fieldNameRegex.test(nodePath.node.arguments[0]['value'].split('_')[0])
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')]
                            && fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id) {
                            $replace1$ = fieldMap[nodePath.node.arguments[0]['value'].split('_')[0].replace('field', '')].e10_sub_form_id
                        }
                        let $replace2$ = parseInt(nodePath.node.arguments[0]['value'].split('_')[1]) + 1;
                        const targetAST: jscodeshift.CallExpression = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "WfForm"},
                                property: {type: "Identifier", name: "getDetailRowSerailNum"}
                            },
                            arguments: [
                                {type: 'Literal', value: $replace1$},
                                {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "WfForm"},
                                        property: {type: "Identifier", name: "getDetailRowIdByIndex"}
                                    },
                                    arguments: [{type: 'Literal', value: $replace1$}, {
                                        type: 'Literal',
                                        value: $replace2$
                                    }]
                                }
                            ]
                        }
                        nodePath.replace(targetAST);
                    } else if (nodePath['node'] && nodePath['node']['arguments'] && nodePath['node']['arguments'].length === 2
                        && nodePath['node']['arguments'][0].type === 'Literal'
                        && nodePath['node']['arguments'][0]['value']
                        && typeof nodePath['node']['arguments'][0]['value'] === 'string'
                        && nodePath['node']['arguments'][1].type === 'Literal'
                        && nodePath['node']['arguments'][1]['value']
                        && typeof nodePath['node']['arguments'][1]['value'] === 'number') {

                        // 直接去替换模版的方式
                        let $replace1$ = nodePath.node.arguments[0]['value'];
                        let $replace2$ = nodePath.node.arguments[1]['value'] + 1;
                        const targetAST: jscodeshift.CallExpression = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "WfForm"},
                                property: {type: "Identifier", name: "getDetailRowSerailNum"}
                            },
                            arguments: [
                                {type: 'Literal', value: $replace1$},
                                {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "WfForm"},
                                        property: {type: "Identifier", name: "getDetailRowIdByIndex"}
                                    },
                                    arguments: [{type: 'Literal', value: $replace1$}, {
                                        type: 'Literal',
                                        value: $replace2$
                                    }]
                                }
                            ]
                        }
                        nodePath.replace(targetAST);
                    }

                });
                //检测代码转换后的代码是否正常
                if (jscodeshift(root.toSource()))
                    return root.toSource();
            }


        } catch (e) {
            if (window.console) console.log('wfGetDetailRowSerailNumTran-e:', e);
        }
        return code;
    }
    /**
     * 获取当前打开请求的基础信息
     *  console.log(WfForm.getBaseInfo());    //返回当前请求基础信息
     * //输出对象说明：
     * {
     *     f_weaver_belongto_userid: "5240"    //用户信息
     *     f_weaver_belongto_usertype: "0"
     *     formid: -2010       //表单id
     *     isbill: "1"         //新表单/老表单
     *     nodeid: 19275       //节点id
     *     requestid: 4487931  //请求id
     *     workflowid: 16084   //路径id
     * }
     *
     * 只处理：WfForm.getBaseInfo().formid这种形式的结构
     *
     * 这边需要对于这几个属性单独处理。
     * window.ebuilderSDK.getCurrUser().id
     * window.ebuilderSDK.getCurrUser().type "inside"
     * window.WeFormSDK.getWeFormInstance().getBaseInfo().formId
     * window.weappWorkflow.getFlowPageSDK().getBaseParam().userCurrentNodeId
     * window.weappWorkflow.getFlowPageSDK().getBaseParam().requestId
     * window.weappWorkflow.getFlowPageSDK().getBaseParam().workflowId
     *
     * E10已经不存在：新老表单之分
     * isbill
     *
     * @param code
     */
    wfGetBaseInfoTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const names = ['f_weaver_belongto_userid', 'f_weaver_belongto_usertype', 'formid', 'isbill', 'nodeid', 'requestid', 'workflowid'];
            names.forEach((name) => {
                const sourceAst: jscodeshift.MemberExpression = {
                    type: "MemberExpression",
                    object: {
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {type: "Identifier", name: "WfForm"},
                            property: {type: "Identifier", name: "getBaseInfo"}
                        },
                        arguments: []
                    },
                    property: {type: "Identifier", name: name}
                };

                const wfGetDetailRowKeyComs = root
                    .find(jscodeshift.MemberExpression, sourceAst);

                if (wfGetDetailRowKeyComs.length > 0) {
                    wfGetDetailRowKeyComs.forEach((nodePath) => {
                        let targetAST: jscodeshift.MemberExpression;
                        if (name === 'f_weaver_belongto_userid') {
                            targetAST = {
                                type: "MemberExpression",
                                object: {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "window.ebuilderSDK"},
                                        property: {type: "Identifier", name: "getCurrUser"}
                                    },
                                    arguments: []
                                },
                                property: {type: "Identifier", name: 'id'}
                            }
                            nodePath.replace(targetAST);
                        } else if (name === 'f_weaver_belongto_usertype') {
                            targetAST = {
                                type: "MemberExpression",
                                object: {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "window.ebuilderSDK"},
                                        property: {type: "Identifier", name: "getCurrUser"}
                                    },
                                    arguments: []
                                },
                                property: {type: "Identifier", name: 'type'}
                            }
                            nodePath.replace(targetAST);
                        } else if (name === 'formid') {
                            targetAST = {
                                type: "MemberExpression",
                                object: {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "WfForm"},
                                        property: {type: "Identifier", name: "getBaseInfo"}
                                    },
                                    arguments: []
                                },
                                property: {type: "Identifier", name: 'formId'}
                            }
                            nodePath.replace(targetAST);
                        } else if (name === 'nodeid') {
                            targetAST = {
                                type: "MemberExpression",
                                object: {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "wffpSdk"},
                                        property: {type: "Identifier", name: "getBaseParam"}
                                    },
                                    arguments: []
                                },
                                property: {type: "Identifier", name: 'userCurrentNodeId'}
                            }
                            nodePath.replace(targetAST);
                        } else if (name === 'requestid') {
                            targetAST = {
                                type: "MemberExpression",
                                object: {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "wffpSdk"},
                                        property: {type: "Identifier", name: "getBaseParam"}
                                    },
                                    arguments: []
                                },
                                property: {type: "Identifier", name: 'requestId'}
                            }
                            nodePath.replace(targetAST);
                        } else if (name === 'workflowid') {
                            targetAST = {
                                type: "MemberExpression",
                                object: {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "wffpSdk"},
                                        property: {type: "Identifier", name: "getBaseParam"}
                                    },
                                    arguments: []
                                },
                                property: {type: "Identifier", name: 'workflowId'}
                            }
                            nodePath.replace(targetAST);
                        } else {
                            nodePath.node.comments = [
                                jscodeshift.commentBlock(` 无法替换部分：${jscodeshift(nodePath).toSource()} `)
                            ]
                        }
                    })
                }
            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfGetBaseInfoTran-e:', e);
        }
        return code;
    }

    /**
     * 可控制显示时间的message信息
     * WfForm.showMessage("结束时间需大于开始时间");   //警告信息，1.5s后自动消失
     * WfForm.showMessage("运算错误", 2, 10);  //错误信息，10s后消失
     * 轉換後
     * // 普通提示
     * window.WeFormSDK.showMessage("This is a success prompt", 3, 2);
     * @param code
     */
    wfShowMessageTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAsts: jscodeshift.CallExpression[] = [{
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "showMessage"}
                },
                arguments: []
            }, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "showConfirm"}
                },
                arguments: []
            }];
            sourceAsts.forEach((sourceAst) => {
                const wfConvertFieldNameToIdComs = root
                    .find(jscodeshift.CallExpression, sourceAst);

                if (wfConvertFieldNameToIdComs.length > 0) {
                    wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                        nodePath['node']['callee']['object']['name'] = 'window.WeFormSDK';
                    });
                }
            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfShowMessageTran-e:', e);
        }
        return code;
    }

    /**
     * 调用右键按钮事件
     * E9——E10
     * WfForm.doRightBtnEvent("BTN_SUBBACKNAME");     //触发提交需反馈——SUBMIT
     * WfForm.doRightBtnEvent("BTN_SUBMIT");     //触发提交不需反馈——SUBMIT_NOFEEDBACK
     * WfForm.doRightBtnEvent("BTN_WFSAVE");     //触发保存——SAVE_DATA
     * WfForm.doRightBtnEvent("BTN_REJECTNAME");     //触发退回——REJECT
     * WfForm.doRightBtnEvent("BTN_FORWARD");     //触发转发——FORWARD
     * WfForm.doRightBtnEvent("BTN_REMARKADVICE");     //触发意见征询——REMARKADVICE
     * WfForm.doRightBtnEvent("BTN_TURNTODO");     //触发转办——TURNTODO
     * WfForm.doRightBtnEvent("BTN_DORETRACT");     //触发强制收回——TAKE_BACK
     * WfForm.doRightBtnEvent("BTN_PRINT");     //触发打印——PRINT
     *
     * 转换后：
     * const wffpSdk = window.weappWorkflow.getFlowPageSDK();
     * //F12通过console查看详情按钮信息
     * console.log(window.weappWorkflow.getFlowPageSDK().baseStore.operMenus);
     * //找到对应按钮，根据menutype触发按钮
     * wffpSdk.doTriggerRightBtn(${menuType});
     *
     * @param code
     */
    wfDoRightBtnEventTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "doRightBtnEvent"}
                },
                arguments: []
            };

            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfConvertFieldNameToIdComs.length > 0) {
                const e9TypeToE10Type: Record<string, string> = {
                    BTN_SUBBACKNAME: "SUBMIT",
                    BTN_SUBMIT: "SUBMIT_NOFEEDBACK",
                    BTN_WFSAVE: "SAVE_DATA",
                    BTN_REJECTNAME: "REJECT",
                    BTN_FORWARD: "FORWARD",
                    BTN_REMARKADVICE: "REMARKADVICE",
                    BTN_TURNTODO: "TURNTODO",
                    BTN_DORETRACT: "TAKE_BACK",
                    BTN_PRINT: "PRINT"
                }
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['object']['name'] = 'wffpSdk';
                    nodePath['node']['callee']['property']['name'] = 'doTriggerRightBtn';
                    if (nodePath.node.arguments
                        && nodePath.node.arguments[0]
                        && nodePath.node.arguments[0].type === 'Literal') {
                        if (e9TypeToE10Type[nodePath.node.arguments[0].value]) {
                            nodePath.node.arguments[0].value = e9TypeToE10Type[nodePath.node.arguments[0].value];
                        }
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfDoRightBtnEventTran-e:', e);
        }
        return code;
    }
    /**
     * 移动端打开链接方式
     * window.showHoverWindow('/workflow/test.jsp', '/req');   //主界面打开链接
     * window.showHoverWindow('https://www.baidu.com', '/req/editDetailRow');   //明细行编辑界面打开链接
     *
     * 转换后：
     * //E9这里是打开iframe，触发入口在表单中；可以先由weappSDK.openLink代替
     * window.weappUtils.weappSDK.openLink({url:'/workflow/test.jsp'});
     *
     * @param code
     */
    wfShowHoverWindowTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAsts: jscodeshift.CallExpression[] = [{
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "window"},
                    property: {type: "Identifier", name: "showHoverWindow"}
                },
                arguments: []
            }, {
                type: "CallExpression",
                callee: {
                    type: "Identifier",
                    name: "showHoverWindow"
                },
                arguments: []
            }];

            sourceAsts.forEach(sourceAst => {
                const wfConvertFieldNameToIdComs = root
                    .find(jscodeshift.CallExpression, sourceAst);

                if (wfConvertFieldNameToIdComs.length > 0) {
                    wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                        let $replace$ = nodePath['node']['arguments']?.[0];
                        nodePath.replace({
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "window.weappUtils.weappSDK"},
                                property: {type: "Identifier", name: "openLink"}
                            },
                            arguments: [{
                                type:"ObjectExpression",
                                properties:[
                                    {
                                        type:"Property",
                                        key:{type: "Identifier", name: "url"},
                                        value:$replace$
                                    }
                                ]
                            }]
                        });
                    });
                }
            });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfShowHoverWindowTran-e:', e);
        }
        return code;
    }
    /**
     * 获取签字意见内容
     * WfForm.getSignRemark();  //获取签字意见内容
     *
     * 转换后：
     * const wffpSdk = window.weappWorkflow.getFlowPageSDK();
     * //获取意见内容，默认去除html格式
     * const remarkContent = wffpSdk.getSignRemark();
     * //获取原始意见内容，含html格式
     * const remarkContent = wffpSdk.getSignRemark(false);
     * @param code
     */
    wfGetSignRemarkTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "getSignRemark"}
                },
                arguments: []
            };

            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfConvertFieldNameToIdComs.length > 0) {
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['object']['name'] = 'wffpSdk';
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfGetSignRemarkTran-e:', e);
        }
        return code;
    }

    /**
     *
     * 设置签字意见内容
     * setSignRemark: function(text,isClear=true,isAfter=true,callback)
     * WfForm.setSignRemark("覆盖设置签字意见内容");
     * WfForm.setSignRemark("原有意见内容前追加请审批", false, false);——这种没对应
     *
     * 转换后：
     * 修改意见：changeRemark = (content: string,isOperDialog:boolean,isClear:boolean,isAfter:boolean,callback)
     * const wffpSdk = window.weappWorkflow.getFlowPageSDK();
     * //修改签字意见
     * wffpSdk.changeRemark('同意');//修改签字意见
     *
     * //原有意见内容前追加内容
     * wffpSdk.changeRemark('你好',false,false,false);
     *
     * @param code
     */
    wfSetSignRemarkTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "setSignRemark"}
                },
                arguments: []
            };

            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfConvertFieldNameToIdComs.length > 0) {
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['object']['name'] = 'wffpSdk';
                    nodePath['node']['callee']['property']['name'] = 'changeRemark';
                    let argumentsTemp = nodePath['node']?.['arguments'];
                    if (argumentsTemp && argumentsTemp.length >= 1) {
                        argumentsTemp.splice(1, 0, {
                            type: "BooleanLiteral",
                            value: false
                        });
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfSetSignRemarkTran-e:', e);
        }
        return code;
    }

    /**
     * 获取字段当前的只读/必填属性
     * WfForm.getFieldCurViewAttr("field110_2");   //获取明细字段属性，1：只读、2：可编辑、3：必填；已办全部为只读；
     *
     * 转换后：
     *  // 获取表单实例
     *     const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *     // 获取主表字段
     *     const fieldMark = weFormSdk.convertFieldNameToId("wbk");
     *     // 获取该字段的读写属性
     *     weFormSdk.getFieldAttr(fieldMark);
     *
     * @param code
     */
    wfGetFieldCurViewAttrTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "getFieldCurViewAttr"}
                },
                arguments: []
            };

            const wfGetFieldCurViewAttrComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetFieldCurViewAttrComs.length > 0) {
                wfGetFieldCurViewAttrComs.forEach((nodePath: any) => {
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {type: "Identifier", name: "WfForm"},
                            property: {type: "Identifier", name: "getFieldAttr"}
                        },
                        arguments: nodePath.node.arguments
                    })
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfGetFieldCurViewAttrTran-e:', e);
        }
        return code;
    }

    /**
     * 系统字段获取值的转换
     * WfForm.getFieldValue("field-1");——请求名称
     * WfForm.getFieldValue("field-2");——紧急程度
     * WfForm.getFieldValue("field-10");——密级
     *
     * 转换后：
     * wffpSdk.getSysFieldValue('RequestName')?.value || '';
     * wffpSdk.getSysFieldValue('RequestLevel')?.value || '';
     * wffpSdk.getSysFieldValue('RequestSecLevel')?.value || '';——E10不支持,后续标准会增加，我们先补充上
     *
     * @param code
     */
    wfGetFieldValueTran = (code: string) => {

        try {

            const fieldMap: Record<string, string> = {
                "field-1": "RequestName",
                "field-2": "RequestLevel",
                "field-10": "RequestSecLevel" //目前标准不支持
            };

            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "getFieldValue"}
                },
                arguments: []
            };

            const wfGetFieldValueObjComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetFieldValueObjComs.length > 0) {
                wfGetFieldValueObjComs.forEach((nodePath: any) => {
                    if (nodePath.node.arguments && nodePath.node.arguments[0] && nodePath.node.arguments[0].type === 'Literal') {
                        if (fieldMap[nodePath.node.arguments[0].value])
                            nodePath.replace({
                                type: "CallExpression",
                                callee: {
                                    type: "MemberExpression",
                                    object: {type: "Identifier", name: "wffpSdk"},
                                    property: {type: "Identifier", name: "getSysFieldValue"}
                                },
                                arguments: [{
                                    type: 'Literal',
                                    value: fieldMap[nodePath.node.arguments[0].value]
                                }]
                            })
                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfGetFieldValueObjTran-e:', e);
        }
        return code;
    }

    /**
     * 获取浏览按钮、选项框等的选中的对象数据
     * wfform.getFieldValueObj("field111_1")
     * 数据结构如下：
     * {
     *     "value": "6649",
     *     "specialobj": [
     *         {
     *             "count": 0,
     *             "id": "6649",
     *             "name": "孙小凤"
     *         }
     *     ]
     * }
     * 转换后：
     * 7.7 获取浏览数据、浏览选项对象
     * getBrowserOptionEntity: (fieldMark: string) => any[];
     *
     *    const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *    // 获取主表字段fieldId
     *    const fieldMark = weFormSdk.convertFieldNameToId("fxk");
     *    // 获取选项数组， 对象结构: [{id:'xxx', name: 'xxx'}]; 结构同3.3的specialObj入参结构
     *    const optionList = weFormSdk.getBrowserOptionEntity(fieldMark)
     *
     * @param code
     */
    wfGetFieldValueObjTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "getFieldValueObj"}
                },
                arguments: []
            };

            const wfGetFieldValueObjComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfGetFieldValueObjComs.length > 0) {
                wfGetFieldValueObjComs.forEach((nodePath: any) => {
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {type: "Identifier", name: "WfForm"},
                            property: {type: "Identifier", name: "getBrowserOptionEntity"}
                        },
                        arguments: nodePath.node.arguments
                    })
                    nodePath.node.comments = [
                        jscodeshift.commentBlock(`E10-getBrowserOptionEntity接口返回的数据接口和E9不一致，value/specialobj属性已经不存在了，只有specialObj属性了!`)
                    ]
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfGetFieldValueObjTran-e:', e);
        }
        return code;
    }


    /**
     * 变化触发事件bindPropertyChange：
     * jQuery("#field27563").bindPropertyChange(function(obj,id,value){
     *         console.log("tri...",obj,id,value);
     *     });
     * 改造成：
     *  // 获取表单实例
     *  const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *  // 获取主表字段fieldId
     *  const textFieldMark = weFormSdk.convertFieldNameToId("wbk");
     *  const selectFieldMark = weFormSdk.convertFieldNameToId("xlk");
     *  // 绑定事件，对主表字段和明细表的某一行绑定
     *  weFormSdk.bindFieldChangeEvent(`${textFieldMark},${selectFieldMark}`, (data) => {
     *  // 取字段标识
     *  const fieldMark = data.id;
     *  // 取字段修改的值
     *  const value = data.value;
     *  console.log(data);
     *  });
     */
    wfBindPropertyChangeTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {
                        type: 'CallExpression',
                        callee: {type: "Identifier", name: "jQuery"},
                        arguments: []
                    },
                    property: {type: "Identifier", name: "bindPropertyChange"}
                },
                arguments: []
            };

            const wfConvertFieldNameToIdComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            const fieldRegex = /^#field\d+$/;
            const fieldDetailRegex = /^#field\d+_\d*$/;
            if (wfConvertFieldNameToIdComs.length > 0) {
                wfConvertFieldNameToIdComs.forEach((nodePath: any) => {
                    let $replace1$ = nodePath['node']['callee']['object']['arguments'][0];
                    if($replace1$.type==='Literal' && typeof $replace1$.value === 'string'
                        && (fieldRegex.test($replace1$.value) || fieldDetailRegex.test($replace1$.value))){

                        $replace1$.value = $replace1$.value.replace("#","");
                        let $replace2$ = nodePath['node']['arguments'][0]?.['params']?.[0]?.['name']
                        let $replace3$ = nodePath['node']['arguments'][0]?.['params']?.[1]?.['name']
                        let $replace4$ = nodePath['node']['arguments'][0]?.['params']?.[2]?.['name']
                        let $replace5$ = nodePath['node']['arguments'][0]?.['body']?.['body']

                        const replacementAst = {
                            type: 'CallExpression',
                            callee: {
                                type: 'MemberExpression',
                                object: {type: 'Identifier', name: 'WfForm'},
                                property: {type: 'Identifier', name: 'bindFieldChangeEvent'},
                            },
                            arguments: [
                                $replace1$,
                                {
                                    type: 'FunctionExpression',
                                    params: [{type: 'Identifier', name: 'weaFormSdkData'}],
                                    body: {
                                        type: 'BlockStatement',
                                        body: [
                                            {
                                                type: 'VariableDeclaration',
                                                kind: 'var',
                                                declarations: [{
                                                    type: 'VariableDeclarator',
                                                    id: {type: 'Identifier', name: $replace2$ ? $replace2$ : 'e9_obj'},
                                                    init: {type: 'Identifier', name: 'weaFormSdkData'}
                                                }]
                                            },
                                            {
                                                type: 'VariableDeclaration',
                                                kind: 'var',
                                                declarations: [{
                                                    type: 'VariableDeclarator',
                                                    id: {type: 'Identifier', name: $replace3$ ? $replace3$ : 'e9_id'},
                                                    init: {
                                                        type: 'MemberExpression',
                                                        object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                        property: {type: 'Identifier', name: 'id'}
                                                    }
                                                }]
                                            },
                                            {
                                                type: 'VariableDeclaration',
                                                kind: 'var',
                                                declarations: [{
                                                    type: 'VariableDeclarator',
                                                    id: {type: 'Identifier', name: $replace4$ ? $replace4$ : 'e9_value'},
                                                    init: {
                                                        type: 'MemberExpression',
                                                        object: {type: 'Identifier', name: 'weaFormSdkData'},
                                                        property: {type: 'Identifier', name: 'value'}
                                                    }
                                                }]
                                            },
                                            ...$replace5$
                                        ]
                                    }
                                },
                            ],
                        };

                        nodePath.replace(replacementAst);
                    }

                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfSetSignRemarkTran-e:', e);
        }
        return code;
    }


    /**
     * 明细新增行渲染后触发事件
     * 说明：重载_customAddFun”+groupid+”函数，groupid从0开始递增，0代表明细1；不论手动添加、联动添加、接口添加都会触发
     * function _customAddFun1(addIndexStr){      //明细2新增成功后触发事件，addIndexStr即刚新增的行标示，添加多行为(1,2,3)
     *     console.log("新增的行标示："+addIndexStr);
     * }
     * 转换后：
     *     const WfForm = window.WeFormSDK.getWeFormInstance();
     *     const detailMark = WfForm.convertFieldNameToId("formtable_main_508_dt1");
     *     // 明细表添加行事件，index为添加行下标，从0开始
     *     WfForm.registerAction(`${window.WeFormSDK.ACTION_ADDROW}${detailMark}`, (index, data)=>{
     *     alert("添加行下标是"+index);
     *     });
     * @param code
     */
    wFCustomAddOrDelFunTran = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);
            // 定义正则表达式来匹配函数名
            const functionNameRegex = [/^_customAddFun\d*$/, /^_customDelFun\d*$/];

            functionNameRegex.forEach((functionNameRegex: any) => {
                //处理 WfForm.bindDetailFieldChangeEvent
                const bindDetailFieldChangeEventCom = root
                    .find(jscodeshift.FunctionDeclaration).filter((nodePath: any) => {
                        const functionName = nodePath.node.id?.name;
                        return functionNameRegex.test(functionName);
                    });

                if (bindDetailFieldChangeEventCom.length > 0) {

                    bindDetailFieldChangeEventCom.forEach((nodePath: any) => {
                        const comments = nodePath?.['node']?.['comments'];
                        let addFlag = true;
                        if (nodePath['node']['id']['name'].indexOf('_customDelFun') === 0) {
                            addFlag = false;
                        }
                        const detailNum = parseInt(nodePath['node']['id']['name'].replace('_customAddFun', '').replace('_customDelFun', '')) + 1;//需要加1，流程的是从1开始
                        let detailMark = detailTables && detailTables[`${detailNum}`] ? detailTables[`${detailNum}`] : `明细表${detailNum}id`;
                        const replacementAst: jscodeshift.ExpressionStatement = {
                            type: 'ExpressionStatement',
                            expression: {
                                type: 'CallExpression',
                                callee: {
                                    type: 'MemberExpression',
                                    object: {type: 'Identifier', name: 'WfForm'},
                                    property: {type: 'Identifier', name: 'registerAction'},
                                },
                                arguments: [
                                    {
                                        type: 'BinaryExpression',
                                        left: {
                                            type: 'MemberExpression',
                                            object: {
                                                type: 'MemberExpression',
                                                object: {type: 'Identifier', name: 'window'},
                                                property: {
                                                    type: 'Identifier',
                                                    name: 'WeFormSDK'
                                                }
                                            },
                                            property: {
                                                type: 'Identifier',
                                                name: addFlag ? 'ACTION_ADDROW' : 'ACTION_DELROW'
                                            }
                                        },
                                        right: {
                                            type: 'Literal',
                                            value: detailMark
                                        },
                                        operator: '+'
                                    },
                                    {
                                        type: 'FunctionExpression',
                                        body: {
                                            type: 'BlockStatement',
                                            body: nodePath['node']['body']['body']
                                        },
                                        params: [{
                                            type: 'Identifier',
                                            name: nodePath['node']['params'][0] && nodePath['node']['params'][0]['name'] ? nodePath['node']['params'][0]['name'] : 'index_e10'
                                        }, {
                                            type: 'Identifier',
                                            name: 'data_e10'
                                        }]
                                    }
                                ]
                            }
                        };

                        nodePath.replace(replacementAst);
                        if (comments)
                            nodePath.node.comments = comments;

                    });

                }
            });
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('wFcustomAddFunTran-e:', e);
        }

        return code;
    }

    /**
     * 处理修改单个字段值方法中系统字段的转换
     * WfForm.changeFieldValue("field-1", {
     *     value: fieldvalue1,
     *     specialobj:fieldvalue2
     * });
     * @param code
     */
    wFChangeFieldValueTran = (code: string) => {

        try {

            const fieldMap: Record<string, string> = {
                "field-1": "RequestName",
                "field-2": "RequestLevel",
                "field-10": "RequestSecLevel" //目前标准不支持
            };

            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "changeFieldValue"}
                },
                arguments: []
            };

            //处理 WfForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (bindDetailFieldChangeEventCom.length > 0) {
                // 遍历匹配的对象并处理
                bindDetailFieldChangeEventCom.forEach(path => {
                    const argumentsTmp = path.value.arguments;
                    if (argumentsTmp?.[0] && argumentsTmp.length >= 2 &&
                        argumentsTmp[0].type == 'Literal' && argumentsTmp[1].type === "ObjectExpression"
                        && typeof argumentsTmp[0].value === 'string'
                        && fieldMap[argumentsTmp[0].value]) {

                        // 检查是否有第二个参数，并且第二个参数是一个对象字面量
                        // 遍历对象字面量的属性，查找 value 属性
                        let fieldName = fieldMap[argumentsTmp[0].value];

                        let fieldValue: any;
                        let fieldValueShowName: any;
                        argumentsTmp[1].properties.forEach(prop => {
                            // 确保属性对象具有 key 属性，并且其类型为 Identifier
                            if (prop.type === "Property" && prop.key && prop.key.type === "Identifier" && prop.key.name === "value") {
                                fieldValue = prop.value;

                            }
                            // 确保属性对象具有 key 属性，并且其类型为 Identifier
                            if (prop.type === "Property" && prop.key && prop.key.type === "Identifier" && prop.key.name === "specialobj") {
                                fieldValueShowName = prop.value;
                            }
                        });

                        if (fieldValue) {
                            let replacementAst: jscodeshift.CallExpression = {
                                type: "CallExpression",
                                callee: {
                                    type: "MemberExpression",
                                    object: {type: "Identifier", name: "wffpSdk"},
                                    property: {type: "Identifier", name: "changeSysFieldValue"}
                                },
                                arguments: [
                                    {type: "Literal", value: fieldName},
                                    fieldValue
                                ]
                            };

                            if (fieldName === 'RequestSecLevel') {
                                replacementAst = {
                                    type: "CallExpression",
                                    callee: {
                                        type: "MemberExpression",
                                        object: {type: "Identifier", name: "wffpSdk"},
                                        property: {type: "Identifier", name: "changeSecLevel"}
                                    },
                                    arguments: [
                                        fieldValue,
                                        fieldValueShowName
                                    ]
                                };
                            }

                            path.replace(replacementAst);
                        }
                    }
                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch
            (e) {
            if (window.console) console.log('wFcustomAddFunTran-e:', e);
        }

        return code;
    }


    /**
     * 针对于浏览按钮的数据值对象进行转换
     * WfForm.changeFieldValue("field11_2", {
     *     value: "2,3",
     *     specialobj:[
     *         {id:"2",name:"张三"},
     *         {id:"3",name:"李四"}
     *     ]
     * });
     *
     * 注意：
     * 1、value需要去掉
     * 2、specialobj由改成specialObj
     * 3、修改后其中的id还是需要手动调整的
     * @param code
     */
    wFBrowserObjectTran = (code: string) => {

        try {
            const root = jscodeshift(code);

            //处理 WfForm.bindDetailFieldChangeEvent
            const bindDetailFieldChangeEventCom = root
                .find(jscodeshift.ObjectExpression).filter(path => {
                    const hasValue = path.node.properties.some(
                        (prop: any) => prop.key?.name === 'value'
                    );
                    const hasSpecialObj = path.node.properties.some(
                        (prop: any) => prop.key?.name === 'specialobj'
                    );
                    return hasValue && hasSpecialObj;
                });

            if (bindDetailFieldChangeEventCom.length > 0) {
                // 遍历匹配的对象并处理
                bindDetailFieldChangeEventCom.forEach(path => {
                    // 移除 value 属性——无需移除，还需保留
                    path.node.properties = path.node.properties.filter(
                        (prop: any) => prop.key?.name !== 'value'
                    );

                    // console.log("path.node.properties", path.node.properties);

                    // 将 specialobj 改成 specialObj
                    path.node.properties = path.node.properties.map((prop: any) => {
                        if (prop.key?.name === 'specialobj') {
                            return jscodeshift.property(
                                'init',
                                jscodeshift.identifier('specialObj'),
                                prop['value']
                            );
                        }
                        return prop;
                    });
                    // console.log("path.node.properties", path.node.properties);
                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch
            (e) {
            if (window.console) console.log('wFcustomAddFunTran-e:', e);
        }

        return code;
    }

    /**
     * 处理 页面调用了_writeBackData的情况：
     * window._writeBackData("field110", 1, {id:"22",name:"lzy"},{replace:false,isSingle:false});    //多人力浏览按钮追加个值lzy
     * _writeBackData("field110", 1, {id:"22",name:"lzy"},{replace:false,isSingle:false});
     * 封装：
     * 会额外实现一个function
     * window._writeBackData = function(fieldMark, isMustInput, data, _options){}
     * @param code
     *
     * 该方法废弃，提示window._writeBackData未找到，
     */
    wfWwriteBackDataTran = (code: string) => {
        try {
            const root = jscodeshift(code);

            const sourceAsts = [{
                type: 'CallExpression',
                callee: {
                    type: 'MemberExpression',
                    object: {type: 'Identifier', name: 'window'},
                    property: {type: 'Identifier', name: '_writeBackData'}
                },
                arguments: []
            }, {
                type: 'CallExpression',
                callee: {
                    type: 'Identifier', name: '_writeBackData'
                },
                arguments: []
            }];
            let containFlag = false;
            sourceAsts.some(sourceAst => {
                const readyComs = root.find(jscodeshift.Node, sourceAst);
                if (readyComs.length > 0) {
                    containFlag = true;
                    return true;
                }
                return false;
            })

            if (containFlag) {
                code = code + "\r\n"
                    + "window._writeBackData = function(fieldMark, isMustInput, data, _options){\n" +
                    "    try{\n" +
                    "        if(typeof data !== \"object\")\n" +
                    "            return;\n" +
                    "        const _id = data.id || \"\";\n" +
                    "        const _name = data.name || \"\";\n" +
                    "        const defaultOptions = { replace: false, isSingle: true, ..._options };\n" +
                    "        let value = \"\";\n" +
                    "        let specialobj = new Array();\n" +
                    "        if(defaultOptions.isSingle || defaultOptions.replace){    //替换值\n" +
                    "            value = _id;\n" +
                    "            if(value !== \"\")\n" +
                    "                specialobj.push({id:_id, name:_name});\n" +
                    "        }else{  //追加值\n" +
                    "            if(_id === \"\" || _name === \"\")\n" +
                    "                return;\n" +
                    "            let curBrowValueArr = window.WeFormSDK.getWeFormInstance().getBrowserOptionEntity(fieldMark) || [];\n" +
                    "            let curBrowValue = window.WeFormSDK.getWeFormInstance().getFieldValue(fieldMark) || \"\";\n" +
                    "            value = (curBrowValue.value || \"\") + \",\" + _id;\n" +
                    "            specialobj = (curBrowValueArr || new Array()).concat([{id:_id, name:_name}]);\n" +
                    "        }\n" +
                    "        window.WeFormSDK.getWeFormInstance().changeFieldValue(fieldMark, {specialObj:specialobj});\n" +
                    "    }catch(e){\n" +
                    "        if(window.console)  console.log(\"_writeBackData Exception:\"+e);\n" +
                    "    }\n" +
                    "}";
            }

            return code;
        } catch (e) {
            if (window.console) console.log('jQueryReadyTran-e', e);
        }
        return code;
    }

    /**
     * 系统样式的Modal弹出框：
     *
     * 文本字段可编辑状态，当值为空显示默认灰色提示信息，鼠标点击输入时 提示消失
     * 限定条件：仅支持单行文本、整数、浮点数、千分位、多行文本字段(非html)字段类型；支持主字段及明细字 段
     *
     * setTextFieldEmptyShowContent:function(ﬁeldMark,showContent)
     *
     * 轉換後
     *
     * //fieldId 字段id；detailId 明细表id；detailNum 明细行 例：1、2、3；content 提示内容
     * ModeFormEcode.setTextFieldEmptyShowContent('973131124361216001','973130840859820033', 1,'显示内容')
     * @param code
     */
    wfSetTextFieldEmptyShowContentTran = (fromModule: string, code: string, fieldMap: any, detailTables: any) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "setTextFieldEmptyShowContent"}
                },
                arguments: []
            };

            const cubeShowModalMsgComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (cubeShowModalMsgComs.length > 0) {
                cubeShowModalMsgComs.forEach((nodePath: any) => {
                    nodePath['node']['callee']['object']['name'] = 'CommonFormEcode';
                    if (nodePath['node']['arguments'].length == 2 && nodePath['node']['arguments'][0]?.value?.indexOf('field') >= 0) {
                        let fieldName = nodePath['node']['arguments'][0]?.value?.replace('field', '');
                        let fieldId = fieldName.split('_')[0];
                        let detailId = "";
                        let detailNum = 1;
                        let content = nodePath['node']['arguments'][1];
                        if (fieldName.indexOf("_") > 0) {
                            fieldId = fieldMap[fieldName.split('_')[0]]?.e10_field_id || '字段ID：' + fieldId + '需转成E10字段ID';
                            detailId = fieldMap[fieldName.split('_')[0]]?.e10_sub_form_id || "";
                            detailNum = parseInt(fieldName.split('_')[1]) + 1;
                        } else {
                            fieldId = fieldMap[fieldName]?.e10_field_id || fieldId;
                        }

                        nodePath['node']['arguments'] = [
                            {
                                type: 'StringLiteral',
                                value: fieldId
                            },
                            {
                                type: 'StringLiteral',
                                value: detailId
                            },
                            {
                                type: 'NumericLiteral',
                                value: detailNum
                            },
                            content
                        ]

                    }
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfSetTextFieldEmptyShowContentTran-e:', e);
        }
        return code;
    }

    /**
     * 操作按钮置灰
     * WfForm.controlBtnDisabled(true); //操作按钮置灰
     *
     * 转换成
     * wffpSdk.controlBtnDisable('all', true) //置灰所有按钮
     *
     * @param code
     */
    wfControlBtnDisabledTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "controlBtnDisabled"}
                },
                arguments: []
            };

            const wfControlBtnDisabledComs = root
                .find(jscodeshift.CallExpression, sourceAst);

            if (wfControlBtnDisabledComs.length > 0) {
                wfControlBtnDisabledComs.forEach((nodePath: any) => {
                    let node = nodePath['node']['arguments']?.[0];
                    let argumentsTemp = nodePath['node']?.['arguments'] || [];
                    argumentsTemp.unshift({
                        type: "StringLiteral",
                        value: "all"
                    });
                    nodePath.replace({
                        type: "CallExpression",
                        callee: {
                            type: "MemberExpression",
                            object: {type: "Identifier", name: "wffpSdk"},
                            property: {type: "Identifier", name: "controlBtnDisable"}
                        },
                        arguments: argumentsTemp
                    })
                });
            }

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfSetTextFieldEmptyShowContentTran-e:', e);
        }
        return code;
    }

    /**
     * 改变字段属性
     * 处理系统字段部分！！！
     * WfForm.changeFieldAttr("field110", 1);
     * WfForm.changeFieldAttr("field-1", 1);
     *
     * 转换后：
     * 表单字段：
     * const weFormSdk = window.WeFormSDK.getWeFormInstance();
     * // 修改文本型-单行文本字段
     * const textFieldMark = weFormSdk.convertFieldNameToId('字段名');
     * weFormSdk.changeFieldAttr(textFieldMark,1);
     *
     * 系统字段：
     * //流程标题、紧急程度、密级不可为空 设置编辑即必填
     * //修改流程标题为只读
     * wffpSdk.changeSysFieldAttr('RequestName', 1);
     * //修改紧急程度为可编辑
     * wffpSdk.changeSysFieldAttr('RequestLevel', '2);
     * //修改密级为必填
     * wffpSdk.changeSysFieldAttr('RequestSecLevel', 3);
     *
     * 特殊处理二：
     * WfForm.changeFieldAttr("field110", "1");
     * 需要将第二个参数转成 1 E10不支持字符串
     *
     * @param code
     */
    wFChangeFieldAttrTran = (code: string) => {

        try {

            const fieldMap: Record<string, string> = {
                "field-1": "RequestName",
                "field-2": "RequestLevel",
                "field-10": "RequestSecLevel"
            };

            let root = jscodeshift(code);
            const sourceAst: jscodeshift.CallExpression = {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "changeFieldAttr"}
                },
                arguments: []
            };

            //处理 WfForm.changeFieldAttr
            const changeFieldAttrCom = root
                .find(jscodeshift.CallExpression, sourceAst);

            const fieldRegex = /^\d+$/;
            if (changeFieldAttrCom.length > 0) {
                // 遍历匹配的对象并处理
                changeFieldAttrCom.forEach(path => {
                    const argumentsTmp = path.value.arguments;
                    if (argumentsTmp.length >= 2
                        && argumentsTmp[1] && argumentsTmp[1].type === 'Literal'
                        && typeof argumentsTmp[1].value === 'string'
                        && fieldRegex.test(argumentsTmp[1].value)) {
                        argumentsTmp[1].value = Number(argumentsTmp[1].value);
                    }
                    if (argumentsTmp.length >= 2 &&
                        argumentsTmp[0].type == 'Literal'
                        && typeof argumentsTmp[0].value === 'string'
                        && fieldMap[argumentsTmp[0].value]) {

                        // 检查是否有第二个参数，并且第二个参数是一个对象字面量
                        // 遍历对象字面量的属性，查找 value 属性
                        let fieldName = fieldMap[argumentsTmp[0].value];

                        const targetAst = {
                            type: "CallExpression",
                            callee: {
                                type: "MemberExpression",
                                object: {type: "Identifier", name: "wffpSdk"},
                                property: {type: "Identifier", name: "changeSysFieldAttr"}
                            },
                            arguments: [
                                {type: "StringLiteral", value: fieldName},
                                argumentsTmp[1]
                            ]
                        }
                        path.replace(targetAst);

                    }
                });
            }
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch
            (e) {
            if (window.console) console.log('wFChangeFieldAttrTran-e:', e);
        }

        return code;
    }

    /**
     * 同时修改字段的值及显示属性
     * WfForm.changeSingleField("field110", {value:"修改的值"}, {viewAttr:"1"});   //修改值同时置为只读
     *
     * E10需要特殊处理，将viewAttr的属性改成 1
     * E10不支持显示属性是字符串
     *
     * @param code
     */
    wfChangeSingleFieldTrans = (code: string)=>{

        try {
            let root = jscodeshift(code);

            const sourceAst:any = {
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'WfForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'changeSingleField'
                    }
                }
            }

            //处理 WfForm.changeFieldAttr
            const changeSingleFieldCom = root
                .find(jscodeshift.CallExpression, sourceAst);

            changeSingleFieldCom.forEach(path => {
                const args = path.value.arguments;
                if (
                    args.length === 3 &&
                    args[2].type === 'ObjectExpression' &&
                    'properties' in args[2]
                ) {
                    args[2].properties.forEach((prop: any) => {
                        if (
                            prop.type === 'Property' &&
                            prop.key.type === 'Identifier' &&
                            prop.key.name === 'viewAttr' &&
                            prop.value.type === 'Literal' &&
                            typeof prop.value.value === 'string' &&
                            !isNaN(Number(prop.value.value))
                        ) {
                            prop.value = jscodeshift.literal(parseInt(prop.value.value));
                        }
                    });
                }
            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfChangeSingleFieldTrans-e:', e);
        }
        return code;
    }

    /**
     * 1、同时修改字段的值及显示属性
     * WfForm.delDetailRow("detail_1", "all");     //删除明细1所有行
     * WfForm.delDetailRow("detail_1", "3,6");     //删除明细1行标为3,6的行
     *
     * 2、同时修改字段的值及显示属性
     * WfForm.checkDetailRow("detail_2", "all"); //勾选明细2所有行
     * WfForm.checkDetailRow("detail_2", "", true);//清除明细2所有已选
     * WfForm.checkDetailRow("detail_2", "3,6", true); //清除明细2全部已选，再勾选行标为3,6的行
     * WfForm.checkDetailRow("detail_2", "7", false);
     * //保持已选记录，追加选中行标为7的行
     *
     * 3、控制明细行check框是否禁用勾选
     * WfForm.controlDetailRowDisableCheck("detail_1", "all", true); //明细1所有行check框置灰禁止选中
     * WfForm.controlDetailRowDisableCheck("detail_1", "1,2", false);
     * //明细1行标为1,2的行释放置灰，允许勾选
     *
     * 4、控制明细数据行的显示及隐藏
     * WfForm.controlDetailRowDisplay("detail_1", "3,5", true); //明细1行标为3,5的隐藏不显示
     * WfForm.controlDetailRowDisplay("detail_1", "all", false);
     * //明细1所有行不隐藏都显示
     *
     * 以上方法都是去处理第二个参数。
     * 改成：weFormSdk.getDetailRowIdByIndex(detailMark, 1);方式
     * @param code
     */
    wfDelDetailRowTrans = (code: string) => {

        try {
            let root = jscodeshift(code);

            // 定义正则表达式来匹配函数名
            const detailNumRegex = /^detail_\d*$/;
            const numberRegex = /^\d+$/;

            const sourceAsts: any[] = [{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'WfForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'delDetailRow'
                    }
                }
            },{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'WfForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'checkDetailRow'
                    }
                }
            },{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'WfForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'controlDetailRowDisableCheck'
                    }
                }
            },{
                callee: {
                    type: 'MemberExpression',
                    object: {
                        type: 'Identifier',
                        name: 'WfForm'
                    },
                    property: {
                        type: 'Identifier',
                        name: 'controlDetailRowDisplay'
                    }
                }
            }]

            sourceAsts.forEach(sourceAst =>{

                //处理 WfForm.changeFieldAttr
                const delDetailRowCom = root
                    .find(jscodeshift.CallExpression, sourceAst);

                delDetailRowCom.forEach(path => {
                    const args = path.value.arguments;
                    if (args.length >= 2 &&
                        args[0].type === 'Literal' &&
                        typeof args[0].value == 'string' &&
                        detailNumRegex.test(args[0].value) &&
                        args[1].type === 'Literal' &&
                        typeof args[1].value == 'string'
                    ) {
                        let detailNumber = args[0].value;
                        let rownumber = args[1].value;
                        let rownumbers = rownumber.split(",");
                        let quasis = [];
                        let expressions:any[] = [];
                        let isFirst = true;
                        rownumbers.forEach(rowTemp => {
                            if(numberRegex.test(rowTemp)) {
                                quasis.push(jscodeshift.templateElement.from({
                                    value: {cooked: isFirst?'':',', raw: isFirst?'':','},
                                    tail: false
                                }));
                                isFirst = false;
                                expressions.push(jscodeshift.callExpression.from({
                                    arguments: [
                                        jscodeshift.literal(detailNumber),
                                        jscodeshift.literal(Number(rowTemp)+1)
                                    ],
                                    callee: jscodeshift.memberExpression(
                                        jscodeshift.identifier('WfForm'),
                                        jscodeshift.identifier('getDetailRowIdByIndex')
                                    )
                                }));
                            }
                        })
                        quasis.push(jscodeshift.templateElement.from({
                            value: {cooked: '', raw: ''},
                            tail: true
                        }));

                        if(expressions.length>1)
                            args[1] = jscodeshift.templateLiteral(quasis, expressions);
                        else if (expressions.length==1)
                            args[1] = expressions[0]

                    }
                })

            })

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfDelDetailRowTrans-e:', e);
        }
        return code;
    }

    /**
     * 将无法替换的一些接口代码注释掉
     * 1、WfForm.getFieldCurViewAttr("field110_2");   //获取明细字段属性，1：只读、2：可编辑、3：必填；已办全部为只读；--已对应
     * 2、WfForm.controlDetailRowDisableCheck("detail_1", "all", true);     //明细1所有行check框置灰禁止选中——已经对应
     * 3、WfForm.controlBtnDisabled(true);    //操作按钮置灰——标准已支持
     * 4、WfForm.setTextFieldEmptyShowContent("field222_0",  "明细字段提示信息");   //需要结合接口5.9添加行事件一并使用
     * 5、WfForm.overrideBrowserProp("field111",{
     *     onBeforeFocusCheck: function(success, fail){}
     * });     //复写浏览按钮字段111的props
     * 6、WfForm.controlRadioPrintText("12580");  //单选框字段12580打印只显示文字
     * 6、WfForm.appendSignEditorBottomBar([
     *     React.createElement("div",{className:"wf-req-signbtn",children:"自定义按钮1"}),
     *     <div>自定义按钮2</div>
     * ]);//扩展签字意见输入框底部按钮
     *
     * @param code
     */
    wfUndefinedTran = (code: string) => {

        try {
            let root = jscodeshift(code);
            const sourceAsts: jscodeshift.CallExpression[] = [{
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "setTextFieldEmptyShowContent"}
                },
                arguments: []
            }, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "overrideBrowserProp"}
                },
                arguments: []
            }, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "controlRadioPrintText"}
                },
                arguments: []
            }, {
                type: "CallExpression",
                callee: {
                    type: "MemberExpression",
                    object: {type: "Identifier", name: "WfForm"},
                    property: {type: "Identifier", name: "appendSignEditorBottomBar"}
                },
                arguments: []
            }];

            sourceAsts.forEach(sourceAst => {
                const wfUndefinedComs = root
                    .find(jscodeshift.CallExpression, sourceAst);

                if (wfUndefinedComs.length > 0) {
                    wfUndefinedComs.forEach((nodePath) => {
                        // nodePath.replace(jscodeshift.commentBlock(` 无法替换部分：${jscodeshift(nodePath).toSource()} `))
                        nodePath.node.comments = [
                            jscodeshift.commentLine(`无法替换部分：${jscodeshift(nodePath).toSource()},需要手动调整！`)
                        ]
                    });
                }
            });

            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();

        } catch (e) {
            if (window.console) console.log('wfUndefinedTran-e:', e);
        }
        return code;
    }

    /**
     * 对于一些有特殊含义的代码进行替换
     * jQuery("#requestid").val() = > wffpSdk.getBaseParam().requestId
     */
    wfSpecialExpTran = (code: string) => {
        try {
            const root = jscodeshift(code);

            // 定义一个映射关系，将 A 替换为 A1
            const expressionMap: any = {
                'CallExpression': {
                    'jQuery("#requestid").val()': 'wffpSdk.getBaseParam().requestId',
                    'jQuery(\'#requestid\').val()': 'wffpSdk.getBaseParam().requestId',
                    'jQuery("input[name=\'requestid\']").val()': 'wffpSdk.getBaseParam().requestId',
                    'jQuery(\'input[name="requestid"]\').val()': 'wffpSdk.getBaseParam().requestId',
                    'jQuery("input[name=\'workflowid\']").val()': 'wffpSdk.getBaseParam().workflowId',
                    'jQuery(\'input[name="workflowid"]\').val()': 'wffpSdk.getBaseParam().workflowId',
                    'jQuery("input[name=\'nodeid\']").val()': 'wffpSdk.getBaseParam().userCurrentNodeId',
                    'jQuery(\'input[name="nodeid"]\').val()': 'wffpSdk.getBaseParam().userCurrentNodeId',
                    'jQuery("#requestname").val()': 'wffpSdk.getSysFieldValue(\'RequestName\')',
                    'jQuery(\'#requestname\').val()': 'wffpSdk.getSysFieldValue(\'RequestName\')'
                    // 添加更多的映射关系...
                },
                'MemberExpression': {
                    'document.getElementById("requestid").value': 'wffpSdk.getBaseParam().requestId',
                    'document.getElementById("workflowid").value': 'wffpSdk.getBaseParam().workflowId',
                    'document.getElementById("nodeid").value': 'wffpSdk.getBaseParam().nodeid',
                    'document.getElementById("requestname").val': 'wffpSdk.getSysFieldValue(\'RequestName\')'
                    // 添加更多的映射关系...
                }
            };

            for (let expressionMapKey in expressionMap) {
                // 遍历 AST，查找并替换表达式
                root.find(jscodeshift[expressionMapKey]).forEach(path => {
                    // 没办法了不知能先让typescript忽略错误，不然没法进行了开发了
                    // @ts-ignore
                    const expression = expressionMap[expressionMapKey][jscodeshift(path.node).toSource()];
                    if (expression) {
                        path.replace(jscodeshift.identifier(expression)); // 替换表达式
                    }
                });
            }


            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch (e) {
            if (window.console) console.log('jQueryReadyTran-e', e);
        }
        return code;

    }

}

const instance = new TransRuleForWorkflow();

export default instance;