import jscodeshift from 'jscodeshift';
import * as API_LIST from '../../../api/TransCodeApi';


export class TransRuleEnd {

    transform = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {

        try {
            if (fromModule === 'workflow' || fromModule === 'cube') {
                code = this.checkCustomizeFormat(fromModule, code);
                code = this.codeFieldIdFormat(fromModule, code, fieldMap, detailTables);
                code = this.wFObjectTran(fromModule, code, fieldMap, detailTables);
                code = this.detailNumFormat(fromModule, code, fieldMap, detailTables);
                code = this.objectFieldIdFormat(fromModule, code, fieldMap, detailTables);
                code = this.codeFormatLastTime(code);
            }
            code = this.codeFormat(code);
        } catch (e) {
            if (window.console) console.log("TransRuleEnd.transform", e);
        }
        return code;

    }

    /**
     * 代码格式化
     * @param code
     */
    codeFormat = (code: string) => {
        try {
            // const root = jscodeshift(code);
            // //处理 function checkCustomize()
            // const expressionStatementComs = root
            //     .find(jscodeshift.ExpressionStatement);
            //
            // if (expressionStatementComs.length > 0) {
            //     expressionStatementComs.forEach((nodePath: any) => {
            //         console.log("nodePath",nodePath.node);
            //     })
            // }
        } catch (e) {
            if (window.console) console.log('e', e);
        }
        return code;
    }

    /**
     * 检查是否有
     * 1、function checkCustomize
     * 2、checkCustomize = function() {}
     * 3、window.checkCustomize = function() {}
     * 如果存在则附加提交前绑定事件
     * // 获取表单示例
     *     const weFormSdk = window.WeFormSDK.getWeFormInstance();
     *     // 注册提交事件
     *     weFormSdk.registerCheckEvent(window.WeFormSDK.OPER_SUBMIT, (successFn: Function, failFn: Function)=>{
     *         // ...执行自定义逻辑
     *         // 如果是移动端，则向下调用，如果非移动端则阻塞调用
     *         window.WeFormSDK.isModule() ? successFn() : failFn();
     *     });
     * @param code
     */
    checkCustomizeFormat = (fromModule: String, code: string) => {
        try {
            var endAppend = '';
            const root = jscodeshift(code);

            //处理 function checkCustomize()
            const checkCustomizeFunctionComs = root
                .find(jscodeshift.FunctionDeclaration)
                .filter((path: any) => {
                    return path.node && path.node.id && path.node.id.name === 'checkCustomize';
                });

            //处理 checkCustomize,window.checkCustomize
            const checkCustomizeExpressionComs = root
                .find(jscodeshift.ExpressionStatement)
                .filter((path: any) => {
                    return path.node && path.node.expression && path.node.expression.left
                        && (path.node.expression.left.name === 'checkCustomize'
                            || (path.node.expression.left.property && path.node.expression.left.property.name === 'checkCustomize'));
                });

            // 找到所有的 checkCustomize = function(){} 赋值语句
            root.find(jscodeshift.AssignmentExpression, {
                left: {
                    type: 'Identifier',
                    name: 'checkCustomize',
                },
                right: {},
            }).forEach(path => {
                if (path.node.right &&
                    (path.node.right.type === 'ArrowFunctionExpression'
                        || path.node.right.type === 'FunctionExpression')) {
                    // 将 checkCustomize 改为 window.checkCustomize
                    const assignment = path.node;
                    assignment.left = jscodeshift.memberExpression(
                        jscodeshift.identifier('window'),
                        jscodeshift.identifier('checkCustomize')
                    );

                    // 替换原来的赋值语句
                    jscodeshift(path).replaceWith(assignment);
                }
            });

            // 找到所有的 function checkCustomize(){} 定义
            root.find(jscodeshift.FunctionDeclaration, {
                id: {
                    type: 'Identifier',
                    name: 'checkCustomize',
                },
            }).forEach(path => {
                // 创建一个赋值表达式：window.checkCustomize = function checkCustomize(){}
                const assignment = jscodeshift.expressionStatement(
                    jscodeshift.assignmentExpression(
                        '=',
                        jscodeshift.memberExpression(
                            jscodeshift.identifier('window'),
                            jscodeshift.identifier('checkCustomize')
                        ),
                        jscodeshift.functionExpression(
                            jscodeshift.identifier('checkCustomize'),
                            path.node.params,
                            path.node.body
                        )
                    )
                );

                // 将原来的 function checkCustomize(){} 替换成赋值表达式
                jscodeshift(path).replaceWith(assignment);
            });

            //处理 onload,window.onload
            const onloadExpressionComs = root
                .find(jscodeshift.ExpressionStatement)
                .filter((path: any) => {
                    return path.node && path.node.expression && path.node.expression.left
                        && (path.node.expression.left.name === 'onload'
                            || (path.node.expression.left.property && path.node.expression.left.property.name === 'onload'));
                });

            // 找到所有的 onload = function(){} 赋值语句
            root.find(jscodeshift.AssignmentExpression, {
                left: {
                    type: 'Identifier',
                    name: 'onload',
                },
                right: {
                    type: 'FunctionExpression',
                },
            }).forEach(path => {
                // 将 checkCustomize 改为 window.checkCustomize
                const assignment = path.node;
                assignment.left = jscodeshift.memberExpression(
                    jscodeshift.identifier('window'),
                    jscodeshift.identifier('onload')
                );
            });

            let readyFunction = '';
            if (checkCustomizeFunctionComs.length > 0
                || checkCustomizeExpressionComs.length > 0) {
                if (fromModule === 'workflow')
                    readyFunction = readyFunction + "  // 提交事件执行自定义函数\n" +
                        "  wffpSdk.registerInterceptEvent('BeforeSubmit', (successFn, failFn)=>{\n" +
                        "    if(window.checkCustomize){\n" +
                        "      if(!window.checkCustomize()){\n" +
                        "        failFn();\n" +
                        "        return;\n" +
                        "      }\n" +
                        "    }\n" +
                        "    successFn();\n" +
                        "  });\n";
                if (fromModule === 'cube')
                    readyFunction = readyFunction + "  // 提交事件执行自定义函数\n" +
                        "  var checkFunc_E10 = (resolve, info) => {\n" +
                        "    if(window.checkCustomize){\n" +
                        "      if(!window.checkCustomize()){\n" +
                        "      resolve(false);\n" +
                        "      return;\n" +
                        "      }\n" +
                        "    }\n" +
                        "    resolve(true);\n" +
                        "  }\n" +
                        "  ebdFormSdk.checkCustomize(checkFunc_E10);  //拦截表单保存前验证\n";
            }

            if (onloadExpressionComs.length > 0) {
                readyFunction = readyFunction + "  // 执行js自带的onload \n" +
                    "  window.onload();\n ";
            }

            if (readyFunction.length > 0) {
                endAppend = "\n window.ebuilderSDK.getPageSDK().on(\"formReady\", function(){\n" + readyFunction + "})";
            }

            return root.toSource() + endAppend;
        } catch (e) {
            if (window.console) console.log('e', e);
        }
        return code;
    }

    /**
     * 将E9字段id转换成E10字段id
     * "field13650_2,field13650_2"
     * @param code
     */
    codeFieldIdFormat = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {
        try {
            const root = jscodeshift(code);

            //处理 function checkCustomize()
            const expressionStatementComs = root
                .find(jscodeshift.Literal).filter((path: any) => {
                    return path.node && path.node['value'] && typeof path.node['value'] === "string" &&
                        path.node['value'] != '#field' && path.node['value'] != 'field' &&
                        (path.node['value'].indexOf('#field') === 0 || path.node['value'].indexOf('field') === 0);
                });

            if (expressionStatementComs.length > 0) {

                // 定义正则表达式来匹配函数名
                const fieldRegex = /^\d*$/;
                // 定义正则表达式来匹配函数名
                const fieldNameRegex = /^\d+$/;
                expressionStatementComs.forEach((nodePath: any) => {
                    const {node} = nodePath;
                    let e9fieldids = node.value;
                    let needTemplateFlag = false;
                    if (e9fieldids && e9fieldids.indexOf("_") > 0) {//需要转成`field${aaa}`的表达式
                        needTemplateFlag = true;
                    } else {//还是保持字符串

                    }
                    let e9fieldidArr = e9fieldids.split(',');
                    let quasis = [];
                    let expressions = [];
                    for (let i = 0; i < e9fieldidArr.length; i++) {
                        let olde9fieldid = e9fieldidArr[i];
                        let hashFlag = olde9fieldid.indexOf('#') >= 0;//就算是#开头，也替换掉，E10上并没有相关dom了
                        olde9fieldid = olde9fieldid.trim().replace('#', '').replace('field', '');

                        let actualFieldId = olde9fieldid;
                        let tailAppend = '';
                        let detailRowNum = -1;
                        if (olde9fieldid.indexOf('_') >= 0) {
                            actualFieldId = olde9fieldid.split('_')?.[0];
                            tailAppend = olde9fieldid.split('_')?.[1];
                            if (tailAppend.length > 0 && fieldNameRegex.test(tailAppend)) {
                                detailRowNum = parseInt(tailAppend) + 1;
                            }
                        }
                        if (actualFieldId.length > 0 && fieldRegex.test(actualFieldId)) {
                            if (fieldMap[actualFieldId]
                                && fieldMap[actualFieldId].e10_field_id) {
                                if (olde9fieldid.indexOf('_') > 0) {
                                    let temResult = (i == 0 ? '' : ',') + (hashFlag ? '#' : '') + "field" + fieldMap[actualFieldId].e10_field_id + "_";

                                    if (detailRowNum > -1 && fieldMap[actualFieldId]?.e10_sub_form_id.length > 0) {
                                        expressions.push(jscodeshift.callExpression.from({
                                            arguments: [
                                                jscodeshift.literal(fieldMap[actualFieldId]?.e10_sub_form_id),
                                                jscodeshift.literal(detailRowNum)
                                            ],
                                            callee: jscodeshift.memberExpression(
                                                jscodeshift.identifier(('cube' === fromModule ? 'ModeForm' : 'WfForm')),
                                                jscodeshift.identifier('getDetailRowIdByIndex')
                                            )
                                        }));
                                    } else {
                                        temResult = temResult + tailAppend
                                        expressions.push(jscodeshift.stringLiteral(""));//占位
                                    }
                                    quasis.push(jscodeshift.templateElement.from({
                                        value: {cooked: '', raw: temResult},
                                        tail: false
                                    }));
                                } else {
                                    if (needTemplateFlag) {
                                        let temResult = (i == 0 ? '' : ',') + (hashFlag ? '#' : '') + "field" + fieldMap[actualFieldId].e10_field_id;
                                        quasis.push(jscodeshift.templateElement.from({
                                            value: {
                                                cooked: null,
                                                raw: temResult
                                            },
                                            tail: false
                                        }));
                                        expressions.push(jscodeshift.stringLiteral(""));//占位
                                    } else {
                                        e9fieldidArr[i] = (hashFlag ? '#' : '') + "field" + fieldMap[actualFieldId].e10_field_id;
                                    }
                                }
                            } else {
                                if (needTemplateFlag) {
                                    let temResult = `字段${e9fieldidArr[i]}需要替换成字段id`;
                                    quasis.push(jscodeshift.templateElement.from({
                                        value: {
                                            cooked: null,
                                            raw: temResult
                                        },
                                        tail: false,
                                    }));
                                    expressions.push(jscodeshift.stringLiteral(""));//占位

                                } else {
                                    e9fieldidArr[i] = `字段${e9fieldidArr[i]}需要替换成字段id`;
                                }
                            }
                        } else {
                            if (needTemplateFlag) {
                                let temResult = `${e9fieldidArr[i]}`;
                                quasis.push(jscodeshift.templateElement.from({
                                    value: {
                                        cooked: null,
                                        raw: temResult
                                    },
                                    tail: false,
                                }));
                                expressions.push(jscodeshift.stringLiteral(""));//占位
                            } else {
                                e9fieldidArr[i] = `${e9fieldidArr[i]}`;
                            }
                        }
                    }
                    //将改造后的字段赋值给node节点
                    // nodePath.replace(jscodeshift.literal(e9fieldidArr.join(',')))

                    if (needTemplateFlag) {
                        nodePath.replace(jscodeshift.templateLiteral(quasis, expressions))
                    } else {
                        nodePath.replace(jscodeshift.literal(e9fieldidArr.join(',')))
                    }
                })
            }
            return root.toSource();
        } catch (e) {
            if (window.console) console.log('e', e);
        }
        return code;
    }

    /**
     * 处理对象数据的key是field***开头的情况：
     * WfForm.changeMoreField({
     *     field110:{value:"修改后的值"},
     *     field111:{value:"2,3",specialobj:[
     *         {id:"2",name:"张三"},{id:"3",name:"李四"}
     *     ]},
     * },{
     *     field110:{viewAttr:2},
     *     field111:{viewAttr:3},
     * });
     *
     * 这边的key是field110 不是 "field110"
     * @param code
     */
    wFObjectTran = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {

        try {
            const root = jscodeshift(code);

            // 定义正则表达式来匹配函数名
            const fieldNameRegex = /^field\d+$/;
            const detailFieldNameRegex = /^field\d+_\d+$/;
            //处理 WfForm.bindDetailFieldChangeEvent
            const wFObjectCom = root
                .find(jscodeshift.ObjectExpression).filter(path => {
                    // 匹配所有以 "field" 开头的键
                    const hasValue = path.value.properties.some((property: any) => {
                            if (property.key?.type === "Identifier" && fieldNameRegex.test(property.key?.name)) {
                                return true;
                            }
                            if (property.key?.type === "Identifier" && detailFieldNameRegex.test(property.key?.name)) {
                                return true;
                            }
                            return false;
                        }
                    );
                    return hasValue;
                });

            wFObjectCom.forEach((path: any) => {
                path.value.properties && path.value.properties.some((property: any) => {
                    if (property.key?.type === "Identifier" && fieldNameRegex.test(property.key?.name)) {
                        let actualFieldId = property.key?.name.replace("field", "");
                        if (fieldMap[actualFieldId]
                            && ((fieldMap[actualFieldId].size > 0) || (Object.keys(fieldMap[actualFieldId]).length > 0))
                            && fieldMap[actualFieldId].e10_field_id) {
                            property.key.name = "field" + fieldMap[actualFieldId].e10_field_id;
                        }
                    }
                    if (property.key?.type === "Identifier" && detailFieldNameRegex.test(property.key?.name)) {
                        let actualFieldId = property.key?.name.replace("field", "").split("_")?.[0];
                        let append = property.key?.name.replace("field", "").split("_")?.[1];
                        let actualRownum = parseInt(append) + 1;
                        if (fieldMap[actualFieldId]
                            && ((fieldMap[actualFieldId].size > 0) || (Object.keys(fieldMap[actualFieldId]).length > 0))
                            && fieldMap[actualFieldId].e10_field_id) {

                            property.key.name = "[`field" + fieldMap[actualFieldId].e10_field_id + "_" +
                                "${" + ('cube' === fromModule ? 'ModeForm' : 'WfForm') + ".getDetailRowIdByIndex('" + fieldMap[actualFieldId].e10_sub_form_id + "'," + actualRownum + ")}`]";
                            // 在字段前面添加注释
                            // property.comments = [jscodeshift.commentLine(`明细行${append}需要手动转成E10rowid。`, true)];
                        }
                    }
                })
            })
            //检测代码转换后的代码是否正常
            if (jscodeshift(root.toSource()))
                return root.toSource();
        } catch
            (e) {
            if (window.console) console.log('wFcustomAddFunTran-e:', e);
        }

        return code;
    }

    /**
     * 将E9字段id转换成E10字段id
     * @param code
     */
    detailNumFormat = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {
        try {
            const root = jscodeshift(code);

            if ('cube' !== fromModule)
                fromModule = 'workflow';

            // 定义正则表达式来匹配函数名
            const functionNameRegex = [/^detail_\d*$/];

            functionNameRegex.forEach((functionNameRegex: any) => {
                //处理 WfForm.bindDetailFieldChangeEvent
                const bindDetailFieldChangeEventCom = root
                    .find(jscodeshift.Literal).filter((nodePath: any) => {
                        const detailTable = nodePath.node?.value;
                        return functionNameRegex.test(detailTable);
                    });

                if (bindDetailFieldChangeEventCom.length > 0) {
                    bindDetailFieldChangeEventCom.forEach((nodePath: any) => {
                        const {node} = nodePath;
                        let detailTable = node.value?.replace('detail_', '');//detail_1
                        if (detailTables[detailTable]) {
                            //将改造后的字段赋值给node节点
                            nodePath.replace(jscodeshift.literal(detailTables[detailTable]))
                        }
                    })
                }

            })


            return root.toSource();

        } catch (e) {

        }
        return code;
    }

    /**
     * 将E9字段id转换成E10字段id
     * 针对于：
     * WfForm.changeMoreField({
     *     field110:{value:"修改后的值"},
     *     field111:{value:"2,3",specialobj:[
     *         {id:"2",name:"张三"},{id:"3",name:"李四"}
     *     ]}
     * },{
     *     field110哈哈哈:{viewAttr:2},
     *     field111:{viewAttr:3}
     * });
     * @param code
     */
    objectFieldIdFormat = (fromModule: String, code: string, fieldMap: any, detailTables: any) => {
        try {
            const root = jscodeshift(code);

            // 定义正则表达式来匹配函数名
            const functionNameRegex = /^field\d*$/;
            //处理 function checkCustomize()
            const expressionStatementComs = root
                .find(jscodeshift.ObjectExpression).filter((path: any) => {
                    const hasValue = path.node.properties.some(
                        (prop: any) => functionNameRegex.test(prop.key?.name)
                    );
                    return hasValue;
                });

            if (expressionStatementComs.length > 0) {
                expressionStatementComs.forEach((nodePath: any) => {

                    nodePath.node.properties.map((prop: any) => {
                        if (functionNameRegex.test(prop.key?.name)
                            && fieldMap[prop.key?.name.replace('field', '')]
                            && fieldMap[prop.key?.name.replace('field', '')].e10_field_id) {
                            return jscodeshift.property(
                                'init',
                                jscodeshift.identifier('field' + fieldMap[prop.key?.name.replace('field', '')].e10_field_id),
                                prop['value']
                            );
                        }
                        return prop;
                    });

                })
            }
            return root.toSource();
        } catch (e) {
            if (window.console) console.log('e', e);
        }
        return code;
    }

    /**
     * 代码格式化
     * @param code
     */
    codeFormatLastTime = (code: string) => {
        try {
            code = code.replaceAll("${\"\"}", "");
        } catch (e) {
            if (window.console) console.log('codeFormatLastTime-e', e);
        }
        return code;
    }
}

const instance = new TransRuleEnd();

export default instance;