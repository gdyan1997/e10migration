import jscodeshift from 'jscodeshift';
import TransRulePre from "./rules/TransRulePre";
import TransRuleForWorkflow from "./rules/TransRuleForWorkflow";
import TransRuleForJs from "./rules/TransRuleForJs";
import TransRuleForJQuery from "./rules/TransRuleForJQuery";
import TransRuleForFormmode from "./rules/TransRuleForFormmode";
import TransRuleForAntd from "./rules/TransRuleForAntd";
import TransRuleForEccom from "./rules/TransRuleForEccom";
import TransRuleForEcodeSDK from "./rules/TransRuleForEcodeSDK";
import TransRuleEnd from "./rules/TransRuleEnd";
import TransRuleForWeaJs from "./rules/TransRuleForWeaJs";


export class WeaJsCodeShift {

    /**
     * 检测转换后的代码是否还包含：
     * 1、包含jQuery("")
     * 2、包含document.
     * 3、包含.html 和 .jsp
     * 4、包含了weappUtils.request()
     *
     * @param code
     */
    verifyJS = (code: string, hasSrcScript: boolean) => {
        let result: any = {data: "", detail: []};
        try {
            //js代码为空
            if (code === '' || code.trim() === '') {
                result.data = "无需转换";
                result.detail.push("JS代码为空！");
                return result;
            }
            if (hasSrcScript) {
                result.detail.push("存在js引入，需要手动调整!");
            }
            //非正常js代码
            if (!TransRulePre.checkNormalCode(code)) {
                result.data = '代码异常';
                result.detail.push("代码结构异常，无法解析，需要手动调整！");
                return result;
            }

            // 定义一个映射关系，将 A 替换为 A1
            const expressionMap: any = {
                //有调用的需要使用CallExpression类型
                'CallExpression': {
                    'jQuery': '存在jQuery语法，需要确认可用性！',
                    'weappUtils.request': '存在request请求，请求接口需要重新开发！',
                    'window.weappUtils.request': '存在request请求，请求接口需要重新开发！'
                    // 添加更多的映射关系...
                },
                'MemberExpression': {
                    'document': '存在document操作，需要确认可用性！',
                    'weaJs': '存在E9接口weaJs，需要手动调整！',
                    'window.weaJs': '存在E9接口weaJs，需要手动调整！'
                    // 添加更多的映射关系...
                }
            };

            const insertedMessages = new Set();  // 用于跟踪已经插入的消息

            const root = jscodeshift(code);

            for (const expressionMapKey in expressionMap) {
                root.find(jscodeshift[expressionMapKey]).forEach(path => {
                    const fullExpressionName = this.getFullExpressionName(path.node);
                    if (fullExpressionName && expressionMap[expressionMapKey][fullExpressionName]) {
                        const message = expressionMap[expressionMapKey][fullExpressionName];
                        if (message && !insertedMessages.has(message)) {
                            result.detail.push(message);
                            insertedMessages.add(message);  // 添加到集合中，避免重复插入
                        }
                    }
                });
            }

            let jspFlag = false;
            let htmlFlag = false;
            root.find(jscodeshift.Literal)
                .forEach(path => {
                    const value = path.value.value;
                    if (typeof value === 'string') {
                        if (value.includes('.jsp')) {
                            jspFlag = true;
                            return false; // 跳出循环
                        }
                        if (value.includes('.html')) {
                            htmlFlag = true;
                            return false; // 跳出循环
                        }
                    }
                })

            if (jspFlag || htmlFlag)
                result.detail.push("存在JSP页面或者Html页面，需要重新开发！");

            // if (jQuerySize > 0
            //     || documentSiz > 0
            //     || requestSize > 0
            //     || jspFlag
            //     || htmlFlag) {
            //     return "无法完全转换";
            // }

            if (!result.data && result.detail.length > 0) {
                result.data = "无法完全转换";
            }

            if(result.data)
                return result;
        } catch (e) {
            if (window.console) console.log("WeaJsCodeShift.verifyJS", e);// 异常将被传递到最外层
            result.data = '代码异常';
            return result;
        }
        result.data = '已转换需确认';
        return result;
    }

    getFullExpressionName(node: any): string | null {
        if(node) {
            if (node.type === 'Identifier') {
                return node.name;
            } else if (node.type === 'MemberExpression') {
                return `${this.getFullExpressionName(node.object)}.${this.getFullExpressionName(node.property)}`;
            } else if (node.type === 'CallExpression') {
                return this.getFullExpressionName(node.callee);
            }
        }
        return null;
    }

    /**
     * 转换代码
     * @param fromModule workflow,cube
     * @param browserData 流程id or 建模模块id
     * @param code 实际代码
     * @param fieldMap 字段信息 ｛'feild111':{e10_field_id:'11111',e10_form_id:'11111',e10_sub_form_id:'11111'}｝
     */
    transform = (fromModule: string, code: string, fieldMap: any = {}, detailTables: any = {}, cubeButton: string = '') => {

        try {
            if (code === '' || code.trim() === '' || !TransRulePre.checkNormalCode(code)) return code;
            code = TransRulePre.transform(fromModule, code);
            code = TransRuleForJs.transform(fromModule, code, fieldMap, detailTables);
            code = TransRuleForJQuery.transform(fromModule, code, fieldMap, detailTables);
            code = TransRuleForWorkflow.transform(fromModule, code, fieldMap, detailTables);
            code = TransRuleForFormmode.transform(fromModule, code, fieldMap, detailTables, cubeButton);
            code = TransRuleForAntd.transform(fromModule, code);
            code = TransRuleForEccom.transform(fromModule, code);
            code = TransRuleForEcodeSDK.transform(code);
            code = TransRuleForWeaJs.transform(code);
            code = TransRuleEnd.transform(fromModule, code, fieldMap, detailTables);
        } catch (e) {
            if (window.console) console.log("WeaJsCodeShift.transform", e);// 异常将被传递到最外层
        }

        return code;
    }

}

const instance = new WeaJsCodeShift();
// 挂载 transform 方法到 window 对象上
(window as any).transformCode = instance.transform;
// 挂载 verifyJS 方法到 window 对象上
(window as any).verifyJSCode = instance.verifyJS;
export default instance;