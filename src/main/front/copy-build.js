// const fs = require('fs-extra');
// const path = require('path');
//
// // 定义源文件夹和目标文件夹路径
// const sourceDir = path.resolve(__dirname, 'build');
// const targetDir = path.resolve(__dirname, '../webapp/main');
//
// // 清空目标文件夹
// fs.emptyDirSync(targetDir);
//
// // 复制 build 文件夹到 webapp 文件夹
// fs.copy(sourceDir, targetDir, (err) => {
//     if (err) {
//         console.error('Error copying build directory:', err);
//     } else {
//         console.log('Build directory copied to webapp successfully!');
//     }
// });

const fs = require('fs-extra');
const path = require('path');

// 定义源文件夹和目标文件夹路径
const sourceDir = path.resolve(__dirname, 'build');
const targetDir = path.resolve(__dirname, '../resources/public');

// 清空目标文件夹
fs.emptyDirSync(targetDir);

// // 复制 build 文件夹到 ../resources/static 文件夹
// fs.copy(sourceDir, targetDir, (err) => {
//     if (err) {
//         console.error('Error copying build directory:', err);
//     } else {
//         console.log('Build directory copied to webapp successfully!');
//     }
// });
// 复制 build 文件夹到 ../resources/static 文件夹，并强制覆盖已存在的文件
try {
    fs.copySync(sourceDir, targetDir, { overwrite: true });
    console.log('Build directory copied to webapp successfully!');
} catch (err) {
    console.error('Error copying build directory:', err);
}
// // 定义源文件夹和目标文件夹路径
// const sourceDir = path.resolve(__dirname, 'build');
// const targetDir = path.resolve(__dirname, '../resources/static');
//
// const targetDirStatic = path.resolve(__dirname, '../resources/static/static');
// // 清空目标文件夹
// fs.emptyDirSync(targetDirStatic);
//
// // 复制 build 文件夹到 ../resources/static 文件夹
// fs.copy(sourceDir, targetDir, (err) => {
//     if (err) {
//         console.error('Error copying build directory:', err);
//     } else {
//         console.log('Build directory copied to webapp successfully!');
//     }
// });
//
//
// // 源文件和目标文件路径
// const sourceFile = path.join(__dirname, 'build', 'index.html');
// const targetFile = path.join(__dirname, '../webapp', 'index.html');
//
// // 复制文件
// fs.copyFileSync(sourceFile, targetFile);
//
// console.log(`Copied ${sourceFile} to ${targetFile}`);
//
//
// console.log('Build directory copied to webapp successfully!');
