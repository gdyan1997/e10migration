const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');
const { override, addBabelPreset, addBabelPlugin, addPostcssPlugins } = require('customize-cra');

module.exports = override(
    (config) => {
        if (!config.plugins) {
            config.plugins = [];
        }

        // 添加 NodePolyfillPlugin
        config.plugins.push(new NodePolyfillPlugin());

        config.resolve.fallback = {
            ...config.resolve.fallback,
            fs: false,  // 确保禁用 fs 模块的解析
            net: false, // 确保禁用 net 模块的解析
            module: false, // 确保禁用 'module' 模块的解析
            // 如果还有其他模块报错，可以在这里继续添加
        };

        console.log('Webpack config:', config);
        return config;
    },
    // 添加 Babel 预设和插件
    addBabelPreset([
        "@babel/preset-env",
        {
            targets: {
                ie: "11",
                chrome: "81"
            },
            useBuiltIns: "entry",
            corejs: 3,
            loose: false  // 确保 loose 设置为 false
        }
    ]),
    addBabelPreset("@babel/preset-react"),
    addBabelPreset("@babel/preset-typescript"),
    addBabelPlugin('@babel/plugin-transform-arrow-functions'),
    addBabelPlugin('@babel/plugin-proposal-class-properties'),
    addBabelPlugin('@babel/plugin-proposal-private-methods'),
    addBabelPlugin('@babel/plugin-proposal-private-property-in-object'),
    addPostcssPlugins([
        require('autoprefixer')
    ])
);
