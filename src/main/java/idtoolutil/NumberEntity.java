package idtoolutil;

public class NumberEntity {
    //E9表名
    private String e9tablename;
    //E10表名
    private String e10tablename;
    //表编号
    private int number;
    //租户
    private String tenant_key;
    //租户标识
    private int tenantkeynumber;
    //流水设置字段（多功能字段）
    private String genidtype;

    public NumberEntity() {
    }

    public NumberEntity(String e9tablename, String e10tablename, int number, String tenant_key, int tenantkeynumber, String genidtype) {
        this.e9tablename = e9tablename;
        this.e10tablename = e10tablename;
        this.number = number;
        this.tenant_key = tenant_key;
        this.tenantkeynumber = tenantkeynumber;
        this.genidtype = genidtype;
    }

    public String getE9tablename() {
        return e9tablename;
    }

    public void setE9tablename(String e9tablename) {
        this.e9tablename = e9tablename;
    }

    public String getE10tablename() {
        return e10tablename;
    }

    public void setE10tablename(String e10tablename) {
        this.e10tablename = e10tablename;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTenant_key() {
        return tenant_key;
    }

    public void setTenant_key(String tenant_key) {
        this.tenant_key = tenant_key;
    }

    public int getTenantkeynumber() {
        return tenantkeynumber;
    }

    public void setTenantkeynumber(int tenantkeynumber) {
        this.tenantkeynumber = tenantkeynumber;
    }

    public String getGenidtype() {
        return genidtype;
    }

    public void setGenidtype(String genidtype) {
        this.genidtype = genidtype;
    }
}
