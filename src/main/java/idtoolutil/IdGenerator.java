package idtoolutil;
/*
 * Copyright 2016-2020 the original author.All rights reserved.
 * Kingstar(honeysoft@126.com)
 * The license,see the LICENSE file.
 */


import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import weaver.general.Util;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;



/**
 * @author Kingstar
 * @since 1.8
 */
public class IdGenerator {
    protected static ReentrantLock lockPool = new ReentrantLock();

    private static String defaultGenType;
    private static ConcurrentHashMap<String, Integer> moreRowMapper = new ConcurrentHashMap<String, Integer>();
    private static ConcurrentHashMap<String, Integer> dbIDMapper = new ConcurrentHashMap<String, Integer>();

    private static final String GEN_ID_TYPE_NOR = "0";//生成id类型，常用规则类型
    private static final String GEN_ID_TYPE_FLOW = "1";//生成id类型，多行流水类型
    private static final String GEN_ID_TYPE_UUID = "2";//生成id类型，uuid类型
    private static final String GEN_ID_TYPE_SNOW = "3";//生成id类型，雪花id
    private static final String GEN_ID_TYPE_NOR16 = "4";//生成id类型，雪花id并且是16位


    private static String flowIdTemplate = "000";//预留3位给流水
    private static String tableIdTemplate = "000000";//表标识编号
    private static int idPlaceHolder = 10;


    private static int id16PlaceHolder = 8;

    private static int flowPlaceHolder = 3;


    
    
    /**
     * 使用雪花id
     * @param
     * @return
     */
    public static String generateSnowId() {
        return generateId();
    }
    
    private IdGenerator() {
    }
    
    /**
     * 使用默认的命名key来获取id.
     *
     * @return long id num.
     */
    private static long generate() {
        return 0;
    }
    
    /**
     * 部分表使用的是uuid
     * @param tableName
     * @return
     */
    private static String generateId(String tableName) {
        if(tableName != null && Constant.UUIDTableSet.contains(tableName.toLowerCase())) {
            String uuid = getUUIDStartsWithLetter();
            return uuid;
        } else {
            return ""+generate();
        }

    }
    
    /**
     * 使用雪花id
     * @param tableName
     * @return
     */
    public static String generateSnowId(String tableName) {
        return generateId();
    }
    
    
    /**
     *
     * @param e9OldTableName
     * @param e9OldPrimaryValue
     * @param ruleId
     * @param e10TableName
     * @return
     */
//    public static String generateIdByRuleStatus(String e9OldTableName, String e9OldPrimaryValue, int ruleId, String e10TableName) {
//        return generateIdByRuleStatus(e9OldTableName,e9OldPrimaryValue,ruleId,e10TableName,-1);
//    }
    
    
    /**
     * 生成UUID，要求不以数字型开头
     * @return
     */
    public static String getUUIDStartsWithLetter(){
        String uuid = getUUID();
        while(Character.isDigit(uuid.charAt(0))){
            uuid = getUUID();
        }
        return uuid;
    }
    
    public static String getUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
    



    /**
     * 默认E10分布式id
     * @return
     */
    private static String generateId() {
        return ""+generate();

    }












    /**
     * 获取E9对应E10的主键Id值  id算法   不存在时返回雪花id还是E9id原值
     * @param e9tablename
     * @param e10tablename
     * @param e9Id
     * @return
     */
    public static String transE10PrimaryInfo(String e10tablename,String e9tablename,String e9Id){
        return  getE9ToE10PrimaryInfo( e10tablename, e9tablename, e9Id,-1);
    }


    /**
     * 获取E9对应E10的主键Id值  id算法   最终的实体方法
     * @param e9TableName
     * @param e10TableName
     * @param e9Id
     * @return
     */
    public static String getE10PrimaryInfo(String e10TableName,String e9TableName,String e9Id,int ruleId){
        e10TableName = Util.null2String(e10TableName).trim();
        e9TableName = Util.null2String(e9TableName).trim();
        e9TableName = e9TableName.toLowerCase().trim();
        e10TableName = e10TableName.toLowerCase().trim();
        e9Id = Util.null2String(e9Id).trim();

        //如果传入的e9id为空 默认生成雪花id
        //同时可以解决一行对一行数据，如果E9的id字段值为空或者没有设置e9对应关系，导致生成的主键是相同导致冲突的问题
        if("".equals(e9Id)) {
            if(ruleId == -1) {//ruleid=-1都是获得id值的场景，如果e9id为空，返回值仍然为空
                return "";
            } else {
                return "";
            }
        }

        //判断是否是生成uuid的表，默认是生成uuid
        if(Constant.UUIDTableSet.contains(e10TableName)) {
            return generateTableUUID(e9TableName, e9Id);
        }

     
        //18位ID
        String tableSignId = "000000000000000000";

        if (!"".equals(e9TableName)) {
            String e9Key = e9TableName + "&";
            String e9e10Key = e9TableName + "&" + e10TableName;
            //表编号记录表中已维护
            if (Constant.e9ToE10primaryNumberMap.containsKey(e9Key) || Constant.e9ToE10primaryNumberMap.containsKey(e9e10Key)) {
                //前缀
                String prefix = Constant.TenantKeyNumber+"";
                //表标识编号
                String tableNumber = "";
                NumberEntity numberEntity = new NumberEntity();
                //如果新id中包含"E9表和E10对应的表"优先获取该编号,
                //例如："workflow_base&","workflow_base&wfp_base" 优先获取e9e10共同拼接"workflow_base&wfp_base"的编号
                if (Constant.e9ToE10primaryNumberMap.containsKey(e9e10Key)){
                    numberEntity = Constant.e9ToE10primaryNumberMap.get(e9e10Key);
                }else {
                    numberEntity = Constant.e9ToE10primaryNumberMap.get(e9Key);
                }
                if(numberEntity == null) {
                    numberEntity = new NumberEntity();
                }
                tableNumber = "" + numberEntity.getNumber();

                int numberSize = tableNumber.length();
                String genIdType = numberEntity.getGenidtype();//生成id类型
                if(StringUtils.isBlank(genIdType)) {
                    genIdType = GEN_ID_TYPE_NOR;
                }
                if(GEN_ID_TYPE_NOR.equals(genIdType)) {//如果是普通的一对一或者一对多表
                    e9Id = generateDataId(e9Id, 0);
                    //生成六位表标识编号，高位补0
                    tableNumber = formateTableSign(numberSize, tableNumber);
                    //拼接18位ID
                    tableSignId =  prefix + tableNumber + e9Id;
                } else if(GEN_ID_TYPE_FLOW.equals(genIdType)){//一对多（同表多行数据）,流水生成ID
                    int flowNumber = 0;
                    //如果ruleid=-1都是获得id值的场景，不需要流水，默认返回第一个；如果ruleId!=-1是生成id的场景，需要判断重复次数，进行流水
                    if(ruleId != -1) {
//                        flowNumber = generateOne2MoreSerialNum(e9Id, e9TableName, e10TableName, ruleId);
                    } else {
                        flowNumber = 1;//如果是流水表 默认取第一个，解决获得id时ruleid=-1，生成的id值关联不上的问题
                    }
                    e9Id = generateDataId(e9Id, flowNumber);
                    //生成六位表标识编号，高位补0
                    tableNumber = formateTableSign(numberSize, tableNumber);
                    //拼接18位ID
                    tableSignId =  prefix + tableNumber + e9Id;
                } else if(GEN_ID_TYPE_UUID.equals(genIdType)) {//生成uuid
                    return generateTableUUID(e9TableName, e9Id);
                } else if(GEN_ID_TYPE_SNOW.equals(genIdType)) {//雪花id
                    return generateId(e10TableName);
                } else if(GEN_ID_TYPE_NOR16.equals(genIdType)) {
                    e9Id = generateDataId16(e9Id, 0);
                    //生成六位表标识编号，高位补0
                    tableNumber = formateTableSign(numberSize, tableNumber);
                    //拼接18位ID
                    tableSignId =  prefix + tableNumber + e9Id;
                }
            } else {//表编号记录表中未维护,表标识默认为999999
                e9Id = generateDataId(e9Id, 0);
                tableSignId =  Constant.TenantKeyNumber + "999999" + e9Id;
            }
        } else {//E9表为空的情况
            e9Id = generateDataId(e9Id, 0);
            tableSignId =  Constant.TenantKeyNumber + "999999" + e9Id;
        }

        //保证长度不要超出
        if(tableSignId != null && tableSignId.length() > 18) {
            tableSignId = tableSignId.substring(0, 18);
        }
        //返回负数
        if("workflow_freenode".equalsIgnoreCase(e9TableName)) {
            tableSignId = "-"+tableSignId;
        }

        return tableSignId;
    }

    /**
     * 生成指定的uuid
     * @return
     */
    public static String generateTableUUID(String e9TableName, String e9Id) {
        String e9IdTemplate = "00000000000000000000000000000000";;//预留10位给数值，高位补0
        if(e9TableName != null) {
            e9TableName = e9TableName.toLowerCase().trim();
            e9TableName = e9TableName.replace("_", "");
            if(e9TableName.length() > 20) {//生成的uuid只能32位，表名可能会超出20的长度
                e9TableName = e9TableName.substring(0, 20);
            }
            e9TableName = e9TableName +  Constant.TenantKeyNumber;//拼接租户值
        }
        //生成10位id
        e9Id = generateDataId(e9Id, 0);
        e9Id = e9TableName + e9Id;
        int e9IdLen =  e9Id.length();
        //低位补零，因为e10的布局要求uuid首位必须是字母
        if(e9IdLen < 32) {
            e9Id = e9Id + e9IdTemplate.substring(e9IdLen, 32);
        }
        return e9Id;
    }


    /**
     * 表标识编号格式化
     * @param numberSize
     * @param tableNumber
     * @return
     */
    public static String formateTableSign(int numberSize, String tableNumber) {
        //生成六位表标识编号，高位补0
        numberSize =  tableIdTemplate.length() - numberSize;
        if(numberSize > 0) {
            tableNumber = tableIdTemplate.substring(0,numberSize) + tableNumber;
        } else if(numberSize < 0) {
            tableNumber = tableNumber.substring(0, tableIdTemplate.length());
        } else {
            tableNumber = tableNumber;
        }

        return tableNumber;
    }

    /**
     * 获取E9对应E10的主键Id值  id算法   最终的实体方法
     * @param e9TableName
     * @param e10TableName
     * @param e9Ids 多值
     * @return
     */
    public static String getE9ToE10PrimaryInfo(String e10TableName,String e9TableName,String e9Ids,int ruleId){


        if(e9Ids != null && e9Ids.indexOf(",") > -1) {
            String[]  e9IdArray = e9Ids.split(",");
            ArrayList<String> list = new ArrayList<String>();
            for(int i = 0; i < e9IdArray.length; i++) {
                String e10Id = getE10PrimaryInfo(e10TableName,e9TableName,e9IdArray[i],ruleId);
                list.add(e10Id);
            }
            return list.stream().collect(Collectors.joining(","));
        } else {
            return getE10PrimaryInfo(e10TableName,e9TableName,e9Ids,ruleId);
        }
    }


    /**
     * 生成数值的十位Id
     * @param
     * @param e9Id
     * @param
     * @return
     */
    public static String generateDataId(String e9Id, int flowNumber) {
        int e9IdSize = e9Id.trim().length();
        String e9IdTemplate = "0000000000";;//预留10位给数值，高位补0
        if(NumberUtils.isNumber(e9Id)) {//e9Id是数值
            if (e9Id.startsWith("-")){//判断是否为负值，负值第一位改成9
                e9IdSize -= 1;
                e9Id = e9Id.substring(1);
                e9IdTemplate = "9000000000";
            }
            if(e9Id.length() > 10) {//大于10位的按照字符串处理
                e9Id = "" + Util.getCRC32(e9Id);
            }

        } else {//e9Id非数值
            e9Id = "" + Util.getCRC32(e9Id);
        }
        e9IdSize = e9Id.trim().length();
        //高位补零
        if(e9IdSize < idPlaceHolder) {
            e9Id = e9IdTemplate.substring(0,(idPlaceHolder - e9IdSize)) + e9Id;
        }
        //拼接流水号
        if(flowNumber != 0) {
            //生成流水号
/*            String flowNumberStr = "" + flowNumber;
            int flowLen = flowNumberStr.length();
            if(flowLen < flowPlaceHolder) {
                flowNumberStr = flowIdTemplate.substring(0,(flowPlaceHolder-flowLen)) + flowNumberStr;
            }

            e9Id = flowNumberStr + e9Id.substring(flowPlaceHolder);*/
          //改造流水算法 减少冲突
            StringBuffer flowNumberStr = new StringBuffer("" + flowNumber);
            int flowLen = flowNumberStr.length();
            flowNumberStr = flowNumberStr.reverse();
            e9Id = flowNumberStr.toString() + e9Id.substring(flowLen);
        }

        return e9Id;
    }
    /**
     * 生成数值的八位Id
     * @param
     * @param e9Id
     * @param
     * @return
     */
    private static String generateDataId16(String e9Id, int flowNumber) {
        int e9IdSize = e9Id.trim().length();
        String e9IdTemplate = "00000000";;//预留8位给数值，高位补0
        if(NumberUtils.isNumber(e9Id)) {//e9Id是数值
            if (e9Id.startsWith("-")){//判断是否为负值，负值第一位改成9
                e9IdSize -= 1;
                e9Id = e9Id.substring(1);
                e9IdTemplate = "90000000";
            }
            if(e9Id.length() > id16PlaceHolder) {//大于10位的按照字符串处理
                e9Id = "" + Util.getCRC32(e9Id);
            }

        } else {//e9Id非数值
            e9Id = "" + Util.getCRC32(e9Id);
        }
        //高位补零
        if(e9IdSize < id16PlaceHolder) {
            e9Id = e9IdTemplate.substring(0,(id16PlaceHolder - e9IdSize)) + e9Id;
        }
        //拼接流水号
        if(flowNumber != 0) {
            //生成流水号
            String flowNumberStr = "" + flowNumber;
            int flowLen = flowNumberStr.length();
            if(flowLen < flowPlaceHolder) {
                flowNumberStr = flowIdTemplate.substring(0,(flowPlaceHolder-flowLen)) + flowNumberStr;
            }

            e9Id = flowNumberStr + e9Id.substring(flowPlaceHolder);
        }

        return e9Id;
    }
    /**
     * 一行变同表多行处理,获得流水号
     * @return
     */
//    public static int generateOne2MoreSerialNum(String e9Id, String e9TableName, String e10TableName, int ruleId) {
//        int serialNumber = 0;
//        int redisSerialInt = 0;
//        String trans_serial_tab = "trans_serial";
//        //备份表迁移模式
//        if(ENVConstant.USEBAKETABLEMODE.equals(ENVConstant.getBakTableMode())) {
//            trans_serial_tab = "trans_serial_ub";
//        }
//        //查询redis缓存
//        String dataKey = ruleId + "_" +e10TableName + "_" + e9TableName + "_" +e9Id;
//        if(JedisUtil.getIsUseRedis()) {//使用redis
//            JedisUtil jedisUtil = new JedisUtil();
//            String redisSerialNumber = jedisUtil.getValue(dataKey);
//            redisSerialInt = Util.getIntValue(redisSerialNumber, 0);
//            if(redisSerialInt == 0) {//如果缓存中没有数据，需要查询一次数据库，判断是否有值（防止服务停止后重新执行的问题）
//                //查询数据库表
//                SourceDBUtil sourceDBUtil = new SourceDBUtil();
//                sourceDBUtil.executeQuery("select oldtable,oldcontent,tablename,ruleid,count(1) as cnt from " + trans_serial_tab + " where  ruleid=? " +
//                        "GROUP BY oldtable,oldcontent,tablename,ruleid",ruleId);
//                while(sourceDBUtil.next()) {
//                    String oldtable = sourceDBUtil.getString("oldtable");
//                    String oldcontent = sourceDBUtil.getString("oldcontent");
//                    String tablename = sourceDBUtil.getString("tablename");
//                    String ruleid = sourceDBUtil.getString("ruleid");
//                    int t_serialNumber = sourceDBUtil.getInt("cnt");
//
//                    String tdataKey = ruleId + "_" +tablename + "_" + oldtable + "_" +oldcontent;
//                    jedisUtil.setValue(tdataKey, ""+t_serialNumber);
//                }
//
//                redisSerialNumber = jedisUtil.getValue(dataKey);
//                redisSerialInt = Util.getIntValue(redisSerialNumber, 0);
//                serialNumber = redisSerialInt;
//            } else {
//                serialNumber = redisSerialInt;
//            }
//
//            serialNumber++;//自增
//            jedisUtil.setValue(dataKey,""+serialNumber);
//        } else {//未使用redis
//            ConcurrentHashMap<String, Object> hisMoreRowMapper =
//                    CacheCommonUtil.getCacheMapByName("" + ruleId, "moreRowMapper");
//            if (hisMoreRowMapper != null && hisMoreRowMapper.size() > 0) {
//                Object object = hisMoreRowMapper.get(dataKey);
//                if (object != null) {
//                    serialNumber = (int) object;
//                }
//                serialNumber++;
//                hisMoreRowMapper.put(dataKey, serialNumber);
//                //放入缓存，规则结束后会清理
//                //CacheCommonUtil.setCacheDataInfo("" + ruleId, "moreRowMapper", hisMoreRowMapper);
//            } else {
//                assert !lockPool.isHeldByCurrentThread();
//                lockPool.lock();
//                try {
//                    hisMoreRowMapper =
//                            CacheCommonUtil.getCacheMapByName("" + ruleId, "moreRowMapper");
//                    if (hisMoreRowMapper == null || hisMoreRowMapper.size() == 0) {
//                        hisMoreRowMapper = new ConcurrentHashMap<String, Object>();
//                        //查询数据库表,判断是否已经迁移
//                        SourceDBUtil sourceDBUtil = new SourceDBUtil();
//                        sourceDBUtil.executeQuery("select oldtable,oldcontent,tablename,ruleid,count(1) as cnt from " + trans_serial_tab + " where  ruleid=? " +
//                                "GROUP BY oldtable,oldcontent,tablename,ruleid", ruleId);
//                        while (sourceDBUtil.next()) {
//                            String oldtable = sourceDBUtil.getString("oldtable");
//                            String oldcontent = sourceDBUtil.getString("oldcontent");
//                            String tablename = sourceDBUtil.getString("tablename");
//                            String ruleid = sourceDBUtil.getString("ruleid");
//                            int t_serialNumber = sourceDBUtil.getInt("cnt");
//                            String tdataKey = ruleId + "_" + tablename + "_" + oldtable + "_" + oldcontent;
//                            hisMoreRowMapper.put(tdataKey, t_serialNumber);
//
//                        }
//                        Object object = hisMoreRowMapper.get(dataKey);
//                        if (object != null) {
//                            serialNumber = (int) object;
//                        }
//                        serialNumber++;
//                        hisMoreRowMapper.put(dataKey, serialNumber);
//
//                        CacheCommonUtil.setCacheDataInfo("" + ruleId, "moreRowMapper", hisMoreRowMapper);
//
//                    }else{
//                        Object object = hisMoreRowMapper.get(dataKey);
//                        if (object != null) {
//                            serialNumber = (int) object;
//                        }
//                        serialNumber++;
//                        hisMoreRowMapper.put(dataKey, serialNumber);
//
//                    }
//
//                }catch(Exception e){
//                    new weaver.general.BaseBean().writeLog("", e);
//                } finally{
//                    lockPool.unlock();
//                }
//            }
//        }
//
//        return serialNumber;
//    }

    /**
     * 插入到表编号表
     * @param e9tablename
     * @param e10tablename
     * @param value
     */
//    public static void inserte9toe10NumberInfoAll(String e9tablename,String e10tablename,int value){
//        SourceDBUtil rs = new SourceDBUtil();
//        SourceDBUtil rs2 = new SourceDBUtil();
//        DBDisLock dbDisLock = new DBDisLock();//创建锁对象
//        if (dbDisLock.getLock("weaver_zxcv")) {
//            rs.executeQuery("select number from e9toe10numberinfo where tenant_key='all' and e9tablename='" + e9tablename + "' and e10tablename='" + e10tablename + "'");
//            if (!rs.next()) {
//                rs2.executeUpdate("insert into e9toe10numberinfo values(?,?,?,?,?,?,?)",
//                        IdGenerator.generateId(),
//                        e9tablename,
//                        e10tablename,
//                        value,
//                        TransUtil.getNowTime(),
//                        "all",
//                        0);
//            }
//            //解锁
//            dbDisLock.unLock("weaver_zxcv");
//        }
//    }

//    public static void main(String[] args) {
//
//        boolean mE9 = Util.isUseE9idToE10id("meeting", "mt_detail");
//
//
//        new BaseBean().writeLog("=====mE9:"+mE9);
//        new BaseBean().writeLog("=====start:"+new Date().getTime());
//        //for(int i = 0; i < 1000000; i++) {
//        initOtherConfig();
//
//            String signID = getE9ToE10PrimaryInfo("mt_detail","meeting","1",1);
//            String signID1 = getE9ToE10PrimaryInfo(FormConstant.custom_mt_detail,"meeting","1",1);
//            new BaseBean().writeLog("signID:"+signID);
//            new BaseBean().writeLog("signID1:"+signID1);
//        //}
//        //
//        //new BaseBean().writeLog("=====  end:"+new Date().getTime());
//
//    }
}
