package idtoolutil;



import org.springframework.stereotype.Component;


import java.util.zip.CRC32;

@Component
public class E10NewIdUtil {

//    @Autowired
//    private static Constant constant;
//    /**
//     * 判断是否使用老id
//     * @param e10TableName
//     * @param e9TableName
//     * @return
//     */
//    public static boolean reserveOldId(String e10TableName, String e9TableName) {
//        boolean useOldId = true;
//        if(!ENVConstant.ID_E9.equals(ENVConstant.getUseNewID())) {
//            return false;
//        }
//        if(!ENVConstant.getNewIDTableSet().contains(e10TableName)) {
//            if(!checkValNull(e9TableName)) {
//                //判断是否指定了e9表，解决E10多表合并后部分e9表使用新id,避免大表合并大表需要使用新id的问题
//                String mixTableName = e10TableName + "@" + e9TableName;
//                if(!ENVConstant.getNewIDTableSet().contains(mixTableName)) {
//                    useOldId = true;
//                } else {
//                    useOldId = false;
//                }
//            } else {
//                useOldId = true;
//            }
//        } else {
//            useOldId = false;
//        }
//        return useOldId;
//    }


    /**
     * 判断空值
     * @param v
     * @return
     */
    public static boolean checkValNull(String v) {
        if(v == null || "".equals(v) || "null".equals(v.toLowerCase())) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * 判断是否走拼接id
     * @param e10TableName
     * @param e9TableName
     * @return
     */
    public static boolean isUseE9idToE10id(String e10TableName, String e9TableName) {
        //加载配置
        Constant.initConfig();

        String e9e10Key = e9TableName + "&" + e10TableName;
        String e9Key = e9TableName + "&";

        if(Constant.e9ToE10primaryNumberMap!=null){
            boolean useNewID = Constant.e9ToE10primaryNumberMap.containsKey(e9e10Key) || Constant.e9ToE10primaryNumberMap.containsKey(e9Key);
            return useNewID;
        }else {
            return false;
        }

    }


    /**
     * 计算crc32值
     * @param content
     * @return
     */
    public static long getCRC32(String content) {
        CRC32 crc32 = new CRC32();
        crc32.update(content.getBytes());
        return crc32.getValue();
    }

}

