package idtoolutil;



import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBean;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import weaver.general.BaseBean;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.tablefiledMappingMap;

@Component
public class Constant {
    /***规则类型***/
    public static final int RULE_TYPE_JVM = 0;// 需要通过内存转换类型
    public static final int RULE_TYPE_DB = 1;// 数据库直接执行脚本
    /***规则类型***/

    // 判断E10数据存在的标识
    public static final String CHECK_DATA_FLAG = "checkdataflag";

    // 是否开启 判断E10数据存在的标识
    public static String IS_CHECK_DATA_FLAG = "";
    // E9表对应E10表的编号信息map
    public static ConcurrentHashMap<String, NumberEntity> e9ToE10primaryNumberMap = new ConcurrentHashMap<>();


    // E9表对应E10表的一对一对应关系
    public static ConcurrentHashMap<String, String> e9ToE10SingleTableMap = new ConcurrentHashMap<>();

    //租户对应编号
    public static int TenantKeyNumber = 10;

    //租户
    public static String TENANT = "allteams";

    //错误个数阈值
    public static int numberFilterCnt = 100;

    public static final String OLD_PRIMARY = "old_primary";
    public static final String OLD_TABLE = "old_table";
    public static final String TRAN_PARAMETER = "tran_parameter";
    public static final String DEF_PRI_FIELD = "id";
//    public static final HashSet<String> OLD_FIELDSET = new HashSet<String>() {{
//        add(OLD_PRIMARY);
//        add(OLD_TABLE);
//    }};

    /***执行状态***/
    public static final int STEP_0 = 0;// 未开始
    public static final int STEP_1 = 1;// 进行中
    public static final int STEP_2 = 2;// 完成
    public static final int STEP_3 = 3;// 失败
    public static final int STEP_4 = 4;//单独执行的规则

    /***转换规则初始化执行状态***/
    public static final int INIT_0 = 0;// 未开始
    public static final int INIT_1 = 1;// 进行中
    public static final int INIT_2 = 2;// 完成


    /**
     * 数据库类型
     **/
    public static final String MYSQL = "mysql";
    public static final String SQLSERVER = "sqlserver";
    public static final String ORACLE = "oracle";

    // 租户
//    public static String TENANT = "allteams";

    //所有租户与租户编号
    public static ConcurrentHashMap<String,Integer> TENANTS=new ConcurrentHashMap<>();

    public static String CREATOR = "0";
    public static String DTYPE = "0";
    // 记录一下初始的id值，用来与生成的数据id 比较，大于这个值的id，都是新生成的
    public static long MAXID_MARK = 0;
    // UUID表
    public static Set<String> UUIDTableSet = new HashSet<String>();
    // E10分表信息
//    public static HashMap<String, ECPaginationInfo> ecPaginationMap = new HashMap<String, ECPaginationInfo>();
    private static final Logger logger = LoggerFactory.getLogger(Constant.class);
    public static final String DEF_OLDTABLE = "none";
    public static Map<String, Integer> dynamicIdMap = new HashMap<String, Integer>();

    // 更新规则字段类型
    public static String UP_S = "0";// 更新
    public static String UP_C = "1";// 更新条件

    // 结束标识元素的表
    public static String FLINK_LASTBEAN = "flinklastbean";

    // 选择的时间类型
    public static String TIME_MONTH = "1";// 最近一个月
    public static String TIME_THRMONTH = "2";// 最近三个月
    public static String TIME_HALFYEAR = "3";// 最近半年
    public static String TIME_YEAR = "4";// 最近一年
    public static String TIME_RANG = "5";// 指定时间
    public static String TIME_ALL = "0";// 全部


    /**
     * sql分批查询间隔
     */
    public static int splitQueryCount = 100000;


    public static int baseRuleid = 1; // 基础的规则id ,给一些持久化缓存使用

    public static final String DATA_SEP = ",_,";

    public static final String PLACEHOLDER_TABLE = "#PLACEHOLDER_TABLE#";

    /***e10固定字段***/
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String CREATOR_NEW = "creator";
    public static final String DELETE_TYPE = "delete_type";
    public static final String TENANT_KEY = "tenant_key";

    /***增量规则ruleid区分***/
    public static final int INC_RULEID_PRE = 2000000000;
    public static final int  UP_RULEID_SU = 2100000000;


    /***增量规则功能名称区分***/
    public static final String INC_RULENAME = "增量新增-";
    public static final String  UP_RULENAME = "增量更新-";

    // 指定的财务流程表单
    public static Set<String> FNA_FORMID = new HashSet<String>();
    // 指定的公文流程表单
    public static Set<String> ODOC_FORMID = new HashSet<String>();
    // 指定的EB流程表单
    public static Set<String> EB_FORMID = new HashSet<String>();

    //无需迁移的建模应用id集合
    public static Set<String> EXPIRE_APPID  = new HashSet<String>();
    //无需迁移的建模模块id集合
    public static Set<String> EXPIRE_MODEID  = new HashSet<String>();


    //是否加载配置
    public static volatile boolean isloadconfig=false;

    /**
     * 初始化配置
     */
    public static void initConfig(){
        if(!isloadconfig){
            initE9AllPrimaryNumber();
        }
    }
    /**
     * 初始化获取E9所有表名对应的编号信息
     */
    public static synchronized void initE9AllPrimaryNumber() {
                                                                                            //传入的租户优先级最高,先查出finkdb表数量，为多行的话，查出上下文租户(TenantContext.getCurrentTenant().getTenantKey();)
        if(isloadconfig){
            return ;
        }

        try{

            Constant.TENANT=SqlCache.getTenantKey();
            Constant.TenantKeyNumber= Integer.parseInt(SqlCache.startNum());

   /*         PropertiesUtil propertiesUtil = new PropertiesUtil();
            String propertiespath = ConfigUtil.getConfigDir() + "e9toe10numberinfo.properties";
            propertiesUtil.load(new File(propertiespath));
            List<String> keys=propertiesUtil.getKeys();
            for(String key:keys){
                String value=propertiesUtil.get(key);
                String e9tablename=key.split("&")[0];
                String e10tablename=key.split("&")[1];
                String number=value.split("&")[0];
                String genidtype=value.split("&")[1];
                Constant.e9ToE10primaryNumberMap.put(e9tablename + "&" + e10tablename,new NumberEntity(e9tablename,e10tablename,Integer.parseInt(number),Constant.TENANT,Constant.TenantKeyNumber,genidtype));
            }
*/
/*            PropertiesUtil propertiesUtil = new PropertiesUtil();
            String confFileName = "sqlreplace.properties";
            String propertiespath = ConfigUtil.getConfigDir() + confFileName;
            propertiesUtil.load(new File(propertiespath));*/

            String e9ToE10primaryNumberMap = new BaseBean().getPropValue("sqlreplace", "e9ToE10primaryNumberMap");

            Constant.e9ToE10primaryNumberMap  = JSONObject.parseObject(e9ToE10primaryNumberMap, new TypeReference<ConcurrentHashMap<String, NumberEntity>>() {});
            isloadconfig=true;
        }catch(Exception e){
            new weaver.general.BaseBean().writeLog("", e);
        }
    }







    /**
     * 表，数据库对应信息
     * key 表名
     * value 库名，多个库以“,”分隔
     */
//    private static HashMap<String, TableInfoEntity> table2DSInfoMap = new HashMap<String, TableInfoEntity>(8192);
//    /**
//     * 字段对应信息
//     * key E9表字段信息
//     * value E10表字段信息
//     */
//    private static HashMap<FieldInfo, FieldInfo> fieldInfoMap = new HashMap<FieldInfo, FieldInfo>(1024);
//    /**
//     * 表对应信息
//     * key E9表信息
//     * value E10表信息，多个表以“,”分隔
//     */
//    private static HashMap<String, String> tableInfoMap = new HashMap<String, String>(1024);

    /**
     * 主表单表名信息
     */
    private static List<String> billTableList = new ArrayList();
    /**
     * 主表单表名信息
     */
    private static List<String> billDetailTableList = new ArrayList();

    private static List<String> viewTableList = new ArrayList();

//    private static List<BrowserBean> browserBeanList = new ArrayList();

    /**
     * 提前迁移的id值维护的信息
     */
    private static HashMap<String, String> idSegmentMap = new HashMap<>();
    public static String EXE_BIGTABLE = "0";
    public static String KAFKA_ADDR = "";
    /**
     * 是否是增量升级数据
     */
    public static final int NOT_INC_DATA = 0;
    public static final int INC_DATA = 1;
    public static final int INC_UP_DATA = 2;


    public static List<String> getBillTableList() {
        return billTableList;
    }

    public static void setBillTableList(List<String> billTableList) {
        Constant.billTableList = billTableList;
    }

    public static List<String> getBillDetailTableList() {
        return billDetailTableList;
    }

    public static void setBillDetailTableList(List<String> billDetailTableList) {
        Constant.billDetailTableList = billDetailTableList;
    }

    public static List<String> getViewTableList() {
        return viewTableList;
    }

    public static void setViewTableList(List<String> viewTableList) {
        Constant.viewTableList = viewTableList;
    }

//    public static List<BrowserBean> getBrowserBeanList() {
//        return browserBeanList;
//    }
//
//    public static void setBrowserBeanList(List<BrowserBean> browserBeanList) {
//        Constant.browserBeanList = browserBeanList;
//    }

    public static HashMap<String, String> getIdSegmentMap() {
        return idSegmentMap;
    }

    public static void setIdSegmentMap(HashMap<String, String> idSegmentMap) {
        Constant.idSegmentMap = idSegmentMap;
    }

    public static int getSplitQueryCount() {
        return splitQueryCount;
    }

    public static void setSplitQueryCount(int splitQueryCount) {
        Constant.splitQueryCount = splitQueryCount;
    }
}

