package com.cloudstore.dev.api.util;

import sun.misc.BASE64Encoder;
import weaver.general.Util;

import java.io.UnsupportedEncodingException;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 文本处理工具类
 */
public class TextUtil {


    /**
     * 处理富文本的css污染
     * 外层包个div，给style加作用域
     * @param str
     * @return
     */
    public static String manageCssPollute(String str){
        try{
            String text = str;
            String swapclassname = "_richSwapDiv_"+new Random().nextInt(1000);
            String addscopename = "."+swapclassname+" ";
            Pattern p1 = Pattern.compile("(<\\s*?style.*?>)([\\s\\S]*?)(<\\s*?/style\\s*?>)");
            Pattern p2 = Pattern.compile("(\\})(.*?)(\\{)");
            Matcher m1 = p1.matcher(text);
            int startindex1 = 0;
            while(m1.find(startindex1)){
                String frontStr1 = text.substring(0, m1.start(2));
                String styleStr = m1.group(2);
                //style类名加作用域
                Matcher m2 = p2.matcher(styleStr);
                int startindex2 = 0;
                while(m2.find(startindex2)){
                    String frontStr2 = styleStr.substring(0, m2.start(2));
                    String classNameStr = m2.group(2);
                    frontStr2 = frontStr2 + addscopename + classNameStr.trim() + m2.group(3);
                    styleStr = frontStr2 + styleStr.substring(m2.end(3));
                    //字符串改变后重新匹配并计算开始index
                    m2 = p2.matcher(styleStr);
                    startindex2 = frontStr2.length();
                }
                frontStr1 = frontStr1 + addscopename + styleStr.trim() + m1.group(3);
                text = frontStr1 + text.substring(m1.end(3));
                //字符串改变后重新匹配并计算开始index
                m1 = p1.matcher(text);
                startindex1 = frontStr1.length();
            }
            if(startindex1 == 0)	//内容不存在style块
                return str;
            text = "<div class=\""+swapclassname+"\">"+text+"</div>";
            return text;
        }catch(Exception e){
            return str;		//异常时返回原串
        }
    }

    /**
     * 移除特殊字符
     * @param s
     * @return
     */
    public static String removeSpecialChar(String s) {
        s = Util.null2String(s);
        StringBuilder sb = new StringBuilder();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            switch (c) {
				/*case '\"':
					sb.append("\\\""); break;
				case '\\':
					sb.append("\\\\"); break;
				case '/':
					sb.append("\\/"); break;*/
                case '\b':
                    sb.append(""); break;
                case '\f':
                    sb.append(""); break;
				/*case '\n':
					sb.append("\\n"); break;
				case '\r':
					sb.append("\\r"); break;*/
				/*case '\t':
					sb.append("\\t"); break;*/
                default:
                    if ((c >= 0 && c <= 31) || c == 127){	//在ASCII码中，第0～31号及第127号(共33个)是控制字符或通讯专用字符

                    } else {
                        sb.append(c);
                    }
                    break;
            }
        }
        return sb.toString();
    }

    /**
     * 将字符串转为base-64编码
     * @param s
     * @return
     */
    public static String toBase64(String s) {
        if (s == null) {
            return s;
        }
        try {
            BASE64Encoder base64Encoder = new BASE64Encoder();
            return base64Encoder.encode(s.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return s;
        }
    }
    
    /**
     * 将字符串转为base-64编码,多语言
     * @param s
     * @return
     */
    public static String toBase64ForMultilang(String s) {
        if (s == null || "".equals(s)) {
            return s;
        }
        if(false){
        	try {
                BASE64Encoder base64Encoder = new BASE64Encoder();
                return "base64_"+base64Encoder.encode(s.getBytes("UTF-8")).replaceAll("\n", "").replaceAll("\r", "");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return s;
            }
        }else{
        	return s;
        }
        
    }

    /**
     * 处理富文本的字符串，转为web展示
     * @param s
     * @return
     */
    public static String richTextToWeb(String s) {
        if (s == null) {
            return s;
        }
        String cssPolluteStr = manageCssPollute(s);
        return toBase64(cssPolluteStr);
    }
}
