package com.weaver.versionupgrade.util.decoder;

import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.general.BaseBean;
import weaver.general.Util;
import weaver.init.InitWeaverServer;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author ygd2020
 * @date 2024-4-3 14:42
 * Description:
 **/
public class decoderUtil {


    /**
     * 针对weaver.properties文件里用户名和密码进行加密
     * @param fname
     * @param key
     * @param value
     * @return
     */
    public static String getEncoderValue( String value){
        String needEncoder = getPropValueBase("DBEncoder", "needEncoder");//加解密开关，1表示启用数据库加密
        String reversible_enc_type = getPropValueBase("weaver_security_type", "reversible_enc_type");//加密方式
        needEncoder = needEncoder == null ? "0" : needEncoder;
        String secretKey = getPropValueBase("DBEncoder", "key");
        secretKey = secretKey == null ? "WEAVERECOLOGYDBENCODER" : secretKey;
        String encoderValue = value;
        if ("1".equals(needEncoder)) {

            try {
                if ("sm4".equals(reversible_enc_type)) {
                    SM4Utils sm4 = new SM4Utils();
                    encoderValue = sm4.decrypt(value, secretKey, SM4Utils.ENC_SM4);
                } else {
                    encoderValue = AESCoder.decrypt(value, secretKey);
                }
                if (encoderValue == null) {
                    encoderValue = value;
                }
                //new BaseBean().writeLog("=============="+encoderValue);
            } catch (Exception e) {

                encoderValue = value;
            }
        }
        return encoderValue==null?value:encoderValue;
    }

    private static String getPropValueBase(String fname, String key) {

        try
        {

            String oapath = getproppath();
			if (StringUtils.isBlank(oapath)) {
                return "";
            }

            Properties propertiesUtil = new Properties();

            InputStream is = new BufferedInputStream(new FileInputStream(oapath + fname + ".properties"));
            propertiesUtil.load(is);

            return Util.null2String(propertiesUtil.get(key));


        }
        catch(Exception e)
        {
            new weaver.general.BaseBean().writeLog("", e);
        }


        return "";

    }

    public static String getproppath() {
        String oapath = Util.null2String(InitWeaverServer.oawebinfopath);

        if (StringUtils.isNotBlank(oapath)) {
            return oapath;
        }


        oapath = Util.null2String(new BaseBean().getPropValue("database", "proppath"));

        if (StringUtils.isNotBlank(oapath)) {
            return oapath;
        }

        return "";
    }
    public static void main(String[] args) {
        String code = "1e37988078d30883dfd2826fdab69e88";

        System.out.println(decoderUtil.getEncoderValue(code));
    }

}
