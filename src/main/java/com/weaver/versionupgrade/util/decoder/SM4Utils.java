package com.weaver.versionupgrade.util.decoder;

import com.weaver.versionupgrade.util.Util;
import sun.misc.BASE64Decoder;

/**
 * @author ygd2020
 * @date 2024-4-3 14:44
 * Description:
 **/
public class SM4Utils {

    private boolean hexString = false;
    private static String ENC_MODE = "AES";

    public static final String ENC_AES = "AES";

    public static final String ENC_SM4 = "SM4";


    public String decrypt(String cipherText, String secretKey, String enc_mode)
    {
        try
        {
            if(!ENC_SM4.equalsIgnoreCase(enc_mode)){
                if(!ENC_MODE.equalsIgnoreCase(ENC_SM4) || ENC_AES.equalsIgnoreCase(enc_mode)){
                    return AESCoder.decrypt(cipherText, secretKey);
                }
            }

            SM4_Context ctx = new SM4_Context();
            ctx.isPadding = true;
            ctx.mode = SM4.SM4_DECRYPT;

            byte[] keyBytes;
            if (hexString)
            {
                keyBytes = hexStringToBytes(secretKey);

            }
            else
            {
                keyBytes = secretKey.getBytes();
            }

            SM4 sm4 = new SM4();
            sm4.sm4_setkey_dec(ctx, keyBytes);
            byte[] decrypted = sm4.sm4_crypt_ecb(ctx, new BASE64Decoder().decodeBuffer(cipherText));
            return new String(decrypted, "UTF-8");
        }
        catch (Exception e)
        {
            new weaver.general.BaseBean().writeLog("", e);
            return null;
        }
    }
    /**
     * Convert hex string to byte[]
     *
     * @param hexString
     *            the hex string
     * @return byte[]
     */
    public static byte[] hexStringToBytes(String hexString)
    {
        if (hexString == null || hexString.equals(""))
        {
            return null;
        }

        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++)
        {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c
     *            char
     * @return byte
     */
    public static byte charToByte(char c)
    {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

}
