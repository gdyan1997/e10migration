package com.weaver.versionupgrade.util;

import java.util.HashMap;

/**
 * User: wy
 * Date: 2024/4/18
 * Time: 11:02
 * Description:
 **/

public class CaseInsensitiveMap<K, V> extends HashMap<K, V> {

    @Override
    public V put(K key, V value) {
        if (key instanceof String) {
            return super.put((K) ((String) key).toLowerCase(), value);
        }
        return super.put(key, value);
    }

    @Override
    public V get(Object key) {
        if (key instanceof String) {
            return super.get(((String) key).toLowerCase());
        }
        return super.get(key);
    }
}
