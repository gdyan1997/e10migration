package com.weaver.versionupgrade.util;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cx
 * @date 2022/3/12
 **/
public class SyncConstant {
    /***规则类型***/
    public static final int RULE_TYPE_JVM = 0;// 需要通过内存转换类型
    public static final int RULE_TYPE_DB = 1;// 数据库直接执行脚本
    /***规则类型***/

    // 判断E10数据存在的标识
    public static final String CHECK_DATA_FLAG = "checkdataflag";
    // 是否开启 判断E10数据存在的标识
    public static String IS_CHECK_DATA_FLAG = "";

    //需要使用雪花id的表对应关系
    public static ConcurrentHashMap<String, Integer> e9ToE10primaryNumberFilterMap = new ConcurrentHashMap<>();
    // E9表对应E10表的一对一对应关系
    public static ConcurrentHashMap<String, String> e9ToE10SingleTableMap = new ConcurrentHashMap<>();
    //租户对应编号
    public static int TenantKeyNumber = 10;
    //E10和E9表对应关系（没有配置E9外键的字段映射）
    public static ConcurrentHashMap<String, HashSet<String>> e10ToE9CorrespondMap = new ConcurrentHashMap<>();
    //错误个数阈值
    public static int numberFilterCnt = 100;

    public static final String OLD_PRIMARY = "old_primary";
    public static final String OLD_TABLE = "old_table";
    public static final String TRAN_PARAMETER = "tran_parameter";
    public static final String DEF_PRI_FIELD = "id";
    public static final HashSet<String> OLD_FIELDSET = new HashSet<String>() {{
        add(OLD_PRIMARY);
        add(OLD_TABLE);
    }};

    /***执行状态***/
    public static final int STEP_0 = 0;// 未开始
    public static final int STEP_1 = 1;// 进行中
    public static final int STEP_2 = 2;// 完成
    public static final int STEP_3 = 3;// 失败
    public static final int STEP_4 = 4;//单独执行的规则

    /***转换规则初始化执行状态***/
    public static final int INIT_0 = 0;// 未开始
    public static final int INIT_1 = 1;// 进行中
    public static final int INIT_2 = 2;// 完成

    public static final String SYNC = "sync";//配置同步
    public static final String SIMPLE_MIGRATION = "simpleMigration";//常规数据迁移

    public static final String DB_SOURCE = "source";//源库
    public static final String DB_TARGET = "target";//源库
    /**
     * 数据库类型
     **/
    public static final String MYSQL = "mysql";
    public static final String SQLSERVER = "sqlserver";
    public static final String ORACLE = "oracle";
    public static final String DM = "dm";
    public static final String POSTGRESQL = "postgresql";
    // 租户
    public static String TENANT = "allteams";
    public static String CREATOR = "0";
    public static String DTYPE = "0";
    // 记录一下初始的id值，用来与生成的数据id 比较，大于这个值的id，都是新生成的
    public static long MAXID_MARK = 0;
    // UUID表
    public static Set<String> UUIDTableSet = new HashSet<String>();
    public static final String DEF_OLDTABLE = "none";
    public static Map<String, Integer> dynamicIdMap = new HashMap<String, Integer>();

    // 更新规则字段类型
    public static String UP_S = "0";// 更新
    public static String UP_C = "1";// 更新条件

    // 结束标识元素的表
    public static String FLINK_LASTBEAN = "flinklastbean";
    // 转换结束标识元素的表
    public static String PARALLELISM_LAST = "ParallelismLast";
    // 规则结束标识元素的表
    public static String PARALLELISM_LAST_END = "ParallelismLastEnd";

    // 选择的时间类型
    public static String TIME_MONTH = "1";// 最近一个月
    public static String TIME_THRMONTH = "2";// 最近三个月
    public static String TIME_HALFYEAR = "3";// 最近半年
    public static String TIME_YEAR = "4";// 最近一年
    public static String TIME_RANG = "5";// 指定时间
    public static String TIME_ALL = "0";// 全部

    /**
     * sql分批查询间隔
     */
    public static int splitQueryCount = 100000;


    public static int baseRuleid = 1; // 基础的规则id ,给一些持久化缓存使用

    public static final String DATA_SEP = ",_,";

    public static final String PLACEHOLDER_TABLE = "#PLACEHOLDER_TABLE#";
    /***e10固定字段***/
    public static final String CREATE_TIME = "create_time";
    public static final String UPDATE_TIME = "update_time";
    public static final String CREATOR_NEW = "creator";
    public static final String DELETE_TYPE = "delete_type";
    public static final String TENANT_KEY = "tenant_key";

    /***增量规则ruleid区分***/
    public static final int INC_RULEID_PRE = 2000000000;
    public static final int UP_RULEID_SU = 2100000000;


    /***增量规则功能名称区分***/
    public static final String INC_RULENAME = "增量新增-";
    public static final String UP_RULENAME = "增量更新-";

    // 指定的财务流程表单
    public static Set<String> FNA_FORMID = new HashSet<String>();
    // 指定的公文流程表单
    public static Set<String> ODOC_FORMID = new HashSet<String>();
    // 指定的EB流程表单
    public static Set<String> EB_FORMID = new HashSet<String>();

    //无需迁移的建模应用id集合
    public static Set<String> EXPIRE_APPID = new HashSet<String>();
    //无需迁移的建模模块id集合
    public static Set<String> EXPIRE_MODEID = new HashSet<String>();


}
