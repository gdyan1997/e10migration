package com.weaver.versionupgrade.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.weaver.versionupgrade.MiDBExecutor;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.mapping.VendorDatabaseIdProvider;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.PlatformTransactionManager;
import weaver.init.InitWeaverServer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import static weaver.init.InitWeaverServer.databaseurl;

@Configuration
@MapperScan("com.weaver.versionupgrade.entity.mapper")
public class MyBatisConfig {



    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> customizer() {
        return (factory) -> {
            factory.addContextCustomizers((context) -> {
                ((StandardJarScanner)context.getJarScanner()).setScanManifest(false);
            });
        };
    }

    /**
     * 设置spring默认的数据源 dataSource
     * 如果mybatisplus和默认的不一致的情况，可以将数据源配置成customDataSource
     * 下方引入的地方改成sqlSessionFactory(DataSource customDataSource)
     *
     * @return
     */
    @Bean
    public DataSource dataSource() {


        if (databaseurl==null){
            InitWeaverServer initWeaverServer = new InitWeaverServer();
            initWeaverServer.init(null);
        }

        // 创建非 Spring 默认的数据源对象
        // 设置数据源参数
        Map<String, String> dbMap = MiDBExecutor.getDBInfo("", "", "");
        if (dbMap != null && dbMap.containsKey("url")) {
            DataSource dataSource = DataSourceBuilder.create().url(dbMap.get("url")).username(dbMap.get("user")).password(dbMap.get("password")).driverClassName(dbMap.get("driver")).build();
            try {
                // 设置最大连接数
                ((HikariDataSource) dataSource).setMaximumPoolSize(10);
            }catch (Exception e){
                new weaver.general.BaseBean().writeLog("", e);
            }

            return dataSource;
        } else {
            return DataSourceBuilder.create().build();
        }
    }

    /**
     * 配置 SqlSessionFactory 对象
     * 当你设置了数据源需要在数据源上添加分页拦截器
     *
     * @param dataSource
     * @return
     * @throws Exception
     */
    @Bean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
        MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        // 设置 DatabaseIdProvider
        if (getDatabasePlatform(dataSource, false) != null) {
            VendorDatabaseIdProvider databaseIdProvider = new VendorDatabaseIdProvider();
            Properties properties = new Properties();
            properties.setProperty(getDatabasePlatform(dataSource, true), getDatabasePlatform(dataSource, false));
            databaseIdProvider.setProperties(properties);
            factoryBean.setDatabaseIdProvider(databaseIdProvider);

//            if("".equals(getDatabasePlatform(dataSource))){
//                //注册类型处理器
//                MybatisConfiguration configuration = new MybatisConfiguration();
//                configuration.getTypeHandlerRegistry().register(ClobTypeHandler.class);
//                factoryBean.setConfiguration(configuration);
//            }
        }

        // 设置 MyBatis XML 文件的路径
        Resource[] resources = new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml");
        factoryBean.setMapperLocations(resources);
        // 添加 MyBatis-Plus 分页插件等其他插件
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        Interceptor[] plugins = new Interceptor[]{interceptor}; // 添加其他插件，如分页插件等

        factoryBean.setPlugins(plugins);

        return factoryBean.getObject();
    }

    @Bean
    public PlatformTransactionManager transactionManager(DataSource customDataSource) {
        return new DataSourceTransactionManager(customDataSource);
    }

    /**
     * 开启分页组件
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
        DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);
        initializer.setEnabled(true);
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();

        // 获取数据库产品名称
        String databasePlatform = getDatabasePlatform(dataSource, false);
        System.out.println("*****************databasePlatform:" + databasePlatform);
        //检查是否要初始化
        if (checkNeedInit(dataSource, "javascript_trans", null, 1)) {//检查数据库表是否存在
            // 根据数据库产品名称加载对应的初始化 SQL 脚本
            switch (databasePlatform) {
                case "mysql":
                    populator.addScript(new ClassPathResource("db/init/data-mysql.sql"));
                    break;
                case "oracle":
                    populator.addScript(new ClassPathResource("db/init/data-oracle.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "sqlserver":
                    populator.addScript(new ClassPathResource("db/init/data-sqlserver.sql"));
                    break;
                case "pg":
                    populator.addScript(new ClassPathResource("db/init/data-pg.sql"));
                    break;
                case "dm":
                    populator.addScript(new ClassPathResource("db/init/data-dm.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "st"://未测试
                    populator.addScript(new ClassPathResource("db/init/data-st.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "jc"://未测试
                    populator.addScript(new ClassPathResource("db/upgrade/data-jc.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                // 其他数据库类型的处理
                default:
                    // 默认加载 MySQL 的初始化 SQL 脚本
                    populator.addScript(new ClassPathResource("db/init/data-mysql.sql"));
            }
        }else if (checkNeedInit(dataSource, "javascript_trans", "tranferstatus", 2)){//检查字段是否存在
            // 根据数据库产品名称加载对应的初始化 SQL 脚本
            switch (databasePlatform) {
                case "mysql":
                    populator.addScript(new ClassPathResource("db/upgrade/data-mysql-tranferstatus.sql"));
                    break;
                case "oracle":
                    populator.addScript(new ClassPathResource("db/upgrade/data-oracle-tranferstatus.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "sqlserver":
                    populator.addScript(new ClassPathResource("db/upgrade/data-sqlserver-tranferstatus.sql"));
                    break;
                case "pg":
                    populator.addScript(new ClassPathResource("db/upgrade/data-pg-tranferstatus.sql"));
                    break;
                case "dm":
                    populator.addScript(new ClassPathResource("db/upgrade/data-dm-tranferstatus.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "st"://未测试
                    populator.addScript(new ClassPathResource("db/upgrade/data-st-tranferstatus.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                // 其他数据库类型的处理
                case "jc"://未测试
                    populator.addScript(new ClassPathResource("db/upgrade/data-jc-tranferstatus.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                // 其他数据库类型的处理
                default:
                    // 默认加载 MySQL 的初始化 SQL 脚本
                    populator.addScript(new ClassPathResource("db/upgrade/data-mysql-tranferstatus.sql"));
            }
        }else if (checkNeedInit(dataSource, "javascript_trans", "otherproperty1", 2)){//检查字段是否存在
            // 根据数据库产品名称加载对应的初始化 SQL 脚本
            switch (databasePlatform) {
                case "mysql":
                    populator.addScript(new ClassPathResource("db/upgrade/data-mysql-otherproperty1.sql"));
                    break;
                case "oracle":
                    populator.addScript(new ClassPathResource("db/upgrade/data-oracle-otherproperty1.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "sqlserver":
                    populator.addScript(new ClassPathResource("db/upgrade/data-sqlserver-otherproperty1.sql"));
                    break;
                case "pg":
                    populator.addScript(new ClassPathResource("db/upgrade/data-pg-otherproperty1.sql"));
                    break;
                case "dm":
                    populator.addScript(new ClassPathResource("db/upgrade/data-dm-otherproperty1.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                case "st"://未测试
                    populator.addScript(new ClassPathResource("db/upgrade/data-st-otherproperty1.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                // 其他数据库类型的处理
                case "jc"://未测试
                    populator.addScript(new ClassPathResource("db/upgrade/data-jc-otherproperty1.sql"));
                    // 设置分隔符为 "/"
                    populator.setSeparator("/");
                    break;
                // 其他数据库类型的处理
                default:
                    // 默认加载 MySQL 的初始化 SQL 脚本
                    populator.addScript(new ClassPathResource("db/upgrade/data-mysql-otherproperty1.sql"));
            }
        }

        initializer.setDatabasePopulator(populator);
        return initializer;
    }

    //获取数据库产品名称
    private String getDatabasePlatform(DataSource dataSource, boolean orginal) {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            if (orginal) return metaData.getDatabaseProductName();
            return getDatabaseId(metaData.getDatabaseProductName());
        } catch (SQLException e) {
            throw new RuntimeException("Error retrieving database platform", e);
        }
    }

    private String getDatabaseId(String databaseProductName) {
        // 根据数据库类型判断并返回相应的 databaseId
        if ("MySQL".equalsIgnoreCase(databaseProductName)) {
            return "mysql";
        } else if ("Microsoft SQL Server".equalsIgnoreCase(databaseProductName)) {
            return "sqlserver";
        } else if ("Oracle".equalsIgnoreCase(databaseProductName)) {
            return "oracle";
        } else if ("PostgreSQL".equalsIgnoreCase(databaseProductName)) {
            return "pg";
        } else if ("DM DBMS".equalsIgnoreCase(databaseProductName)) {
            return "dm";
        } else if ("OSCAR".equalsIgnoreCase(databaseProductName)) {
            return "st";
        } else if ("JC".equalsIgnoreCase(databaseProductName)) {  // 添加 JC 数据库
            return "jc";
        }
        // 其他数据库类型的处理...
        return "oracle";//默认 oracle
    }

    //判断是否要初始化
    private boolean checkNeedInit(DataSource dataSource, String tableName, String fieldName, int type) {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            if (type == 1) {
                return !tableExists(metaData, tableName);
            } else if (type == 2) {
                return !columnExists(metaData, tableName, fieldName);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error retrieving database platform", e);
        }
        return true;
    }

    private boolean tableExists(DatabaseMetaData metaData, String tableName) throws SQLException {
        // Convert the table name to upper case to ensure case insensitivity
        try (ResultSet tables = metaData.getTables(null, null, tableName.toUpperCase(), null)) {
            if (!tables.next()) {
                // If not found, try lower case
                try (ResultSet tablesLower = metaData.getTables(null, null, tableName.toLowerCase(), null)) {
                    return tablesLower.next();
                }
            }
            return true;
        }
    }

    private boolean columnExists(DatabaseMetaData metaData, String tableName, String columnName) throws SQLException {
        // Convert the table name and column name to upper case to ensure case insensitivity
        try (ResultSet columns = metaData.getColumns(null, null, tableName.toUpperCase(), columnName.toUpperCase())) {
            if (!columns.next()) {
                // If not found, try lower case
                try (ResultSet columnsLower = metaData.getColumns(null, null, tableName.toLowerCase(), columnName.toLowerCase())) {
                    return columnsLower.next();
                }
            }
            return true;
        }
    }
}
