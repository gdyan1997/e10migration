package com.weaver.versionupgrade.config;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import weaver.general.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.sql.*;

/**
 * mybatisplus会已经有实现类去自动处理clob数据
 * 这个类只处理xml中返回map的形式，需要调用com.weaver.versionupgrade.config.ClobTypeHandler#clobToString(java.lang.Object)
 * @ClassName ClobTypeHandler
 * @Description:
 * @Version 1.0
 **/
public class ClobTypeHandler {
    /**
     * 获取clob数据 处
     * 理数据直接从clob中获取的情况
     * @param object
     * @return
     */
    public static String clobToString(Object object) {
        // 假设在您的类中进行判断
        String scriptStr;
        if (object instanceof Clob) {
            // 如果是 Clob 类型，进行特殊处理
            Clob clob = (Clob) object;
            scriptStr = clobToStringTrans(clob);
        } else {
            // 如果不是 Clob 类型，直接转换为字符串
            scriptStr = Util.null2String(object);
        }
        return scriptStr;
    }

    // 辅助方法，将 Clob 转换为字符串
    public static String clobToStringTrans(Clob clob) {
        StringBuilder sb = new StringBuilder();
        try {
            Reader reader = clob.getCharacterStream();
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            br.close();
        } catch (IOException | SQLException e) {
            // 处理异常
            new weaver.general.BaseBean().writeLog("", e);
        }
        return sb.toString();
    }
}
