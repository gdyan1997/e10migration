package com.weaver.versionupgrade.e10Migration;


import com.alibaba.fastjson.JSONObject;
import com.weaver.version.upgrade.util.sql.SQLUtil;
import com.weaver.version.upgrade.util.sql.bean.SqlReplaceParam;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import weaver.e10Migration.MigrationOperation;
import weaver.general.BaseBean;
import weaver.general.Util;

import java.io.File;
import java.util.Map;
import java.util.regex.Pattern;

@SpringBootApplication
@ComponentScan(basePackages = {"com.weaver", "weaver"})
public class E10MigrationApplication {

    public static void main(String[] args) {


        SpringApplication app = new SpringApplication(E10MigrationApplication.class);
        app.addInitializers(new ConditionalApplicationContextInitializer());
        app.run(args);


        String autoonce = Util.null2String(new BaseBean().getPropValue("database", "autoonce"));


//        {
//            String sql="select MANAGERSTR from hrmresource";
//            System.out.println(sql);
//            String newsql = SQLUtil.replaceSql(sql);
//            System.out.println(newsql);
//            System.out.println("____________________________");
//        }
//
//
//        {
//            String sql="select * from workflow_selectitem";
//            System.out.println(sql);
//            String newsql = SQLUtil.replaceSql(sql);
//            System.out.println(newsql);
//            System.out.println("____________________________");
//        }
//        {
//            String sql="select detailtable from workflow_billfield where detailtable=1";
//            System.out.println(sql);
//            String newsql = SQLUtil.replaceSql(sql);
//            System.out.println(newsql);
//            System.out.println("____________________________");
//        }

//        {
//            String sql="select * from (select t1.id as t1_id,t1.requestid as t1_requestid,t1.formmodeid as t1_formmodeid,t1.modedatacreater as t1_modedatacreater,t1.modedatacreatertype as t1_modedatacreatertype,t1.modedatacreatedate as t1_modedatacreatedate,t1.modedatacreatetime as t1_modedatacreatetime,t1.modeuuid as t1_modeuuid,t1.form_biz_id as t1_form_biz_id,t1.xm as t1_xm,t1.dxwb as t1_dxwb,t1.zs as t1_zs,t1.fds as t1_fds,t1.sm as t1_sm,t1.modedatamodifier as t1_modedatamodifier,t1.modedatamodifydatetime as t1_modedatamodifydatetime from uf_hccjmbd  t1 ) tmp1 where 1=1";
//
//            SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
//            sqlReplaceParam.setFirstQueryField(false);
//            sqlReplaceParam.setSourceCode(sql);
//            sqlReplaceParam.setSelectItem(true);
//
//
//            Map<String, Object> stringObjectMap = SQLUtil.replaceSql(sqlReplaceParam);
//
//            System.out.println(sqlReplaceParam);
//
//        }


//                {
//            String sql="select * from (select d.lastname leader,c.lastname,t.* from TM_TaskInfo t\n" +
//                    "\t\tleft join TM_TaskPartner p on t.id = p.taskid\n" +
//                    "\t\tleft join hrmresource c  on p.partnerid = c.id\n" +
//                    "\t\tleft join hrmresource d  on t.principalid = d.id\n" +
//                    "\t\twhere (p.partnerid={?userid} or t.principalid={?userid}) and deleted is null ) zz\n" +
//                    "\t\torder by enddate desc";
//
//            SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
//            sqlReplaceParam.setFirstQueryField(false);
//            sqlReplaceParam.setSourceCode(sql);
//            sqlReplaceParam.setSelectItem(true);
//
//
//            Map<String, Object> stringObjectMap = SQLUtil.replaceSql(sqlReplaceParam);
//
//            System.out.println(sqlReplaceParam);
//
//        }




//
//        Map<String, Object> stringObjectMap = SQLUtil.replaceSqlResultMap("SELECT *\n" +
//                "FROM hrmresource ,hrmde where id=1 and hrmresource=2 and 3=hrmresource and 3=hrmresource and 3=hrmresource");



        if (autoonce.equals("1")) {
            File allecologyzip = MigrationOperation.doMigrationOperation("", "0", "all,workflow,cube,interfaces,portal,edc,mobilemode,cubrowser");
            // File allecologyzip = MigrationOperation.doMigrationOperation("", "0", "workflow");

            System.out.println(allecologyzip.getPath());
            System.out.println("默认抓取完成!!!" );
            new BaseBean().writeLog("默认抓取完成!!!"  );
            new BaseBean().writeLog("默认抓取完成!!!"  );
        }else {
            new BaseBean().writeLog("启动完成!!!  请访问 http:///部署服务ip:4398 拉取清单!!!"  );
            new BaseBean().writeLog("启动完成!!!  请访问 http:///部署服务ip:4398 拉取清单!!!"  );
        }



    }



}
