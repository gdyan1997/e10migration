package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.ValidObjCacheUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.util.VFormUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;
import org.checkerframework.checker.units.qual.A;
import weaver.general.Util;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.weaver.versionupgrade.e10Migration.modules.mobilemode.util.VFormUtil.removeFirstAnd;

/**
 * 列表
 */
public class NListHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        JSONObject sourceData = (JSONObject) mecparamjson.get("sourceData");
        String searchCondition = Util.null2String(mecparamjson.get("searchCondition"));

        JSONArray customBtns = (JSONArray) mecparamjson.get("customBtns");
        JSONArray selectableBtns = (JSONArray) mecparamjson.get("selectableBtns");
        JSONArray swipeItems = (JSONArray) mecparamjson.get("swipeItems");

        if (sourceData != null) {
            String sourceType = Util.null2String(sourceData.get("sourceType"));
            String searchid = Util.null2String(sourceData.get("searchid"));
            String dataurl = Util.null2String(sourceData.get("dataurl"));


            boolean modeQuery = "1".equals(sourceType);
            boolean isMockData = "2".equals(sourceType);
            boolean sqlDataset = "3".equals(sourceType);
            boolean url = "4".equals(sourceType);
            boolean interfacelink = "5".equals(sourceType);


            String sourceSql = Util.null2String(sourceData.get("sourceSql"));
            String vdatasource = Util.null2String(sourceData.get("datasource"));


            if (modeQuery && !ValidObjCacheUtil.isValidCustomSearch(searchid)) {//如果是建模查询 但是是无效的查询 用虚拟表作
                sqlDataset = true;
                sourceSql = VFormUtil.getCustomSearchSql(searchid);

                vdatasource = VFormUtil.getVdatasourceBysearchId(searchid);
            }

            if (sqlDataset) {
                beans.add(
                        new ComponentBean().setHandType(HandType.sqlDev).setResult(
                                "列表数据 sql 需要调整，数据数据源：" + E10CubeUtil.getdatasourcename(vdatasource) + "\n sql : " + sourceSql));
            }

            if (url) {
                beans.add(
                        new ComponentBean().setHandType(HandType.linkDev).setResult(
                                "列表内容来源 url 需要调整，url：" + Util.null2String(sourceData.get("sourceUrl"))));
            }
            if (interfacelink) {
                JSONObject apiConfig = (JSONObject) sourceData.get("apiConfig");


                beans.add(
                        new ComponentBean().setHandType(HandType.javaDev).setResult(
                                "列表内容来源 接口 需要调整，接口信息：" + Util.null2String(apiConfig.get("api"))));
            }

            if (StringUtils.isNotBlank(dataurl)) {
                String type = dataurl.startsWith("javascript:") ? "js" : "";
                beans.add(
                        new ComponentBean().setHandType(HandType.linkDev).setResult(
                                "列表链接地址   " + type + " 需要调整:" + dataurl));
            }

        }

        if (customBtns!= null && customBtns.size()>0) {

            for (Object a : customBtns) {
                JSONObject customBtn  = (JSONObject) a;
                String btnText = Util.null2String(customBtn.get("btnText"));
                String btnScript = Util.null2String(customBtn.get("btnScript"));

                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "自定义按钮js 需要调整 按钮名称： " + Util.formatMultiLang(btnText) + "\n js : " + btnScript));
            }
        }
        if (StringUtils.isNotBlank(searchCondition)) {
            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult("搜索设置->快捷搜索查询条件 需要调整  ： " + searchCondition));
        }

        if (selectableBtns!= null && selectableBtns.size()>0) {

            for (Object a : selectableBtns) {
                JSONObject customBtn  = (JSONObject) a;
                String btnText = Util.null2String(customBtn.get("btnText"));
                String btnScript = Util.null2String(customBtn.get("btnScript"));
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult("数据可选->按钮列表->按钮js 需要调整 按钮名称： " + Util.formatMultiLang(btnText) + "\n js : " + btnScript));

            }
        }

        if (swipeItems!= null && swipeItems.size()>0) {

            for (Object a : swipeItems) {
                JSONObject customBtn  = (JSONObject) a;
                String btnText = Util.null2String(customBtn.get("btnText"));
                String btnScript = Util.null2String(customBtn.get("btnScript"));
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult("左滑配置->按钮js 需要调整 按钮名称： " + Util.formatMultiLang(btnText) + "\n js : " + btnScript));


            }
        }


        this.setLists(beans);

    }

}
