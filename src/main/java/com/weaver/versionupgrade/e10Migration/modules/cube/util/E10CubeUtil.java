package com.weaver.versionupgrade.e10Migration.modules.cube.util;

import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Hex;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import weaver.conn.RecordSet;
import weaver.general.BaseBean;
import weaver.general.Util;
import weaver.init.InitWeaverServer;
import weaver.systeminfo.SystemEnv;

import java.util.*;

/**
 * @author ygd2020
 * @date 2024-1-23 15:45
 * Description:
 **/
public class E10CubeUtil {


    /**
     * 转换map中的字段名称到e10 字段id 字段名不用区分大小写 仅支持主表
     *
     * @param cusFields 字段集合，key 数据库字段名（不用区分大小写）,值无所谓 会返回e10的字段id
     * @param e9_modeid e9模块id
     * @Author ygd2020
     * @Date 17:21 2022-12-22
     * @Return void
     **/
    public static void transE10CusFields(HashMap<String, String> cusFields, String e9_modeid) {//转换字段名称->字段id

        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();

        String sql = "select b.* from modeinfo a,workflow_billfield b where b.billid = a.formid   and a.id = " + e9_modeid + " and VIEWTYPE = 0";
        rs.executeSql(sql);
        while (rs.next()) {
            String fieldname = rs.getString("fieldname");
            String fieldid = rs.getString("id");


            for (String key : cusFields.keySet()) {
                if (key.equalsIgnoreCase(fieldname)) {//匹配忽略大小写匹配
                    cusFields.put(key, fieldid);
                }
            }
        }
    }

    public static String getCss(String content) {
        return getCss(content, false);
    }

    public static String getCss(String content, boolean needDecode) {

        if (needDecode) {
            content = decodeStr(content);
        }

        StringBuilder css = new StringBuilder();
        Document document = Jsoup.parse(content);
        //<a></a>视为资源标签 解析里面的id
        Elements resourceElement = document.select("style");

        for (Element element : resourceElement) {

            css.append(element.html());
        }

        return css.toString();


    }


    public static String getScript(String content) {
        return getScript(content, false);
    }


    public static String getScript(String content, boolean needDecode) {

        if (needDecode) {//代码块
            content = decodeStr(content);
        }

        StringBuilder script = new StringBuilder();
        Document document = Jsoup.parse(content);
        //<a></a>视为资源标签 解析里面的id
        Elements resourceElement = document.select("script");

        for (Element element : resourceElement) {
            String src = element.attr("src");
            if (!src.equals("")) {//js文件的模式 把源标签解析出来
                script.append("/* ").append(element.toString()).append(" */  \n");
            }
            script.append(element.html()).append(" \n");

        }

        return script.toString();


    }


    public static String decodeStr(String content) {

        if (StringUtils.isBlank(content)) {
            return "";
        }

        byte[] keybytes = "WEAVER E-DESIGN.".getBytes();
        byte[] iv = "weaver e-design.".getBytes();
        try {
            BufferedBlockCipher engine = new PaddedBufferedBlockCipher(
                    new CBCBlockCipher(new AESFastEngine()));
            engine.init(true, new ParametersWithIV(new KeyParameter(keybytes), iv));
            byte[] deByte = Hex.decode(content);
            engine.init(false, new ParametersWithIV(new KeyParameter(keybytes), iv));
            byte[] dec = new byte[engine.getOutputSize(deByte.length)];
            int size1 = engine.processBytes(deByte, 0, deByte.length, dec, 0);
            int size2 = engine.doFinal(dec, size1);
            byte[] decryptedContent = new byte[size1 + size2];
            System.arraycopy(dec, 0, decryptedContent, 0,
                    decryptedContent.length);
            return new String(decryptedContent, "GBK");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static String getRealFromName(String virtualFormName) {
        if (virtualFormName.length() > 33 && virtualFormName.charAt(32) == '_') {
            virtualFormName = virtualFormName.substring(33);
        }
        return virtualFormName;
    }


    public static String getModeFieldLinkageSql(String entryid) {
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        RecordSet rs2 = new RecordSet();
        RecordSet rs3 = new RecordSet();
        String finalSql = "select ";

        rs1.executeQuery("select * from modeDataInputmain where ENTRYID=?", entryid);
        while (rs1.next()) {
            String backfields = "";
            String sqlfrom = "";
            String sqlwhere = "";

            String mainId = Util.null2String(rs1.getString("id"));
            rs2.executeQuery("select * from modeDataInputtable  where DATAINPUTID=?", mainId);
            while (rs2.next()) {
                String tableid = Util.null2String(rs2.getString("id"));
                String tablename = Util.null2String(rs2.getString("TABLENAME"));
                String alias = Util.null2String(rs2.getString("ALIAS"));
                if ("".equals(alias)) {
                    alias = tablename;
                    sqlfrom = sqlfrom + tablename + ",";
                } else {
                    sqlfrom = sqlfrom + tablename + " " + alias + ",";
                }


                rs3.executeQuery("select * from modeDataInputfield  where DATAINPUTID=? and TABLEID=?", mainId, tableid);
                while (rs3.next()) {
                    String type = Util.null2String(rs3.getString("type"));// 1为条件字段，2为查询字段
                    String dbfieldname = Util.null2String(rs3.getString("DBFIELDNAME"));
                    String pagefieldname = Util.null2String(rs3.getString("PAGEFIELDNAME"));
                    if ("1".equals(type)) {// 拼接条件
                        sqlwhere = sqlwhere + alias + "." + dbfieldname + "='$" + pagefieldname + "$'" + " and ";
                    }
                    if ("2".equals(type)) {
                        backfields = backfields + alias + "." + dbfieldname + ",";
                    }
                }
            }
            backfields = backfields.trim();
            sqlfrom = sqlfrom.trim();
            sqlwhere = sqlwhere.trim();
            if (backfields.endsWith(",")) {
                backfields = backfields.substring(0, backfields.length() - 1);
            }
            if (sqlfrom.endsWith(",")) {
                sqlfrom = sqlfrom.substring(0, sqlfrom.length() - 1);
            }
            if (sqlwhere.endsWith("and")) {
                sqlwhere = sqlwhere.substring(0, sqlwhere.length() - 3);
            }

            String whereclause = Util.null2String(rs1.getString("WHERECLAUSE")).trim();
            if (whereclause.toLowerCase().startsWith("where")) {
                whereclause.substring(5);
            }

            if ("".equals(backfields)) {
                backfields = "*";
            }
            finalSql = finalSql + backfields + " from " + sqlfrom + " where 1=1 ";
            if (!"".equals(whereclause)) {
                finalSql = finalSql + " and " + whereclause;
            }
            if (!"".equals(sqlwhere)) {
                finalSql = finalSql + " and " + sqlwhere;
            }
        }
        return finalSql;
    }

    public static String getDmlActionName(String dmlactionid) {

        RecordSet rs = new RecordSet();

        String sqldml = "select  * from mode_dmlactionset a,mode_dmlactionsqlset b where a.id = ? and a.id=b.ACTIONID";
        rs.executeQuery(sqldml, dmlactionid);

        String dmlactionname = "";

        if (rs.next()) {

            dmlactionname = Util.null2String(rs.getString("DMLACTIONNAME"));

        }

        return dmlactionname;
    }

    public static boolean checkCodeHasDevCode(String code) {


        boolean haskaifa = false;
        try {
            org.jsoup.nodes.Element body = org.jsoup.Jsoup.parse(code);
            org.jsoup.select.Elements scripts = body.select("script");
            for (int i = 0; i < scripts.size(); i++) {
                org.jsoup.nodes.Element script = scripts.get(i);
                if (script != null) {
                    String str = removeComments(script.data().trim());
                    if (StringUtils.isNotBlank(str)) {
                        haskaifa = true;
                    }

                    String src = Util.null2String(script.attr("src"));
                    if (!"".equals(src)) {
                        haskaifa = true;
                    }
                }
            }
            org.jsoup.select.Elements styles = body.select("style");
            for (int i = 0; i < styles.size(); i++) {
                org.jsoup.nodes.Element style = styles.get(i);
                if (style != null) {
                    String str = removeComments(style.data().trim());
                    if (StringUtils.isNotBlank(str)) {
                        haskaifa = true;
                    }
                    String src = Util.null2String(style.attr("src"));
                    if (!"".equals(src)) {
                        haskaifa = true;
                    }
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }


        return haskaifa;
    }

    public static String removeComments(String input) {
        StringBuilder result = new StringBuilder();
        boolean inSingleLineComment = false;
        boolean inMultiLineComment = false;

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);
            char nextChar = i < input.length() - 1 ? input.charAt(i + 1) : '\0';

            if (!inSingleLineComment && !inMultiLineComment) {
                if (currentChar == '/' && nextChar == '*') {
                    inMultiLineComment = true;
                    i++;
                } else if (currentChar == '/' && nextChar == '/') {
                    inSingleLineComment = true;
                    i++;
                } else {
                    result.append(currentChar);
                }
            } else if (inSingleLineComment) {
                if (currentChar == '\n') {
                    inSingleLineComment = false;
                    result.append(currentChar);
                }
            } else if (inMultiLineComment) {
                if (currentChar == '*' && nextChar == '/') {
                    inMultiLineComment = false;
                    i++;
                }
            }
        }

        return result.toString();
    }

    public static String getDevCode(String code, String csstag) {

        String content = "";

        try {
            org.jsoup.nodes.Element body = org.jsoup.Jsoup.parse(code);
            org.jsoup.select.Elements scripts = body.select(csstag);
            for (int i = 0; i < scripts.size(); i++) {
                org.jsoup.nodes.Element script = scripts.get(i);
                if (script != null) {
                    String str = removeComments(script.data().trim());
                    if (StringUtils.isNotBlank(str)) {
                        content += script.data() + "\n";
                    }

                    String src = Util.null2String(script.attr("src"));
                    if (!"".equals(src)) {
                        content += script.data() + "\n";
                    }
                }
            }


        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }


        return content;
    }

    public static String getDmlSqlContent(String dmlactionid) {

        if (StringUtils.isBlank(dmlactionid)) {
            return "";
        }

        String sqlContent = execute(dmlactionid);


        return sqlContent;

    }

    public static void main(String[] args) {

        InitWeaverServer initWeaverServer = new InitWeaverServer();
        initWeaverServer.init(null);


        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();
        rs.executeQuery("select * from  mode_dmlactionset");
        while (rs.next()) {

            String id = rs.getString("id");

            System.out.println(execute(id));

        }
//        String id = "12";
//
//        System.out.println(id + "---" + JSON.toJSONString(execute(id)));

    }

    /**
     * 执行dmlaction
     */
    public static String execute(String dmlactionid) {

        List<String> dmlsqllist = new ArrayList<>();


        try {

            org.apache.flink.runtime.customRest.util.SourceDBUtil rst = new org.apache.flink.runtime.customRest.util.SourceDBUtil();
            org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();

            //获取dmlaction 属性
            List dmlList = getDmlActionSet(rst, rs, dmlactionid);


            if (dmlList.size() > 0) {
                String fieldname = "";
                List fieldtypeList = new ArrayList();
                List fieldnameList = new ArrayList();
                List fieldvalueList = new ArrayList();
                List sqlsetList = new ArrayList();
                for (int i = 0; i < dmlList.size(); i++) {
                    sqlsetList.clear();
                    sqlsetList = (List) dmlList.get(i);
                    if (null != sqlsetList && sqlsetList.size() > 7) {
                        String sqlsetid = (String) sqlsetList.get(0);
                        String datasourceid = (String) sqlsetList.get(1);
                        String dmltype = (String) sqlsetList.get(2);
                        String dmlsql = (String) sqlsetList.get(3);
                        String dmlfieldtypes = (String) sqlsetList.get(4);
                        String dmlfieldnames = (String) sqlsetList.get(5);
                        String dmlcuswhere = (String) sqlsetList.get(6);
                        String dmlmainsqltype = (String) sqlsetList.get(7);
                        String dmlcussql = (String) sqlsetList.get(8);
                        String isresetright = (String) sqlsetList.get(9);
                        String targetmodeid = (String) sqlsetList.get(10);
                        int dmlsource = Util.getIntValue(Util.null2String(sqlsetList.get(11)), -1);
                        String dmlsourcetype = (String) sqlsetList.get(12);
                        try {
                            fieldtypeList.clear();
                            fieldnameList.clear();

                            fieldnameList = TokenizerString(dmlfieldnames, "|");
                            fieldtypeList = TokenizerString(dmlfieldtypes, "|");
                            dmlcussql = (String) sqlsetList.get(8);
                            dmlcuswhere = (String) sqlsetList.get(6);
                            fieldvalueList.clear();

                            dmlcussql = getFieldValues(dmlcussql, fieldnameList);
                            dmlsql = getFieldValues(dmlsql, fieldnameList);
                            //以sql的形式执行dmlaction
                            executeDMLAction(dmlsqllist, rst, rs, datasourceid, dmltype, dmlsql, fieldtypeList, fieldvalueList, dmlcuswhere.trim(), dmlmainsqltype, dmlcussql.trim(), isresetright, targetmodeid);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.join("\n", dmlsqllist);
    }


    private static String getFieldValues(String sql, List fieldnameList) {


        for (int f = 0; f < fieldnameList.size(); f++) {

            String fieldname = (String) fieldnameList.get(f);
            fieldname = fieldname.toLowerCase();
            if (fieldname.indexOf("=") > 0) {
                String[] arr = fieldname.split("=");
                if (arr.length == 2) {
                    fieldname = arr[1];
                }
            }
            if (sql.toLowerCase().trim().startsWith("insert")) {
                sql = sql.replaceFirst("\\?", "{" + fieldname + "}");
            } else {
                sql = sql.replaceFirst("=\\?", "={?" + fieldname + "}");
            }

        }
        return sql;

    }


    private static void executeDMLAction(List<String> dmlsqllist, org.apache.flink.runtime.customRest.util.SourceDBUtil rst, org.apache.flink.runtime.customRest.util.SourceDBUtil rs, String datasourceid, String dmltype, String dmlsql, List fieldtypeList, List fieldvalueList, String dmlcuswhere, String dmlmainsqltype, String dmlcussql, String isresetright, String targetmodeid) {

        try {
            //如果为update或者delete，必须有条件
            if (dmltype.equals("update") || dmltype.equals("delete")) {

                //先执行设置的sql
                if (!"".equals(dmlsql) && (dmlsql.toLowerCase().indexOf(" where ") > -1 || !"".equals(dmlcuswhere))) {

                    if (!"".equals(dmlcuswhere)) {
                        if (dmlsql.toLowerCase().indexOf(" where ") > -1) {
                            dmlsql += " and " + dmlcuswhere;
                        } else {
                            dmlsql += " where " + dmlcuswhere;
                        }
                    }

                    //获取更新数据的表单id 表单名称    res 已经不使用，存储过程执行getQuerySql会导致报错不执行
                    //Map<String,Object> res = getUpdateDatas(ids,rs,dmltype,dmlsql,fieldtypeList,datasourceid);
                    //更新数据放在最后,否则若条件字段也参与了被赋值字段,会导致上面的更新权限找不到记录
                    dmlsqllist.add(dmlsql);
                }

                //再执行自定义dmlsql
                if ((dmlcussql.toLowerCase().indexOf("where") > -1 && Util.getIntValue(dmlmainsqltype, 0) == 0) || (!dmlcussql.equals("") && Util.getIntValue(dmlmainsqltype, 0) == 1)) {


                    //获取更新数据的表单id 表单名称  res 已经不使用，存储过程执行getQuerySql会导致报错不执行
                    //Map<String,Object> res = getUpdateDatas(ids,rs,dmltype,dmlcussql,fieldtypeList,datasourceid);
                    //更新数据放在最后,否则若条件字段也参与了被赋值字段,会导致上面的更新权限找不到记录
                    dmlsqllist.add(dmlcussql);

                }
            } else {
                if (!"".equals(dmlsql)) {
                    dmlsqllist.add(dmlsql);

                }
                //再执行自定义dmlsql
                if (!"".equals(dmlcussql)) {
                    String lowserDmlcussql = dmlcussql.toLowerCase();


                    //第三方库存储过程使用executeProc2方法
                    if (!"".equals(datasourceid) && (dmlcussql.startsWith("call") || dmlcussql.startsWith("EXECUTE") || dmlcussql.startsWith("execute"))) {

                        dmlsqllist.add(dmlcussql);
                    } else {

                        dmlsqllist.add(dmlcussql);
                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static ArrayList TokenizerString(String str, String dim) {
        return TokenizerString(str, dim, false);
    }

    public static ArrayList TokenizerString(String str, String dim, boolean returndim) {
        str = Util.null2String(str);
        dim = Util.null2String(dim);
        ArrayList strlist = new ArrayList();
        StringTokenizer strtoken = new StringTokenizer(str, dim, returndim);
        while (strtoken.hasMoreTokens()) {
            strlist.add(strtoken.nextToken());
        }
        return strlist;
    }

    /**
     * 获取dmlaction 属性
     *
     * @param rst
     * @param rs
     * @param dmlactionid
     * @return
     * @throws Exception
     */
    private static List getDmlActionSet(org.apache.flink.runtime.customRest.util.SourceDBUtil rst, org.apache.flink.runtime.customRest.util.SourceDBUtil rs, String dmlactionid) throws Exception {
        List dmlList = new ArrayList();
        StringBuffer sqlsb = new StringBuffer();
        sqlsb.append("select t.id,");
        sqlsb.append("s.datasourceid,");
        sqlsb.append("s.dmltype,");
        sqlsb.append("s.isresetright,");
        sqlsb.append("s.targetmodeid,");
        sqlsb.append("t.dmlsql,");
        sqlsb.append("t.dmlfieldtypes,");
        sqlsb.append("t.dmlfieldnames,");
        sqlsb.append("t.dmlcuswhere,");
        sqlsb.append("t.dmlmainsqltype,");
        sqlsb.append("t.dmlcussql,");
        sqlsb.append("s.dmlsource,");
        sqlsb.append("s.dmlsourcetype");
        sqlsb.append(" from mode_dmlactionset s, mode_dmlactionsqlset t");
        sqlsb.append(" where s.id = t.actionid");
        sqlsb.append(" and s.id =" + dmlactionid);

        sqlsb.append("	 order by s.dmlorder");
        if (null != rst) {
            rst.executeSql(sqlsb.toString());
            while (rst.next()) {
                List sqlsetList = setSqlsetList(rst, null);
                if (sqlsetList.size() > 0) {
                    dmlList.add(sqlsetList);
                }
            }
        } else if (null != rs) {
            rs.executeSql(sqlsb.toString());
            while (rs.next()) {
                List sqlsetList = setSqlsetList(null, rs);
                if (sqlsetList.size() > 0) {
                    dmlList.add(sqlsetList);
                }
            }
        }
        return dmlList;
    }

    /**
     * 将dmlaction属性进行组装
     *
     * @param rst
     * @param rs
     * @return
     */
    private static List setSqlsetList(org.apache.flink.runtime.customRest.util.SourceDBUtil rst, org.apache.flink.runtime.customRest.util.SourceDBUtil rs) {
        List sqlsetList = new ArrayList();
        if (null != rst) {
            sqlsetList.add(rst.getString("id"));
            sqlsetList.add(rst.getString("datasourceid"));
            sqlsetList.add(rst.getString("dmltype"));
            sqlsetList.add(rst.getString("dmlsql"));
            sqlsetList.add(rst.getString("dmlfieldtypes"));
            sqlsetList.add(rst.getString("dmlfieldnames"));
            sqlsetList.add(rst.getString("dmlcuswhere"));
            sqlsetList.add(rst.getString("dmlmainsqltype"));
            sqlsetList.add(rst.getString("dmlcussql"));
            sqlsetList.add(rst.getString("isresetright"));
            sqlsetList.add(rst.getString("targetmodeid"));
            sqlsetList.add(rst.getString("dmlsource"));
            sqlsetList.add(rst.getString("dmlsourcetype"));
        } else if (null != rs) {
            sqlsetList.add(rs.getString("id"));
            sqlsetList.add(rs.getString("datasourceid"));
            sqlsetList.add(rs.getString("dmltype"));
            sqlsetList.add(rs.getString("dmlsql"));
            sqlsetList.add(rs.getString("dmlfieldtypes"));
            sqlsetList.add(rs.getString("dmlfieldnames"));
            sqlsetList.add(rs.getString("dmlcuswhere"));
            sqlsetList.add(rs.getString("dmlmainsqltype"));
            sqlsetList.add(rs.getString("dmlcussql"));
            sqlsetList.add(rs.getString("isresetright"));
            sqlsetList.add(rs.getString("targetmodeid"));
            sqlsetList.add(rs.getString("dmlsource"));
            sqlsetList.add(rs.getString("dmlsourcetype"));
        }
        return sqlsetList;
    }

    public static String getdttableByWorkflowId(String workflowId, String ordeid) {

        SourceDBUtil sourceDBUtil = new SourceDBUtil();

        sourceDBUtil.executeQuery("select workflow_base.id,ORDERID,TABLENAME\n" +
                "from workflow_base join workflow_billdetailtable on FORMID = BILLID where workflow_base.id = ? and ORDERID = ?", workflowId, ordeid);

        if (sourceDBUtil.next()) {
            return sourceDBUtil.getString("TABLENAME");
        } else {
            return "";
        }
    }


    public static String getLabelName(String labelId) {

        if (labelId.equals("")) {//id为空无需处理
            return "";
        }
        String labelName = "";
        RecordSet rs = new RecordSet();
        rs.executeQuery("select labelname from htmlLabelinfo where INDEXID = ? and LANGUAGEID = '7' ", labelId);
        if (rs.next()) {
            labelName = rs.getString("labelname");
        }
        return labelName;
    }


    public static String getLayoutType(String type) {


        String layouttype = "";
        int typeNew = Util.getIntValue(Util.null2String(type));

        switch (typeNew) {
            case 0:
                layouttype = "显示布局";
                break;
            case 1:
                layouttype = "新建布局";
                break;
            case 2:
                layouttype = "编辑布局";
                break;
            case 3:
                layouttype = "监控布局";
                break;
            case 4:
                layouttype = "打印布局";
                break;
        }


        return layouttype;
    }

    public static String getbtnname(String expendname, String issystem, String issystemflag) {

        int languageid = 7;
        String str = expendname;
        if (issystem.equals("1")) {
            str += "(";
            if (issystemflag.equals("1")) {
                str += SystemEnv.getHtmlLabelName(30829, languageid);//"(新建保存)30829";
            } else if (issystemflag.equals("2")) {
                str += SystemEnv.getHtmlLabelName(30830, languageid);//"(编辑保存)30830";
            } else if (issystemflag.equals("3")) {
                str += SystemEnv.getHtmlLabelName(30831, languageid);//"(查看编辑)30831";
            } else if (issystemflag.equals("4")) {
                str += SystemEnv.getHtmlLabelName(30832, languageid);//"(查看共享)30832";
            } else if (issystemflag.equals("5")) {
                str += SystemEnv.getHtmlLabelName(30833, languageid);//"(查看删除)30833";
            } else if (issystemflag.equals("6")) {
                str += SystemEnv.getHtmlLabelName(30834, languageid);//"(监控删除)30834";
            } else if (issystemflag.equals("7")) {
                str += SystemEnv.getHtmlLabelName(257, languageid);//"(打印)257";
            } else if (issystemflag.equals("8")) {
                str += SystemEnv.getHtmlLabelName(33418, languageid); //"(清空条件)33418";
            } else if (issystemflag.equals("100")) {
                str += SystemEnv.getHtmlLabelName(197, languageid);//"(搜索)197";
            } else if (issystemflag.equals("101")) {
                str += SystemEnv.getHtmlLabelName(82, languageid);//"(新建)82
            } else if (issystemflag.equals("102")) {
                str += SystemEnv.getHtmlLabelName(91, languageid);//"(删除)91";
            } else if (issystemflag.equals("103")) {
                str += SystemEnv.getHtmlLabelName(26601, languageid);//"(批量导入)26601";
            } else if (issystemflag.equals("105")) {
                str += SystemEnv.getHtmlLabelName(17416, languageid);//"(导出)17416";
            } else if (issystemflag.equals("104")) {
                str += SystemEnv.getHtmlLabelName(18037, languageid);//"(批量共享)18037";
            } else if (issystemflag.equals("9")) {
                str += SystemEnv.getHtmlLabelName(83, languageid);//"(日志)83";
            } else if (issystemflag.equals("10")) {
                str += SystemEnv.getHtmlLabelName(32720, languageid);//"(保存并新建)32720";
            } else if (issystemflag.equals("17")) {
                str += SystemEnv.getHtmlLabelName(500750, languageid);//"(保存并复制)500750";
            } else if (issystemflag.equals("106")) {
                str += SystemEnv.getHtmlLabelName(32535, languageid);//"(显示定制列)32535";
            } else if (issystemflag.equals("11")) {
                str += SystemEnv.getHtmlLabelName(125511, languageid);//"(生成二维码)125511";
            } else if (issystemflag.equals("12")) {
                str += SystemEnv.getHtmlLabelName(125512, languageid);//"(批量生成二维码)125512";
            } else if (issystemflag.equals("13")) {
                str += SystemEnv.getHtmlLabelName(220, languageid);//"(草稿)220";
            } else if (issystemflag.equals("14")) {
                str += SystemEnv.getHtmlLabelName(81470, languageid);//"(卡片页面)81470";
            } else if (issystemflag.equals("170")) {
                str += SystemEnv.getHtmlLabelName(126683, languageid);//"(生成二条形码)126683";
            } else if (issystemflag.equals("171")) {
                str += SystemEnv.getHtmlLabelName(126684, languageid);//"(批量生成条形码)126684";
            } else if (issystemflag.equals("110")) {
                str += SystemEnv.getHtmlLabelName(389877, languageid);//"(地图页面)82639+22967";
            } else if (issystemflag.equals("167")) {
                str += SystemEnv.getHtmlLabelName(384962, languageid);//"(批量设置标签 )";
            } else if (issystemflag.equals("172")) {
                str += SystemEnv.getHtmlLabelName(26382, languageid);//"(批量打印 )";
            } else if (issystemflag.equals("98")) {
                str += SystemEnv.getHtmlLabelName(510160, languageid);//"(批量新增保存 )";
            } else if (issystemflag.equals("99")) {
                str += SystemEnv.getHtmlLabelName(510161, languageid);//"(批量修改保存 )";
            }
            str += ")";
        }

        return str;

    }

    public static String getConditionType(String type) {


        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(type));

        switch (typeNew) {
            case 1:
                text = "普通类型";
                break;
            case 2:
                text = "SQL";
                break;
            case 3:
                text = "JAVA";
                break;

        }


        return text;
    }

    public static String isEnable(String isenable, String enabletag) {

        return enabletag.equals(isenable) ? "是" : "否";


    }


    public static String getHrefType(String hreftype) {

        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(hreftype));

        switch (typeNew) {
            case 1:
                text = "模块";
                break;
            case 2:
                text = "手动输入";
                break;
            case 3:
                text = "模块查询列表";
                break;
            case 4:
                text = "批量修改字段值";
                break;
            case 5:
                text = "回复评论";
                break;
            case 6:
                text = "看板";
                break;
            case 7:
                text = "新建流程";
                break;
            case 8:
                text = "甘特图";
                break;

        }

        return text;
    }


    public static String getInterFaceTypeName(String type) {

        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(type));

        switch (typeNew) {
            case 1:
                text = "数据审批";
                break;
            case 2:
                text = "集成自定义接口";
                break;
            case 3:
                text = "自定义接口动作";
                break;
            case 4:
                text = "DML接口动作";
                break;
            case 5:
                text = "提醒/即时提醒";
                break;
            case 6:
                text = "ESB接口动作";
                break;

        }

        return text;
    }

    public static String isOpen(String type) {

        return type = "1".equals(type) ? "是" : "否";
    }

    public static String getOpentype(String type) {

        String text = "";
        int typeNew = Util.getIntValue(Util.null2String(type));


        switch (typeNew) {
            case 1:
                text = "默认窗口";
                break;
            case 2:
                text = "弹出窗口";
                break;
            case 3:
                text = "其它(js)";
                break;
            case 4:
                text = "右侧展开";
                break;
            case 5:
                text = "下方展开列表小卡片";
                break;


        }

        return text;
    }


    /**
     * 建模获取e10 表名
     *
     * @param modeid       模块id
     * @param formid       表单id  明细表是subformid
     * @param e9_tablename e9原始表名
     * @param isdetial     是否明细表
     * @Author ygd2020
     * @Date 10:10 2023-8-23
     * @Return java.lang.String
     **/
    public static String getE10Tablename(String modeid, String formid, String e9_tablename, String isdetial) {
        boolean repeatMode = ValidObjCacheUtil.isRepeatMode(modeid);


        String suffix = "";
        if (repeatMode) {//如果是重复的模块 /其他的模块 e9 表名加后缀
            suffix = Util.null2String(ValidObjCacheUtil.repeatFormMode.get(modeid));
        }

        if (!isdetial.equals("1")) {//主表标识都用modeid
            formid = "-" + modeid;
        }

        if (isdetial.equals("1") && repeatMode && suffix.equals("eb")) {// 如果明细表 并且还是重复的模块 ，并且后缀都是eb后缀,会导致无法区分明细表表名
            if (ValidObjCacheUtil.deployType != 0) {
                //云端部署的情况下  建模表全部加后缀 主表由于是跟着模块id 拆表 ftab_modeid_租户 不会重复
                // 但是明细表 会是明细表id 如果多个明细表 只用 _eb是无法分开的
                suffix = modeid + "_" + suffix;
            }
        }


        return SqlCache.getTableName(formid, e9_tablename, "1", isdetial, "", suffix);

    }


    public static String getappname(String appid, String appname, String subcompanyid) {

        String sheetname = appid + "_" + appname;
        if (!subcompanyid.equals("")) {
            RecordSet rs = new RecordSet();
            rs.executeQuery("select subcompanyname from HrmSubCompany where id= ? ", subcompanyid);
            if (rs.next()) {
                sheetname += "_" + Util.processBody(rs.getString("subcompanyname"));

            } else {
                new BaseBean().writeLog(appid + "     " + "无分权 ");
            }
        }

        return sheetname;
    }

    public static String getappname(String appid) {

        RecordSet rs = new RecordSet();
        rs.executeQuery("select id,treeFieldName,subcompanyid from modeTreeField  where id = ? order by id", appid);

        String appname = "";
        String subcompanyid = "";
        while (rs.next()) {
            appname = Util.formatMultiLang(rs.getString("treeFieldName"));
            subcompanyid = Util.formatMultiLang(rs.getString("subcompanyid"));
        }

        String sheetname = getappname(appid, appname, subcompanyid);
        return sheetname;
    }


    // 获取e10 的主表表单名称
    public static String getE10MainTablename(String e9_modeid, String e9_tablename) {
        String formid = ValidObjCacheUtil.getFormIdByValidModeId(e9_modeid);
        if (formid == null) {
            return "";
        }
        return getE10Tablename(e9_modeid, formid, e9_tablename, "0");

    }


    // 获取e10 的明细表表单名称
    public static String getE10DetailTablename(String e9_modeid, String e9_sub_form_id, String detailtable) {


        return getE10Tablename(e9_modeid, e9_sub_form_id, detailtable, "1");


    }

    public static String getE9SubFormId(String detailtable) {

        String e9_sub_form_id = "";

        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();
        rs.executeQuery("select id    from workflow_billdetailtable where tablename='" + detailtable + "' ");
        if (rs.next()) {
            e9_sub_form_id = rs.getString("id");
        }

        return e9_sub_form_id;
    }

    public static HashSet<String> localConn = new HashSet<String>() {
        {//本地数据源特征
            this.add("local");
            this.add("");
            this.add("$ECOLOGY_SYS_LOCAL_POOLNAME");
        }
    };

    public static String getdatasourcename(String datasourceid) {

        if (localConn.contains(datasourceid)) {
            return "内部数据源";
        }

        return datasourceid;
    }

    public static String getOrderIdByDtTable(String formid, String detailtable) {
        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();
        rs.executeQuery("select orderid from workflow_billdetailtable where BILLID = ? and  tablename = ?   ", formid, detailtable);

        if (rs.next()) {
            return rs.getString("orderid");
        }
        return "";
    }


    /**
     * 判断字段是否存在
     *
     * @param tablename
     * @param fieldname
     * @return
     */
    public static boolean checkColumnExist(String tablename, String fieldname) {
        if (org.apache.commons.lang.StringUtils.isBlank(tablename) || org.apache.commons.lang.StringUtils.isBlank(fieldname)) {
            return false;
        }
        SourceDBUtil rs = new SourceDBUtil();
        String dbtype = rs.getDbtype();

        rs.executeQuery(getCheckFieldExistSql(tablename, fieldname, dbtype));
        if (rs.next()) {
            int cnt = Util.getIntValue(rs.getString("cnt"), -1);
            if (cnt > 0) {
                return true;
            }
        }
        return false;

    }

    /**
     * 获取判断列是否存存在的sql
     *
     * @param tablename
     * @param fieldName
     * @return
     */
    public static String getCheckFieldExistSql(String tablename, String fieldName, String dbtype) {
        if ("oracle".equalsIgnoreCase(dbtype)) {
            return "select count(1) as cnt from  USER_TAB_COLUMNS WHERE TABLE_NAME = UPPER('" + tablename + "') AND COLUMN_NAME = UPPER('" + fieldName + "') ";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {
            return "select count(*) as cnt from information_schema.columns " +
                    "        where table_name ='" + tablename + "' and column_name ='" + fieldName + "' and TABLE_SCHEMA = (select database())";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return "select count(1) as cnt from information_schema.columns WHERE table_schema=(SELECT current_schema()) " +
                    "and table_name = lower('" + tablename + "') and column_name = lower('" + fieldName + "') ";
        } else {
            return "select count(1) as cnt from syscolumns  where id=object_id('" + tablename + "') and name='" + fieldName + "'";
        }
    }


    public static String getE9Tablename(String formid) {

        String e9_tablename = "";

        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();
        rs.executeQuery("select tablename from workflow_bill where id='" + formid + "' ");
        if (rs.next()) {
            e9_tablename = rs.getString("tablename");
        }

        return e9_tablename;
    }
}
