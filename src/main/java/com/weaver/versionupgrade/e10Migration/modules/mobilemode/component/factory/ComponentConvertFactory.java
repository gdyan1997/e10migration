package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.factory;


import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;

public class ComponentConvertFactory {

    public static AbstractComponentHandler getComponentHandler(ComponentType type, ComponentConvertContext componentConvertContext) {

        //获取该路径下所有类
        try {

            String packagePath = type.getPackagePath();

            if (type.getComponentBean() != null) {
                packagePath = "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.CommonHandler";
            }
//            else if (packagePath.isEmpty()){
//                packagePath = "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler";
//            }

            Class<AbstractComponentHandler> handlerClass = (Class<AbstractComponentHandler>) Class.forName(packagePath);
            AbstractComponentHandler componentHandler = handlerClass.newInstance();
            componentHandler.getHandInfo(componentConvertContext);

            return componentHandler;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
