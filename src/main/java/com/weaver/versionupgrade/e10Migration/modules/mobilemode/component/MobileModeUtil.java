package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 17:33
 */
public class MobileModeUtil {


    public static List<ComponentBean> getRemindConfig(JSONObject item, String prefix) {

        List<ComponentBean> beans = new ArrayList<>();
        try {

            String isremind = Util.null2String(item.get("isremind"));
            if (isremind.equals("1")) {

                String remindtype = Util.null2String(item.get("remindtype"));

                //sql
                if (remindtype.equals("1")) {
                    String reminddatasource = Util.null2String(item.get("reminddatasource"));
                    String remindsql = Util.null2String(item.get("remindsql"));
                    beans.add(
                            new ComponentBean().setHandType(HandType.sqlDev).setResult(prefix + "提醒需要调整数据源："
                                    + E10CubeUtil.getdatasourcename(reminddatasource) + " sql:" + remindsql));

                    //接口
                } else if (remindtype.equals("4")) {
                    String remindapiconfig = Util.null2String(item.get("remindapiconfig"));
                    JSONObject apiConfig = JSON.parseObject(remindapiconfig);
                    JSONObject apiConfig1 = (JSONObject) apiConfig.get("apiConfig");
                    beans.add(
                            new ComponentBean().setHandType(HandType.javaDev).setResult(
                                    prefix + " 提醒需要调整接口 接口信息:" + Util.null2String(apiConfig1.get("api"))));
                }
            }

        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }


        return beans;

    }

    public static List<ComponentBean> getLinkDev(JSONObject item, String prefix) {

        List<ComponentBean> beans = new ArrayList<>();


        try {

            String source = Util.null2String(item.get("source"));

            if (source.equals("3")) {
                String jscode = Util.null2String(item.get("jscode"));

                if (StringUtils.isNotBlank(jscode)) {
                    beans.add(
                            new ComponentBean().setHandType(HandType.jsDev).setResult(
                                    prefix + " 需要调整脚本 " + jscode));
                }


            } else if (source.equals("2")) {
                String custom = Util.null2String(item.get("custom"));
                if (StringUtils.isNotBlank(custom)) {
                    beans.add(
                            new ComponentBean().setHandType(HandType.jsDev).setResult(
                                    prefix + " 需要调整链接 " + custom));

                }
            } else if (source.equals("1")) {


            }

        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }


        return beans;

    }
}
