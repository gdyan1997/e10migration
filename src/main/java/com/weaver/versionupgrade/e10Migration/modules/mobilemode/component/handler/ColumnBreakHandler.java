package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 17:22
 */
public class ColumnBreakHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);

        String btnEnabled = Util.null2String(mecparamjson.get("btnEnabled"));

        if (btnEnabled.equals("1")){
            String btnScript = Util.null2String(mecparamjson.get("btnScript"));
            if (StringUtils.isNotBlank(btnScript)) {
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "分栏需要需要调整脚本 " + btnScript));

            }

        }

        this.setLists(beans);
    }

}
