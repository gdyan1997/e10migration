package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class DataSetHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String sourceType = Util.null2String(mecparamjson.get("sourceType"));
        String tableid = Util.null2String(mecparamjson.get("tableid"));
        String sqlwhere = Util.null2String(mecparamjson.get("sqlwhere"));

        String datasource = Util.null2String(mecparamjson.get("datasource"));//
        String sql = Util.null2String(mecparamjson.get("sql"));//

        String sourceTableCondition = Util.null2String(mecparamjson.get("sourceTableCondition"));//

        String url = Util.null2String(mecparamjson.get("url"));//


        RecordSet rs = new RecordSet();

        if ("0".equals(sourceType) ) {// 0 表单

            rs.executeQuery("select tablename from workflow_bill where id = ?", tableid);
            if (rs.next()) {

                beans.add(
                        new ComponentBean().setHandType(HandType.sqlDev).setResult("来源表单 ： " + rs.getString("tablename")));

            }

            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult("过滤条件 ： sql" + sqlwhere));


        }else if ("1".equals(sourceType)) {// sql

            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult(" sql   数据源 ：" + E10CubeUtil.getdatasourcename(datasource) + "  sql:" + sql));

        }else if ("2".equals(sourceType)) {// 自定义url

            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult("来源自定义URL ： " + url));

        } else if ("3".equals(sourceType)) {//接口

            JSONObject apiConfig = (JSONObject) mecparamjson.get("apiConfig");


            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult(
                            " 来源 接口  接口信息：" + Util.null2String(apiConfig.get("api"))));


        }


        if (StringUtils.isNotBlank(sourceTableCondition)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult(" sql条件 需要调整  ： " + sourceTableCondition));

        }


        this.setLists(beans);

    }

}
