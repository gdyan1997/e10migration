package com.weaver.versionupgrade.e10Migration.modules.cube.util;

import com.weaver.versionupgrade.customRest.db.DBUtil;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;
import weaver.conn.RecordSet;

import weaver.general.BaseBean;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ygd2020
 * @date 2024-1-23 15:46
 * Description:
 **/
public class ValidObjCacheUtil {

    public static ConcurrentHashMap<String, String> modeExtendForm = new ConcurrentHashMap<>();//是否虚拟表单， key表单id 值 1 是本地虚拟表单，2是外部虚拟表单  不在这个表里就是实体表单

    public static ConcurrentHashMap<String, String> validApp = new ConcurrentHashMap<>();//有效的应用

    public static ConcurrentHashMap<String, String> validMode = new ConcurrentHashMap<>();//有效的模块

    public static ConcurrentHashMap<String, String> expireMode = new ConcurrentHashMap<>();//虚拟模块


    public static ConcurrentHashMap<String, String> validCustomSearch = new ConcurrentHashMap<>();//有效的查询

    public static ConcurrentHashMap<String, String> expireCustomSearch = new ConcurrentHashMap<>();//虚拟表的查询

    //有效的浏览按钮id mode_browser 缓存
    public static ConcurrentHashMap<String, String> validBrowserId = new ConcurrentHashMap<>();//有效的建模浏览框


    public static ConcurrentHashMap<String, String> repeatFormMode = new ConcurrentHashMap<>();//多个模块引用同一个表单 导致表单名称变化

    public static boolean isinit = false;

    public static int deployType;
    public static String workflowcubeissamedb;

    /**
     * 是否缓存
     *
     * @param
     * @return
     */
    public static boolean isloadCache() {

        if (!isinit) {
            initCache();
        }
        return true;
    }

    /**
     * 初始化建模有效的数据
     *
     * @param
     * @return
     */
    public static synchronized void initCache() {
        if (isinit) {
            return;
        }


        SourceDBUtil rs = new SourceDBUtil();


        rs.executeQuery("select FORMID,VDATASOURCE  from modeformextend WHERE ISVIRTUALFORM = 1");

        while (rs.next()) {

            String formid = rs.getString("FORMID");
            String vdatasource = rs.getString("VDATASOURCE");

            String value = "";

            if (vdatasource.equals("$ECOLOGY_SYS_LOCAL_POOLNAME")) {//本地数据源

            } else {
                value = "2";//外部数据源
            }
            modeExtendForm.put(formid, value);

        }


        rs.executeQuery("select id,treeFieldName from modeTreeField WHERE " + DBCacheUtil.getSourceDB_ISNULL_FUN() + "(isdelete,0)!=1  ");

        while (rs.next()) {

            validApp.put(rs.getString("id"), rs.getString("treeFieldName"));

        }

        //仅按照是否废弃进行排除  其余通过配置文件限制模块

        rs.executeQuery("select id,modetype,formid,isdelete from modeinfo WHERE 1=1 ");//不是删除的模块，表单id小于0

        while (rs.next()) {

            String isdelete = rs.getString("isdelete");
            if (isdelete.equals("1")) {// 模块被废弃
                continue;
            }
            String modetype = rs.getString("modetype");
            if (!validApp.containsKey(modetype)) {//有效的应用不包含 跳过
                continue;
            }

            String formid = rs.getString("formid");


            if (formid.startsWith("-") && !modeExtendForm.containsKey(formid)) {//负号开头说明formid<0 是自定义表单，并且不是虚拟表
                validMode.put(rs.getString("id"), formid);
            } else {
                expireMode.put(rs.getString("id"), formid);//失效的模块
            }
        }

//        remove_ecme_modeid();//移除掉ecme 的表单，他们那边不存建模的模块id 没办法过滤了，先把模板排除掉

        remove_edcTable_modeid();//移除掉数据中心的模块id  无需迁移

        remove_other_modeid();// 移除掉非标的建模表不做迁移

        rs.executeQuery("select id, appid, modeid, formid from mode_customsearch where 1=1");//存在导入失败的模块  modeid = null || modeid = 0

        while (rs.next()) {

            String appid = rs.getString("appid");

            if (!validApp.containsKey(appid)) {// 不是有效的应用 查询直接跳过
                continue;
            }

            String modeid = rs.getString("modeid");

            if (validMode.containsKey(modeid)) {
                validCustomSearch.put(rs.getString("id"), rs.getString("appid"));
            } else {//不是有效的就是失效的
                expireCustomSearch.put(rs.getString("id"), rs.getString("appid"));
            }

        }

        if (!validMode.isEmpty()) {// 存在有效的模块  再进行重复模块的判定 不然sql会报错
            rs.executeQuery("select a.id as modeid ,b.id as formid,b.TABLENAME,a.MODETYPE as appid  " +
                    "from modeinfo a join  workflow_bill b on a.FORMID = b.id  where formid in (select formid  " +
                    "from modeinfo where   " + getSubINClause(String.join(",", validMode.keySet()), "id", "in", 999) + "  group by formid having count(*) >1)");//多个模块引用同一个表单 导致表单名称变化

            while (rs.next()) {

                if (validApp.containsKey(rs.getString("appid")) && validMode.containsKey(rs.getString("modeid"))
                        && !modeExtendForm.containsKey(rs.getString("formid"))) {//有效应用 && 自定义表单 && 有效的模块

                    repeatFormMode.put(rs.getString("modeid"), rs.getString("modeid"));

                }
            }
        }


        if (checkServiceDb()) {

            addWorkflowFromRepeat();

        }


        if (ConfigUtil.isE9()) {
            HashSet<String> browserType = new HashSet<>();
            rs.executeQuery("select id,showname from mode_browser order by id ");

            while (rs.next()) {

                String id = Util.null2String(rs.getString("id"));
                String showname = Util.null2String(rs.getString("showname"));


                if (browserType.contains(showname)) {
                    continue;
                } else {
                    browserType.add(showname);
                }

                validBrowserId.put(id, showname);
            }

        }

        new BaseBean().writeLog("初始化的有效应用数量有 ：" + validApp.size(), 23000000);
        new BaseBean().writeLog("初始化的有效应用id = " + String.join(",", validApp.keySet()), 23000000);

        new BaseBean().writeLog("初始化的有效模块数量有 ：" + validMode.size(), 23000000);
        new BaseBean().writeLog("初始化的有效模块id = " + String.join(",", validMode.keySet()), 23000000);

        new BaseBean().writeLog("初始化的有效查询数量有 ：" + validCustomSearch.size(), 23000000);
        new BaseBean().writeLog("初始化的有效查询id = " + String.join(",", validCustomSearch.keySet()), 23000000);

        isinit = true;
    }

    public static boolean checkServiceDb() {

        BaseBean baseBean = new BaseBean();
        workflowcubeissamedb = Util.null2String(baseBean.getPropValue("tenant", "workflowcubeissamedb"), "1");
        deployType = Util.getIntValue(baseBean.getPropValue("tenant", "deployType"), 0);

        SourceDBUtil sourceDBUtil = new SourceDBUtil();

        String e9toe10numberinfo = DBUtil.getCheckTableExistSql("finkdb_paramsetting", sourceDBUtil.getDbtype());
        sourceDBUtil.execute(e9toe10numberinfo);
        if (sourceDBUtil.next()) {
            String count = sourceDBUtil.getString("count");
            if ("1".equalsIgnoreCase(count)) {
                sourceDBUtil.executeQuery("select tenant_key,deploytype from finkdb_paramsetting");
                if (sourceDBUtil.next()) {
                    deployType = Util.getIntValue(sourceDBUtil.getString("deploytype"), 0);
                }
            }
        }

        e9toe10numberinfo = DBUtil.getCheckTableExistSql("e10transclazzvarmap", sourceDBUtil.getDbtype());
        sourceDBUtil.execute(e9toe10numberinfo);
        if (sourceDBUtil.next()) {
            String count = sourceDBUtil.getString("count");
            if ("1".equalsIgnoreCase(count)) {

                sourceDBUtil.executeQuery("select v_key,v_value\n" +
                        "from e10transclazzvarmap where v_key = 'workflowcubeissamedb");

                while (sourceDBUtil.next()) {

                    String v_value = sourceDBUtil.getString("v_value");
                    workflowcubeissamedb = Util.null2String(v_value, "1");
                }
            }
        }


        return "1".equals(workflowcubeissamedb);
    }

    public static int getDeployType() {
        isloadCache();
        return deployType;
    }


    private static void addWorkflowFromRepeat() {

        SourceDBUtil rs = new SourceDBUtil();

        rs.executeQuery("select modeinfo.id\n" +
                "from modeinfo\n" +
                "         join workflow_base on modeinfo.FORMID = workflow_base.FORMID");

        while (rs.next()) {

            String id = rs.getString("id");
            if (!repeatFormMode.containsKey(id)) {
                repeatFormMode.put(id, "eb");
            }
        }

        if (deployType != 0) {//云端部署的情况下  建模表全部加后缀

            rs.executeQuery("select  id  from modeinfo ");

            while (rs.next()) {

                String id = rs.getString("id");
                repeatFormMode.put(id, "eb");
            }
        }

    }


    public static void remove_edcTable_modeid() {

        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select modeinfo.id ,workflow_bill.TABLENAME,workflow_bill.id as formid\n" +
                "from modeinfo join workflow_bill on modeinfo.FORMID = workflow_bill.id where TABLENAME like 'edc_%'");
        while (rs.next()) {
            String modeid = rs.getString("id");
            String tablename = rs.getString("tablename");
            String formid = rs.getString("formid");

            if (isEDCTable(formid)) {//如果是数据中心的表单  建模无需迁移
                validMode.remove(modeid);
                expireMode.put(modeid, modeid);//有效的移除 就要放到无效的里边去
            }
        }


    }

    /**
     * 传入表单id，判断是否是数据中心的表单
     * 建模使用 如果是数据中心的表单  建模无需迁移
     * 属于数据中心的表单  返回 true, 其他返回false
     **/
    public static boolean isEDCTable(String formid) {

        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select formid from edc_form_page where formid = ?", formid);
        if (rs.next()) {
            return true;
        }

        return false;

    }

    public static void remove_ecme_modeid() {

        ArrayList<String> modemark = new ArrayList<String>() {{
            add("'crm'");
            add("'govern'");
            add("'prj'");
            add("'workrelateplan'");
            add("'workrelate'");
            add("'workrelategoal'");
            add("'cpt'");
            add("'blog'");
        }};


        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select modeinfo.id from ecme_modeinfo join modeinfo on modeinfo.modetype = ecme_modeinfo.id" +
                " where  ecme_modeinfo.modemark in (" + String.join(",", modemark) + ")");
        while (rs.next()) {
            String modeid = rs.getString("id");

            validMode.remove(modeid);
            expireMode.put(modeid, modeid);//有效的移除 就要放到无效的里边去
        }
    }

    public static void remove_other_modeid() {

        SourceDBUtil rs = new SourceDBUtil();

        HashSet<String> albummode = new HashSet<>();

        rs.executeQuery("select modeinfo.id  from modeinfo join workflow_bill on FORMID = workflow_bill.ID  where TABLENAME = 'album_category'");

        while (rs.next()) {
            String id = rs.getString("id");
            albummode.add(id);
        }

        rs.executeQuery("select modeinfo.id  from modeinfo join workflow_bill on FORMID = workflow_bill.ID  where TABLENAME = 'album_base'");

        while (rs.next()) {
            String id = rs.getString("id");
            albummode.add(id);
        }

        if (albummode.size() < 2) {// 小于两个 说明没有相册两个表的数据 清空掉 不迁了
            albummode.clear();
        }
        for (String id : albummode) {
            validMode.remove(id);
            expireMode.put(id, id);//有效的移除 就要放到无效的里边去
        }

    }


    public static boolean isValidCustomSearch(String customid) {

        isloadCache();


        return validCustomSearch.containsKey(customid);

    }

    public static String getFormIdByValidModeId(String e9_modeid) {

        isloadCache();


        return validMode.get(e9_modeid);
    }

    public static boolean isExpireCustomSearch(String customid) {

        isloadCache();


        return expireCustomSearch.containsKey(customid);

    }

    public static boolean isValidMode(String modeid) {

        isloadCache();


        return validMode.containsKey(modeid);

    }

    public static boolean isExpireMode(String modeid) {

        isloadCache();

        return expireMode.containsKey(modeid);

    }

    public static boolean isValidApp(String appid) {

        isloadCache();


        return validApp.containsKey(appid);

    }

    public static boolean isVirtualForm(String e9_formid) {

        isloadCache();


        return modeExtendForm.containsKey(e9_formid);

    }

    public static boolean isRepeatMode(String modeid) {

        isloadCache();


        return repeatFormMode.containsKey(modeid);

    }


    /**
     * @param browserId mode_browser.id
     * @Author ygd2020
     * @Date 18:55 2023-2-9
     * @Return boolean
     **/
    public static boolean isValidBrowser(String browserId) {
        if (!ConfigUtil.isE9()) {// 如果不是e9 那就都是有效的浏览按钮
            return true;
        }

        isloadCache();

        return validBrowserId.containsKey(browserId);

    }

    public static String getSubINClause(String strOrg, String columnName, String instr) {
        return getSubINClause(strOrg, columnName, instr, 999);
    }

    /**
     * 当SQL语句IN查询子句列表超过1000，将会查询报错。故在此将In语句分隔成
     * 多个In子句，并用 Or 来进行关联。
     *
     * @param strOrg     IN语句的列表，以逗号分隔。 如：
     * @param columnName IN语句查询的列名
     * @param instr      IN语句关键字，可以是 IN 或 NOT IN
     * @param len        IN语句支持最大长度列表，一般为999
     * @return
     */
    public static String getSubINClause(String strOrg, String columnName, String instr, int len) {
        String[] orglist = StringUtils.split(strOrg, ",");

        List<String> groupList = new ArrayList<String>();
        List<String> tempList = new ArrayList<String>();
        for (int i = 0; i < orglist.length; i++) {
            if (tempList.size() >= len) {
                String tempGroup = StringUtils.join(tempList, ',');
                groupList.add(columnName + " " + instr + " (" + tempGroup + ")");
                tempList = new ArrayList<String>();
            }

            if (StringUtils.isEmpty(orglist[i])) continue;
            tempList.add(orglist[i]);
        }

        if (tempList.size() > 0) {
            String tempGroup = StringUtils.join(tempList, ',');
            groupList.add(columnName + " " + instr + " (" + tempGroup + ")");
        }

        String orand = "IN".equalsIgnoreCase(instr) ? " OR " : " AND ";
        return "(" + StringUtils.join(groupList, orand) + ")";
    }


    public static HashMap<String, String> modecount = new HashMap<String, String>();
    public static HashMap<String, String> modedate = new HashMap<String, String>();
    public static HashMap<String, String> modetablename = new HashMap<String, String>();


    public static String getModeDataCount(String modeid) {

        if (modecount.containsKey(modeid)) {
            return modecount.get(modeid);
        }
        if (!isValidMode(modeid)) {
            return "";
        }

        RecordSet rs2 = new RecordSet();

        String tablename = getmodetablename(modeid);


        if (!E10CubeUtil.checkColumnExist(tablename, "formmodeid")) {
            modecount.put(modeid, "0");
            return "0";
        }

        rs2.executeQuery(" select count(*) as count from " + tablename + "   where formmodeid = ? ", modeid);
        String count = "0";
        if (rs2.next()) {
            count = rs2.getString("count");
        }
        modecount.put(modeid, count);
        return count;
    }

    public static String getmodedatacreatedate(String modeid) {
        if (modedate.containsKey(modeid)) {
            return modedate.get(modeid);
        }
        if (!isValidMode(modeid)) {
            return "";
        }
        RecordSet rs2 = new RecordSet();
        String modedatacreatedate = "";
        String tablename = getmodetablename(modeid);

        if (!E10CubeUtil.checkColumnExist(tablename, "modedatacreatedate")) {
            modecount.put(modeid, "");
            return "";
        }


        rs2.executeQuery(" select max(modedatacreatedate) as modedatacreatedate from " + tablename + "  where formmodeid = ?  ", modeid);
        if (rs2.next()) {
            modedatacreatedate = Util.formatMultiLang(rs2.getString("modedatacreatedate"));
        }

        modedate.put(modeid, modedatacreatedate);
        return modedatacreatedate;
    }

    public static String getmodetablename(String modeid) {
        if (modetablename.containsKey(modeid)) {
            return modetablename.get(modeid);
        }

        if (!isValidMode(modeid)) {
            return "";
        }
        RecordSet rs2 = new RecordSet();

        rs2.executeQuery("select workflow_bill.tablename from modeinfo join workflow_bill on  modeinfo.formid = workflow_bill.id and modeinfo.id = ?", modeid);

        String tablename = "";
        if (rs2.next()) {
            tablename = rs2.getString("tablename");
        }


        modetablename.put(modeid, tablename);
        return tablename;
    }


}
