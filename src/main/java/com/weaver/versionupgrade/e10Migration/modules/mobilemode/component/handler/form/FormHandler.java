package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class FormHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String formtype = Util.null2String(mecparamjson.get("formtype"));
        String formsuburl = Util.null2String(mecparamjson.get("formsuburl"));
        String fieldSourceType = Util.null2String(mecparamjson.get("fieldSourceType"));// 1  数据来源字段 2  自定义url
        String datasource = Util.null2String(mecparamjson.get("datasource"));//
        String tablename = Util.null2String(mecparamjson.get("tablename"));//
        String fieldSourceURL = Util.null2String(mecparamjson.get("fieldSourceURL"));//


        if ("1".equals(formtype)||"2".equals(formtype)) {//1  表单建模  2 创建流程
//            beans.add(
//                    new ComponentBean().setHandType(HandType.javaDev).setResult("表单提交类型自定义URL ： " + formsuburl));


        }else if ("3".equals(formtype)) {// 自定义url


            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult("表单提交类型自定义URL ： " + formsuburl));

        }



        if ("1".equals(fieldSourceType)) {
            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult("表单字段来源->表单字段 ：  数据源： " + E10CubeUtil.getdatasourcename(datasource) + "  字段表表名： " + tablename));

        }


        if ("2".equals(fieldSourceType)) {
            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult("表单字段来源->自定义URL ：  " + fieldSourceURL));
        }



        String validateScript = Util.null2String(mecparamjson.get("validateScript"));
        String pageLoadedValidateScript = Util.null2String(mecparamjson.get("pageLoadedValidateScript"));



        if (StringUtils.isNotBlank(validateScript)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.jsDev).setResult(" 验证脚本 需要调整  ： " + validateScript));

        }

        if (StringUtils.isNotBlank(pageLoadedValidateScript)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.jsDev).setResult(" 加载脚本 需要调整  ： " + pageLoadedValidateScript));

        }




        this.setLists(beans);

    }

}
