package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.MobileModeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 17:44
 */
public class TabBarHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);


        JSONArray btn_datas = (JSONArray) mecparamjson.get("datas");


        for (Object btnData : btn_datas) {
            JSONObject Item = (JSONObject) btnData;


            String prefix="标签栏 标签:"+Item.get("showname")+" ";

            beans.addAll(MobileModeUtil.getLinkDev(Item,prefix ));
            beans.addAll(MobileModeUtil.getRemindConfig(Item,prefix));
        }





        this.setLists(beans);
    }

}
