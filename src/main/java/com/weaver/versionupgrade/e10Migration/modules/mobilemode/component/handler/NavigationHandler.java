package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.MobileModeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 15:31
 */
public class NavigationHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);



        JSONArray btns = (JSONArray) mecparamjson.get("nav_items");
        for (Object btn : btns) {
            JSONObject jsonObject = (JSONObject) btn;
     /*       String source = Util.null2String(jsonObject.get("source"));
            if (source.equals("3")){

            String customScript = Util.null2String(jsonObject.get("jscode"));
            String btnText = Util.null2String(jsonObject.get("uiname"));
            if (StringUtils.isNotBlank(customScript)) {
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "导航栏>导航:" + btnText + " 需要调整脚本 "  + customScript));
            }

            }
*/

            String btnText = Util.null2String(jsonObject.get("uiname"));
            beans.addAll(MobileModeUtil.getLinkDev(jsonObject,"导航栏>导航:" + btnText ));
            beans.addAll(MobileModeUtil.getRemindConfig(jsonObject,"导航栏>导航:" + btnText ));



        }


        this.setLists(beans);
    }

}
