package com.weaver.versionupgrade.e10Migration.modules;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.beans.BeanUtils;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;

import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/11 14:27
 */
public class SB2ExcleWrite {
    /**
     * 统一在这个方法里面封装excel需要的table
     * @param writeEntity
     * @return
     */
    public static String excelWrite_html(ExcelWriteEntity writeEntity) {
        ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
        String filePath = excelWrite.excelWrite(writeEntity);

        return filePath;
    }

    public static void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }

}
