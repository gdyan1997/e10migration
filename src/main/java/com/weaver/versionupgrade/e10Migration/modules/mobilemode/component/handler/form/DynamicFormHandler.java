package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class DynamicFormHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String sourceType = Util.null2String(mecparamjson.get("sourceType"));
        String sourceTableId = Util.null2String(mecparamjson.get("sourceTableId"));

        String saveType = Util.null2String(mecparamjson.get("saveType"));

        String saveTableName = Util.null2String(mecparamjson.get("saveTableName"));//

        String saveDataSource = Util.null2String(mecparamjson.get("saveDataSource"));//
        String sourceTableCondition = Util.null2String(mecparamjson.get("sourceTableCondition"));//
        String saveUrl = Util.null2String(mecparamjson.get("saveUrl"));//
        String sourceUrl = Util.null2String(mecparamjson.get("sourceUrl"));//


        RecordSet rs = new RecordSet();

        if ("0".equals(sourceType) ) {// 0 表单

            rs.executeQuery("select tablename from workflow_bill where id = ?", sourceTableId);
            if (rs.next()) {

                beans.add(
                        new ComponentBean().setHandType(HandType.sqlDev).setResult("来源表单 ： " + rs.getString("tablename")));

            }

        }else if ("2".equals(sourceType)) {// 自定义url

            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult("来源自定义URL ： " + sourceUrl));

        } else if ("3".equals(sourceType)) {//接口

            JSONObject apiConfig = (JSONObject) mecparamjson.get("sourceApiConfig");


            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult(
                            " 来源 接口  接口信息：" + Util.null2String(apiConfig.get("api"))));


        }


        if (StringUtils.isNotBlank(sourceTableCondition)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult(" sql条件 需要调整  ： " + sourceTableCondition));

        }


        if ("1".equals(saveType)) {// 存储来源表单
            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult(" 数据存储->存储方式 : 表单   数据源 ：" + E10CubeUtil.getdatasourcename(saveDataSource) + "  表单:" + saveTableName));

        } else if (StringUtils.isNotBlank(saveUrl)){
            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult(" 数据存储->存储方式 : url   ：" + saveUrl));

        }
        this.setLists(beans);

    }

}
