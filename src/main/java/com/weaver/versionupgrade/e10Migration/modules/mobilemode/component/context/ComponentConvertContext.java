package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context;


import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentType;

/**
 * 主要用来传输数据
 * 包括一些映射idMap之类的，可以自由添加配置信息
 * @author joye
 */
public class ComponentConvertContext {

    /**
     * E9组件type
     */
    private String E9CompType;
    ComponentType componentType;

    /**
     * e9组件id
     */
    private String E9CompId;

    private String E9pageId;

    private String mecparam;

    public String getE9CompType() {
        return E9CompType;
    }

    public void setE9CompType(String e9CompType) {
        E9CompType = e9CompType;
    }

    public String getE9CompId() {
        return E9CompId;
    }

    public void setE9CompId(String e9CompId) {
        E9CompId = e9CompId;
    }

    public String getE9pageId() {
        return E9pageId;
    }

    public void setE9pageId(String e9pageId) {
        E9pageId = e9pageId;
    }

    public String getMecparam() {
        return mecparam;
    }

    public void setMecparam(String mecparam) {
        this.mecparam = mecparam;
    }

    public String E9CompType() {
        return E9CompType;
    }

    public ComponentType getComponentType() {
        return componentType;
    }

    public ComponentConvertContext setComponentType(ComponentType componentType) {
        this.componentType = componentType;
        return this;
    }

    public String E9CompId() {
        return E9CompId;
    }

    public String E9pageId() {
        return E9pageId;
    }

    public String mecparam() {
        return mecparam;
    }
}
