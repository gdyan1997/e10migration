package com.weaver.versionupgrade.e10Migration.modules.cubrowser;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.weaver.version.upgrade.util.sql.BrowserSQLUtil;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;
import org.apache.poi.ss.usermodel.*;
import weaver.conn.RecordSet;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelFunctionBean;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.BaseBean;
import weaver.general.Util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @program: e10migration
 * @description:
 * @author: yhq
 * @create: 2024-08-28
 **/
public class CustomBrowserExport {
    private static String[] qysArr = new String[]{"qysSignAction", "qysPublicSignAction", "qysPublicTemplate", "qysPrivateTemplate",
            "qysPrivateCompany", "qysPrivatePhysicalCategory", "qysPrivateElectronicCategory", "qysPublicElectronicCategory",
            "qysSealCategory", "qysSealEditCategory", "qysSealMakeCategory", "qysPrivateSealSpecSample", "qysPrivateSealSpec",
            "qysPrivateSealType", "qysPrivateElectronicSeal", "qysPublicElectronicSeal", "qysPhysicsSeal"};
    //列头
    List<String> commonhead = new ArrayList<>(Arrays.asList("处理情况", "唯一标识", "名称", "浏览按钮值转换类型", "E9所属表", "E10所属表", "查询sql", "描述信息", "引用位置_表单字段", "引用位置_流程后台设置", "引用位置_模块自定义字段"));
    //缓存标准表映射
    private static Map<String, List<Object>> tablefiledMappingMap;
    //标准表集合
    private static HashSet<String> standardTableSet;
    //系统内表单名
    private static HashSet<String> standardBillNames;
    static {
        final String fieldName = new BaseBean().getPropValue("sqlreplace", "e9toe10fieldmapping");
        tablefiledMappingMap = JSONObject.parseObject(fieldName, new TypeReference<Map<String, List<Object>>>() {});

        final String e9sysfield = new BaseBean().getPropValue("sqlreplace", "e9sysfield");
        standardTableSet = JSONObject.parseObject(e9sysfield, new TypeReference<HashSet<String>>() {});
        standardTableSet = standardTableSet.stream().map(s -> s.substring(0, s.indexOf("|sys|"))).map(String::toLowerCase).map(String::toLowerCase).collect(Collectors.toCollection(HashSet::new));
    }
    /**
     * 功能维度
     */
    public String doBrowserAll(String path) {
        try {
            if (CollectionUtils.isEmpty(standardBillNames)) {
                standardBillNames = getSystemBillName();
            }
            //2.缓存标准表映射
            if (tablefiledMappingMap == null || tablefiledMappingMap.size() == 0) {
                throw new RuntimeException("sqlreplace配置文件不存在");
            }
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("自定义浏览按钮信息统计", path);
            getJCbrowser(writeEntity, "集成自定义浏览按钮");
            getCubBrowser(writeEntity, "建模自定义浏览按钮");
            getCubTreeBrowser(writeEntity, "建模树形自定义浏览按钮");

            String filePath = "";
            if (writeEntity.getSheets().size() > 0) {
                ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
                CellStyle bodyStyle = getBodyStyle(excelWrite.workbook);
                excelWrite.setBodystyle(bodyStyle);
                filePath = excelWrite.excelWrite(writeEntity);
            }
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
            new weaver.general.BaseBean().writeLog("", e);
        }
        return "";
    }

    /**
     * 集成
     *
     * @param writeEntity
     * @param sheetname
     */
    private void getJCbrowser(ExcelWriteEntity writeEntity, String sheetname) {
        List<List<String>> values = parseCustomerBrowserInfos();
        addExcelModal(writeEntity, sheetname, sheetname, commonhead, values);
    }

    /**
     * 建模
     *
     * @param writeEntity
     * @param sheetname
     */
    private void getCubBrowser(ExcelWriteEntity writeEntity, String sheetname) {
        List<List<String>> values = parseCubeBrowserInfos();
        addExcelModal(writeEntity, sheetname, sheetname, commonhead, values);
    }

    /**
     * 建模树形
     *
     * @param writeEntity
     * @param sheetname
     */
    private void getCubTreeBrowser(ExcelWriteEntity writeEntity, String sheetname) {
        List<List<String>> values = parseCubeTreeBrowserInfos();
        addExcelModal(writeEntity, sheetname, sheetname, commonhead, values);
    }

    private List<List<String>> parseCustomerBrowserInfos() {
        List<List<String>> values = new ArrayList<>();
        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        String dbType = sourceDBUtil.getDbtype();
        if (dbType.equalsIgnoreCase("postgresql")) {
            sourceDBUtil.executeQuery("select * from datashowset  where (customid=0  or customid is null) order by id  DESC");
        } else {
            sourceDBUtil.executeQuery("select * from datashowset  where (customid=0  or customid = '' or customid is null) order by id  DESC");
        }
        while (sourceDBUtil.next()) {

            String sign = Util.null2String(sourceDBUtil.getString("showname"));//标识
            String datasourceid = Util.null2String(sourceDBUtil.getString("DATASOURCEID")).replace("datasource.", "");//数据源
            String name = Util.formatMultiLang(Util.null2String(sourceDBUtil.getString("name")));//名称
            String sqltext = Util.null2String(sourceDBUtil.getString("SQLTEXT"));
            String showtype = Util.null2String(sourceDBUtil.getString("SHOWTYPE"));//1 列表式 ；2 树形； 3 自定义页面
            String showclass = Util.null2String(sourceDBUtil.getString("SHOWCLASS"));//	1 浏览框 2 查询页面
            String keyfield = Util.null2String(sourceDBUtil.getString("keyfield")).toLowerCase();//表单中的主键字段
            if (StringUtils.isBlank(sign)) {
                continue;
            }
            //已放入标准脚本中初始化
            if (Arrays.asList(qysArr).contains(sign)) {
                continue;
            }

            System.out.println("主键字段前keyfield = " + keyfield);
            if (StringUtils.isBlank(keyfield)) {
                //从sqltext中解析第一个字段
                keyfield = findFirstField(sqltext);
            }
            System.out.println("===集成自定义浏览按钮处理=== ");
            System.out.println("标识sign = " + sign);
            System.out.println("数据源datasourceid = " + datasourceid);
            System.out.println("checkIsLocalDataSource(datasourceid) " + SqlCache.isSystemData(datasourceid));
            System.out.println("名称name = " + name);
            System.out.println("主键字段后keyfield = " + keyfield);
            String e9Table;
            String e10Table;
            Integer browserValueType;
            Boolean isView = false;
            Boolean sqlParseSuccess = false;
            CustomerBrowserEntity browser = new CustomerBrowserEntity();
            boolean isLocalDataSource = SqlCache.isSystemData(datasourceid);
            //外部数据源一律name处理
            if (!isLocalDataSource) {
                e9Table = "";
                e10Table = "";
                browserValueType = 1;
                //固定的几个
            } else if (sign.equalsIgnoreCase("baseBrowser")) {
                e9Table = "hrmresource";
                e10Table = "employee";
                browserValueType = 0;
            } else if (sign.equalsIgnoreCase("governTask")) {
                e9Table = "govern_task ";
                e10Table = "govern_task ";
                browserValueType = 0;
            } else if (sign.equalsIgnoreCase("governCategory")) {
                e9Table = "govern_Category";
                e10Table = "govern_Category";
                browserValueType = 0;
            } else if (sign.equalsIgnoreCase("governDeptConfig")) {
                e9Table = "hrmdepartment";
                e10Table = "department";
                browserValueType = 0;
            } else {
                HashMap<String, Object> browserBelongTableMap = getBrowserBelongTable("1", keyfield, sqltext);
                e9Table = (String) browserBelongTableMap.get("e9Table");
                e10Table = (String) browserBelongTableMap.get("e10Table");
                browserValueType = (Integer) browserBelongTableMap.get("browserValueType");
                isView = (Boolean) browserBelongTableMap.get("isView");
                sqlParseSuccess = (Boolean) browserBelongTableMap.get("sqlParseSuccess");
                System.out.println("解析返回结果 = " + browserBelongTableMap);
            }
            browser.setLlanbs(sign);
            browser.setSzlx(1);
            browser.setE9zszb(e9Table);
            browser.setE10zszb(e10Table);
            browser.setAnzlx(browserValueType);
            browser.setFromsql(sqltext);

            HashMap<String, ArrayList<String>> resultMap = generateDesc4CustomerBrowser(sign, showclass, showtype);
            ArrayList<String> formList = resultMap.get("form");
            ArrayList<String> settingList = resultMap.get("setting");
            ArrayList<String> cusFieldList = resultMap.get("cus");

            //使用次数
            int useCount = formList.size() + settingList.size() + cusFieldList.size();
            updateAnszlxByCount(browserValueType, browser, useCount);

            ArrayList<String> baseList = new ArrayList<>();
            baseList.add("纬度:集成中心-数据展现集成-自定义浏览按钮");
            baseList.add("浏览按钮:" + name);
            baseList.add("主键字段:" + keyfield);
            baseList.add("是否是本地数据源:" + isLocalDataSource);
            baseList.add("是否是视图:" + isView);
            baseList.add("sql是否解析成功:" + sqlParseSuccess);
            baseList.add(baseList.size(), "引用次数:" + useCount);

            String baseListStr = String.join(System.lineSeparator(), baseList);
            String formListStr = String.join(System.lineSeparator(), formList);
            String settingListStr = String.join(System.lineSeparator(), settingList);
            String cusFieldListStr = String.join(System.lineSeparator(), cusFieldList);

            browser.setDescription(baseListStr);

            //按列组装数据
            ArrayList<String> value = new ArrayList<>();
            //1.是否处理
            boolean hasDone = browser.getAnzlx() != null && (browser.getAnzlx() != 0 || StringUtils.isNoneBlank(e9Table, e10Table));
            value.add(hasDone ? "1" : "0");
            //2.唯一标识
            value.add(browser.getLlanbs());
            //名称
            value.add(name);
            //3.浏览按钮值转换类型
            value.add(browser.getAnzlxName());
            //4.E9所属表
            value.add(browser.getE9zszb());
            //5.E10所属表
            value.add(browser.getE10zszb());
            //6.查询sql
            String sql = BrowserSQLUtil.printSql(browser.getFromsql());
            value.add(sql);
            //7.描述信息
            value.add(baseListStr);
            //8.引用查询
            value.add(formListStr);
            //8.引用查询
            value.add(settingListStr);
            //8.引用查询
            value.add(cusFieldListStr);

            values.add(value);
        }

        values = values.stream().sorted(Comparator.comparingInt(o -> Integer.parseInt(o.get(0)))).map(values1 -> {
            if (values1.get(0).equals("0")) {
                values1.set(0, "未处理");
            } else {
                values1.set(0, "已处理");
            }
            return values1;
        }).collect(Collectors.toList());
        return values;
    }

    private List<List<String>> parseCubeBrowserInfos() {
        List<List<String>> values = new ArrayList<>();
        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        //join mode_custombrowser 排除脏数据
        sourceDBUtil.executeQuery("select a.*, b.MODENAME, c.TREEFIELDNAME as appname\n" +
                                          " from mode_browser a  join mode_custombrowser on a.customid = mode_custombrowser.ID\n" +
                                          "         left join modeinfo b on mode_custombrowser.MODEID = b.id\n" +
                                          "         left join modeTreeField c on mode_custombrowser.APPID = c.id");
        SourceDBUtil rs1 = new SourceDBUtil();
        while (sourceDBUtil.next()) {
            String sign = sourceDBUtil.getString("showname");// sign
            String datasourceid = Util.null2String(sourceDBUtil.getString("datasourceid")).replace("datasource.", "");
            String browserName = Util.formatMultiLang(Util.null2String(sourceDBUtil.getString("name")));
            String modename = Util.formatMultiLang(Util.null2String(sourceDBUtil.getString("modename")));
            String appname = Util.formatMultiLang(Util.null2String(sourceDBUtil.getString("appname")));
            String customid = sourceDBUtil.getString("customid");
            String sqltext = sourceDBUtil.getString("sqltext");
            if (StringUtils.isBlank(sign)) {
                continue;
            }

            String e9Table;
            String e10Table;
            Integer browserValueType;
            Boolean isView = false;
            Boolean sqlParseSuccess = false;
            Boolean nameFlag = false;
            //1.找主键字段 默认id
            String keyfield = "id";
            //2.表单字段是否有设置主键
            rs1.executeQuery("select FIELDNAME  from mode_custombrowserdspfield join workflow_billfield on workflow_billfield.id = FIELDID where CUSTOMID = ? and ISPK =1", customid);
            if (rs1.next()) {
                keyfield = Util.null2String(rs1.getString("FIELDNAME")).toLowerCase();
                //这种情况只能是设置字符类型的字段 一定是name类型
                nameFlag = true;
            }
            //3.虚拟表单是否有设置主键
            rs1.executeQuery("select vprimarykey from modeformextend join mode_custombrowser md on md.formid = ModeFormExtend.formid where id=?", customid);
            if (rs1.next()) {
                keyfield = Util.null2String(rs1.getString("vprimarykey")).toLowerCase();
            }

            CustomerBrowserEntity browser = new CustomerBrowserEntity();
            //外部数据源一律name处理
            boolean isLocalDataSource = SqlCache.isSystemData(datasourceid);
            System.out.println("===建模浏览按钮处理=== ");
            System.out.println("建模浏览按钮标识: " + sign);
            System.out.println("建模浏览按钮keyfield = " + keyfield);
            System.out.println("建模浏览按钮nameFlag = " + nameFlag);
            System.out.println("建模浏览按钮datasourceid = " + datasourceid);
            System.out.println("建模浏览按钮checkIsLocalDataSource(datasourceid) " + isLocalDataSource);
            if (!isLocalDataSource) {
                e9Table = "";
                e10Table = "";
                browserValueType = 1;
            } else {
                if (nameFlag) {
                    e9Table = "";
                    e10Table = "";
                    browserValueType = 1;
                } else {
                    HashMap<String, Object> browserBelongTableMap = getBrowserBelongTable("2", keyfield, sqltext);
                    e9Table = (String) browserBelongTableMap.get("e9Table");
                    e10Table = (String) browserBelongTableMap.get("e10Table");
                    browserValueType = (Integer) browserBelongTableMap.get("browserValueType");
                    isView = (Boolean) browserBelongTableMap.get("isView");
                    sqlParseSuccess = (Boolean) browserBelongTableMap.get("sqlParseSuccess");
                    System.out.println("解析返回结果 = " + browserBelongTableMap);
                }
            }
            browser.setLlanbs(sign);
            browser.setSzlx(1);
            browser.setAnzlx(browserValueType);
            browser.setE9zszb(e9Table);
            browser.setE10zszb(e10Table);
            browser.setFromsql(sqltext);

            HashMap<String, ArrayList<String>> resultMap = generateDesc4ModeBrowser(sign);
            ArrayList<String> formList = resultMap.get("form");
            ArrayList<String> settingList = resultMap.get("setting");
            ArrayList<String> cusFieldList = resultMap.get("cus");
            //使用次数
            int useCount = formList.size() + settingList.size() + cusFieldList.size();
            updateAnszlxByCount(browserValueType, browser, useCount);

            ArrayList<String> baseList = new ArrayList<>();
            baseList.add("纬度:建模引擎-浏览按钮");
            baseList.add("浏览按钮:" + sign);
            baseList.add("所属应用:" + appname);
            baseList.add("所属模块:" + modename);
            baseList.add("主键字段:" + keyfield);
            baseList.add("是否是本地数据源:" + isLocalDataSource);
            baseList.add("是否是视图:" + isView);
            baseList.add("sql是否解析:" + sqlParseSuccess);
            baseList.add(baseList.size(), "引用次数:" + useCount);

            String baseListStr = String.join(System.lineSeparator(), baseList);
            String formListStr = String.join(System.lineSeparator(), formList);
            String settingListStr = String.join(System.lineSeparator(), settingList);
            String cusFieldListStr = String.join(System.lineSeparator(), cusFieldList);

            browser.setDescription(baseListStr);

            //按列组装数据
            ArrayList<String> value = new ArrayList<>();
            //1.是否处理
            boolean hasDone = browser.getAnzlx() != null && (browser.getAnzlx() != 0 || StringUtils.isNoneBlank(e9Table, e10Table));
            value.add(hasDone ? "1" : "0");
            //2.唯一标识
            value.add(browser.getLlanbs());
            //名称
            value.add(browserName);
            //3.浏览按钮值转换类型
            value.add(browser.getAnzlxName());
            //4.E9所属表
            value.add(browser.getE9zszb());
            //5.E10所属表
            value.add(browser.getE10zszb());
            //6.查询sql
            String sql = BrowserSQLUtil.printSql(browser.getFromsql());
            value.add(sql);
            //7.描述信息
            value.add(baseListStr);
            //8.引用查询
            value.add(formListStr);
            //8.引用查询
            value.add(settingListStr);
            //8.引用查询
            value.add(cusFieldListStr);

            values.add(value);
        }

        values = values.stream().sorted(Comparator.comparingInt(o -> Integer.parseInt(o.get(0)))).map(values1 -> {
            if (values1.get(0).equals("0")) {
                values1.set(0, "未处理");
            } else {
                values1.set(0, "已处理");
            }
            return values1;
        }).collect(Collectors.toList());
        return values;
    }

    /**
     * 解析树形浏览按钮
     *
     * @return
     */
    private List<List<String>> parseCubeTreeBrowserInfos() {
        List<List<String>> values = new ArrayList<>();
        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        sourceDBUtil.executeSql("select a.*,b.TREENAME,b.SHOWTYPE from mode_customtreedetail a join mode_customtree b on a.mainid = b.id and b.SHOWTYPE = 1 ");
        while (sourceDBUtil.next()) {
            String nodeId = sourceDBUtil.getString("id");//树节点id
            String mainid = sourceDBUtil.getString("MAINID");//树id
            String tablename = Util.null2String(sourceDBUtil.getString("TABLENAME")).toLowerCase();//树表名
            String tablekey = Util.null2String(sourceDBUtil.getString("TABLEKEY")).toLowerCase();//树节点主键字段
            String datacondition = Util.null2String(sourceDBUtil.getString("DATACONDITION"));//树条件
            String nodename = Util.formatMultiLang(Util.null2String(sourceDBUtil.getString("nodename")));//节点名
            String treename = Util.formatMultiLang(Util.null2String(sourceDBUtil.getString("TREENAME")));//树名
            String sourcefrom = Util.null2String(sourceDBUtil.getString("SOURCEFROM"));//1 模块 2 手动输入
            //只有树不需要解e9表
            String e9Table = tablename;
            String e10Table = null;
            Integer browserValueType = null;
            Boolean isView = false;
            Boolean sqlParseSuccess = false;
            String fromSql = "";

            System.out.println("===树形浏览按钮处理=== ");
            System.out.println("nodeId = " + nodeId);
            System.out.println("树节点主键字段tablekey = " + tablekey);
            System.out.println("树表名tablename = " + tablename);
            System.out.println("树名treename = " + treename);
            System.out.println("sourcefrom = " + sourcefrom);
            if (!StringUtils.isAnyBlank(tablekey, tablename)) {
                //选择框做数据源的情况
                if (tablename.equalsIgnoreCase("workflow_selectitem")) {
                    if (tablekey.equalsIgnoreCase("SELECTVALUE")) {
                        e10Table = "";
                        browserValueType = 1;
                        fromSql = "select " + tablekey + " from " + tablename;
                    }
                } else {
                    fromSql = "select " + tablekey + " from " + tablename;
                    if (StringUtils.isNotBlank(datacondition)) {
                        fromSql += " where " + datacondition;
                    }
                    HashMap<String, Object> browserBelongTableMap = getBrowserBelongTable("3", e9Table, tablekey, fromSql);
                    e10Table = (String) browserBelongTableMap.get("e10Table");
                    browserValueType = (Integer) browserBelongTableMap.get("browserValueType");
                    isView = (Boolean) browserBelongTableMap.get("isView");
                    sqlParseSuccess = (Boolean) browserBelongTableMap.get("sqlParseSuccess");
                }
            }
            CustomerBrowserEntity browser = new CustomerBrowserEntity();
            //树节点id做唯一标识
            browser.setLlanbs(nodeId);
            browser.setSzlx(2);
            browser.setAnzlx(browserValueType);
            browser.setE9zszb(tablename);
            browser.setE10zszb(e10Table);
            browser.setFromsql(fromSql);

            HashMap<String, ArrayList<String>> resultMap = generateDesc4ModeTreeBrowser(mainid);
            ArrayList<String> formList = resultMap.get("form");
            ArrayList<String> settingList = resultMap.get("setting");
            ArrayList<String> cusFieldList = resultMap.get("cus");
            //使用次数
            int useCount = formList.size() + settingList.size() + cusFieldList.size();
            updateAnszlxByCount(browserValueType, browser, useCount);

            ArrayList<String> baseList = new ArrayList<>();
            baseList.add("纬度:建模引擎-树");
            baseList.add("浏览按钮:" + treename);
            baseList.add("树节点ID:" + nodeId);
            baseList.add("树节点名:" + nodename);
            baseList.add("主键字段:" + tablekey);
            baseList.add(1, "是否是视图:" + isView);
            baseList.add(2, "sql是否解析:" + sqlParseSuccess);
            baseList.add(baseList.size(), "引用次数:" + useCount);

            String baseListStr = String.join(System.lineSeparator(), baseList);
            String formListStr = String.join(System.lineSeparator(), formList);
            String settingListStr = String.join(System.lineSeparator(), settingList);
            String cusFieldListStr = String.join(System.lineSeparator(), cusFieldList);

            browser.setDescription(baseListStr);

            //按列组装数据
            ArrayList<String> value = new ArrayList<>();
            //1.是否处理
            boolean hasDone = browser.getAnzlx() != null && (browser.getAnzlx() != 0 || StringUtils.isNoneBlank(e9Table, e10Table));
            value.add(hasDone ? "1" : "0");
            //2.唯一标识
            value.add(browser.getLlanbs());
            //名称
            value.add(nodename);
            //3.浏览按钮值转换类型
            value.add(browser.getAnzlxName());
            //4.E9所属表
            value.add(browser.getE9zszb());
            //5.E10所属表
            value.add(browser.getE10zszb());
            //6.查询sql
            String sql = BrowserSQLUtil.printSql(browser.getFromsql());
            value.add(sql);
            //7.描述信息
            value.add(baseListStr);
            //8.引用查询
            value.add(formListStr);
            //8.引用查询
            value.add(settingListStr);
            //9.引用查询
            value.add(cusFieldListStr);

            values.add(value);
        }

        values = values.stream().sorted(Comparator.comparingInt(o -> Integer.parseInt(o.get(0)))).map(values1 -> {
            if (values1.get(0).equals("0")) {
                values1.set(0, "未处理");
            } else {
                values1.set(0, "已处理");
            }
            return values1;
        }).collect(Collectors.toList());
        return values;
    }

    /**
     * 生成自定义浏览按钮描述
     *
     * @param showname
     * @param showclass
     * @param showtype
     * @return
     */
    public HashMap<String, ArrayList<String>> generateDesc4CustomerBrowser(
            String showname,
            String showclass,
            String showtype) {
        HashMap<String, ArrayList<String>> resultMap = new HashMap<>();

        ArrayList<String> cusFieldList = new ArrayList<>();
        cusFieldList.addAll(getCustomFieldUsed(showname, 1));

        ArrayList<String> setList = new ArrayList<>();
        setList.addAll(getCustomBrowserQuotePlace(showname, 1));
        ArrayList<String> formList = new ArrayList<>();

        SourceDBUtil rs = new SourceDBUtil();//277481,lv,[90]集成中心－解决代码质量问题修复--操作数据库的对象不能作为成员变量
        if ("1".equals(showclass) && ("1".equals(showtype) || "2".equals(showtype))) {
            String sql = "select *\n" +
                    " from (select b.id,\n" +
                    "             0                       as frommmode,\n" +
                    "             b.workflowname,\n" +
                    "             (select fieldlable\n" +
                    "              from workflow_fieldlable\n" +
                    "              where fieldid = ff.fieldid\n" +
                    "                and ff.formid = formid\n" +
                    "                and langurageid = 7) as fieldname\n" +
                    "      from workflow_base b,\n" +
                    "           workflow_formdict fd,\n" +
                    "           workflow_formfield ff\n" +
                    "      where fd.id = ff.fieldid\n" +
                    "        and ff.formid = b.formid\n" +
                    "        and b.isbill = 0\n" +
                    "        and fd.fielddbtype = 'browser." + showname + "'\n" +
                    "\n" +
                    "      union all\n" +
                    "\n" +
                    "      select b.id,\n" +
                    "             0                      as frommmode,\n" +
                    "             b.workflowname,\n" +
                    "             (select labelname\n" +
                    "              from htmllabelinfo\n" +
                    "              where indexid = bf.fieldlabel\n" +
                    "                and languageid = 7) as fieldname\n" +
                    "      from workflow_billfield bf,\n" +
                    "           workflow_base b\n" +
                    "      where bf.billid = b.formid\n" +
                    "        and b.isbill = 1\n" +
                    "        and bf.fielddbtype = 'browser." + showname + "'\n" +
                    "\n" +
                    "      union all\n" +
                    "\n" +
                    "      select a.id,\n" +
                    "             1                      as frommmode,\n" +
                    "             a.modename             as workflowname,\n" +
                    "             (select labelname\n" +
                    "              from htmllabelinfo\n" +
                    "              where indexid = b.fieldlabel\n" +
                    "                and languageid = 7) as fieldname\n" +
                    "      from modeinfo a,\n" +
                    "           workflow_billfield b\n" +
                    "      where a.formid = b.billid\n" +
                    "        and b.fielddbtype = 'browser." + showname + "') r\n" +
                    " order by r.id";
            rs.executeSql(sql);
            // 假设已经创建了一个用于存储 workflowname 和对应 fieldname 的 Map
            Map<String, HashSet<String>> workflowFieldMap = new HashMap<>();
            while (rs.next()) {
                //建模引用
                boolean fromMode = Util.null2String(rs.getString("frommmode")).equals("1");
                String prefixStr = fromMode ? "模块名称: " : "流程名称: ";
                String workflowname = Util.formatMultiLang(rs.getString("workflowname"));
                if (StringUtils.isNotBlank(workflowname)) {
                    workflowname = prefixStr + workflowname;
                }
                String fieldname = rs.getString("fieldname");
                workflowFieldMap.putIfAbsent(workflowname, new HashSet<>());
                workflowFieldMap.get(workflowname).add(fieldname);
            }
            // 遍历 workflowFieldMap，生成输出格式
            for (Map.Entry<String, HashSet<String>> entry : workflowFieldMap.entrySet()) {
                String workflowname = entry.getKey();
                String fieldnames = entry.getValue().stream().collect(Collectors.joining(", ", "  表单字段: {", "}"));
                formList.add(workflowname + fieldnames);
            }
        }

        formList = sortFormList(formList);

        resultMap.put("form", formList);
        resultMap.put("setting", setList);
        resultMap.put("cus", cusFieldList);
        return resultMap;
    }

    /**
     * 建模自定义浏览框
     *
     * @param showname
     * @return
     */
    public HashMap<String, ArrayList<String>> generateDesc4ModeBrowser(
            String showname
    ) {

        HashMap<String, ArrayList<String>> resultMap = new HashMap<>();

        SourceDBUtil rs = new SourceDBUtil();
        SourceDBUtil rs2 = new SourceDBUtil();
        String sql = "select a.billid,b.labelname from workflow_billfield a,HtmlLabelInfo b where a.fielddbtype='browser."
                + showname
                + "' and (a.type = 161 or a.type = 162) and a.fieldlabel=b.indexid and b.languageid=7 order by a.billid asc";
        rs.executeSql(sql);
        Map<String, HashSet<String>> workflowFieldMap = new HashMap<>();
        Map<String, HashSet<String>> modeFieldMap = new HashMap<>();
        while (rs.next()) {
            String formid = Util.null2String(rs.getString("billid"));
            String fieldName = Util.null2String(rs.getString("labelname"));
            //查询相关的流程
            sql = "select id,workflowname,version from workflow_base where formid=" + formid;
            rs2.executeSql(sql);
            while (rs2.next()) {
                String workflowname = Util.formatMultiLang(Util.null2String(rs2.getString("workflowname")));
                int version = Util.getIntValue(rs2.getString("version"), 1);
                workflowname = "流程名称: " + workflowname + "(流程版本:v" + version + ")";
                workflowFieldMap.putIfAbsent(workflowname, new HashSet<>());
                workflowFieldMap.get(workflowname).add(fieldName);
            }
            //查询相关的模块
            sql = "select id,modename from modeinfo where formid=" + formid;
            rs2.executeSql(sql);
            while (rs2.next()) {
                String modeTempName = "模块名称: " + Util.formatMultiLang(Util.null2String(rs2.getString("modename")));
                modeFieldMap.putIfAbsent(modeTempName, new HashSet<>());
                modeFieldMap.get(modeTempName).add(fieldName);
            }
        }

        ArrayList<String> formList = new ArrayList<>();
        for (Map.Entry<String, HashSet<String>> entry : workflowFieldMap.entrySet()) {
            String workflowname = entry.getKey();
            String fieldnames = entry.getValue().stream().collect(Collectors.joining(", ", " 表单字段: {", "}"));
            formList.add(workflowname + fieldnames);
        }
        for (Map.Entry<String, HashSet<String>> entry : modeFieldMap.entrySet()) {
            String modeName = entry.getKey();
            String fieldnames = entry.getValue().stream().collect(Collectors.joining(", ", " 表单字段: {", "}"));
            formList.add(modeName + fieldnames);
        }

        formList = sortFormList(formList);

        ArrayList<String> cusFieldList = new ArrayList<>();
        cusFieldList.addAll(getCustomFieldUsed(showname, 2));

        ArrayList<String> setList = new ArrayList<>();
        setList.addAll(getCustomBrowserQuotePlace(showname, 2));

        resultMap.put("form", formList);
        resultMap.put("setting", setList);
        resultMap.put("cus", cusFieldList);
        return resultMap;
    }

    /**
     * 树形浏览按钮描述
     *
     * @param treeId     树id
     * @return
     */
    public HashMap<String, ArrayList<String>> generateDesc4ModeTreeBrowser(
            String treeId
    ) {
        HashMap<String, ArrayList<String>> resultMap = new HashMap<>();

        SourceDBUtil rs = new SourceDBUtil();
        String sql = "select *\n" +
                " from (select b.id,\n" +
                "             0                       as frommmode,\n" +
                "             b.workflowname,\n" +
                "             (select fieldlable\n" +
                "              from workflow_fieldlable\n" +
                "              where fieldid = ff.fieldid\n" +
                "                and ff.formid = formid\n" +
                "                and langurageid = 7) as fieldname\n" +
                "      from workflow_base b,\n" +
                "           workflow_formdict fd,\n" +
                "           workflow_formfield ff\n" +
                "      where fd.id = ff.fieldid\n" +
                "        and ff.formid = b.formid\n" +
                "        and b.isbill = 0\n" +
                "        and fd.FIELDHTMLTYPE = 3\n" +
                "        and fd.TYPE in (257, 256)\n" +
                "        and fd.FIELDDBTYPE = '" + treeId + "'\n" +
                "\n" +
                "      union all\n" +
                "\n" +
                "      select b.id,\n" +
                "             0                      as frommmode,\n" +
                "             b.workflowname,\n" +
                "             (select labelname\n" +
                "              from htmllabelinfo\n" +
                "              where indexid = bf.fieldlabel\n" +
                "                and languageid = 7) as fieldname\n" +
                "      from workflow_billfield bf,\n" +
                "           workflow_base b\n" +
                "      where bf.billid = b.formid\n" +
                "        and b.isbill = 1\n" +
                "        and bf.FIELDHTMLTYPE = 3\n" +
                "        and bf.TYPE in (257, 256)\n" +
                "        and bf.FIELDDBTYPE = '" + treeId + "'\n" +
                "\n" +
                "      union all\n" +
                "\n" +
                "      select a.id,\n" +
                "             1                      as frommmode,\n" +
                "             a.modename             as workflowname,\n" +
                "             (select labelname\n" +
                "              from htmllabelinfo\n" +
                "              where indexid = b.fieldlabel\n" +
                "                and languageid = 7) as fieldname\n" +
                "      from modeinfo a,\n" +
                "           workflow_billfield b\n" +
                "      where a.formid = b.billid\n" +
                "        and b.FIELDHTMLTYPE = 3\n" +
                "        and b.TYPE in (257, 256)\n" +
                "        and b.FIELDDBTYPE = '" + treeId + "' ) r\n" +
                " order by r.id";
        rs.executeQuery(sql);
        // 假设已经创建了一个用于存储 workflowname 和对应 fieldname 的 Map
        Map<String, HashSet<String>> workflowFieldMap = new HashMap<>();
        while (rs.next()) {
            //建模引用
            boolean fromMode = Util.null2String(rs.getString("frommmode")).equals("1");
            String prefixStr = fromMode ? "模块名称:" : "流程名称:";
            String workflowname = Util.formatMultiLang(rs.getString("workflowname"));
            if (StringUtils.isNotBlank(workflowname)) {
                workflowname = prefixStr + workflowname;
            }
            String fieldname = rs.getString("fieldname");
            workflowFieldMap.putIfAbsent(workflowname, new HashSet<>());
            workflowFieldMap.get(workflowname).add(fieldname);
        }

        ArrayList<String> setList = new ArrayList<>();
        setList.addAll(getCustomBrowserQuotePlace(treeId, 3));

        ArrayList<String> formList = new ArrayList<>();
        // 遍历 workflowFieldMap，生成输出格式
        for (Map.Entry<String, HashSet<String>> entry : workflowFieldMap.entrySet()) {
            String workflowname = entry.getKey();
            String fieldnames = entry.getValue().stream().collect(Collectors.joining(", ", " 表单字段: {", "}"));
            formList.add(workflowname + fieldnames);
        }
        formList = sortFormList(formList);

        ArrayList<String> cusFieldList = new ArrayList<>();
        cusFieldList.addAll(getCustomFieldUsed(treeId, 3));

        resultMap.put("form", formList);
        resultMap.put("setting", setList);
        resultMap.put("cus", cusFieldList);
        return resultMap;
    }

    private static HashMap<String, Object> getBrowserBelongTable(String fromType, String primaryKey, String sourceSql) {
        return getBrowserBelongTable(fromType, null, primaryKey, sourceSql);
    }

    /**
     * 获取浏览按钮主键字段归属表
     * <p>
     * 解析sql 判断主键字段所在表
     * 判断E10table是否是视图 视图跳过
     * 判断值转换类型
     * 1.当前表的id就直接是id类型
     * 2.如果是其他类型字段 关联 workflow_billfield 查询 确认是否是浏览按钮类型，如果是浏览按钮类型根据小类型可以找到对应的具体浏览按钮 ,非浏览按钮类型则为name
     * <p>
     * fromType 1:来自集成 2:建模浏览 3:建模树
     *
     * @return
     */
    private static HashMap<String, Object> getBrowserBelongTable(String fromType, String e9TreeTableName, String primaryKey, String sourceSql) {
        HashMap<String, Object> result = new HashMap<>();
        String e9Table = null;
        String e10Table = null;
        Integer browserValueType = null; //0 id 1 name 2 mix
        Boolean isView = false;
        Boolean sqlParseSuccess = false;
        System.out.println("fromType = " + fromType);
        if (StringUtils.isNoneBlank(primaryKey, sourceSql)) {
            //只有树不需要解e9表
            if ("3".equals(fromType)) {
                e9Table = e9TreeTableName;
            } else {
                System.out.println("数据源sourceSql = " + sourceSql);
                System.out.println("主键primaryKey = " + primaryKey);

                Map<String, Object> tableResult = BrowserSQLUtil.getFieldTableBrowserMenu(sourceSql, primaryKey);
                System.out.println("sql解析结果tableResult = " + tableResult);

                e9Table = (String) tableResult.get("browserMenuTable");//主键所在e9表
                String primaryKeyTemp = (String) tableResult.get("browserMenuField");//主键真实字段
                System.out.println("真实主键字段 = " + primaryKeyTemp);

                // 如果 primaryKeyTemp 不为空并且包含"."，提取最后一个"."后面的部分作为新的主键字段
                if (StringUtils.isNotBlank(primaryKeyTemp)) {
                    primaryKey = primaryKeyTemp.contains(".") ? primaryKeyTemp.substring(primaryKeyTemp.lastIndexOf('.') + 1) : primaryKeyTemp;
                }

                System.out.println("最终主键字段 = " + primaryKey);
                System.out.println("sql解析结果e9Table = " + e9Table);
            }

            if (StringUtils.isNotBlank(e9Table)) {
                //判断是否是视图
                isView = BrowserSQLUtil.checkIsView(e9Table);
                System.out.println("checkIsView = " + isView);
                if (!isView) {
                    sqlParseSuccess = true;
                    SourceDBUtil sb = new SourceDBUtil();
                    //判断是否是系统内表单 暂时不考虑老表单isbill=0 基本不存在将老表单中的数据做自定义浏览按钮的情况 但需要考虑自定义表单明细
                    sb.executeQuery("select b.TABLENAME,\n" +
                                            "       a.*\n" +
                                            " from workflow_billfield a\n" +
                                            "         join workflow_bill b on a.BILLID = b.id\n" +
                                            " where ( UPPER(b.TABLENAME ) = ? and (a.DETAILTABLE is null or a.DETAILTABLE ='') and UPPER(FIELDNAME)= ?)\n" +
                                            "   or ( UPPER(a.DETAILTABLE) = ? and UPPER(FIELDNAME)= ?)",
                                    e9Table.toUpperCase(), primaryKey.toUpperCase(), e9Table.toUpperCase(), primaryKey.toUpperCase());
                    if (sb.next()) {
                        String fieldhtmltype = sb.getString("FIELDHTMLTYPE");
                        String type = sb.getString("TYPE");
                        //自定义单选多选  browser.  自定义树是数字对应的是树的id 非树节点id
                        String fielddbtype = sb.getString("FIELDDBTYPE");
                        //浏览按钮
                        if ("3".equals(fieldhtmltype)) {
                            HashMap<String, Object> e10BrowserInfo = getE10BrowserInfo(fromType, type, fielddbtype, standardTableSet, tablefiledMappingMap);
                            browserValueType = (Integer) e10BrowserInfo.get("browserValueType");
                            e9Table = (String) e10BrowserInfo.get("e9Table");
                            e10Table = (String) e10BrowserInfo.get("e10Table");
                            //选择框　基本没有这么用的　先给name
                        } else if ("5".equals(fieldhtmltype)) {
                            browserValueType = 1;
                            e9Table = "";
                            e10Table = "";
                            //非浏览按钮类型则为name类型
                        } else {
                            browserValueType = 1;
                            e9Table = "";
                            e10Table = "";
                        }
                        System.out.println("找到对应表单字段 fieldhtmltype= " + fieldhtmltype);
                        System.out.println("找到对应表单字段 fielddbtype= " + fielddbtype);
                        System.out.println("找到对应表单字段 type= " + type);
                        System.out.println("找到对应表单字段 browserValueType= " + browserValueType);
                        System.out.println("找到对应表单字段 e9Table= " + e9Table);
                        System.out.println("找到对应表单字段 e10Table= " + e10Table);
                    } else {
                        //业务表单表的冗余字段先判断
                        if (CollectionUtils.isNotEmpty(standardBillNames) && (standardBillNames.contains(e9Table.toLowerCase()) || e9Table.startsWith("uf_") || e9Table.startsWith("formtable_main_"))) {
                            //formtable_main 和 uf_ 都有一些预置的字段 不在billfield表中,需要拿出来单独判断
                            switch (primaryKey.toLowerCase()) {
                                case "id":
                                    browserValueType = 0;
                                    e10Table = e9Table;
                                    break;
                                case "requestid":
                                    browserValueType = 0;
                                    e9Table = "workflow_requestbase";
                                    e10Table = "wfc_requestbase";
                                    break;
                                case "modedatacreater":
                                case "modedatamodifier":
                                    e9Table = "hrmresource";
                                    e10Table = "employee";
                                    browserValueType = 0;
                                    break;
                                case "modedatamodifydatetime":
                                case "modedatacreatedate":
                                    browserValueType = 1;
                                    e9Table = "";
                                    e10Table = "";
                                    break;
                            }
                            System.out.println("表单表 固定字段解析  e9Table = " + e9Table);
                            System.out.println("表单表 固定字段解析   e10Table = " + e10Table);
                            System.out.println("表单表 固定字段解析   browserValueType = " + browserValueType);
                            //是标准系统表
                        } else if (standardTableSet.contains(e9Table.toLowerCase())) {
                            List<Object> tableMappings = Optional.ofNullable(tablefiledMappingMap.get(e9Table.toLowerCase())).orElse(new ArrayList<>());
                            for (Object o : tableMappings) {
                                JSONObject obj = (JSONObject) o;
                                String e9TableField = obj.getString("e9TableField");
                                String fieldType = obj.getString("fieldType");//1.当前表主键 2.外键字段 3.原值 4:json
                                if (!primaryKey.equalsIgnoreCase(e9TableField)) {
                                    continue;
                                }
                                System.out.println("配置文件中 业务表主键字段的类型 fieldType = " + fieldType);
                                //如果该字段是当前表的主外键则一定是ID类型转换
                                if (fieldType.equals("1")) {
                                    //wjh配置文件捞的有问题 部分表单独写死
                                    if (e9Table.equalsIgnoreCase("workflow_flownode") && primaryKey.equalsIgnoreCase("nodeid")) {
                                        e9Table = "workflow_nodebase";
                                        e10Table = "wfp_node";
                                    } else {
                                        //e9table不变
                                        e10Table = Util.null2String(obj.getString("e10Tablename"));
                                        if (e10Table.indexOf(".") > -1) {
                                            e10Table = e10Table.substring(e10Table.indexOf(".") + 1);
                                        }
                                    }
                                    browserValueType = 0;
                                    break;
                                } else if (fieldType.equals("2")) {
                                    browserValueType = 0;
                                    //e9relevancytable真正的关联表的name 比如用hrm表中的companyid做主键,e9table应为hrmdepartment
                                    e9Table = Util.null2String(obj.getString("e9relevancytable"));
                                    e10Table = Util.null2String(obj.getString("relevancytable"));
                                    if (e10Table.indexOf(".") > -1) {
                                        e10Table = e10Table.substring(e10Table.indexOf(".") + 1);
                                    }
                                    break;
                                } else if (fieldType.equals("3")) {
                                    browserValueType = 1;
                                    break;
                                } else if (fieldType.equals("4")) {
                                    //e9的映射类型
                                    browserValueType = 3;
                                    break;
                                }
                                System.out.println("配置文件中 业务表对应的  e9Table = " + e9Table);
                                System.out.println("配置文件中 业务表对应的  e10Table = " + e10Table);
                            }
                        } else {
                            //独立表+ 可能是手动库中新增的 无法判断
                            System.out.println("独立表 = " + e9Table);
                        }
                    }
                } else {
                    System.out.println("是sql视图不处理 ");
                }
            } else {
                System.out.println("sql解析失败 == primaryKey:" + primaryKey + " sourceSql:" + sourceSql);
            }
        }
        result.put("e9Table", e9Table);
        result.put("e10Table", e10Table);
        result.put("browserValueType", browserValueType);
        result.put("isView", isView);
        result.put("sqlParseSuccess", sqlParseSuccess);
        return result;
    }

    /**
     * 小类型获取浏览按钮信息
     *
     * @param fromType             1集成 2建模 3树
     * @param fieldType
     * @param browserKey           唯一标识
     * @param standardTableSet
     * @param tablefiledMappingMap
     * @return
     */
    public static HashMap<String, Object> getE10BrowserInfo(String fromType, String fieldType, String browserKey, HashSet<String> standardTableSet, Map<String, List<Object>> tablefiledMappingMap) {
        String e9tablename = null;
        String e10tablename = null;
        Integer browserValueType = null;
        //数字型的浏览按钮都是表单字段的类型
        int fieldTypeInt = Util.getIntValue(fieldType);
        if (fieldTypeInt == 141) {
        } else if (fieldTypeInt == 161 || fieldTypeInt == 162) { //自定义单选 多选
            //去掉表单中字段的标识前缀
            if (browserKey.startsWith("browser.")) {
                browserKey = browserKey.replace("browser.", "");
            }
            //集成
            if (fromType.equals("1")) {
                SourceDBUtil sb = new SourceDBUtil();
                sb.executeQuery("select * from datashowset where showname = ? ", browserKey);
                while (sb.next()) {
                    String sign = Util.null2String(sb.getString("showname"));//标识
                    String sqltext = Util.null2String(sb.getString("SQLTEXT"));
                    String datasourceid = Util.null2String(sb.getString("DATASOURCEID"));
                    String keyfield = Util.null2String(sb.getString("keyfield")).toLowerCase();
                    if (StringUtils.isBlank(sign)) {
                        continue;
                    }
                    //外部数据源一律name处理
                    if (StringUtils.isNotBlank(datasourceid)) {
                        browserValueType = 1;
                        e9tablename = "";
                        e10tablename = "";
                    } else {
                        HashMap<String, Object> browserBelongTable = getBrowserBelongTable(fromType, keyfield, sqltext);
                        browserValueType = (Integer) browserBelongTable.get("browserValueType");
                        e9tablename = (String) browserBelongTable.get("e9Table");
                        e10tablename = (String) browserBelongTable.get("e10Table");
                    }
                }
                //建模
            } else if (fromType.equals("2")) {
                SourceDBUtil sourceDBUtil = new SourceDBUtil();
                sourceDBUtil.executeQuery("select a.*,b.MODENAME,c.TREEFIELDNAME as appname  from mode_browser a  left join modeinfo b on a.customid = b.id  left join modeTreeField c on b.MODETYPE  = c.id ");
                SourceDBUtil rs1 = new SourceDBUtil();
                while (sourceDBUtil.next()) {
                    String sign = sourceDBUtil.getString("showname");// sign
                    if (!browserKey.equalsIgnoreCase(sign)) {
                        continue;
                    }
                    String customid = sourceDBUtil.getString("customid");
                    String sqltext = sourceDBUtil.getString("sqltext");
                    String datasourceid = sourceDBUtil.getString("datasourceid");
                    if (StringUtils.isBlank(sign)) {
                        continue;
                    }
                    Boolean nameFlag = false;
                    //1.找主键字段 默认id
                    String keyfield = "id";
                    //2.表单字段是否有设置主键
                    rs1.executeQuery("select FIELDNAME  from mode_custombrowserdspfield join workflow_billfield on workflow_billfield.id = FIELDID where CUSTOMID = ? and ISPK =1", customid);
                    if (rs1.next()) {
                        keyfield = Util.null2String(rs1.getString("FIELDNAME")).toLowerCase();
                        //这种情况只能是 字符类型的字段 一定是name类型
                        nameFlag = true;
                    }
                    //3.虚拟表单是否有设置主键
                    rs1.executeQuery("select vprimarykey from modeformextend join mode_custombrowser md on md.formid = ModeFormExtend.formid where id=?", customid);
                    if (rs1.next()) {
                        keyfield = Util.null2String(rs1.getString("vprimarykey")).toLowerCase();
                    }
                    //外部数据源一律name处理
                    if (StringUtils.isNotBlank(datasourceid)) {
                        e9tablename = "";
                        e10tablename = "";
                        browserValueType = 1;
                    } else {
                        if (nameFlag) {
                            e9tablename = "";
                            e10tablename = "";
                            browserValueType = 1;
                        } else {
                            HashMap<String, Object> browserBelongTableMap = getBrowserBelongTable(fromType, keyfield, sqltext);
                            e9tablename = (String) browserBelongTableMap.get("e9Table");
                            e10tablename = (String) browserBelongTableMap.get("e10Table");
                            browserValueType = (Integer) browserBelongTableMap.get("browserValueType");
                        }
                    }
                }
            }
        } else if (fieldTypeInt == 257 || fieldTypeInt == 256) { //自定义树形多选 单选
            if (fromType.equals("3")) {
                //这种理论不应该存在,目前没法兼容, 相当于一个自定浏览按钮, 数据源引用的uf_demo表,主键字段设置的是 自定义树单、多选, 此时该浏览按钮被标记成 自定义的浏览按钮 ,但是里面的值却是来自树形浏览按钮的
                //如果此树只有一个叶子节点还是可以处理的，但是有多个叶子节点在当前模式下是没法办法仅用一个e9table和e10table来设置对应关系的
                SourceDBUtil sourceDBUtil = new SourceDBUtil();
                //表单的存的是树id 但是也是一对多的关系 一个mainid下可能有多个nodeid
                sourceDBUtil.executeQuery("select count(a.id) as count from mode_customtreedetail a join mode_customtree b on a.mainid = b.id and b.SHOWTYPE = 1 where  a.mainid  = ? ", browserKey);
                if (sourceDBUtil.next()) {
                    int count = sourceDBUtil.getInt("count");
                    if (count == 1) {
                        sourceDBUtil.clear();
                        sourceDBUtil.executeQuery("select a.*,b.TREENAME,b.SHOWTYPE from mode_customtreedetail a join mode_customtree b on a.mainid = b.id and b.SHOWTYPE = 1 where  a.mainid  = ? ", browserKey);
                        if (sourceDBUtil.next()) {
                            String tablename = Util.null2String(sourceDBUtil.getString("TABLENAME")).toLowerCase();//树表名
                            String tablekey = Util.null2String(sourceDBUtil.getString("TABLEKEY")).toLowerCase();//树节点主键字段
                            //只有树不需要解e9表
                            e9tablename = tablename;
                            if (!StringUtils.isAnyBlank(tablekey, tablename)) {
                                //选择框做数据源的情况
                                if (tablename.equalsIgnoreCase("workflow_selectitem")) {
                                    if (tablekey.equalsIgnoreCase("SELECTVALUE")) {
                                        e10tablename = "";
                                        browserValueType = 1;
                                    }
                                } else {
                                    String fromSql = "select " + tablekey + " from " + tablename;
                                    HashMap<String, Object> browserBelongTableMap = getBrowserBelongTable("3", e9tablename, tablekey, fromSql);
                                    e10tablename = (String) browserBelongTableMap.get("e10Table");
                                    browserValueType = (Integer) browserBelongTableMap.get("browserValueType");
                                }
                            }
                        }
                    }
                }
            }
        } else if (fieldTypeInt == 226 //系统集成单选浏览按钮
                || fieldTypeInt == 299 //预申请费用记录
                || fieldTypeInt == 400 //督办类型
                || fieldTypeInt == 401  //督办任务
                || fieldTypeInt == 137 //车辆
                || fieldTypeInt == 196 //相关合同
                || fieldTypeInt == 9999 //行程单
                || fieldTypeInt == 23 || fieldTypeInt == 315 //资产   -待处理  226     转 eb
                || fieldTypeInt == 242 //资产类型
                || fieldTypeInt == 320 //仓库
                || fieldTypeInt == 294 //多维度预算科目
                || fieldTypeInt == 295 //多维度承担主体
                || fieldTypeInt == 296 //多维度预算周期
                || fieldTypeInt == 55 //发文字号
                || fieldTypeInt == 298 //多维度账套
                || fieldTypeInt == 314 //多资产资料
                || fieldTypeInt == 179 //资产资料
        ) {
        } else if (fieldTypeInt == 1 || fieldTypeInt == 17 || 165 == fieldTypeInt || 166 == fieldTypeInt || 160 == fieldTypeInt) {//人员
            browserValueType = 0;
            e10tablename = "employee";
            e9tablename = "hrmresource";
        } else if (fieldType.equals("2")) { //日期 需要转换为时间戳
            browserValueType = 1;
        } else if (fieldType.equals("403")) { //年月 2022-11
            browserValueType = 1;
        } else if (fieldType.equals("178") || fieldType.equals("402")) { //年 2022
            browserValueType = 1;
        } else if (fieldType.equals("19")) { //时间
            browserValueType = 1;
        } else if (fieldType.equals("290")) { //日期 时间
            browserValueType = 1;
        } else if (fieldType.equals("269")) { //多提醒  -公共选择框
            browserValueType = 0;
            e10tablename = "mt_remind_type";
            e9tablename = "meeting_remind_type";
        } else if (fieldType.equals("268")) { //星期多选 -公共选择框
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "meeting_week_type";
        } else if (fieldType.equals("245")) { //工作类型  -公共选择框
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "Prj_WorkType";
        } else if (4 == fieldTypeInt || 57 == fieldTypeInt || 167 == fieldTypeInt || 168 == fieldTypeInt) { //部门
            browserValueType = 0;
            e10tablename = "department";
            e9tablename = "hrmdepartment";
        } else if (12 == fieldTypeInt) {//币种
            browserValueType = 0;
            e10tablename = "i18n_currency";
            e9tablename = "FnaCurrency";
        } else if (69 == fieldTypeInt) {//计量单位  -E10无表名 todo E10表名暂无对应
            browserValueType = 0;
            e10tablename = "i18n_measure_setting";
            e9tablename = "LgcAssetUnit";
        } else if (31 == fieldTypeInt) {//用工性质
            browserValueType = 0;
            e10tablename = "hr_dictionary_setting";
            e9tablename = "HrmUseKind";
        } else if (182 == fieldTypeInt) {//单网上调查
            browserValueType = 0;
            e10tablename = "report_incollect_config";
            e9tablename = "voting";
        } else if (284 == fieldTypeInt) {//工资银行
            browserValueType = 0;
            e10tablename = "hr_dictionary_setting";
            e9tablename = "hrmbank";
        } else if (119 == fieldTypeInt) { //专业  -E10无表名
            browserValueType = 0;
            e10tablename = "hr_speciality";
            e9tablename = "HrmSpeciality";
        } else if (274 == fieldTypeInt) { //商机来源  -E10无表名 todo E10表名暂无对应
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "WorkPlan";
        } else if (280 == fieldTypeInt) { //考勤班次
            browserValueType = 0;
            e10tablename = "attend_shift";
            e9tablename = "kq_ShiftManagement_view";
        } else if (388 == fieldTypeInt) {//考勤班次
            browserValueType = 0;
            e10tablename = "attend_shift";
            e9tablename = "hrm_schedule_shifts_set_id";
        } else if (270 == fieldTypeInt) {//服务项目
            browserValueType = 0;
            e10tablename = "mt_service_item";
            e9tablename = "Meeting_Service_Item";
        } else if (59 == fieldTypeInt) {//称呼
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "CRM_ContacterTitle";
        } else if (61 == fieldTypeInt) {//客户描述
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "CRM_CustomerDesc";
        } else if (318 == fieldTypeInt) {//客户价值
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "CRM_Evaluation_Level";
        } else if (62 == fieldTypeInt) {//客户规模
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "CRM_CustomerSize";
        } else if (265 == fieldTypeInt) {//获得途径
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "CRM_ContactWay";
        } else if (410 == fieldTypeInt) {//企业账号  -E10无表名 todo E10表名需要确认
            browserValueType = 0;
            e10tablename = "bank_tenpay_trans";
            e9tablename = "fnaTenPaySignInfo";
        } else if (262 == fieldTypeInt) {//办公地点
            browserValueType = 0;
            e10tablename = "hrm_offspace";
            e9tablename = "HrmLocations";
        } else if (164 == fieldTypeInt || 194 == fieldTypeInt || 169 == fieldTypeInt || 170 == fieldTypeInt) {//分部
            browserValueType = 0;
            e10tablename = "department";
            e9tablename = "hrmsubcompany";
        } else if (24 == fieldTypeInt || 278 == fieldTypeInt) {//岗位
            browserValueType = 0;
            e10tablename = "position";
            e9tablename = "hrmjobtitles";
        } else if (9 == fieldTypeInt || 37 == fieldTypeInt) {//文档
            browserValueType = 0;
            e10tablename = "document";
            e9tablename = "docdetail";
        } else if (8 == fieldTypeInt || 135 == fieldTypeInt) {//项目
            browserValueType = 0;
            e10tablename = "mainline";
            e9tablename = "prj_projectinfo";
        } else if (142 == fieldTypeInt) {//收发文单位
            browserValueType = 0;
            e10tablename = "odoc_receive_unit";
            e9tablename = "docreceiveunit";
        } else if (38 == fieldTypeInt) {//相关产品
            browserValueType = 0;
            e10tablename = "crm_production";
            e9tablename = "lgcasset";
        } else if (316 == fieldTypeInt || 321 == fieldTypeInt || 241 == fieldTypeInt) {//客户商机
            browserValueType = 0;
            e10tablename = "crm_sale_chance";
            e9tablename = "crm_sellchance";
        } else if (35 == fieldTypeInt) {//业务合同
            browserValueType = 0;
            e10tablename = "crm_contract";
            e9tablename = "crm_contract";
        } else if (279 == fieldTypeInt) {//合同
            browserValueType = 0;
            e10tablename = "hr_contract";
            e9tablename = "HrmContract";
        } else if (322 == fieldTypeInt || 67 == fieldTypeInt) {// 客户联系人
            browserValueType = 0;
            e10tablename = "contact";
            e9tablename = "crm_customercontacter";
        } else if (18 == fieldTypeInt || 7 == fieldTypeInt) {//客户
            browserValueType = 0;
            e10tablename = "customer";
            e9tablename = "crm_customerinfo";
        } else if (16 == fieldTypeInt || 152 == fieldTypeInt || 171 == fieldTypeInt) {// 流程
            browserValueType = 0;
            e10tablename = "wfc_requestbase";
            e9tablename = "workflow_requestbase";
        } else if (53 == fieldTypeInt) {// 紧急程度 -公文
            browserValueType = 0;
            e10tablename = "wfp_customlevel";
            e9tablename = "docinstancylevel";
        } else if (54 == fieldTypeInt) {// 秘密等级 -公文
            browserValueType = 0;
            e10tablename = "odoc_secret_level";
            e9tablename = "docsecretlevel";
        } else if (52 == fieldTypeInt) {// 公文种类 -公文
            browserValueType = 0;
            e10tablename = "odoc_doc_type";
            e9tablename = "odoc_odoctype";
        } else if (285 == fieldTypeInt) {// 主题分类 -公文
            browserValueType = 0;
            e10tablename = "odoc_topic_type";
            e9tablename = "odoc_topictype";
        } else if (324 == fieldTypeInt) {// 文号类型 -公文
            browserValueType = 0;
            e10tablename = "odoc_number_type";
            e9tablename = "odoc_numbermanage";
        } else if (28 == fieldTypeInt) {// 会议 -会议
            browserValueType = 0;
            e10tablename = "mt_detail";
            e9tablename = "meeting";
        } else if (87 == fieldTypeInt || 184 == fieldTypeInt) {// 会议室 -会议
            browserValueType = 0;
            e10tablename = "mt_room";
            e9tablename = "meetingroom";
        } else if (89 == fieldTypeInt) {// 会议类型 -会议
            browserValueType = 0;
            e10tablename = "mt_type";
            e9tablename = "meeting_type";
        } else if (30 == fieldTypeInt) {// 学历 -人事
            browserValueType = 0;
            e10tablename = "hr_dictionary_setting";
            e9tablename = "hrmeducationlevel ";
        } else if (34 == fieldTypeInt) {// 请假类型 -人事
            browserValueType = 0;
            e10tablename = "attend_vacation_setting";
            e9tablename = "kq_leaverules";
        } else if (399 == fieldTypeInt) {// 老请假类型 -人事
            browserValueType = 0;
            e10tablename = "attend_vacation_setting";
            e9tablename = "kq_leaverules";
        } else if (388 == fieldTypeInt) {// 考勤组班次 -人事
            browserValueType = 0;
            e10tablename = "attend_shift";
            e9tablename = "kq_shiftmanagement";
        } else if (65 == fieldTypeInt || 267 == fieldTypeInt) {//多角色、角色
            browserValueType = 0;
            e10tablename = "auth_role";
            e9tablename = "hrmroles";
        } else if (319 == fieldTypeInt) {//联系方式
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "crm_customercontactway";
        } else if (13 == fieldTypeInt) {//产品类别
            browserValueType = 0;
            e10tablename = "crm_tree_node";
            e9tablename = "lgcassetassortment";
        } else if (36 == fieldTypeInt) {//合同性质（合同类型）
            browserValueType = 0;
            e10tablename = "crm_dictionary_setting";
            e9tablename = "crm_contracttype";
        } else if (60 == fieldTypeInt) {//客户类型
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "crm_customertype";
        } else if (63 == fieldTypeInt) {//行业
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "crm_sectorinfo";
        } else if (264 == fieldTypeInt) {//客户状态
            browserValueType = 0;
            e10tablename = "customer_property";
            e9tablename = "crm_customerstatus";
        } else if (129 == fieldTypeInt) {//项目模板
            browserValueType = 0;
            e10tablename = "mt_mainline_template";
            e9tablename = "prj_template";
        } else if (244 == fieldTypeInt) {//项目类型\
            browserValueType = 0;
            e10tablename = "mainline_group";
            e9tablename = "prj_projecttype";
        } else if (282 == fieldTypeInt) {//职务
            browserValueType = 0;
            e10tablename = "hrm_jobset";
            e9tablename = "hrmjobactivities";
        } else if (281 == fieldTypeInt) {//职务类别
            browserValueType = 0;
            e10tablename = "hrm_jobtype_set";
            e9tablename = "hrmjobgroups";
        } else if (258 == fieldTypeInt) {//国家
            browserValueType = 0;
            e10tablename = "administrative_area";
            e9tablename = "hrmcountry";
        } else if (58 == fieldTypeInt) { //城市
            browserValueType = 0;
            e10tablename = "administrative_area";
            e9tablename = "hrmcity";
        } else if (2222 == fieldTypeInt) { //省份
            browserValueType = 0;
            e10tablename = "administrative_area";
            e9tablename = "hrmprovince";
        } else if (263 == fieldTypeInt) {//区县
            browserValueType = 0;
            e10tablename = "administrative_area";
            e9tablename = "hrmcitytwo";
        } else if (125 == fieldTypeInt) {//目标
            browserValueType = 0;
            e10tablename = "wrgm_baseinfo";
            e9tablename = "gm_goalinfo";
        } else if (126 == fieldTypeInt) {//报告
            browserValueType = 0;
            e10tablename = "work_report";
            e9tablename = "pr_planreport";
        } else if (36 == fieldTypeInt) {//合同性质
            browserValueType = 0;
            e10tablename = "crm_dictionary_setting";
            e9tablename = "CRM_ContractType";
        } else if (129 == fieldTypeInt) {//项目模板
            browserValueType = 0;
            e10tablename = "mt_mainline_template";
            e9tablename = "prj_template";
        } else if (30 == fieldTypeInt) {//学历
            browserValueType = 0;
            e10tablename = "hr_dictionary_setting";
            e9tablename = "HrmEducationLevel";
        } else if (260 == fieldTypeInt) {//职称
            browserValueType = 0;
            e10tablename = "hrm_jobcall";
            e9tablename = "HrmJobCall";
        } else if (119 == fieldTypeInt) {//专业
            browserValueType = 0;
            e10tablename = "hr_speciality";
            e9tablename = "HrmSpeciality";
        } else if (52 == fieldTypeInt) {//公文种类
            browserValueType = 0;
            e10tablename = "odoc_doc_type";
            e9tablename = "odoc_odoctype";
        } else if (270 == fieldTypeInt) {//服务项目
            browserValueType = 0;
            e10tablename = "mt_service_item";
            e9tablename = "meeting_Service_item";
        } else if (292 == fieldTypeInt || 293 == fieldTypeInt || 291 == fieldTypeInt) {//发票
            browserValueType = 0;
            e10tablename = "fnainvoiceledger";
            e9tablename = "fnainvoiceledger";
        } else if (246 == fieldTypeInt) {//项目状态
            browserValueType = 0;
            e10tablename = "mainline_cust_status";
            e9tablename = "Prj_ProjectStatus";
        } else if (298 == fieldTypeInt || "budgetAccount".equalsIgnoreCase(fieldType)) {//多维度账套、账套 - 财务 - E10EB搭建
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "fnaaccountinfo";
        } else if (137 == fieldTypeInt) {//车辆 - E10EB搭建
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "CarInfo";
        } else if (401 == fieldTypeInt) {//督办任务- E10EB搭建
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "govern_task";
        } else if (400 == fieldTypeInt) {//督办类型 - E10EB搭建
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "govern_category";
        } else if (22 == fieldTypeInt || 222 == fieldTypeInt || "FnaBudgetfeeType".equalsIgnoreCase(fieldType)) {//报销费用类型、多报销费用类型、费控设置的费用科目浏览按钮 - 财务
            browserValueType = 0;
            e10tablename = "fexs_subject";
            e9tablename = "fnabudgetfeetype";
        } else if (251 == fieldTypeInt || "FnaCostCenter".equalsIgnoreCase(fieldType) || fieldTypeInt == 6) {//成本中心、多成本中心、多成本中心类别 - 财务 -E10EB搭建
            browserValueType = 0;
            e10tablename = "fexs_cost_center";
            e9tablename = "fnacostcenter";
        } else if (295 == fieldTypeInt) {//多维度承担主体 -E10EB搭建，多表到一个EB表，但是多表id不重复
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "fnabudgetbearer";
        } else if (294 == fieldTypeInt) {//多维度科目 -E10EB搭建，多表到一个EB表，但是多表id不重复
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "fnabudgetsubject";
        } else if (296 == fieldTypeInt) {//多维度周期 -E10EB搭建，多表到一个EB表，但是多表id不重复
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "fnaperiodsetting";
        } else if (55 == fieldTypeInt) {//公文- 发文字号
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "docsenddocnumber";
        } else if (242 == fieldTypeInt) {//资产类型（涉及拆表）
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "cptcapitaltype";
        } else if (179 == fieldTypeInt || 314 == fieldTypeInt) {//资产资料（涉及拆表）
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "cptcapital";
        } else if (23 == fieldTypeInt || 315 == fieldTypeInt) {//资产（涉及拆表）
            browserValueType = 0;
            e9tablename = "cptcapital";
            e10tablename = null;
        } else if (320 == fieldTypeInt) {//仓库
            browserValueType = 0;
            e10tablename = null;
            e9tablename = "cptcapitalwarehouse";
        }
        //如果是 仓库 资产 资料 资产类型 公文 发文字号 财务等转EB的表  如果获取到的E10表名为空则直接使用 e9tablename
        //仅仅为了取雪花ID而已，e10表不是很重要
        if (fieldTypeInt == 320
                || fieldTypeInt == 23
                || fieldTypeInt == 315
                || fieldTypeInt == 179
                || fieldTypeInt == 314
                || fieldTypeInt == 242
                || fieldTypeInt == 55
                || fieldTypeInt == 296
                || fieldTypeInt == 294
                || fieldTypeInt == 295
                || fieldTypeInt == 298
                || "budgetAccount".equalsIgnoreCase(fieldType)
                || fieldTypeInt == 137
                || fieldTypeInt == 401
                || fieldTypeInt == 400) {
            browserValueType = 0;
            if (e10tablename == null) e10tablename = e9tablename;
        }
        HashMap<String, Object> result = new HashMap<>();
        result.put("e9Table", e9tablename);
        result.put("e10Table", e10tablename);
        result.put("browserValueType", browserValueType);
        return result;
    }

    public String findFirstField(String sql) {
        try {
            // 创建一个正则表达式，匹配"select"和"from"之间的内容
            Pattern pattern = Pattern.compile("(?i)\\bselect\\b(.*?)\\bfrom\\b");
            Matcher matcher = pattern.matcher(sql);
            // 如果找到匹配，取出匹配的内容
            if (matcher.find()) {
                // 获取"select"和"from"之间的内容，并去掉前后的空格
                String fields = matcher.group(1).trim();
                // 将字段分割成数组
                String[] words = fields.split(",");
                // 取出第一个字段
                String firstField = words[0].trim();
                if (firstField.toLowerCase().contains("distinct")) {
                    firstField = firstField.replace("distinct", "");
                }
                // 如果字段有别名（比如"a1 as field"），则只取出字段名
                if (firstField.toLowerCase().contains(" as ")) {
                    firstField = firstField.split(" as ")[1].trim();
                } else if (firstField.toLowerCase().contains(".")) {
                    //主表
                    firstField = firstField.split("\\.")[1].trim();
                }
                return firstField;
            }
            // 如果没有找到匹配，返回null
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取系统内表单名称
     *
     * @return
     */
    public static HashSet<String> getSystemBillName() {
        SourceDBUtil sb = new SourceDBUtil();
        sb.executeQuery("select TABLENAME\n" +
                                " from workflow_BILL\n" +
                                " WHERE not exists(select 1 from modeformextend where formid = workflow_BILL.id)\n" +
                                " union\n" +
                                " select TABLENAME\n" +
                                " from workflow_billdetailtable\n" +
                                " WHERE not exists(select 1 from modeformextend where formid = BILLID)");
        HashSet<String> billNames = new HashSet<>();
        while (sb.next()) {
            String billName = Util.null2String(sb.getString("TABLENAME")).toLowerCase();
            billNames.add(billName);
        }
        return billNames;
    }

    private static void updateAnszlxByCount(Integer browserValueType, CustomerBrowserEntity browser, Integer useCount) {
        if (useCount <= 0 && Objects.isNull(browserValueType)) {
            //未被引用且未识别给name
            browserValueType = 1;
            System.out.println("当前浏览按钮没有引用,设为NAME");
            browser.setAnzlx(browserValueType);
        }
    }

    /**
     * 根据标识获取引用自定义流程按钮的地方
     *
     * @param mark
     * @return void
     * @create 2024/3/26 15:47
     * @author mcd
     **/
    public List<String> getCustomBrowserQuotePlace(String mark, int type) {
        if (type != 3) {
            if (!mark.startsWith("browser.")) {
                mark = "browser." + mark;
            }
        }

        List<String> list = new ArrayList<>();
        try {
            //E9：1：出口，2：批次条件 ，3：引用  4：督办 5：流程数据交换 6：代理条件 7：相同子流程 :8：不同子流程 10：超时条件
            String sql = "select * from rule_base where exists (SELECT ruleid  FROM rule_expressionbase WHERE HTMLTYPE=3 AND FIELDTYPE IN(161,162,256,257) and dbtype in ('" + mark + "') and ruleid=rule_base.id)";
            SourceDBUtil rs = new SourceDBUtil();
            SourceDBUtil rs1 = new SourceDBUtil();
            SourceDBUtil rs2 = new SourceDBUtil();
            rs.executeQuery(sql);
            while (rs.next()) {
                String result = "";
                String rulesrc = Util.null2String(rs.getString("rulesrc"));
                String linkid = Util.null2String(rs.getString("linkid"));
                String rulename = Util.null2String(rs.getString("rulename"));
                String condit = Util.null2String(rs.getString("condit"));

                if ("1".equals(rulesrc)) {//1：出口
                    if (!"".equals(linkid)) {
                        String linksql = "select b.workflowname,a.linkname from workflow_nodelink a left join workflow_base b on a.workflowid=b.id where  a.id=?";
                        rs1.executeQuery(linksql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            String linkname = Util.formatMultiLang(rs1.getString("linkname"));
                            result = "流程名称:" + workflowname + " 出口名称:" + linkname + " " + condit + " (出口)";
                            list.add(result);
                        }
                    }
                } else if ("2".equals(rulesrc)) {//2：批次条件
                    if (!"".equals(linkid)) {
                        String groupsql = "select e.workflowname,d.nodename,b.groupname from workflow_groupdetail a,workflow_nodegroup b,workflow_flownode c,workflow_nodebase d,workflow_base e where a.groupid = b.id and b.nodeid = c.nodeid and c.workflowid=e.id and  c.nodeid=d.id and  a.id=?";
                        rs1.executeQuery(groupsql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            String nodename = Util.formatMultiLang(rs1.getString("nodename"));
                            String groupname = Util.formatMultiLang(rs1.getString("groupname"));
                            result = "流程名称:" + workflowname + " 节点名称:" + nodename + " " + " 操作组名称:" + groupname + " " + condit + " (批次)";
                            list.add(result);
                        }
                    }
                } else if ("4".equals(rulesrc)) {//督办条件
                    if (!"".equals(linkid)) {
                        String upgersql = "select workflowname from workflow_urgerdetail a,workflow_base b where a.workflowid=b.id and a.id=?";
                        rs1.executeQuery(upgersql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            result = "流程名称:" + workflowname + " " + condit + " (督办)";
                            list.add(result);
                        }
                    }
                } else if ("6".equals(rulesrc)) {//代理（委托）条件
                    if (!"".equals(linkid)) {
                        String agentsql = "select workflowname from  workflow_agentconditionset a,workflow_base b where a.workflowid=b.id and  conditionkeyid=?";
                        rs1.executeQuery(agentsql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            result = "流程名称:" + workflowname + " " + condit + " (代理)";
                            list.add(result);
                        }
                    }
                } else if ("7".equals(linkid)) {//相同子流程条件
                    if (!"".equals(linkid)) {
                        String subWfsql = "select b.workflowname,c.nodename from Workflow_SubwfSet a,workflow_base b,workflow_nodebase c  where a.mainworkflowid=b.id  and a.triggernodeid=c.id and a.id=?";
                        rs1.executeQuery(subWfsql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            String nodename = Util.formatMultiLang(rs1.getString("nodename"));
                            result = "流程名称:" + workflowname + " 节点名称:" + nodename + " " + condit + " (相同子流程)";
                            list.add(result);
                        }
                    }
                } else if ("8".equals(linkid)) {//不同子流程条件
                    if (!"".equals(linkid)) {
                        String diffWfsql = "select b.workflowname,c.nodename from Workflow_TriDiffWfDiffField a,workflow_base b,workflow_nodebase c  where a.mainworkflowid=b.id  and a.triggernodeid=c.id and a.id=?";
                        rs1.executeQuery(diffWfsql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            String nodename = Util.formatMultiLang(rs1.getString("nodename"));
                            result = "流程名称:" + workflowname + " " + " 节点名称:" + nodename + " " + condit + " (不同子流程)";
                            list.add(result);
                        }
                    }
                } else if ("10".equals(linkid)) {//节点信息-超时设置-条件
                    if (!"".equals(linkid)) {
                        String overTimeWfsql = "select b.workflowname,c.nodename from workflow_nodeovertime a,workflow_base b,workflow_nodebase c  where a.workflowid=b.id  and a.nodeid=c.id and a.id=?";
                        rs1.executeQuery(overTimeWfsql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            String nodename = Util.formatMultiLang(rs1.getString("nodename"));
                            result = "流程名称:" + workflowname + " " + " 节点名称:" + nodename + " " + condit + " (节点超时)";
                            list.add(result);
                        }
                    }
                } else if ("5".equals(linkid)) {//流程交换条件
                    if (!"".equals(linkid)) {
                        String changesql = "select workflowname from  wfec_indatawfset a,workflow_base b where a.workflowid=b.id and  a.id=?";
                        rs1.executeQuery(changesql, linkid);
                        if (rs1.next()) {
                            String workflowname = Util.formatMultiLang(rs1.getString("workflowname"));
                            result = "流程名称:" + workflowname + " " + condit + " (流程交换)";
                            list.add(result);
                        }
                    }
                }
            }
            //附加操作sql
            String addinoperateSql = "select workflowname,nodename,ispreadd,isnode from workflow_addinoperate t, workflow_billfield t2,workflow_nodebase t3,workflow_base t4 where (t.fieldid=t2.id or t.fieldop1id=t2.id or t.fieldop2id=t2.id) and t.objid=t3.id and t.workflowid=t4.id    and t2.fieldhtmltype=3 and t2.type in(161,162,256,257) and t2.fielddbtype in( '" + mark + "' )";
            rs2.executeQuery(addinoperateSql);
            while (rs2.next()) {
                String result = "";
                String workflowname = Util.formatMultiLang(rs2.getString("workflowname"));
                String nodename = Util.formatMultiLang(rs2.getString("nodename"));
                String ispreadd = Util.null2String(rs2.getString("ispreadd"));//节点前
                String isnode = Util.null2String(rs2.getString("isnode"));//节点还是出口的附加规则
                if ("1".equals(isnode)) {//节点
                    if ("0".equals(ispreadd)) {//节点后
                        result = "流程名称:" + workflowname + " 节点名称:" + nodename + " (节点后附加操作)";
                    } else {
                        result = "流程名称:" + workflowname + " 节点名称:" + nodename + " (节点前附加操作)";
                    }
                } else {//附加规则
                    result = "流程名称:" + workflowname + " 节点名称:" + nodename + " (出口附加规则)";
                }
                list.add(result);
            }
            System.out.println("标识>>>>>>" + mark + ">>" + JSON.toJSONString(list));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * 获取各模块自定义字段引用的地址
     *
     * @return
     */
    public ArrayList<String> getCustomFieldUsed(String sign, int type) {
        ArrayList<String> result = new ArrayList<>();
        sign = sign.toLowerCase();
        String hrmSql =
                "select a.fieldname as fieldDbName,\n" +
                        "       b.labelname as fieldName,\n" +
                        "       d.labelname as groupName,\n" +
                        "       case\n" +
                        "           when c.GROUPTYPE = '-1' then '人员卡片字段定义-基本信息'\n" +
                        "           when c.GROUPTYPE = '1' then '人员卡片字段定义-个人信息'\n" +
                        "           when c.GROUPTYPE = '3' then '人员卡片字段定义-工作信息'\n" +
                        "           when c.GROUPTYPE = '4' then '分部字段定义'\n" +
                        "           when c.GROUPTYPE = '5' then '部门字段定义'\n" +
                        "           END     as bigGroupName\n" +
                        " from hrm_formfield a\n" +
                        "         left join htmllabelinfo b\n" +
                        "                   on a.fieldlabel = b.indexid\n" +
                        "         left join hrm_fieldgroup c on c.id = a.groupid\n" +
                        "         left join htmllabelinfo d on c.GROUPLABEL = d.indexid\n" +
                        " where b.languageid = 7\n" +
                        "  and d.languageid = 7\n" +
                        "  and (a.issystem is null or a.issystem = 0)\n" +
                        "  and lower(a.DMLURL) = ?";
        RecordSet rs = new RecordSet();
        rs.executeQuery(hrmSql, sign);
        if (rs.next()) {
            String fieldName = Util.null2String(rs.getString("fieldName"));
            String groupName = Util.null2String(rs.getString("groupName"));
            String bigGroupName = Util.null2String(rs.getString("bigGroupName"));
            String fieldDbName = Util.null2String(rs.getString("fieldDbName"));
            result.add("[人力模块]自定义设置-" + bigGroupName + "-" + groupName + " 字段显示名:" + fieldName + "  数据库字段名称:" + fieldDbName);
        }

        String meetingSql = "select c.LABELNAME as groupName,\n" +
                "       d.LABELNAME as fieldName,\n" +
                "       a.FIELDNAME as fieldDbName,\n" +
                "       a.FIELDDBTYPE,\n" +
                "       case\n" +
                "           when a.GROUPTYPE = '1' then '会议信息字段定义'\n" +
                "           when a.GROUPTYPE = '2' then '会议服务字段定义'\n" +
                "           when a.GROUPTYPE = '3' then '会议议程字段定义'\n" +
                "           END     as bigGroupName\n" +
                " from meeting_formfield a\n" +
                "         left join meeting_fieldgroup b on a.GROUPID = b.id\n" +
                "         left join htmllabelinfo c on a.FIELDLABEL = c.INDEXID and c.LANGUAGEID = 7\n" +
                "         left join htmllabelinfo d on b.GROUPLABEL = d.INDEXID and d.LANGUAGEID = 7\n" +
                " where lower(a.FIELDDBTYPE) = ? ";
        if (type == 3) {
            rs.executeQuery(meetingSql, sign);
        } else {
            rs.executeQuery(meetingSql, "browser." + sign);
        }
        if (rs.next()) {
            String fieldName = Util.null2String(rs.getString("fieldName"));
            String groupName = Util.null2String(rs.getString("groupName"));
            String bigGroupName = Util.null2String(rs.getString("bigGroupName"));
            String fieldDbName = Util.null2String(rs.getString("fieldDbName"));
            result.add("[会议模块]自定义设置-" + bigGroupName + "-" + groupName + " 字段显示名:" + fieldName + "  数据库字段名称:" + fieldDbName);
        }

        String crmSql = "select a.FIELDNAME as fieldDbName,\n" +
                "       b.LABELNAME as fieldName,\n" +
                "       d.LABELNAME as groupName,\n" +
                "       case\n" +
                "           when a.USETABLE = 'CRM_CustomerInfo' then '基本信息字段设置'\n" +
                "           when a.USETABLE = 'CRM_CustomerContacter' then '联系人信息字段设置'\n" +
                "           when a.USETABLE = 'CRM_CustomerAddress' then '地址信息字段设置'\n" +
                "           when a.USETABLE = 'CRM_SellChance' then '基本信息字段设置'\n" +
                "           END     as bigGroupName\n" +
                " from crm_customerdefinfield a\n" +
                "         left join htmllabelinfo b on a.FIELDLABEL = b.INDEXID and b.LANGUAGEID = 7\n" +
                "         left join crm_customerdefinfieldgroup c on a.GROUPID = c.ID\n" +
                "         left join htmllabelinfo d on c.GROUPLABEL = d.INDEXID and d.LANGUAGEID = 7\n" +
                " where  lower(a.DMLURL) = ?  ";
        if (type == 3) {
            rs.executeQuery(crmSql, sign);
        } else {
            rs.executeQuery(crmSql, "browser." + sign);
        }
        if (rs.next()) {
            String fieldName = Util.null2String(rs.getString("fieldName"));
            String groupName = Util.null2String(rs.getString("groupName"));
            String bigGroupName = Util.null2String(rs.getString("bigGroupName"));
            String fieldDbName = Util.null2String(rs.getString("fieldDbName"));
            result.add("[客户模块]自定义设置-" + bigGroupName + "  组名:" + groupName + " 字段显示名:" + fieldName + " 数据库字段名称:" + fieldDbName);
        }

        String docSql =
                "select *\n" +
                        "from cus_formdict\n" +
                        "where SCOPE = 'DocCustomFieldBySecCategory'\n" +
                        "  and lower(FIELDDBTYPE) = ?";
        rs.executeQuery(docSql, "browser." + sign);
        if (rs.next()) {
            String fieldName = Util.null2String(rs.getString("FIELDLABEL"));
            String fieldDbName = Util.null2String(rs.getString("FIELDNAME"));
            result.add("[知识模块]目录设置-字段管理  数据库字段名:" + fieldName + "  显示名称:" + fieldDbName);
        }

        return result;
    }

    public static void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {

        //创建一个模块 功能项
        ExcelFunctionBean functionBean = new ExcelFunctionBean(functionName);

        //功能有多少列，指定每个列的列名
        functionBean.setHeaderNames(headerNames);

        //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
        functionBean.setValues(values);

        List<ExcelFunctionBean> list = new ArrayList<>();//一个sheet页有多少功能项
        list.add(functionBean);
        LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();
        if (sheets.containsKey(sheetname)) {//包含sheet页  就补在sheet页中
            sheets.get(sheetname).addAll(list);
        } else { //不存在当前sheet页，维护新的
            sheets.put(sheetname, list);
        }
        writeEntity.setSheets(sheets);
    }

    public static CellStyle getBodyStyle(Workbook workbook) {
        Font font = workbook.createFont();
        //设置字体名字
        font.setFontName("宋体");
        //设置样式;
        CellStyle style = workbook.createCellStyle();
        //设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        //设置底边框颜色;
        style.setBottomBorderColor(IndexedColors.BLACK.index);
        //设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        //设置左边框颜色;
        style.setLeftBorderColor(IndexedColors.BLACK.index);
        //设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        //设置右边框颜色;
        style.setRightBorderColor(IndexedColors.BLACK.index);
        //设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        //设置顶边框颜色;
        style.setTopBorderColor(IndexedColors.BLACK.index);
        //在样式用应用设置的字体;
        style.setFont(font);
        //设置自动换行;
        style.setWrapText(true);
        //设置水平对齐的样式为居中对齐;
        style.setAlignment(HorizontalAlignment.LEFT);
        //设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        return style;
    }

    private static ArrayList<String> sortFormList(ArrayList<String> formList) {
        Map<Boolean, List<String>> groupedMap = formList.stream().collect(Collectors.partitioningBy(s -> s.contains("流程名称")));
        ArrayList<String> resultList = new ArrayList<>();
        resultList.addAll(groupedMap.get(true));   //添加包含"流程名称"的元素
        resultList.addAll(groupedMap.get(false));  //
        return resultList;
    }

}
