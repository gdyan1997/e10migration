package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public enum HandType {

    sqlDev("sql调整", 2),
    linkDev("链接/js调整", 3),
    javaDev("java调整", 4),
    jsDev("js调整", 5),

    cusComp("自定义组件需要重新配置", 6),
    notSupportComp("E8组件需要重新配置", 7),
    ;


    private String type;

    private int ordeid;

    HandType(String type, int ordeid) {
        this.type = type;
        this.ordeid = ordeid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getOrdeid() {
        return ordeid;
    }

    /**
     * @author ygd2020
     * @date 2024-5-13 11:01
     * Description:   返回基于orderid升序排序的操作类型
     **/
    public static List<HandType> getValues() {
        return Arrays.stream(HandType.values()).sorted(Comparator.comparingInt(HandType::getOrdeid)).collect(Collectors.toList());
    }
}
