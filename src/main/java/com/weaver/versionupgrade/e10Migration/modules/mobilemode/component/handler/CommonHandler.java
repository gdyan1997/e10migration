package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ygd2020
 * @date 2024-5-8 18:40
 * Description:
 **/
public class CommonHandler extends AbstractComponentHandler {

    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {

        ComponentType componentType = componentConvertContext.getComponentType();

        List<ComponentBean> componentBeans = componentType.getComponentBean();

        if (componentBeans == null) {
            this.setLists(new ArrayList<ComponentBean>(){{
                this.add(new ComponentBean().setHandType(HandType.cusComp).setResult(componentType.getType() + "组件没有定义抓取key "));
            }});
            return;
        }

        String mecparam = componentConvertContext.getMecparam();


        List<ComponentBean> newbean = new ArrayList<>();

        JSONObject mecparamjson = JSON.parseObject(mecparam);

        for (ComponentBean componentBean : componentBeans) {

            String targetpath = componentBean.getTargetpath();

            String desc = componentBean.getDesc();


            String[] split = targetpath.split("\\.");

            Object o = null;
            boolean hasarray = false;
            boolean hasdev = false;

            for (int i = 0; i < split.length; i++) {
                String key = split[i];

                if (key.contains("-")) {

                    if (o == null && i == 0) {
                        o = mecparamjson;
                    }

                    JSONObject o3 = (JSONObject) o;
                    String[] split1 = key.split("-");
                    for (int j = 0; j < split1.length; j++) {
                        if (o3 != null) {
                            String info = Util.null2String(o3.get(split1[j]));
                            if (StringUtils.isNotBlank(info)) {
                                hasdev = true;
                                desc = desc.replace("#{" + split1[j]  + "}", info);
                            } else {
                                desc = desc.replace("#{" + split1[j]   + "}", "空");
                            }

                            // 如果是最后一个 但是是空的  就算没有开发
                            if (j == split1.length-1 && StringUtils.isBlank(info)) {
                                hasdev = false;
                            }

                        }
                    }

                    break;
                }

                if (o == null) {
                    o = mecparamjson.get(key);
                }

                if (o instanceof String) {
                    if (StringUtils.isNotBlank(o.toString())) {
                        hasdev = true;
                        desc = desc.replace("#{" + key  + "}", o.toString());
                    } else {
                        desc = desc.replace("#{" + key   + "}","空");
                    }
                } else if (o instanceof JSONObject) {
                    JSONObject o1 = (JSONObject) o;

                    o = o1.get(key);


                } else if (o instanceof JSONArray) {
                    hasarray = true;

                    JSONArray o1 = (JSONArray) o;

                    for (Object o2 : o1) {
                        JSONObject o3 = (JSONObject) o2;
                        String desbak = desc;
                        String key1 = split[i + 1];
                        String[] split1 = key1.split("-");
                        for (int j = 0; j < split1.length; j++) {
                            if (o3 != null) {
                                String info = Util.null2String(o3.get(split1[j]));
                                if (StringUtils.isNotBlank(info)) {
                                    hasdev = true;
                                    desbak = desbak.replace("#{" + split1[j]  + "}", info);
                                } else {
                                    desbak = desbak.replace("#{" + split1[j]  + "}", "空");
                                }

                                // 如果是最后一个 但是是空的  就算没有开发
                                if (j == split1.length-1 && StringUtils.isBlank(info)) {
                                    hasdev = false;
                                }

                            }


                        }
                        if (hasdev) {
                            componentBean.setResult(desbak);
                            newbean.add(new ComponentBean().setHandType(componentBean.getHandType()).
                                    setDesc(componentBean.getDesc()).setTargetpath(componentBean.getTargetpath()).
                                    setResult(componentBean.getResult()));

                        }
                    }
                    break;


                }
            }
            if (!hasarray&&hasdev) {
                componentBean.setResult(desc);
                newbean.add(componentBean);
            }


        }

        this.setLists(newbean);

    }


}

