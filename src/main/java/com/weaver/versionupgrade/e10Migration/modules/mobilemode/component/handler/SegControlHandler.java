package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.MobileModeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 17:26
 */
public class SegControlHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);

        JSONArray btnDatas = (JSONArray) mecparamjson.get("btn_datas");
        for (Object btnData : btnDatas) {
            JSONObject Item = (JSONObject) btnData;


            String prefix="分段插件 分段:"+Item.get("segName")+" ";
            String segScript =  Util.null2String(Item.get("segScript"));
            if(StringUtils.isNotBlank(segScript)){
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "分段插件 分段:"+Item.get("segName")+" 需要调整脚本" + segScript));
            }
            beans.addAll(MobileModeUtil.getRemindConfig(Item,prefix));
        }

        this.setLists(beans);
    }

}
