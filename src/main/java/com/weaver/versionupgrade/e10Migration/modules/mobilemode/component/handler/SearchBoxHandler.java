package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 15:07
 */
public class SearchBoxHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);

        String searchAction = Util.null2String(mecparamjson.get("searchAction"));

        //搜索动作：	自定义
        if (searchAction.equals("4")) {

            String actionScript = Util.null2String(mecparamjson.get("actionScript"));
            if (StringUtils.isNotBlank(actionScript)) {
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "搜索框->自定义搜索动作需要调整脚本" +
                                        "" + actionScript));
            }

        }






        JSONArray searchGroups = (JSONArray) mecparamjson.get("searchGroups");

        if (searchGroups!=null){

        for (Object searchGroup : searchGroups) {
            JSONObject jsonObject = (JSONObject) searchGroup;
            Object showLabel = jsonObject.get("showLabel");

            JSONArray childsArray = (JSONArray) jsonObject.get("childs");

            for (Object o : childsArray) {
                JSONObject object = (JSONObject) o;
                String customScript = Util.null2String(object.get("customScript"));
                String showLabel_tmp = Util.null2String(object.get("showLabel"));
                if (StringUtils.isNotBlank(customScript)) {
                    beans.add(
                            new ComponentBean().setHandType(HandType.jsDev).setResult(
                                    "搜索框->常用搜索分类:" + showLabel + " 搜索:" + showLabel_tmp + " 需要调整脚本 "+ customScript));
                }

            }


        }
        }






        JSONArray btns = (JSONArray) mecparamjson.get("btns");

        if (btns!=null){

            for (Object btn : btns) {
            JSONObject jsonObject = (JSONObject) btn;
            String customScript = Util.null2String(jsonObject.get("btnScript"));
            String btnText = Util.null2String(jsonObject.get("btnText"));
            if (StringUtils.isNotBlank(customScript)) {
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "搜索框->自定义按钮:" + btnText + " 需要调整脚本" +
                                        "" + customScript));
            }
        }
        }


        this.setLists(beans);
    }

}
