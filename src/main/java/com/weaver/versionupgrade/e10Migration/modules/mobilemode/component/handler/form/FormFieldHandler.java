package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class FormFieldHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


//        beans.add(
//                new ComponentBean().setHandType(HandType.sqlDev).setResult("表单字段组件需要调整  "));


        this.setLists(beans);

    }

}
