package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.MobileModeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 16:03
 */
public class NavHeader extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);


        JSONArray btn_datas = (JSONArray) mecparamjson.get("btn_datas");
        String clickType = Util.null2String(mecparamjson.get("clickType"));
        String clickScript = Util.null2String(mecparamjson.get("clickScript"));

        if (clickType.equals("2")) {

            if (StringUtils.isNotBlank(clickScript)) {
                beans.add(
                    new ComponentBean().setHandType(HandType.jsDev).setResult(
                            "导航头>单击事件需要调整js脚本:" + clickScript));
            }

        }


        for (Object btn : btn_datas) {

            JSONObject jsonObject = (JSONObject) btn;

            String btnClickType = Util.null2String(jsonObject.get("btnClickType"));
            String btnText = Util.null2String(jsonObject.get("btnText"));

            //点击时间
            if (btnClickType.equals("0")) {


                String btnScript = Util.null2String(jsonObject.get("btnScript"));
                if (StringUtils.isNotBlank(btnScript)) {
                    beans.add(
                            new ComponentBean().setHandType(HandType.jsDev).setResult(
                                    "导航头>按钮:" + btnText + " 需要调整js脚本 " + btnText));

                }


                //添加菜单
            } else if (btnClickType.equals("1")) {
                try {

                    String prefix = "导航头 按钮:" + btnText + " 菜单显示名:";

                    String btnMenu = Util.null2String(jsonObject.get("btnMenu"));
                    JSONObject mecparamjson_tmp = JSON.parseObject(btnMenu);
                    JSONArray navItems = (JSONArray) mecparamjson_tmp.get("nav_items");

                    for (Object navItem : navItems) {

                        JSONObject Item = (JSONObject) navItem;
                        String source = Util.null2String(Item.get("source"));
                        String uiname = Util.null2String(Item.get("uiname"));
                        beans.addAll(MobileModeUtil.getLinkDev(Item,prefix + uiname));
                        //处理提醒
                        beans.addAll(MobileModeUtil.getRemindConfig(Item,prefix + uiname));
                    }

                } catch (Exception e) {

                }
            }
        }


        this.setLists(beans);
    }

}
