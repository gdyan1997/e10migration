package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.MobileModeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 15:57
 */
public class TabHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);

        String searchAction = Util.null2String(mecparamjson.get("searchAction"));


        JSONArray btns = (JSONArray) mecparamjson.get("tabMecMaps");
        for (Object btn : btns) {

            JSONObject jsonObject = (JSONObject) btn;

            String btnText = Util.null2String(jsonObject.get("tabLoadedScript"));
            String tabName = Util.null2String(jsonObject.get("tabName"));


            //提醒
            beans.addAll(MobileModeUtil.getRemindConfig(jsonObject,"tab页>显示名称:" + tabName));



            if (StringUtils.isNotBlank(btnText)) {
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "tab页>显示名称:" + tabName + " 需要调整脚本 " + btnText));


            }







        }


        this.setLists(beans);
    }

}
