package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component;


import java.util.ArrayList;
import java.util.List;

/**
 * @author joye
 */

public enum ComponentType {


    // 未迁移

    /**
     * ColumnBreak
     */
    ColumnBreak("ColumnBreak", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.ColumnBreakHandler", null),

    /**
     * 数据集
     **/
    DataSet("DataSet", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.DataSetHandler", null),


    /**
     * Timelinr
     **/
    Timelinr("Timelinr", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NotSupportedCompHandler", null),


    /**
     * LargeList
     **/
    LargeList("LargeList", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NotSupportedCompHandler", null),


    /**
     * HoriList
     **/
    HoriList("HoriList", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NotSupportedCompHandler", null),


    /**
     * Reply
     **/
    Reply("Reply", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("script")
                .setDesc("提交-回调js脚本需要调整，js源码:#{script}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("replyUrl")
                .setDesc("提交-提交url-链接地址需要调整，链接配置方式#{replyUrl}"));

    }}),

    /**
     * DynamicForm
     **/
    DynamicForm("DynamicForm", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.DynamicFormHandler", null),

    /**
     * ChartBoard
     **/
    ChartBoard("ChartBoard", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NotSupportedCompHandler", null),



    // 未迁移 end


    /**
     * 幻灯片->幻灯片
     */
    Slide("Slide", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.SlideHandler", null),

    /**
     * Html->HTML
     */
    Html("Html", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.HtmlHandler", null),
    /**
     * 文本->文本
     */
    RichText("RichText", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.RichTextHandler", null),
    /**
     * 导航面板->导航面板
     */
    NavPanel("NavPanel", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NavPanelHandler", null),
    /**
     * 搜索框->搜索框
     */
    SearchBox("SearchBox", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.SearchBoxHandler", null),
    /**
     * 分段组件->分段组件
     */
    SegControl("SegControl", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.SegControlHandler", null),
    /**
     * 标签栏->标签栏
     */
    TabBar("TabBar", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.TabBarHandler", null),
    /**
     * 列表->列表
     */
    NList("NList", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NListHandler", null),

    /**
     * 列表->列表模版
     */
    List("List", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NotSupportedCompHandler", null),
    /**
     * url列表->列表模版
     */
    UrlList("UrlList", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NotSupportedCompHandler", null),
    /**
     * 表格->列表
     */
    NGridTable("NGridTable", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NListHandler", null),
    /**
     * 横向列表->列表
     */
    NHoriList("NHoriList", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NListHandler", null),
    /**
     * 图表->图表
     */
    Chart("Chart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("图表sql需要调整，数据源：#{datasource}，sql: #{sql}"));
    }}),

    /**
     * 饼状图->图表（饼状图）
     */
    PieChart("PieChart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("饼状图-数据来源sql需要调整，数据源:#{datasource}，sql:#{sql}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("clickUrl")
                .setDesc("饼状图-交互设置-链接地址需要调整，链接配置方式#{clickUrl}"));

    }}),
    /**
     * 柱状图->图表（柱状图）
     */
    BarChart("BarChart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("柱状图-数据来源sql需要调整，数据源:#{datasource}，sql:#{sql}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("clickUrl")
                .setDesc("柱状图-交互设置-链接地址需要调整，链接配置方式#{clickUrl}"));

    }}),
    /**
     * 折线图->图表（折线图）
     */
    LineChart("LineChart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("折线图-数据来源sql需要调整，数据源:#{datasource}，sql:#{sql}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("clickUrl")
                .setDesc("折线图-交互设置-链接地址需要调整，链接配置方式#{clickUrl}"));

    }}),
    /**
     * 漏斗图->图表（漏斗图）
     */
    FunnelChart("FunnelChart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("漏斗图-数据来源sql需要调整，数据源:#{datasource}，sql:#{sql}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("clickUrl")
                .setDesc("漏斗图-交互设置-链接地址需要调整，链接配置方式#{clickUrl}"));

    }}),
    /**
     * 雷达图->图表（雷达图）
     */
    RadarChart("RadarChart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("雷达图-数据来源sql需要调整，数据源:#{datasource}，sql:#{sql}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("clickUrl")
                .setDesc("雷达图-交互设置-链接地址需要调整，链接配置方式#{clickUrl}"));

    }}),
    /**
     * 仪表图->图表（仪表图）
     */
    GaugeChart("GaugeChart", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("仪表图-数据来源sql需要调整，数据源:#{datasource}，sql:#{sql}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("clickUrl")
                .setDesc("仪表图-交互设置-链接地址需要调整，链接配置方式#{clickUrl}"));

    }}),
    /**
     * 工具栏->导航面板
     */
    Toolbar("Toolbar", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.ToolbarHandler", null),
    /**
     * 按钮->按钮
     */
    Button("Button", "",
            new ArrayList<ComponentBean>() {{
                this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("button_datas.buttonName-script")
                        .setDesc("按钮执行脚本需要调整，按钮名称：#{buttonName}，script: #{script}"));
            }}
            ),
    /**
     * 浮动按钮->浮动按钮
     */
    FloatButton("FloatButton", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.FloatButtonHandler", null),
    /**
     * tab->tab
     */
    Tab("Tab", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.TabHandler", null),
    /**
     * 导航头->页头
     */
    NavHeader("NavHeader", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NavHeader", null),
    /**
     * 时间轴->时间轴
     */
    NTimeline("NTimeline", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NListHandler", null),
    /**
     * rss列表->rss列表
     */
    RSSList("RSSList", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("source")
                .setDesc("RSS列表订阅源需要调整，订阅源：#{source} "));
    }}),
    /**
     * 顶部搜索->筛选组件
     */
    TopSearch("TopSearch", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("actionScript")
                .setDesc("顶部搜索 自定义搜索js脚本 需要调整，js：#{actionScript}"));
    }}),
    /**
     * 数据明细->数据明细
     */
    DataDetail("DataDetail", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("dataDetail_datas.entryName-entryContent-entryClickScript")
                .setDesc("数据明细js 需要调整 ： 明细名称:  #{entryName} ;明细内容:  #{entryContent} ; 单击事件脚本:#{entryClickScript}  "));
    }}),
    /**
     * 统计面板->数据明细
     */
    CountPanel("CountPanel", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("items.text-content-script")
                .setDesc("统计面板js 需要调整 ： 面板名称:  #{text} ;面板内容:  #{content} ; 单击事件脚本:#{script}  "));
    }}),
    /**
     * 地图->地图
     */
    AMap("AMap", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-additionalSql")
                .setDesc("地图-附加点信息:sql需要调整，数据源:#{datasource}，sql:#{additionalSql}"));
        this.add(new ComponentBean().setHandType(HandType.javaDev).setTargetpath("apiConfig")
                .setDesc("地图-附加点信息:接口信息需要调整，链接配置方式#{apiConfig}"));

    }}),
    /**
     * 树形->树形
     */
    Tree("Tree", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("customdetail.appendhtml")
                .setDesc("树形-节点附加html需要调整,html源码:#{appendhtml}"));
        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("customdetail.inputurl")
                .setDesc("树形-节点url解析:链接地址需要调整，链接配置方式#{inputurl}"));


    }}),
    /**
     * 倒计时->倒计时
     */
    Countdown("Countdown", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 二维码->二维码
     */
    QRCode("QRCode", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 用户头像->用户头像
     */
    UserAvatar("UserAvatar", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 日历->日历
     */
    Calendar("Calendar", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("clickScript")
                .setDesc("日历-点击新建自定义js脚本需要调整,js脚本源码:#{clickScript}"));
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("customClickScript")
                .setDesc("日历-交互设置:自定义js脚本需要调整,js脚本源码:#{customClickScript}"));

        this.add(new ComponentBean().setHandType(HandType.sqlDev).setTargetpath("datasource-sql")
                .setDesc("日历-日期标记:sql需要调整,数据源:#{datasource},sql:#{sql}"));

        this.add(new ComponentBean().setHandType(HandType.linkDev).setTargetpath("url")
                .setDesc("日历-日期标记:链接地址需要调整,链接配置方式#{url}"));

    }}),
    /**
     * 天气->天气
     */
    Weather("Weather", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 进度条->进度条
     */
    ProgressBar("ProgressBar", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 图片->图片
     */
    Picture("Picture", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("jscode")
                .setDesc("图片-自定义链接js脚本需要调整,js脚本源码:#{jscode}"));
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("custom")
                .setDesc("图片-自定义链接:链接地址需要调整,链接配置方式:#{custom}"));


    }}),
    /**
     * 视频->视频
     */
    Video("Video", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 消息面板->文本
     */
    MessagePanel("MessagePanel", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 提示面板->文本
     */
    TipPanel("TipPanel", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * iframe->文本
     */
    Iframe("Iframe", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 通知公告->通知公告
     */
    NoticeBar("NoticeBar", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 步骤条->步骤条
     */
    Steps("Steps", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.EmptyHandler", null),
    /**
     * 导航栏->导航栏
     */
    Navigation("Navigation", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NavigationHandler", null),
    /**
     * 表单->中间表
     */
    Form("Form", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormHandler", null),
    /**
     * 单行文本->单行文本
     */
    FInputText("FInputText", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 多行文本->多行文本
     */
    FTextarea("FTextarea", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 下拉项->下拉菜单
     */
    FSelect("FSelect", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.SelectFieldHandler", null),
    /**
     * 单多选->复选框
     */
    FCheckbox("FCheckbox", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.SelectFieldHandler", null),
    /**
     * 图片->图片
     */
    FPhoto("FPhoto", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 附件->附件
     */
    FFile("FFile", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 时间日期->时间日期
     */
    FDateTime("FDateTime", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 表单按钮->表单按钮
     */
    FButton("FButton", "", new ArrayList<ComponentBean>() {{
        this.add(new ComponentBean().setHandType(HandType.jsDev).setTargetpath("fbutton_datas.buttonname-_action-callbackfunction")
                .setDesc("表单按钮需要调整 ： 按钮类型:  #{_action} ; 按钮名称:#{buttonname}  ; 按钮回调js函数:#{callbackfunction}  "));
    }}),
    /**
     * 大图列表->列表（列表式）
     */
    NLargeList("NLargeList", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.NListHandler", null),
    /**
     * 隐藏域-> 单行文本
     */
    FHidden("FHidden", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * check框-> 开关
     */
    FCheck("FCheck", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 浏览按钮-> 关联类型--
     */
    FBrowser("FBrowser", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 星级评分-> 评分
     */
    FScores("FScores", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 语音-> 语音
     */
    FSound("FSound", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * lbs-> 地理位置
     */
    FLbs4amap("FLbs4amap", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 批注-> 签名
     */
    FHandwriting("FHandwriting", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 拍视频-> 拍视频
     */
    FVideo("FVideo", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 滑块-> 进度条
     */
    FRange("FRange", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),
    /**
     * 地址-> 单行文本
     */
    FAddress("FAddress", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.FormFieldHandler", null),

    /**
     * 浏览按钮接口
     */
    FAPIBrowser("FAPIBrowser", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.BrowserCusFieldHandler", null),

    /**
     * 明细表-> 明细表
     */
    DetailTable("DetailTable", "com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form.DetailTableHandler", null);



    private String type;

    private String packagePath;
    private List<ComponentBean> componentBeans;

    ComponentType(String type, String packagePath, List<ComponentBean> componentBeans) {
        this.type = type;
        this.packagePath = packagePath;
        this.componentBeans = componentBeans;
    }

    public String getType() {
        return type;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public List<ComponentBean> getComponentBean() {
        return componentBeans;
    }
}
