package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component;


import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 组件转换抽象类
 * 所有需要转换数据的组件都需要继承这个类
 */
public abstract class AbstractComponentHandler implements Serializable {


    List<ComponentBean> lists;

    public List<ComponentBean> getLists() {
        if (lists == null) {
            return new ArrayList<>();
        }
        return lists;
    }

    public void setLists(List<ComponentBean> lists) {
    
        this.lists = lists;
    }

    public abstract void getHandInfo(ComponentConvertContext componentConvertContext);

}
