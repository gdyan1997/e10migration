package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 13:51
 */
public class SlideHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String slideBtnType = String.valueOf( mecparamjson.get("slideBtnType"));

        if (slideBtnType.equals("R")){
            String slideUrl = Util.null2String(mecparamjson.get("slideUrl"));

            beans.add(
                    new ComponentBean().setHandType(HandType.linkDev).setResult(
                            "幻灯片图片来源URL需要调整：" + slideUrl ));


        }else {
            JSONArray picItems = (JSONArray) mecparamjson.get("pic_items");
            for (Object picItem : picItems) {
                JSONObject picItemJson=(JSONObject)picItem;

                String source = Util.null2String(picItemJson.get("source"));
                String pic_desc = Util.null2String(picItemJson.get("pic_desc"));

                if (source.equals("1")){

                    String pic_path = Util.null2String(picItemJson.get("pic_path"));

                    beans.add(
                            new ComponentBean().setHandType(HandType.linkDev).setResult(
                                    "幻灯片"+pic_desc+" 图片链接地址需要调整" + pic_path));

                }else if (source.equals("3")){
                    String jscode = Util.null2String(picItemJson.get("jscode"));



                    beans.add(
                            new ComponentBean().setHandType(HandType.linkDev).setResult(
                                    "幻灯片"+pic_desc+" 图片链接脚本需要调整" + jscode));
                }






            }

        }

        this.setLists(beans);


    }

}
