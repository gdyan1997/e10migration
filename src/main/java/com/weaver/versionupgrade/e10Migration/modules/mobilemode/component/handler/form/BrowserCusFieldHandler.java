package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class BrowserCusFieldHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String sourcePageId = Util.null2String(mecparamjson.get("sourcePageId"));
        String sourceMecId = Util.null2String(mecparamjson.get("sourceMecId"));
        String rightAction_SQL = Util.null2String(mecparamjson.get("rightAction_SQL"));

        if (StringUtils.isNotBlank(sourcePageId)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.javaDev).setResult("表单字段 浏览按钮组件 接口 需要调整  sourcePageId："
                            + sourcePageId + " sourceMecId:" + sourceMecId));

        }

        JSONObject apiConfig = (JSONObject) mecparamjson.get("apiConfig");

        beans.add(
                new ComponentBean().setHandType(HandType.javaDev).setResult(
                        " 显示名称存取  接口信息：" + Util.null2String(apiConfig.get("api"))));




        this.setLists(beans);

    }

}
