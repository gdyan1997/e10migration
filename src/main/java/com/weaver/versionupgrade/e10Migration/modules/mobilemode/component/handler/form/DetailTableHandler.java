package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.ValidObjCacheUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.util.VFormUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class DetailTableHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String tablename = Util.null2String(mecparamjson.get("tablename"));
        String datasource = Util.null2String(mecparamjson.get("datasource"));
        String sqlwhere = Util.null2String(mecparamjson.get("sqlwhere"));
        String validateScript = Util.null2String(mecparamjson.get("validateScript"));
        String pageLoadedValidateScript = Util.null2String(mecparamjson.get("pageLoadedValidateScript"));


        beans.add(
                new ComponentBean().setHandType(HandType.sqlDev).setResult("明细表数据源 需要调整  数据源： " + E10CubeUtil.getdatasourcename(datasource) + "  子表表名： " + tablename));



        if (StringUtils.isNotBlank(sqlwhere)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult("明细表查询条件 需要调整  ： " + sqlwhere));

        }


        if (StringUtils.isNotBlank(validateScript)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.jsDev).setResult("明细表验证脚本 需要调整  ： " + validateScript));

        }

        if (StringUtils.isNotBlank(pageLoadedValidateScript)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.jsDev).setResult("明细表加载脚本 需要调整  ： " + pageLoadedValidateScript));

        }




        this.setLists(beans);

    }

}
