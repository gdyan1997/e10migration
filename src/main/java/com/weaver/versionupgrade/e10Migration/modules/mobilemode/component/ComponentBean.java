package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component;

import weaver.general.Util;

/**
 * @author ygd2020
 * @date 2024-5-8 18:27
 * Description:
 **/
public class ComponentBean {

    public String targetpath = ""; // 路径  多个参数


    public String desc = "";  // 如果需要描述多个 可以用 #{参数名称}


    public String result = "";  // 如果需要描述多个 可以用 #{参数名称}

    public String getResult() {
        return result;
    }

    public ComponentBean setResult(String result) {

        result = Util.formatMultiLang(result);

        this.result = result;
        return this;
    }

    HandType handType;

    public String getTargetpath() {
        return targetpath;
    }

    public String getDesc() {
        return desc;
    }

    public HandType getHandType() {
        return handType;
    }

    public ComponentBean setTargetpath(String targetpath) {
        this.targetpath = targetpath;
        return this;
    }

    public ComponentBean setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public ComponentBean setHandType(HandType handType) {
        this.handType = handType;
        return this;
    }


    @Override
    public String toString() {
        return result;
    }
}
