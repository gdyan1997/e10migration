package com.weaver.versionupgrade.e10Migration.modules.mobilemode;

import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.ValidObjCacheUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;

import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.factory.ComponentConvertFactory;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.Util;
import weaver.init.InitWeaverServer;
import weaver.systeminfo.SystemEnv;

import java.util.*;

import com.alibaba.fastjson.JSON;

import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author ygd2020
 * @date 2024-5-8 15:43
 * Description:
 **/
public class ExportMobileModeExcel {


    public int pagejs = 0;//页面js
    public int pagecss = 0;// 页面css
    public int cusCompcount = 0;//自定义组件
    public int NotSupportCompcount = 0;//e8组件
    public int sqlDevcount = 0;//调整组件
    public int linkDevcount = 0;//链接/js 组件
    public int javaDevcount = 0;// java 组件
    public int jsDevcount = 0;// js组件


    public String domobilemodeApp(String path) {
        // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("移动建模_应用清单_MobilemodeApp", path);

        RecordSet rs = new RecordSet();
        rs.executeQuery("select id,subcompanyid,appname  from mobileappbaseinfo where ISDELETE = 0 order by id ");


        while (rs.next()) {
            String appid = rs.getString("id");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String subcompanyid = rs.getString("subcompanyid");
            doMobileModeExcel(writeEntity, appid, appname, subcompanyid);

        }


        pageDevCountList(writeEntity);//页面二开数量统计
        CompDevCountList(writeEntity);//页面组件二开调整数量统计

        pagePositionList(writeEntity);//迁移后的页面位置


        String filePath = ExcelWrite.excelWriteToFile(writeEntity);

        return filePath;

    }

    private void pagePositionList(ExcelWriteEntity writeEntity) {

        isloadCache();

        List<String> headerNames = new ArrayList<String>() {{
            add("应用id ");
            add("应用名称");
            add("页面id");
            add("页面名称");
            add("E10位置调整说明");
        }};

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mobileappbaseinfo.id as appid ,mobileappbaseinfo.appname,apphomepage.ID as pageid,pagename \n" +
                "from mobileappbaseinfo join apphomepage on mobileappbaseinfo.ID = APPID " +
                " where   mobileappbaseinfo.ISDELETE =0 ");


        if (ConfigUtil.isE9()) {// 不是8
            sql += "and apphomepage.isdelete = 0 ";
        }


        sql += " order by mobileappbaseinfo.id,apphomepage.ID";

        rs.executeQuery(sql);


        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String pageid = rs.getString("pageid");
            String pagename = Util.formatMultiLang(rs.getString("pagename"));


            value.add(_appid);
            value.add(appname);
            value.add(pageid);
            value.add(pagename);

            value.add(getPositionDesc(_appid, appname, pageid, pagename));


            values.add(value);

        }


        ExcelWrite.addExcelFirstModal(writeEntity, "调整数量概览", "迁移后移动页面迁移位置明细", headerNames, values);


    }



    private String getPositionDesc(String _appid, String appname, String pageid, String pagename) {


        if (!modepage.containsKey(pageid)) {
            return "移动建模文件夹->" + appname + "->页面->" + pagename;
        }


        String modeinfo = modepage.get(pageid);

        String uitype = "";
        String e9_modeid = "";
        String isdefault = "";
        try {
            String[] split = modeinfo.split("&&&");
            e9_modeid = split[0];
            uitype = split[1];
            isdefault = split[2];
        } catch (Exception e) {
            e.printStackTrace();
        }

        //0 新建布局  1 显示布局 2 编辑布局
        switch (uitype) {
            case "0":
                pagename += "_" + "新建";
                break;
            case "1":
                pagename += "_" + "显示";
                break;
            case "2":
                pagename += "_" + "编辑";
                break;
        }

        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        sourceDBUtil.executeQuery("\n" +
                "select mt.id         as appid,\n" +
                "       TREEFIELDNAME as appname,\n" +
                "       mi.id         as modeid,\n" +
                "       mi.MODENAME   as modename\n" +
                "       from modetreefield mt  join modeinfo mi\n" +
                "on mi.MODETYPE = mt.id where mi.id = ?", e9_modeid);

        String modename = "";
        if (sourceDBUtil.next()) {
            appname = Util.processBody(sourceDBUtil.getString("appname"));
            modename = Util.processBody(sourceDBUtil.getString("modename"));
        }

        return "表单建模文件夹->" + appname + "->表单->" + modename + "->视图-移动端->" + pagename;
    }

    private void CompDevCountList(ExcelWriteEntity writeEntity) {

        List<String> headerNames = new ArrayList<String>() {{
            add("自定义组件");
            add("E8组件");
            add("sql调整");
            add("链接/js调整");
            add("java开发调整");
            add("js开发调整");
        }};

        List<List<String>> values = new ArrayList<List<String>>() {{
            add(new ArrayList<String>() {{
                add(cusCompcount + "");
                add(NotSupportCompcount + "");
                add(sqlDevcount + "");
                add(linkDevcount + "");
                add(javaDevcount + "");
                add(jsDevcount + "");
            }});
        }};

        ExcelWrite.addExcelFirstModal(writeEntity, "调整数量概览", "组件二开调整数量统计", headerNames, values);

    }

    private void pageDevCountList(ExcelWriteEntity writeEntity) {

        List<String> headerNames = new ArrayList<String>() {{
            add("页面js");
            add("页面css");
        }};

        List<List<String>> values = new ArrayList<List<String>>() {{
            add(new ArrayList<String>() {{
                add(pagejs + "");
                add(pagecss + "");
            }});
        }};

        ExcelWrite.addExcelFirstModal(writeEntity, "调整数量概览", "页面js/css数量统计", headerNames, values);


    }


    void doMobileModeExcel(ExcelWriteEntity writeEntity, String appid, String appname, String subcompanyid) {


        String sheetname = E10CubeUtil.getappname(appid, appname, subcompanyid);

        pagecodeList(writeEntity, "页面代码块清单", sheetname, appid);//页面组件清单

        pageCompList(writeEntity, "页面组件清单", sheetname, appid);//页面组件清单


    }

    void pagecodeList(ExcelWriteEntity writeEntity, String functionName, String sheetname, String appid) {
        List<String> headerNames = pagecodeListNames();
        List<List<String>> values = pagecodeListValue(appid);
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);

    }


    void pageCompList(ExcelWriteEntity writeEntity, String functionName, String sheetname, String appid) {
        List<String> headerNames = pageCompListNames();
        List<List<String>> values = pageCompListValue(appid);
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);


    }


    List<String> pageCompListNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("页面id");
        headerNames.add("页面名称");
        headerNames.add("组件id");
        headerNames.add("组件类型");
        headerNames.add("组件名称");


        for (HandType handType : HandType.getValues()) {

            headerNames.add(handType.getType());

        }


        return headerNames;
    }

    List<String> pagecodeListNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用id");
        headerNames.add("应用名称");
        headerNames.add("页面id");
        headerNames.add("页面名称");
        headerNames.add("js");
        headerNames.add("css");

        return headerNames;
    }

    public static final List<Map<String, String>>
            plugins = JSON.parseObject("[{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Navigation\",\"text\":\"128104\",\"type\":\"nav\",\"order\":\"1\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Slide\",\"text\":\"128105\",\"type\":\"basic\",\"order\":\"2\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"List\",\"text\":\"128106\",\"type\":\"list\",\"order\":\"3\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Html\",\"text\":\"128107\",\"type\":\"basic\",\"order\":\"4\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Toolbar\",\"text\":\"128108\",\"type\":\"nav\",\"order\":\"5\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Chart\",\"text\":\"128109\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"Map\",\"text\":\"128111\",\"type\":\"other\",\"order\":\"7\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"Timelinr\",\"text\":\"128112\",\"type\":\"list\",\"order\":\"8\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NavPanel\",\"text\":\"128114\",\"type\":\"nav\",\"order\":\"10\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Tree\",\"text\":\"128116\",\"type\":\"other\",\"order\":\"11\"},{\"isEnabled\":\"0\",\"unique\":\"1\",\"id\":\"TouchButton\",\"text\":\"128170\",\"type\":\"nav\",\"order\":\"12\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Countdown\",\"text\":\"128118\",\"type\":\"other\",\"order\":\"13\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"UrlList\",\"text\":\"128119\",\"type\":\"list\",\"order\":\"14\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Form\",\"text\":\"128145\",\"type\":\"form\",\"order\":\"1\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FInputText\",\"text\":\"128146\",\"type\":\"form\",\"order\":\"16\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FHidden\",\"text\":\"128147\",\"type\":\"form\",\"order\":\"17\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FTextarea\",\"text\":\"128148\",\"type\":\"form\",\"order\":\"18\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FSelect\",\"text\":\"128149\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"FButton\",\"text\":\"128171\",\"type\":\"form\",\"order\":\"49\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FSound\",\"text\":\"128154\",\"type\":\"form\",\"order\":\"21\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"FLbs\",\"text\":\"128155\",\"type\":\"form\",\"order\":\"22\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FPhoto\",\"text\":\"128156\",\"type\":\"form\",\"order\":\"23\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"QRCode\",\"text\":\"128122\",\"type\":\"other\",\"order\":\"24\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"Reply\",\"text\":\"128123\",\"type\":\"other\",\"order\":\"25\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Tab\",\"text\":\"128127\",\"type\":\"nav\",\"order\":\"28\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"UserAvatar\",\"text\":\"128129\",\"type\":\"other\",\"order\":\"29\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NavHeader\",\"text\":\"128130\",\"type\":\"nav\",\"order\":\"30\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FCheckbox\",\"text\":\"128150\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DataSet\",\"text\":\"128131\",\"type\":\"data\",\"order\":\"32\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"SegControl\",\"text\":\"128133\",\"type\":\"nav\",\"order\":\"46\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FCheck\",\"text\":\"128151\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FBrowser\",\"text\":\"128152\",\"type\":\"form\",\"order\":\"20\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"GridTable\",\"text\":\"128134\",\"type\":\"list\",\"order\":\"36\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"Calendar\",\"text\":\"128135\",\"type\":\"other\",\"order\":\"37\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Button\",\"text\":\"128142\",\"type\":\"basic\",\"order\":\"48\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"Chart2\",\"text\":\"128110\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DataDetail\",\"text\":\"128132\",\"type\":\"data\",\"order\":\"32\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"TabBar\",\"text\":\"128136\",\"type\":\"nav\",\"order\":\"47\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"HoriList\",\"text\":\"128137\",\"type\":\"list\",\"order\":\"15\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Weather\",\"text\":\"128138\",\"type\":\"other\",\"order\":\"43\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"ProgressBar\",\"text\":\"128139\",\"type\":\"other\",\"order\":\"44\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"ColumnBreak\",\"text\":\"128140\",\"type\":\"nav\",\"order\":\"45\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FFile\",\"text\":\"128158\",\"type\":\"form\",\"order\":\"45\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Picture\",\"text\":\"128141\",\"type\":\"other\",\"order\":\"47\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Video\",\"text\":\"128143\",\"type\":\"other\",\"order\":\"49\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Iframe\",\"text\":\"128144\",\"type\":\"other\",\"order\":\"50\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FHandwriting\",\"text\":\"128157\",\"type\":\"form\",\"order\":\"24\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"UrlGridTable\",\"text\":\"128756\",\"type\":\"list\",\"order\":\"52\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"BarChart\",\"text\":\"128820\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"PieChart\",\"text\":\"128841\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"LineChart\",\"text\":\"32615\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DetailTable\",\"text\":\"130658\",\"type\":\"form\",\"order\":\"2\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FunnelChart\",\"text\":\"131032\",\"type\":\"chart\",\"order\":\"6\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FScores\",\"text\":\"131418\",\"type\":\"form\",\"order\":\"20\"},{\"isEnabled\":\"1\",\"unique\":\"\",\"id\":\"CountPanel\",\"text\":\"381999\",\"type\":\"data\",\"order\":\"33\"},{\"isEnabled\":\"1\",\"unique\":\"\",\"id\":\"RSSList\",\"text\":\"382128\",\"type\":\"list\",\"order\":\"33\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FRange\",\"text\":\"382243\",\"type\":\"form\",\"order\":\"46\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FDateTime\",\"text\":\"382272\",\"type\":\"form\",\"order\":\"47\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"RadarChart\",\"text\":\"382297\",\"type\":\"chart\",\"order\":\"7\"},{\"isEnabled\":\"1\",\"unique\":\"\",\"id\":\"FloatButton\",\"text\":\"128170\",\"type\":\"nav\",\"order\":\"12\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"GaugeChart\",\"text\":\"382317\",\"type\":\"chart\",\"order\":\"8\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"LargeList\",\"text\":\"383006\",\"type\":\"list\",\"order\":\"16\"},{\"isEnabled\":\"0\",\"unique\":\"0\",\"id\":\"APIList\",\"text\":\"386407\",\"type\":\"list\",\"order\":\"53\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FAPIBrowser\",\"text\":\"388150\",\"type\":\"form\",\"order\":\"19\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"AMap\",\"text\":\"128111\",\"type\":\"other\",\"order\":\"7\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FLbs4amap\",\"text\":\"128155\",\"type\":\"form\",\"order\":\"22\"},{\"isEnabled\":\"1\",\"unique\":\"1\",\"id\":\"TipPanel\",\"text\":\"500722\",\"type\":\"other\",\"order\":\"51\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"SearchBox\",\"text\":\"502101\",\"type\":\"basic\",\"order\":\"52\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"Steps\",\"text\":\"503558\",\"type\":\"other\",\"order\":\"53\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NoticeBar\",\"text\":\"503937\",\"type\":\"other\",\"order\":\"54\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NList\",\"text\":\"128106\",\"type\":\"list\",\"order\":\"1\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"RichText\",\"text\":\"507147\",\"type\":\"basic\",\"order\":\"47\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"TopSearch\",\"text\":\"507676\",\"type\":\"list\",\"order\":\"53\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NGridTable\",\"text\":\"128134\",\"type\":\"list\",\"order\":\"2\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"DynamicForm\",\"text\":\"512437\",\"type\":\"form\",\"order\":\"3\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NTimeline\",\"text\":\"128112\",\"type\":\"list\",\"order\":\"8\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NHoriList\",\"text\":\"128137\",\"type\":\"list\",\"order\":\"15\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"NLargeList\",\"text\":\"383006\",\"type\":\"list\",\"order\":\"16\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FAddress\",\"text\":\"516534\",\"type\":\"form\",\"order\":\"48\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"ChartBoard\",\"text\":\"521227\",\"type\":\"chart\",\"order\":\"9\"},{\"isEnabled\":\"1\",\"unique\":\"0\",\"id\":\"FVideo\",\"text\":\"523351\",\"type\":\"form\",\"order\":\"25\"}]", List.class);

    List<List<String>> pageCompListValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mobileappbaseinfo.id as appid ,mobileappbaseinfo.appname,apphomepage.ID as pageid,pagename,mectype,mecparam,mobileextendcomponent.id\n" +
                "from mobileappbaseinfo join apphomepage on mobileappbaseinfo.ID = APPID join mobileextendcomponent on apphomepage.ID = OBJID\n" +
                " where   mobileappbaseinfo.ISDELETE =0 ");


        if (ConfigUtil.isE9()) {// 不是8
            sql += "and apphomepage.isdelete = 0 ";
        }

        if (!appid.equals("")) {
            sql += " and mobileappbaseinfo.id=" + appid;
        }

        sql += " order by mobileappbaseinfo.id,apphomepage.ID";

        rs.executeQuery(sql);


        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String pageid = rs.getString("pageid");
            String mectype = rs.getString("mectype");
            String pagename = Util.formatMultiLang(rs.getString("pagename"));
            String compid = rs.getString("id");
            String mecparam = rs.getString("mecparam");


            value.add(_appid);
            value.add(appname);
            value.add(pageid);
            value.add(pagename);

            value.add(compid);
            value.add(mectype);
            // todo  判断mectype 后可以判断是否需要检查
            String mecTypeDesc = getMecTypeDesc(plugins, mectype);


            value.add(mecTypeDesc);

            List<ComponentBean> lists = getMobileModeMeCHandInfoList(pageid, compid, mectype, mecparam);
//
            if (lists.isEmpty()) {// 没抓出配置项
                continue;
            }

            Map<String, List<ComponentBean>> collect = lists.stream().sorted(Comparator.comparingInt(bean -> bean.getHandType().getOrdeid())).collect(Collectors.groupingBy(bean -> bean.getHandType().getType()));

            setCount(lists);

            for (HandType handType : HandType.getValues()) {

                String type = handType.getType();

                if (collect.containsKey(type)) {
                    value.add(type + ":\n" + StringUtils.join(collect.get(type), "\n"));
                } else {
                    value.add("");
                }

            }

            values.add(value);

        }

        return values;

    }

    private void setCount(List<ComponentBean> lists) {

        for (ComponentBean bean : lists) {

            HandType handType = bean.getHandType();
            switch (handType) {
                case sqlDev:
                    sqlDevcount++;
                    break;
                case cusComp:
                    cusCompcount++;
                    break;
                case linkDev:
                    linkDevcount++;
                    break;
                case jsDev:
                    jsDevcount++;
                    break;
                case javaDev:
                    javaDevcount++;
                    break;
                case notSupportComp:
                    NotSupportCompcount++;
                    break;

            }

        }

    }


    public static void main(String[] args) {


        InitWeaverServer initWeaverServer = new InitWeaverServer();
        initWeaverServer.init(null);

        String mecid = "CAC448DDB10000011FB1B0D014DF1077";

        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select * from mobileextendcomponent where  id = ?", mecid);
        if (rs.next()) {

            List<ComponentBean> lists = getMobileModeMeCHandInfoList(rs.getString("objid")
                    , rs.getString("id"), rs.getString("mectype"), rs.getString("mecparam"));
            Map<String, List<ComponentBean>> collect = lists.stream().sorted(Comparator.comparingInt(bean -> bean.getHandType().getOrdeid())).collect(Collectors.groupingBy(bean -> bean.getHandType().getType()));

            for (HandType handType : HandType.values()) {

                String type = handType.getType();

                if (collect.containsKey(type)) {
                    System.out.println(type + ":\n" + StringUtils.join(collect.get(type), "\n"));
                }

            }
        }


        System.out.println();


    }

    public static List<ComponentBean> getMobileModeMeCHandInfoList(String pageid, String compid, String mectype, String mecparam) {


        ComponentConvertContext componentConvertContext = new ComponentConvertContext();
        componentConvertContext.setE9CompId(compid);

        componentConvertContext.setE9pageId(pageid);
        componentConvertContext.setE9CompType(mectype);
        componentConvertContext.setMecparam(mecparam);
        ComponentType componentType = null;
        try {

            componentType = ComponentType.valueOf(mectype);

        } catch (Exception e) {
            System.out.println("not found " + mectype);

            return new ArrayList<ComponentBean>() {{
                this.add(new ComponentBean().setHandType(HandType.cusComp).setResult("自定义组件需要重新配置开发"));
            }};
        }
        try {

            componentConvertContext.setComponentType(componentType);

            AbstractComponentHandler componentHandler = ComponentConvertFactory.getComponentHandler(componentType, componentConvertContext);

            return componentHandler.getLists();
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
            return new ArrayList<ComponentBean>() {{
                this.add(new ComponentBean().setHandType(HandType.javaDev).setResult("组件抓取异常，请联系版本升级部->" + Util.getExceptionPintStackTraceInfo(e)));

            }};
        }

    }

    List<List<String>> pagecodeListValue(String appid) {

        List<List<String>> values = new ArrayList<List<String>>();


        RecordSet rs = new RecordSet();

        String sql = ("select mobileappbaseinfo.id as appid ,mobileappbaseinfo.appname,apphomepage.ID as pageid,pagename,pagecontent\n" +
                "from mobileappbaseinfo join apphomepage on mobileappbaseinfo.ID = APPID  " +
                " where mobileappbaseinfo.ISDELETE =0 ");


        if (ConfigUtil.isE9()) {// 不是8
            sql += "and apphomepage.isdelete = 0 ";
        }

        if (!appid.equals("")) {
            sql += " and mobileappbaseinfo.id=" + appid;
        }

        sql += " order by mobileappbaseinfo.id,apphomepage.ID";

        rs.executeQuery(sql);


        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String _appid = rs.getString("appid");
            String appname = Util.formatMultiLang(rs.getString("appname"));
            String pageid = rs.getString("pageid");
            String pagecontent = rs.getString("pagecontent");
            String pagename = Util.formatMultiLang(rs.getString("pagename"));


            value.add(_appid);
            value.add(appname);
            value.add(pageid);
            value.add(pagename);
            String script = E10CubeUtil.getDevCode(pagecontent, "script");
            String style = E10CubeUtil.getDevCode(pagecontent, "style");

            if (StringUtils.isBlank(script) && StringUtils.isBlank(style)) {
                continue;
            }

            if (StringUtils.isNotBlank(script)) {
                pagejs++;
            }

            if (StringUtils.isNotBlank(style)) {
                pagecss++;
            }

            value.add(script);
            value.add(style);


            values.add(value);

        }

        return values;

    }

    public String getText(String type, Map<String, String> plugin) {
        if ("custom".equals(type)) {
            return Util.null2String(plugin.get("text"));
        } else {
            return SystemEnv.getHtmlLabelName(Util.getIntValue(plugin.get("text")), 7);
        }
    }

    private String getMecTypeDesc(List<Map<String, String>> plugins, String mectype) {
        for (Map<String, String> plugin : plugins) {
            String id = Util.null2String(plugin.get("id"));

            if (id.equals(mectype)) {
                return getText(Util.null2String(plugin.get("type")), plugin);
            }

        }
        return "组件未识别";
    }


    public static boolean isinit = false;

    public static ConcurrentHashMap<String, String> validapp = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, String> modepage = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, String> cuspage = new ConcurrentHashMap<>();


    /**
     * 是否缓存
     *
     * @param
     * @return
     */
    public static boolean isloadCache() {

        if (!isinit) {
            initCache();
        }
        return true;
    }


    public synchronized static void initCache() {
        if (isinit) {
            return;
        }

        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();
        rs.executeQuery("select id,isdelete from mobileappbaseinfo ");
        while (rs.next()) {
            String isdelete = rs.getString("isdelete");
            String id = rs.getString("id");
            if (!"1".equals(isdelete)) {//不是删除的就是有效的
                validapp.put(id, id);
            }
        }


        rs.executeQuery("select id,isdelete,appid  from apphomepage");

        while (rs.next()) {
            String pageid = rs.getString("id");
            String isdelete = rs.getString("isdelete");
            if ("1".equals(isdelete)) {//删除状态
                continue;
            }

            String appid = rs.getString("appid");

            if (!validapp.containsKey(appid)) {// 不是有效的应用 无需处理
                continue;
            }
            checkPage(pageid);

        }

        isinit = true;
    }

    public static void checkPage(String pageid) {


        org.apache.flink.runtime.customRest.util.SourceDBUtil rs = new org.apache.flink.runtime.customRest.util.SourceDBUtil();

        rs.executeQuery("select  modelid, uitype,isdefault from apphomepage_model where uitype !=3 and apphomepageid =?", pageid);


        if (rs.next()) {//存在模块布局相关的东西
            String modelid = rs.getString("modelid");
            String uitype = rs.getString("uitype");// 0 新建布局  1 显示布局 2 编辑布局
            String isdefault = rs.getString("isdefault");

            if (ValidObjCacheUtil.isValidMode(modelid)) {//有效的模块  进行布局类型 布局模块 默认布局的信息保存
                modepage.put(pageid, modelid + "&&&" + uitype + "&&&" + isdefault);
            } else {
                cuspage.put(pageid, pageid);
            }
        } else {//不存在模块布局相关的 都转为自定义页面
            cuspage.put(pageid, pageid);
        }


    }
}
