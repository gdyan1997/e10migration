package com.weaver.versionupgrade.e10Migration.modules.edc;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.Util;
import weaver.hrm.resource.ResourceComInfo;

import java.util.*;

/**
 * @author wujiahao
 * @date 2024/5/11 14:07
 */
public class ExportEdcExcel {

    public static String getDataSetType(String datasetId, String sqlIds) {
        String dataSetType = "";

        RecordSet rs = new RecordSet();

        List<String> edcList = new ArrayList<String>();
        List<String> ufList = new ArrayList<String>();
        List<String> formTableList = new ArrayList<String>();


        int i = 0;
        rs.executeQuery("select uuid,tableName,dataSetId  from  edc_reportdstable where dataSetId = '" + datasetId + "'");
        while (rs.next()) {
            String tableName = rs.getString("tableName");

            if (tableName != null && !"".equals(tableName)) {
                i++;

                if (!tableName.contains("select ") && !tableName.contains("SELECT ")) { //不是sql
                    if (tableName.contains("uf_") || tableName.contains("edc_uf_") || tableName.contains("formtable_main")) {
                        if (tableName.contains("formtable_main")) {
                            formTableList.add(tableName);
                        } else if (tableName.contains("edc_uf_")) {
                            edcList.add(tableName);
                        } else {
                            ufList.add(tableName);
                        }
                    }
                }

            }

        }

        if (formTableList.size() == i) {
            dataSetType = "1";
        } else if (ufList.size() == i) {
            dataSetType = "2";
        } else if (edcList.size() == i) {
            dataSetType = "3";
        } else if (formTableList.size() + edcList.size() + ufList.size() == i) {  //直连数仓
            dataSetType = "999";
        } else { //sql数据集
            dataSetType = "-100";
        }


        //设置sql过滤, 分组聚合, 新增计算列, 都迁移成sql
        if (!"-100".equals(dataSetType)) {
            if (("," + sqlIds + ",").contains("," + datasetId + ",")) {
                dataSetType = "-100";
            }
        }


        return dataSetType;

    }

    //导出主方法
    public String doExportEdcDetail(String path) {
        // 创建一个模块excel文件
        ExcelWriteEntity writeEntity = new ExcelWriteEntity("数据中心配置清单_edcAll", path);  //excel文件名称


        RecordSet e9Rs = new RecordSet();
        String sqlIds = "";
        /*e9Rs.executeQuery("SELECT  uuid \n" +
                "                    FROM edc_reportdataset es\n" +
                "                    WHERE\n" +
                "                        EXISTS (SELECT 1 FROM edc_reportdsfilter  ef WHERE es.uuid=ef.dataSetId  and  conditionsql is not null)\n" +
                "                        OR\n" +
                "                        EXISTS (SELECT 1 FROM edc_reportdsaggregate eg WHERE es.uuid=eg.dataSetId)\n" +
                "                     OR  EXISTS (SELECT 1 FROM edc_reportdsaddcoloumns  ec WHERE es.uuid=ec.dataSetId) \n");
        while (e9Rs.next()) {
            sqlIds += e9Rs.getString("uuid") + ",";
        }
        if (StringUtils.isNotBlank(sqlIds) && sqlIds.endsWith(",")) {
            sqlIds = sqlIds.substring(0, sqlIds.length() - 1);
        }*/


        RecordSet rs = new RecordSet();
        rs.executeQuery("select *  from  edc_reportdataset");
        String datasetIds = "";

        String dirConnDatasetIds = "";

        while (rs.next()) {
            String datasetid = rs.getString("uuid");
            //if ("-100".equals(getDataSetType(datasetid, sqlIds))) {
				if(true){

                datasetIds += "'" + datasetid + "'" + ",";
            } else {
                dirConnDatasetIds += "'" + datasetid + "'" + ",";
            }


        }


        if (StringUtils.isNotBlank(datasetIds)) {
			if(datasetIds.endsWith(",")) {
                    datasetIds = datasetIds.substring(0, datasetIds.length() - 1);
               }

            

            getEdcReport(writeEntity, "数据集和报表 (因数据集和报表有回收站功能, 所以将已删除数据也拉出来)", "数据集和报表", datasetIds);  //sheet页

            getEdcBoard(writeEntity, "数据集和看板", "数据集和看板", datasetIds); //sheet页


        }

        if (StringUtils.isNotBlank(dirConnDatasetIds)) {
            dirConnDatasetIds = dirConnDatasetIds.substring(0, dirConnDatasetIds.length() - 1);
        }


        getEdcFormApp(writeEntity, "", "自由填报");

        getExcelReport(writeEntity, "", "多级填报");

        //getEdcDataSet( writeEntity, "注意:  版本升级开发看使用数量,  项目人员无需关注此项",  "数据集", datasetIds, dirConnDatasetIds);

        String filePath = excelWrite_html(writeEntity);

        return filePath;

    }


	public static String getOracleSQLIn(List<?> ids, int count, String field) {
        int len = ids.size();
        int size = len % count;
        if (size == 0) {
            size = len / count;
        } else {
            size = (len / count) + 1;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            int fromIndex = i * count;
            int toIndex = Math.min(fromIndex + count, len);
            //System.out.println(ids.subList(fromIndex, toIndex));
            String productId = StringUtils.defaultIfEmpty(StringUtils.join(ids.subList(fromIndex, toIndex), ","), "");
            if (i != 0) {
                builder.append(" or ");
            }
            builder.append(field).append(" in (").append(productId).append(")");
        }

        return  builder.toString();

    }

    //functionName:为excel表格顶部的区域说明,    sheetname: sheet页名称
    void getEdcReport(ExcelWriteEntity writeEntity, String functionName, String sheetname, String datasetIds) {
        List<String> headerNames = reportDataHeaderNames();


        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();

		String dataSetWhere = "";

        List<String> list = Arrays.asList(datasetIds.split(","));

		dataSetWhere = "("+getOracleSQLIn(list, 800, "ds.uuid")+")";

        String sql = "select ds.name, ds.creator, ds.isdeleted,   es.name,es.creator,ds.uuid  from edc_reportsheetds  esds inner join  edc_reportdataset ds  on esds.dataSetId=ds.uuid   left join edc_reportsheet es on  esds.sheetId = es.uuid\n" +
                "where " + dataSetWhere + " and  es.quickType <> 'GroupCross'  order by es.isdeleted, es.uuid";

        rs.executeQuery(sql);

        while (rs.next()) {
            List<String> value = new ArrayList<String>();


            String sheetCreaterName = "";
            String dataSetCreaterName = "";

            try {
                ResourceComInfo rci = new ResourceComInfo();

                dataSetCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(2)), "7");  //多语言
                sheetCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(5)), "7");


            } catch (Exception e) {
                new weaver.general.BaseBean().writeLog("", e);
            }


            value.add(rs.getString(6)); //数据集id
            value.add(rs.getString(1)); //数据集名称
            value.add(dataSetCreaterName); //数据集创建人
            value.add("调整sql");
            value.add("1".equals(rs.getString(3)) ? "是" : "否"); //数据集已删除
            value.add(rs.getString(4));  //报表名称
            value.add(sheetCreaterName); //报表创建人
            value.add("1".equals(rs.getString(6)) ? "是" : "否"); //报表已删除
            value.add("迁移后需要手动调整");


            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


    List<String> reportDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("数据集id");
        headerNames.add("数据集名称");
        headerNames.add("数据集创建人");
        headerNames.add("调整内容");
        headerNames.add("数据集已删除");
        headerNames.add("报表名称");
        headerNames.add("报表创建人");
        headerNames.add("报表已删除");
        headerNames.add("调整内容");

        return headerNames;
    }


    void getEdcBoard(ExcelWriteEntity writeEntity, String functionName, String sheetname, String datasetIds) {
        List<String> headerNames = boardDataHeaderNames();


        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();


		String dataSetWhere = "";
        List<String> list = Arrays.asList(datasetIds.split(","));
		dataSetWhere = "("+getOracleSQLIn(list, 800, "edc_reportdataset.uuid")+")";

        rs.executeQuery("select  edc_reportdataset.name, edc_reportdataset.creator, edc_board_dashboard.name, edc_board_dashboard.creator,  edc_board_widget.name, edc_reportdataset.uuid   from edc_board_widget, edc_board_dashboard,edc_reportdataset  where edc_board_widget.board = edc_board_dashboard.id  \n" +
                "    and edc_reportdataset.uuid = edc_board_widget.datamodel  and  "+dataSetWhere+"  order by  edc_board_dashboard.id");

        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String dataSetCreaterName = "";
            String boardCreaterName = "";

            try {
                ResourceComInfo rci = new ResourceComInfo();

                dataSetCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(2)), "7");  //多语言
                boardCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(4)), "7");


            } catch (Exception e) {
                new weaver.general.BaseBean().writeLog("", e);
            }


            value.add(rs.getString(6)); //数据集id
            value.add(rs.getString(1)); //数据集名称
            value.add(dataSetCreaterName);  //数据集创建人
            value.add("调整sql");
            value.add(rs.getString(3)); //看板名称
            value.add(boardCreaterName); //看板创建人
            value.add(rs.getString(5)); //图表名称
            value.add("因数据集sql要调整, 看板重点测试功能, 若字段设置出问题,手动调整 (不是一定要调)");

            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


    List<String> boardDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("数据集id");
        headerNames.add("数据集名称");
        headerNames.add("数据集创建人");
        headerNames.add("调整内容");
        headerNames.add("看板名称");
        headerNames.add("看板创建人");
        headerNames.add("图表名称");
        headerNames.add("调整内容");
        return headerNames;
    }


    void getEdcFormApp(ExcelWriteEntity writeEntity, String functionName, String sheetname) {

        try {


            List<String> headerNames = edcAppDataHeaderNames();


            List<List<String>> values = new ArrayList<List<String>>();
            RecordSet rs = new RecordSet();


            Map<String, String> edcCubeMap = new HashMap<String, String>();
            RecordSet rs1 = new RecordSet();
            rs1.executeQuery("select  formId,formmodeId,nodeid from  edc_joinCubeSetting  where isUsed = '1' and formmodeId is not null and formmodeId <> ''");
            while (rs1.next()) {
                String formmodeId = rs1.getString("formmodeId");
                String formId = rs1.getString("formId");
                String nodeId = rs1.getString("nodeId");
                if (StringUtils.isBlank(nodeId)) {
                    edcCubeMap.put(formId, formmodeId);
                }


            }


            rs.executeQuery("select edc_app.id , edc_app.name , edc_app.creator , edc_app.createdate, edc_form_page.codeblock,edc_app.cubeAppId,edc_form_page.formid   from edc_app  left join  edc_form_page  on  edc_app.id = edc_form_page.appid    where edc_app.displaytype = 'FORM' and \n" +
                    "    (edc_app.isdeleted <> 1 or edc_app.isdeleted is null or edc_app.isdeleted='')");

            while (rs.next()) {

                String codeblock = rs.getString("codeblock");

                String jsContent = "";
                String cssContent = "";


                try {

                    boolean isJSONObject = JSONObject.isValidObject(codeblock);
                    if (isJSONObject) {
                        JSONObject codeJson = JSONObject.parseObject(codeblock);
                        jsContent = codeJson.getString("JS");
                        cssContent = codeJson.getString("CSS");

                    } else {
                        jsContent = codeblock;
                        cssContent = codeblock;
                    }

                } catch (Exception e) {
                    jsContent = codeblock;
                    cssContent = codeblock;
                }


                String formid = rs.getString("formid");

                String cubeAppId = edcCubeMap.get(formid);


                if ((jsContent != null && !"".equals(jsContent)) || (cssContent != null && !"".equals(cssContent)) || (cubeAppId != null && !"".equals(cubeAppId))) {

                    List<String> value = new ArrayList<String>();

                    String edcAppCreaterName = "";


                    try {
                        ResourceComInfo rci = new ResourceComInfo();

                        edcAppCreaterName = Util.formatMultiLang(rci.getLastname(rs.getString(3)), "7");  //多语言


                    } catch (Exception e) {
                        new weaver.general.BaseBean().writeLog("", e);
                    }


                    value.add(rs.getString(1)); //应用ID
                    value.add(rs.getString(2));  //应用名称
                    value.add(edcAppCreaterName); //创建人
                    value.add(rs.getString(4)); //创建日期
                    value.add(jsContent); //JS代码块
                    value.add(cssContent); //CSS代码块
                    value.add(cubeAppId == null ? "" : cubeAppId); //关联建模应用ID
                    value.add("迁移后需要重新调整"); //调整内容

                    values.add(value);


                }
            }

            addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);

        }
    }


    List<String> edcAppDataHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用ID");
        headerNames.add("应用名称");
        headerNames.add("创建人");
        headerNames.add("创建日期");
        headerNames.add("JS代码块");
        headerNames.add("CSS代码块");
        headerNames.add("关联建模应用ID");
        headerNames.add("调整内容");
        return headerNames;
    }


    void getExcelReport(ExcelWriteEntity writeEntity, String functionName, String sheetname) {
        List<String> headerNames = edcExcelReportHeaderNames();


        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from edc_app  where  displaytype = 'EXCEL'");

        while (rs.next()) {

            String edc_app_id = rs.getString("id");

            String name = rs.getString("name");

            String isdeleted = "1".equals(rs.getString("isdeleted")) ? "是 (无回收站,咨询是否使用)" : "否";
            String cubeAppId = rs.getString("cubeAppId");


            List<String> value = new ArrayList<String>();

            value.add(edc_app_id); //应用ID
            value.add(name);  //应用名称
            value.add(isdeleted); //是否删除
            value.add(cubeAppId); //关联建模应用ID
            value.add("E10标准不支持功能很多,请尽快跟版本升级部确认");
            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);

    }


    List<String> edcExcelReportHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("应用ID");
        headerNames.add("应用名称");
        headerNames.add("是否删除");
        headerNames.add("关联建模应用ID");
        headerNames.add("调整内容");
        return headerNames;
    }


    void getEdcDataSet(ExcelWriteEntity writeEntity, String functionName, String sheetname, String datasetIds, String dirConnDatasetIds) {
        List<String> headerNames = edcDataSetHeaderNames();


        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();

        if (StringUtils.isBlank(datasetIds)) {
            datasetIds = "''";
        }

        if (StringUtils.isBlank(dirConnDatasetIds)) {
            dirConnDatasetIds = "''";
        }


        rs.executeQuery("select *   from edc_reportdataset   where uuid in (" + datasetIds + ") or uuid in (" + dirConnDatasetIds + ")");

        while (rs.next()) {

            String dataSetId = rs.getString("uuid");

            String name = rs.getString("name");

            String dataSetType = "";
            if (datasetIds.contains("'" + dataSetId + "'")) {
                dataSetType = "sql";
            } else if (dirConnDatasetIds.contains("'" + dataSetId + "'")) {
                dataSetType = "直连";
            }

            String isdeleted = "1".equals(rs.getString("isdeleted")) ? "是" : "否";


            List<String> value = new ArrayList<String>();


            value.add(dataSetId); //数据集ID
            value.add(name);  //数据集名称
            value.add(dataSetType); //数据集类型
            value.add(isdeleted); //是否删除

            values.add(value);

        }


        addExcelModal(writeEntity, sheetname, functionName, headerNames, values);


    }


    List<String> edcDataSetHeaderNames() {

        ArrayList<String> headerNames = new ArrayList<String>();
        headerNames.add("数据集ID");
        headerNames.add("数据集名称");
        headerNames.add("数据集类型");
        headerNames.add("是否删除");
        return headerNames;
    }

    /**
     * 统一在这个方法里面封装excel需要的table
     * @param writeEntity
     * @return
     */
    public String excelWrite_html(ExcelWriteEntity writeEntity) {
        ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
        String filePath = excelWrite.excelWrite(writeEntity);

        return filePath;
    }
    public void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }

}
