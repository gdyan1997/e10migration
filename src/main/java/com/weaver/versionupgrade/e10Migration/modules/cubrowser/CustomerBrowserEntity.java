package com.weaver.versionupgrade.e10Migration.modules.cubrowser;

import java.util.Objects;

/**
 * @program: flink
 * @description: 自定义浏览按钮实体
 * @author: yhq
 * @create: 2023-07-27
 **/

public class CustomerBrowserEntity {
    //列表渲染的key 前端用
    private String key;
    private String uuid;
    //主键
    private Integer id;
    //是否已自动处理
    private Boolean hasDone;

    /**
     * 0：系统内置浏览按钮
     * 1：自定义浏览按钮
     * 2：自定义树形浏览按钮
     */
    private Integer szlx;
    /**
     * 0：ID类型
     * 1：NAME类型
     * 2：MIX类型
     * 3：QYS类型
     */
    private Integer anzlx;

    //浏览按钮唯一标识
    private String llanbs;
    //e9所属表
    private String e9zszb;

    //e10所属表
    private String e10zszb;
    //描述
    private String description;
    //查询sql
    private String fromsql;
    private String javaCode;
    private String javaClassName;



    public  String getAnzlxName() {
        if (anzlx==null) {
            return "";
        }
        switch (anzlx) {
            case 0:
                return "ID转换类型";
            case 1:
                return "原值转换类型";
            case 2:
                return "混合转换类型";
            case 3:
                return "契约锁浏览按钮值转换类型";
            default:
                return "";
        }
    }

    public String getJavaCode() {
        return javaCode;
    }

    public void setJavaCode(String javaCode) {
        this.javaCode = javaCode;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    @Override
    public String toString() {
        return "CustomerBrowserEntity{" +
                "key='" + key + '\'' +
                ", uuid='" + uuid + '\'' +
                ", id=" + id +
                ", hasDone=" + hasDone +
                ", szlx=" + szlx +
                ", anzlx=" + anzlx +
                ", llanbs='" + llanbs + '\'' +
                ", e9zszb='" + e9zszb + '\'' +
                ", e10zszb='" + e10zszb + '\'' +
                ", description='" + description + '\'' +
                ", fromsql='" + fromsql + '\'' +
                ", javaCode='" + javaCode + '\'' +
                ", javaClassName='" + javaClassName + '\'' +
                '}';
    }

    public Boolean getHasDone() {
        return hasDone;
    }

    public void setHasDone(Boolean hasDone) {
        this.hasDone = hasDone;
    }

    public String getFromsql() {
        return fromsql;
    }

    public void setFromsql(String fromsql) {
        this.fromsql = fromsql;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSzlx() {
        return szlx;
    }

    public void setSzlx(Integer szlx) {
        this.szlx = szlx;
    }

    public Integer getAnzlx() {
        return anzlx;
    }

    public void setAnzlx(Integer anzlx) {
        this.anzlx = anzlx;
    }

    public String getLlanbs() {
        return llanbs;
    }

    public void setLlanbs(String llanbs) {
        this.llanbs = llanbs;
    }

    public String getE9zszb() {
        return e9zszb;
    }

    public void setE9zszb(String e9zszb) {
        this.e9zszb = e9zszb;
    }

    public String getE10zszb() {
        return e10zszb;
    }

    public void setE10zszb(String e10zszb) {
        this.e10zszb = e10zszb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerBrowserEntity browser = (CustomerBrowserEntity) o;
        return Objects.equals(key, browser.key) && Objects.equals(id, browser.id)
                && Objects.equals(szlx, browser.szlx) && Objects.equals(
                anzlx,
                browser.anzlx) && Objects.equals(llanbs, browser.llanbs)
                && Objects.equals(e9zszb, browser.e9zszb) && Objects.equals(
                e10zszb,
                browser.e10zszb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, id, szlx, anzlx, llanbs, e9zszb, e10zszb);
    }

}
