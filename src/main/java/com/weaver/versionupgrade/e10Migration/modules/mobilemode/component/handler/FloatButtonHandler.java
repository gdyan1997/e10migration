package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.MobileModeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import org.apache.commons.lang3.StringUtils;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/5/9 15:50
 */
public class FloatButtonHandler extends AbstractComponentHandler {
    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {
        List<ComponentBean> beans = new ArrayList<>();
        String mecparam = componentConvertContext.getMecparam();
        JSONObject mecparamjson = JSON.parseObject(mecparam);

        String clickType = Util.null2String(mecparamjson.get("clickType"));

        if (clickType.equals("2")){



            beans.addAll(MobileModeUtil.getRemindConfig(mecparamjson,"浮动按钮"));



            String clickScript = Util.null2String(mecparamjson.get("clickScript"));
            if (StringUtils.isNotBlank(clickScript)) {
                beans.add(
                        new ComponentBean().setHandType(HandType.jsDev).setResult(
                                "浮动按钮需要调整脚本 " + clickScript));
            }


        }



        this.setLists(beans);
    }

}
