package com.weaver.versionupgrade.e10Migration.modules.mobilemode.util;


import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ygd2020
 * @date 2022-12-26 16:35
 * Description:
 **/
public class VFormUtil {

    public final static String maintableAlias = "t1";


    public static String getRealFromName(String virtualFormName) {
        if (virtualFormName.length() > 33 && virtualFormName.charAt(32) == '_') {
            virtualFormName = virtualFormName.substring(33);
        }
        return virtualFormName;
    }

    public static String getVdatasourceBysearchId(String searchId) {

        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select formid,detailtable,defaultsql from \n" +
                "mode_customsearch where id = ? ", searchId);

        String e9_formid = "0";
        if (rs.next()) {//能查到说明是虚拟表单
            e9_formid = rs.getString("formid");
        }

        rs.executeQuery("select * from \n" +
                "modeformextend where FORMID = ? ", e9_formid);
        String vdatasource = "";
        if (rs.next()) {//能查到说明是虚拟表单
            vdatasource = rs.getString("vdatasource");//数据源
        }


        return vdatasource;
    }


    public static String getCustomSearchSql(String searchId) {


        String vformtype = "1";//1 表单视图 2 sql视图
        String vsql = "";
        String vdatasource = "";


        SourceDBUtil rs = new SourceDBUtil();

        String e9_formid = "";
        String detailtable = "";
        String sqlwhere = "";


        rs.executeQuery("select formid,detailtable,defaultsql from \n" +
                "mode_customsearch where id = ? ", searchId);

        if (rs.next()) {//能查到说明是虚拟表单
            e9_formid = rs.getString("formid");
            detailtable = rs.getString("detailtable");
            sqlwhere = rs.getString("defaultsql");
        }

        rs.executeQuery("select * from \n" +
                "modeformextend where FORMID = ? ", e9_formid);

        if (rs.next()) {//能查到说明是虚拟表单
            vsql = rs.getString("vsql");//sql 视图sql
            vdatasource = rs.getString("vdatasource");//数据源
            vformtype = rs.getString("vformtype");//1 表单视图 2 sql视图
        }

        if (vformtype.equals("")) {
            vformtype = "1";
        }


        String fromSql = VFormUtil.getFromSql(vformtype, vsql, e9_formid, detailtable);


        rs.executeQuery("select id,\n" +
                "       fieldname,\n" +
                "       fieldlabel,\n" +
                "       fieldhtmltype,\n" +
                "       type,\n" +
                "       fielddbtype,\n" +
                "       detailtable,viewtype \n" +
                "from workflow_billfield\n" +
                "where billid = ?", e9_formid);


        List<String> fieldnames = new ArrayList<>();


        String detailtable_orderid = "1";


        boolean fieldhasid = false;


        while (rs.next()) {
            String fieldname = rs.getString("FIELDNAME");
            String viewtype = rs.getString("viewtype");
            String _detailtable = rs.getString("detailtable");
            String orderid = "0";

            if (viewtype.equals("1")) {//明细表字段重新获取下orderid
                orderid = E10CubeUtil.getOrderIdByDtTable(e9_formid, _detailtable);
            }

            if (detailtable.equalsIgnoreCase(_detailtable)) {
                detailtable_orderid = orderid;
            }

            if (fieldname.equalsIgnoreCase("id")) {
                fieldhasid = true;
            }

            String sourcefield = fieldname;
            String targetfield = fieldname;


            if (viewtype.equals("1")) {// 明细字段
                if (fieldname.equalsIgnoreCase("id")) {
                    targetfield = "id";
                } else {
                    sourcefield = "dt" + orderid + "." + fieldname;
                    targetfield = "dt" + orderid + "_" + fieldname;
                }
            }


            fieldnames.add(sourcefield + " as " + targetfield);

        }

        if (!fieldhasid) {//字段表中没有id字段 需要默认补一个字段id
            Map<String, Object> fieldAttribute = new HashMap<>();
            fieldAttribute.put("type", "TEXT");

            if (StringUtils.isNotBlank(detailtable)) {// 明细字段
                fieldnames.add(0, "dt" + detailtable_orderid + ".id as id ");
            } else {
                fieldnames.add(0, VFormUtil.maintableAlias+".id as id ");// 主表id字段
            }


        }

        String sql = " select  " + String.join(",", fieldnames) + "  " + fromSql;

        if (StringUtils.isNotBlank(sqlwhere)) {

            sqlwhere = removeFirstAnd(sqlwhere);
            sql += " where " + sqlwhere;
        }

        return sql;
    }



    public static String removeFirstAnd(String sqlwhere) {
        sqlwhere = sqlwhere.trim();
        if (sqlwhere.length() > 3) {//这里我要截取前三个 看下是不是and开头的
            String andstr = sqlwhere.substring(0, 3);

            if (andstr.equalsIgnoreCase("and")) {
                sqlwhere = sqlwhere.substring(3);
            }
        }
        return sqlwhere;
    }

    public static String getFromSql(String vformtype, String vsql, String e9_formid, String detailtable) {

        StringBuilder formsql = new StringBuilder(" from ");

        if (!vformtype.equals("1")) {//不是1 说明是虚拟视图sql
            return formsql.append(" (").append(vsql).append(")").append(" "+VFormUtil.maintableAlias).toString();
        }

        SourceDBUtil rs = new SourceDBUtil();

        rs.executeQuery("select tablename,detailkeyfield  from workflow_bill where id = ?", e9_formid);


        String detailkeyfield = "detailkeyfield";
        if (rs.next()) {
            String maintablename = rs.getString("tablename");
            detailkeyfield = rs.getString("detailkeyfield");
            formsql.append(getTableSelect(maintablename, VFormUtil.maintableAlias));
        }


        String dtsql = "select tablename,orderid\n" +
                "from workflow_billdetailtable\n" +
                "where billid = ? ";

        String joinstr = " left join ";// 默认主表查询时肯定是左连接 主要是获取主表数据

        if (StringUtils.isNotBlank(detailtable)) {// 存在明细表时 就要切换成全连接 inner join 主要获取明细表数据
            joinstr = " inner join ";
            dtsql += " and tablename = '" + detailtable + "'";
        }


        rs.executeQuery(dtsql, e9_formid);

        while (rs.next()) {

            String dttable = rs.getString("TABLENAME");
            String orderid = rs.getString("ORDERID");

            formsql.append(joinstr)
                    .append(getTableSelect(dttable, " dt" + orderid))
                    .append(" on "+VFormUtil.maintableAlias+".id=")
                    .append(" dt")
                    .append(orderid)
                    .append(".")
                    .append(detailkeyfield);
        }

        return formsql.toString();

    }
    public static String getTableSelect(String tablename, String alias) {


        tablename = getRealFromName(tablename);
//        if (StringUtils.isNotBlank(Constant.getViewTableMap().get(tablename.toLowerCase()))) {// 存在内容再进行替换
//
//            tablename = "(" + Constant.getViewTableMap().get(tablename.toLowerCase()) + ")";
//        }

        return " " + tablename + " " + alias + " ";

    }

}
