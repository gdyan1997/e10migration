package com.weaver.versionupgrade.e10Migration.modules.all;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.weaver.version.upgrade.util.sql.SQLUtil;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.Constant;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import org.apache.commons.lang3.StringUtils;
import org.checkerframework.framework.qual.FromByteCode;
import weaver.conn.RecordSet;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.BaseBean;
import weaver.general.Util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.weaver.version.upgrade.util.sql.SQLUtil.dmlTotalReplacement;
import static com.weaver.version.upgrade.util.sql.cache.SqlCache.custom_view_useCount;
import static com.weaver.versionupgrade.e10Migration.modules.SB2ExcleWrite.excelWrite_html;
import static com.weaver.versionupgrade.e10Migration.modules.constant.E9_STANDARD_TABLE;
import static com.weaver.versionupgrade.e10Migration.modules.constant.E9_STANDARD_TABLE_FILTER;
import static weaver.e10Migration.excel.file.ExcelWrite.addExcelModal;

/**
 * @author wujiahao
 * @date 2024/5/11 14:26
 */
public class ExportAllDataExcel {

    public static int actionCount=0;


    //总数据量
    List<String> tableField1 = new ArrayList<String>(Arrays.asList( "系统版本", "数据库类型","总表数量","数据总量","流程路径量" ,"建模模块量" ,"接口代码量","清单生成时间" ));
    //top50表info
    List<String> tableField2 = new ArrayList<String>(Arrays.asList("表名", "数据总量", "是否为标准表"));
    //非标准表info
    List<String> tableField3 = new ArrayList<String>(Arrays.asList("表名", "数据总量"));


    List<String> tableField9 = new ArrayList<String>(Arrays.asList("表名", "数据总量", "是否为标准表"));


    //替换信息抓取
    List<String> tableField4 = new ArrayList<String>(Arrays.asList("查询 sql总量", "完全替换", "部分sql片段未替换", "不是正确sql", "sql中存在表或字段不是oa标准表字段，请手动调整", "成功替换占比"));

    List<String> dmltableField4 = new ArrayList<String>(Arrays.asList("DML sql总量", "均找到E10对应字段", "部分sql片段未替换", "不是正确sql", "sql中存在表或字段不是oa标准表字段，请手动调整", "成功替换占比"));


    List<String> tableField5 = new ArrayList<String>(Arrays.asList("表名称", "sql中使用次数"));

    List<String> tableField6 = new ArrayList<String>(Arrays.asList("表名称^列名称", "sql中使用次数"));

    List<String> errorsqlheader = new ArrayList<String>(Arrays.asList("报错sql", "sql参数", "报错原因"));
    List<String> tableheader = new ArrayList<String>(Arrays.asList("表单id", "表单名称", "表名", "数据量"));

    List<String> modetableheader = new ArrayList<String>(Arrays.asList("表单id", "表单名称", "表名", "关联业务模块", "数据量", "确认说明"));


    String DBTYPE = new RecordSet().getDBType();

    String LIMITCOUNT = "50";

    /**
     * 功能维度
     */
    public String doDataAll(String path) {
        try {
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("数据量统计", path);
            getAllDataCount(writeEntity, "数据量分析");
            String filePath = excelWrite_html(writeEntity);

            return filePath;
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }
        return "";
    }


    void getAllDataCount(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        try {
            List<List<String>> values1 = new ArrayList<List<String>>();
            List<List<String>> values2 = new ArrayList<List<String>>();
            List<List<String>> values3 = new ArrayList<List<String>>();
            List<List<String>> values4 = new ArrayList<List<String>>();

            List<List<String>> values9 = new ArrayList<List<String>>();
            RecordSet rs1 = new RecordSet();
            RecordSet rs2 = new RecordSet();
            RecordSet rs3 = new RecordSet();
            String countSql1 = "";
            String countSql2 = "";
            String countSql3 = "";

//            //数据总量
            List<String> value1 = new ArrayList<String>();
            if ("mysql".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "SELECT COUNT(table_name) AS tableCount1,SUM(TABLE_ROWS) AS tableDataCount1\n" +
                        "FROM information_schema.TABLES\n" +
                        "WHERE upper(TABLE_SCHEMA) = upper((SELECT database()))\n" +
                        "  and upper(TABLE_TYPE) in ('BASE TABLE')";
            } else if ("sqlserver".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "SELECT\n" +
                        "    COUNT(a.name) AS tableCount1,\n" +
                        "    SUM(CONVERT(BIGINT, b.rows)) AS tableDataCount1\n" +
                        "FROM\n" +
                        "    sysobjects AS a\n" +
                        "INNER JOIN\n" +
                        "    sysindexes AS b ON a.id = b.id\n" +
                        "WHERE\n" +
                        "    a.type = 'u'\n" +
                        "    AND b.indid IN (0, 1)";
            } else if ("oracle".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "select count(u.TABLE_NAME) as tableCount1, sum(u.NUM_ROWS) as tableDataCount1\n" +
                        "from user_tables u\n" +
                        "WHERE u.NUM_ROWS IS not NULL";
            } else if ("postgresql".equalsIgnoreCase(DBTYPE)) {
                countSql1 = "SELECT count(relname)  AS tableCount1,\n" +
                        "       sum(n_live_tup) AS tableDataCount1\n" +
                        "FROM pg_stat_user_tables\n" +
                        "where schemaname = (SELECT current_schema())";

                SourceDBUtil sourceDBUtil = new SourceDBUtil();
                sourceDBUtil.execute("VACUUM ANALYZE  ");
            } else {


            }


            SourceDBUtil miDBExecutor = new SourceDBUtil();
            SourceDBUtil miDBExecutor_tmp = new SourceDBUtil();

            System.out.println("sql=" + countSql1);

            System.out.println("miDBExecutor.getRealDbtype() wjh" + miDBExecutor.getRealDbtype());
            //达梦特殊处理
            if ("dm".equalsIgnoreCase(miDBExecutor.getRealDbtype())
                    || "jc".equalsIgnoreCase(miDBExecutor.getRealDbtype())) {


                //创建一个测试表
                miDBExecutor.execute("DROP TABLE IF EXISTS table_dm_wjh_E10");
                miDBExecutor.execute("create table table_dm_wjh_E10 (tab_name varchar(100),row_num int)");

                miDBExecutor.execute("delete from  table_dm_wjh_E10 where 1=1");
                miDBExecutor.executeQuery("select table_name from dba_tables where (owner='ECOLOGY' or owner='ecology')  and TABLESPACE_NAME<>'TEMP'");
                while (miDBExecutor.next()) {
                    String tableName = miDBExecutor.getString("table_name");
                    miDBExecutor_tmp.execute("insert into table_dm_wjh_E10(tab_name,row_num) select  '" + tableName + "',count(*) from " + tableName);
                    System.out.println("insert into table_dm_wjh_E10(tab_name,row_num) select  '" + tableName + "',count(*) from " + tableName);
                }
                countSql1 = "select count(*),sum(row_num) from table_dm_wjh_E10 ";
            }


            System.out.println("sql=" + countSql1);
            rs1.executeQuery(countSql1);
            String tableCount1 = "0";
            String tableDataCount1 = "0";
            if (rs1.next()) {
                tableCount1 = rs1.getString(1);
                tableDataCount1 = rs1.getString(2);
            }



            RecordSet recordSet = new RecordSet();

            recordSet.executeQuery("select cversion  from license");
            if (recordSet.next()) {
                value1.add(recordSet.getString(1));
            }else {
                value1.add("9");
            }


            value1.add(recordSet.getRealDbtype());

            value1.add(tableCount1);
            value1.add(tableDataCount1);


            //流程路径量

            {

                recordSet.executeQuery("select count(*)\n" +
                        "from workflow_base");
                if (recordSet.next()) {
                    value1.add(recordSet.getString(1));
                }

            }

            //建模模块量
            {

                recordSet.executeQuery("select count(*)  from modeinfo WHERE  isdelete is null or  isdelete <> '1'");
                if (recordSet.next()) {
                    value1.add(recordSet.getString(1));
                }else {
                    value1.add("0");
                }

            }
            //接口代码量

            {
                value1.add(actionCount+"");
            }

            ExportAllDataExcel.actionCount=0;



                // 获取当前的日期和时间
                LocalDateTime now = LocalDateTime.now();

                // 创建一个日期时间格式化器，用于格式化日期和时间
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                // 格式化并打印当前的日期和时间
                String formattedDateTime = now.format(formatter);
                value1.add(formattedDateTime);





            values1.add(value1);


            //top50表数据量分析 不含表单
            if ("mysql".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT TABLE_NAME,TABLE_ROWS  \n" +
                        "FROM information_schema.TABLES a\n" +
                        "WHERE upper(TABLE_SCHEMA) = upper((SELECT database()))\n" +
                        "  and upper(TABLE_TYPE) in ('BASE TABLE')\n" +
                        "  and not EXISTS(select 1 from workflow_bill where a.TABLE_NAME = workflow_bill.tablename or a.TABLE_NAME = 'workflow_form')\n" +
                        "  and not EXISTS(select 1 from Workflow_billdetailtable where a.TABLE_NAME = Workflow_billdetailtable.tablename) " +
                        " order by TABLE_ROWS desc\n" +
                        "limit \n" + LIMITCOUNT;
            } else if ("sqlserver".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT TOP " + LIMITCOUNT + " a.name, b.rows \n" +
                        "FROM sysobjects AS a\n" +
                        "         INNER JOIN sysindexes AS b ON a.id = b.id\n" +
                        "WHERE (a.type = 'u')\n" +
                        "  AND (b.indid IN (0, 1))\n" +
                        "  and not EXISTS (select 1 from workflow_bill  where a.name = workflow_bill.tablename  or a.name = 'workflow_form')\n" +
                        "  and not EXISTS(select 1 from Workflow_billdetailtable where a.name = Workflow_billdetailtable.tablename) " +
                        " order by b.rows desc\n";
            } else if ("oracle".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT TABLE_NAME , NUM_ROWS \n" +
                        "FROM (SELECT TABLE_NAME, NUM_ROWS\n" +
                        "      FROM user_tables a\n" +
                        "      WHERE NUM_ROWS IS NOT NULL\n" +
                        "      and not EXISTS(select 1 from workflow_bill where a.TABLE_NAME = workflow_bill.tablename  or a.TABLE_NAME = 'workflow_form')\n" +
                        "      and not EXISTS(select 1 from Workflow_billdetailtable where a.TABLE_NAME = Workflow_billdetailtable.tablename) " +
                        "      ORDER BY NUM_ROWS DESC" +
                        ")\n" +
                        "WHERE ROWNUM <= " + LIMITCOUNT;
            } else if ("postgresql".equalsIgnoreCase(DBTYPE)) {
                countSql2 = "SELECT \n" +
                        "       a.relname    AS TABLE_NAME,\n" +
                        "       a.n_live_tup AS NUM_ROWS\n" +
                        "FROM pg_stat_user_tables a\n" +
                        "where a.schemaname = (SELECT current_schema())\n" +
                        "  and not exists(select 1 from workflow_bill where a.relname = workflow_bill.tablename or a.relname = 'workflow_form')\n" +
                        "  and not EXISTS(select 1 from Workflow_billdetailtable where a.relname = Workflow_billdetailtable.tablename)\n" +
                        "ORDER BY n_live_tup DESC\n" +
                        "LIMIT " + LIMITCOUNT;
            } else {
            }


            if ("dm".equalsIgnoreCase(miDBExecutor.getRealDbtype())
                    || "jc".equalsIgnoreCase(miDBExecutor.getRealDbtype())) {
                countSql2 = "SELECT tab_name, row_num\n" +
                        "FROM table_dm_wjh_E10 a\n" +
                        "WHERE row_num IS NOT NULL\n" +
                        "  and not EXISTS(select 1\n" +
                        "                 from workflow_bill\n" +
                        "                 where a.tab_name = workflow_bill.tablename\n" +
                        "                    or a.tab_name = 'workflow_form')\n" +
                        "  and not EXISTS(select 1 from Workflow_billdetailtable where a.tab_name = Workflow_billdetailtable.tablename)\n" +
                        "ORDER BY row_num DESC LIMIT 50";
            }


            System.out.println("sql countSql2=" + countSql2);


            rs2.executeQuery(countSql2);
            while (rs2.next()) {
                List<String> value2 = new ArrayList<String>();
                //是否为标准表
                Boolean isStandradTable = false;
                String tableName2 = rs2.getString(1);
                String tableDataCount2 = rs2.getString(2);
                if (containsIgnoreCase(E9_STANDARD_TABLE, tableName2)) {
                    isStandradTable = true;
                }
                //标准表top统计需要跳过无意义的子表 , 表单表和表单明细表已通过sql过滤
                if (containsIgnoreCase(E9_STANDARD_TABLE_FILTER, tableName2)) {
                    continue;
                }
                value2.add(tableName2.toLowerCase());
                value2.add(tableDataCount2);
                value2.add(isStandradTable ? "是" : "否");
                values2.add(value2);
            }


            //非标准表统计
            if ("mysql".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT  TABLE_NAME,TABLE_ROWS\n" +
                        "FROM information_schema.TABLES a\n" +
                        "WHERE upper(TABLE_SCHEMA) = upper((SELECT database()))\n" +
                        "  and upper(TABLE_TYPE) in ( 'BASE TABLE')\n" +
                        "order by TABLE_ROWS desc";
            } else if ("sqlserver".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT  a.name, b.rows \n" +
                        "FROM sysobjects AS a\n" +
                        "         INNER JOIN sysindexes AS b ON a.id = b.id\n" +
                        "WHERE (a.type = 'u')\n" +
                        "  AND (b.indid IN (0, 1))\n" +
                        "order by b.rows desc";
            } else if ("oracle".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT TABLE_NAME, NUM_ROWS\n" +
                        "      FROM user_tables a\n" +
                        "      WHERE NUM_ROWS IS NOT NULL\n" +
                        "      ORDER BY NUM_ROWS DESC\n";
            } else if ("postgresql".equalsIgnoreCase(DBTYPE)) {
                countSql3 = "SELECT relname  AS TABLE_NAME,\n" +
                        "       n_live_tup AS NUM_ROWS\n" +
                        "FROM pg_stat_user_tables\n" +
                        "where schemaname = (SELECT current_schema()) " +
                        "ORDER BY n_live_tup DESC";
            } else {
            }


            if (
                    "dm".equalsIgnoreCase(miDBExecutor.getRealDbtype()) ||
                            "jc".equalsIgnoreCase(miDBExecutor.getRealDbtype())
            ) {
                countSql3 = "SELECT tab_name, row_num\n" +
                        "FROM table_dm_wjh_E10   order by row_num desc\n";
            }


            System.out.println("sql countSql3=" + countSql3);

            Long ignoreCount=0l;

            final String ignoreTable = new BaseBean().getPropValue("sqlreplace", "ignoreTable");
            List<String> collect = Arrays.stream(ignoreTable.split(",")).map(x -> x.toLowerCase()).collect(Collectors.toList());



            rs3.executeQuery(countSql3);
            while (rs3.next()) {

                List<String> value3 = new ArrayList<String>();
                String tableName3 = rs3.getString(1);
                String tableDataCount3 = rs3.getString(2);
                if (tableDataCount3.equals("")) {
                    System.out.println("tableName3 0=" + tableName3);
                    tableDataCount3 = "0";
                }

                List<String> value9 = new ArrayList<String>();

                boolean isStandradTable = false;
                if (containsIgnoreCase(E9_STANDARD_TABLE, tableName3)) {
                    isStandradTable = true;
                }
                value9.add(tableName3.toLowerCase());

                if (collect.contains(tableName3.toLowerCase())){
                    ignoreCount+=Long.parseLong(tableDataCount3);
                }

                value9.add(tableDataCount3);
                value9.add(isStandradTable ? "是" : "否");
                values9.add(value9);


                if (containsIgnoreCase(E9_STANDARD_TABLE, tableName3)) {
                    continue;
                }
                value3.add(tableName3.toLowerCase());
                value3.add(tableDataCount3);
                values3.add(value3);
            }
            new BaseBean().writeLog("ignoreCount="+ignoreCount);

            Long s1=0l;

            try {
                s1= Long.valueOf(values1.get(0).get(3));
            }catch (Exception e){
                e.printStackTrace();
            }


            new BaseBean().writeLog("count="+s1);
            new BaseBean().writeLog("s1-ignoreCount="+String.valueOf(s1-ignoreCount));

            values1.get(0).set(3, String.valueOf(s1-ignoreCount));

            addExcelModal(writeEntity, sheetname, "总数据量", tableField1, values1);
            addExcelModal(writeEntity, sheetname, "前50数据的表(不含表单表)", tableField2, values2);
            addExcelModal(writeEntity, sheetname, "非标准表及数据量", tableField3, values3);


            values9.sort((o1, o2) -> Integer.parseInt(o2.get(1)) - Integer.parseInt(o1.get(1)));

            addExcelModal(writeEntity, "系统中表数据量明细", "表名称及对应数据量统计", tableField9, values9);


            //表单中字段明细


            SourceDBUtil sourceDBUtil = new SourceDBUtil();

            List<Map<String, String>> formFieldCount = new ArrayList<>();
//            //老表但
//            sourceDBUtil.executeQuery(" select formid,isdetail,groupid,count(*) cnt from workflow_formfield group by formid,isdetail,groupid  order by formid");
//
//            while (sourceDBUtil.next()) {
//                Map<String ,String> map=new HashMap<>();
//                //是否明细表
//                map.put("isdetail", Util.null2String(sourceDBUtil.getString("isdetail"),"0"));
//                map.put("cnt", Util.null2String(sourceDBUtil.getString("cnt"),"0"));
//                map.put("formid", Util.null2String(sourceDBUtil.getString("formid"),"0"));
//
//                map.put("formid", Util.null2String(sourceDBUtil.getString("formid"),"0"));
//            }


            List<List<String>> tableColumnList = new ArrayList<>();

            //自定义表单
            sourceDBUtil.executeQuery("select count(*)      cnt,\n" +
                    "       BILLID,\n" +
                    "       detailtable,\n" +
                    "      t.tablename\n" +
                    "from workflow_billfield t1 left join workflow_bill t on  t.id = t1.billid  and t1.viewtype = 0\n" +
                    " where  not exists(select 1 from modeformextend where t.id = modeformextend.FORMID)  \n" +
                    "group by BILLID, detailtable,t.tablename\n" +
                    "order by count(*) desc");


            while (sourceDBUtil.next()) {
                List<String> list = new ArrayList<>();

                String tablename = sourceDBUtil.getString("tablename");
                String detailtable = sourceDBUtil.getString("detailtable");
                String BILLID = sourceDBUtil.getString("BILLID");

                String cnt = sourceDBUtil.getString("cnt");

                //表名称
                list.add(StringUtils.isBlank(tablename) ? detailtable : tablename);

                //BILLID
                list.add(BILLID);
                list.add(StringUtils.isBlank(tablename) ? "明细表" : "主表");
                //cnt
                list.add(cnt);
                tableColumnList.add(list);
            }

            List<String> errorsqlheader_tmp = new ArrayList<String>(Arrays.asList("表名称", "BILLID", "主表或明细", "字段数量"));


            addExcelModal(writeEntity, "表单字段数量抓取", "表单字段数量抓取", errorsqlheader_tmp, tableColumnList);










            /*    totalReplacement - 表示总量的变量
    fullReplacement - 表示完全替换的变量
    partialUnreplacedSQL - 表示部分SQL片段未替换的变量
    incorrectSQL - 表示不是正确SQL的变量
    nonStandardFields - 表示SQL中存在表或字段不是OA标准表字段的变量*/


            List<String> list4 = new ArrayList<>();
            // 总量
            list4.add(String.valueOf(SQLUtil.totalReplacement));
            // 完全替换
            list4.add(String.valueOf(SQLUtil.fullReplacement));
            // 部分sql片段未替换
            list4.add(String.valueOf(SQLUtil.partialUnreplacedSQL));
            // 不是正确sql
            list4.add(String.valueOf(SQLUtil.incorrectSQL));
            // sql中存在表或字段不是oa标准表字段，请手动调整
            list4.add(String.valueOf(SQLUtil.nonStandardFields));

            double successReplacementRatio = SQLUtil.fullReplacement * 100.00 / SQLUtil.totalReplacement;

            // 成功替换占比，保留一位小数
            list4.add(String.format("%.2f%%", successReplacementRatio));


            //总量
            //完全替换
            //部分sql片段未替换
            //不是正确sql
            //sql中存在表或字段不是oa标准表字段，请手动调整
            //成功替换占比

            values4.add(list4);
            addExcelModal(writeEntity, "sql脚本分析", "查询sql替换分析:\n目前sql替换结果，在E10数据库单体部署情况下,可以直接在E10 ecology数据库执行。如E10使用微服务多库，请针对sql中每张表所在的数据库对sql进行调整。\nE10数据库单体部署客户,参考清单调整sql时,如发现提示\"完全替换\"但不能直接使用或sql结果不正确，可联系武佳豪协助调整。", tableField4, values4);


            List<List<String>> valuesdml = new ArrayList<List<String>>() {{
                List<String> list1 = new ArrayList<>();
                list1.add(String.valueOf(SQLUtil.dmlTotalReplacement));
                list1.add(String.valueOf(SQLUtil.dmlFullReplacement));
                list1.add(String.valueOf(SQLUtil.dmlPartialUnreplacedSQL));
                list1.add(String.valueOf(SQLUtil.dmlIncorrectSQL));
                list1.add(String.valueOf(SQLUtil.dmlNonStandardFields));
                list1.add("dml替换仅供参考，具体情况请手动确认");
                this.add(list1);
            }};

            addExcelModal(writeEntity, "sql脚本分析", "DML (增，删，改) sql替换分析", dmltableField4, valuesdml);


            List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(FieldMappingCache.tableCount.entrySet());
            //使用list.sort()排序
            list.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));

            System.out.println("开始打印未替换的表");


            List<List<String>> values5 = new ArrayList<List<String>>();


            //输出结果
            for (Map.Entry<String, Integer> entry : list) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
                List<String> list_tmp = new ArrayList<>();
                list_tmp.add(entry.getKey());
                list_tmp.add(entry.getValue() + "");

                values5.add(list_tmp);
            }
            addExcelModal(writeEntity, "sql脚本分析", "E10未找到对应关系的表", tableField5, values5);


            List<List<String>> values6 = new ArrayList<List<String>>();


            List<Map.Entry<String, Integer>> FieldCount = new ArrayList<>(FieldMappingCache.FieldCount.entrySet());
            //使用list.sort()排序
            FieldCount.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));
            System.out.println("开始打印常用字段未替换字段");


            for (Map.Entry<String, Integer> entry : FieldCount) {
                List<String> list_tmp = new ArrayList<>();
                list_tmp.add(entry.getKey());
                list_tmp.add(entry.getValue() + "");
                System.out.println(entry.getKey() + ": " + entry.getValue());
                values6.add(list_tmp);
            }
            new BaseBean().writeLog("values6=" + values6.size());

            addExcelModal(writeEntity, "sql脚本分析", "E10未找到对应关系的列", tableField6, values6);


            String system_view = new BaseBean().getPropValue("sqlreplace", "system_view");
            Set<String> set = Arrays.stream(system_view.split(",")).map(s -> s.trim().toLowerCase()).collect(Collectors.toSet());

            List<List<String>> valuesview = new ArrayList<List<String>>();


            SourceDBUtil rs_tt = new SourceDBUtil();
            rs_tt.executeQuery(SqlCache.getE9CustomViewSql());
            while (rs_tt.next()) {
                String table_name = rs_tt.getString("TABLE_NAME");
                String TEXT = rs_tt.getString("TEXT");
                if (!set.contains(table_name.toLowerCase())) {
                    List<String> list_tmp = new ArrayList<>();
                    list_tmp.add(table_name);
                    list_tmp.add(TEXT);
                    list_tmp.add(SQLUtil.replaceSql(TEXT));
                    if (custom_view_useCount.containsKey(table_name.toLowerCase())) {
                        list_tmp.add("" + custom_view_useCount.get(table_name.toLowerCase()));
                    } else {
                        list_tmp.add("0");
                    }
                    valuesview.add(list_tmp);
                }
            }
            List<String> tableFieldview = new ArrayList<String>(Arrays.asList("视图名称", "视图定义语句", "替换后视图", "E9引用次数"));

            if (valuesview.size() > 0) {
                //valuesview.sort(Comparator.comparingInt(o -> Integer.valueOf(o.get(3))));
                valuesview.sort(Comparator.comparingInt((List<String> o) -> Integer.valueOf(o.get(3))).reversed());
                addExcelModal(writeEntity, "E9自定义视图分析", "E9自定义视图", tableFieldview, valuesview);
            }


            addExcelModal(writeEntity, "抓取过程中sql报错分析", "抓取过程中存在sql报错请联系升级组确认", errorsqlheader, ConfigUtil.errorsqllist);


            SourceDBUtil rs = new SourceDBUtil();
            SourceDBUtil rs_1 = new SourceDBUtil();
            String sql = "select  id,namelabel,tablename\n" +
                    "                    from   workflow_bill\n" +
                    "                    where not exists(select 1 from modeinfo where modeinfo.FORMID = workflow_bill.ID)\n" +
                    "                      and not exists(select 1 from modeformextend where workflow_bill.ID = modeformextend.FORMID)\n" +
                    "                      and not exists(select 1 from workflow_base where workflow_bill.ID = workflow_base.FORMID)\n" +
                    "                      and id <0\n";


            if (ConfigUtil.isE9()) {
                sql += "   and not exists(select 1 from edc_form_page where workflow_bill.ID = edc_form_page.FORMID)\n";

            }


            HashSet<String> standtables = new HashSet<>();
            standtables.add("'uf_likes'");
            standtables.add("'uf_reply'");


            sql += " and lower(tablename) not in (" + StringUtils.join(standtables, ",") + ")";

            rs.executeQuery(sql);


            List<List<String>> values7 = new ArrayList<List<String>>();

            while (rs.next()) {

                ArrayList<String> arrayList = new ArrayList<>();
                String formid = rs.getString("id");
                String tablename = rs.getString("tablename");

                if (tablename.toLowerCase().startsWith("edc_")) {// edc 的表都会迁
                    continue;
                }

                String name = E10CubeUtil.getLabelName(rs.getString("namelabel"));
                rs_1.executeQuery("select count(*) as count from " + tablename);
                String count = "0";
                if (rs_1.next()) {
                    count = rs_1.getString("count");
                }

                if (count.equals("0")) {// 数据量为0 肯定么关系
                    continue;
                }


                arrayList.add(formid);
                arrayList.add(name);
                arrayList.add(tablename);
                arrayList.add(count);
                values7.add(arrayList);
            }

            addExcelModal(writeEntity, "表单管理中未关联建模/流程/数据中心的实体表单", "表单管理中未关联建模/流程/数据中心的实体表单,请确认是否存在二开业务", tableheader, values7);

//
//


            List<List<String>> values8 = new ArrayList<List<String>>();

            sql = "select id, namelabel, tablename\n" +
                    "from workflow_bill\n" +
                    "where (exists(select 1 from modeinfo where modeinfo.FORMID = workflow_bill.ID)\n" +
                    "    or exists(select 1 from workflow_base where workflow_bill.ID = workflow_base.FORMID))\n" +
                    "  and not exists(select 1 from modeformextend where workflow_bill.ID = modeformextend.FORMID)\n" +
                    "  and lower(tablename) not like 'edc_%'";


            if (ConfigUtil.isE9()) {
                sql += "   and not exists(select 1 from edc_form_page where workflow_bill.ID = edc_form_page.FORMID)\n";

            }

            sql += " and lower(tablename) not in (" + StringUtils.join(standtables, ",") + ")";

            rs.executeQuery(sql);


            while (rs.next()) {

                ArrayList<String> arrayList = new ArrayList<>();
                String formid = rs.getString("id");
                String tablename = rs.getString("tablename");

                if (tablename.toLowerCase().startsWith("edc_")) {// edc 的表都会迁
                    continue;
                }

                String formname = E10CubeUtil.getLabelName(rs.getString("namelabel"));


                String desc = "";
                String handdesc = "";
                String sqlwhere = " where 1=1 ";


                rs_1.executeQuery("select 'mode' as type , id ,modename as name from modeinfo where formid =?\n" +
                        "union\n" +
                        "select 'wf' as type , id ,workflowname as name from workflow_base where formid =?", formid, formid);

                if (rs_1.getCounts() <= 0) {// 没查到数据 不用处理
                    continue;
                }

                boolean mode = false;
                boolean request = false;
                while (rs_1.next()) {

                    String type = rs_1.getString("type");
                    String id = rs_1.getString("id");
                    String name = Util.processBody(rs_1.getString("name"));

                    mode = mode || type.equals("mode");
                    request = request || type.equals("wf");
                    desc += "\n" + type+ ":" + id + "-" + name;
                }


                if (mode) {
                    sqlwhere += " and formmodeid is null";
                }
                if (request) {
                    sqlwhere += " and requestid is null";
                }


                rs_1.executeQuery("select count(*) as count from " + tablename + sqlwhere);

                int count = 0;
                if (rs_1.next()) {
                    count = rs_1.getInt("count");
                }

                int _count = 0;

                if (mode) {
                    //
                    rs_1.executeQuery("select count(*) as count from " + tablename + " where formmodeid  is not null and requestid is not null ");

                    if (rs_1.next()) {
                        _count = rs_1.getInt("count");
                    }

                }


                if (count == 0 && _count == 0) {// 数据量为0 肯定么关系
                    continue;
                }


                if (request && mode && count > 0) {// 如果流程表与建模表公用  并且存在两个都是null的数据

                    handdesc += " 该表单同时关联流程与建模模块，但存在没有关联流程与建模的数据(requestid is null and formmodeid is null ),请确认是否存在二开业务";
                }

                if (request && !mode && count > 0) {// 如果仅关联流程  但是存在requestid是null的数据

                    handdesc += " 该表单关联流程，但表中存在没有关联流程的数据(requestid is null ),请确认是否存在二开业务";
                }

                if (!request && mode && count > 0) {// 如果仅关联模块  但是存在formmodeid 是null的数据

                    handdesc += " 该表单关联模块，但表中存在没有关联模块的数据(formmodeid is null ),请确认是否存在二开业务";

                    // 关联建模 但是没关联流程   没关联建模的数据中 还有requestid 不为null的数据
                    rs_1.executeQuery("select count(*) as count from " + tablename + " where formmodeid  is  null and requestid is not null ");

                    if (rs_1.next()) {
                        if (rs_1.getInt("count") > 0) {
                            handdesc += " \n该表单关联模块，没关联流程，但表中存在没有关联模块的数据关联了流程数据(formmodeid  is  null and requestid is not null )" +
                                    ",数量：" + rs_1.getInt("count") + ",请确认是否存在二开业务";
                        }
                    }

                }


                if (_count > 0) {
                    handdesc += "\n该表单关联模块，但表中存在关联模块的数据中 还关联了流程 (requetid is not null and formmodeid is not null )，数量：" + _count + ",请确认是否存在二开业务";
                }

                arrayList.add(formid);
                arrayList.add(formname);
                arrayList.add(tablename);
                arrayList.add(desc);
                arrayList.add(count + "");
                arrayList.add(handdesc);
                values8.add(arrayList);
            }


            addExcelModal(writeEntity, "表单管理中关联建模/流程表单未关联业务", "表单管理中关联建模/流程的实体表单,但未关联业务逻辑，请确认是否存在二开业务 \n 流程表中requestid 为空，建模表中formmodeid为空\n 数据已迁移至对应表单中，请确认相关业务并于e10中实现", modetableheader, values8);


        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
            new BaseBean().writeLog("e.getMessage() = " + e.getMessage());
            throw new RuntimeException(e);
        }

    }

    public boolean containsIgnoreCase(String[] array, String target) {
        for (String str : array) {
            //标准表里带*号的
            int indexOfAsterisk = str.indexOf('*');
            if (indexOfAsterisk != -1) {
                str = str.substring(0, indexOfAsterisk);
                if (target.toLowerCase().contains(str.toLowerCase())) {
                    return true;
                }
            } else {
                if (str.equalsIgnoreCase(target)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void getStandTableDevCol(ExcelWriteEntity writeEntity, String sheetname, String funcname) {

        List<String> headernames = new ArrayList<String>() {{
            add("类型");// 标准表二开字段，非表单管理中的二开字段
            add("表名");
            add("字段名称");
            add("数据库字段类型");
        }};

        final String e9sysfield = new BaseBean().getPropValue("sqlreplace", "e9sysfield");

        //缓存E9标准字段
        HashSet<String> e9SysFieldSet = JSONObject.parseObject(e9sysfield, new TypeReference<HashSet<String>>() {
        });


        //缓存E9标准字段
        HashSet<String> e9CubTableSet = new HashSet<>();
        HashSet<String> e9CubTableFieldsSet = new HashSet<>();
        HashSet<String> e9StandFieldsSet = new HashSet<String>() {{
            add("id");
            add("mainid");
            add("form_biz_id");
            add("formmodeid");
            add("modedatacreatedate");
            add("modedatacreater");
            add("modedatacreatertype");
            add("modedatacreatetime");
            add("modedatamodifydatetime");
            add("modedatamodifier");
            add("modeuuid");
            add("requestid");
            add("modelableid");
            add("modedatastatus");
        }};


        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select tablename as  maintable,detailtable,fieldname\n" +
                "from workflow_bill join workflow_billfield on workflow_bill.ID = BILLID where workflow_bill.ID not in (select formid from  modeformextend)");

        while (rs.next()) {

            String maintable = rs.getString("maintable").toLowerCase();
            String detailtable = rs.getString("detailtable").toLowerCase();
            String fieldname = rs.getString("fieldname").toLowerCase();

            String tablname = StringUtils.isNotBlank(detailtable) ? detailtable : maintable;

            e9CubTableSet.add(tablname);
            e9CubTableFieldsSet.add(tablname + "|cus|" + fieldname);

        }


        List<List<String>> standvalues = new ArrayList<>();
        List<List<String>> custablevalues = new ArrayList<>();


        String dbtype = rs.getDbtype();

        String sql = geTableAndtColumnInfoSql(dbtype);


        rs.executeQuery(sql);

        while (rs.next()) {

            List<String> value = new ArrayList<>();

            String table_name = rs.getString("table_name").toLowerCase();
            String column_name = rs.getString("column_name").toLowerCase();
            String column_type = rs.getString("column_type");

            value.add(table_name);
            value.add(column_name);
            value.add(column_type);
            if (e9SysFieldSet.contains(table_name + "|sys|" + column_name)) {// 标准字段

                continue;
            }


            if (e9CubTableFieldsSet.contains(table_name + "|cus|" + column_name)) {// 表单管理里的字段

                continue;
            }

            if (e9CubTableSet.contains(table_name) && e9StandFieldsSet.contains(column_name)) {// 表单表 并且表单表系统字段
                continue;
            }
            if (containsIgnoreCase(new String[]{"modedatashare_*", "modeviewlog_*"}, table_name)) {// 表单表 并且表单表系统字段

                continue;
            }

            if (e9CubTableSet.contains(table_name)) {// 说明是表单表

                value.add(0, "非表单管理中的二开字段");
                custablevalues.add(value);
            } else if (containsIgnoreCase(E9_STANDARD_TABLE, table_name)) {
                value.add(0, "标准表二开字段");
                standvalues.add(value);
            }


        }

        standvalues.addAll(custablevalues);


        addExcelModal(writeEntity, sheetname, funcname, headernames, standvalues);


    }


    /**
     * 获取数据库中所有表及其字段的查询sql
     *
     * @param dbtype
     * @return
     */
    public static String geTableAndtColumnInfoSql(String dbtype) {
        String sql = "select table_name,column_name,column_type,column_default,is_nullable from information_schema.COLUMNS where   TABLE_SCHEMA = (SELECT DATABASE ()) order by table_name, COLUMN_NAME";
        if (dbtype.equals(Constant.SQLSERVER)) {
//            sql="select name as COLUMN_NAME from syscolumns where id=(select max(id) from sysobjects where xtype='u' and name='"++"')   order by name ";
            sql = "select  table_name,column_name,data_type as column_type,is_nullable,column_default  from INFORMATION_SCHEMA.COLUMNS  order by table_name, COLUMN_NAME ";
        } else if (dbtype.equals(Constant.ORACLE)) {
            sql = "SELECT table_name,column_name,data_type as column_type,nullable as is_nullable,TRANSLONG_TO_CHAR(table_name,column_name) as column_default FROM USER_TAB_COLUMNS  order by table_name, COLUMN_NAME ";
        } else if (dbtype.equals(Constant.POSTGRESQL)) {
            sql = "select table_name,column_name,data_type as column_type,is_nullable,column_default," +
                    "character_maximum_length as data_length from information_schema.columns where table_schema=(SELECT current_schema())   order by table_name, COLUMN_NAME";
        } else if (dbtype.equals(Constant.MYSQL)) {
            sql = "select table_name,column_name,column_type,column_default,is_nullable from information_schema.COLUMNS where    TABLE_SCHEMA = (SELECT DATABASE ()) order by table_name, COLUMN_NAME ";
        }
        return sql;
    }


}
