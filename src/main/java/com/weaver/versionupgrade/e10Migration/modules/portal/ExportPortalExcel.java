package com.weaver.versionupgrade.e10Migration.modules.portal;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import weaver.conn.RecordSet;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.Util;
import weaver.hrm.company.SubCompanyComInfo;
import weaver.portal.*;
import weaver.portal.interfaces.HpElementHandler;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.weaver.versionupgrade.e10Migration.modules.SB2ExcleWrite.excelWrite_html;
import static weaver.e10Migration.excel.file.ExcelWrite.addExcelModal;

/**
 * @author wujiahao
 * @date 2024/5/11 14:38
 */
public class ExportPortalExcel {


    //流程表单信息
    List<String> portalHead = new ArrayList<String>(Arrays.asList("飘窗名称", "是否启用", "图片链接", "操作说明"));
    List<String> pcHpinfoHead = new ArrayList<String>(Arrays.asList("ID", "门户名称", "启用状态", "元素信息", "元素类型", "E9设置", "设置提示", "替换后sql", "是否完全替换", "替换信息"));
    List<String> pcHpinfoUselessHead = new ArrayList<String>(Arrays.asList("ID", "门户名称", "门户类型", "启用状态", "元素信息", "元素ID", "元素类型"));
    List<String> pcHpinfoForLayoutHead = new ArrayList<String>(Arrays.asList("ID", "门户名称", "门户类型", "启用状态", "提示信息"));
    List<String> hpSynergyinfoNoPassHead = new ArrayList<String>(Arrays.asList("ID", "协同名称", "启用状态"));

    /**
     * 功能维度
     */
    public String doPoralAll(String path) {
        try {
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("门户模块_功能维度_PortalAll", path);
            getHpFloatArea(writeEntity, "门户飘窗");
            getPCHpinfo(writeEntity, "PC门户页面");
            getMobileHpinfo(writeEntity, "移动门户");
            getHpSynergyinfo(writeEntity, "协同区");
            getDevInfo(writeEntity, "二开部分");

            String filePath = "";
            if (writeEntity.getSheets().size() > 0) {
                filePath = excelWrite_html(writeEntity);
            }
            return filePath;
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }
        return "";
    }

    private void getHpFloatArea(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet(false);
        rs.executeQuery(" select * from hpfloatarea ");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String nname = rs.getString("nname");//飘窗名称
            String isuse = rs.getString("isuse");//是否启用
            String detailContent = rs.getString("detailContent");//具体设置
            String picSrc = "";//图片路径
            JSONObject detailContentJson = new JSONObject();
            if (!"".equals(detailContent)) {
                try {
                    detailContentJson = JSON.parseObject(detailContent);
                    picSrc = detailContentJson.getString("picSrc"); //图片路径

                } catch (Exception ex) {
                }
            }

            if ("".equals(picSrc)) continue;

            value.add(nname);
            value.add("1".equals(isuse) ? "启用" : "未启用");
            value.add(picSrc);
            value.add("需要重新上传飘窗图片");

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, portalHead, values);
    }

    private void getPCHpinfo(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet(false);
        RecordSet rs1 = new RecordSet(false);
        rs.executeQuery(" select * from hpinfo ");
        while (rs.next()) {

            String id = rs.getString("id");//门户名称
            String infoname = rs.getString("infoname");//门户名称
            String isuse = rs.getString("isuse");//是否启用

            rs1.executeQuery("select * from hplayout where hpid =? and usertype=3 order by AREAFLAG,userid", id);
            HashSet<String> baseIds = new HashSet<>();//当前门户默认的门户元素id
            while (rs1.next()) {
                String areaelements = Util.null2String(rs1.getString("areaElements"));//所属区域包含的元素
                if (!"".equals(areaelements)) {
                    String[] areaelement = areaelements.split(",");
                    for (String areaelement1 : areaelement) {
                        if ("".equals(areaelement1))
                            continue;
                        baseIds.add(areaelement1);
                    }
                }
            }

            if (baseIds.size() == 0)
                continue;

            rs1.executeSql("select * from hpelement where id in (" + String.join(",", baseIds) + ")");
            while (rs1.next()) {
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("1".equals(isuse) ? "启用" : "未启用");


                //元素类型
                String ebaseid = rs1.getString("ebaseid");
                String hpElementId = rs1.getString("id");
                String hpElementTitle = Util.formatMultiLang(rs1.getString("title"));

                if (CheckElementEnum.getExist(ebaseid) == null)//不存在则跳过
                    continue;

                value.add(hpElementTitle + "(" + hpElementId + ")");//元素信息
                value.add(Objects.requireNonNull(CheckElementEnum.getExist(ebaseid)).getName());//元素类型
                String results = "";
                HashMap<String, Object> handlerResult = new HashMap<>();
                try {

                    HpElementHandler hpElementHandler = new HpElementFactory(CheckElementEnum.getExist(ebaseid)).getHpElementHandler();
                    if (hpElementHandler != null) {
                        results = hpElementHandler.handle(new HashMap<String, Object>() {
                            {
                                put("id", Util.null2String(rs1.getString("id")));
                                put("title", Util.formatMultiLang(rs1.getString("title")));
                                put("strsqlwhere", rs1.getString("strsqlwhere"));//通过^,^分隔
                                put("islocked", rs1.getString("islocked"));
                                put("issyselement", rs1.getString("issyselement"));
                                put("ebaseid", rs1.getString("ebaseid"));
                                put("hpid", rs1.getString("hpid"));
                                put("shareuser", rs1.getString("shareuser"));//通过^^分隔
                                put("isuse", rs1.getString("isuse"));//是否使用
                                //put("datasourceconfig", rs1.getString("datasourceconfig"));//是否
                                put("height", rs1.getString("height"));// 样式-高度
                                put("marginTop", rs1.getString("margintop")); // 样式- 上间距
                                put("marginBottom", rs1.getString("marginbottom")); // 样式- 下间距
                                put("marginRight", rs1.getString("marginright"));//样式- 右间距
                                put("marginLeft", rs1.getString("marginleft"));//样式- 左右间距
                                //PC端的库为hpmenustyle
                                put("styleid", rs1.getString("styleid"));//元素样式id
                                put("logo", rs1.getString("logo"));//元素图标
                            }
                        }, handlerResult);//设置数据提取
                    }
                } catch (Exception ex) {
                    results = "";
                }

                if ("".equals(results)) continue;//无设置，直接排除掉

                value.add(results);
                value.add(Objects.requireNonNull(CheckElementEnum.getExist(ebaseid)).getDescription());//设置描述

                value.add(Util.null2String(handlerResult.get("targetCode")));
                value.add(Util.null2String(handlerResult.get("transStatus")));
                value.add(Util.null2String(handlerResult.get("transMsg")));

                //添加到excel中
                values.add(value);
            }

        }
        addExcelModal(writeEntity, sheetname, sheetname, pcHpinfoHead, values);
    }

    public void getMobileHpinfo(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet(false);
        RecordSet rs1 = new RecordSet(false);
        rs.executeQuery(" select * from hp_mobile_hpinfo ");
        while (rs.next()) {

            String id = rs.getString("id");//门户名称
            String infoname = rs.getString("infoname");//门户名称
            String isuse = rs.getString("isuse");//是否启用

            rs1.executeQuery("select * from hp_mobile_hplayout where hpid =? ", id);
            HashSet<String> baseIds = new HashSet<>();//当前门户默认的门户元素id
            while (rs1.next()) {
                String areaelements = Util.null2String(rs1.getString("areaElement"));//所属区域包含的元素
                if (!"".equals(areaelements)) {
                    String[] areaelement = areaelements.split(",");
                    for (String areaelement1 : areaelement) {
                        if ("".equals(areaelement1))
                            continue;
                        baseIds.add(areaelement1);
                    }
                }
            }

            if (baseIds.size() == 0)
                continue;

            rs1.executeSql("select * from hp_mobile_element where id in (" + String.join(",", baseIds) + ")");
            while (rs1.next()) {
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("1".equals(isuse) ? "启用" : "未启用");


                //元素类型
                String ebaseid = rs1.getString("ebaseid");
                String hpElementId = rs1.getString("id");
                String hpElementTitle = Util.formatMultiLang(rs1.getString("title"));
                if (CheckElementMbEnum.getExist(ebaseid) == null)//不存在则跳过
                    continue;

                value.add(hpElementTitle + "(" + hpElementId + ")");//元素信息
                value.add(Objects.requireNonNull(CheckElementMbEnum.getExist(ebaseid)).getName());//元素类型
                String results = "";
                HashMap<String, Object> handlerResult = new HashMap<>();
                try {

                    HpElementHandler hpElementHandler = new HpElementMbFactory(CheckElementMbEnum.getExist(ebaseid)).getHpElementHandler();
                    if (hpElementHandler != null) {
                        results = hpElementHandler.handle(new HashMap<String, Object>() {
                            {
                                put("id", Util.null2String(rs1.getString("id")));
                                put("title", Util.formatMultiLang(rs1.getString("title")));
                                put("strsqlwhere", rs1.getString("strsqlwhere"));//通过^,^分隔
                                put("islocked", rs1.getString("islocked"));
                                put("issyselement", rs1.getString("issyselement"));
                                put("ebaseid", rs1.getString("ebaseid"));
                                put("hpid", rs1.getString("hpid"));
                                put("shareuser", rs1.getString("shareuser"));//通过^^分隔
                                put("isuse", rs1.getString("isuse"));//是否使用
                                //put("datasourceconfig",rs1.getString("datasourceconfig"));//是否
                                //移动端的库为hpmobilestyle
                                //PC端的库为hpmenustyle
                                put("styleid", rs1.getString("styleid"));//元素样式id
                                put("logo", rs1.getString("logo"));//元素图标
                            }
                        }, handlerResult);//设置数据提取
                    }
                } catch (Exception ex) {
                    results = "";
                }

                if ("".equals(results)) continue;//无设置，直接排除掉

                value.add(results);
                value.add(Objects.requireNonNull(CheckElementMbEnum.getExist(ebaseid)).getDescription());//设置描述

                value.add(Util.null2String(handlerResult.get("targetCode")));
                value.add(Util.null2String(handlerResult.get("transStatus")));
                value.add(Util.null2String(handlerResult.get("transMsg")));

                //添加到excel中
                values.add(value);
            }

        }
        addExcelModal(writeEntity, sheetname, sheetname, pcHpinfoHead, values);
    }

    private void getHpSynergyinfo(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();//处理了迁移了的协同门户元素数据
        List<List<String>> valuesNoPass = new ArrayList<List<String>>();//处理无法迁移的协同门户

        RecordSet rs = new RecordSet(false);
        //配置信息
        rs.executeSql(" select hpid,isuse,width,defaultExpand from synergyconfig  ");
        ConcurrentHashMap<String, Object> synergyconfigMap = new ConcurrentHashMap<>();
        while (rs.next()) {
            if ("".equals(Util.null2String(rs.getString("hpid")))) continue;
            String hpid = Math.abs(rs.getInt("hpid")) + "";
            String isuse = rs.getString("isuse");
            String width = rs.getString("width");
            String defaultExpand = rs.getString("defaultExpand");

            if (synergyconfigMap.containsKey(hpid))
                continue;
            synergyconfigMap.put(hpid, new HashMap() {{
                put("hpid", hpid);
                put("isuse", isuse);
                put("width", width);
                put("defaultExpand", defaultExpand);
            }});
        }

        RecordSet rs1 = new RecordSet(false);
        rs.executeQuery(" select * from synergy_base order by frommodule");
        while (rs.next()) {

            String id = rs.getString("id");//协同ID
            String frommodule = rs.getString("frommodule");//来源模块
            String frompagepara = rs.getString("frompagepara");//来源参数
            String frompage = rs.getString("frompage");//来源页面
            String contentId = rs.getString("wfid");//具体流程、知识的id
            String modeid = rs.getString("modeid");//建模模块
            String supid = rs.getString("supid");// -1 表示是具体的流程、知识数据 其他的标识系统菜单
            HashMap currentSynergyconfigMap = (HashMap) synergyconfigMap.get(id);
            if (currentSynergyconfigMap == null)
                currentSynergyconfigMap = new HashMap<>();
            String isuse = Util.null2String(currentSynergyconfigMap.get("isuse"));//是否启用

            String samepageid = rs.getString("samepageid");// 如果是显示其他页面的内容，不需要处理
            if (!"".equals(samepageid)) {
                //重复数据，不需要插入
                continue;
            }

            if (frommodule.equals("wf|menu") || frommodule.equals("wf|operat") || frommodule.equals("doc|menu") || frommodule.equals("doc|operat")) {
                //类型数据，不需要插入
                continue;
            }

            String infoname = "";

            //流程页面的门户页面 以及 建模页面的门户页面
            if (frommodule.equals("hp_workflow_form")) {
                //表单中的门户元素
                rs1.executeQuery("select * from workflow_base where id = ? ", contentId);
                if (rs1.next())
                    infoname += "流程模版中门户元素:" + Util.formatMultiLang(rs1.getString("WORKFLOWNAME"));
                else
                    continue;
                isuse = "3";
            } else if (frommodule.equals("hp_mode_form")) {
                //表单中的门户元素
                rs1.executeQuery("select * from modeinfo where id = ? ", modeid);
                if (rs1.next())
                    infoname += "建模模版中门户元素:" + Util.formatMultiLang(rs1.getString("MODENAME"));
                else
                    continue;
                isuse = "3";
            } else if (supid.equals("-1")) {

                //处理下文档目录和流程
                if (frommodule.equals("doc")) {
                    //文档id contentId
                    rs1.executeQuery("select * from docseccategory where id = ? ", contentId);
                    if (rs1.next())
                        infoname += "知识协同-具体文档:" + Util.formatMultiLang(rs1.getString("CATEGORYNAME"));
                    else
                        continue;
                } else if (frommodule.equals("workflow")) {
                    //流程id contentId
                    rs1.executeQuery("select * from workflow_base where id = ? ", contentId);
                    if (rs1.next())
                        infoname += "流程协同-具体流程:" + Util.formatMultiLang(rs1.getString("WORKFLOWNAME"));
                    else
                        continue;
                }

                if (frompage.equals("/docs/docs/DocAdd.jsp") || frompage.equals("/workflow/request/AddRequest.jsp")) {
                    //创建
                    infoname += "（新建页）";
                } else {
                    //查看
                    infoname += "（查看处理页）";
                }
            } else {
                String pageName = generatePageKey(frompage, frompagepara);
                if ("".equals(pageName)) {
                    continue;
                } else {
                    if (pageName.indexOf("-") > 0) {
                        //处理无法对应的情况
                        if ("".equals(infoname)) {
                            //无法迁移的元素
                            List<String> value = new ArrayList<String>();
                            value.add(id);
                            value.add(pageName);
                            value.add("1".equals(isuse) ? "启用" : ("3".equals(isuse) ? "" : "未启用"));
                            valuesNoPass.add(value);
                            continue;
                        }
                    } else {
                        //处理迁移了的页面，需要检测元素
                        infoname = pageName;
                    }
                }
            }

            rs1.executeQuery("select * from hplayout where hpid =? order by usertype desc,userid", "-" + id);
            HashSet<String> baseIds = new HashSet<>();//当前门户默认的门户元素id
            while (rs1.next()) {
                String areaelements = Util.null2String(rs1.getString("areaElements"));//所属区域包含的元素
                if (!"".equals(areaelements)) {
                    String[] areaelement = areaelements.split(",");
                    for (String areaelement1 : areaelement) {
                        if ("".equals(areaelement1))
                            continue;
                        baseIds.add(areaelement1);
                    }
                }
            }

            if (baseIds.size() == 0)
                continue;

            rs1.executeSql("select * from hpelement where id in (" + String.join(",", baseIds) + ")");
            while (rs1.next()) {
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("1".equals(isuse) ? "启用" : ("3".equals(isuse) ? "" : "未启用"));


                //元素类型
                String ebaseid = rs1.getString("ebaseid");
                String hpElementId = rs1.getString("id");
                String hpElementTitle = Util.formatMultiLang(rs1.getString("title"));
                if (CheckElementEnum.getExist(ebaseid) == null)//不存在则跳过
                    continue;

                value.add(hpElementTitle + "(" + hpElementId + ")");//元素信息
                value.add(Objects.requireNonNull(CheckElementEnum.getExist(ebaseid)).getName());//元素类型
                String results = "";
                HashMap<String, Object> handlerResult = new HashMap<>();
                try {

                    HpElementHandler hpElementHandler = new HpElementFactory(CheckElementEnum.getExist(ebaseid)).getHpElementHandler();
                    if (hpElementHandler != null) {
                        results = hpElementHandler.handle(new HashMap<String, Object>() {
                            {
                                put("id", Util.null2String(rs1.getString("id")));
                                put("title", Util.formatMultiLang(rs1.getString("title")));
                                put("strsqlwhere", rs1.getString("strsqlwhere"));//通过^,^分隔
                                put("islocked", rs1.getString("islocked"));
                                put("issyselement", rs1.getString("issyselement"));
                                put("ebaseid", rs1.getString("ebaseid"));
                                put("hpid", rs1.getString("hpid"));
                                put("shareuser", rs1.getString("shareuser"));//通过^^分隔
                                put("isuse", rs1.getString("isuse"));//是否使用
                                //put("datasourceconfig", rs1.getString("datasourceconfig"));//是否
                                put("height", rs1.getString("height"));// 样式-高度
                                put("marginTop", rs1.getString("margintop")); // 样式- 上间距
                                put("marginBottom", rs1.getString("marginbottom")); // 样式- 下间距
                                put("marginRight", rs1.getString("marginright"));//样式- 右间距
                                put("marginLeft", rs1.getString("marginleft"));//样式- 左右间距
                                //PC端的库为hpmenustyle
                                put("styleid", rs1.getString("styleid"));//元素样式id
                                put("logo", rs1.getString("logo"));//元素图标
                                put("from", "synergy");//数据来源
                            }
                        }, handlerResult);//设置数据提取
                    }
                } catch (Exception ex) {
                    results = "";
                }

                if ("".equals(results)) continue;//无设置，直接排除掉

                value.add(results);
                value.add(Objects.requireNonNull(CheckElementEnum.getExist(ebaseid)).getDescription());//设置描述

                value.add(Util.null2String(handlerResult.get("targetCode")));
                value.add(Util.null2String(handlerResult.get("transStatus")));
                value.add(Util.null2String(handlerResult.get("transMsg")));

                //添加到excel中
                values.add(value);
            }

        }
        addExcelModal(writeEntity, sheetname, sheetname, pcHpinfoHead, values);
        addExcelModal(writeEntity, sheetname, sheetname + "-E10不支持", hpSynergyinfoNoPassHead, valuesNoPass);
    }

    //获取E9E10协同区对应关系
    private String generatePageKey(String frompage, String frompagepara) {
        String page_key = "";
        String page_name = "";
        //通过select * from hp_synergy_menu where tenant_key='all_teams' 查询
        if (frompage.equals("/workflow/request/RequestTypeShow.jsp")) {
            //新建流程 E10新建页面是弹出页的方式 无法迁移 => 提交申请
            page_key = "workflow_newflow";
            page_name = "新建流程";
        } else if (frompage.equals("/workflow/search/WFSearchResult.jsp") && frompagepara.equals("scope=doing")) {
            //待办事宜 => 待办流程
            page_key = "workflow_list_todo";
            page_name = "待办事宜";
        } else if (frompage.equals("/workflow/search/WFSuperviseList.jsp")) {
            //流程督办
            page_key = "workflow_list_supervise";
            page_name = "流程督办";
        } else if (frompage.equals("/workflow/search/WFSearchResult.jsp") && frompagepara.equals("scope=done")) {
            //已办事宜 => 已办流程
            page_key = "workflow_list_done";
            page_name = "已办事宜";
        } else if (frompage.equals("/workflow/search/WFSearchResult.jsp") && frompagepara.equals("scope=mine")) {
            //我的请求 => 我发起的流程
            page_key = "workflow_list_mine";
            page_name = "我的请求";
        } else if (frompage.equals("/workflow/request/wfAgentList.jsp") && frompagepara.equals("scope=1")) {
            //流程代理  目前对应到E10=>我待处理的流程
            page_key = "workflow_agent_my";
            page_name = "流程代理";
        } else if (frompage.equals("/system/systemmonitor/workflow/WorkflowMonitorList.jsp")) {
            //流程监控
            page_key = "workflow_list_monitor";
            page_name = "流程监控";
        } else if (frompage.equals("/workflow/search/WFSearchShow.jsp")) {
            //查询流程 => 全部流程
            page_key = "workflow_list_all";
            page_name = "查询流程";
        } else if (frompage.equals("/docs/docs/DocList.jsp")) {
            //新建文档 目前对应到了 E10公共知识库（上面有新建文档、批量共享的按钮）=>公共知识库
            page_key = "doc_knowledge_200";
            page_name = "新建文档";
        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=5")) {
            //知识中心
            page_key = "doc_knowledge_center";
            page_name = "知识中心";
        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=14")) {
            //查阅文档 => 全部知识
            page_key = "doc_knowledge_all";
            page_name = "查阅文档";
        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=6")) {
            //文档目录
            page_name = "文档目录-E10不支持";
        } else if (frompage.equals("/docs/docdummy/DocDummyRight.jsp")) {
            //虚拟目录 => 虚拟知识库
            page_key = "doc_dummy_400";
            page_name = "虚拟目录";
        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=2")) {
            //知识排名
            page_name = "知识排名-E10不支持";

        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=0")) {
            //最新文档
            page_name = "最新文档-E10不支持";

        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=7")) {
            //文档订阅 => 知识订阅
            page_key = "doc_subscribe";
            page_name = "文档订阅";
        } else if (frompage.equals("/docs/search/DocCommonContent.jsp") && frompagepara.equals("scope=13")) {
            //批量共享 暂未对应
            page_name = "批量共享-E10不支持";

        } else if (frompage.equals("/docs/tools/DocCopyMove.jsp")) {
            //移动复制
            page_key = "doc_movecopy_200";
            page_name = "移动复制";
        } else if (frompage.equals("/system/systemmonitor/docs/DocMonitor.jsp")) {
            //文档监控 => 知识监控
            page_key = "doc_knowledge_systemManager";
            page_name = "文档监控";
        }
        return page_name;
    }

    private void getDevInfo(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        List<List<String>> values = new ArrayList<List<String>>();
        List<List<String>> valuesNoPass = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet(false);
        RecordSet rs1 = new RecordSet(false);

        //获取门户元素名称
        HashMap<String, String> hpbaseelementMap = new HashMap<String, String>();
        RecordSet rs2 = new RecordSet();
        rs2.executeQuery("select * from hpbaseelement");
        while (rs2.next()) {
            hpbaseelementMap.put(rs2.getString("id"), Util.formatMultiLang(rs2.getString("title")));
        }

        rs2.executeQuery("select id,layouttype,cellmergeinfo,layouttable,allowarea,ftl from pagelayout ");
        HashSet<String> layoutIds = new HashSet<>();//无法转换的布局
        while (rs2.next()){
            String id = rs2.getString("id");
            String layouttype = rs2.getString("layouttype");
            if(!"sys".equals(layouttype) && !"design".equals(layouttype))
                layoutIds.add(id);
        }

        //====================================PC门户=================================================
        rs.executeQuery(" select * from hpinfo ");
        while (rs.next()) {

            String id = rs.getString("id");//门户名称
            String infoname = rs.getString("infoname");//门户名称
            String layoutid = rs.getString("layoutid");//所属布局id
            String isuse = rs.getString("isuse");//是否启用

            if(layoutIds.contains(layoutid)){
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("PC门户");
                value.add("1".equals(isuse) ? "启用" : ("3".equals(isuse) ? "" : "未启用"));
                value.add("无法解析页面布局，会转成一栏布局，需要手动调整。");
                valuesNoPass.add(value);
            }

            rs1.executeQuery("select * from hplayout where hpid =? and usertype=3 order by AREAFLAG,userid", id);
            HashSet<String> baseIds = new HashSet<>();//当前门户默认的门户元素id
            while (rs1.next()) {
                String areaelements = Util.null2String(rs1.getString("areaElements"));//所属区域包含的元素
                if (!"".equals(areaelements)) {
                    String[] areaelement = areaelements.split(",");
                    for (String areaelement1 : areaelement) {
                        if ("".equals(areaelement1))
                            continue;
                        baseIds.add(areaelement1);
                    }
                }
            }

            if (baseIds.size() == 0)
                continue;

            rs1.executeSql("select * from hpelement where id in (" + String.join(",", baseIds) + ")");
            while (rs1.next()) {
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("PC门户");
                value.add("1".equals(isuse) ? "启用" : "未启用");

                //元素类型
                String ebaseid = rs1.getString("ebaseid");
                String hpElementId = rs1.getString("id");
                String hpElementTitle = Util.formatMultiLang(rs1.getString("title"));

                //如果不存在，则是二开元素
                if (EbddComponentType.getComponentById(ebaseid) == null) {
                    value.add(hpElementTitle);//元素信息
                    value.add(hpElementId);//元素ID
                    value.add(hpbaseelementMap.get(ebaseid));//元素类型

                    values.add(value);
                }else{
                    continue;
                }
            }
        }

        //====================================移动门户=================================================

        rs.executeQuery(" select * from hp_mobile_hpinfo ");
        while (rs.next()) {

            String id = rs.getString("id");//门户名称
            String infoname = rs.getString("infoname");//门户名称
            String isuse = rs.getString("isuse");//是否启用

            rs1.executeQuery("select * from hp_mobile_hplayout where hpid =? ", id);
            HashSet<String> baseIds = new HashSet<>();//当前门户默认的门户元素id
            while (rs1.next()) {
                String areaelements = Util.null2String(rs1.getString("areaElement"));//所属区域包含的元素
                if (!"".equals(areaelements)) {
                    String[] areaelement = areaelements.split(",");
                    for (String areaelement1 : areaelement) {
                        if ("".equals(areaelement1))
                            continue;
                        baseIds.add(areaelement1);
                    }
                }
            }

            if (baseIds.size() == 0)
                continue;

            rs1.executeSql("select * from hp_mobile_element where id in (" + String.join(",", baseIds) + ")");
            while (rs1.next()) {
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("移动门户");
                value.add("1".equals(isuse) ? "启用" : "未启用");

                //元素类型
                String ebaseid = rs1.getString("ebaseid");
                String hpElementId = rs1.getString("id");
                String hpElementTitle = Util.formatMultiLang(rs1.getString("title"));
                if (EbddComponentType.getComponentById4mobile(ebaseid) == null)//不存在则跳过
                {
                    value.add(hpElementTitle);//元素信息
                    value.add(hpElementId);//元素ID
                    value.add(hpbaseelementMap.get(ebaseid));//元素类型

                    values.add(value);
                }else{
                    continue;
                }
            }
        }


        //====================================协同区门户=================================================
        //配置信息
        rs.executeSql(" select hpid,isuse,width,defaultExpand from synergyconfig  ");
        ConcurrentHashMap<String, Object> synergyconfigMap = new ConcurrentHashMap<>();
        while (rs.next()) {
            if ("".equals(Util.null2String(rs.getString("hpid")))) continue;
            String hpid = Math.abs(rs.getInt("hpid")) + "";
            String isuse = rs.getString("isuse");
            String width = rs.getString("width");
            String defaultExpand = rs.getString("defaultExpand");

            if (synergyconfigMap.containsKey(hpid))
                continue;
            synergyconfigMap.put(hpid, new HashMap() {{
                put("hpid", hpid);
                put("isuse", isuse);
                put("width", width);
                put("defaultExpand", defaultExpand);
            }});
        }

        rs.executeQuery(" select * from synergy_base order by frommodule");
        while (rs.next()) {

            String id = rs.getString("id");//协同ID
            String frommodule = rs.getString("frommodule");//来源模块
            String frompagepara = rs.getString("frompagepara");//来源参数
            String frompage = rs.getString("frompage");//来源页面
            String contentId = rs.getString("wfid");//具体流程、知识的id
            String modeid = rs.getString("modeid");//建模模块
            String supid = rs.getString("supid");// -1 表示是具体的流程、知识数据 其他的标识系统菜单
            HashMap currentSynergyconfigMap = (HashMap) synergyconfigMap.get(id);
            if (currentSynergyconfigMap == null)
                currentSynergyconfigMap = new HashMap<>();
            String isuse = Util.null2String(currentSynergyconfigMap.get("isuse"));//是否启用

            String samepageid = rs.getString("samepageid");// 如果是显示其他页面的内容，不需要处理
            if (!"".equals(samepageid)) {
                //重复数据，不需要插入
                continue;
            }

            if (frommodule.equals("wf|menu") || frommodule.equals("wf|operat") || frommodule.equals("doc|menu") || frommodule.equals("doc|operat")) {
                //类型数据，不需要插入
                continue;
            }

            String infoname = "";

            //流程页面的门户页面 以及 建模页面的门户页面
            if (frommodule.equals("hp_workflow_form")) {
                //表单中的门户元素
                rs1.executeQuery("select * from workflow_base where id = ? ", contentId);
                if (rs1.next())
                    infoname += "流程模版中门户元素:" + Util.formatMultiLang(rs1.getString("WORKFLOWNAME"));
                else
                    continue;
                isuse = "3";
            } else if (frommodule.equals("hp_mode_form")) {
                //表单中的门户元素
                rs1.executeQuery("select * from modeinfo where id = ? ", modeid);
                if (rs1.next())
                    infoname += "建模模版中门户元素:" + Util.formatMultiLang(rs1.getString("MODENAME"));
                else
                    continue;
                isuse = "3";
            } else if (supid.equals("-1")) {

                //处理下文档目录和流程
                if (frommodule.equals("doc")) {
                    //文档id contentId
                    rs1.executeQuery("select * from docseccategory where id = ? ", contentId);
                    if (rs1.next())
                        infoname += "知识协同-具体文档:" + Util.formatMultiLang(rs1.getString("CATEGORYNAME"));
                    else
                        continue;
                } else if (frommodule.equals("workflow")) {
                    //流程id contentId
                    rs1.executeQuery("select * from workflow_base where id = ? ", contentId);
                    if (rs1.next())
                        infoname += "流程协同-具体流程:" + Util.formatMultiLang(rs1.getString("WORKFLOWNAME"));
                    else
                        continue;
                }

                if (frompage.equals("/docs/docs/DocAdd.jsp") || frompage.equals("/workflow/request/AddRequest.jsp")) {
                    //创建
                    infoname += "（新建页）";
                } else {
                    //查看
                    infoname += "（查看处理页）";
                }
            } else {
                String pageName = generatePageKey(frompage, frompagepara);
                if ("".equals(pageName)) {
                    continue;
                } else {
                    if (pageName.indexOf("-") > 0) {
                        //处理无法对应的情况
                        if ("".equals(infoname)) {
                            continue;
                        }
                    } else {
                        //处理迁移了的页面，需要检测元素
                        infoname = pageName;
                    }
                }
            }

            rs1.executeQuery("select * from hplayout where hpid =? order by usertype desc,userid", "-" + id);
            HashSet<String> baseIds = new HashSet<>();//当前门户默认的门户元素id
            while (rs1.next()) {
                String areaelements = Util.null2String(rs1.getString("areaElements"));//所属区域包含的元素
                if (!"".equals(areaelements)) {
                    String[] areaelement = areaelements.split(",");
                    for (String areaelement1 : areaelement) {
                        if ("".equals(areaelement1))
                            continue;
                        baseIds.add(areaelement1);
                    }
                }
            }

            if (baseIds.size() == 0)
                continue;

            rs1.executeSql("select * from hpelement where id in (" + String.join(",", baseIds) + ")");
            while (rs1.next()) {
                List<String> value = new ArrayList<String>();
                value.add(id);
                value.add(infoname);
                value.add("协同区");
                value.add("1".equals(isuse) ? "启用" : ("3".equals(isuse) ? "" : "未启用"));

                //元素类型
                String ebaseid = rs1.getString("ebaseid");
                String hpElementId = rs1.getString("id");
                String hpElementTitle = Util.formatMultiLang(rs1.getString("title"));
                if (EbddComponentType.getComponentById(ebaseid) == null)//不存在则跳过
                {
                    value.add(hpElementTitle);//元素信息
                    value.add(hpElementId);//元素ID
                    value.add(hpbaseelementMap.get(ebaseid));//元素类型

                    values.add(value);
                }else{
                    continue;
                }

            }
        }

        addExcelModal(writeEntity, sheetname, sheetname, pcHpinfoUselessHead, values);
        addExcelModal(writeEntity, sheetname, "需调整布局相关PC门户", pcHpinfoForLayoutHead, valuesNoPass);
    }


}
