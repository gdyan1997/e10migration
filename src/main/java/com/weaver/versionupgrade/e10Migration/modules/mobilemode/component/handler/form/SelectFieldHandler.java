package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler.form;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class SelectFieldHandler extends AbstractComponentHandler {


    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String rightActionType = Util.null2String(mecparamjson.get("rightActionType"));
        String datasource = Util.null2String(mecparamjson.get("datasource"));
        String rightAction_SQL = Util.null2String(mecparamjson.get("rightAction_SQL"));

        if ("2".equals(rightActionType)) {

            beans.add(
                    new ComponentBean().setHandType(HandType.sqlDev).setResult("表单字段选项组件需要调整  数据源："
                            + E10CubeUtil.getdatasourcename(datasource) + " sql:" + rightAction_SQL));

        }


        this.setLists(beans);

    }

}
