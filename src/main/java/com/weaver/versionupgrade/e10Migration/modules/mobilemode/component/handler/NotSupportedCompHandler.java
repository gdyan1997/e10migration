package com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.AbstractComponentHandler;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.ComponentBean;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.HandType;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.component.context.ComponentConvertContext;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ygd2020
 * @date 2024-5-8 18:40
 * Description:  没支持  的  当作自定义组件
 **/
public class NotSupportedCompHandler extends AbstractComponentHandler {

    @Override
    public void getHandInfo(ComponentConvertContext componentConvertContext) {


        List<ComponentBean> beans = new ArrayList<>();

        String mecparam = componentConvertContext.getMecparam();
        String e9CompType = componentConvertContext.getE9CompType();

        JSONObject mecparamjson = JSON.parseObject(mecparam);


        String contentSource = Util.null2String(mecparamjson.get("contentSource"));// 1 查询列表
        String sourceUrl = Util.null2String(mecparamjson.get("sourceUrl"));// 1 查询列表


        beans.add(new ComponentBean().setHandType(HandType.notSupportComp).setResult(componentConvertContext.getE9CompType()));// e8 组件需要调整


        if (e9CompType.equals("LargeList")|| e9CompType.equals("HoriList")) {// 大图列表 横向列表

            if ("1".equals(contentSource)) {
                beans.add(new ComponentBean().setHandType(HandType.javaDev).setResult("内容url需要调整： " + sourceUrl));// e8 组件需要调整
            } else if ("2".equals(contentSource)) {
                beans.add(new ComponentBean().setHandType(HandType.sqlDev).setResult("查询列表数据源需要调整"));// e8 组件需要调整
            }

        } else if ("1".equals(contentSource)) {
            beans.add(new ComponentBean().setHandType(HandType.sqlDev).setResult("查询列表数据源需要调整"));// e8 组件需要调整
        }

        this.setLists(beans);
    }


}

