package com.weaver.versionupgrade.e10Migration.service;

import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.hrm.company.SubCompanyComInfo;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

/**
 * @author wujiahao
 * @date 2024/4/3 13:59
 */
public class E7ScheduleExcel {
    public static void getE7Schedule(ExcelWriteEntity writeEntity, String sheetname) throws Exception {


        //获取配置文件路径
        String rootPath = ConfigUtil.getConfigDir();
        System.out.println("rootPath=" + rootPath);
        int indexOf = rootPath.indexOf("e10Migration");
        String substring = rootPath.substring(0, indexOf);
       String actionfilepath = substring + "ecology" + File.separator + "WEB-INF" + File.separator + "service" + File.separator + "schedule.xml";
       // String actionfilepath = rootPath + "ecology" + File.separator + "WEB-INF" + File.separator + "service" + File.separator + "schedule.xml";


        List<List<String>> valuese7 = new ArrayList<List<String>>();



        try {
            // 指定要解析的XML文件
            File inputFile = new File(actionfilepath);
            // 创建 DocumentBuilderFactory 实例
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            // 创建 DocumentBuilder 实例
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            // 使用 DocumentBuilder 解析 XML 文件并返回 Document 对象
            Document doc = dBuilder.parse(inputFile);
            // 获取XML文档中所有名为 "service-point" 的元素
            NodeList nodeList = doc.getElementsByTagName("service-point");
            // 遍历每个 "service-point" 元素
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    // 获取 "service-point" 元素的 id 属性
                    String id = element.getAttribute("id");
                    // 获取 "invoke-factory" 元素下的 "construct" 元素的 "class" 属性
                    Element constructElement = (Element) element.getElementsByTagName("construct").item(0);
                    String className = constructElement.getAttribute("class");
                    // 获取 "construct" 元素下的 "set" 元素的 "value" 属性
                    Element setElement = (Element) constructElement.getElementsByTagName("set").item(0);
                    String value = setElement.getAttribute("value");
                    // 输出 id、class 和 value 属性值
                    System.out.println("id: " + id);
                    System.out.println("class: " + className);
                    System.out.println("value: " + value);
                    List<String> list = new ArrayList<>();
                    list.add(id);
                    list.add(className);
                    list.add(value);
                    valuese7.add(list);
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }

       List<String> formdata = new ArrayList<String>(Arrays.asList("计划任务标识", "计划任务类", "定时时间"));

        addExcelModal(writeEntity, "E7计划任务", "E7计划任务", formdata, valuese7);

    }
    public static void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


}
