package com.weaver.versionupgrade.e10Migration.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import com.weaver.versionupgrade.config.ClobTypeHandler;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.service.GetWorkflowJsCodeService;
import com.weaver.versionupgrade.entity.dto.JavascriptTransDTO;
import com.weaver.versionupgrade.entity.entity.*;
import com.weaver.versionupgrade.entity.mapper.JavascriptTransMapper;
import com.weaver.versionupgrade.entity.service.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import weaver.general.Util;
import weaver.hrm.company.SubCompanyComInfo;
import weaver.init.InitWeaverServer;
import weaver.systeminfo.SystemEnv;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * User: wy
 * Date: 2024/3/27
 * Time: 10:17
 * Description:
 **/
@Service
public class GetWorkflowJsCodeServiceImp implements GetWorkflowJsCodeService {

    @Autowired
    IWorkflowBaseService workflowBaseService;
    @Autowired
    IWorkflowFormbaseService workflowFormbaseService;
    @Autowired
    IWorkflowBillService workflowBillService;
    @Autowired
    IWorkflowNodebaseService workflowNodebaseService;
    @Autowired
    IWorkflowNodehtmllayoutService workflowNodehtmllayoutService;
    @Autowired
    IWorkflowBillfieldService workflowBillfieldService;
    @Autowired
    IWorkflowBilldetailtableService workflowBilldetailtableService;
    @Autowired
    IWorkflowFormfieldService workflowFormfieldService;
    @Autowired
    IJavascriptTransService javascriptTransService;
    @Autowired
    JavascriptTransMapper javascriptTransMapper;
    @Autowired
    IModehtmllayoutService modehtmllayoutService;

    @Override
    public HashMap<String, Object> getWorkflowJsCodeByPage(int page, int perPage, String workflowName, boolean isAll) {
        HashMap<String, Object> result = new HashMap<>();
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<HashMap<String, Object>> values = new ArrayList<>();

        IPage<Map<String, Object>> results = workflowNodehtmllayoutService.selectNodeHtmlLayoutByWorkflowIdWithPage(page, perPage, InitWeaverServer.oaversion.startsWith("9"), Util.null2String(workflowName), isAll);

        List<Map<String, Object>> records = results.getRecords();

        HashMap<String, String> workflowIdSetFor0 = new HashMap<>();//老表单
        HashMap<String, String> workflowIdSetFor1 = new HashMap<>();//新表单
        for (Map<String, Object> record : records) {
            String workflowid = Util.null2String(record.get("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(record.get("workflowname")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(record.get("subcompanyid"))));
            String isbill = Util.null2String(record.get("isbill"));

            String id = Util.null2String(record.get("layoutid"));
            String formid = Util.null2String(record.get("formid"));
            String nodeid = Util.null2String(record.get("nodeid"));
            String scriptstr = Util.null2String(ClobTypeHandler.clobToString(record.get("scriptstr")));
            String type = Util.null2String(record.get("type"));
            String typeName = "0".equals(type) ? "显示模版" : (("1".equals(type)) ? "打印模版" : "移动模版");
            String layoutName = Util.formatMultiLang(Util.null2String(record.get("layoutname")));

            //脚本中包含src属性的script标签
            boolean hasSrcScript = false;
            //脚本中包含style标签
            boolean hasCssStyle = false;

            String deScriptstr = E10CubeUtil.decodeStr(scriptstr);
            String deScriptWithoutTag = E10CubeUtil.getScript(deScriptstr);

            try {
                Element body = Jsoup.parse(deScriptstr);
                Elements scripts = body.select("script");
                for (Element script : scripts) {
                    if (script != null) {
                        String src = Util.null2String(script.attr("src"));
                        if (!"".equals(src)) {
                            hasSrcScript = true;
                            break;
                        }
                    }
                }

                Elements styles = body.select("style");
                for (Element style : styles) {
                    if (style != null) {
                        String str = E10CubeUtil.removeComments(style.data().trim());
                        if (StringUtils.isNotBlank(str)) {
                            hasCssStyle = true;
                            break;
                        }
                    }
                }

                Elements links = body.select("link");
                if (!links.isEmpty()) hasCssStyle = true;
            } catch (Exception e) {
                new weaver.general.BaseBean().writeLog("", e);
            }

            HashMap<String, Object> data = new HashMap<>();
            data.put("id", id);
            data.put("workflowid", workflowid);
            data.put("workflowname", workflowname);
            data.put("subcompanyname", subCompanyname);
            data.put("typename", typeName);
            data.put("layoutname", layoutName);
            data.put("formname", getFormName(isbill, formid));
            data.put("nodename", getNodeName(nodeid));
            data.put("scriptstr", deScriptWithoutTag);
            data.put("hassrcscript", hasSrcScript);
            data.put("hascssstyle", hasCssStyle);
            data.put("tranfercontent", null);

            values.add(data);

            if ("0".equals(isbill)) {
                workflowIdSetFor0.put(workflowid, formid);
            }
            if ("1".equals(isbill)) {
                workflowIdSetFor1.put(workflowid, formid);
            }

        }

        HashMap<String, Object> workflowWithFieldMap = new HashMap<>();
        HashMap<String, Object> workflowWithDetailTableMap = new HashMap<>();
        //处理新表单字段信息
        HashSet<String> newTableFormid = new HashSet<>(workflowIdSetFor1.values());

        if (!newTableFormid.isEmpty()) {
            QueryWrapper<WorkflowBillfield> newTableFieldQueryWrapper = new QueryWrapper<>();
            newTableFieldQueryWrapper.lambda().in(WorkflowBillfield::getBillid, newTableFormid);
            List<WorkflowBillfield> workflowBillfields = workflowBillfieldService.list(newTableFieldQueryWrapper);
            QueryWrapper<WorkflowBilldetailtable> newTableQueryWrapper = new QueryWrapper<>();
            newTableQueryWrapper.lambda().in(WorkflowBilldetailtable::getBillid, newTableFormid);
            List<WorkflowBilldetailtable> workflowBilldetailtables = workflowBilldetailtableService.list(newTableQueryWrapper);

            workflowIdSetFor1.forEach((workflowid, formid) -> {
                HashMap<String, Object> fieldMap = new HashMap<>();
                workflowBillfields.stream().filter(x -> formid.equals(Util.null2String(x.getBillid()))).forEach(workflowBillfield -> {
                    fieldMap.put(Util.null2String(workflowBillfield.getId()), new HashMap<String, Object>() {{
                        put("e10_field_id", workflowBillfield.getE10Id());
                        put("e10_form_id", workflowBillfield.getE10FormId());
                        WorkflowBilldetailtable workflowBilldetailtable = workflowBilldetailtables.stream().filter(x -> workflowBillfield.getDetailtable() != null && workflowBillfield.getDetailtable().equals(x.getTablename())).findAny().orElse(null);
                        put("e10_sub_form_id", workflowBilldetailtable != null ? workflowBilldetailtable.getE10Id() : "");
                    }});
                });
                workflowWithFieldMap.put(workflowid, fieldMap);

                HashMap<String, Object> detailTableMap = new HashMap<>();
                workflowBilldetailtables.stream().filter(x -> formid.equals(Util.null2String(x.getBillid()))).forEach(workflowBilldetailtable -> {
                    detailTableMap.put(Util.null2String(workflowBilldetailtable.getOrderid()), workflowBilldetailtable.getE10Id());
                });
                workflowWithDetailTableMap.put(workflowid, detailTableMap);
            });
        }

        //处理老表单字段信息
        HashSet<String> oldTableFormid = new HashSet<>(workflowIdSetFor0.values());

        if (!oldTableFormid.isEmpty()) {
            QueryWrapper<WorkflowFormfield> oldTableQueryWrapper = new QueryWrapper<>();
            oldTableQueryWrapper.lambda().in(WorkflowFormfield::getFormid, oldTableFormid);
            List<WorkflowFormfield> workflowFormfields = workflowFormfieldService.list(oldTableQueryWrapper);
            workflowIdSetFor0.forEach((workflowid, formid) -> {
                HashMap<String, Object> fieldMap = new HashMap<>();
                HashMap<String, Object> detailTableMap = new HashMap<>();
                workflowFormfields.stream().filter(x -> formid.equals(Util.null2String(x.getFormid()))).forEach(workflowFormfield -> {
                    fieldMap.put(Util.null2String(workflowFormfield.getFieldid()), new HashMap<String, Object>() {{
                        put("e10_field_id", workflowFormfield.getE10Id());
                        put("e10_form_id", workflowFormfield.getE10FormId());
                        put("e10_sub_form_id", workflowFormfield.getE10SubFormId());
                    }});

                    if (!"".equals(workflowFormfield.getE10SubFormId())) {
                        detailTableMap.put(Util.null2String(workflowFormfield.getGroupid() + 1), workflowFormfield.getE10SubFormId());
                    }
                });
                workflowWithFieldMap.put(workflowid, fieldMap);
                workflowWithDetailTableMap.put(workflowid, detailTableMap);
            });
        }

        result.put("list", values);
        result.put("total", results.getTotal());
        result.put("fields", workflowWithFieldMap);
        result.put("detailTables", workflowWithDetailTableMap);

        return result;
    }

    @Override
    public HashMap<String, Object> getWorkflowJsCodeByPageNew(int page, int perPage, String workflowName, String fromModule, boolean isAll) {

        HashMap<String, Object> result = new HashMap<>();
        List<JavascriptTrans> datas = new ArrayList<>();

        //如果没有数据，要进行初始化
        if (javascriptTransService.count(new QueryWrapper<JavascriptTrans>().lambda().eq(JavascriptTrans::getFrommodule, "workflow")) < 1)
            initAll("workflow", false, workflowName);//true的时候会清空数据
        if (javascriptTransService.count(new QueryWrapper<JavascriptTrans>().lambda().eq(JavascriptTrans::getFrommodule, "cube")) < 1)
            initAll("cube", false, workflowName);//true的时候会清空数据

        QueryWrapper<JavascriptTrans> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().and(wrapper -> wrapper.isNull(JavascriptTrans::getIsactive));
        if (fromModule != null && !fromModule.isEmpty())
            queryWrapper.lambda().eq(JavascriptTrans::getFrommodule, fromModule);
        if (workflowName != null && !workflowName.isEmpty())
            queryWrapper.lambda().like(JavascriptTrans::getWorkflowname, workflowName);
        queryWrapper.lambda()
                .orderBy(true, false, JavascriptTrans::getFrommodule)  // 按照 fromModule 降序排序
                .orderBy(true, true, JavascriptTrans::getWorkflowid)  // 按照 workflowid 升序排序
                .orderBy(true, true, JavascriptTrans::getType)       // 按照 type 升序排序
                .orderBy(true, true, JavascriptTrans::getId);       // 按照 ID 升序排序
        long total = 0;
        if (isAll) {
            datas = javascriptTransService.list(queryWrapper);
            total = datas.size();
        } else {
            Page<JavascriptTrans> pageCom = new Page<>(page, perPage);
            Page<JavascriptTrans> records = javascriptTransService.page(pageCom, queryWrapper);
            datas = records.getRecords();
            total = records.getTotal();
        }

        getWorkflowResult(datas, result);

        result.put("total", total);

        return result;
    }

    @Override
    public HashMap<String, Object> getWorkflowJsCodeByPageNewWithOutTrans(int page, int perPage, String workflowName, String fromModule) {
        HashMap<String, Object> result = new HashMap<>();
        List<JavascriptTrans> datas = new ArrayList<>();

        Page<JavascriptTrans> pageInfo = new Page<>(page, perPage);
        //获取未转换的数据
        QueryWrapper<JavascriptTrans> queryWrapper = new QueryWrapper<>();
//        queryWrapper.lambda().and(wrapper -> wrapper.isNull(JavascriptTrans::getTranfercontent).or().eq(JavascriptTrans::getTranfercontent, ""));
        queryWrapper.lambda().and(wrapper -> wrapper.isNull(JavascriptTrans::getTranfercontent));
        queryWrapper.lambda().and(wrapper -> wrapper.isNull(JavascriptTrans::getIsactive));
        if (fromModule != null && !fromModule.isEmpty())
            queryWrapper.lambda().eq(JavascriptTrans::getFrommodule, fromModule);
        if (workflowName != null && !workflowName.isEmpty())
            queryWrapper.lambda()
                    .like(JavascriptTrans::getWorkflowname, workflowName);

        Page<JavascriptTrans> pageDatas = javascriptTransService.page(pageInfo, queryWrapper);
        datas = pageDatas.getRecords();

        long total = pageDatas.getTotal();

        getWorkflowResult(datas, result);

        result.put("total", total);

        return result;
    }

    public String getNodeName(String nodeid) {
        WorkflowNodebase workflowNodebase = workflowNodebaseService.getById(nodeid);
        String nodename = "";
        if (workflowNodebase != null) {
            nodename = workflowNodebase.getNodename();
        }
        return Util.formatMultiLang(nodename, "7");
    }

    public String getFormName(String isbill, String formid) {
        String formname = "";
        if ("0".equals(isbill)) {
            //老表单
            WorkflowFormbase workflowFormbase = workflowFormbaseService.getById(formid);
            if (workflowFormbase != null) formname = Util.null2String(workflowFormbase.getFormname());
        } else {
            //新表单
            WorkflowBill workflowBill = workflowBillService.getById(formid);
            if (workflowBill != null) formname = SystemEnv.getHtmlLabelName(workflowBill.getNamelabel(), 7);
        }

        return Util.formatMultiLang(formname);
    }

    /**
     * 初始化流程模版js到表中
     *
     * @param isForce 是否强制初始化，强制会先删除然后全量重新生成，false只同步没有生成的数据
     */
    public void initAll(String fromModule, boolean isForce, String workflowName) {
        List<Map<String, Object>> records = new ArrayList<>();
        if ("workflow".equals(fromModule)) {
            //获取所有有效流程的流程模版
            IPage<Map<String, Object>> results = workflowNodehtmllayoutService.selectNodeHtmlLayoutByWorkflowIdWithPage(1, 10, InitWeaverServer.oaversion.startsWith("9"), workflowName, true);

            records = results.getRecords();

        } else if ("cube".equals(fromModule)) {
            //获取建模模版
            IPage<Map<String, Object>> results = modehtmllayoutService.selectLayoutByModeIdWithPage(1, 10, InitWeaverServer.oaversion.startsWith("9"), workflowName, true);

            records = results.getRecords();
        }
        if (isForce) {
            QueryWrapper<JavascriptTrans> deleteWrapper = new QueryWrapper<>();
            deleteWrapper.lambda().eq(JavascriptTrans::getFrommodule, fromModule);
            if (workflowName != null && !workflowName.isEmpty()) {
                deleteWrapper.lambda().like(JavascriptTrans::getWorkflowname, workflowName);
            }
            javascriptTransService.remove(deleteWrapper);
        }
        List<JavascriptTrans> javascriptTransList = new ArrayList<>();
        for (Map<String, Object> record : records) {
            String workflowid = Util.null2String(record.get("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(record.get("workflowname")), "7");
            String isbill = Util.null2String(record.get("isbill"));//建模默认是1，流程分为老表单和新表单

            String layoutid = Util.null2String(record.get("layoutid"));
            String formid = Util.null2String(record.get("formid"));
            String nodeid = Util.null2String(record.get("nodeid"));//流程是节点id，建模是应用id
            String nodename = "";
            if ("workflow".equals(fromModule)) {
                nodename = getNodeName(nodeid);
            } else if ("cube".equals(fromModule)) {
                nodename = Util.formatMultiLang(Util.null2String(record.get("nodename")), "7");
            }
            String scriptstr = Util.null2String(ClobTypeHandler.clobToString(record.get("scriptstr")));
            int type = Util.getIntValue(Util.null2String(record.get("type")));
            String typeName = "";
            if ("workflow".equals(fromModule)) {
                typeName = "显示模版";
                switch (type) {
                    case 0:
                        typeName = "显示模版";
                        break;
                    case 1:
                        typeName = "打印模版";
                        break;
                    case 2:
                        typeName = "移动模版";
                        break;
                }
            } else if ("cube".equals(fromModule)) {
                typeName = "显示布局";
                switch (type) {
                    case 0:
                        typeName = "显示布局";
                        break;
                    case 1:
                        typeName = "新建布局";
                        break;
                    case 2:
                        typeName = "编辑布局";
                        break;
                    case 3:
                        typeName = "监控布局";
                        break;
                    case 4:
                        typeName = "打印布局";
                        break;
                }
            }
            String layoutName = Util.formatMultiLang(Util.null2String(record.get("layoutname")));

            //脚本中包含src属性的script标签
            boolean hasSrcScript = false;
            //排除注释后有代码
            boolean hashScriptCode = false;
            //脚本中包含style标签 或者引入了 link 标签
            boolean hasCssStyle = false;
            String deScriptstr = E10CubeUtil.decodeStr(scriptstr);


            try {
                Element body = Jsoup.parse(deScriptstr);
                Elements scripts = body.select("script");
                for (int i = 0; i < scripts.size(); i++) {
                    Element script = scripts.get(i);
                    if (script != null) {
                        String str = E10CubeUtil.removeComments(script.data().trim());
                        if (StringUtils.isNotBlank(str)) {
                            hashScriptCode = true;
                        }
                        String src = Util.null2String(script.attr("src"));
                        if (!"".equals(src)) {
                            hasSrcScript = true;
                            break;
                        }
                    }
                }

                Elements styles = body.select("style");
                for (int i = 0; i < styles.size(); i++) {
                    Element style = styles.get(i);
                    if (style != null) {
                        String str = E10CubeUtil.removeComments(style.data().trim());
                        if (StringUtils.isNotBlank(str)) {
                            hasCssStyle = true;
                            break;
                        }
                    }
                }

                Elements links = body.select("link");
                if (links.size() > 0) hasCssStyle = true;
            } catch (Exception e) {
                new weaver.general.BaseBean().writeLog("", e);
            }

            //去掉注释后是否还存在js代码，不存在的无需处理
            if (!hashScriptCode && !hasSrcScript) {
                continue;
            }

            String deScriptWithoutTag = E10CubeUtil.getScript(deScriptstr);
            if (deScriptWithoutTag.trim().isEmpty()) {
                continue;
            }

            if (!isForce) {
                JavascriptTrans oldJsTrans = javascriptTransService.getOne(
                        new QueryWrapper<JavascriptTrans>().lambda()
                                .eq(JavascriptTrans::getLayoutid, layoutid)
                                .eq(JavascriptTrans::getFrommodule, fromModule));
                if (oldJsTrans != null) continue;
            }

            JavascriptTrans jsTrans = new JavascriptTrans();
            jsTrans.setLayoutid(Util.getIntValue(layoutid));
            jsTrans.setFrommodule(fromModule);
            jsTrans.setWorkflowid(Util.getIntValue(workflowid));
            jsTrans.setWorkflowname(workflowname);
            jsTrans.setFormid(Util.getIntValue(formid));
            jsTrans.setFormname(getFormName(isbill, formid));
            jsTrans.setIsbill(Util.getIntValue(isbill));
            jsTrans.setNodeid(Util.getIntValue(nodeid));
            jsTrans.setNodename(nodename);
            jsTrans.setType(Util.getIntValue(type));
            jsTrans.setTypename(typeName);
            jsTrans.setLayoutname(layoutName);
            jsTrans.setScriptstr(deScriptWithoutTag);
            jsTrans.setTranfercontent(null);
            jsTrans.setTransferflag("未转换");
            jsTrans.setHascssstyle(hasCssStyle ? "是" : "否");//是否包含CSS样式
            jsTrans.setHassrcscript(hasSrcScript ? "是" : "否");//是否包含js链接

            javascriptTransList.add(jsTrans);
        }

        try {
            //批量新建或者更新
            javascriptTransService.saveOrUpdateBatch(javascriptTransList, 50);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("批量更新失败，开始逐条更新");
            //批量失败
            for (JavascriptTrans jsTrans : javascriptTransList) {
                javascriptTransService.saveOrUpdate(jsTrans);
            }
        }

        try {
            //刷新数据，将代码带相同的根据formid、isbill以及scriptstr来划分
            refreshData();
        } catch (Exception ex) {

        }

    }

    /**
     * 初始化流程模版js到表中
     *
     * @param jsTrans 流程模版js对象
     */
    public boolean initTranfercontent(JavascriptTransDTO jsTrans) {
        boolean success = false;
        try {
            //更新数据
            if (jsTrans.getId() != null && jsTrans.getId() > 0) {
                javascriptTransMapper.updateJsTrans(jsTrans.trans());
            }
            success = true;
        } catch (Exception ex) {
            System.out.println("===================initTranfercontent-报错了");
            // 处理异常
            ex.printStackTrace();
        }
        return success;

    }

    /**
     * 初始化流程模版js到表中
     *
     * @param jsTransList 流程模版js对象
     */
    public boolean initTranfercontentBatch(List<JavascriptTransDTO> jsTransList) {
        boolean success = false;
        System.out.println("===================initTranfercontentBatch-start");
        try {
            // 更新数据
            int batchSize = 50; // 每次处理的批量大小
            int totalSize = jsTransList.size();
            ArrayList<JavascriptTrans> sources = new ArrayList<>();
            for (int i = 0; i < totalSize; i += batchSize) {
                System.out.println("===================initTranfercontentBatch-totalSize:" + totalSize);
                List<JavascriptTransDTO> batchList = jsTransList.subList(i, Math.min(i + batchSize, totalSize));

                HashMap<String, Object> sourceTmp = new HashMap<>();
                for (JavascriptTransDTO jsTrans : batchList) {
                    if (jsTrans.getId() != null && jsTrans.getId() > 0) {
                        System.out.println("===================initTranfercontentBatch-jsTrans-start:" + jsTrans);
                        sources.add(jsTrans.trans());

                        javascriptTransMapper.updateJsTrans(jsTrans.trans());
                        System.out.println("===================initTranfercontentBatch-jsTrans-end:");
                    }
                }
                if (!sources.isEmpty()) {
                    //mybatisplus批量保存有问题呀，目前先跳过，后续再看！
//                    javascriptTransMapper.updateBatchField(sources);
                }
            }
            success = true;
        } catch (Exception ex) {
            System.out.println("===================initTranfercontentBatch-报错了");
            // 处理异常
            ex.printStackTrace();
        }
        System.out.println("===================initTranfercontentBatch-end");
        return success;
    }

    public void exportAll(OutputStream outputStream, String fromModule, String workflowName) {
        try (Workbook workbook = new XSSFWorkbook()) {
            QueryWrapper<JavascriptTrans> queryWrapper = new QueryWrapper<>();
            if (fromModule != null && !fromModule.isEmpty())
                queryWrapper.lambda().like(JavascriptTrans::getFrommodule, fromModule);
            //fromModule 为空的时候，全部导出，不去管是否有workflowName
            if (workflowName != null && !workflowName.isEmpty())
                queryWrapper.lambda().like(JavascriptTrans::getWorkflowname, workflowName);
            queryWrapper.lambda()
                    .orderBy(true, false, JavascriptTrans::getFrommodule)  // 按照 fromModule 降序排序
                    .orderBy(true, true, JavascriptTrans::getWorkflowid)  // 按照 workflowid 升序排序
                    .orderBy(true, true, JavascriptTrans::getType)       // 按照 type 升序排序
                    .orderBy(true, true, JavascriptTrans::getId);       // 按照 id 升序排序
            List<JavascriptTrans> jsscriptTransList = javascriptTransService.list(queryWrapper);

            // 创建单元格样式
            CellStyle style = workbook.createCellStyle();
            // 设置文本居左显示
            style.setAlignment(HorizontalAlignment.LEFT);
            style.setVerticalAlignment(VerticalAlignment.TOP);

            if ("workflow".equals(fromModule) || "".equals(fromModule)) {
                Sheet sheet = workbook.createSheet("工作流程");
                sheet.setColumnWidth(6, pixelsToCharacterUnits(500));
                sheet.setColumnWidth(7, pixelsToCharacterUnits(500));

                // 添加表头
                Row headerRow = sheet.createRow(0);
                // 创建批注
                Drawing drawing = sheet.createDrawingPatriarch();
                XSSFClientAnchor anchor = new XSSFClientAnchor();
                anchor.setCol1(0); // 批注开始的列（0开始）
                anchor.setCol2(5); // 批注结束的列
                anchor.setRow1(0); // 批注开始的行（0开始）
                anchor.setRow2(12); // 批注结束的行
                anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);

                String[] columns = {"数据ID", "工作流名称", "表单名称", "类型名称", "布局名称", "节点名称", "脚本内容", "转换后内容", "转换情况", "转换说明", "包含CSS样式", "相同转换"};
                for (int i = 0; i < columns.length; i++) {
                    Cell cell = headerRow.createCell(i);
                    cell.setCellValue(columns[i]);

                    if ("转换情况".equals(columns[i])) {
                        Comment comment = drawing.createCellComment(anchor);
                        comment.setString(new XSSFRichTextString("转换情况：\n1、存在JS引入--E9的代码块中存在引入的JS，引入的JS是无法通过转换工具进行转换的，此种类型下的代码块均需要手动调整；\n2、代码异常--E9代码块本身存在错误，此种代码块贴入转换工具会直接报错，无法转换；\n3、无法完全转换--E9的代码块转换后，无法直接使用，需要手动调整，此类型代码块框架已全部存在，减少手动设置工作量；\n4、已转换需确认--E9的代码块支持全部转换，转换后需要验证;"));
                        cell.setCellComment(comment);
                    }

                    if ("脚本内容".equals(columns[i])) {
                        Comment comment = drawing.createCellComment(anchor);
                        comment.setString(new XSSFRichTextString("JS代码段内容过长时，会自动切割成多行，使用时请注意手动合并代码！"));
                        cell.setCellComment(comment);
                    }
                }

                List<JavascriptTrans> filteredList = jsscriptTransList.stream()
                        .filter(item -> "workflow".equals(item.getFrommodule())) // 使用过滤条件来保留符合条件的元素
                        .collect(Collectors.toList()); // 将过滤后的元素收集到一个新的列表中

                // 添加数据
                int rowIndex = 1;
                for (JavascriptTrans javascriptTrans : filteredList) {

                    String longDataOriginal = Util.null2String(javascriptTrans.getScriptstr());
                    String longData = Util.null2String(javascriptTrans.getTranfercontent());
                    // 计算数据需要的行数
                    int rowCount = (int) Math.ceil((double) longData.length() / 32767);
                    rowCount = Math.max(rowCount, 1);

                    String isactive = Util.null2String(javascriptTrans.getIsactive());
                    if (!"".equals(isactive)) rowCount = 1;

                    // 创建剩余行并拆分数据
                    for (int i = 0; i < rowCount; i++) {
                        Row row = sheet.createRow(rowIndex++);
                        int startIndex = i * 32767;
                        int endIndex = (i + 1) * 32767;//转换后的一定比转换前的长
                        if (endIndex > longData.length()) {
                            endIndex = longData.length();
                        }
                        String rowData = longData.substring(startIndex, endIndex);

                        int endIndexOriginal = (i + 1) * 32767;
                        if (endIndexOriginal > longDataOriginal.length()) {
                            endIndexOriginal = longDataOriginal.length();
                        }
                        String rowDataOriginal = "";
                        if (startIndex <= longDataOriginal.length())
                            rowDataOriginal = longDataOriginal.substring(startIndex, endIndexOriginal);

                        row.createCell(0).setCellValue(Util.null2String(javascriptTrans.getId()));
                        row.createCell(1).setCellValue(Util.null2String(javascriptTrans.getWorkflowname()));
                        row.createCell(2).setCellValue(Util.null2String(javascriptTrans.getFormname()));
                        row.createCell(3).setCellValue(Util.null2String(javascriptTrans.getTypename()));
                        row.createCell(4).setCellValue(Util.null2String(javascriptTrans.getLayoutname()));
                        row.createCell(5).setCellValue(Util.null2String(javascriptTrans.getNodename()));
                        if ("".equals(isactive)) {
                            Cell rowDataOriginalCell = row.createCell(6);
                            rowDataOriginalCell.setCellStyle(style);
                            rowDataOriginalCell.setCellValue(Util.null2String(rowDataOriginal));
                            Cell rowDataCell = row.createCell(7);
                            rowDataCell.setCellStyle(style);
                            rowDataCell.setCellValue(Util.null2String(rowData));
                        } else {
                            row.createCell(6).setCellValue("参见数据ID：" + isactive + " 脚本内容");
                            row.createCell(7).setCellValue("参见数据ID：" + isactive + " 转换后内容");
                        }
                        row.createCell(8).setCellValue(Util.null2String(javascriptTrans.getTransferflag()));
                        row.createCell(9).setCellValue(Util.null2String(javascriptTrans.showTransferstatus()));
                        row.createCell(10).setCellValue(Util.null2String(javascriptTrans.getHascssstyle()));
                        row.createCell(11).setCellValue(isactive);
                    }

                    if (rowCount > 1) {
                        // 合并其他单元格
                        for (int i = 0; i < 10; i++) {
                            if (i == 6 || i == 7) continue;
                            sheet.addMergedRegion(new CellRangeAddress(rowIndex - rowCount, rowIndex - 1, i, i)); // 合并从第 i 行第 2 列到第 11 列的单元格
                        }
                    }
                }
            }

            if ("cube".equals(fromModule) || "".equals(fromModule)) {
                Sheet sheet = workbook.createSheet("表单建模");
                // 创建批注
                Drawing drawing = sheet.createDrawingPatriarch();
                XSSFClientAnchor anchor = new XSSFClientAnchor();
                anchor.setCol1(0); // 批注开始的列（0开始）
                anchor.setCol2(5); // 批注结束的列
                anchor.setRow1(0); // 批注开始的行（0开始）
                anchor.setRow2(12); // 批注结束的行
                anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);
                // 添加表头
                Row headerRow = sheet.createRow(0);
                sheet.setColumnWidth(6, pixelsToCharacterUnits(500));
                sheet.setColumnWidth(7, pixelsToCharacterUnits(500));
                String[] columns = {"数据ID", "模块名称", "表单名称", "类型名称", "布局名称", "应用名称", "脚本内容", "转换后内容", "转换情况", "转换说明", "包含CSS样式", "相同转换"};
                for (int i = 0; i < columns.length; i++) {
                    Cell cell = headerRow.createCell(i);
                    cell.setCellValue(columns[i]);

                    if ("转换情况".equals(columns[i])) {
                        Comment comment = drawing.createCellComment(anchor);
                        comment.setString(new XSSFRichTextString("转换情况：\n1、存在JS引入--E9的代码块中存在引入的JS，引入的JS是无法通过转换工具进行转换的，此种类型下的代码块均需要手动调整；\n2、代码异常--E9代码块本身存在错误，此种代码块贴入转换工具会直接报错，无法转换；\n3、无法完全转换--E9的代码块转换后，无法直接使用，需要手动调整，此类型代码块框架已全部存在，减少手动设置工作量；\n4、已转换需确认--E9的代码块支持全部转换，转换后需要验证;"));
                        cell.setCellComment(comment);
                    }

                    if ("脚本内容".equals(columns[i])) {
                        Comment comment = drawing.createCellComment(anchor);
                        comment.setString(new XSSFRichTextString("JS代码段内容过长时，会自动切割成多行，请注意手动合并代码！"));
                        cell.setCellComment(comment);
                    }
                }

                List<JavascriptTrans> filteredList = jsscriptTransList.stream()
                        .filter(item -> "cube".equals(item.getFrommodule())) // 使用过滤条件来保留符合条件的元素
                        .collect(Collectors.toList()); // 将过滤后的元素收集到一个新的列表中

                // 添加数据
                int rowIndex = 1;
                for (JavascriptTrans javascriptTrans : filteredList) {

                    String longDataOriginal = Util.null2String(javascriptTrans.getScriptstr());
                    String longData = Util.null2String(javascriptTrans.getTranfercontent());
                    // 计算数据需要的行数
                    int rowCount = (int) Math.ceil((double) longData.length() / 32767);
                    rowCount = Math.max(rowCount, 1);

                    String isactive = Util.null2String(javascriptTrans.getIsactive());
                    if (!"".equals(isactive)) rowCount = 1;

                    // 创建剩余行并拆分数据
                    for (int i = 0; i < rowCount; i++) {
                        Row row = sheet.createRow(rowIndex++);
                        int startIndex = i * 32767;
                        int endIndex = (i + 1) * 32767;//转换后的一定比转换前的长
                        if (endIndex > longData.length()) {
                            endIndex = longData.length();
                        }
                        String rowData = longData.substring(startIndex, endIndex);

                        int endIndexOriginal = (i + 1) * 32767;
                        if (endIndexOriginal > longDataOriginal.length()) {
                            endIndexOriginal = longDataOriginal.length();
                        }
                        String rowDataOriginal = "";
                        if (startIndex <= longDataOriginal.length())
                            rowDataOriginal = longDataOriginal.substring(startIndex, endIndexOriginal);

                        row.createCell(0).setCellValue(Util.null2String(javascriptTrans.getId()));
                        row.createCell(1).setCellValue(Util.null2String(javascriptTrans.getWorkflowname()));
                        row.createCell(2).setCellValue(Util.null2String(javascriptTrans.getFormname()));
                        row.createCell(3).setCellValue(Util.null2String(javascriptTrans.getTypename()));
                        row.createCell(4).setCellValue(Util.null2String(javascriptTrans.getLayoutname()));
                        row.createCell(5).setCellValue(Util.null2String(javascriptTrans.getNodename()));
                        if ("".equals(isactive)) {
                            Cell rowDataOriginalCell = row.createCell(6);
                            rowDataOriginalCell.setCellStyle(style);
                            rowDataOriginalCell.setCellValue(Util.null2String(rowDataOriginal));
                            Cell rowDataCell = row.createCell(7);
                            rowDataCell.setCellStyle(style);
                            rowDataCell.setCellValue(Util.null2String(rowData));
                        } else {
                            row.createCell(6).setCellValue("参见数据ID：" + isactive + " 脚本内容");
                            row.createCell(7).setCellValue("参见数据ID：" + isactive + " 转换后内容");
                        }
                        row.createCell(8).setCellValue(Util.null2String(javascriptTrans.getTransferflag()));
                        row.createCell(9).setCellValue(Util.null2String(javascriptTrans.showTransferstatus()));
                        row.createCell(10).setCellValue(Util.null2String(javascriptTrans.getHascssstyle()));
                        row.createCell(11).setCellValue(isactive);
                    }

                    if (rowCount > 1) {
                        //合并其他单元格
                        for (int i = 0; i < 10; i++) {
                            if (i == 6 || i == 7) continue;
                            sheet.addMergedRegion(new CellRangeAddress(rowIndex - rowCount, rowIndex - 1, i, i)); // 合并从第 i 行第 2 列到第 11 列的单元格
                        }
                    }

                }
            }

            workbook.write(outputStream);
        } catch (IOException e) {
            // 处理 IO 异常
            new weaver.general.BaseBean().writeLog("", e);
        }
    }

    /**
     * 将获取到的数据进行封装，返回给前端使用
     *
     * @param datas
     * @param result
     */
    private void getWorkflowResult(List<JavascriptTrans> datas, HashMap<String, Object> result) {

        List<JavascriptTransDTO> values = new ArrayList<JavascriptTransDTO>();

        HashMap<String, String> workflowIdSetFor0 = new HashMap<>();//老表单
        HashMap<String, String> workflowIdSetFor1 = new HashMap<>();//新表单
        HashMap<String, String> workflowIdSetForCube = new HashMap<>();//新表单
        datas.forEach(record -> {
            values.add(record.trans());

            //统计表单信息
            if ("0".equals(Util.null2String(record.getIsbill())) && "workflow".equals(record.getFrommodule())) {
                workflowIdSetFor0.put(Util.null2String(record.getWorkflowid()), Util.null2String(record.getFormid()));
            }
            if ("1".equals(Util.null2String(record.getIsbill())) && "workflow".equals(record.getFrommodule())) {
                workflowIdSetFor1.put(Util.null2String(record.getWorkflowid()), Util.null2String(record.getFormid()));
            }
            if ("1".equals(Util.null2String(record.getIsbill())) && "cube".equals(record.getFrommodule())) {
                workflowIdSetForCube.put(Util.null2String(record.getWorkflowid()), Util.null2String(record.getFormid()));
            }
        });

        HashMap<String, Object> workflowWithFieldMap = new HashMap<>();
        HashMap<String, Object> workflowWithDetailTableMap = new HashMap<>();
        HashMap<String, Object> cubeWithFieldMap = new HashMap<>();
        HashMap<String, Object> cubeWithDetailTableMap = new HashMap<>();
        //处理新表单字段信息,注意建模只有新表单的情况
        HashSet<String> newTableFormid = new HashSet<>(workflowIdSetFor1.values());

        if (!newTableFormid.isEmpty()) {
            QueryWrapper<WorkflowBillfield> newTableFieldQueryWrapper = new QueryWrapper<>();
            newTableFieldQueryWrapper.lambda().in(WorkflowBillfield::getBillid, newTableFormid);
            List<WorkflowBillfield> workflowBillfields = workflowBillfieldService.list(newTableFieldQueryWrapper);
            QueryWrapper<WorkflowBilldetailtable> newTableQueryWrapper = new QueryWrapper<>();
            newTableQueryWrapper.lambda().in(WorkflowBilldetailtable::getBillid, newTableFormid);
            List<WorkflowBilldetailtable> workflowBilldetailtables = workflowBilldetailtableService.list(newTableQueryWrapper);

            workflowIdSetFor1.forEach((workflowid, formid) -> {
                HashMap<String, Object> fieldMap = new HashMap<>();
                workflowBillfields.stream().filter(x -> formid.equals(Util.null2String(x.getBillid()))).forEach(workflowBillfield -> {
                    fieldMap.put(Util.null2String(workflowBillfield.getId()), new HashMap<String, Object>() {{
                        put("e10_field_id", workflowBillfield.getE10Id());
                        put("e10_form_id", workflowBillfield.getE10FormId());
                        WorkflowBilldetailtable workflowBilldetailtable = workflowBilldetailtables.stream().filter(x -> workflowBillfield.getDetailtable() != null && workflowBillfield.getDetailtable().equals(x.getTablename())).findAny().orElse(null);
                        put("e10_sub_form_id", workflowBilldetailtable != null ? workflowBilldetailtable.getE10Id() : "");
                    }});
                });
                workflowWithFieldMap.put(workflowid, fieldMap);

                HashMap<String, Object> detailTableMap = new HashMap<>();
                workflowBilldetailtables.stream().filter(x -> formid.equals(Util.null2String(x.getBillid()))).forEach(workflowBilldetailtable -> {
                    detailTableMap.put(Util.null2String(workflowBilldetailtable.getOrderid()), workflowBilldetailtable.getE10Id());
                });
                workflowWithDetailTableMap.put(workflowid, detailTableMap);
            });
        }

        //处理老表单字段信息
        HashSet<String> oldTableFormid = new HashSet<>(workflowIdSetFor0.values());

        if (!oldTableFormid.isEmpty()) {
            QueryWrapper<WorkflowFormfield> oldTableQueryWrapper = new QueryWrapper<>();
            oldTableQueryWrapper.lambda().in(WorkflowFormfield::getFormid, oldTableFormid);
            List<WorkflowFormfield> workflowFormfields = workflowFormfieldService.list(oldTableQueryWrapper);
            workflowIdSetFor0.forEach((workflowid, formid) -> {
                HashMap<String, Object> fieldMap = new HashMap<>();
                HashMap<String, Object> detailTableMap = new HashMap<>();
                workflowFormfields.stream().filter(x -> formid.equals(Util.null2String(x.getFormid()))).forEach(workflowFormfield -> {
                    fieldMap.put(Util.null2String(workflowFormfield.getFieldid()), new HashMap<String, Object>() {{
                        put("e10_field_id", workflowFormfield.getE10Id());
                        put("e10_form_id", workflowFormfield.getE10FormId());
                        put("e10_sub_form_id", workflowFormfield.getE10SubFormId());
                    }});

                    if (!"".equals(workflowFormfield.getE10SubFormId())) {
                        detailTableMap.put(Util.null2String(workflowFormfield.getGroupid() + 1), workflowFormfield.getE10SubFormId());
                    }
                });
                workflowWithFieldMap.put(workflowid, fieldMap);
                workflowWithDetailTableMap.put(workflowid, detailTableMap);
            });
        }


        //处理建模表单字段
        HashSet<String> newTableFormidForCube = new HashSet<>(workflowIdSetForCube.values());

        if (!newTableFormidForCube.isEmpty()) {
            QueryWrapper<WorkflowBillfield> newTableFieldQueryWrapper = new QueryWrapper<>();
            newTableFieldQueryWrapper.lambda().in(WorkflowBillfield::getBillid, newTableFormidForCube);
            List<WorkflowBillfield> workflowBillfields = workflowBillfieldService.list(newTableFieldQueryWrapper);
            QueryWrapper<WorkflowBilldetailtable> newTableQueryWrapper = new QueryWrapper<>();
            newTableQueryWrapper.lambda().in(WorkflowBilldetailtable::getBillid, newTableFormidForCube);
            List<WorkflowBilldetailtable> workflowBilldetailtables = workflowBilldetailtableService.list(newTableQueryWrapper);

            workflowIdSetForCube.forEach((workflowid, formid) -> {
                HashMap<String, Object> fieldMap = new HashMap<>();
                workflowBillfields.stream().filter(x -> formid.equals(Util.null2String(x.getBillid()))).forEach(workflowBillfield -> {
                    fieldMap.put(Util.null2String(workflowBillfield.getId()), new HashMap<String, Object>() {{
                        put("e10_field_id", workflowBillfield.getE10IdForCube(workflowid));
                        put("e10_form_id", workflowBillfield.getE10FormIdForCube(workflowid));
                        WorkflowBilldetailtable workflowBilldetailtable = workflowBilldetailtables.stream().filter(x -> workflowBillfield.getDetailtable() != null && workflowBillfield.getDetailtable().equals(x.getTablename())).findAny().orElse(null);
                        put("e10_sub_form_id", workflowBilldetailtable != null ? workflowBilldetailtable.getE10IdForCube(workflowid) : "");
                    }});
                });
                cubeWithFieldMap.put(workflowid, fieldMap);

                HashMap<String, Object> detailTableMap = new HashMap<>();
                workflowBilldetailtables.stream().filter(x -> formid.equals(Util.null2String(x.getBillid()))).forEach(workflowBilldetailtable -> {
                    detailTableMap.put(Util.null2String(workflowBilldetailtable.getOrderid()), workflowBilldetailtable.getE10IdForCube(workflowid));

                });
                cubeWithDetailTableMap.put(workflowid, detailTableMap);
            });
        }

        HashMap<String, Object> fields = new HashMap<>();
        fields.put("workflow", workflowWithFieldMap);
        fields.put("cube", cubeWithFieldMap);

        HashMap<String, Object> detailTables = new HashMap<>();
        detailTables.put("workflow", workflowWithDetailTableMap);
        detailTables.put("cube", cubeWithDetailTableMap);

        result.put("list", values);
        result.put("fields", fields);
        result.put("detailTables", detailTables);
        String cubeButtonId = TransUtil.getNewTableID("ebdf_button", "id", "mode_pageexpand", "1");
        result.put("cubeButton", cubeButtonId.substring(0, cubeButtonId.length() - 1)+"0");
    }

    /**
     * 将像素转换为字符单位
     *
     * @param pixels 像素值
     * @return 字符单位
     */
    private int pixelsToCharacterUnits(int pixels) {
        // 字体的标准宽度（假设为 8 个像素）
        int fontWidth = 8;
        // 每个字符单位对应的像素值（假设为 256）
        int charUnitsPerPixel = 256;
        // 计算字符单位
        return pixels * charUnitsPerPixel / fontWidth;
    }

    private static final int BATCH_SIZE = 1000;

    public void refreshData() {
        int currentPage = 1;
        int pageSize = BATCH_SIZE;

        while (true) {
            IPage<JavascriptTrans> page = new Page<>(currentPage, pageSize);
            IPage<JavascriptTrans> resultPage = javascriptTransMapper.selectPage(page, new QueryWrapper<>());

            if (resultPage.getRecords().isEmpty()) {
                break; // 没有更多数据了，退出循环
            }

            // 按 formid 和 isbill 分组处理
            Map<String, List<JavascriptTrans>> groupedByFormIdAndIsBill = resultPage.getRecords().stream()
                    .collect(Collectors.groupingBy(record -> record.getFormid() + "-" + record.getIsbill()));

            for (Map.Entry<String, List<JavascriptTrans>> formGroup : groupedByFormIdAndIsBill.entrySet()) {
                List<JavascriptTrans> formGroupRecords = formGroup.getValue();

                // 按 scriptstr 分组，找出每组中最小的 id
                Map<String, Integer> minIdMap = formGroupRecords.stream()
                        .collect(Collectors.groupingBy(JavascriptTrans::getScriptstr,
                                Collectors.collectingAndThen(Collectors.minBy(Comparator.comparingInt(JavascriptTrans::getId)),
                                        optional -> optional.map(JavascriptTrans::getId).orElse(null))));

                // 批量更新 isactive 字段
                for (Map.Entry<String, Integer> entry : minIdMap.entrySet()) {
                    String scriptstr = entry.getKey();
                    Integer minId = entry.getValue();

                    // 获取需要更新的记录
                    List<Integer> idsToUpdate = formGroupRecords.stream()
                            .filter(record -> record.getScriptstr().equals(scriptstr) && !record.getId().equals(minId))
                            .map(JavascriptTrans::getId)
                            .collect(Collectors.toList());

                    if (!idsToUpdate.isEmpty()) {
                        // 批量更新 isactive 字段
                        UpdateWrapper<JavascriptTrans> updateWrapper = new UpdateWrapper<>();
                        updateWrapper.in("id", idsToUpdate);
                        updateWrapper.set("isactive", minId);

                        javascriptTransMapper.update(null, updateWrapper);
                    }
                }
            }

            currentPage++;
        }
    }


}
