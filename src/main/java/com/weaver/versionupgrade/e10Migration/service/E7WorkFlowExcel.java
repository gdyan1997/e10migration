package com.weaver.versionupgrade.e10Migration.service;

import com.weaver.version.upgrade.util.sql.util.TransUtil;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.hrm.company.SubCompanyComInfo;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

/**
 * @author wujiahao
 * @date 2024/4/3 13:59
 */
public class E7WorkFlowExcel {
    public static void getE7WorkflowActionSet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {


        //获取配置文件路径
        String rootPath = ConfigUtil.getConfigDir();
        System.out.println("rootPath=" + rootPath);
       int indexOf = rootPath.indexOf("e10Migration");
        String substring = rootPath.substring(0, indexOf);
        String actionfilepath = substring + "ecology" + File.separator + "WEB-INF" + File.separator + "service" + File.separator + "action.xml";
        //  String actionfilepath = rootPath + "ecology" + File.separator + "WEB-INF" + File.separator + "service" + File.separator + "action.xml";
        File inputFile = new File(actionfilepath);
        // 创建 DocumentBuilderFactory 实例
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        // 创建 DocumentBuilder 实例
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        // 使用 DocumentBuilder 解析 XML 文件并返回 Document 对象
        Document doc = dBuilder.parse(inputFile);
        Map<String, String> actionMap = new HashMap<>();
        // 获取XML文档中所有名为 "service-point" 的元素
        NodeList nodeList = doc.getElementsByTagName("service-point");
        // 遍历每个 "service-point" 元素
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                // 获取 "service-point" 元素的 id 属性
                String id = element.getAttribute("id");
                // 获取 "invoke-factory" 元素下的 "construct" 元素的 "class" 属性
                Element constructElement = (Element) element.getElementsByTagName("construct").item(0);
                String className = constructElement.getAttribute("class");
                actionMap.put(id, className);
            }
        }


        List<List<String>> valuese7 = new ArrayList<List<String>>();


        SourceDBUtil sourceDBUtil = new SourceDBUtil();

        sourceDBUtil.execute("select t2.WORKFLOWNAME,t2.SUBCOMPANYID,TYPE,CUSTOMERVALUE,t3.nodename,CUSTOMERVALUE,isnode,t4.LINKNAME,t2.id as id\n" +
                "from workflow_addinoperate t1\n" +
                "         left join workflow_base t2 on t1.WORKFLOWID = t2.id\n" +
                "         left join workflow_nodebase t3 on t1.OBJID = t3.id\n" +
                "         left join workflow_nodelink t4 on t1.OBJID = t4.id\n" +
                "where type in (2, 3)   order by t1.WORKFLOWID");
        while (sourceDBUtil.next()) {
            List<String> list = new ArrayList<>();


            String workflowname = TransUtil.null2String( sourceDBUtil.getString("WORKFLOWNAME"));
            String isnode = TransUtil.null2String( sourceDBUtil.getString("isnode"));
            String SUBCOMPANYID = TransUtil.null2String( sourceDBUtil.getString("SUBCOMPANYID"));

            String linkname = TransUtil.null2String( sourceDBUtil.getString("LINKNAME"));
            String workflowid = TransUtil.null2String( sourceDBUtil.getString("id"));



            String type = TransUtil.null2String( sourceDBUtil.getString("TYPE"));
            String CUSTOMERVALUE = TransUtil.null2String( sourceDBUtil.getString("CUSTOMERVALUE"));

            if (CUSTOMERVALUE.contains(".")){
                CUSTOMERVALUE=  CUSTOMERVALUE.split("\\.")[1];
            }

            String nodename = TransUtil.null2String( sourceDBUtil.getString("nodename"));

            list.add(workflowname+"("+workflowid+")");//流程名称

            SubCompanyComInfo subCompanyComInfo=new SubCompanyComInfo();
            list.add(subCompanyComInfo.getSubCompanyname(SUBCOMPANYID).equals("")?"总部":subCompanyComInfo.getSubCompanyname(SUBCOMPANYID));//所属分部
            list.add(TransUtil.null2String( sourceDBUtil.getString("CUSTOMERVALUE")));//动作名称

            if (isnode.equals("1")){
                list.add("节点");//节点/出口
                list.add(nodename);//节点/出口名称
            }else {
                list.add("出口");//动作名称
                list.add(linkname);//节点/出口名称
            }

            list.add("启用");//是否启用

            list.add(actionMap.get(CUSTOMERVALUE));//class路径
            list.add("action");//接口类型
            valuese7.add(list);
        }

        List<String> formdata = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "class路径", "接口类型"));


        addExcelModal(writeEntity, "E7流程节点操作", "E7流程节点附加操作", formdata, valuese7);

    }
    public static void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


}
