package com.weaver.versionupgrade.e10Migration.service;

import com.weaver.versionupgrade.entity.dto.JavascriptTransDTO;
import com.weaver.versionupgrade.entity.entity.JavascriptTrans;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/27
 * Time: 10:17
 * Description:
 **/
public interface GetWorkflowJsCodeService {
    HashMap<String, Object> getWorkflowJsCodeByPage(int page, int perPage, String info, boolean isAll);

    HashMap<String, Object> getWorkflowJsCodeByPageNew(int page, int perPage, String info, String fromModule, boolean isAll);

    HashMap<String, Object> getWorkflowJsCodeByPageNewWithOutTrans(int page, int perPage, String info, String fromModule);

    void initAll(String fromModule, boolean isForce, String workflowName);

    boolean initTranfercontent(JavascriptTransDTO jsTrans);

    boolean initTranfercontentBatch(List<JavascriptTransDTO> jsTransList);

    void exportAll(OutputStream outputStream, String fromModule, String workflowName);
	
    void refreshData();
}
