package com.weaver.versionupgrade.e10Migration.service;

import com.weaver.version.upgrade.util.sql.SQLUtil;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import com.weaver.versionupgrade.e10Migration.modules.all.ExportAllDataExcel;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import weaver.conn.RecordSet;
import weaver.e10Migration.excel.file.ExcelWrite;
import weaver.e10Migration.excel.file.bean.ExcelFunctionBean;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.Util;
import weaver.hrm.company.SubCompanyComInfo;
import weaver.init.InitWeaverServer;
import weaver.systeminfo.SystemEnv;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;
import java.util.function.Consumer;

import static com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil.decodeStr;
import static com.weaver.versionupgrade.e10Migration.service.impl.GetWorkflowJsCodeServiceImp.*;
import static weaver.e10Migration.excel.file.ExcelWrite.addExcelModal;

/**
 * @author wujiahao
 * @date 2024/4/10 17:44
 */
public class ExportWorkflowExcel {

    String[] STANDARACTION = new String[]{
            "com.alitriph5.action.SetApplyAction", "com.alitriph5.action.SetApplyStatusAction", "com.alitriph5.action.SetIndoorApplyAction",
            "com.alitriph5.action.SetIndoorApplyStatusAction", "com.customization.workflow.action.DocCommonAction", "com.dttrip.action.TravelApplicationSyncAction",
            "com.engine.cpt.systemBill.Bill14_AfterAction", "com.engine.cpt.systemBill.Bill18_AfterAction", "com.engine.cpt.systemBill.Bill19_AfterAction",
            "com.engine.cpt.systemBill.Bill201_AfterAction", "com.engine.cpt.systemBill.Bill220_AfterAction", "com.engine.cpt.systemBill.Bill221_AfterAction",
            "com.engine.cpt.systemBill.Bill222_AfterAction", "com.engine.cpt.systemBill.Bill224_AfterAction", "com.engine.cpt.systemBill.Bill226_AfterAction",
            "com.engine.crm.biz.systemBill.SystemBill49_PreAction", "com.engine.crm.biz.systemBill.SystemBill79_AfterAction",
            "com.engine.doc.cmd.systembill.SystemBill28AfterAction", "com.engine.doc.cmd.systembill.Systembill200AfterAction",
            "com.engine.doc.cmd.systembill.Systembill241AfterAction", "com.engine.fna.systemBill.Bill13_AfterAction", "com.engine.fna.systemBill.Bill154_AfterAction",
            "com.engine.fna.systemBill.Bill156_AfterAction", "com.engine.fna.systemBill.Bill157_AfterAction", "com.engine.fna.systemBill.Bill158_AfterAction",
            "com.engine.fna.systemBill.Bill159_AfterAction", "com.engine.fna.systemBill.Bill7_AfterAction", "com.engine.fnaMulDimensions.biz.action.FnaApprovalBudget",
            "com.engine.fnaMulDimensions.biz.action.FnaBudgetCheck", "com.engine.fnaMulDimensions.biz.action.FnaDeductBudget",
            "com.engine.fnaMulDimensions.biz.action.FnaEffectChangeBudget", "com.engine.fnaMulDimensions.biz.action.FnaFreezeBudget",
            "com.engine.fnaMulDimensions.biz.action.FnaReleaseBudget", "com.engine.fnaMulDimensions.biz.action.FnaSynModeDataBizAction",
            "com.engine.fnaMulDimensions.biz.action.FnaprojectApprovalBackBudget", "com.engine.fnaMulDimensions.biz.action.FnaprojectApprovalBudget",
            "com.engine.govern.interfaces.GovernAddAction", "com.engine.govern.interfaces.GovernAssignmentAction", "com.engine.govern.interfaces.GovernChangeAction",
            "com.engine.govern.interfaces.GovernDecomposeAction", "com.engine.govern.interfaces.GovernDelayAction", "com.engine.govern.interfaces.GovernDocumentService",
            "com.engine.govern.interfaces.GovernEndAction", "com.engine.govern.interfaces.GovernPromptAction", "com.engine.govern.interfaces.GovernReferralAction",
            "com.engine.govern.interfaces.GovernReportAction", "com.engine.hrm.biz.SystemBill180Formal_AfterAction", "com.engine.hrm.biz.SystemBill180_AfterAction",
            "com.engine.info.interfaces.InfoJournalPassAction", "com.engine.info.interfaces.InfoJournalRefusalAction", "com.engine.info.interfaces.InfoPassAction",
            "com.engine.info.interfaces.InfoRefusalAction", "com.engine.kq.wfset.action.KqDeductionVacationAction", "com.engine.kq.wfset.action.KqFreezeVacationAction",
            "com.engine.kq.wfset.action.KqPaidLeaveAction", "com.engine.kq.wfset.action.KqReleaseVacationAction", "com.engine.kq.wfset.action.KqSplitAction",
            "com.engine.odoc.cmd.haoqian.HqGenerateSealDocAction", "com.engine.odoc.cmd.odocNumberManage.AutoGenerateOdocNumberAction",
            "com.engine.prj.systemBill.Bill152_AfterAction", "com.engine.prj.systemBill.Bill74_AfterAction", "com.engine.prj.wfactions.PrjStatusChangeAction",
            "com.engine.prj.wfactions.PrjTaskBreakAction", "com.engine.workflow.biz.systemBill.Demo_AfterAction", "com.engine.workflow.biz.systemBill.Demo_LinkAction",
            "com.engine.workflow.biz.systemBill.Demo_PreAction", "weaver.common.WeaverAction", "weaver.cpt.wfactions.CptApplyAction", "weaver.cpt.wfactions.CptApplyUseAction",
            "weaver.cpt.wfactions.CptBackAction", "weaver.cpt.wfactions.CptChangeAction", "weaver.cpt.wfactions.CptDiscardAction", "weaver.cpt.wfactions.CptFetchAction",
            "weaver.cpt.wfactions.CptLendAction", "weaver.cpt.wfactions.CptLossAction", "weaver.cpt.wfactions.CptMendAction", "weaver.cpt.wfactions.CptMoveAction",
            "weaver.formmode.cuspage.cpt.act.ComfirmCptStockInAction", "weaver.formmode.cuspage.cpt.act.CptStockInAction", "weaver.formmode.cuspage.cpt.act.CptUseInfoAction",
            "weaver.formmode.cuspage.cpt.wfactions.Mode4CptApplyAction", "weaver.formmode.cuspage.cpt.wfactions.Mode4CptBackAction",
            "weaver.formmode.cuspage.cpt.wfactions.Mode4CptDiscardAction", "weaver.formmode.cuspage.cpt.wfactions.Mode4CptFetchAction",
            "weaver.formmode.cuspage.cpt.wfactions.Mode4CptFrozenumAction", "weaver.formmode.cuspage.cpt.wfactions.Mode4CptLendAction",
            "weaver.formmode.cuspage.cpt.wfactions.Mode4CptLossAction", "weaver.formmode.cuspage.cpt.wfactions.Mode4CptMendAction",
            "weaver.formmode.cuspage.cpt.wfactions.Mode4CptMoveAction", "weaver.formmode.cuspage.cpt.wfactions.Mode4CptReleasenumAction",
            "weaver.formmode.esb.FormModeESBAction", "weaver.formmode.interfaces.action.BaseAction", "weaver.formmode.interfaces.action.SapAction",
            "weaver.formmode.interfaces.action.WebServiceAction", "weaver.formmode.interfaces.action.WorkflowToMode", "weaver.formmode.interfaces.action.fybxWorkflowToMode",
            "weaver.formmode.interfaces.action.fybxWorkflowToModeupdatefyxx", "weaver.formmode.interfaces.dmlaction.commands.actions.DMLAction",
            "weaver.hrm.attendance.action.HrmDeductionVacation4FormalAction", "weaver.hrm.attendance.action.HrmDeductionVacationAction",
            "weaver.hrm.attendance.action.HrmFreezeVacationAction", "weaver.hrm.attendance.action.HrmPaidLeaveAction", "weaver.hrm.attendance.action.HrmReleaseVacationAction",
            "weaver.hrm.attendance.action.VacationAction", "weaver.hrm.pm.action.HrmResourceDismissAction", "weaver.hrm.pm.action.HrmResourceEntrantAction",
            "weaver.hrm.pm.action.HrmResourceExtendAction", "weaver.hrm.pm.action.HrmResourceFireAction", "weaver.hrm.pm.action.HrmResourceHireAction",
            "weaver.hrm.pm.action.HrmResourceReHireAction", "weaver.hrm.pm.action.HrmResourceRedeployAction", "weaver.hrm.pm.action.HrmResourceRetireAction",
            "weaver.hrm.pm.action.HrmResourceTryAction", "weaver.hrm.pm.action.PmAction", "weaver.hrm.schedule.action.HrmScheduleShiftAction",
            "weaver.interfaces.workflow.action.ActionExtends", "weaver.interfaces.workflow.action.ActionTestRst", "weaver.interfaces.workflow.action.AmountControlAction",
            "weaver.interfaces.workflow.action.ArchiveDocument", "weaver.interfaces.workflow.action.BaseAction", "weaver.interfaces.workflow.action.CreateTraceDocument",
            "weaver.interfaces.workflow.action.CreateTraceDocumentByClear", "weaver.interfaces.workflow.action.ExchangeApprovalAgree",
            "weaver.interfaces.workflow.action.ExchangeApprovalDisagree", "weaver.interfaces.workflow.action.ExchangeSetValueAction",
            "weaver.interfaces.workflow.action.FnaAdvanceEffectNew", "weaver.interfaces.workflow.action.FnaAdvanceFreezeNew",
            "weaver.interfaces.workflow.action.FnaAdvanceReleaseFreezeNew", "weaver.interfaces.workflow.action.FnaAdvanceReleaseNew",
            "weaver.interfaces.workflow.action.FnaAdvanceReverseNew", "weaver.interfaces.workflow.action.FnaBorrowEffectNew", "weaver.interfaces.workflow.action.FnaBorrowFreezeNew",
            "weaver.interfaces.workflow.action.FnaBorrowReleaseFreezeNew", "weaver.interfaces.workflow.action.FnaBorrowReleaseNew", "weaver.interfaces.workflow.action.FnaBorrowReverseNew",
            "weaver.interfaces.workflow.action.FnaBudgetOverAction", "weaver.interfaces.workflow.action.FnaChangeEffectNew", "weaver.interfaces.workflow.action.FnaChangeFreezeNew",
            "weaver.interfaces.workflow.action.FnaChangeRejectNew", "weaver.interfaces.workflow.action.FnaCostStandardCtrlAction",
            "weaver.interfaces.workflow.action.FnaElecInvoiceClosureNew", "weaver.interfaces.workflow.action.FnaElecInvoiceInitNew",
            "weaver.interfaces.workflow.action.FnaElecInvoiceLockNew", "weaver.interfaces.workflow.action.FnaElectronicPdf4Approval",
            "weaver.interfaces.workflow.action.FnaElectronicPdf4Invoice", "weaver.interfaces.workflow.action.FnaElectronicPdf4Receipt",
            "weaver.interfaces.workflow.action.FnaElectronicPdf4Voucher", "weaver.interfaces.workflow.action.FnaSetCreaterInfoAction",
            "weaver.interfaces.workflow.action.FnaShareEffectNew", "weaver.interfaces.workflow.action.FnaShareFreezeNew", "weaver.interfaces.workflow.action.FnaShareRejectNew",
            "weaver.interfaces.workflow.action.NcReceiveAction", "weaver.interfaces.workflow.action.OdocCancelSignByKG", "weaver.interfaces.workflow.action.OdocSendDocToE8",
            "weaver.interfaces.workflow.action.OdocSignByKG", "weaver.interfaces.workflow.action.QiyuesuoMatterSupportToCoworkAction",
            "weaver.interfaces.workflow.action.SendMailToCreaterAction", "weaver.interfaces.workflow.action.WfFormHtmlToPdf", "weaver.interfaces.workflow.action.WorkflowFnaEffect",
            "weaver.interfaces.workflow.action.WorkflowFnaEffectNew", "weaver.interfaces.workflow.action.WorkflowFnaInWorkflow",
            "weaver.interfaces.workflow.action.WorkflowFnaInWorkflowNew", "weaver.interfaces.workflow.action.WorkflowFnaInvoiceLedger",
            "weaver.interfaces.workflow.action.WorkflowFnaReject", "weaver.interfaces.workflow.action.WorkflowFnaRejectNew",
            "weaver.interfaces.workflow.action.WorkflowToBankEnterpriseRunXml", "weaver.interfaces.workflow.action.WorkflowToDoc", "weaver.interfaces.workflow.action.WorkflowToFinance",
            "weaver.interfaces.workflow.action.WorkflowToFinanceRunXml", "weaver.interfaces.workflow.action.WorkflowToFinanceUrl",
            "weaver.interfaces.workflow.action.WorkflowToFinanceUrlDoc", "weaver.meeting.action.WFMeetingAction", "weaver.meeting.action.WFMeetingReceiptAction",
            "weaver.odoc.ofd.action.convertOfdAction", "weaver.proj.wfactions.PrjApproveAction", "weaver.proj.wfactions.PrjGenerateAction",
            "weaver.proj.wfactions.PrjTemplateApproveAction", "weaver.workflow.action.BaseAction", "weaver.workflow.action.ESBAction", "weaver.workflow.action.ESBNewAction",
            "weaver.workflow.action.SapAction", "weaver.workflow.action.WebServiceAction", "weaver.workflow.dmlaction.commands.actions.DMLAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckLocation4CompanyAcrossAction", "weaver.workflow.qiyuesuo.singleSeal.action.QYSSingleSignAction",
            "weaver.workflow.qiyuesuo.action.QYSAutoSign4CurrentOperatorAction", "weaver.workflow.qiyuesuo.action.QYSCreateWorkflowAuditContract",
            "weaver.workflow.qiyuesuo.action.QYSCheckContractAction", "weaver.workflow.qiyuesuo.action.QYSAutoSign4AcrossPageAction",
            "weaver.workflow.qiyuesuo.action.QYSRestoreFileAction", "weaver.workflow.qiyuesuo.action.QYSAddSignatoryAction",
            "weaver.workflow.qiyuesuo.action.QYSAutoSign4AllPersonalAction", "weaver.workflow.qiyuesuo.sealApply.action.QYSSealApplyCheckFileAndTemplateAction",
            "weaver.workflow.qiyuesuo.action.QYSAction", "weaver.workflow.qiyuesuo.action.QYSFillParamsAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSEditSealBasicAction", "weaver.workflow.qiyuesuo.action.QYSCheckLocation4CompanySealAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSEditSealAction", "weaver.workflow.qiyuesuo.action.QYSAutoSign4AllPlatformAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckSign4CompanyAction", "weaver.workflow.qiyuesuo.seal.action.QYSRevokeSealAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSCreateSealAction", "weaver.workflow.qiyuesuo.seal.action.QYSEnableSealAction",
            "weaver.workflow.qiyuesuo.action.QYSDownloadContractAction", "weaver.workflow.qiyuesuo.singleSeal.action.MustSignAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSSealChangeAction", "weaver.workflow.qiyuesuo.seal.action.QYSDeleteSealAction",
            "weaver.workflow.qiyuesuo.action.QYSValidateReceiverDocParamsAction", "weaver.workflow.qiyuesuo.seal.action.QYSUpdateSealImageAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckLocation4AllDocumentsAction", "weaver.workflow.qiyuesuo.seal.action.QYSDisableSealAction",
            "weaver.workflow.qiyuesuo.action.QYSAddSponsorDocumentAction", "weaver.workflow.qiyuesuo.seal.action.QYSCreateLPSealAction",
            "weaver.workflow.qiyuesuo.action.QYSAddCopiersAction", "weaver.workflow.qiyuesuo.action.QYSAutoSign4Personal",
            "weaver.workflow.qiyuesuo.seal.action.QYSEditSealUploadAction", "weaver.workflow.qiyuesuo.sealApply.action.QYSSealApplyAuthAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckCurrentOperatorSignAction", "weaver.workflow.qiyuesuo.action.QYSCheckAllSignatorySignAction",
            "weaver.workflow.qiyuesuo.action.QYSCencelContractAction", "weaver.workflow.qiyuesuo.action.QYSCheckLocation4PlatformSealAction",
            "weaver.workflow.qiyuesuo.action.QYSCreateContractAction", "weaver.workflow.qiyuesuo.sealApply.action.QYSSealUsedFileUploadAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSCreateElectronicSealAction", "weaver.workflow.qiyuesuo.action.QYSCheckLocation4PersonalAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSUpdateSealUserAction", "weaver.workflow.qiyuesuo.action.QYSCheckLocation4PlatformAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckSign4PersonalAction", "weaver.workflow.qiyuesuo.companyAuth.action.QYSCompanyAuthSendNotifyAction",
            "weaver.workflow.qiyuesuo.sealApply.action.QYSSealAddCountAction", "weaver.workflow.qiyuesuo.action.QYSCheckLocation4CompanyAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckLocation4CompanySealAction", "weaver.workflow.qiyuesuo.action.QYSSetDocumentPrintCountAction",
            "weaver.workflow.qiyuesuo.action.QYSSendAction", "weaver.workflow.qiyuesuo.action.QYSCompleteAction",
            "weaver.workflow.qiyuesuo.action.QYSCheckLocation4PlatformAcrossAction", "weaver.workflow.qiyuesuo.action.QYSCheckSign4PlatformAction",
            "weaver.workflow.qiyuesuo.action.QYSAuditAction", "weaver.workflow.qiyuesuo.seal.action.QYSCreatePhysicalSealAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSUpdateSealManagerAction", "weaver.workflow.qiyuesuo.action.QYSEditContractAction",
            "weaver.workflow.qiyuesuo.action.QYSAutoSign4PlatformAction", "weaver.workflow.qiyuesuo.action.QYSRecallContractAction",
            "weaver.workflow.qiyuesuo.action.QYSForceFinishContractAction", "weaver.workflow.qiyuesuo.action.QYSCheckFileAndTemplate",
            "weaver.workflow.qiyuesuo.companyAuth.action.QYSCompanyAuthDeleteAction", "weaver.workflow.qiyuesuo.action.QYSValidateDocParamsHasFinishedAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSEditSealAutoAction", "weaver.workflow.qiyuesuo.sealApply.action.QYSSealFinishAction",
            "weaver.workflow.qiyuesuo.seal.action.QYSEditBasicSealAction", "weaver.workflow.qiyuesuo.action.QYSCencelContractsAction",
            "weaver.contractmanagement.action.ContractManagementCoreAction"
    };//E9标准的action


    String[] FNA_STANDARACTION = new String[]{
            "com.engine.fna.systemBill.Bill13_AfterAction", "com.engine.fna.systemBill.Bill154_AfterAction",
            "com.engine.fna.systemBill.Bill156_AfterAction", "com.engine.fna.systemBill.Bill157_AfterAction", "com.engine.fna.systemBill.Bill158_AfterAction",
            "com.engine.fna.systemBill.Bill159_AfterAction", "com.engine.fna.systemBill.Bill7_AfterAction", "com.engine.fnaMulDimensions.biz.action.FnaApprovalBudget",
            "com.engine.fnaMulDimensions.biz.action.FnaBudgetCheck", "com.engine.fnaMulDimensions.biz.action.FnaDeductBudget",
            "com.engine.fnaMulDimensions.biz.action.FnaEffectChangeBudget", "com.engine.fnaMulDimensions.biz.action.FnaFreezeBudget",
            "com.engine.fnaMulDimensions.biz.action.FnaReleaseBudget", "com.engine.fnaMulDimensions.biz.action.FnaSynModeDataBizAction",
            "com.engine.fnaMulDimensions.biz.action.FnaprojectApprovalBackBudget", "com.engine.fnaMulDimensions.biz.action.FnaprojectApprovalBudget",
            "weaver.interfaces.workflow.action.FnaAdvanceEffectNew", "weaver.interfaces.workflow.action.FnaAdvanceFreezeNew",
            "weaver.interfaces.workflow.action.FnaAdvanceReleaseFreezeNew", "weaver.interfaces.workflow.action.FnaAdvanceReleaseNew",
            "weaver.interfaces.workflow.action.FnaAdvanceReverseNew", "weaver.interfaces.workflow.action.FnaBorrowEffectNew", "weaver.interfaces.workflow.action.FnaBorrowFreezeNew",
            "weaver.interfaces.workflow.action.FnaBorrowReleaseFreezeNew", "weaver.interfaces.workflow.action.FnaBorrowReleaseNew", "weaver.interfaces.workflow.action.FnaBorrowReverseNew",
            "weaver.interfaces.workflow.action.FnaBudgetOverAction", "weaver.interfaces.workflow.action.FnaChangeEffectNew", "weaver.interfaces.workflow.action.FnaChangeFreezeNew",
            "weaver.interfaces.workflow.action.FnaChangeRejectNew", "weaver.interfaces.workflow.action.FnaCostStandardCtrlAction",
            "weaver.interfaces.workflow.action.FnaElecInvoiceClosureNew", "weaver.interfaces.workflow.action.FnaElecInvoiceInitNew",
            "weaver.interfaces.workflow.action.FnaElecInvoiceLockNew", "weaver.interfaces.workflow.action.FnaElectronicPdf4Approval",
            "weaver.interfaces.workflow.action.FnaElectronicPdf4Invoice", "weaver.interfaces.workflow.action.FnaElectronicPdf4Receipt",
            "weaver.interfaces.workflow.action.FnaElectronicPdf4Voucher", "weaver.interfaces.workflow.action.FnaSetCreaterInfoAction",
            "weaver.interfaces.workflow.action.FnaShareEffectNew", "weaver.interfaces.workflow.action.FnaShareFreezeNew",
            "weaver.interfaces.workflow.action.FnaShareRejectNew", "weaver.interfaces.workflow.action.WorkflowFnaEffect",
            "weaver.interfaces.workflow.action.WorkflowFnaEffectNew", "weaver.interfaces.workflow.action.WorkflowFnaInWorkflow",
            "weaver.interfaces.workflow.action.WorkflowFnaInWorkflowNew", "weaver.interfaces.workflow.action.WorkflowFnaInvoiceLedger",
            "weaver.interfaces.workflow.action.WorkflowFnaReject", "weaver.interfaces.workflow.action.WorkflowFnaRejectNew",
    };//E9标准的action

    String[] HRM_STANDARACTION = new String[]{
            "weaver.hrm.pm.action.HrmResourceDismissAction", "weaver.hrm.pm.action.HrmResourceEntrantAction",
            "weaver.hrm.pm.action.HrmResourceExtendAction", "weaver.hrm.pm.action.HrmResourceFireAction", "weaver.hrm.pm.action.HrmResourceHireAction",
            "weaver.hrm.pm.action.HrmResourceReHireAction", "weaver.hrm.pm.action.HrmResourceRedeployAction", "weaver.hrm.pm.action.HrmResourceRetireAction",
            "weaver.hrm.pm.action.HrmResourceTryAction", "weaver.hrm.pm.action.PmAction"
    };//E9标准的action

    String[] CUSTOMPAGE = new String[]{
            "workflow/request/ext/GridExtProxy.jsp", "workflow/request/ext/GridJsonGet.jsp", "workflow/request/ext/GridSuperviseExtProxy.jsp", "workflow/request/ext/HrmCustomLeave.jsp",
            "workflow/request/ext/HrmCustomLeave4Mobile.jsp", "workflow/request/ext/HrmCustomLeave_e9.jsp", "workflow/request/ext/HrmCustomOverTimeWork.jsp", "workflow/request/ext/HrmCustomOverTimeWork4Mobile.jsp",
            "workflow/request/ext/HrmCustomOverTimeWork_e9.jsp", "workflow/request/ext/Request.jsp", "workflow/request/ext/TreeJsonGet.jsp", "workflow/request/ext/WorkflowRequestPictureProxy.jsp",
            "workflow/request/ext4e9/common.js", "workflow/request/ext4e9/HrmCustomCard_e9.jsp", "workflow/request/ext4e9/HrmCustomEvection_e9.jsp", "workflow/request/ext4e9/HrmCustomLeave_e9.jsp",
            "workflow/request/ext4e9/HrmCustomLeaveBack_e9.jsp", "workflow/request/ext4e9/HrmCustomOther_e9.jsp", "workflow/request/ext4e9/HrmCustomOut_e9.jsp", "workflow/request/ext4e9/HrmCustomOvertime_e9.jsp",
            "workflow/request/ext4e9/HrmCustomProcessChange_e9.jsp", "workflow/request/ext4e9/HrmCustomShift_e9.jsp", "meeting/template/meetingReceiptInitJs.js", "meeting/template/meetingReceiptInitJs.jsp",
            "meeting/template/meetingSubmitRequestJs.js", "meeting/template/MeetingSubmitRequestJs.jsp", "meeting/template/MeetingSubmitRequestJs4Mobile.jsp", "fna/template/FnaSubmitInvoiceFjJs.jsp",
            "fna/template/FnaSubmitRequestJs.jsp", "fna/template/FnaSubmitRequestJs4Mobile.jsp", "fna/template/FnaSubmitRequestJs4Mobile_e9.jsp", "fna/template/FnaSubmitRequestJs_e9.jsp",
            "fna/template/FnaSubmitRequestJsAdvance.jsp", "fna/template/FnaSubmitRequestJsAdvance4Mobile.jsp", "fna/template/FnaSubmitRequestJsAdvance4Mobile_e9.jsp", "fna/template/FnaSubmitRequestJsAdvance_e9.jsp",
            "fna/template/FnaSubmitRequestJsBorrow.jsp", "fna/template/FnaSubmitRequestJsBorrow4Mobile.jsp", "fna/template/FnaSubmitRequestJsBorrow4Mobile_e9.jsp", "fna/template/FnaSubmitRequestJsBorrow_e9.jsp", "fna/template/FnaSubmitRequestJsChange.jsp",
            "fna/template/FnaSubmitRequestJsChange4Mobile.jsp", "fna/template/FnaSubmitRequestJsChange4Mobile_e9.jsp", "fna/template/FnaSubmitRequestJsChange_e9.jsp", "fna/template/FnaSubmitRequestJsMulti.jsp",
            "fna/template/FnaSubmitRequestJsMulti4Mobile.jsp", "fna/template/FnaSubmitRequestJsRepayment.jsp", "fna/template/FnaSubmitRequestJsRepayment4Mobile.jsp", "fna/template/FnaSubmitRequestJsRepayment4Mobile_e9.jsp",
            "fna/template/FnaSubmitRequestJsRepayment_e9.jsp", "fna/template/FnaSubmitRequestJsShare.jsp", "fna/template/FnaSubmitRequestJsShare4Mobile.jsp", "fna/template/FnaSubmitRequestJsShare4Mobile_e9.jsp",
            "fna/template/FnaSubmitRequestJsShare_e9.jsp", "workflow/qiyuesuo/js/wf_qiyuesuoCustomPage.jsp", "contractmanagement/cmcustompage.jsp", "cpt/template/CapitalSubmitRequestJs.jsp",
    };


    //流程表单信息
    List<String> formdata = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "流程状态", "表单表名", "数据量", "数据最新时间"));
    //字段联动
    List<String> fdlinkSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "联动名称", "触发字段", "是否启用", "sql文本", "替换后sql", "是否完全替换", "替换信息"));
    //字段属性
    List<String> nodeFdlinkSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "表单名称", "节点名称", "字段名称", "sql文本", "替换后sql", "是否完全替换", "替换信息"));
    //dml附加操作
    List<String> dmlSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "DMl名称", "sql文本", "替换后sql", "是否完全替换", "替换信息"));
    //action附加操作
    List<String> actionSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "接口名称", "class路径", "接口类型"));
    List<String> actionSheet_sys = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "接口名称", "class路径", "接口类型"));
    //webservice附加操作
    List<String> webserviceSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "动作名称", "webservice接口名称", "webservice地址", "回写sql", "替换后sql", "是否完全替换", "替换信息"));
    //esb附加操作
    List<String> esbSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "动作名称", "引用esb"));
    List<String> sapActionSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "sap名称", "异构系统", "数据源", "注册服务名"));
    //自定义页面
    List<String> custompageSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "文件路径", "是否启用"));
    //代码块
    List<String> customcodeSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "表单名称", "节点名称", "模板名称", "代码内容","复杂程度"));
    List<String> nodelinkconditionSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "出口名称", "条件内容"));
    List<String> piciconditionSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "流程状态", "节点名称", "操作组名称", "条件sql", "条件内容"));
    List<String> dubanconditionSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "表单名称", "节点名称", "代码内容"));

    //开启了sap多选按钮的流程,节点
    List<String> sapSheet = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "sap源"));

    // 节点操作组 使用外部接口(自定义java 、自定义sql )
    List<String> groupoutdetail = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "节点名称", "操作组名称", "外部接口类型", "外部接口名称", "外部接口内容", "替换后sql", "是否完全替换", "替换信息"));
    //流程交换的列表
    List<String> workflowChangeSheet = new ArrayList<String>(Arrays.asList("流程交换名称", "交换类型", "流程名称(id)", "所属分部", "交换主表", "是否启用", "说明"));
    //流程创建日程的列表
    List<String> workflowCreatePlanSheet = new ArrayList<String>(Arrays.asList("动作", "流程名称(id)", "触发节点", "说明"));

    //非HTML模板的节点模板需要拉出来清单，因为这些个模板会默认迁成流式布局
    List<String> oldHtmlTemp = new ArrayList<String>(Arrays.asList("流程名称(id)", "所属分部", "节点名称", "模板名称", "说明"));

    int version = 9;


    /**
     * 功能维度
     */
    public String doWorkFlowAll(String path, int version) {
        try {
            this.version = version;
            System.out.println("version= " + version);
            // 创建一个模块excel文件
            ExcelWriteEntity writeEntity = new ExcelWriteEntity("工作流程_功能维度_WorkFlowAll", path);
            if (com.weaver.versionupgrade.customRest.util.SourceDBUtil.version.equals("7")) {
                E7WorkFlowExcel.getE7WorkflowActionSet(writeEntity, "流程节点附加操作");
            }
            getFormData(writeEntity, "流程表单信息");
            getFdlinkSheet(writeEntity, "流程字段联动");
            getNodeFdlinkSheet(writeEntity, "流程字段属性sql(E10流程字段联动)");
            getWorkflowActionSet(writeEntity, "流程节点附加操作_自定义");
            getWorkflowActionSetSystem(writeEntity, "流程节点附加操作_标准");


            getCustompage(writeEntity, "流程自定义页面");


            getCustomcode(writeEntity, "流程表单代码块");
            getCondition(writeEntity, "流程老规则条件");
            getSap(writeEntity, "SAP关联流程");
            getWorkflowChange(writeEntity, "流程交换列表");
            getWorkflowCreatePlan(writeEntity, "流程创建日程列表");
            getWorkflowgroupoutdetail(writeEntity, "流程节点操作组外部接口");

            getOldHtmlTemp(writeEntity, "非html模板清单");

            getWorkflowErrorSetting(writeEntity, "流程升级后需要调整的异常设置");

            String filePath = excelWrite_html(writeEntity);



           ExcelWriteEntity writeEntity_tmp = new ExcelWriteEntity("工作流程_业务维度_WorkFlowAll", path);
            LinkedHashMap<String, List<ExcelFunctionBean>> sheets_tmp = new LinkedHashMap<>();
            LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();
            sheets.values().forEach(excelFunctionBeans -> {
                for (ExcelFunctionBean excelFunctionBean : excelFunctionBeans) {
                    int nameIndex = getNameIndex(excelFunctionBean, "流程名称(id)");
                    if (nameIndex>-1){
                    for (List<String> beanValue : excelFunctionBean.getValues()) {
                        String workflowName = beanValue.get(nameIndex);
                        if ("".equals(workflowName)){
                            continue;
                        }
                        if (workflowName.length()>30){
                            workflowName=workflowName.substring(0,20);
                        }
                        List<ExcelFunctionBean> orDefault = sheets_tmp.getOrDefault(workflowName, new ArrayList<>());
                        addBeanValue(orDefault,excelFunctionBean,beanValue);
                        //添加sheet页
                        sheets_tmp.put(workflowName, orDefault);

                    }}
                }

            });
            writeEntity_tmp.setSheets(sheets_tmp);
            String filePath_tmp = excelWrite_html(writeEntity_tmp);

            return filePath;
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
            e.printStackTrace();
        }
        return "";
    }


    public static int addBeanValue(List<ExcelFunctionBean> excelFunctionBeanList, ExcelFunctionBean excelFunctionBean,List<String> beanValue){


        ExcelFunctionBean excelFunctionBean_tmp=null;

        for (ExcelFunctionBean functionBean : excelFunctionBeanList) {
            if (excelFunctionBean.getFunctionName().equals(functionBean.getFunctionName())&&excelFunctionBean.getHeaderNames().equals(functionBean.getHeaderNames())) {
                excelFunctionBean_tmp=functionBean;
            }
        }
        if (excelFunctionBean_tmp==null){
            excelFunctionBean_tmp=new ExcelFunctionBean(excelFunctionBean.getFunctionName());
            excelFunctionBean_tmp.setValues(new ArrayList<>());
            excelFunctionBean_tmp.setHeaderNames(excelFunctionBean.getHeaderNames());
            excelFunctionBeanList.add(excelFunctionBean_tmp);
        }

        List<List<String>> values = excelFunctionBean_tmp.getValues();
        values.add(beanValue);

        return -1;
    }

    public static int getNameIndex(ExcelFunctionBean excelFunctionBean, String name) {

        //拿到流程id
        List<String> headerNames = excelFunctionBean.getHeaderNames();
        for (int i = 0; i < headerNames.size(); i++) {
            if (name.toLowerCase().contains(headerNames.get(i).toLowerCase())) {
                return i;
            }
        }
        return -1;
    }

    private void getWorkflowErrorSetting(ExcelWriteEntity writeEntity,  String sheetname) {


        {
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("SELECT\n" +
                "\t( SELECT workflowname FROM workflow_base t1 WHERE t1.id = f.workflowid ) AS workflowname,\n" +
                "\t( SELECT nodename FROM workflow_nodebase t2 WHERE t2.id = f.nodeid ) AS nodename,\n" +
                "\tnodeid,\n" +
                "\tworkflowid,\n" +
                "\tformid\n" +
                "       ,f.formula\n" +
                "FROM\n" +
                "\tworkflow_formula_htmllayout f\n" +
                "WHERE\n" +
                "\tformula LIKE '%WfForm%'");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString(1)), "7");

            String nodeid = Util.processBody(Util.null2String(rs.getString(3)));

            String workflowid = Util.processBody(Util.null2String(rs.getString(4)));

            value.add(workflowname + "(" + workflowid + ")");
            value.add(getNodeName(nodeid));
            value.add(Util.null2String(rs.getString(5)));
            value.add(Util.null2String(rs.getString(6)));

            values.add(value);

        }
        List<String> oldHtmlTemp_1 = new ArrayList<String>(Arrays.asList("流程名称(id)",  "节点名称", "formid", "公式内容"));
        addExcelModal(writeEntity, sheetname, "流程节点公式", oldHtmlTemp_1, values);
        }


        {


            List<List<String>> values = new ArrayList<List<String>>();
            RecordSet rs = new RecordSet();
            rs.executeQuery("\tselect *\n" +
                    "  from (\n" +
                    "        select workflow_flownode.workflowid,\n" +
                    "                workflow_base.workflowname,\n" +
                    "                count(case when nodeattribute = 1 then 1 end) as count_nodeattribute_1,\n" +
                    "                count(case when nodeattribute > 2 then 1 end) as count_nodeattribute_3\n" +
                    "          from workflow_flownode\n" +
                    "          left join workflow_base\n" +
                    "            on workflow_flownode.workflowid = workflow_base.id\n" +
                    "          left join workflow_nodebase\n" +
                    "            on workflow_flownode.nodeid = workflow_nodebase.id\n" +
                    "         group by workflow_flownode.workflowid, workflow_base.workflowname\n" +
                    "\n" +
                    "        ) a\n" +
                    " where a.count_nodeattribute_1 <> (a.count_nodeattribute_3)");
            while (rs.next()) {
                List<String> value = new ArrayList<String>();
                String workflowname = Util.processBody(Util.null2String(rs.getString(2)), "7");


                String workflowid = Util.processBody(Util.null2String(rs.getString(1)));

                value.add(workflowname + "(" + workflowid + ")");
                value.add(Util.null2String(rs.getString(3)));
                value.add(Util.null2String(rs.getString(4)));

                values.add(value);

            }
            List<String> oldHtmlTemp_1 = new ArrayList<String>(Arrays.asList("流程名称(id)",  "分叉开始节点数量", "分叉开始结束数量"));
            addExcelModal(writeEntity, sheetname, "分叉流程", oldHtmlTemp_1, values);

        }


    }


    /**
     * 统一在这个方法里面封装excel需要的table
     *
     * @param writeEntity
     * @return
     */
    public String excelWrite_html(ExcelWriteEntity writeEntity) {
        ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
        String filePath = excelWrite.excelWrite(writeEntity);

        return filePath;
    }

    public void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {
        ExcelWrite.addExcelModal(writeEntity, sheetname, functionName, headerNames, values);
    }


    /**
     * 获取表单数据信息
     *
     * @param writeEntity
     * @param sheetname
     */
    void getFormData(ExcelWriteEntity writeEntity, String sheetname) throws Exception {


        Map<String, Map<String, String>> wrokflowCountMap = new HashMap<>();
        RecordSet sourceDBUtil = new RecordSet();
        sourceDBUtil.executeQuery("select count(*) totalnum, max(CREATEDATE) createdate,workflowid\n" +
                "from workflow_requestbase\n" +
                "group by workflowid\n");
        while (sourceDBUtil.next()) {
            Map<String, String> map1 = new HashMap<>();
            map1.put("totalnum", sourceDBUtil.getString("totalnum"));
            map1.put("createdate", sourceDBUtil.getString("createdate"));
            wrokflowCountMap.put(sourceDBUtil.getString("workflowid"), map1);
        }


        Map<String, Integer> detailTableCount = new HashMap<>();

        Map<String, LinkedHashMap<String, Integer>> workflowTableDetailInfo = new HashMap<>();

        //datasync
        sourceDBUtil.executeQuery("select t1.id as id,t3.TABLENAME as tablename\n" +
                "                from workflow_base t1\n" +
                "                         left join workflow_bill t2 on t1.isbill = 1 and t1.formid = t2.id\n" +
                "                        left join workflow_billdetailtable t3  on  t3.billid = t2.id\n" +
                "where t3.TABLENAME is not null");

        while (sourceDBUtil.next()) {
            String id = sourceDBUtil.getString("id");
            String tablename = sourceDBUtil.getString("tablename").toLowerCase();
            Integer cnt = 0;
            if (detailTableCount.containsKey(tablename)) {
                cnt = detailTableCount.get(tablename);
            } else {
                SourceDBUtil sourceDBUtil_tmp = new SourceDBUtil();
                sourceDBUtil_tmp.executeQuery("select count(*) cnt from " + tablename);
                if (sourceDBUtil_tmp.next()) {
                    detailTableCount.put(tablename, sourceDBUtil_tmp.getInt("cnt"));
                    cnt = detailTableCount.get(tablename);
                }
            }

            if (workflowTableDetailInfo.containsKey(id)) {
                Map<String, Integer> map = workflowTableDetailInfo.get(id);
                map.put(tablename, cnt);
            } else {
                LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
                map.put(tablename, cnt);
                workflowTableDetailInfo.put(id, map);
            }
        }


        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values1 = new ArrayList<List<String>>();
        List<List<String>> values2 = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        //老表单
        rs.executeQuery("select * from workflow_base  where  isbill=0 ");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowid = Util.null2String(rs.getString("id"));
            String ISVALID = Util.null2String(rs.getString("ISVALID"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String totalnum = "0";
            String createdate = "";
//            rs1.executeQuery("select count( REQUESTID) totalnum, max(CREATEDATE) createdate from  workflow_requestbase   where  WORKFLOWID =?", workflowid);
//
//            if (rs1.next()) {
//                totalnum = rs1.getString("totalnum");//表单数量
//                createdate = rs1.getString("createdate");//最新创建日期
//            }

            ////isvalid 0 无效  1 活动版本 2 测试  3 历史版本
            if (!ISVALID.equals("1")) {


            }


            if (wrokflowCountMap.containsKey(workflowid)) {
                Map<String, String> map = wrokflowCountMap.get(workflowid);
                totalnum = map.get("totalnum");
                createdate = map.get("createdate");
            }

            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            ////isvalid 0 无效  1 活动版本 2 测试  3 历史版本
            if (!ISVALID.equals("3")) {
                if (!ISVALID.equals("1") && !ISVALID.equals("2"))
                    ISVALID = "0";
                if (!ISVALID.equals("1") && !ISVALID.equals("2")) {
                    value.add("无效");
                } else if (ISVALID.equals("1")) {
                    value.add("有效");
                } else if (ISVALID.equals("2")) {
                    value.add("测试");
                } else {
                    value.add("有效");
                }
            } else {


                value.add("历史版本");

            }
            value.add("workflow_form");
            value.add(totalnum);
            value.add(createdate);

            values1.add(value);
        }


        //新表单
        rs.executeQuery("select a.id,a.WORKFLOWNAME,b.TABLENAME,a.SUBCOMPANYID,ISVALID from workflow_base a left join workflow_bill b on a.FORMID = b.id where a.isbill=1 and    (a.istemplate is null or a.istemplate <> '1')  ");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String tablename = Util.null2String(rs.getString("TABLENAME"));
            String ISVALID = Util.null2String(rs.getString("ISVALID"));
            String workflowid = Util.null2String(rs.getString("id"));
            String totalnum = "0";
            String createdate = "";
//            rs1.executeQuery("select count( REQUESTID) totalnum, max(CREATEDATE) createdate from  workflow_requestbase   where  WORKFLOWID =?", id);
//            if (rs1.next()) {
//                totalnum = rs1.getString("totalnum");//表单数量
//                createdate = rs1.getString("createdate");//最新创建日期
//            }

            if (wrokflowCountMap.containsKey(workflowid)) {
                Map<String, String> map = wrokflowCountMap.get(workflowid);
                totalnum = map.get("totalnum");
                createdate = map.get("createdate");
            }


            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            ////isvalid 0 无效  1 活动版本 2 测试  3 历史版本
            if (!ISVALID.equals("3")) {
                if (!ISVALID.equals("1") && !ISVALID.equals("2"))
                    ISVALID = "0";
                if (!ISVALID.equals("1") && !ISVALID.equals("2")) {
                    value.add("无效");
                } else if (ISVALID.equals("1")) {
                    value.add("有效");
                } else if (ISVALID.equals("2")) {
                    value.add("测试");
                } else {
                    value.add("有效");
                }
            } else {
                value.add("历史版本");
            }
            value.add(tablename);
            value.add(totalnum);
            value.add(createdate);

            value.add(workflowid);

            values2.add(value);


        }

        values1.sort(Comparator.comparingInt(o -> -Integer.valueOf(o.get(4))));
        values2.sort(Comparator.comparingInt(o -> -Integer.valueOf(o.get(4))));

        addExcelModal(writeEntity, sheetname, "老表单流程", formdata, values1);


        for (int i = values2.size() - 1; i >= 0; i--) {
            List<String> list = values2.get(i);
            String workflowid = list.get(6);
            list.remove(6);


            if (workflowTableDetailInfo.containsKey(workflowid)) {

                LinkedHashMap<String, Integer> linkedHashMap = workflowTableDetailInfo.get(workflowid);
                // 将 entrySet 转换为 List
                List<Map.Entry<String, Integer>> entryList = new ArrayList<>(linkedHashMap.entrySet());
                // 使用 Collections.sort() 进行排序，根据值降序排列
                Collections.sort(entryList, Map.Entry.comparingByValue());
                // 打印排序后的结果
                for (Map.Entry<String, Integer> entry : entryList) {
                    List<String> value_tmp = new ArrayList<String>();
                    value_tmp.add("");
                    value_tmp.add("");
                    value_tmp.add("");
                    value_tmp.add("" + entry.getKey());
                    value_tmp.add("" + entry.getValue());
                    value_tmp.add("");
                    values2.add(i + 1, value_tmp);
                }
            }


        }


        addExcelModal(writeEntity, sheetname, "新表单流程", formdata, values2);


        List<String> formdata = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "明细表单表名", "数据量", "数据最新时间"));


        //addExcelModal(writeEntity, sheetname, "明细表单", formdata, values2);


    }

    private void getFdlinkSheet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        List<List<String>> values = new ArrayList<List<String>>();

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        RecordSet rs2 = new RecordSet();
        RecordSet rs3 = new RecordSet();
        // 查询当前流程绑定的字段联动
        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.* from workflow_datainput_entry a,workflow_base b where a.WORKFLOWID=b.id and  b.ISVALID=1 order by a.WORKFLOWID");
        while (rs.next()) {

            String entryid = Util.null2String(rs.getString("id"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));

            String enable = (Util.null2String(rs.getString("enable")).equals("0")||Util.null2String(rs.getString("enable")).equals("")) ? "启用" : "未启用";
            if (version == 7) {
                //E7只要有数据就是启用
                enable = "启用";
            }
            String triggername = Util.null2String(rs.getString("TRIGGERNAME"));
            String triggerfieldid = Util.null2String(rs.getString("TRIGGERFIELDNAME"));
            //0、主字段，1、明细字段
            String detailtype = Util.null2String(rs.getString("type"));
            if (triggerfieldid.startsWith("field")) {
                triggerfieldid = triggerfieldid.substring(5);
            }
            String fieldName = getFieldName(workflowid, triggerfieldid, detailtype);

            rs1.executeQuery("select * from workflow_datainput_main where ENTRYID=?", entryid);
            while (rs1.next()) {
                String backfields = "";
                String sqlfrom = "";
                String sqlwhere = "";

                String mainId = Util.null2String(rs1.getString("id"));
                rs2.executeQuery("select * from workflow_datainput_table  where DATAINPUTID=?", mainId);
                while (rs2.next()) {
                    String tableid = Util.null2String(rs2.getString("id"));
                    String tablename = Util.null2String(rs2.getString("TABLENAME"));
                    String alias = Util.null2String(rs2.getString("ALIAS"));
                    if ("".equals(alias)) {
                        alias = tablename;
                        sqlfrom = sqlfrom + tablename + ",";
                    } else {
                        sqlfrom = sqlfrom + tablename + " " + alias + ",";
                    }


                    rs3.executeQuery("select * from workflow_datainput_field  where DATAINPUTID=? and TABLEID=?", mainId, tableid);
                    while (rs3.next()) {
                        String type = Util.null2String(rs3.getString("type"));// 1为条件字段，2为查询字段
                        String dbfieldname = Util.null2String(rs3.getString("DBFIELDNAME"));
                        String pagefieldname = Util.null2String(rs3.getString("PAGEFIELDNAME"));
                        if ("1".equals(type)) {// 拼接条件
                            sqlwhere = sqlwhere + alias + "." + dbfieldname + "='$" + pagefieldname + "$'" + " and ";
                        }
                        if ("2".equals(type)) {
                            backfields = backfields + alias + "." + dbfieldname + ",";
                        }
                    }
                }
                backfields = backfields.trim();
                sqlfrom = sqlfrom.trim();
                sqlwhere = sqlwhere.trim();
                if (backfields.endsWith(",")) {
                    backfields = backfields.substring(0, backfields.length() - 1);
                }
                if (sqlfrom.endsWith(",")) {
                    sqlfrom = sqlfrom.substring(0, sqlfrom.length() - 1);
                }
                if (sqlwhere.endsWith("and")) {
                    sqlwhere = sqlwhere.substring(0, sqlwhere.length() - 3);
                }

                String whereclause = Util.null2String(rs1.getString("WHERECLAUSE")).trim();
                if (whereclause.toLowerCase().startsWith("where")) {
                    whereclause.substring(5);
                }
                String finalSql = "select ";
                if ("".equals(backfields)) {
                    backfields = "*";
                }
                finalSql = finalSql + backfields + " from " + sqlfrom + " where 1=1 ";
                if (!"".equals(whereclause)) {
                    finalSql = finalSql + " and " + whereclause;
                }
                if (!"".equals(sqlwhere)) {
                    finalSql = finalSql + " and " + sqlwhere;
                }
                List<String> value = new ArrayList<String>();
                value.add(workflowname + "(" + workflowid + ")");
                value.add(subCompanyname);
                value.add(triggername);
                value.add(fieldName);
                value.add(enable);
                value.add(finalSql);

                Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(finalSql);
                value.add(Util.null2String(sqlResultMap.get("targetCode")));
                value.add(Util.null2String(sqlResultMap.get("transStatus")));
                value.add(Util.null2String(sqlResultMap.get("transMsg")));


                values.add(value);
            }

        }
        addExcelModal(writeEntity, sheetname, sheetname, fdlinkSheet, values);

    }


    private void getNodeFdlinkSheet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();


        String nodesql = "select b.*, a.* " +
                " from workflow_nodefieldattr a " +
                "         left join " +
                "     (select c.WORKFLOWID, d.SUBCOMPANYID, d.WORKFLOWNAME, d.ISVALID, c.NODEID " +
                "      from workflow_flownode c " +
                "               left join workflow_base d on c.WORKFLOWID = d.id) b " +
                "     on a.NODEID = b.NODEID " +
                " where b.ISVALID = 1 " +
                "  and b.WORKFLOWID > 0  and a.attrcontent like '%doFieldSQL%'";

        rs.executeQuery(nodesql);
        while (rs.next()) {
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String workflowid = Util.null2String(rs.getString("WORKFLOWID"));
            String nodeid = Util.null2String(rs.getString("nodeid"));
            String fieldid = Util.null2String(rs.getString("FIELDID"));
            String formid = Util.null2String(rs.getString("FORMID"));
            String sql_cus = Util.null2String(rs.getString("ATTRCONTENT"));

            int index = sql_cus.indexOf("doFieldSQL(\"");
            if (index > -1) {
                sql_cus = sql_cus.substring(index + 12);
                index = sql_cus.lastIndexOf("\")");
                if (index > -1) {
                    sql_cus = sql_cus.substring(0, index);
                }

                sql_cus = sql_cus.trim();
            }

            List<String> value = new ArrayList<String>();
            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(getFormName(workflowid, formid));
            value.add(getNodeName(nodeid));
            value.add(getFieldName(workflowid, fieldid, "0"));
            value.add(sql_cus);


            Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(sql_cus);
            value.add(Util.null2String(sqlResultMap.get("targetCode")));
            value.add(Util.null2String(sqlResultMap.get("transStatus")));
            value.add(Util.null2String(sqlResultMap.get("transMsg")));


            values.add(value);
        }
        addExcelModal(writeEntity, sheetname, sheetname, nodeFdlinkSheet, values);
    }

    private void getWorkflowActionSet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        List<List<String>> actionValues = new ArrayList<List<String>>();
        List<List<String>> dmlValues = new ArrayList<List<String>>();
        List<List<String>> wsValues = new ArrayList<List<String>>();
        List<List<String>> esbValues = new ArrayList<List<String>>();
        List<List<String>> sapValues = new ArrayList<List<String>>();
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();

        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();

        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.*,a.WORKFLOWID as WORKFLOWID from workflowactionset a , workflow_base b where a.WORKFLOWID=b.id  and b.ISVALID=1  order by a.WORKFLOWID");
        while (rs.next()) {
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String actionname = Util.null2String(rs.getString("actionname"));
            String workflowid = Util.null2String(rs.getString("WORKFLOWID"));
            int nodeid = Util.getIntValue(rs.getString("NODEID"), 0);
            int nodelinkid = Util.getIntValue(rs.getString("NODELINKID"), 0);
            String interfaceid = Util.null2String(rs.getString("INTERFACEID"));
            int isused = Util.getIntValue(Util.null2String(rs.getString("ISUSED")), 0);
            int interfacetype = Util.getIntValue(Util.null2String(rs.getString("interfacetype")));// 动作流类型 1.dml 2.webservice 3.action 6.esb


            switch (interfacetype) {
                case 1: //dml

                    List<String> dmlValue = new ArrayList<String>();
                    String sqldml = "select  * from formactionset a,formactionsqlset b where a.id = ? and a.id=b.ACTIONID";
                    rs1.executeQuery(sqldml, interfaceid);
                    if (rs1.next()) {
                        String dmlname = Util.null2String(rs1.getString("DMLACTIONNAME"));
                        String dmlsql = Util.null2String(rs1.getString("DMLSQL"));
                        String dmlcussql = Util.null2String(rs1.getString("DMLCUSSQL"));

                        dmlValue.add(workflowname + "(" + workflowid + ")");
                        dmlValue.add(subCompanyname);
                        dmlValue.add(actionname);
                        dmlValue.add(nodeid > 0 ? "节点" : "出口");
                        dmlValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        dmlValue.add(isused == 1 ? "启用" : "未启用");
                        dmlValue.add(dmlname);
                        dmlValue.add("".equals(dmlsql) ? dmlcussql : dmlsql);


                        Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap("".equals(dmlsql) ? dmlcussql : dmlsql);
                        dmlValue.add(Util.null2String(sqlResultMap.get("targetCode")));
                        dmlValue.add(Util.null2String(sqlResultMap.get("transStatus")));
                        dmlValue.add(Util.null2String(sqlResultMap.get("transMsg")));

                        dmlValues.add(dmlValue);
                    }
                    break;
                case 2://webservice
                    List<String> wsValue = new ArrayList<String>();
                    String wsaction = "select b.CUSTOMNAME,b.WEBSERVICEURL,a.* from wsformactionset a ,wsregiste b where a.WSURL = b.id and a.id = ?";
                    rs1.executeQuery(wsaction, interfaceid);
                    if (rs1.next()) {
                        String wsactionname = Util.null2String(rs1.getString("ACTIONNAME"));
                        String customname = Util.null2String(rs1.getString("CUSTOMNAME"));
                        String webserviceurl = Util.null2String(rs1.getString("WEBSERVICEURL"));
                        String retstr = Util.null2String(rs1.getString("RETSTR"));

                        wsValue.add(workflowname + "(" + workflowid + ")");
                        wsValue.add(subCompanyname);
                        wsValue.add(actionname);
                        wsValue.add(nodeid > 0 ? "节点" : "出口");
                        wsValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        wsValue.add(isused == 1 ? "启用" : "未启用");
                        wsValue.add(wsactionname);
                        wsValue.add(customname);
                        wsValue.add(webserviceurl);
                        wsValue.add(retstr);


                        Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(retstr);
                        wsValue.add(Util.null2String(sqlResultMap.get("targetCode")));
                        wsValue.add(Util.null2String(sqlResultMap.get("transStatus")));
                        wsValue.add(Util.null2String(sqlResultMap.get("transMsg")));


                        wsValues.add(wsValue);

                    }

                    break;
                case 3://action
                    List<String> actionValue = new ArrayList<String>();
                    String sqlaction = "select  * from actionsetting where actionname = ?";
                    rs1.executeQuery(sqlaction, interfaceid);
                    if (rs1.next()) {
                        String actionshowname = Util.null2String(rs1.getString("ACTIONSHOWNAME"));
                        String actionclass = Util.null2String(rs1.getString("ACTIONCLASS"));
                        boolean isStandarAction = false;
                        List<String> list = Arrays.asList(STANDARACTION);
                        if (list.contains(actionclass)) {
                            isStandarAction = true;
                            continue;
                        }
                        actionValue.add(workflowname + "(" + workflowid + ")");
                        actionValue.add(subCompanyname);
                        actionValue.add("".equals(actionname) ? actionshowname : actionname);
                        actionValue.add(nodeid > 0 ? "节点" : "出口");
                        actionValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        actionValue.add(isused == 1 ? "启用" : "未启用");
                        actionValue.add(actionshowname);
                        actionValue.add(actionclass);
                        actionValue.add(isStandarAction ? "标准接口" : "自定义接口");

                        actionValues.add(actionValue);
                    }
                    break;
                case 6://esb
                    List<String> esbValue = new ArrayList<String>();
                    String sqlesb = "select  * from esb_actionset where actionname = ?";
                    rs1.executeQuery(sqlesb, interfaceid);
                    if (rs1.next()) {
                        String esbid = Util.null2String(rs1.getString("esbid"));
                        esbValue.add(workflowname + "(" + workflowid + ")");
                        esbValue.add(subCompanyname);
                        esbValue.add(actionname);
                        esbValue.add(nodeid > 0 ? "节点" : "出口");
                        esbValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        esbValue.add(isused == 1 ? "启用" : "未启用");
                        esbValue.add(Util.formatMultiLang(interfaceid, "7"));
                        esbValue.add(esbid);

                        esbValues.add(esbValue);
                    }
                    break;

                case 4://sap动作
                    List<String> sapValue = new ArrayList<String>();
                    String sqlsap = "select * from int_BrowserbaseInfo where MARK = ?";
                    rs1.executeQuery(sqlsap, interfaceid);
                    if (rs1.next()) {

                        String regservice = Util.null2String(rs1.getString("REGSERVICE"));//所属服务
                        int w_enable = Util.getIntValue(rs1.getString("w_enable"));//是否启用

                        sapValue.add(workflowname + "(" + workflowid + ")");
                        sapValue.add(subCompanyname);
                        sapValue.add(actionname);
                        sapValue.add(nodeid > 0 ? "节点" : "出口");
                        sapValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        sapValue.add(w_enable == 1 ? "启用" : "未启用");
                        sapValue.add(Util.formatMultiLang(interfaceid, "7"));
                        String sql = "select b.HETENAME,c.POOLNAME, a.* from  sap_service a " +
                                "left join int_heteproducts b on  a.HPID=b.id   " +
                                "left join sap_datasource c on a.POOLID=c.id  " +
                                "where a.id=?";
                        rs1.executeQuery(sql, regservice);
                        if (rs1.next()) {
                            sapValue.add(Util.null2String(rs1.getString("HETENAME")));
                            sapValue.add(Util.null2String(rs1.getString("POOLNAME")));
                            sapValue.add(Util.null2String(rs1.getString("REGNAME")));
                        }

                        sapValues.add(sapValue);
                    }
                    break;
                default:
                    break;

            }
        }

        ExportAllDataExcel.actionCount+=actionValues.size();
        addExcelModal(writeEntity, sheetname, "节点/出口action(自定义)", actionSheet, actionValues);
        addExcelModal(writeEntity, sheetname, "节点/出口dml(自定义)", dmlSheet, dmlValues);
        addExcelModal(writeEntity, sheetname, "节点/出口webservice", webserviceSheet, wsValues);
        addExcelModal(writeEntity, sheetname, "节点/出口esb", esbSheet, esbValues);
        addExcelModal(writeEntity, sheetname, "节点/出口sap动作流", sapActionSheet, sapValues);

    }


    /**
     * 抓取E7
     *
     * @param writeEntity
     * @param sheetname
     * @throws Exception
     */
    private void getE7WorkflowActionSet(ExcelWriteEntity writeEntity, String sheetname) throws Exception {


        //获取配置文件路径
        String rootPath = ConfigUtil.getConfigDir();
        System.out.println("rootPath=" + rootPath);
/*        int indexOf = rootPath.indexOf("e10Migration");
        String substring = rootPath.substring(0, indexOf);*/
//        String actionfilepath = substring + "ecology" + File.separator + "WEB-INF" + File.separator + "service" + File.separator + "action.xml";
        String actionfilepath = rootPath + "ecology" + File.separator + "WEB-INF" + File.separator + "service" + File.separator + "action.xml";
        File inputFile = new File(actionfilepath);
        // 创建 DocumentBuilderFactory 实例
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        // 创建 DocumentBuilder 实例
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        // 使用 DocumentBuilder 解析 XML 文件并返回 Document 对象
        Document doc = dBuilder.parse(inputFile);
        Map<String, String> actionMap = new HashMap<>();
        // 获取XML文档中所有名为 "service-point" 的元素
        NodeList nodeList = doc.getElementsByTagName("service-point");
        // 遍历每个 "service-point" 元素
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                // 获取 "service-point" 元素的 id 属性
                String id = element.getAttribute("id");
                // 获取 "invoke-factory" 元素下的 "construct" 元素的 "class" 属性
                Element constructElement = (Element) element.getElementsByTagName("construct").item(0);
                String className = constructElement.getAttribute("class");
                actionMap.put(id, className);
            }
        }


        List<List<String>> valuese7 = new ArrayList<List<String>>();


        SourceDBUtil sourceDBUtil = new SourceDBUtil();

        sourceDBUtil.execute("select t2.WORKFLOWNAME,t2.SUBCOMPANYID,TYPE,CUSTOMERVALUE,t3.nodename,CUSTOMERVALUE\n" +
                "from workflow_addinoperate t1\n" +
                "         left join workflow_base t2 on t1.WORKFLOWID = t2.id\n" +
                "         left join workflow_nodebase t3 on t1.OBJID = t3.id\n" +
                "where type in (2, 3)  and t1.isnode=1 order by t1.WORKFLOWID");
        while (sourceDBUtil.next()) {
            List<String> list = new ArrayList<>();


            String workflowname = TransUtil.null2String(sourceDBUtil.getString("WORKFLOWNAME"));
            String SUBCOMPANYID = TransUtil.null2String(sourceDBUtil.getString("SUBCOMPANYID"));
            String type = TransUtil.null2String(sourceDBUtil.getString("TYPE"));
            String CUSTOMERVALUE = TransUtil.null2String(sourceDBUtil.getString("CUSTOMERVALUE"));
            String nodename = TransUtil.null2String(sourceDBUtil.getString("nodename"));

            list.add(workflowname);//流程名称
            SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
            list.add(subCompanyComInfo.getSubCompanyname(SUBCOMPANYID).equals("") ? "总部" : subCompanyComInfo.getSubCompanyname(SUBCOMPANYID));//所属分部


            list.add(CUSTOMERVALUE);//动作名称


            list.add(CUSTOMERVALUE);//节点/出口

            list.add(CUSTOMERVALUE);//节点/出口名称

            list.add(CUSTOMERVALUE);//是否启用

            list.add(CUSTOMERVALUE);//动作名称


            list.add(actionMap.get(CUSTOMERVALUE));//class路径

            list.add(actionMap.get(CUSTOMERVALUE));//接口类型

            valuese7.add(list);
        }

        List<String> formdata = new ArrayList<String>(Arrays.asList("流程名称", "所属分部", "动作名称", "节点/出口", "节点/出口名称", "是否启用", "动作名称", "class路径", "接口类型"));


        addExcelModal(writeEntity, "E7流程节点操作", "E7流程节点附加操作", formdata, valuese7);

    }


    private void getWorkflowActionSetSystem(ExcelWriteEntity writeEntity, String sheetname) throws Exception {

        List<List<String>> actionValues = new ArrayList<List<String>>();

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();

        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();

        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.*,b.id as workflowid from workflowactionset a , workflow_base b where a.WORKFLOWID=b.id  and b.ISVALID=1  order by a.WORKFLOWID");
        while (rs.next()) {
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String actionname = Util.null2String(rs.getString("actionname"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            int nodeid = Util.getIntValue(rs.getString("NODEID"), 0);
            int nodelinkid = Util.getIntValue(rs.getString("NODELINKID"), 0);
            String interfaceid = Util.null2String(rs.getString("INTERFACEID"));
            int isused = Util.getIntValue(Util.null2String(rs.getString("ISUSED")), 0);
            int interfacetype = Util.getIntValue(Util.null2String(rs.getString("interfacetype")));// 动作流类型 1.dml 2.webservice 3.action 6.esb


            switch (interfacetype) {
                case 1: //dml


                    break;
                case 2://webservice


                    break;
                case 3://action
                    List<String> actionValue = new ArrayList<String>();
                    String sqlaction = "select  * from actionsetting where actionname = ?";
                    rs1.executeQuery(sqlaction, interfaceid);
                    if (rs1.next()) {
                        String actionshowname = Util.null2String(rs1.getString("ACTIONSHOWNAME"));
                        String actionclass = Util.null2String(rs1.getString("ACTIONCLASS"));
                        boolean isStandarAction = false;
                        List<String> list = Arrays.asList(STANDARACTION);
                        if (!list.contains(actionclass)) {
                            continue;
                        }
                        actionValue.add(workflowname + "(" + workflowid + ")");
                        actionValue.add(subCompanyname);
                        actionValue.add(actionname);
                        actionValue.add(nodeid > 0 ? "节点" : "出口");
                        actionValue.add(nodeid > 0 ? getNodeName(nodeid + "") : getNodeLinkName(nodelinkid + ""));
                        actionValue.add(isused == 1 ? "启用" : "未启用");
                        actionValue.add(Util.formatMultiLang(actionshowname, "7"));
                        actionValue.add(actionclass);

                        List<String> fnaList = Arrays.asList(FNA_STANDARACTION);
                        List<String> hrmList = Arrays.asList(HRM_STANDARACTION);
                        if (fnaList.contains(actionclass)) {
                            actionValue.add("标准财务节点附加操作，暂无法迁移，升级后需重新按照E10功能配置财务节点动作");
                        } else if (hrmList.contains(actionclass)) {
                            actionValue.add("标准人事状态变更动作，暂无法迁移，升级后需重新按照E10功能配置ESB动作");
                        } else {
                            actionValue.add("");
                        }


                        actionValues.add(actionValue);
                    }
                    break;
                case 6://esb

                    break;

                case 4://sap动作
                    break;
                default:
                    break;

            }
        }
        addExcelModal(writeEntity, sheetname, "节点/出口action(标准)", actionSheet_sys, actionValues);


    }


    private void getCustompage(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select * from workflow_base where ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String custompage = Util.null2String(rs.getString("custompage")).trim();
            String workflowid = Util.null2String(rs.getString("id")).trim();

            List<String> list = Arrays.asList(CUSTOMPAGE);
            boolean isbiaozhun = false;
            for (String page : list) {
                if (custompage.indexOf(page) > -1) {
                    isbiaozhun = true;
                    break;
                }
            }
            if (isbiaozhun) {
                continue;
            }

            if (!"".equals(custompage)) {
                value.add(workflowname + "(" + workflowid + ")");
                value.add(subCompanyname);
                value.add(custompage);
                value.add("启用");

                values.add(value);
            }
        }

        rs.executeQuery("select  b.WORKFLOWNAME,b.SUBCOMPANYID,a.*,b.id as workflowid from workflow_custompageconfig a,workflow_base b where a.workflowid=b.id and b.ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String custompage = Util.null2String(rs.getString("url"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            int enable = Util.getIntValue(rs.getString("enable"), 0);

            List<String> list = Arrays.asList(CUSTOMPAGE);
            boolean isbiaozhun = false;
            for (String page : list) {
                if (custompage.indexOf(page) > -1) {
                    isbiaozhun = true;
                    break;
                }
            }
            if (isbiaozhun) {
                continue;
            }

            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(custompage);
            value.add(enable == 1 ? "启用" : "未启用");

            values.add(value);

        }

        ExportAllDataExcel.actionCount+=values.size();

        addExcelModal(writeEntity, sheetname, sheetname, custompageSheet, values);

    }

    private void getCustomcode(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select  WORKFLOWNAME, SUBCOMPANYID , id  from workflow_base  where  ISVALID=1 ");
        while (rs.next()) {

            String workflowid = Util.null2String(rs.getString("id"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));

            List<Map<String, String>> htmllayoutinfo = gethtmllayoutinfo(workflowid);
            if (htmllayoutinfo.isEmpty()) {
                continue;
            }
            for (Map<String, String> map : htmllayoutinfo) {
                List<String> value = new ArrayList<String>();
                String formid = Util.null2String(map.get("formid"));
                String nodeid = Util.null2String(map.get("nodeid"));
                String scriptstr = Util.null2String(map.get("scriptstr"));
                String layoutname = Util.null2String(map.get("layoutname"));
//            new BaseBean().writeLog("scriptstr==================" + scriptstr);
                String deScriptstr = decodeStr(scriptstr);

                if ("".equals(deScriptstr)) {
                    continue;
                }

                boolean haskaifa = false;
                try {
                    org.jsoup.nodes.Element body = org.jsoup.Jsoup.parse(deScriptstr);
                    org.jsoup.select.Elements scripts = body.select("script");
                    for (int i = 0; i < scripts.size(); i++) {
                        org.jsoup.nodes.Element script = scripts.get(i);
                        if (script != null) {
                            String str = script.data().trim();
                            if (!"".equals(str)) {
                                haskaifa = true;
                            }
                            String src = Util.null2String(script.attr("src"));
                            if (!"".equals(src)) {
                                haskaifa = true;
                            }
                        }
                    }
                } catch (Exception e) {
                    new weaver.general.BaseBean().writeLog("", e);
                }

                if (!haskaifa) {
                    continue;
                }

                value.add(workflowname + "(" + workflowid + ")");
                value.add(subCompanyname);
                value.add(getFormName(workflowid, formid));
                value.add(getNodeName(nodeid));
                value.add(layoutname);
                value.add(deScriptstr);



                int line=countNewlines(deScriptstr);
                //<50 简单
                //<<200  一般
                //<<200  一般
                if (line<50){
                    value.add("简单");
                }else if (line<200){
                    value.add("一般");
                } else {
                    value.add("复杂");
                }




                values.add(value);
            }

        }
        ExportAllDataExcel.actionCount+=values.size();


        addExcelModal(writeEntity, sheetname, sheetname, customcodeSheet, values);

    }
    public static int countNewlines(String text) {
        int newlineCount = 0;
        int length = text.length();
        int i = 0;

        while (i < length) {
            char c = text.charAt(i);

            if (c == '\r') {
                // 检查下一个字符是否是'\n'，如果是，则只将这对字符视为一个换行符
                if (i + 1 < length && text.charAt(i + 1) == '\n') {
                    newlineCount++;
                    i++; // 跳过'\n'
                } else {
                    // 单独的'\r'也被视为一个换行符（根据需要可以注释掉这行代码）
                    newlineCount++;
                }
            } else if (c == '\n') {
                // 如果不是'\r\n'的情况，则单独的'\n'也被视为一个换行符
                newlineCount++;
            }
            i++;
        }

        // 注意：如果决定不将单独的'\r'视为换行符，则上面的else if分支中的newlineCount++应该保留，
        // 而处理'\r'的分支中的newlineCount++（在else部分）应该被注释掉。

        return newlineCount;
    }

    public static List<Map<String, String>> gethtmllayoutinfo(String workflowid) {

        String scriptcol = "scriptstr";

        if (!InitWeaverServer.oaversion.startsWith("9")) {
            scriptcol = "SCRIPTS";
        }
        List<Map<String, String>> infos = new ArrayList<>();
        RecordSet rs = new RecordSet(false);//不用缓存
        rs.executeQuery("select formid,nodeid,layoutname," + scriptcol + "  from workflow_nodehtmllayout where WORKFLOWID = ?  order by nodeid", workflowid);


        while (rs.next()) {
            HashMap<String, String> map = new HashMap<>();
            map.put("formid", rs.getString("formid"));
            map.put("nodeid", rs.getString("nodeid"));
            map.put("scriptstr", rs.getString(scriptcol));
            map.put("layoutname", rs.getString("layoutname"));
            infos.add(map);

        }
        return infos;


    }


    private void getCondition(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> nodelinkValues = new ArrayList<List<String>>();
        List<List<String>> piciValues = new ArrayList<List<String>>();
        List<List<String>> dubanValues = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();

        //出口条件老规则
        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.* from workflow_nodelink a,workflow_base b where a.WORKFLOWID=b.id and a.CONDITION like '%(%' and b.ISVALID=1");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String linkname = Util.formatMultiLang(Util.null2String(rs.getString("LINKNAME")), "7");
            String condition = Util.null2String(rs.getString("CONDITION"));


            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(linkname);
            value.add(condition);

            nodelinkValues.add(value);
        }

        //批次条件老规则
        rs.executeQuery("select (select  workflowname from workflow_base where workflow_base.id = t3.workflowid) as workflowname, " +
                "       (select case isvalid " +
                "                   when '1' then '有效' " +
                "                   when '0' then '无效' " +
                "                   when '2' then '测试' " +
                "                   when '3' then '历史版本' " +
                "                   else '未知' end\n" +
                "        from workflow_base " +
                "        where workflow_base.id = t3.workflowid)                                                  as isvalid, " +
                "       t2.groupname                                                                              as groupname, " +
                "       t3.WORKFLOWID                                                                               as workflowid, " +
                "       (select  nodename  from workflow_nodebase where workflow_nodebase.id = t2.nodeid) as nodename, " +
                "       conditions                                                                                as conditions, " +
                "       conditioncn                                                                               as conditioncn " +
                "from workflow_groupdetail t, " +
                "     workflow_nodegroup t2, " +
                "     workflow_flownode t3 " +
                "where t2.id = t.groupid " +
                "  and t3.nodeid = t2.nodeid " +
                "  and conditions like '%(%'");

        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String isvalid = Util.null2String(rs.getString("isvalid"));
            String nodename = Util.processBody(Util.null2String(rs.getString("nodename")), "7");
            String groupname = Util.null2String(rs.getString("groupname"));
            String conditions = Util.null2String(rs.getString("conditions"));
            String conditioncn = Util.null2String(rs.getString("conditioncn"));
            String workflowid = Util.null2String(rs.getString("workflowid"));


            value.add(workflowname + "(" + workflowid + ")");
            value.add(isvalid);
            value.add(nodename);
            value.add(groupname);
            value.add(conditions);
            value.add(conditioncn);

            piciValues.add(value);
        }
        rs.executeQuery("select (select  workflowname  " +
                "        from workflow_base " +
                "        where workflow_base.id = workflow_urgerdetail.workflowid) as workflowname, WORKFLOWID ," +
                "       (select case isvalid " +
                "                   when '1' then '有效' " +
                "                   when '0' then '无效' " +
                "                   when '2' then '测试' " +
                "                   when '3' then '历史版本' " +
                "                   else '未知' end " +
                "        from workflow_base " +
                "        where workflow_base.id = workflow_urgerdetail.workflowid) as isvaid, " +
                "       conditions                                                 as conditions, " +
                "       conditioncn                                                as conditioncn " +
                "from workflow_urgerdetail " +
                "where exists (select 1 from workflow_base where workflow_base.id = workflow_urgerdetail.workflowid) " +
                "  and conditions like '%(%' ");
        //督办条件老规则
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String isvaid = Util.null2String(rs.getString("isvaid"));
            String conditions = Util.null2String(rs.getString("conditions"));
            String conditioncn = Util.null2String(rs.getString("conditioncn"));
            String workflowid = Util.null2String(rs.getString("WORKFLOWID"));


            value.add(workflowname + "(" + workflowid + ")");
            value.add(isvaid);
            value.add(conditions);
            value.add(conditioncn);

            dubanValues.add(value);
        }


        addExcelModal(writeEntity, sheetname, "出口条件老规则", nodelinkconditionSheet, nodelinkValues);
        addExcelModal(writeEntity, sheetname, "批次条件老规则", piciconditionSheet, piciValues);
        addExcelModal(writeEntity, sheetname, "督办条件老规则", dubanconditionSheet, dubanValues);


    }

    private void getSap(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        RecordSet rs1 = new RecordSet();
        rs.executeQuery("select * from  workflow_base  where SAPSource <>'' and ISVALID=1");
        while (rs.next()) {

            List<String> value = new ArrayList<String>();

            String workflowname = Util.formatMultiLang(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String sapSource = Util.null2String(rs.getString("SAPSource"));
            String workflowid = Util.null2String(rs.getString("id"));
            rs1.executeQuery("select * from sap_datasource where id=?", sapSource);
            if (rs1.next()) {
                String poolname = Util.null2String(rs1.getString("POOLNAME"));

                value.add(workflowname + "(" + workflowid + ")");
                value.add(subCompanyname);
                value.add(poolname);


                values.add(value);
            }
        }

        addExcelModal(writeEntity, sheetname, sheetname, sapSheet, values);

    }

    private void getWorkflowChange(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        List<List<String>> values = new ArrayList<List<String>>();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,a.*,b.id as workflowid from wfec_indatawfset a,workflow_base b where a.WORKFLOWID=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String name = Util.processBody(Util.null2String(rs.getString("NAME")), "7");
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String outermaintable = Util.null2String(rs.getString("OUTERMAINTABLE"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            int STATUS = Util.getIntValue(Util.null2String(rs.getString("STATUS")));

            value.add(name);
            value.add("推送");
            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(outermaintable);
            value.add(STATUS == 1 ? "启用" : "未启用");
            value.add("请根据实际业务逻辑，使用ESB动作流重新搭建出使用场景，搭建过程中有疑问可以咨询卢震昊！");

            values.add(value);

        }

        rs.executeQuery("select b.WORKFLOWNAME,a.* ,b.id as workflowid  from wfec_outdatawfset a,workflow_base b where a.WORKFLOWID=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String name = Util.processBody(Util.null2String(rs.getString("NAME")), "7");
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));
            String outermaintable = Util.null2String(rs.getString("OUTERMAINTABLE"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            int STATUS = Util.getIntValue(Util.null2String(rs.getString("STATUS")));

            value.add(name);
            value.add("接收");
            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(outermaintable);
            value.add(STATUS == 1 ? "启用" : "未启用");
            value.add("请根据实际业务逻辑，使用ESB动作流重新搭建出使用场景，搭建过程中有疑问可以咨询卢震昊！");

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, workflowChangeSheet, values);
    }

    private void getWorkflowCreatePlan(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();

        RecordSet rs = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,a.*,b.id as workflowid  from workflow_createplan a,workflow_base b where a.wfid=b.id");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("WORKFLOWNAME")), "7");
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String nodeid = Util.null2String(rs.getString("NODEID"));

            value.add("流程创建日程");
            value.add(workflowname + "(" + workflowid + ")");
            value.add(getNodeName(nodeid));
            value.add("创建日程主体可迁移，具体创建，提醒细节需手动调整设置！过程中有疑问可以咨询卢震昊！");

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, workflowCreatePlanSheet, values);
    }

    private void getWorkflowgroupoutdetail(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();

        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();


        RecordSet rs = new RecordSet();
        rs.executeQuery("select t3.workflowid,\n" +
                "       workflowname,\n" +
                "       subcompanyid,\n" +
                "       t2.nodeid,\n" +
                "       t2.groupname,\n" +
                "       t.type,\n" +
                "       t.jobobj,\n" +
                "       t.jobfield,\n" +
                "       t.zdysqlname,\n" +
                "       t4.id as workflowid\n" +
                "from workflow_groupdetail t,\n" +
                "     workflow_nodegroup t2,\n" +
                "     workflow_flownode t3,\n" +
                "     workflow_base t4\n" +
                "where t.groupid = t2.id and t4.id = t3.workflowid and t4.isvalid =1\n" +
                "  and t2.nodeid = t3.nodeid\n" +
                "  and t.type in (97, 98)");

        while (rs.next()) {
            List<String> value = new ArrayList<String>();

            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String nodeid = Util.null2String(rs.getString("nodeid"));
            String workflowid = Util.null2String(rs.getString("workflowid"));
            String groupname = Util.null2String(rs.getString("groupname"));
            String jobfield = Util.null2String(rs.getString("jobfield"));
            String type = Util.null2String(rs.getString("type"));// 97 java 98 sql
            String typename = type.equals("97") ? Util.null2String(rs.getString("jobobj")) : Util.null2String(rs.getString("zdysqlname"));
            type = type.equals("97") ? "java" : "sql";

            String subCompanyname = Util.null2String(subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID"))));

            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(getNodeName(nodeid));
            value.add(groupname);
            value.add(type);
            value.add(typename);
            value.add(jobfield);


            Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(jobfield);
            value.add(Util.null2String(sqlResultMap.get("targetCode")));
            value.add(Util.null2String(sqlResultMap.get("transStatus")));
            value.add(Util.null2String(sqlResultMap.get("transMsg")));

            values.add(value);
        }

        addExcelModal(writeEntity, sheetname, sheetname, groupoutdetail, values);
    }

    private void getOldHtmlTemp(ExcelWriteEntity writeEntity, String sheetname) throws Exception {
        List<List<String>> values = new ArrayList<List<String>>();
        SubCompanyComInfo subCompanyComInfo = new SubCompanyComInfo();
        RecordSet rs = new RecordSet();
        rs.executeQuery("select b.WORKFLOWNAME,b.SUBCOMPANYID,a.*,b.id as workflowid   from workflow_nodehtmllayout a,workflow_base b where a.WORKFLOWID=b.id and  a.isactive=1 and a.version!=2");
        while (rs.next()) {
            List<String> value = new ArrayList<String>();
            String workflowname = Util.processBody(Util.null2String(rs.getString("workflowname")), "7");
            String subCompanyname = subCompanyComInfo.getSubCompanyname(Util.null2String(rs.getString("SUBCOMPANYID")));
            String layoutname = Util.processBody(Util.null2String(rs.getString("LAYOUTNAME")));
            String nodeid = Util.processBody(Util.null2String(rs.getString("NODEID")));
            String workflowid = Util.processBody(Util.null2String(rs.getString("workflowid")));
            value.add(workflowname + "(" + workflowid + ")");
            value.add(subCompanyname);
            value.add(getNodeName(nodeid));
            value.add(layoutname);
            value.add("非HTML模板(普通模板，老E7html模板)，迁移E10全部初始化为默认的流式布局模板，请根据需要重新调整或者生成模板!");

            values.add(value);

        }
        addExcelModal(writeEntity, sheetname, sheetname, oldHtmlTemp, values);
    }


    public static String getFieldName(String wfid, String fieldid, String isdetail) {
        String name = "";
        RecordSet rs = new RecordSet();
        String isbill = "";
        String formid = "";
        rs.executeQuery("select isbill ,formid from workflow_base where id=?", wfid);
        if (rs.next()) {
            isbill = Util.null2String(rs.getString("isbill"));
            formid = Util.null2String(rs.getString("formid"));
        }
        String sql = "";
        if (isbill.equals("1")) {
            sql = "select t2.labelname as name from workflow_billfield t, htmllabelinfo t2 where t.fieldlabel=t2.indexid and t2.languageid=7 and t.id='" + Util.getIntValue(fieldid, -1) + "'";
        } else {
            sql = "select fieldlable as name from workflow_fieldlable where formid='" + formid + "'  and langurageid=7 and fieldid='" + Util.getIntValue(fieldid, -1) + "'";
        }
        rs.executeQuery(sql);
        if (rs.next()) {
            name = rs.getString("name");
        }
        return Util.formatMultiLang(name, "7");
    }

    public static String getNodeName(String nodeid) {
        RecordSet rs = new RecordSet();
        rs.executeQuery("select nodename from workflow_nodebase where id = ?", nodeid);
        String nodename = "";
        if (rs.next()) {
            nodename = Util.null2String(rs.getString("nodename"));
        }
        return Util.formatMultiLang(nodename, "7");
    }

    public static String getNodeLinkName(String nodelinkid) {
        RecordSet rs = new RecordSet();
        rs.executeQuery("select linkname from workflow_nodelink where id = ?", nodelinkid);
        String linkname = "";
        if (rs.next()) {
            linkname = Util.null2String(rs.getString("linkname"));
        }
        return Util.formatMultiLang(linkname, "7");
    }

    public static String getFormName(String workflowid, String formid) {
        RecordSet rs = new RecordSet();
        int isbill = 0;//0:老表单   1:新表单
        String formname = "";
        rs.executeQuery("select isbill from workflow_base where id=?", workflowid);
        if (rs.next()) {
            isbill = Util.getIntValue(Util.null2String(rs.getString("isbill")));
        }
        if (isbill == 0) {
            //老表单
            rs.executeQuery("select formname from workflow_formbase where id = ?", formid);
            if (rs.next()) {
                formname = Util.null2String(rs.getString("formname"));
            }
        } else {
            //新表单
            rs.executeQuery("select * from workflow_bill where id=?", formid);
            if (rs.next()) {
                int namelabel = Util.getIntValue(rs.getString("NAMELABEL"));
                formname = SystemEnv.getHtmlLabelName(namelabel, 7);
            }
        }

        return Util.formatMultiLang(formname);
    }

}
