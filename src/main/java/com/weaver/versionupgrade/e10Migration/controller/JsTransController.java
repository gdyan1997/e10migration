package com.weaver.versionupgrade.e10Migration.controller;

import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.service.GetWorkflowJsCodeService;
import com.weaver.versionupgrade.entity.dto.JavascriptTransDTO;
import com.weaver.versionupgrade.entity.entity.JavascriptTrans;
import com.weaver.versionupgrade.util.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.company.SubCompanyComInfo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ygd2020
 * @date 2024-1-18 10:21
 * Description:
 **/
@CrossOrigin
@Controller
@RequestMapping("/jscode")
public class JsTransController {

    @Autowired
    public GetWorkflowJsCodeService getWorkflowJsCodeService;

    @ResponseBody
    @GetMapping("/getWorkflowJsCodeByPage")
    public HashMap<String, Object> getWorkflowJsCodeByPage(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                                           @RequestParam(value = "perPage", required = false, defaultValue = "10") int perPage,
                                                           @RequestParam(value = "workflowName", required = false, defaultValue = "") String workflowName,
                                                           @RequestParam(value = "isAll", required = false, defaultValue = "false") boolean isAll) {
        return getWorkflowJsCodeService.getWorkflowJsCodeByPage(page, perPage, workflowName, isAll);
    }

    /**
     * 获取流程、建模代码段
     *
     * @return
     */
    @ResponseBody
    @PostMapping("/getWorkflowJsCodeByPageNew")
    public HashMap<String, Object> getWorkflowJsCodeByPageNew(@RequestBody Map<String, Object> params) {
        int page = (int) params.getOrDefault("page", 1);
        int perPage = (int) params.getOrDefault("perPage", 10);
        String workflowName = (String) params.getOrDefault("workflowName", "");
        boolean isAll = (boolean) params.getOrDefault("isAll", false);
        String fromModule = (String) params.getOrDefault("fromModule", "");
        return getWorkflowJsCodeService.getWorkflowJsCodeByPageNew(page, perPage, workflowName, fromModule, isAll);
    }

    /**
     * 未转换的js模版数据
     *
     * @return
     */
    @ResponseBody
    @PostMapping("/getWorkflowJsCodeByPageNewWithOutTrans")
//    public HashMap<String, Object> getWorkflowJsCodeByPageNewWithOutTrans(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
//                                                                          @RequestParam(value = "perPage", required = false, defaultValue = "10") int perPage,
//                                                                          @RequestParam(value = "workflowName", required = false, defaultValue = "") String workflowName,
//                                                                          @RequestParam(value = "fromModule", required = false, defaultValue = "") String fromModule) {
    public HashMap<String, Object> getWorkflowJsCodeByPageNewWithOutTrans(@RequestBody Map<String, Object> params) {
        int page = (int) params.getOrDefault("page", 1);
        int perPage = (int) params.getOrDefault("perPage", 10);
        String workflowName = (String) params.getOrDefault("workflowName", "");
        String fromModule = (String) params.getOrDefault("fromModule", "");
        return getWorkflowJsCodeService.getWorkflowJsCodeByPageNewWithOutTrans(page, perPage, workflowName, fromModule);
    }


    /**
     * 初始化所有工作流到中间表中
     * isForce为true，会删除所有数据，然后全量同步
     * isForce为false，只同步新增数据
     *
     * @param isForce
     * @return
     */
    @ResponseBody
    @GetMapping("/initAll")
    public ApiResponse<Void> initAll(@RequestParam(value = "isForce", required = false, defaultValue = "false") boolean isForce,
                                     @RequestParam(value = "fromModule", required = false, defaultValue = "") String fromModule,
                                     @RequestParam(value = "workflowName", required = false, defaultValue = "") String workflowName) {
        if ("".equals(fromModule)) {
            //如果没有数据，要进行初始化
            getWorkflowJsCodeService.initAll("workflow", isForce, workflowName);//true的时候会清空数据
            //如果没有数据，要进行初始化
            getWorkflowJsCodeService.initAll("cube", isForce, workflowName);//true的时候会清空数据
        } else {
            getWorkflowJsCodeService.initAll(fromModule, isForce, workflowName);
        }
        return new ApiResponse<>(true, "初始化完成", null);
    }

    /**
     * 保存转换后的JS数据
     * @param jsTrans
     * @return
     */
    @ResponseBody
    @PostMapping("/trans")
    public ApiResponse<Void> initTranfercontent(@RequestBody JavascriptTransDTO jsTrans) {
        boolean result = getWorkflowJsCodeService.initTranfercontent(jsTrans);
        return new ApiResponse<>(result, result ? "转换完成" : "转换失败", null);
    }

    /**
     * 保存转换后的JS数据
     * @param jsTransList
     * @return
     */
    @ResponseBody
    @PostMapping("/transBatch")
    public ApiResponse<Void> initTranfercontentBatch(@RequestBody List<JavascriptTransDTO> jsTransList) {
        System.out.println("==========transBatch-begin");
        boolean result = getWorkflowJsCodeService.initTranfercontentBatch(jsTransList);
        System.out.println("==========transBatch-end");
        return new ApiResponse<>(result, result ? "批量转换完成" : "批量转换失败", null);
    }

    /**
     * 导出JS转换数据
     * @param workflowName
     * @param fromModule
     * @param response
     */
    @ResponseBody
    @GetMapping("/exportExcelForAll")
    public void exportExcelForAll(@RequestParam(value = "workflowName", required = false, defaultValue = "") String workflowName,
                                  @RequestParam(value = "fromModule", required = false, defaultValue = "") String fromModule,
                                  HttpServletResponse response) {

        try {
            // 设置响应头
            String fileName = "JS代码替换清单" + (workflowName.isEmpty() ? "" : "-" + workflowName) + ".xlsx";
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));

            // 导出数据到 Excel
            getWorkflowJsCodeService.exportAll(response.getOutputStream(), fromModule, workflowName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * 刷新数据，并分组
     */
    @ResponseBody
    @GetMapping("/refreshDatas")
    public ApiResponse<Void> refreshData() {
        try {
            // 将数据分组
            getWorkflowJsCodeService.refreshData();
            return new ApiResponse<>(true, "数据分组成功", null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}