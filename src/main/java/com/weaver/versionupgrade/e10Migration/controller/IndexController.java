package com.weaver.versionupgrade.e10Migration.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ygd2020
 * @date 2024-1-18 10:21
 * Description:
 **/
@Controller
public class IndexController {

    // Mapping for the index route and all other routes
    @GetMapping(value = {"/main/{path:[^\\.]*}"})
    public String forward(HttpServletRequest request, @PathVariable(required = false) String path) {
        String currentPath = request.getRequestURI();
        return "forward:/index.html";
    }

    // Mapping for the index route and all other routes
    @GetMapping(value = {"/"})
    public String forwardForRoot(HttpServletRequest request, @PathVariable(required = false) String path) {
        String currentPath = request.getRequestURI();
        return "forward:/e10Migration/entry.html";
    }
}