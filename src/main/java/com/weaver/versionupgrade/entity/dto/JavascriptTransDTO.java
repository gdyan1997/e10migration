package com.weaver.versionupgrade.entity.dto;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.weaver.versionupgrade.entity.entity.JavascriptTrans;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-17
 */
public class JavascriptTransDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String frommodule;
    private String workflowname;
    private String formname;
    private String nodename;
    private String scriptstr;
    private String tranfercontent;
    private String workflowid;
    private String typename;
    private String layoutname;

    private List<String> tranferstatus;
    private String hassrcscript;
    private String hascssstyle;

    private String transferflag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTranfercontent() {
        return tranfercontent;
    }

    public void setTranfercontent(String tranfercontent) {
        this.tranfercontent = tranfercontent;
    }

    public List<String> getTranferstatus() {
        return tranferstatus;
    }

    public void setTranferstatus(List<String> tranferstatus) {
        this.tranferstatus = tranferstatus;
    }

    public String getTransferflag() {
        return transferflag;
    }

    public void setTransferflag(String transferflag) {
        this.transferflag = transferflag;
    }

    public String getFrommodule() {
        return frommodule;
    }

    public void setFrommodule(String frommodule) {
        this.frommodule = frommodule;
    }

    public String getWorkflowname() {
        return workflowname;
    }

    public void setWorkflowname(String workflowname) {
        this.workflowname = workflowname;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public String getNodename() {
        return nodename;
    }

    public void setNodename(String nodename) {
        this.nodename = nodename;
    }

    public String getScriptstr() {
        return scriptstr;
    }

    public void setScriptstr(String scriptstr) {
        this.scriptstr = scriptstr;
    }

    public String getWorkflowid() {
        return workflowid;
    }

    public void setWorkflowid(String workflowid) {
        this.workflowid = workflowid;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getLayoutname() {
        return layoutname;
    }

    public void setLayoutname(String layoutname) {
        this.layoutname = layoutname;
    }

    public String getHassrcscript() {
        return hassrcscript;
    }

    public void setHassrcscript(String hassrcscript) {
        this.hassrcscript = hassrcscript;
    }

    public String getHascssstyle() {
        return hascssstyle;
    }

    public void setHascssstyle(String hascssstyle) {
        this.hascssstyle = hascssstyle;
    }

    public JavascriptTrans trans(){
        JavascriptTrans javascriptTrans = new JavascriptTrans();
        javascriptTrans.setId(this.id);
        javascriptTrans.setTranfercontent(this.tranfercontent);
        javascriptTrans.setTransferflag(this.transferflag);
        try{
            if(this.tranferstatus != null)
                javascriptTrans.setTranferstatus(JSONObject.toJSONString(this.tranferstatus));
        }catch (Exception ex){

        }
        return javascriptTrans;
    }
}
