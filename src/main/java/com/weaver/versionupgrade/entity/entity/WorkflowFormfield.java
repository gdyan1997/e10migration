package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import weaver.general.Util;
import weaver.idtool.IdGenerator;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowFormfield implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("FORMID")
    private Integer formid;

    @TableField("FIELDID")
    private Integer fieldid;

    @TableField("FIELDPARAMETER")
    private String fieldparameter;

    @TableField("NEEDCHECK")
    private String needcheck;

    @TableField("CHECKSCRIPT")
    private String checkscript;

    @TableField("ISMULTIROWS")
    private String ismultirows;

    @TableField("FIELDORDER")
    private Integer fieldorder;

    @TableField("ISDETAIL")
    private String isdetail;

    @TableField("GROUPID")
    private Integer groupid;

    private String uuid;

//    private Integer fieldgroupid;

    public String getE10Id() {
        if("1".equals(isdetail)){
            return TransUtil.getNewTableID("form_field", "id", "workflow_formfield", formid + "_" + fieldid + "_" + isdetail);
        }else{//主
            return TransUtil.getNewTableID("form_field", "id", "workflow_formfield", formid + "_" + fieldid + "_0");
        }
    }

    public Integer getFormid() {
        return formid;
    }

    public void setFormid(Integer formid) {
        this.formid = formid;
    }

    public String getE10FormId() {
        return TransUtil.getNewTableID("form", "id", "workflow_formbase", Util.null2String(formid));
    }

    public Integer getFieldid() {
        return fieldid;
    }

    public void setFieldid(Integer fieldid) {
        this.fieldid = fieldid;
    }

    public String getFieldparameter() {
        return fieldparameter;
    }

    public void setFieldparameter(String fieldparameter) {
        this.fieldparameter = fieldparameter;
    }

    public String getNeedcheck() {
        return needcheck;
    }

    public void setNeedcheck(String needcheck) {
        this.needcheck = needcheck;
    }

    public String getCheckscript() {
        return checkscript;
    }

    public void setCheckscript(String checkscript) {
        this.checkscript = checkscript;
    }

    public String getIsmultirows() {
        return ismultirows;
    }

    public void setIsmultirows(String ismultirows) {
        this.ismultirows = ismultirows;
    }

    public Integer getFieldorder() {
        return fieldorder;
    }

    public void setFieldorder(Integer fieldorder) {
        this.fieldorder = fieldorder;
    }

    public String getIsdetail() {
        return isdetail;
    }

    public void setIsdetail(String isdetail) {
        this.isdetail = isdetail;
    }

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public String getE10SubFormId(){
        if("1".equals(isdetail)){
            return TransUtil.getNewTableID("sub_form","id","workflow_formbase",formid + "_" + groupid);
        }else{//主
            return "";
        }
    }

//    public String getUuid() {
//        return uuid;
//    }
//
//    public void setUuid(String uuid) {
//        this.uuid = uuid;
//    }
//
//    public Integer getFieldgroupid() {
//        return fieldgroupid;
//    }
//
//    public void setFieldgroupid(Integer fieldgroupid) {
//        this.fieldgroupid = fieldgroupid;
//    }

    @Override
    public String toString() {
        return "WorkflowFormfield{" +
                "formid=" + formid +
                ", fieldid=" + fieldid +
                ", fieldparameter=" + fieldparameter +
                ", needcheck=" + needcheck +
                ", checkscript=" + checkscript +
                ", ismultirows=" + ismultirows +
                ", fieldorder=" + fieldorder +
                ", isdetail=" + isdetail +
                ", groupid=" + groupid +
                ", uuid=" + uuid +
//                ", fieldgroupid=" + fieldgroupid +
                "}";
    }
}
