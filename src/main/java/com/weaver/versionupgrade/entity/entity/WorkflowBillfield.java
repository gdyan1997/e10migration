package com.weaver.versionupgrade.entity.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import weaver.general.Util;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowBillfield implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    private Integer id;

    @TableField("BILLID")
    private Integer billid;

    @TableField("FIELDNAME")
    private String fieldname;

    @TableField("FIELDLABEL")
    private Integer fieldlabel;

    @TableField("FIELDDBTYPE")
    private String fielddbtype;

    @TableField("FIELDHTMLTYPE")
    private String fieldhtmltype;

    @TableField("TYPE")
    private Integer type;

    @TableField("VIEWTYPE")
    private Integer viewtype;

    @TableField("DETAILTABLE")
    private String detailtable;

    @TableField("FROMUSER")
    private String fromuser;

    @TableField("TEXTHEIGHT")
    private Integer textheight;

    @TableField("DSPORDER")
    private BigDecimal dsporder;

    @TableField("CHILDFIELDID")
    private Integer childfieldid;

    @TableField("IMGHEIGHT")
    private Integer imgheight;

    @TableField("IMGWIDTH")
    private Integer imgwidth;

    @TableField("PLACES")
    private Integer places;

    @TableField("QFWS")
    private String qfws;

    @TableField("TEXTHEIGHT_2")
    private String textheight2;

    @TableField("SELECTITEM")
    private Integer selectitem;

    @TableField("LINKFIELD")
    private Integer linkfield;

    @TableField("SELECTITEMTYPE")
    private String selectitemtype;

    @TableField("PUBCHOICEID")
    private Integer pubchoiceid;

    @TableField("PUBCHILCHOICEID")
    private Integer pubchilchoiceid;

    @TableField("STATELEV")
    private Integer statelev;

    @TableField("LOCATETYPE")
    private String locatetype;

//    private Integer fieldshowtypes;

//    private String uuid;

//    private Integer fieldgroupid;
//
//    private String isencrypt;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getE10Id() {
        return TransUtil.getNewTableID("form_field", "id", "workflow_billfield", Util.null2String(id));
    }

    public String getE10IdForCube(String modeid) {
        //fieldid + "_" + e9_modeid + "_" + CubeUtil.FORM_INFO_Suffix
        return TransUtil.getNewTableID("form_field", "id", "workflow_billfield", id + "_" + modeid + "_cube");
    }

    public Integer getBillid() {
        return billid;
    }

    public void setBillid(Integer billid) {
        this.billid = billid;
    }

    public String getE10FormId() {
        return TransUtil.getNewTableID("form", "id", "workflow_bill", Util.null2String(billid));
    }

    public String getE10FormIdForCube(String modeid) {

        return TransUtil.getNewTableID("form", "id", "workflow_bill", modeid + "_cube");
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public Integer getFieldlabel() {
        return fieldlabel;
    }

    public void setFieldlabel(Integer fieldlabel) {
        this.fieldlabel = fieldlabel;
    }

    public String getFielddbtype() {
        return fielddbtype;
    }

    public void setFielddbtype(String fielddbtype) {
        this.fielddbtype = fielddbtype;
    }

    public String getFieldhtmltype() {
        return fieldhtmltype;
    }

    public void setFieldhtmltype(String fieldhtmltype) {
        this.fieldhtmltype = fieldhtmltype;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getViewtype() {
        return viewtype;
    }

    public void setViewtype(Integer viewtype) {
        this.viewtype = viewtype;
    }

    public String getDetailtable() {
        return detailtable;
    }

    public void setDetailtable(String detailtable) {
        this.detailtable = detailtable;
    }

    public String getFromuser() {
        return fromuser;
    }

    public void setFromuser(String fromuser) {
        this.fromuser = fromuser;
    }

    public Integer getTextheight() {
        return textheight;
    }

    public void setTextheight(Integer textheight) {
        this.textheight = textheight;
    }

    public BigDecimal getDsporder() {
        return dsporder;
    }

    public void setDsporder(BigDecimal dsporder) {
        this.dsporder = dsporder;
    }

    public Integer getChildfieldid() {
        return childfieldid;
    }

    public void setChildfieldid(Integer childfieldid) {
        this.childfieldid = childfieldid;
    }

    public Integer getImgheight() {
        return imgheight;
    }

    public void setImgheight(Integer imgheight) {
        this.imgheight = imgheight;
    }

    public Integer getImgwidth() {
        return imgwidth;
    }

    public void setImgwidth(Integer imgwidth) {
        this.imgwidth = imgwidth;
    }

    public Integer getPlaces() {
        return places;
    }

    public void setPlaces(Integer places) {
        this.places = places;
    }

    public String getQfws() {
        return qfws;
    }

    public void setQfws(String qfws) {
        this.qfws = qfws;
    }

    public String getTextheight2() {
        return textheight2;
    }

    public void setTextheight2(String textheight2) {
        this.textheight2 = textheight2;
    }

    public Integer getSelectitem() {
        return selectitem;
    }

    public void setSelectitem(Integer selectitem) {
        this.selectitem = selectitem;
    }

    public Integer getLinkfield() {
        return linkfield;
    }

    public void setLinkfield(Integer linkfield) {
        this.linkfield = linkfield;
    }

    public String getSelectitemtype() {
        return selectitemtype;
    }

    public void setSelectitemtype(String selectitemtype) {
        this.selectitemtype = selectitemtype;
    }

    public Integer getPubchoiceid() {
        return pubchoiceid;
    }

    public void setPubchoiceid(Integer pubchoiceid) {
        this.pubchoiceid = pubchoiceid;
    }

    public Integer getPubchilchoiceid() {
        return pubchilchoiceid;
    }

    public void setPubchilchoiceid(Integer pubchilchoiceid) {
        this.pubchilchoiceid = pubchilchoiceid;
    }

    public Integer getStatelev() {
        return statelev;
    }

    public void setStatelev(Integer statelev) {
        this.statelev = statelev;
    }

    public String getLocatetype() {
        return locatetype;
    }

    public void setLocatetype(String locatetype) {
        this.locatetype = locatetype;
    }

//    public Integer getFieldshowtypes() {
//        return fieldshowtypes;
//    }
//
//    public void setFieldshowtypes(Integer fieldshowtypes) {
//        this.fieldshowtypes = fieldshowtypes;
//    }
//
//    public String getUuid() {
//        return uuid;
//    }
//
//    public void setUuid(String uuid) {
//        this.uuid = uuid;
//    }
//
//    public Integer getFieldgroupid() {
//        return fieldgroupid;
//    }
//
//    public void setFieldgroupid(Integer fieldgroupid) {
//        this.fieldgroupid = fieldgroupid;
//    }
//
//    public String getIsencrypt() {
//        return isencrypt;
//    }
//
//    public void setIsencrypt(String isencrypt) {
//        this.isencrypt = isencrypt;
//    }

    @Override
    public String toString() {
        return "WorkflowBillfield{" +
                "id=" + id +
                ", billid=" + billid +
                ", fieldname=" + fieldname +
                ", fieldlabel=" + fieldlabel +
                ", fielddbtype=" + fielddbtype +
                ", fieldhtmltype=" + fieldhtmltype +
                ", type=" + type +
                ", viewtype=" + viewtype +
                ", detailtable=" + detailtable +
                ", fromuser=" + fromuser +
                ", textheight=" + textheight +
                ", dsporder=" + dsporder +
                ", childfieldid=" + childfieldid +
                ", imgheight=" + imgheight +
                ", imgwidth=" + imgwidth +
                ", places=" + places +
                ", qfws=" + qfws +
                ", textheight2=" + textheight2 +
                ", selectitem=" + selectitem +
                ", linkfield=" + linkfield +
                ", selectitemtype=" + selectitemtype +
                ", pubchoiceid=" + pubchoiceid +
                ", pubchilchoiceid=" + pubchilchoiceid +
                ", statelev=" + statelev +
                ", locatetype=" + locatetype +
//                ", fieldshowtypes=" + fieldshowtypes +
//                ", uuid=" + uuid +
//                ", fieldgroupid=" + fieldgroupid +
//                ", isencrypt=" + isencrypt +
                "}";
    }
}
