package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowFormbase implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("FORMNAME")
    private String formname;

    @TableField("FORMDESC")
    private String formdesc;

    /**
     * @TableField("SECURELEVEL") private String securelevel;
     * @TableField("USERID") private Integer userid;
     * @TableField("FORMHTMLCODE") private String formhtmlcode;
     * @TableField("FORMDATE") private String formdate;
     * @TableField("SUBCOMPANYID") private Integer subcompanyid;
     * @TableField("SUBCOMPANYID3") private Integer subcompanyid3;
     * <p>
     * private String uuid;
     **/


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public String getFormdesc() {
        return formdesc;
    }

    public void setFormdesc(String formdesc) {
        this.formdesc = formdesc;
    }

    /**
    public String getSecurelevel() {
        return securelevel;
    }

    public void setSecurelevel(String securelevel) {
        this.securelevel = securelevel;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getFormhtmlcode() {
        return formhtmlcode;
    }

    public void setFormhtmlcode(String formhtmlcode) {
        this.formhtmlcode = formhtmlcode;
    }

    public String getFormdate() {
        return formdate;
    }

    public void setFormdate(String formdate) {
        this.formdate = formdate;
    }

    public Integer getSubcompanyid() {
        return subcompanyid;
    }

    public void setSubcompanyid(Integer subcompanyid) {
        this.subcompanyid = subcompanyid;
    }

    public Integer getSubcompanyid3() {
        return subcompanyid3;
    }

    public void setSubcompanyid3(Integer subcompanyid3) {
        this.subcompanyid3 = subcompanyid3;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
     **/

    @Override
    public String toString() {
        return "WorkflowFormbase{" +
                "id=" + id +
                ", formname=" + formname +
                ", formdesc=" + formdesc +
//                ", securelevel=" + securelevel +
//                ", userid=" + userid +
//                ", formhtmlcode=" + formhtmlcode +
//                ", formdate=" + formdate +
//                ", subcompanyid=" + subcompanyid +
//                ", subcompanyid3=" + subcompanyid3 +
//                ", uuid=" + uuid +
                "}";
    }
}
