package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
public class Modeinfo implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 自增主键
     */
        @TableId(value = "ID", type = IdType.AUTO)
      private Integer id;

    @TableField("MODENAME")
    private String modename;

    @TableField("MODEDESC")
    private String modedesc;

    @TableField("MODETYPE")
    private Integer modetype;

    @TableField("FORMID")
    private Integer formid;

    @TableField("MAINCATEGORY")
    private Integer maincategory;

    @TableField("SUBCATEGORY")
    private Integer subcategory;

    @TableField("SECCATEGORY")
    private Integer seccategory;

    @TableField("ISIMPORTDETAIL")
    private Integer isimportdetail;

    @TableField("CODEID")
    private Integer codeid;

    @TableField("CUSTOMPAGE")
    private String custompage;

    @TableField("DEFAULTSHARED")
    private String defaultshared;

    @TableField("NONDEFAULTSHARED")
    private String nondefaultshared;

    @TableField("DSPORDER")
    private Double dsporder;

    @TableField("MODECODE")
    private String modecode;

    @TableField("ISDELETE")
    private Integer isdelete;

    @TableField("SUBCOMPANYID")
    private Integer subcompanyid;

//    @TableField("ISALLOWREPLY")
//    private Integer isallowreply;
//
//    @TableField("CATEGORYTYPE")
//    private Integer categorytype;
//
//    @TableField("SELECTCATEGORY")
//    private Integer selectcategory;
//
//    private Integer isimportdetailofeditlayout;
//
//    @TableField("isAddRightByWorkFlow")
//    private Integer isaddrightbyworkflow;
//
//    private Integer istagset;
//
//    private Integer replyposition;
//
//    @TableField("isWatermark")
//    private Integer iswatermark;
//
//    @TableField("fileFormat")
//    private String fileformat;
//
//    @TableField("empowmentType")
//    private String empowmenttype;
//
//    private Integer expcardtemplate;
//
//    private Integer isdelfile;
//
//    private Integer isexpcardexcel;
//
//    private Integer isexpcardhtml;
//
//    private Integer isexpcardword;
//
//    private Integer isexpcardpdf;
//
//    private String expcardhtmlindexconfig;
//
//    private Integer expcardhtmltemplate;
//
//    private Integer expcardwordtemplate;
//
//    private Integer expcardpdftemplate;
//
//    private Integer expcardtypeexcel;
//
//    private Integer expcardtypehtml;
//
//    private Integer expcardtypeword;
//
//    private Integer expcardtypepdf;
//
//    private String filenameexcel;
//
//    private String filenamehtml;
//
//    private String filenameword;
//
//    private String filenamepdf;
//
//    private String attitle;
//
//    private String modetitle;
//
//    private Integer isfrontmultlang;
//
//    @TableField("isAtAll")
//    private String isatall;
//
//    private Integer isecme;
//
//    private Integer fileisdisplay;
//
//    private Integer iscustomorder;
//
//    private String customordertables;
//
//    private String allowlike;
//
//    private String likeshowname;
//
//    private String allowdislike;
//
//    private String dislikeshowname;
//
//    private String defaultcontraction;
//
//    private Integer iscustompage;
//
//    private Integer classprotect;
//
//    private Integer virtualrightopen;
//
//    private String cubeuuid;

    
    public Integer getId() {
        return id;
    }

      public void setId(Integer id) {
          this.id = id;
      }
    
    public String getModename() {
        return modename;
    }

      public void setModename(String modename) {
          this.modename = modename;
      }
    
    public String getModedesc() {
        return modedesc;
    }

      public void setModedesc(String modedesc) {
          this.modedesc = modedesc;
      }
    
    public Integer getModetype() {
        return modetype;
    }

      public void setModetype(Integer modetype) {
          this.modetype = modetype;
      }
    
    public Integer getFormid() {
        return formid;
    }

      public void setFormid(Integer formid) {
          this.formid = formid;
      }
    
    public Integer getMaincategory() {
        return maincategory;
    }

      public void setMaincategory(Integer maincategory) {
          this.maincategory = maincategory;
      }
    
    public Integer getSubcategory() {
        return subcategory;
    }

      public void setSubcategory(Integer subcategory) {
          this.subcategory = subcategory;
      }
    
    public Integer getSeccategory() {
        return seccategory;
    }

      public void setSeccategory(Integer seccategory) {
          this.seccategory = seccategory;
      }
    
    public Integer getIsimportdetail() {
        return isimportdetail;
    }

      public void setIsimportdetail(Integer isimportdetail) {
          this.isimportdetail = isimportdetail;
      }
    
    public Integer getCodeid() {
        return codeid;
    }

      public void setCodeid(Integer codeid) {
          this.codeid = codeid;
      }
    
    public String getCustompage() {
        return custompage;
    }

      public void setCustompage(String custompage) {
          this.custompage = custompage;
      }
    
    public String getDefaultshared() {
        return defaultshared;
    }

      public void setDefaultshared(String defaultshared) {
          this.defaultshared = defaultshared;
      }
    
    public String getNondefaultshared() {
        return nondefaultshared;
    }

      public void setNondefaultshared(String nondefaultshared) {
          this.nondefaultshared = nondefaultshared;
      }
    
    public Double getDsporder() {
        return dsporder;
    }

      public void setDsporder(Double dsporder) {
          this.dsporder = dsporder;
      }
    
    public String getModecode() {
        return modecode;
    }

      public void setModecode(String modecode) {
          this.modecode = modecode;
      }
    
    public Integer getIsdelete() {
        return isdelete;
    }

      public void setIsdelete(Integer isdelete) {
          this.isdelete = isdelete;
      }
    
    public Integer getSubcompanyid() {
        return subcompanyid;
    }

      public void setSubcompanyid(Integer subcompanyid) {
          this.subcompanyid = subcompanyid;
      }
    
//    public Integer getIsallowreply() {
//        return isallowreply;
//    }
//
//      public void setIsallowreply(Integer isallowreply) {
//          this.isallowreply = isallowreply;
//      }
//
//    public Integer getCategorytype() {
//        return categorytype;
//    }
//
//      public void setCategorytype(Integer categorytype) {
//          this.categorytype = categorytype;
//      }
//
//    public Integer getSelectcategory() {
//        return selectcategory;
//    }
//
//      public void setSelectcategory(Integer selectcategory) {
//          this.selectcategory = selectcategory;
//      }
//
//    public Integer getIsimportdetailofeditlayout() {
//        return isimportdetailofeditlayout;
//    }
//
//      public void setIsimportdetailofeditlayout(Integer isimportdetailofeditlayout) {
//          this.isimportdetailofeditlayout = isimportdetailofeditlayout;
//      }
//
//    public Integer getIsaddrightbyworkflow() {
//        return isaddrightbyworkflow;
//    }
//
//      public void setIsaddrightbyworkflow(Integer isaddrightbyworkflow) {
//          this.isaddrightbyworkflow = isaddrightbyworkflow;
//      }
//
//    public Integer getIstagset() {
//        return istagset;
//    }
//
//      public void setIstagset(Integer istagset) {
//          this.istagset = istagset;
//      }
//
//    public Integer getReplyposition() {
//        return replyposition;
//    }
//
//      public void setReplyposition(Integer replyposition) {
//          this.replyposition = replyposition;
//      }
//
//    public Integer getIswatermark() {
//        return iswatermark;
//    }
//
//      public void setIswatermark(Integer iswatermark) {
//          this.iswatermark = iswatermark;
//      }
//
//    public String getFileformat() {
//        return fileformat;
//    }
//
//      public void setFileformat(String fileformat) {
//          this.fileformat = fileformat;
//      }
//
//    public String getEmpowmenttype() {
//        return empowmenttype;
//    }
//
//      public void setEmpowmenttype(String empowmenttype) {
//          this.empowmenttype = empowmenttype;
//      }
//
//    public Integer getExpcardtemplate() {
//        return expcardtemplate;
//    }
//
//      public void setExpcardtemplate(Integer expcardtemplate) {
//          this.expcardtemplate = expcardtemplate;
//      }
//
//    public Integer getIsdelfile() {
//        return isdelfile;
//    }
//
//      public void setIsdelfile(Integer isdelfile) {
//          this.isdelfile = isdelfile;
//      }
//
//    public Integer getIsexpcardexcel() {
//        return isexpcardexcel;
//    }
//
//      public void setIsexpcardexcel(Integer isexpcardexcel) {
//          this.isexpcardexcel = isexpcardexcel;
//      }
//
//    public Integer getIsexpcardhtml() {
//        return isexpcardhtml;
//    }
//
//      public void setIsexpcardhtml(Integer isexpcardhtml) {
//          this.isexpcardhtml = isexpcardhtml;
//      }
//
//    public Integer getIsexpcardword() {
//        return isexpcardword;
//    }
//
//      public void setIsexpcardword(Integer isexpcardword) {
//          this.isexpcardword = isexpcardword;
//      }
//
//    public Integer getIsexpcardpdf() {
//        return isexpcardpdf;
//    }
//
//      public void setIsexpcardpdf(Integer isexpcardpdf) {
//          this.isexpcardpdf = isexpcardpdf;
//      }
//
//    public String getExpcardhtmlindexconfig() {
//        return expcardhtmlindexconfig;
//    }
//
//      public void setExpcardhtmlindexconfig(String expcardhtmlindexconfig) {
//          this.expcardhtmlindexconfig = expcardhtmlindexconfig;
//      }
//
//    public Integer getExpcardhtmltemplate() {
//        return expcardhtmltemplate;
//    }
//
//      public void setExpcardhtmltemplate(Integer expcardhtmltemplate) {
//          this.expcardhtmltemplate = expcardhtmltemplate;
//      }
//
//    public Integer getExpcardwordtemplate() {
//        return expcardwordtemplate;
//    }
//
//      public void setExpcardwordtemplate(Integer expcardwordtemplate) {
//          this.expcardwordtemplate = expcardwordtemplate;
//      }
//
//    public Integer getExpcardpdftemplate() {
//        return expcardpdftemplate;
//    }
//
//      public void setExpcardpdftemplate(Integer expcardpdftemplate) {
//          this.expcardpdftemplate = expcardpdftemplate;
//      }
//
//    public Integer getExpcardtypeexcel() {
//        return expcardtypeexcel;
//    }
//
//      public void setExpcardtypeexcel(Integer expcardtypeexcel) {
//          this.expcardtypeexcel = expcardtypeexcel;
//      }
//
//    public Integer getExpcardtypehtml() {
//        return expcardtypehtml;
//    }
//
//      public void setExpcardtypehtml(Integer expcardtypehtml) {
//          this.expcardtypehtml = expcardtypehtml;
//      }
//
//    public Integer getExpcardtypeword() {
//        return expcardtypeword;
//    }
//
//      public void setExpcardtypeword(Integer expcardtypeword) {
//          this.expcardtypeword = expcardtypeword;
//      }
//
//    public Integer getExpcardtypepdf() {
//        return expcardtypepdf;
//    }
//
//      public void setExpcardtypepdf(Integer expcardtypepdf) {
//          this.expcardtypepdf = expcardtypepdf;
//      }
//
//    public String getFilenameexcel() {
//        return filenameexcel;
//    }
//
//      public void setFilenameexcel(String filenameexcel) {
//          this.filenameexcel = filenameexcel;
//      }
//
//    public String getFilenamehtml() {
//        return filenamehtml;
//    }
//
//      public void setFilenamehtml(String filenamehtml) {
//          this.filenamehtml = filenamehtml;
//      }
//
//    public String getFilenameword() {
//        return filenameword;
//    }
//
//      public void setFilenameword(String filenameword) {
//          this.filenameword = filenameword;
//      }
//
//    public String getFilenamepdf() {
//        return filenamepdf;
//    }
//
//      public void setFilenamepdf(String filenamepdf) {
//          this.filenamepdf = filenamepdf;
//      }
//
//    public String getAttitle() {
//        return attitle;
//    }
//
//      public void setAttitle(String attitle) {
//          this.attitle = attitle;
//      }
//
//    public String getModetitle() {
//        return modetitle;
//    }
//
//      public void setModetitle(String modetitle) {
//          this.modetitle = modetitle;
//      }
//
//    public Integer getIsfrontmultlang() {
//        return isfrontmultlang;
//    }
//
//      public void setIsfrontmultlang(Integer isfrontmultlang) {
//          this.isfrontmultlang = isfrontmultlang;
//      }
//
//    public String getIsatall() {
//        return isatall;
//    }
//
//      public void setIsatall(String isatall) {
//          this.isatall = isatall;
//      }
//
//    public Integer getIsecme() {
//        return isecme;
//    }
//
//      public void setIsecme(Integer isecme) {
//          this.isecme = isecme;
//      }
//
//    public Integer getFileisdisplay() {
//        return fileisdisplay;
//    }
//
//      public void setFileisdisplay(Integer fileisdisplay) {
//          this.fileisdisplay = fileisdisplay;
//      }
//
//    public Integer getIscustomorder() {
//        return iscustomorder;
//    }
//
//      public void setIscustomorder(Integer iscustomorder) {
//          this.iscustomorder = iscustomorder;
//      }
//
//    public String getCustomordertables() {
//        return customordertables;
//    }
//
//      public void setCustomordertables(String customordertables) {
//          this.customordertables = customordertables;
//      }
//
//    public String getAllowlike() {
//        return allowlike;
//    }
//
//      public void setAllowlike(String allowlike) {
//          this.allowlike = allowlike;
//      }
//
//    public String getLikeshowname() {
//        return likeshowname;
//    }
//
//      public void setLikeshowname(String likeshowname) {
//          this.likeshowname = likeshowname;
//      }
//
//    public String getAllowdislike() {
//        return allowdislike;
//    }
//
//      public void setAllowdislike(String allowdislike) {
//          this.allowdislike = allowdislike;
//      }
//
//    public String getDislikeshowname() {
//        return dislikeshowname;
//    }
//
//      public void setDislikeshowname(String dislikeshowname) {
//          this.dislikeshowname = dislikeshowname;
//      }
//
//    public String getDefaultcontraction() {
//        return defaultcontraction;
//    }
//
//      public void setDefaultcontraction(String defaultcontraction) {
//          this.defaultcontraction = defaultcontraction;
//      }
//
//    public Integer getIscustompage() {
//        return iscustompage;
//    }
//
//      public void setIscustompage(Integer iscustompage) {
//          this.iscustompage = iscustompage;
//      }
//
//    public Integer getClassprotect() {
//        return classprotect;
//    }
//
//      public void setClassprotect(Integer classprotect) {
//          this.classprotect = classprotect;
//      }
//
//    public Integer getVirtualrightopen() {
//        return virtualrightopen;
//    }
//
//      public void setVirtualrightopen(Integer virtualrightopen) {
//          this.virtualrightopen = virtualrightopen;
//      }
//
//    public String getCubeuuid() {
//        return cubeuuid;
//    }
//
//      public void setCubeuuid(String cubeuuid) {
//          this.cubeuuid = cubeuuid;
//      }

    @Override
    public String toString() {
        return "Modeinfo{" +
              "id=" + id +
                  ", modename=" + modename +
                  ", modedesc=" + modedesc +
                  ", modetype=" + modetype +
                  ", formid=" + formid +
                  ", maincategory=" + maincategory +
                  ", subcategory=" + subcategory +
                  ", seccategory=" + seccategory +
                  ", isimportdetail=" + isimportdetail +
                  ", codeid=" + codeid +
                  ", custompage=" + custompage +
                  ", defaultshared=" + defaultshared +
                  ", nondefaultshared=" + nondefaultshared +
                  ", dsporder=" + dsporder +
                  ", modecode=" + modecode +
                  ", isdelete=" + isdelete +
                  ", subcompanyid=" + subcompanyid +
//                  ", isallowreply=" + isallowreply +
//                  ", categorytype=" + categorytype +
//                  ", selectcategory=" + selectcategory +
//                  ", isimportdetailofeditlayout=" + isimportdetailofeditlayout +
//                  ", isaddrightbyworkflow=" + isaddrightbyworkflow +
//                  ", istagset=" + istagset +
//                  ", replyposition=" + replyposition +
//                  ", iswatermark=" + iswatermark +
//                  ", fileformat=" + fileformat +
//                  ", empowmenttype=" + empowmenttype +
//                  ", expcardtemplate=" + expcardtemplate +
//                  ", isdelfile=" + isdelfile +
//                  ", isexpcardexcel=" + isexpcardexcel +
//                  ", isexpcardhtml=" + isexpcardhtml +
//                  ", isexpcardword=" + isexpcardword +
//                  ", isexpcardpdf=" + isexpcardpdf +
//                  ", expcardhtmlindexconfig=" + expcardhtmlindexconfig +
//                  ", expcardhtmltemplate=" + expcardhtmltemplate +
//                  ", expcardwordtemplate=" + expcardwordtemplate +
//                  ", expcardpdftemplate=" + expcardpdftemplate +
//                  ", expcardtypeexcel=" + expcardtypeexcel +
//                  ", expcardtypehtml=" + expcardtypehtml +
//                  ", expcardtypeword=" + expcardtypeword +
//                  ", expcardtypepdf=" + expcardtypepdf +
//                  ", filenameexcel=" + filenameexcel +
//                  ", filenamehtml=" + filenamehtml +
//                  ", filenameword=" + filenameword +
//                  ", filenamepdf=" + filenamepdf +
//                  ", attitle=" + attitle +
//                  ", modetitle=" + modetitle +
//                  ", isfrontmultlang=" + isfrontmultlang +
//                  ", isatall=" + isatall +
//                  ", isecme=" + isecme +
//                  ", fileisdisplay=" + fileisdisplay +
//                  ", iscustomorder=" + iscustomorder +
//                  ", customordertables=" + customordertables +
//                  ", allowlike=" + allowlike +
//                  ", likeshowname=" + likeshowname +
//                  ", allowdislike=" + allowdislike +
//                  ", dislikeshowname=" + dislikeshowname +
//                  ", defaultcontraction=" + defaultcontraction +
//                  ", iscustompage=" + iscustompage +
//                  ", classprotect=" + classprotect +
//                  ", virtualrightopen=" + virtualrightopen +
//                  ", cubeuuid=" + cubeuuid +
              "}";
    }
}
