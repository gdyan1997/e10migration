package com.weaver.versionupgrade.entity.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.sql.Timestamp;

import com.baomidou.mybatisplus.annotation.TableField;
import com.weaver.versionupgrade.entity.dto.JavascriptTransDTO;
import weaver.general.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-17
 */
public class JavascriptTrans implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("LAYOUTID")
    private Integer layoutid;

    @TableField("FROMMODULE")
    private String frommodule;

    @TableField("WORKFLOWID")
    private Integer workflowid;

    @TableField("WORKFLOWNAME")
    private String workflowname;

    @TableField("FORMID")
    private Integer formid;

    @TableField("FORMNAME")
    private String formname;

    @TableField("ISBILL")
    private Integer isbill;

    @TableField("NODEID")
    private Integer nodeid;

    @TableField("NODENAME")
    private String nodename;

    @TableField("TYPE")
    private Integer type;

    @TableField("TYPENAME")
    private String typename;

    @TableField("LAYOUTNAME")
    private String layoutname;

    private String scriptstr;

    @TableField("TRANFERCONTENT")
    private String tranfercontent;

    @TableField("TRANFERSTATUS")
    private String tranferstatus;

    @TableField("TRANSFERFLAG")
    private String transferflag;

    @TableField("HASCSSSTYLE")
    private String hascssstyle;

    @TableField("HASSRCSCRIPT")
    private String hassrcscript;

    @TableField("ISACTIVE")
    private Integer isactive;

    @TableField("CREATETIME")
    private Timestamp createtime;

    @TableField("UPDATETIME")
    private Timestamp updatetime;

    public Integer getLayoutid() {
        return layoutid;
    }

    public void setLayoutid(Integer layoutid) {
        this.layoutid = layoutid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFrommodule() {
        return frommodule;
    }

    public void setFrommodule(String frommodule) {
        this.frommodule = frommodule;
    }

    public Integer getWorkflowid() {
        return workflowid;
    }

    public void setWorkflowid(Integer workflowid) {
        this.workflowid = workflowid;
    }

    public String getWorkflowname() {
        return workflowname;
    }

    public void setWorkflowname(String workflowname) {
        this.workflowname = workflowname;
    }

    public Integer getFormid() {
        return formid;
    }

    public void setFormid(Integer formid) {
        this.formid = formid;
    }

    public String getFormname() {
        return formname;
    }

    public void setFormname(String formname) {
        this.formname = formname;
    }

    public Integer getIsbill() {
        return isbill;
    }

    public void setIsbill(Integer isbill) {
        this.isbill = isbill;
    }

    public Integer getNodeid() {
        return nodeid;
    }

    public void setNodeid(Integer nodeid) {
        this.nodeid = nodeid;
    }

    public String getNodename() {
        return nodename;
    }

    public void setNodename(String nodename) {
        this.nodename = nodename;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getLayoutname() {
        return layoutname;
    }

    public void setLayoutname(String layoutname) {
        this.layoutname = layoutname;
    }

    public String getScriptstr() {
        return scriptstr;
    }

    public void setScriptstr(String scriptstr) {
        this.scriptstr = scriptstr;
    }

    public String getTranfercontent() {
        return tranfercontent;
    }

    public void setTranfercontent(String tranfercontent) {
        this.tranfercontent = tranfercontent;
    }

    public String getTranferstatus() {
        return tranferstatus;
    }

    public void setTranferstatus(String tranferstatus) {
        this.tranferstatus = tranferstatus;
    }

    public String getTransferflag() {
        return transferflag;
    }

    public String showTransferstatus(){

        try {
            if (this.tranferstatus != null) {
                // 将 JSON 字符串转换为 ArrayList
                List<Object> tranferstatusArr = JSON.parseArray(this.tranferstatus);

                // 收集信息示例
                StringBuilder collectedInfo = new StringBuilder();
                for (int i = 0; i < tranferstatusArr.size(); i++) {
                    collectedInfo.append(i + 1).append(". ").append(tranferstatusArr.get(i).toString()).append("\n");
                }
                return collectedInfo.toString();
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return this.tranferstatus;
    }

    public void setTransferflag(String transferflag) {
        this.transferflag = transferflag;
    }

    public String getHascssstyle() {
        return hascssstyle;
    }

    public void setHascssstyle(String hascssstyle) {
        this.hascssstyle = hascssstyle;
    }

    public String getHassrcscript() {
        return hassrcscript;
    }

    public void setHassrcscript(String hassrcscript) {
        this.hassrcscript = hassrcscript;
    }

    public Integer getIsactive() {
        return isactive;
    }

    public void setIsactive(Integer isactive) {
        this.isactive = isactive;
    }

    public Timestamp getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Timestamp createtime) {
        this.createtime = createtime;
    }

    public Timestamp getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Timestamp updatetime) {
        this.updatetime = updatetime;
    }

    public JavascriptTransDTO trans(){
        JavascriptTransDTO javascriptTransDTO = new JavascriptTransDTO();
        javascriptTransDTO.setId(this.id);
        javascriptTransDTO.setFrommodule(this.frommodule);
        javascriptTransDTO.setWorkflowid(Util.null2String(this.workflowid));
        javascriptTransDTO.setWorkflowname(this.workflowname);
        javascriptTransDTO.setTypename(this.typename);
        javascriptTransDTO.setLayoutname(this.layoutname);
        javascriptTransDTO.setFormname(this.formname);
        javascriptTransDTO.setNodename(this.nodename);
        boolean stringGreaterThan1MB = isStringGreaterThan1MB(this.scriptstr);
        javascriptTransDTO.setScriptstr(stringGreaterThan1MB?"数据内容过大，请手动处理。":this.scriptstr);
        javascriptTransDTO.setHassrcscript(this.hassrcscript);
        javascriptTransDTO.setHascssstyle(this.hascssstyle);
        javascriptTransDTO.setTranfercontent(stringGreaterThan1MB?"数据内容过大，请手动处理":this.tranfercontent);
        javascriptTransDTO.setTransferflag(stringGreaterThan1MB?"请手动处理":this.transferflag);
        try{
            if(this.tranferstatus != null)
                javascriptTransDTO.setTranferstatus(JSONObject.parseObject(this.tranferstatus, ArrayList.class));
        }catch (Exception ex){

        }
        return javascriptTransDTO;
    }

    public static boolean isStringGreaterThan1MB(String str) {
        if (str == null) {
            return false;
        }
        // 获取字符串的字节表示，使用默认字符集
        byte[] byteArray = str.getBytes();
        // 检查字节数组的长度是否大于1MB
        return byteArray.length > 1048576; // 1MB = 1048576 bytes
    }
}
