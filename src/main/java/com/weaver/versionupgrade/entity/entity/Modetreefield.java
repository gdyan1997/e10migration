package com.weaver.versionupgrade.entity.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
public class Modetreefield implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 自增主键
     */
        @TableId(value = "ID", type = IdType.AUTO)
      private Integer id;

    @TableField("TREEFIELDNAME")
    private String treefieldname;

    @TableField("SUPERFIELDID")
    private Integer superfieldid;

    @TableField("ALLSUPERFIELDID")
    private String allsuperfieldid;

    @TableField("TREELEVEL")
    private Integer treelevel;

    @TableField("ISLAST")
    private String islast;

    @TableField("showOrder")
    private BigDecimal showorder;

    @TableField("TREEFIELDDESC")
    private String treefielddesc;

    @TableField("ISDELETE")
    private Integer isdelete;

    @TableField("SUBCOMPANYID")
    private Integer subcompanyid;

//    private String icon;
//
//    @TableField("iconColor")
//    private String iconcolor;
//
//    @TableField("iconBg")
//    private String iconbg;
//
//    private Integer mobileappid;
//
//    private String cubeuuid;

    
    public Integer getId() {
        return id;
    }

      public void setId(Integer id) {
          this.id = id;
      }
    
    public String getTreefieldname() {
        return treefieldname;
    }

      public void setTreefieldname(String treefieldname) {
          this.treefieldname = treefieldname;
      }
    
    public Integer getSuperfieldid() {
        return superfieldid;
    }

      public void setSuperfieldid(Integer superfieldid) {
          this.superfieldid = superfieldid;
      }
    
    public String getAllsuperfieldid() {
        return allsuperfieldid;
    }

      public void setAllsuperfieldid(String allsuperfieldid) {
          this.allsuperfieldid = allsuperfieldid;
      }
    
    public Integer getTreelevel() {
        return treelevel;
    }

      public void setTreelevel(Integer treelevel) {
          this.treelevel = treelevel;
      }
    
    public String getIslast() {
        return islast;
    }

      public void setIslast(String islast) {
          this.islast = islast;
      }
    
    public BigDecimal getShoworder() {
        return showorder;
    }

      public void setShoworder(BigDecimal showorder) {
          this.showorder = showorder;
      }
    
    public String getTreefielddesc() {
        return treefielddesc;
    }

      public void setTreefielddesc(String treefielddesc) {
          this.treefielddesc = treefielddesc;
      }
    
    public Integer getIsdelete() {
        return isdelete;
    }

      public void setIsdelete(Integer isdelete) {
          this.isdelete = isdelete;
      }
    
    public Integer getSubcompanyid() {
        return subcompanyid;
    }

      public void setSubcompanyid(Integer subcompanyid) {
          this.subcompanyid = subcompanyid;
      }
    
//    public String getIcon() {
//        return icon;
//    }
//
//      public void setIcon(String icon) {
//          this.icon = icon;
//      }
//
//    public String getIconcolor() {
//        return iconcolor;
//    }
//
//      public void setIconcolor(String iconcolor) {
//          this.iconcolor = iconcolor;
//      }
//
//    public String getIconbg() {
//        return iconbg;
//    }
//
//      public void setIconbg(String iconbg) {
//          this.iconbg = iconbg;
//      }
//
//    public Integer getMobileappid() {
//        return mobileappid;
//    }
//
//      public void setMobileappid(Integer mobileappid) {
//          this.mobileappid = mobileappid;
//      }
//
//    public String getCubeuuid() {
//        return cubeuuid;
//    }
//
//      public void setCubeuuid(String cubeuuid) {
//          this.cubeuuid = cubeuuid;
//      }

    @Override
    public String toString() {
        return "Modetreefield{" +
              "id=" + id +
                  ", treefieldname=" + treefieldname +
                  ", superfieldid=" + superfieldid +
                  ", allsuperfieldid=" + allsuperfieldid +
                  ", treelevel=" + treelevel +
                  ", islast=" + islast +
                  ", showorder=" + showorder +
                  ", treefielddesc=" + treefielddesc +
                  ", isdelete=" + isdelete +
                  ", subcompanyid=" + subcompanyid +
//                  ", icon=" + icon +
//                  ", iconcolor=" + iconcolor +
//                  ", iconbg=" + iconbg +
//                  ", mobileappid=" + mobileappid +
//                  ", cubeuuid=" + cubeuuid +
              "}";
    }
}
