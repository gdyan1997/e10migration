package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowBill implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("ID")
    private Integer id;

    @TableField("NAMELABEL")
    private Integer namelabel;

    @TableField("TABLENAME")
    private String tablename;

    @TableField("CREATEPAGE")
    private String createpage;

    @TableField("MANAGEPAGE")
    private String managepage;

    @TableField("VIEWPAGE")
    private String viewpage;

    @TableField("DETAILTABLENAME")
    private String detailtablename;

    @TableField("DETAILKEYFIELD")
    private String detailkeyfield;

    /**
     * @TableField("OPERATIONPAGE") private String operationpage;
     * @TableField("HASFILEUP") private String hasfileup;
     * @TableField("INVALID") private Integer invalid;
     * @TableField("FORMDES") private String formdes;
     * @TableField("SUBCOMPANYID") private Integer subcompanyid;
     * @TableField("DSPORDER") private Double dsporder;
     * @TableField("SUBCOMPANYID3") private Integer subcompanyid3;
     * @TableField("FROM_MODULE_") private String fromModule;
     * <p>
     * private String clazz;
     * <p>
     * private String uuid;
     **/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNamelabel() {
        return namelabel;
    }

    public void setNamelabel(Integer namelabel) {
        this.namelabel = namelabel;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getCreatepage() {
        return createpage;
    }

    public void setCreatepage(String createpage) {
        this.createpage = createpage;
    }

    public String getManagepage() {
        return managepage;
    }

    public void setManagepage(String managepage) {
        this.managepage = managepage;
    }

    public String getViewpage() {
        return viewpage;
    }

    public void setViewpage(String viewpage) {
        this.viewpage = viewpage;
    }

    public String getDetailtablename() {
        return detailtablename;
    }

    public void setDetailtablename(String detailtablename) {
        this.detailtablename = detailtablename;
    }

    public String getDetailkeyfield() {
        return detailkeyfield;
    }

    public void setDetailkeyfield(String detailkeyfield) {
        this.detailkeyfield = detailkeyfield;
    }

    /**
    public String getOperationpage() {
        return operationpage;
    }

    public void setOperationpage(String operationpage) {
        this.operationpage = operationpage;
    }

    public String getHasfileup() {
        return hasfileup;
    }

    public void setHasfileup(String hasfileup) {
        this.hasfileup = hasfileup;
    }

    public Integer getInvalid() {
        return invalid;
    }

    public void setInvalid(Integer invalid) {
        this.invalid = invalid;
    }

    public String getFormdes() {
        return formdes;
    }

    public void setFormdes(String formdes) {
        this.formdes = formdes;
    }

    public Integer getSubcompanyid() {
        return subcompanyid;
    }

    public void setSubcompanyid(Integer subcompanyid) {
        this.subcompanyid = subcompanyid;
    }

    public Double getDsporder() {
        return dsporder;
    }

    public void setDsporder(Double dsporder) {
        this.dsporder = dsporder;
    }

    public Integer getSubcompanyid3() {
        return subcompanyid3;
    }

    public void setSubcompanyid3(Integer subcompanyid3) {
        this.subcompanyid3 = subcompanyid3;
    }

    public String getFromModule() {
        return fromModule;
    }

    public void setFromModule(String fromModule) {
        this.fromModule = fromModule;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    **/
    @Override
    public String toString() {
        return "WorkflowBill{" +
                "id=" + id +
                ", namelabel=" + namelabel +
                ", tablename=" + tablename +
                ", createpage=" + createpage +
                ", managepage=" + managepage +
                ", viewpage=" + viewpage +
                ", detailtablename=" + detailtablename +
                ", detailkeyfield=" + detailkeyfield +
//                ", operationpage=" + operationpage +
//                ", hasfileup=" + hasfileup +
//                ", invalid=" + invalid +
//                ", formdes=" + formdes +
//                ", subcompanyid=" + subcompanyid +
//                ", dsporder=" + dsporder +
//                ", subcompanyid3=" + subcompanyid3 +
//                ", fromModule=" + fromModule +
//                ", clazz=" + clazz +
//                ", uuid=" + uuid +
                "}";
    }
}
