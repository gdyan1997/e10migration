package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowNodehtmllayout implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    private Integer id;

    @TableField("WORKFLOWID")
    private Integer workflowid;

    @TableField("FORMID")
    private Integer formid;

    @TableField("ISBILL")
    private Integer isbill;

    @TableField("NODEID")
    private Integer nodeid;

    @TableField("TYPE")
    private Integer type;

    @TableField("LAYOUTNAME")
    private String layoutname;

    @TableField("SYSPATH")
    private String syspath;

    @TableField("CSSFILE")
    private Integer cssfile;

    @TableField("HTMLPARSESCHEME")
    private Integer htmlparsescheme;

    @TableField("VERSION")
    private Integer version;

    @TableField("OPERUSER")
    private Integer operuser;

    @TableField("OPERTIME")
    private String opertime;

    @TableField("DATAJSON")
    private String datajson;

    @TableField("PLUGINJSON")
    private String pluginjson;

    @TableField("SCRIPTS")
    private String scripts;

//    @TableField("ISACTIVE")
//    private Integer isactive;

    private String scriptstr;

    private String stylestr;

    private String uuid;

    @TableField("isFixed")
    private String isfixed;

    @TableField("initScripts")
    private String initscripts;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWorkflowid() {
        return workflowid;
    }

    public void setWorkflowid(Integer workflowid) {
        this.workflowid = workflowid;
    }

    public Integer getFormid() {
        return formid;
    }

    public void setFormid(Integer formid) {
        this.formid = formid;
    }

    public Integer getIsbill() {
        return isbill;
    }

    public void setIsbill(Integer isbill) {
        this.isbill = isbill;
    }

    public Integer getNodeid() {
        return nodeid;
    }

    public void setNodeid(Integer nodeid) {
        this.nodeid = nodeid;
    }

    public Integer getType() {
        return type;
    }

    //tmp.getType() == 0 ? "显示模版" : ((tmp.getType() == 1) ? "打印模版" : "移动模版")
    public String getTypeName() {
        switch (type) {
            case 0:
                return "显示模版";
            case 1:
                return "打印模版";
            case 2:
                return "移动模版";
            default:
                return "";
        }
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getLayoutname() {
        return layoutname;
    }

    public void setLayoutname(String layoutname) {
        this.layoutname = layoutname;
    }

    public String getSyspath() {
        return syspath;
    }

    public void setSyspath(String syspath) {
        this.syspath = syspath;
    }

    public Integer getCssfile() {
        return cssfile;
    }

    public void setCssfile(Integer cssfile) {
        this.cssfile = cssfile;
    }

    public Integer getHtmlparsescheme() {
        return htmlparsescheme;
    }

    public void setHtmlparsescheme(Integer htmlparsescheme) {
        this.htmlparsescheme = htmlparsescheme;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getOperuser() {
        return operuser;
    }

    public void setOperuser(Integer operuser) {
        this.operuser = operuser;
    }

    public String getOpertime() {
        return opertime;
    }

    public void setOpertime(String opertime) {
        this.opertime = opertime;
    }

    public String getDatajson() {
        return datajson;
    }

    public void setDatajson(String datajson) {
        this.datajson = datajson;
    }

    public String getPluginjson() {
        return pluginjson;
    }

    public void setPluginjson(String pluginjson) {
        this.pluginjson = pluginjson;
    }

    public String getScripts() {
        return scripts;
    }

    public void setScripts(String scripts) {
        this.scripts = scripts;
    }

//    public Integer getIsactive() {
//        return isactive;
//    }
//
//    public void setIsactive(Integer isactive) {
//        this.isactive = isactive;
//    }

    public String getScriptstr() {
        return scriptstr;
    }

    public void setScriptstr(String scriptstr) {
        this.scriptstr = scriptstr;
    }

    public String getStylestr() {
        return stylestr;
    }

    public void setStylestr(String stylestr) {
        this.stylestr = stylestr;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIsfixed() {
        return isfixed;
    }

    public void setIsfixed(String isfixed) {
        this.isfixed = isfixed;
    }

    public String getInitscripts() {
        return initscripts;
    }

    public void setInitscripts(String initscripts) {
        this.initscripts = initscripts;
    }

    @Override
    public String toString() {
        return "WorkflowNodehtmllayout{" +
                "id=" + id +
                ", workflowid=" + workflowid +
                ", formid=" + formid +
                ", isbill=" + isbill +
                ", nodeid=" + nodeid +
                ", type=" + type +
                ", layoutname=" + layoutname +
                ", syspath=" + syspath +
                ", cssfile=" + cssfile +
                ", htmlparsescheme=" + htmlparsescheme +
                ", version=" + version +
                ", operuser=" + operuser +
                ", opertime=" + opertime +
                ", datajson=" + datajson +
                ", pluginjson=" + pluginjson +
                ", scripts=" + scripts +
//                ", isactive=" + isactive +
                ", scriptstr=" + scriptstr +
                ", stylestr=" + stylestr +
                ", uuid=" + uuid +
                ", isfixed=" + isfixed +
                ", initscripts=" + initscripts +
                "}";
    }
}
