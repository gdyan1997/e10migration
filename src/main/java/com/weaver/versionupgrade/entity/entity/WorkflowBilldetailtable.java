package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import idtoolutil.Constant;
import weaver.general.Util;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowBilldetailtable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("BILLID")
    private Integer billid;

    @TableField("TABLENAME")
    private String tablename;

//    @TableField("TITLE")
//    private String title;

    @TableField("ORDERID")
    private Integer orderid;

//    private String uuid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getE10Id() {
        return TransUtil.getNewTableID("sub_form", "id", "workflow_billdetailtable", Util.null2String(id));
    }

    public String getE10IdForCube(String modeid) {
        // 为了保证建模表迁移的明细表id 唯一且有序  所有的id转换都我这里计算
        // e9_sub_formid 10548  e9_modeid 21846
        // 109001054800021846

        String e10id = Constant.TenantKeyNumber + "9";

        return e10id + generateIdLength(Util.null2String(id), 7) + generateIdLength(modeid, 8);
    }

    public String generateIdLength(String e9id, int len) {

        e9id = Util.getIntValue(e9id) + "";

        if (e9id.length() < len) {
            len = len - e9id.length();

            while (len-- >0) {

                e9id  = "0" + e9id;
            }

        }

        return e9id;


    }

    public Integer getBillid() {
        return billid;
    }

    public void setBillid(Integer billid) {
        this.billid = billid;
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }
//
//    public String getUuid() {
//        return uuid;
//    }
//
//    public void setUuid(String uuid) {
//        this.uuid = uuid;
//    }

    @Override
    public String toString() {
        return "WorkflowBilldetailtable{" +
                "id=" + id +
                ", billid=" + billid +
                ", tablename=" + tablename +
//                ", title=" + title +
//                ", orderid=" + orderid +
//                ", uuid=" + uuid +
                "}";
    }
}
