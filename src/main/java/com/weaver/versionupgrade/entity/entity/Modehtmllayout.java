package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
public class Modehtmllayout implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 自增主键
     */
        @TableId(value = "ID", type = IdType.AUTO)
      private Integer id;

    @TableField("MODEID")
    private Integer modeid;

    @TableField("FORMID")
    private Integer formid;

    @TableField("TYPE")
    private Integer type;

    @TableField("LAYOUTNAME")
    private String layoutname;

    @TableField("SYSPATH")
    private String syspath;

    @TableField("COLSPERROW")
    private Integer colsperrow;

    @TableField("CSSFILE")
    private Integer cssfile;

    @TableField("ISDEFAULT")
    private Integer isdefault;

    @TableField("VERSION")
    private Integer version;

    @TableField("OPERUSER")
    private Integer operuser;

    @TableField("OPERTIME")
    private String opertime;

    @TableField("DATAJSON")
    private String datajson;

    @TableField("PLUGINJSON")
    private String pluginjson;

    @TableField("SCRIPTS")
    private String scripts;

    private String scriptstr;

    private String stylestr;

//    private Integer feaid;
//
//    private Integer isecme;
//
//    @TableField("secondPassword")
//    private String secondpassword;
//
//    @TableField("dsDesignerid")
//    private String dsdesignerid;
//
//    private Integer isquickedit;
//
//    private String secondauth;
//
//    private Integer doubleauth;
//
//    private Integer authverifier;
//
//    private String cubeuuid;

    
    public Integer getId() {
        return id;
    }

      public void setId(Integer id) {
          this.id = id;
      }
    
    public Integer getModeid() {
        return modeid;
    }

      public void setModeid(Integer modeid) {
          this.modeid = modeid;
      }
    
    public Integer getFormid() {
        return formid;
    }

      public void setFormid(Integer formid) {
          this.formid = formid;
      }
    
    public Integer getType() {
        return type;
    }

      public void setType(Integer type) {
          this.type = type;
      }
    
    public String getLayoutname() {
        return layoutname;
    }

      public void setLayoutname(String layoutname) {
          this.layoutname = layoutname;
      }
    
    public String getSyspath() {
        return syspath;
    }

      public void setSyspath(String syspath) {
          this.syspath = syspath;
      }
    
    public Integer getColsperrow() {
        return colsperrow;
    }

      public void setColsperrow(Integer colsperrow) {
          this.colsperrow = colsperrow;
      }
    
    public Integer getCssfile() {
        return cssfile;
    }

      public void setCssfile(Integer cssfile) {
          this.cssfile = cssfile;
      }
    
    public Integer getIsdefault() {
        return isdefault;
    }

      public void setIsdefault(Integer isdefault) {
          this.isdefault = isdefault;
      }
    
    public Integer getVersion() {
        return version;
    }

      public void setVersion(Integer version) {
          this.version = version;
      }
    
    public Integer getOperuser() {
        return operuser;
    }

      public void setOperuser(Integer operuser) {
          this.operuser = operuser;
      }
    
    public String getOpertime() {
        return opertime;
    }

      public void setOpertime(String opertime) {
          this.opertime = opertime;
      }
    
    public String getDatajson() {
        return datajson;
    }

      public void setDatajson(String datajson) {
          this.datajson = datajson;
      }
    
    public String getPluginjson() {
        return pluginjson;
    }

      public void setPluginjson(String pluginjson) {
          this.pluginjson = pluginjson;
      }
    
    public String getScripts() {
        return scripts;
    }

      public void setScripts(String scripts) {
          this.scripts = scripts;
      }
    
    public String getScriptstr() {
        return scriptstr;
    }

      public void setScriptstr(String scriptstr) {
          this.scriptstr = scriptstr;
      }
    
    public String getStylestr() {
        return stylestr;
    }

      public void setStylestr(String stylestr) {
          this.stylestr = stylestr;
      }
    
//    public Integer getFeaid() {
//        return feaid;
//    }
//
//      public void setFeaid(Integer feaid) {
//          this.feaid = feaid;
//      }
//
//    public Integer getIsecme() {
//        return isecme;
//    }
//
//      public void setIsecme(Integer isecme) {
//          this.isecme = isecme;
//      }
//
//    public String getSecondpassword() {
//        return secondpassword;
//    }
//
//      public void setSecondpassword(String secondpassword) {
//          this.secondpassword = secondpassword;
//      }
//
//    public String getDsdesignerid() {
//        return dsdesignerid;
//    }
//
//      public void setDsdesignerid(String dsdesignerid) {
//          this.dsdesignerid = dsdesignerid;
//      }
//
//    public Integer getIsquickedit() {
//        return isquickedit;
//    }
//
//      public void setIsquickedit(Integer isquickedit) {
//          this.isquickedit = isquickedit;
//      }
//
//    public String getSecondauth() {
//        return secondauth;
//    }
//
//      public void setSecondauth(String secondauth) {
//          this.secondauth = secondauth;
//      }
//
//    public Integer getDoubleauth() {
//        return doubleauth;
//    }
//
//      public void setDoubleauth(Integer doubleauth) {
//          this.doubleauth = doubleauth;
//      }
//
//    public Integer getAuthverifier() {
//        return authverifier;
//    }
//
//      public void setAuthverifier(Integer authverifier) {
//          this.authverifier = authverifier;
//      }
//
//    public String getCubeuuid() {
//        return cubeuuid;
//    }
//
//      public void setCubeuuid(String cubeuuid) {
//          this.cubeuuid = cubeuuid;
//      }

    @Override
    public String toString() {
        return "Modehtmllayout{" +
              "id=" + id +
                  ", modeid=" + modeid +
                  ", formid=" + formid +
                  ", type=" + type +
                  ", layoutname=" + layoutname +
                  ", syspath=" + syspath +
                  ", colsperrow=" + colsperrow +
                  ", cssfile=" + cssfile +
                  ", isdefault=" + isdefault +
                  ", version=" + version +
                  ", operuser=" + operuser +
                  ", opertime=" + opertime +
                  ", datajson=" + datajson +
                  ", pluginjson=" + pluginjson +
                  ", scripts=" + scripts +
                  ", scriptstr=" + scriptstr +
                  ", stylestr=" + stylestr +
//                  ", feaid=" + feaid +
//                  ", isecme=" + isecme +
//                  ", secondpassword=" + secondpassword +
//                  ", dsdesignerid=" + dsdesignerid +
//                  ", isquickedit=" + isquickedit +
//                  ", secondauth=" + secondauth +
//                  ", doubleauth=" + doubleauth +
//                  ", authverifier=" + authverifier +
//                  ", cubeuuid=" + cubeuuid +
              "}";
    }
}
