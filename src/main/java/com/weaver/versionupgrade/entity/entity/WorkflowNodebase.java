package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public class WorkflowNodebase implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 自增主键
     */
      private Integer id;

    @TableField("NODENAME")
    private String nodename;

    @TableField("ISSTART")
    private String isstart;

    @TableField("ISREJECT")
    private String isreject;

    @TableField("ISREOPEN")
    private String isreopen;

    @TableField("ISEND")
    private String isend;

    @TableField("DRAWXPOS")
    private Integer drawxpos;

    @TableField("DRAWYPOS")
    private Integer drawypos;

    @TableField("TOTALGROUPS")
    private Integer totalgroups;

    @TableField("NODEATTRIBUTE")
    private String nodeattribute;

    @TableField("PASSNUM")
    private Integer passnum;

    @TableField("ISFREENODE")
    private String isfreenode;

    @TableField("FLOWORDER")
    private Integer floworder;

    @TableField("SIGNTYPE")
    private String signtype;

    @TableField("OPERATORS_1")
    private String operators1;

    @TableField("REQUESTID")
    private Integer requestid;

    @TableField("STARTNODEID")
    private Integer startnodeid;

    @TableField("OPERATORS")
    private String operators;

/**
    @TableField("ECOLOGY_PINYIN_SEARCH")
    private String ecologyPinyinSearch;

    private String drawstyle;

    private String uuid;
**/
    
    public Integer getId() {
        return id;
    }

      public void setId(Integer id) {
          this.id = id;
      }
    
    public String getNodename() {
        return nodename;
    }

      public void setNodename(String nodename) {
          this.nodename = nodename;
      }
    
    public String getIsstart() {
        return isstart;
    }

      public void setIsstart(String isstart) {
          this.isstart = isstart;
      }
    
    public String getIsreject() {
        return isreject;
    }

      public void setIsreject(String isreject) {
          this.isreject = isreject;
      }
    
    public String getIsreopen() {
        return isreopen;
    }

      public void setIsreopen(String isreopen) {
          this.isreopen = isreopen;
      }
    
    public String getIsend() {
        return isend;
    }

      public void setIsend(String isend) {
          this.isend = isend;
      }
    
    public Integer getDrawxpos() {
        return drawxpos;
    }

      public void setDrawxpos(Integer drawxpos) {
          this.drawxpos = drawxpos;
      }
    
    public Integer getDrawypos() {
        return drawypos;
    }

      public void setDrawypos(Integer drawypos) {
          this.drawypos = drawypos;
      }
    
    public Integer getTotalgroups() {
        return totalgroups;
    }

      public void setTotalgroups(Integer totalgroups) {
          this.totalgroups = totalgroups;
      }
    
    public String getNodeattribute() {
        return nodeattribute;
    }

      public void setNodeattribute(String nodeattribute) {
          this.nodeattribute = nodeattribute;
      }
    
    public Integer getPassnum() {
        return passnum;
    }

      public void setPassnum(Integer passnum) {
          this.passnum = passnum;
      }
    
    public String getIsfreenode() {
        return isfreenode;
    }

      public void setIsfreenode(String isfreenode) {
          this.isfreenode = isfreenode;
      }
    
    public Integer getFloworder() {
        return floworder;
    }

      public void setFloworder(Integer floworder) {
          this.floworder = floworder;
      }
    
    public String getSigntype() {
        return signtype;
    }

      public void setSigntype(String signtype) {
          this.signtype = signtype;
      }
    
    public String getOperators1() {
        return operators1;
    }

      public void setOperators1(String operators1) {
          this.operators1 = operators1;
      }
    
    public Integer getRequestid() {
        return requestid;
    }

      public void setRequestid(Integer requestid) {
          this.requestid = requestid;
      }
    
    public Integer getStartnodeid() {
        return startnodeid;
    }

      public void setStartnodeid(Integer startnodeid) {
          this.startnodeid = startnodeid;
      }
    
    public String getOperators() {
        return operators;
    }

      public void setOperators(String operators) {
          this.operators = operators;
      }

/**
    public String getEcologyPinyinSearch() {
        return ecologyPinyinSearch;
    }

      public void setEcologyPinyinSearch(String ecologyPinyinSearch) {
          this.ecologyPinyinSearch = ecologyPinyinSearch;
      }
    
    public String getDrawstyle() {
        return drawstyle;
    }

      public void setDrawstyle(String drawstyle) {
          this.drawstyle = drawstyle;
      }
    
    public String getUuid() {
        return uuid;
    }

      public void setUuid(String uuid) {
          this.uuid = uuid;
      }
 **/

    @Override
    public String toString() {
        return "WorkflowNodebase{" +
              "id=" + id +
                  ", nodename=" + nodename +
                  ", isstart=" + isstart +
                  ", isreject=" + isreject +
                  ", isreopen=" + isreopen +
                  ", isend=" + isend +
                  ", drawxpos=" + drawxpos +
                  ", drawypos=" + drawypos +
                  ", totalgroups=" + totalgroups +
                  ", nodeattribute=" + nodeattribute +
                  ", passnum=" + passnum +
                  ", isfreenode=" + isfreenode +
                  ", floworder=" + floworder +
                  ", signtype=" + signtype +
                  ", operators1=" + operators1 +
                  ", requestid=" + requestid +
                  ", startnodeid=" + startnodeid +
                  ", operators=" + operators +
//                  ", ecologyPinyinSearch=" + ecologyPinyinSearch +
//                  ", drawstyle=" + drawstyle +
//                  ", uuid=" + uuid +
              "}";
    }
}
