package com.weaver.versionupgrade.entity.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Wy
 * @since 2024-04-02
 */
public class WorkflowBase implements Serializable {

    private static final long serialVersionUID = 1L;

      /**
     * 自增主键
     */
      private Integer id;

    @TableField("WORKFLOWNAME")
    private String workflowname;

    @TableField("WORKFLOWDESC")
    private String workflowdesc;

    @TableField("WORKFLOWTYPE")
    private Integer workflowtype;

    @TableField("SECURELEVEL")
    private String securelevel;

    @TableField("FORMID")
    private Integer formid;

    @TableField("USERID")
    private Integer userid;

    @TableField("ISBILL")
    private String isbill;

    @TableField("ISCUST")
    private Integer iscust;

    @TableField("HELPDOCID")
    private Integer helpdocid;

    @TableField("ISVALID")
    private String isvalid;

    @TableField("NEEDMARK")
    private String needmark;

    @TableField("MESSAGETYPE")
    private Integer messagetype;

    @TableField("MULTISUBMIT")
    private Integer multisubmit;

    @TableField("DEFAULTNAME")
    private Integer defaultname;

    @TableField("DOCPATH")
    private String docpath;

    @TableField("SUBCOMPANYID")
    private Integer subcompanyid;

//    @TableField("MAILMESSAGETYPE")
//    private Integer mailmessagetype;
//
//    @TableField("DOCRIGHTBYOPERATOR")
//    private Integer docrightbyoperator;
//
//    @TableField("DOCCATEGORY")
//    private String doccategory;

//    @TableField("ISTEMPLATE")
//    private String istemplate;
//
//    @TableField("TEMPLATEID")
//    private Integer templateid;
//
//    @TableField("CATELOGTYPE")
//    private Integer catelogtype;
//
//    @TableField("SELECTEDCATELOG")
//    private Integer selectedcatelog;
//
//    @TableField("DOCRIGHTBYHRMRESOURCE")
//    private Integer docrightbyhrmresource;
//
//    @TableField("NEEDAFFIRMANCE")
//    private String needaffirmance;
//
//    @TableField("ISREMARKS")
//    private String isremarks;
//
//    @TableField("ISANNEXUPLOAD")
//    private String isannexupload;
//
//    @TableField("ANNEXDOCCATEGORY")
//    private String annexdoccategory;
//
//    @TableField("ISSHOWONREPORTINPUT")
//    private String isshowonreportinput;
//
//    @TableField("TITLEFIELDID")
//    private Integer titlefieldid;
//
//    @TableField("KEYWORDFIELDID")
//    private Integer keywordfieldid;
//
//    @TableField("ISSHOWCHART")
//    private String isshowchart;
//
//    @TableField("ORDERBYTYPE")
//    private String orderbytype;
//
//    @TableField("ISTRIDIFFWORKFLOW")
//    private String istridiffworkflow;
//
//    @TableField("ISMODIFYLOG")
//    private String ismodifylog;
//
//    @TableField("IFVERSION")
//    private String ifversion;
//
//    @TableField("WFDOCPATH")
//    private String wfdocpath;
//
//    @TableField("WFDOCOWNER")
//    private String wfdocowner;
//
//    @TableField("ISEDIT")
//    private String isedit;
//
//    @TableField("EDITOR")
//    private Integer editor;
//
//    @TableField("EDITDATE")
//    private String editdate;
//
//    @TableField("EDITTIME")
//    private String edittime;
//
//    @TableField("SHOWDELBUTTONBYREJECT")
//    private String showdelbuttonbyreject;
//
//    @TableField("SHOWUPLOADTAB")
//    private String showuploadtab;
//
//    @TableField("ISSIGNDOC")
//    private String issigndoc;
//
//    @TableField("SHOWDOCTAB")
//    private String showdoctab;
//
//    @TableField("ISSIGNWORKFLOW")
//    private String issignworkflow;
//
//    @TableField("SHOWWORKFLOWTAB")
//    private String showworkflowtab;
//
//    @TableField("CANDELACC")
//    private String candelacc;
//
//    @TableField("ISFORWARDRIGHTS")
//    private String isforwardrights;
//
//    @TableField("ISIMPORTWF")
//    private String isimportwf;
//
//    @TableField("ISREJECTREMIND")
//    private String isrejectremind;
//
//    @TableField("ISCHANGREJECTNODE")
//    private String ischangrejectnode;
//
//    @TableField("WFDOCOWNERTYPE")
//    private Integer wfdocownertype;
//
//    @TableField("WFDOCOWNERFIELDID")
//    private Integer wfdocownerfieldid;
//
//    @TableField("NEWDOCPATH")
//    private String newdocpath;
//
//    @TableField("KEEPSIGN")
//    private Integer keepsign;
//
//    @TableField("SECCATEGORYID")
//    private Integer seccategoryid;
//
//    @TableField("CUSTOMPAGE")
//    private String custompage;
//
//    @TableField("ISSIGNVIEW")
//    private Integer issignview;
//
//    @TableField("ISSELECTREJECTNODE")
//    private String isselectrejectnode;
//
//    @TableField("FORBIDATTDOWNLOAD")
//    private Integer forbidattdownload;
//
//    @TableField("ISIMPORTDETAIL")
//    private String isimportdetail;
//
//    @TableField("SPECIALAPPROVAL")
//    private String specialapproval;
//
//    @TableField("FREQUENCY")
//    private Integer frequency;
//
//    @TableField("CYCLE")
//    private String cycle;
//
//    @TableField("NOSYNFIELDS")
//    private String nosynfields;
//
//    @TableField("ISNEEDDELACC")
//    private String isneeddelacc;
//
//    @TableField("SAPSOURCE")
//    private String sapsource;
//
//    @TableField("ISFNACONTROL")
//    private String isfnacontrol;
//
//    @TableField("FNANODEID")
//    private String fnanodeid;
//
//    @TableField("FNADEPARTMENTID")
//    private String fnadepartmentid;
//
//    @TableField("SMSALERTSTYPE")
//    private String smsalertstype;
//
//    @TableField("FORWARDRECEIVEDEF")
//    private String forwardreceivedef;
//
//    @TableField("ISSAVECHECKFORM")
//    private String issavecheckform;
//
//    @TableField("ARCHIVENOMSGALERT")
//    private String archivenomsgalert;
//
//    @TableField("ARCHIVENOMAILALERT")
//    private String archivenomailalert;
//
//    @TableField("ISFNABUDGETWF")
//    private String isfnabudgetwf;
//
//    @TableField("CHATSTYPE")
//    private Integer chatstype;
//
//    @TableField("CHATSALERTTYPE")
//    private Integer chatsalerttype;
//
//    @TableField("NOTREMINDIFARCHIVED")
//    private Integer notremindifarchived;
//
//    @TableField("ISWORKFLOWDOC")
//    private Integer isworkflowdoc;
//
//    @TableField("VERSION")
//    private Integer version;
//
//    @TableField("ACTIVEVERSIONID")
//    private Integer activeversionid;
//
//    @TableField("VERSIONDESCRIPTION")
//    private String versiondescription;
//
//    @TableField("VERSIONCREATER")
//    private Integer versioncreater;
//
//    @TableField("DSPORDER")
//    private Integer dsporder;
//
//    @TableField("FIELDNOTIMPORT")
//    private String fieldnotimport;
//
//    @TableField("ISFREE")
//    private String isfree;
//
//    @TableField("ECOLOGY_PINYIN_SEARCH")
//    private String ecologyPinyinSearch;
//
//    @TableField("OFFICALTYPE")
//    private Integer officaltype;
//
//    @TableField("CUSTOMPAGE4EMOBLE")
//    private String custompage4emoble;

//    @TableField("ISUPDATETITLE")
//    private Integer isupdatetitle;
//
//    @TableField("ISSHARED")
//    private String isshared;
//
//    @TableField("ISOVERRB")
//    private String isoverrb;
//
//    @TableField("ISOVERIV")
//    private String isoveriv;
//
//    @TableField("ISAUTOAPPROVE")
//    private String isautoapprove;
//
//    @TableField("ISAUTOCOMMIT")
//    private String isautocommit;
//
//    @TableField("SENDTOMESSAGETYPE")
//    private String sendtomessagetype;
//
//    @TableField("TRACEFIELDID")
//    private Integer tracefieldid;
//
//    @TableField("TRACESAVESECID")
//    private Integer tracesavesecid;
//
//    @TableField("TRACECATEGORYTYPE")
//    private String tracecategorytype;
//
//    @TableField("TRACECATEGORYFIELDID")
//    private Integer tracecategoryfieldid;
//
//    @TableField("TRACEDOCOWNERTYPE")
//    private Integer tracedocownertype;
//
//    @TableField("TRACEDOCOWNERFIELDID")
//    private Integer tracedocownerfieldid;
//
//    @TableField("TRACEDOCOWNER")
//    private Integer tracedocowner;
//
//    @TableField("showChartUrl")
//    private String showcharturl;
//
//    private String isshowsrc;
//
//    private String islockworkflow;
//
//    private String limitvalue;
//
//    private String locknodes;
//
//    private String titletemplate;
//
//    private String freewftype;
//
//    private String titleset;
//
//    @TableField("isAutoRemark")
//    private String isautoremark;
//
//    @TableField("importReadOnlyField")
//    private String importreadonlyfield;
//
//    private String enablesignature;
//
//    private Integer submittype;
//
//    @TableField("isSmsRemind")
//    private String issmsremind;
//
//    @TableField("isWechatRemind")
//    private String iswechatremind;
//
//    @TableField("isEmailRemind")
//    private String isemailremind;
//
//    @TableField("isDefaultSmsRemind")
//    private String isdefaultsmsremind;
//
//    @TableField("isDefaultWechatRemind")
//    private String isdefaultwechatremind;
//
//    @TableField("isDefaultEmailRemind")
//    private String isdefaultemailremind;
//
//    @TableField("IsArchiveNoRemind")
//    private String isarchivenoremind;
//
//    @TableField("isCCNoRemind")
//    private String isccnoremind;
//
//    @TableField("isChoseReminder")
//    private String ischosereminder;
//
//    @TableField("alterRemindNodesType")
//    private String alterremindnodestype;
//
//    @TableField("alterRemindNodes")
//    private String alterremindnodes;
//
//    @TableField("hrmConditionShowType")
//    private String hrmconditionshowtype;
//
//    @TableField("defaultNameRuleType")
//    private String defaultnameruletype;
//
//    @TableField("isOnlyOneAutoApprove")
//    private String isonlyoneautoapprove;
//
//    @TableField("isOpenCommunication")
//    private Integer isopencommunication;
//
//    @TableField("attachAllowEdit")
//    private String attachallowedit;
//
//    private String docfiles;
//
//    @TableField("reqLevelColorJson")
//    private String reqlevelcolorjson;

//    private String uuid;
//
//    private String cusTitletemplate;
//
//    @TableField("allowViewEmShareLog")
//    private String allowviewemsharelog;
//
//    @TableField("autoFlowRequestlogTrail")
//    private String autoflowrequestlogtrail;
//
//    @TableField("errorRemindType")
//    private String errorremindtype;
//
//    @TableField("errorRemindObjids")
//    private String errorremindobjids;
//
//    @TableField("isOpenSignLocation")
//    private Integer isopensignlocation;
//
//    @TableField("isShowModifylog")
//    private String isshowmodifylog;
//
//    @TableField("isShowSignCommunicate")
//    private String isshowsigncommunicate;
//
//    @TableField("isExpendCommunicate")
//    private String isexpendcommunicate;
//
//    private String wfdocpathtype;
//
//    private String wfdocsource;
//
//    private String wfdocrelatedformdisplay;
//
//    private String wfdocrelatefieldid;
//
//    private String wfdocdiaplaywatermark;
//
//    @TableField("printHelpField")
//    private Integer printhelpfield;
//
//    @TableField("showUploader")
//    private String showuploader;
//
//    @TableField("showUploadTime")
//    private String showuploadtime;
//
//    private String wfdocpathfieldid;

//    @TableField("mobileUrl")
//    private String mobileurl;
//
//    @TableField("hideFileSize")
//    private Integer hidefilesize;
//
//    private String prohibitbatchforward;
//
//    private String remindscope;
//
//    @TableField("fileSecFormat")
//    private String filesecformat;
//
//    private String isencryptshare;
//
//    private String encryptrange;
//
//    @TableField("stopInChart")
//    private String stopinchart;
//
//    @TableField("stopInForm")
//    private String stopinform;
//
//    @TableField("checkOutHeartBeatTime")
//    private String checkoutheartbeattime;
//
//    @TableField("formrangeType")
//    private String formrangetype;
//
//    private String formrange;
//
//    private Integer wfidentkey;
//
//    @TableField("ruleRelationship")
//    private String rulerelationship;
//
//    private String conditioncn;
//
//    @TableField("emailSetting")
//    private String emailsetting;
//
//    @TableField("multiSubmitSpan")
//    private String multisubmitspan;

//    private String isautoedit;

    
    public Integer getId() {
        return id;
    }

      public void setId(Integer id) {
          this.id = id;
      }
    
    public String getWorkflowname() {
        return workflowname;
    }

      public void setWorkflowname(String workflowname) {
          this.workflowname = workflowname;
      }
    
    public String getWorkflowdesc() {
        return workflowdesc;
    }

      public void setWorkflowdesc(String workflowdesc) {
          this.workflowdesc = workflowdesc;
      }
    
    public Integer getWorkflowtype() {
        return workflowtype;
    }

      public void setWorkflowtype(Integer workflowtype) {
          this.workflowtype = workflowtype;
      }
    
    public String getSecurelevel() {
        return securelevel;
    }

      public void setSecurelevel(String securelevel) {
          this.securelevel = securelevel;
      }
    
    public Integer getFormid() {
        return formid;
    }

      public void setFormid(Integer formid) {
          this.formid = formid;
      }
    
    public Integer getUserid() {
        return userid;
    }

      public void setUserid(Integer userid) {
          this.userid = userid;
      }
    
    public String getIsbill() {
        return isbill;
    }

      public void setIsbill(String isbill) {
          this.isbill = isbill;
      }
    
    public Integer getIscust() {
        return iscust;
    }

      public void setIscust(Integer iscust) {
          this.iscust = iscust;
      }
    
    public Integer getHelpdocid() {
        return helpdocid;
    }

      public void setHelpdocid(Integer helpdocid) {
          this.helpdocid = helpdocid;
      }
    
    public String getIsvalid() {
        return isvalid;
    }

      public void setIsvalid(String isvalid) {
          this.isvalid = isvalid;
      }
    
    public String getNeedmark() {
        return needmark;
    }

      public void setNeedmark(String needmark) {
          this.needmark = needmark;
      }
    
    public Integer getMessagetype() {
        return messagetype;
    }

      public void setMessagetype(Integer messagetype) {
          this.messagetype = messagetype;
      }
    
    public Integer getMultisubmit() {
        return multisubmit;
    }

      public void setMultisubmit(Integer multisubmit) {
          this.multisubmit = multisubmit;
      }
    
    public Integer getDefaultname() {
        return defaultname;
    }

      public void setDefaultname(Integer defaultname) {
          this.defaultname = defaultname;
      }
    
    public String getDocpath() {
        return docpath;
    }

      public void setDocpath(String docpath) {
          this.docpath = docpath;
      }
    
    public Integer getSubcompanyid() {
        return subcompanyid;
    }

      public void setSubcompanyid(Integer subcompanyid) {
          this.subcompanyid = subcompanyid;
      }
    
//    public Integer getMailmessagetype() {
//        return mailmessagetype;
//    }
//
//      public void setMailmessagetype(Integer mailmessagetype) {
//          this.mailmessagetype = mailmessagetype;
//      }
//
//    public Integer getDocrightbyoperator() {
//        return docrightbyoperator;
//    }
//
//      public void setDocrightbyoperator(Integer docrightbyoperator) {
//          this.docrightbyoperator = docrightbyoperator;
//      }
//
//    public String getDoccategory() {
//        return doccategory;
//    }
//
//      public void setDoccategory(String doccategory) {
//          this.doccategory = doccategory;
//      }
//
//    public String getIstemplate() {
//        return istemplate;
//    }
//
//      public void setIstemplate(String istemplate) {
//          this.istemplate = istemplate;
//      }
//
//    public Integer getTemplateid() {
//        return templateid;
//    }
//
//      public void setTemplateid(Integer templateid) {
//          this.templateid = templateid;
//      }
//
//    public Integer getCatelogtype() {
//        return catelogtype;
//    }
//
//      public void setCatelogtype(Integer catelogtype) {
//          this.catelogtype = catelogtype;
//      }
//
//    public Integer getSelectedcatelog() {
//        return selectedcatelog;
//    }
//
//      public void setSelectedcatelog(Integer selectedcatelog) {
//          this.selectedcatelog = selectedcatelog;
//      }
//
//    public Integer getDocrightbyhrmresource() {
//        return docrightbyhrmresource;
//    }
//
//      public void setDocrightbyhrmresource(Integer docrightbyhrmresource) {
//          this.docrightbyhrmresource = docrightbyhrmresource;
//      }
//
//    public String getNeedaffirmance() {
//        return needaffirmance;
//    }
//
//      public void setNeedaffirmance(String needaffirmance) {
//          this.needaffirmance = needaffirmance;
//      }
//
//    public String getIsremarks() {
//        return isremarks;
//    }
//
//      public void setIsremarks(String isremarks) {
//          this.isremarks = isremarks;
//      }
//
//    public String getIsannexupload() {
//        return isannexupload;
//    }
//
//      public void setIsannexupload(String isannexupload) {
//          this.isannexupload = isannexupload;
//      }
//
//    public String getAnnexdoccategory() {
//        return annexdoccategory;
//    }
//
//      public void setAnnexdoccategory(String annexdoccategory) {
//          this.annexdoccategory = annexdoccategory;
//      }
//
//    public String getIsshowonreportinput() {
//        return isshowonreportinput;
//    }
//
//      public void setIsshowonreportinput(String isshowonreportinput) {
//          this.isshowonreportinput = isshowonreportinput;
//      }
//
//    public Integer getTitlefieldid() {
//        return titlefieldid;
//    }
//
//      public void setTitlefieldid(Integer titlefieldid) {
//          this.titlefieldid = titlefieldid;
//      }
//
//    public Integer getKeywordfieldid() {
//        return keywordfieldid;
//    }
//
//      public void setKeywordfieldid(Integer keywordfieldid) {
//          this.keywordfieldid = keywordfieldid;
//      }
//
//    public String getIsshowchart() {
//        return isshowchart;
//    }
//
//      public void setIsshowchart(String isshowchart) {
//          this.isshowchart = isshowchart;
//      }
//
//    public String getOrderbytype() {
//        return orderbytype;
//    }
//
//      public void setOrderbytype(String orderbytype) {
//          this.orderbytype = orderbytype;
//      }
//
//    public String getIstridiffworkflow() {
//        return istridiffworkflow;
//    }
//
//      public void setIstridiffworkflow(String istridiffworkflow) {
//          this.istridiffworkflow = istridiffworkflow;
//      }
//
//    public String getIsmodifylog() {
//        return ismodifylog;
//    }
//
//      public void setIsmodifylog(String ismodifylog) {
//          this.ismodifylog = ismodifylog;
//      }
//
//    public String getIfversion() {
//        return ifversion;
//    }
//
//      public void setIfversion(String ifversion) {
//          this.ifversion = ifversion;
//      }
//
//    public String getWfdocpath() {
//        return wfdocpath;
//    }
//
//      public void setWfdocpath(String wfdocpath) {
//          this.wfdocpath = wfdocpath;
//      }
//
//    public String getWfdocowner() {
//        return wfdocowner;
//    }
//
//      public void setWfdocowner(String wfdocowner) {
//          this.wfdocowner = wfdocowner;
//      }
//
//    public String getIsedit() {
//        return isedit;
//    }
//
//      public void setIsedit(String isedit) {
//          this.isedit = isedit;
//      }
//
//    public Integer getEditor() {
//        return editor;
//    }
//
//      public void setEditor(Integer editor) {
//          this.editor = editor;
//      }
//
//    public String getEditdate() {
//        return editdate;
//    }
//
//      public void setEditdate(String editdate) {
//          this.editdate = editdate;
//      }
//
//    public String getEdittime() {
//        return edittime;
//    }
//
//      public void setEdittime(String edittime) {
//          this.edittime = edittime;
//      }
//
//    public String getShowdelbuttonbyreject() {
//        return showdelbuttonbyreject;
//    }
//
//      public void setShowdelbuttonbyreject(String showdelbuttonbyreject) {
//          this.showdelbuttonbyreject = showdelbuttonbyreject;
//      }
//
//    public String getShowuploadtab() {
//        return showuploadtab;
//    }
//
//      public void setShowuploadtab(String showuploadtab) {
//          this.showuploadtab = showuploadtab;
//      }
//
//    public String getIssigndoc() {
//        return issigndoc;
//    }
//
//      public void setIssigndoc(String issigndoc) {
//          this.issigndoc = issigndoc;
//      }
//
//    public String getShowdoctab() {
//        return showdoctab;
//    }
//
//      public void setShowdoctab(String showdoctab) {
//          this.showdoctab = showdoctab;
//      }
//
//    public String getIssignworkflow() {
//        return issignworkflow;
//    }
//
//      public void setIssignworkflow(String issignworkflow) {
//          this.issignworkflow = issignworkflow;
//      }
//
//    public String getShowworkflowtab() {
//        return showworkflowtab;
//    }
//
//      public void setShowworkflowtab(String showworkflowtab) {
//          this.showworkflowtab = showworkflowtab;
//      }
//
//    public String getCandelacc() {
//        return candelacc;
//    }
//
//      public void setCandelacc(String candelacc) {
//          this.candelacc = candelacc;
//      }
//
//    public String getIsforwardrights() {
//        return isforwardrights;
//    }
//
//      public void setIsforwardrights(String isforwardrights) {
//          this.isforwardrights = isforwardrights;
//      }
//
//    public String getIsimportwf() {
//        return isimportwf;
//    }
//
//      public void setIsimportwf(String isimportwf) {
//          this.isimportwf = isimportwf;
//      }
//
//    public String getIsrejectremind() {
//        return isrejectremind;
//    }
//
//      public void setIsrejectremind(String isrejectremind) {
//          this.isrejectremind = isrejectremind;
//      }
//
//    public String getIschangrejectnode() {
//        return ischangrejectnode;
//    }
//
//      public void setIschangrejectnode(String ischangrejectnode) {
//          this.ischangrejectnode = ischangrejectnode;
//      }
//
//    public Integer getWfdocownertype() {
//        return wfdocownertype;
//    }
//
//      public void setWfdocownertype(Integer wfdocownertype) {
//          this.wfdocownertype = wfdocownertype;
//      }
//
//    public Integer getWfdocownerfieldid() {
//        return wfdocownerfieldid;
//    }
//
//      public void setWfdocownerfieldid(Integer wfdocownerfieldid) {
//          this.wfdocownerfieldid = wfdocownerfieldid;
//      }
//
//    public String getNewdocpath() {
//        return newdocpath;
//    }
//
//      public void setNewdocpath(String newdocpath) {
//          this.newdocpath = newdocpath;
//      }
//
//    public Integer getKeepsign() {
//        return keepsign;
//    }
//
//      public void setKeepsign(Integer keepsign) {
//          this.keepsign = keepsign;
//      }
//
//    public Integer getSeccategoryid() {
//        return seccategoryid;
//    }
//
//      public void setSeccategoryid(Integer seccategoryid) {
//          this.seccategoryid = seccategoryid;
//      }
//
//    public String getCustompage() {
//        return custompage;
//    }
//
//      public void setCustompage(String custompage) {
//          this.custompage = custompage;
//      }
//
//    public Integer getIssignview() {
//        return issignview;
//    }
//
//      public void setIssignview(Integer issignview) {
//          this.issignview = issignview;
//      }
//
//    public String getIsselectrejectnode() {
//        return isselectrejectnode;
//    }
//
//      public void setIsselectrejectnode(String isselectrejectnode) {
//          this.isselectrejectnode = isselectrejectnode;
//      }
//
//    public Integer getForbidattdownload() {
//        return forbidattdownload;
//    }
//
//      public void setForbidattdownload(Integer forbidattdownload) {
//          this.forbidattdownload = forbidattdownload;
//      }
//
//    public String getIsimportdetail() {
//        return isimportdetail;
//    }
//
//      public void setIsimportdetail(String isimportdetail) {
//          this.isimportdetail = isimportdetail;
//      }
//
//    public String getSpecialapproval() {
//        return specialapproval;
//    }
//
//      public void setSpecialapproval(String specialapproval) {
//          this.specialapproval = specialapproval;
//      }
//
//    public Integer getFrequency() {
//        return frequency;
//    }
//
//      public void setFrequency(Integer frequency) {
//          this.frequency = frequency;
//      }
//
//    public String getCycle() {
//        return cycle;
//    }
//
//      public void setCycle(String cycle) {
//          this.cycle = cycle;
//      }
//
//    public String getNosynfields() {
//        return nosynfields;
//    }
//
//      public void setNosynfields(String nosynfields) {
//          this.nosynfields = nosynfields;
//      }
//
//    public String getIsneeddelacc() {
//        return isneeddelacc;
//    }
//
//      public void setIsneeddelacc(String isneeddelacc) {
//          this.isneeddelacc = isneeddelacc;
//      }
//
//    public String getSapsource() {
//        return sapsource;
//    }
//
//      public void setSapsource(String sapsource) {
//          this.sapsource = sapsource;
//      }
//
//    public String getIsfnacontrol() {
//        return isfnacontrol;
//    }
//
//      public void setIsfnacontrol(String isfnacontrol) {
//          this.isfnacontrol = isfnacontrol;
//      }
//
//    public String getFnanodeid() {
//        return fnanodeid;
//    }
//
//      public void setFnanodeid(String fnanodeid) {
//          this.fnanodeid = fnanodeid;
//      }
//
//    public String getFnadepartmentid() {
//        return fnadepartmentid;
//    }
//
//      public void setFnadepartmentid(String fnadepartmentid) {
//          this.fnadepartmentid = fnadepartmentid;
//      }
//
//    public String getSmsalertstype() {
//        return smsalertstype;
//    }
//
//      public void setSmsalertstype(String smsalertstype) {
//          this.smsalertstype = smsalertstype;
//      }
//
//    public String getForwardreceivedef() {
//        return forwardreceivedef;
//    }
//
//      public void setForwardreceivedef(String forwardreceivedef) {
//          this.forwardreceivedef = forwardreceivedef;
//      }
//
//    public String getIssavecheckform() {
//        return issavecheckform;
//    }
//
//      public void setIssavecheckform(String issavecheckform) {
//          this.issavecheckform = issavecheckform;
//      }
//
//    public String getArchivenomsgalert() {
//        return archivenomsgalert;
//    }
//
//      public void setArchivenomsgalert(String archivenomsgalert) {
//          this.archivenomsgalert = archivenomsgalert;
//      }
//
//    public String getArchivenomailalert() {
//        return archivenomailalert;
//    }
//
//      public void setArchivenomailalert(String archivenomailalert) {
//          this.archivenomailalert = archivenomailalert;
//      }
//
//    public String getIsfnabudgetwf() {
//        return isfnabudgetwf;
//    }
//
//      public void setIsfnabudgetwf(String isfnabudgetwf) {
//          this.isfnabudgetwf = isfnabudgetwf;
//      }
//
//    public Integer getChatstype() {
//        return chatstype;
//    }
//
//      public void setChatstype(Integer chatstype) {
//          this.chatstype = chatstype;
//      }
//
//    public Integer getChatsalerttype() {
//        return chatsalerttype;
//    }
//
//      public void setChatsalerttype(Integer chatsalerttype) {
//          this.chatsalerttype = chatsalerttype;
//      }
//
//    public Integer getNotremindifarchived() {
//        return notremindifarchived;
//    }
//
//      public void setNotremindifarchived(Integer notremindifarchived) {
//          this.notremindifarchived = notremindifarchived;
//      }
//
//    public Integer getIsworkflowdoc() {
//        return isworkflowdoc;
//    }
//
//      public void setIsworkflowdoc(Integer isworkflowdoc) {
//          this.isworkflowdoc = isworkflowdoc;
//      }
//
//    public Integer getVersion() {
//        return version;
//    }
//
//      public void setVersion(Integer version) {
//          this.version = version;
//      }
//
//    public Integer getActiveversionid() {
//        return activeversionid;
//    }
//
//      public void setActiveversionid(Integer activeversionid) {
//          this.activeversionid = activeversionid;
//      }
//
//    public String getVersiondescription() {
//        return versiondescription;
//    }
//
//      public void setVersiondescription(String versiondescription) {
//          this.versiondescription = versiondescription;
//      }
//
//    public Integer getVersioncreater() {
//        return versioncreater;
//    }
//
//      public void setVersioncreater(Integer versioncreater) {
//          this.versioncreater = versioncreater;
//      }
//
//    public Integer getDsporder() {
//        return dsporder;
//    }
//
//      public void setDsporder(Integer dsporder) {
//          this.dsporder = dsporder;
//      }
//
//    public String getFieldnotimport() {
//        return fieldnotimport;
//    }
//
//      public void setFieldnotimport(String fieldnotimport) {
//          this.fieldnotimport = fieldnotimport;
//      }
//
//    public String getIsfree() {
//        return isfree;
//    }
//
//      public void setIsfree(String isfree) {
//          this.isfree = isfree;
//      }
//
//    public String getEcologyPinyinSearch() {
//        return ecologyPinyinSearch;
//    }
//
//      public void setEcologyPinyinSearch(String ecologyPinyinSearch) {
//          this.ecologyPinyinSearch = ecologyPinyinSearch;
//      }
//
//    public Integer getOfficaltype() {
//        return officaltype;
//    }
//
//      public void setOfficaltype(Integer officaltype) {
//          this.officaltype = officaltype;
//      }
//
//    public String getCustompage4emoble() {
//        return custompage4emoble;
//    }
//
//      public void setCustompage4emoble(String custompage4emoble) {
//          this.custompage4emoble = custompage4emoble;
//      }
    
//    public Integer getIsupdatetitle() {
//        return isupdatetitle;
//    }
//
//      public void setIsupdatetitle(Integer isupdatetitle) {
//          this.isupdatetitle = isupdatetitle;
//      }
//
//    public String getIsshared() {
//        return isshared;
//    }
//
//      public void setIsshared(String isshared) {
//          this.isshared = isshared;
//      }
//
//    public String getIsoverrb() {
//        return isoverrb;
//    }
//
//      public void setIsoverrb(String isoverrb) {
//          this.isoverrb = isoverrb;
//      }
//
//    public String getIsoveriv() {
//        return isoveriv;
//    }
//
//      public void setIsoveriv(String isoveriv) {
//          this.isoveriv = isoveriv;
//      }
//
//    public String getIsautoapprove() {
//        return isautoapprove;
//    }
//
//      public void setIsautoapprove(String isautoapprove) {
//          this.isautoapprove = isautoapprove;
//      }
//
//    public String getIsautocommit() {
//        return isautocommit;
//    }
//
//      public void setIsautocommit(String isautocommit) {
//          this.isautocommit = isautocommit;
//      }
//
//    public String getSendtomessagetype() {
//        return sendtomessagetype;
//    }
//
//      public void setSendtomessagetype(String sendtomessagetype) {
//          this.sendtomessagetype = sendtomessagetype;
//      }
//
//    public Integer getTracefieldid() {
//        return tracefieldid;
//    }
//
//      public void setTracefieldid(Integer tracefieldid) {
//          this.tracefieldid = tracefieldid;
//      }
//
//    public Integer getTracesavesecid() {
//        return tracesavesecid;
//    }
//
//      public void setTracesavesecid(Integer tracesavesecid) {
//          this.tracesavesecid = tracesavesecid;
//      }
//
//    public String getTracecategorytype() {
//        return tracecategorytype;
//    }
//
//      public void setTracecategorytype(String tracecategorytype) {
//          this.tracecategorytype = tracecategorytype;
//      }
//
//    public Integer getTracecategoryfieldid() {
//        return tracecategoryfieldid;
//    }
//
//      public void setTracecategoryfieldid(Integer tracecategoryfieldid) {
//          this.tracecategoryfieldid = tracecategoryfieldid;
//      }
//
//    public Integer getTracedocownertype() {
//        return tracedocownertype;
//    }
//
//      public void setTracedocownertype(Integer tracedocownertype) {
//          this.tracedocownertype = tracedocownertype;
//      }
//
//    public Integer getTracedocownerfieldid() {
//        return tracedocownerfieldid;
//    }
//
//      public void setTracedocownerfieldid(Integer tracedocownerfieldid) {
//          this.tracedocownerfieldid = tracedocownerfieldid;
//      }
//
//    public Integer getTracedocowner() {
//        return tracedocowner;
//    }
//
//      public void setTracedocowner(Integer tracedocowner) {
//          this.tracedocowner = tracedocowner;
//      }
//
//    public String getShowcharturl() {
//        return showcharturl;
//    }
//
//      public void setShowcharturl(String showcharturl) {
//          this.showcharturl = showcharturl;
//      }
//
//    public String getIsshowsrc() {
//        return isshowsrc;
//    }
//
//      public void setIsshowsrc(String isshowsrc) {
//          this.isshowsrc = isshowsrc;
//      }
//
//    public String getIslockworkflow() {
//        return islockworkflow;
//    }
//
//      public void setIslockworkflow(String islockworkflow) {
//          this.islockworkflow = islockworkflow;
//      }
//
//    public String getLimitvalue() {
//        return limitvalue;
//    }
//
//      public void setLimitvalue(String limitvalue) {
//          this.limitvalue = limitvalue;
//      }
//
//    public String getLocknodes() {
//        return locknodes;
//    }
//
//      public void setLocknodes(String locknodes) {
//          this.locknodes = locknodes;
//      }
//
//    public String getTitletemplate() {
//        return titletemplate;
//    }
//
//      public void setTitletemplate(String titletemplate) {
//          this.titletemplate = titletemplate;
//      }
//
//    public String getFreewftype() {
//        return freewftype;
//    }
//
//      public void setFreewftype(String freewftype) {
//          this.freewftype = freewftype;
//      }
//
//    public String getTitleset() {
//        return titleset;
//    }
//
//      public void setTitleset(String titleset) {
//          this.titleset = titleset;
//      }
//
//    public String getIsautoremark() {
//        return isautoremark;
//    }
//
//      public void setIsautoremark(String isautoremark) {
//          this.isautoremark = isautoremark;
//      }
//
//    public String getImportreadonlyfield() {
//        return importreadonlyfield;
//    }
//
//      public void setImportreadonlyfield(String importreadonlyfield) {
//          this.importreadonlyfield = importreadonlyfield;
//      }
//
//    public String getEnablesignature() {
//        return enablesignature;
//    }
//
//      public void setEnablesignature(String enablesignature) {
//          this.enablesignature = enablesignature;
//      }
//
//    public Integer getSubmittype() {
//        return submittype;
//    }
//
//      public void setSubmittype(Integer submittype) {
//          this.submittype = submittype;
//      }
//
//    public String getIssmsremind() {
//        return issmsremind;
//    }
//
//      public void setIssmsremind(String issmsremind) {
//          this.issmsremind = issmsremind;
//      }
//
//    public String getIswechatremind() {
//        return iswechatremind;
//    }
//
//      public void setIswechatremind(String iswechatremind) {
//          this.iswechatremind = iswechatremind;
//      }
//
//    public String getIsemailremind() {
//        return isemailremind;
//    }
//
//      public void setIsemailremind(String isemailremind) {
//          this.isemailremind = isemailremind;
//      }
//
//    public String getIsdefaultsmsremind() {
//        return isdefaultsmsremind;
//    }
//
//      public void setIsdefaultsmsremind(String isdefaultsmsremind) {
//          this.isdefaultsmsremind = isdefaultsmsremind;
//      }
//
//    public String getIsdefaultwechatremind() {
//        return isdefaultwechatremind;
//    }
//
//      public void setIsdefaultwechatremind(String isdefaultwechatremind) {
//          this.isdefaultwechatremind = isdefaultwechatremind;
//      }
//
//    public String getIsdefaultemailremind() {
//        return isdefaultemailremind;
//    }
//
//      public void setIsdefaultemailremind(String isdefaultemailremind) {
//          this.isdefaultemailremind = isdefaultemailremind;
//      }
//
//    public String getIsarchivenoremind() {
//        return isarchivenoremind;
//    }
//
//      public void setIsarchivenoremind(String isarchivenoremind) {
//          this.isarchivenoremind = isarchivenoremind;
//      }
//
//    public String getIsccnoremind() {
//        return isccnoremind;
//    }
//
//      public void setIsccnoremind(String isccnoremind) {
//          this.isccnoremind = isccnoremind;
//      }
//
//    public String getIschosereminder() {
//        return ischosereminder;
//    }
//
//      public void setIschosereminder(String ischosereminder) {
//          this.ischosereminder = ischosereminder;
//      }
//
//    public String getAlterremindnodestype() {
//        return alterremindnodestype;
//    }
//
//      public void setAlterremindnodestype(String alterremindnodestype) {
//          this.alterremindnodestype = alterremindnodestype;
//      }
//
//    public String getAlterremindnodes() {
//        return alterremindnodes;
//    }
//
//      public void setAlterremindnodes(String alterremindnodes) {
//          this.alterremindnodes = alterremindnodes;
//      }
//
//    public String getHrmconditionshowtype() {
//        return hrmconditionshowtype;
//    }
//
//      public void setHrmconditionshowtype(String hrmconditionshowtype) {
//          this.hrmconditionshowtype = hrmconditionshowtype;
//      }
//
//    public String getDefaultnameruletype() {
//        return defaultnameruletype;
//    }
//
//      public void setDefaultnameruletype(String defaultnameruletype) {
//          this.defaultnameruletype = defaultnameruletype;
//      }
//
//    public String getIsonlyoneautoapprove() {
//        return isonlyoneautoapprove;
//    }
//
//      public void setIsonlyoneautoapprove(String isonlyoneautoapprove) {
//          this.isonlyoneautoapprove = isonlyoneautoapprove;
//      }
//
//    public Integer getIsopencommunication() {
//        return isopencommunication;
//    }
//
//      public void setIsopencommunication(Integer isopencommunication) {
//          this.isopencommunication = isopencommunication;
//      }
//
//    public String getAttachallowedit() {
//        return attachallowedit;
//    }
//
//      public void setAttachallowedit(String attachallowedit) {
//          this.attachallowedit = attachallowedit;
//      }
//
//    public String getDocfiles() {
//        return docfiles;
//    }
//
//      public void setDocfiles(String docfiles) {
//          this.docfiles = docfiles;
//      }
//
//    public String getReqlevelcolorjson() {
//        return reqlevelcolorjson;
//    }
//
//      public void setReqlevelcolorjson(String reqlevelcolorjson) {
//          this.reqlevelcolorjson = reqlevelcolorjson;
//      }
    
//    public String getUuid() {
//        return uuid;
//    }
//
//      public void setUuid(String uuid) {
//          this.uuid = uuid;
//      }
//
//    public String getCusTitletemplate() {
//        return cusTitletemplate;
//    }
//
//      public void setCusTitletemplate(String cusTitletemplate) {
//          this.cusTitletemplate = cusTitletemplate;
//      }
//
//    public String getAllowviewemsharelog() {
//        return allowviewemsharelog;
//    }
//
//      public void setAllowviewemsharelog(String allowviewemsharelog) {
//          this.allowviewemsharelog = allowviewemsharelog;
//      }
//
//    public String getAutoflowrequestlogtrail() {
//        return autoflowrequestlogtrail;
//    }
//
//      public void setAutoflowrequestlogtrail(String autoflowrequestlogtrail) {
//          this.autoflowrequestlogtrail = autoflowrequestlogtrail;
//      }
//
//    public String getErrorremindtype() {
//        return errorremindtype;
//    }
//
//      public void setErrorremindtype(String errorremindtype) {
//          this.errorremindtype = errorremindtype;
//      }
//
//    public String getErrorremindobjids() {
//        return errorremindobjids;
//    }
//
//      public void setErrorremindobjids(String errorremindobjids) {
//          this.errorremindobjids = errorremindobjids;
//      }
//
//    public Integer getIsopensignlocation() {
//        return isopensignlocation;
//    }
//
//      public void setIsopensignlocation(Integer isopensignlocation) {
//          this.isopensignlocation = isopensignlocation;
//      }
//
//    public String getIsshowmodifylog() {
//        return isshowmodifylog;
//    }
//
//      public void setIsshowmodifylog(String isshowmodifylog) {
//          this.isshowmodifylog = isshowmodifylog;
//      }
//
//    public String getIsshowsigncommunicate() {
//        return isshowsigncommunicate;
//    }
//
//      public void setIsshowsigncommunicate(String isshowsigncommunicate) {
//          this.isshowsigncommunicate = isshowsigncommunicate;
//      }
//
//    public String getIsexpendcommunicate() {
//        return isexpendcommunicate;
//    }
//
//      public void setIsexpendcommunicate(String isexpendcommunicate) {
//          this.isexpendcommunicate = isexpendcommunicate;
//      }
//
//    public String getWfdocpathtype() {
//        return wfdocpathtype;
//    }
//
//      public void setWfdocpathtype(String wfdocpathtype) {
//          this.wfdocpathtype = wfdocpathtype;
//      }
//
//    public String getWfdocsource() {
//        return wfdocsource;
//    }
//
//      public void setWfdocsource(String wfdocsource) {
//          this.wfdocsource = wfdocsource;
//      }
//
//    public String getWfdocrelatedformdisplay() {
//        return wfdocrelatedformdisplay;
//    }
//
//      public void setWfdocrelatedformdisplay(String wfdocrelatedformdisplay) {
//          this.wfdocrelatedformdisplay = wfdocrelatedformdisplay;
//      }
//
//    public String getWfdocrelatefieldid() {
//        return wfdocrelatefieldid;
//    }
//
//      public void setWfdocrelatefieldid(String wfdocrelatefieldid) {
//          this.wfdocrelatefieldid = wfdocrelatefieldid;
//      }
//
//    public String getWfdocdiaplaywatermark() {
//        return wfdocdiaplaywatermark;
//    }
//
//      public void setWfdocdiaplaywatermark(String wfdocdiaplaywatermark) {
//          this.wfdocdiaplaywatermark = wfdocdiaplaywatermark;
//      }
//
//    public Integer getPrinthelpfield() {
//        return printhelpfield;
//    }
//
//      public void setPrinthelpfield(Integer printhelpfield) {
//          this.printhelpfield = printhelpfield;
//      }
//
//    public String getShowuploader() {
//        return showuploader;
//    }
//
//      public void setShowuploader(String showuploader) {
//          this.showuploader = showuploader;
//      }
//
//    public String getShowuploadtime() {
//        return showuploadtime;
//    }
//
//      public void setShowuploadtime(String showuploadtime) {
//          this.showuploadtime = showuploadtime;
//      }
//
//    public String getWfdocpathfieldid() {
//        return wfdocpathfieldid;
//    }
//
//      public void setWfdocpathfieldid(String wfdocpathfieldid) {
//          this.wfdocpathfieldid = wfdocpathfieldid;
//      }
    
//    public String getMobileurl() {
//        return mobileurl;
//    }
//
//      public void setMobileurl(String mobileurl) {
//          this.mobileurl = mobileurl;
//      }
//
//    public Integer getHidefilesize() {
//        return hidefilesize;
//    }
//
//      public void setHidefilesize(Integer hidefilesize) {
//          this.hidefilesize = hidefilesize;
//      }
//
//    public String getProhibitbatchforward() {
//        return prohibitbatchforward;
//    }
//
//      public void setProhibitbatchforward(String prohibitbatchforward) {
//          this.prohibitbatchforward = prohibitbatchforward;
//      }
//
//    public String getRemindscope() {
//        return remindscope;
//    }
//
//      public void setRemindscope(String remindscope) {
//          this.remindscope = remindscope;
//      }
//
//    public String getFilesecformat() {
//        return filesecformat;
//    }
//
//      public void setFilesecformat(String filesecformat) {
//          this.filesecformat = filesecformat;
//      }
//
//    public String getIsencryptshare() {
//        return isencryptshare;
//    }
//
//      public void setIsencryptshare(String isencryptshare) {
//          this.isencryptshare = isencryptshare;
//      }
//
//    public String getEncryptrange() {
//        return encryptrange;
//    }
//
//      public void setEncryptrange(String encryptrange) {
//          this.encryptrange = encryptrange;
//      }
//
//    public String getStopinchart() {
//        return stopinchart;
//    }
//
//      public void setStopinchart(String stopinchart) {
//          this.stopinchart = stopinchart;
//      }
//
//    public String getStopinform() {
//        return stopinform;
//    }
//
//      public void setStopinform(String stopinform) {
//          this.stopinform = stopinform;
//      }
//
//    public String getCheckoutheartbeattime() {
//        return checkoutheartbeattime;
//    }
//
//      public void setCheckoutheartbeattime(String checkoutheartbeattime) {
//          this.checkoutheartbeattime = checkoutheartbeattime;
//      }
//
//    public String getFormrangetype() {
//        return formrangetype;
//    }
//
//      public void setFormrangetype(String formrangetype) {
//          this.formrangetype = formrangetype;
//      }
//
//    public String getFormrange() {
//        return formrange;
//    }
//
//      public void setFormrange(String formrange) {
//          this.formrange = formrange;
//      }
//
//    public Integer getWfidentkey() {
//        return wfidentkey;
//    }
//
//      public void setWfidentkey(Integer wfidentkey) {
//          this.wfidentkey = wfidentkey;
//      }
//
//    public String getRulerelationship() {
//        return rulerelationship;
//    }
//
//      public void setRulerelationship(String rulerelationship) {
//          this.rulerelationship = rulerelationship;
//      }
//
//    public String getConditioncn() {
//        return conditioncn;
//    }
//
//      public void setConditioncn(String conditioncn) {
//          this.conditioncn = conditioncn;
//      }
//
//    public String getEmailsetting() {
//        return emailsetting;
//    }
//
//      public void setEmailsetting(String emailsetting) {
//          this.emailsetting = emailsetting;
//      }
//
//    public String getMultisubmitspan() {
//        return multisubmitspan;
//    }
//
//      public void setMultisubmitspan(String multisubmitspan) {
//          this.multisubmitspan = multisubmitspan;
//      }
//
//    public String getIsautoedit() {
//        return isautoedit;
//    }
//
//      public void setIsautoedit(String isautoedit) {
//          this.isautoedit = isautoedit;
//      }

    @Override
    public String toString() {
        return "WorkflowBase{" +
              "id=" + id +
                  ", workflowname=" + workflowname +
                  ", workflowdesc=" + workflowdesc +
                  ", workflowtype=" + workflowtype +
                  ", securelevel=" + securelevel +
                  ", formid=" + formid +
                  ", userid=" + userid +
                  ", isbill=" + isbill +
                  ", iscust=" + iscust +
                  ", helpdocid=" + helpdocid +
                  ", isvalid=" + isvalid +
                  ", needmark=" + needmark +
                  ", messagetype=" + messagetype +
                  ", multisubmit=" + multisubmit +
                  ", defaultname=" + defaultname +
                  ", docpath=" + docpath +
                  ", subcompanyid=" + subcompanyid +
//                  ", mailmessagetype=" + mailmessagetype +
//                  ", docrightbyoperator=" + docrightbyoperator +
//                  ", doccategory=" + doccategory +
//                  ", istemplate=" + istemplate +
//                  ", templateid=" + templateid +
//                  ", catelogtype=" + catelogtype +
//                  ", selectedcatelog=" + selectedcatelog +
//                  ", docrightbyhrmresource=" + docrightbyhrmresource +
//                  ", needaffirmance=" + needaffirmance +
//                  ", isremarks=" + isremarks +
//                  ", isannexupload=" + isannexupload +
//                  ", annexdoccategory=" + annexdoccategory +
//                  ", isshowonreportinput=" + isshowonreportinput +
//                  ", titlefieldid=" + titlefieldid +
//                  ", keywordfieldid=" + keywordfieldid +
//                  ", isshowchart=" + isshowchart +
//                  ", orderbytype=" + orderbytype +
//                  ", istridiffworkflow=" + istridiffworkflow +
//                  ", ismodifylog=" + ismodifylog +
//                  ", ifversion=" + ifversion +
//                  ", wfdocpath=" + wfdocpath +
//                  ", wfdocowner=" + wfdocowner +
//                  ", isedit=" + isedit +
//                  ", editor=" + editor +
//                  ", editdate=" + editdate +
//                  ", edittime=" + edittime +
//                  ", showdelbuttonbyreject=" + showdelbuttonbyreject +
//                  ", showuploadtab=" + showuploadtab +
//                  ", issigndoc=" + issigndoc +
//                  ", showdoctab=" + showdoctab +
//                  ", issignworkflow=" + issignworkflow +
//                  ", showworkflowtab=" + showworkflowtab +
//                  ", candelacc=" + candelacc +
//                  ", isforwardrights=" + isforwardrights +
//                  ", isimportwf=" + isimportwf +
//                  ", isrejectremind=" + isrejectremind +
//                  ", ischangrejectnode=" + ischangrejectnode +
//                  ", wfdocownertype=" + wfdocownertype +
//                  ", wfdocownerfieldid=" + wfdocownerfieldid +
//                  ", newdocpath=" + newdocpath +
//                  ", keepsign=" + keepsign +
//                  ", seccategoryid=" + seccategoryid +
//                  ", custompage=" + custompage +
//                  ", issignview=" + issignview +
//                  ", isselectrejectnode=" + isselectrejectnode +
//                  ", forbidattdownload=" + forbidattdownload +
//                  ", isimportdetail=" + isimportdetail +
//                  ", specialapproval=" + specialapproval +
//                  ", frequency=" + frequency +
//                  ", cycle=" + cycle +
//                  ", nosynfields=" + nosynfields +
//                  ", isneeddelacc=" + isneeddelacc +
//                  ", sapsource=" + sapsource +
//                  ", isfnacontrol=" + isfnacontrol +
//                  ", fnanodeid=" + fnanodeid +
//                  ", fnadepartmentid=" + fnadepartmentid +
//                  ", smsalertstype=" + smsalertstype +
//                  ", forwardreceivedef=" + forwardreceivedef +
//                  ", issavecheckform=" + issavecheckform +
//                  ", archivenomsgalert=" + archivenomsgalert +
//                  ", archivenomailalert=" + archivenomailalert +
//                  ", isfnabudgetwf=" + isfnabudgetwf +
//                  ", chatstype=" + chatstype +
//                  ", chatsalerttype=" + chatsalerttype +
//                  ", notremindifarchived=" + notremindifarchived +
//                  ", isworkflowdoc=" + isworkflowdoc +
//                  ", version=" + version +
//                  ", activeversionid=" + activeversionid +
//                  ", versiondescription=" + versiondescription +
//                  ", versioncreater=" + versioncreater +
//                  ", dsporder=" + dsporder +
//                  ", fieldnotimport=" + fieldnotimport +
//                  ", isfree=" + isfree +
//                  ", ecologyPinyinSearch=" + ecologyPinyinSearch +
//                  ", officaltype=" + officaltype +
//                  ", custompage4emoble=" + custompage4emoble +
//                  ", isupdatetitle=" + isupdatetitle +
//                  ", isshared=" + isshared +
//                  ", isoverrb=" + isoverrb +
//                  ", isoveriv=" + isoveriv +
//                  ", isautoapprove=" + isautoapprove +
//                  ", isautocommit=" + isautocommit +
//                  ", sendtomessagetype=" + sendtomessagetype +
//                  ", tracefieldid=" + tracefieldid +
//                  ", tracesavesecid=" + tracesavesecid +
//                  ", tracecategorytype=" + tracecategorytype +
//                  ", tracecategoryfieldid=" + tracecategoryfieldid +
//                  ", tracedocownertype=" + tracedocownertype +
//                  ", tracedocownerfieldid=" + tracedocownerfieldid +
//                  ", tracedocowner=" + tracedocowner +
//                  ", showcharturl=" + showcharturl +
//                  ", isshowsrc=" + isshowsrc +
//                  ", islockworkflow=" + islockworkflow +
//                  ", limitvalue=" + limitvalue +
//                  ", locknodes=" + locknodes +
//                  ", titletemplate=" + titletemplate +
//                  ", freewftype=" + freewftype +
//                  ", titleset=" + titleset +
//                  ", isautoremark=" + isautoremark +
//                  ", importreadonlyfield=" + importreadonlyfield +
//                  ", enablesignature=" + enablesignature +
//                  ", submittype=" + submittype +
//                  ", issmsremind=" + issmsremind +
//                  ", iswechatremind=" + iswechatremind +
//                  ", isemailremind=" + isemailremind +
//                  ", isdefaultsmsremind=" + isdefaultsmsremind +
//                  ", isdefaultwechatremind=" + isdefaultwechatremind +
//                  ", isdefaultemailremind=" + isdefaultemailremind +
//                  ", isarchivenoremind=" + isarchivenoremind +
//                  ", isccnoremind=" + isccnoremind +
//                  ", ischosereminder=" + ischosereminder +
//                  ", alterremindnodestype=" + alterremindnodestype +
//                  ", alterremindnodes=" + alterremindnodes +
//                  ", hrmconditionshowtype=" + hrmconditionshowtype +
//                  ", defaultnameruletype=" + defaultnameruletype +
//                  ", isonlyoneautoapprove=" + isonlyoneautoapprove +
//                  ", isopencommunication=" + isopencommunication +
//                  ", attachallowedit=" + attachallowedit +
//                  ", docfiles=" + docfiles +
//                  ", reqlevelcolorjson=" + reqlevelcolorjson +
//                  ", uuid=" + uuid +
//                  ", cusTitletemplate=" + cusTitletemplate +
//                  ", allowviewemsharelog=" + allowviewemsharelog +
//                  ", autoflowrequestlogtrail=" + autoflowrequestlogtrail +
//                  ", errorremindtype=" + errorremindtype +
//                  ", errorremindobjids=" + errorremindobjids +
//                  ", isopensignlocation=" + isopensignlocation +
//                  ", isshowmodifylog=" + isshowmodifylog +
//                  ", isshowsigncommunicate=" + isshowsigncommunicate +
//                  ", isexpendcommunicate=" + isexpendcommunicate +
//                  ", wfdocpathtype=" + wfdocpathtype +
//                  ", wfdocsource=" + wfdocsource +
//                  ", wfdocrelatedformdisplay=" + wfdocrelatedformdisplay +
//                  ", wfdocrelatefieldid=" + wfdocrelatefieldid +
//                  ", wfdocdiaplaywatermark=" + wfdocdiaplaywatermark +
//                  ", printhelpfield=" + printhelpfield +
//                  ", showuploader=" + showuploader +
//                  ", showuploadtime=" + showuploadtime +
//                  ", wfdocpathfieldid=" + wfdocpathfieldid +
//                  ", mobileurl=" + mobileurl +
//                  ", hidefilesize=" + hidefilesize +
//                  ", prohibitbatchforward=" + prohibitbatchforward +
//                  ", remindscope=" + remindscope +
//                  ", filesecformat=" + filesecformat +
//                  ", isencryptshare=" + isencryptshare +
//                  ", encryptrange=" + encryptrange +
//                  ", stopinchart=" + stopinchart +
//                  ", stopinform=" + stopinform +
//                  ", checkoutheartbeattime=" + checkoutheartbeattime +
//                  ", formrangetype=" + formrangetype +
//                  ", formrange=" + formrange +
//                  ", wfidentkey=" + wfidentkey +
//                  ", rulerelationship=" + rulerelationship +
//                  ", conditioncn=" + conditioncn +
//                  ", emailsetting=" + emailsetting +
//                  ", multisubmitspan=" + multisubmitspan +
//                  ", isautoedit=" + isautoedit +
              "}";
    }
}
