package com.weaver.versionupgrade.entity.mapper;

import com.weaver.versionupgrade.entity.entity.WorkflowBase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wy
 * @since 2024-04-02
 */
public interface WorkflowBaseMapper extends BaseMapper<WorkflowBase> {

}
