package com.weaver.versionupgrade.entity.mapper;

import com.weaver.versionupgrade.entity.entity.JavascriptTrans;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wy
 * @since 2024-04-17
 */
public interface JavascriptTransMapper extends BaseMapper<JavascriptTrans> {
    @Delete("TRUNCATE TABLE javascript_trans")
    void truncateTable();

    void updateBatchField(@Param("jsTransArr") List<JavascriptTrans> jsTransArr);

    void updateJsTrans(JavascriptTrans jsTrans);
}
