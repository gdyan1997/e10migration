package com.weaver.versionupgrade.entity.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weaver.versionupgrade.entity.entity.Modehtmllayout;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
public interface ModehtmllayoutMapper extends BaseMapper<Modehtmllayout> {

    IPage<Map<String, Object>> selectLayoutByModeIdWithPage(Page<?> page, boolean isE9OA, String workflowName);

    List<Map<String, Object>> selectLayoutByModeIdWithPage(boolean isE9OA, String workflowName);
}
