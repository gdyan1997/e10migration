package com.weaver.versionupgrade.entity.mapper;

import com.weaver.versionupgrade.entity.entity.WorkflowFormfield;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public interface WorkflowFormfieldMapper extends BaseMapper<WorkflowFormfield> {

}
