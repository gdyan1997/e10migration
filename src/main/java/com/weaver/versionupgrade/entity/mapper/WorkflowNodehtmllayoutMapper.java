package com.weaver.versionupgrade.entity.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weaver.versionupgrade.entity.entity.WorkflowNodehtmllayout;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public interface WorkflowNodehtmllayoutMapper extends BaseMapper<WorkflowNodehtmllayout> {

    IPage<Map<String, Object>> selectNodeHtmlLayoutByWorkflowIdWithPage(Page<?> page, boolean isE9OA, String workflowName);

    List<Map<String, Object>> selectNodeHtmlLayoutByWorkflowIdWithPage(boolean isE9OA, String workflowName);
}
