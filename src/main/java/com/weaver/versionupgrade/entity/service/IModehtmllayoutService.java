package com.weaver.versionupgrade.entity.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.weaver.versionupgrade.entity.entity.Modehtmllayout;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
public interface IModehtmllayoutService extends IService<Modehtmllayout> {
    IPage<Map<String, Object>> selectLayoutByModeIdWithPage(int pageNum, int pageSize, boolean isE9OA, String workflowName, boolean isAll);
}
