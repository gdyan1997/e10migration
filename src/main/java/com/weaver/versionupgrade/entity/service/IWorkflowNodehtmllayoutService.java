package com.weaver.versionupgrade.entity.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weaver.versionupgrade.entity.entity.WorkflowNodehtmllayout;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public interface IWorkflowNodehtmllayoutService extends IService<WorkflowNodehtmllayout> {
    IPage<Map<String, Object>> selectNodeHtmlLayoutByWorkflowIdWithPage(int pageNum, int pageSize, boolean isE9OA, String workflowName, boolean isAll);
}
