package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.JavascriptTrans;
import com.weaver.versionupgrade.entity.mapper.JavascriptTransMapper;
import com.weaver.versionupgrade.entity.service.IJavascriptTransService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-17
 */
@Service
public class JavascriptTransServiceImpl extends ServiceImpl<JavascriptTransMapper, JavascriptTrans> implements IJavascriptTransService {
    @Override
    public void truncateTable(){
        this.baseMapper.truncateTable();
    }
}
