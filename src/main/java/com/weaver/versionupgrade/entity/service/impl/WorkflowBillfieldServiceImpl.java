package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.WorkflowBillfield;
import com.weaver.versionupgrade.entity.mapper.WorkflowBillfieldMapper;
import com.weaver.versionupgrade.entity.service.IWorkflowBillfieldService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
@Service
public class WorkflowBillfieldServiceImpl extends ServiceImpl<WorkflowBillfieldMapper, WorkflowBillfield> implements IWorkflowBillfieldService {

}
