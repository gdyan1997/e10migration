package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.WorkflowBill;
import com.weaver.versionupgrade.entity.mapper.WorkflowBillMapper;
import com.weaver.versionupgrade.entity.service.IWorkflowBillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
@Service
public class WorkflowBillServiceImpl extends ServiceImpl<WorkflowBillMapper, WorkflowBill> implements IWorkflowBillService {

}
