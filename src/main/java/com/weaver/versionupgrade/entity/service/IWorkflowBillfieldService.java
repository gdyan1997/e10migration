package com.weaver.versionupgrade.entity.service;

import com.weaver.versionupgrade.entity.entity.WorkflowBillfield;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
public interface IWorkflowBillfieldService extends IService<WorkflowBillfield> {

}
