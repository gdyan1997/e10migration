package com.weaver.versionupgrade.entity.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.weaver.versionupgrade.entity.entity.WorkflowBase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wy
 * @since 2024-04-02
 */
public interface IWorkflowBaseService extends IService<WorkflowBase> {
    /**
     * 分页查询 WorkflowBase 数据
     * @param pageNum  当前页码
     * @param pageSize 每页显示的记录数
     * @return 分页结果
     */
    IPage<WorkflowBase> listWorkflowBaseByPage(QueryWrapper<WorkflowBase> queryWrapper, long pageNum, long pageSize, long totle);
    IPage<WorkflowBase> listWorkflowBaseByPage(QueryWrapper<WorkflowBase> queryWrapper, long pageNum, long pageSize);
}
