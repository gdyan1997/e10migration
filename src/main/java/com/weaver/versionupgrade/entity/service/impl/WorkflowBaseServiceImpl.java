package com.weaver.versionupgrade.entity.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weaver.versionupgrade.entity.mapper.WorkflowBaseMapper;
import com.weaver.versionupgrade.entity.entity.WorkflowBase;
import com.weaver.versionupgrade.entity.service.IWorkflowBaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-02
 */
@Service
public class WorkflowBaseServiceImpl extends ServiceImpl<WorkflowBaseMapper, WorkflowBase> implements IWorkflowBaseService {
    @Override
    public IPage<WorkflowBase> listWorkflowBaseByPage(QueryWrapper<WorkflowBase> queryWrapper, long pageNum, long pageSize, long totle) {
        // 创建分页对象
        Page<WorkflowBase> page = new Page<>(pageNum, pageSize, totle);

        // 执行分页查询
        return baseMapper.selectPage(page, queryWrapper); // 这里的 null 表示不加任何条件查询，你可以根据需要自行添加查询条件
    }

    @Override
    public IPage<WorkflowBase> listWorkflowBaseByPage(QueryWrapper<WorkflowBase> queryWrapper, long pageNum, long pageSize) {
        return listWorkflowBaseByPage(queryWrapper, pageNum, pageSize, 0);
    }
}
