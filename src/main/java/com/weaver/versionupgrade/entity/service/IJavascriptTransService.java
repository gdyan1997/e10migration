package com.weaver.versionupgrade.entity.service;

import com.weaver.versionupgrade.entity.entity.JavascriptTrans;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Wy
 * @since 2024-04-17
 */
public interface IJavascriptTransService extends IService<JavascriptTrans> {
    void truncateTable();
}
