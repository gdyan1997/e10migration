package com.weaver.versionupgrade.entity.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.weaver.versionupgrade.entity.entity.Modehtmllayout;
import com.weaver.versionupgrade.entity.mapper.ModehtmllayoutMapper;
import com.weaver.versionupgrade.entity.service.IModehtmllayoutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
@Service
public class ModehtmllayoutServiceImpl extends ServiceImpl<ModehtmllayoutMapper, Modehtmllayout> implements IModehtmllayoutService {
    @Override
    public IPage<Map<String, Object>> selectLayoutByModeIdWithPage(int pageNum, int pageSize, boolean isE9OA, String workflowName, boolean isAll) {
        if (isAll) {

            // 查询所有数据
            List<Map<String, Object>> list = baseMapper.selectLayoutByModeIdWithPage(isE9OA, workflowName);

            // 构造 IPage 对象
            Page<Map<String, Object>> page = new Page<>(pageNum, pageSize);
            page.setRecords(list);
            page.setTotal(list.size());

            return page;
        } else {
            // 创建分页对象
            Page<Map<String, Object>> page = new Page<>(pageNum, pageSize);

            // 调用带分页的方法进行查询
            return baseMapper.selectLayoutByModeIdWithPage(page, isE9OA, workflowName);
        }
    }
}
