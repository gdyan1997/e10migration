package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.WorkflowFormfield;
import com.weaver.versionupgrade.entity.mapper.WorkflowFormfieldMapper;
import com.weaver.versionupgrade.entity.service.IWorkflowFormfieldService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
@Service
public class WorkflowFormfieldServiceImpl extends ServiceImpl<WorkflowFormfieldMapper, WorkflowFormfield> implements IWorkflowFormfieldService {

}
