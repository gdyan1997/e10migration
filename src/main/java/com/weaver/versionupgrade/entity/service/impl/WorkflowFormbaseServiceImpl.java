package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.WorkflowFormbase;
import com.weaver.versionupgrade.entity.mapper.WorkflowFormbaseMapper;
import com.weaver.versionupgrade.entity.service.IWorkflowFormbaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
@Service
public class WorkflowFormbaseServiceImpl extends ServiceImpl<WorkflowFormbaseMapper, WorkflowFormbase> implements IWorkflowFormbaseService {

}
