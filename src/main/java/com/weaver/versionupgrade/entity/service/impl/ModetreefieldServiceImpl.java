package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.Modetreefield;
import com.weaver.versionupgrade.entity.mapper.ModetreefieldMapper;
import com.weaver.versionupgrade.entity.service.IModetreefieldService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
@Service
public class ModetreefieldServiceImpl extends ServiceImpl<ModetreefieldMapper, Modetreefield> implements IModetreefieldService {

}
