package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.service.IWorkflowNodebaseService;
import com.weaver.versionupgrade.entity.entity.WorkflowNodebase;
import com.weaver.versionupgrade.entity.mapper.WorkflowNodebaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
@Service
public class WorkflowNodebaseServiceImpl extends ServiceImpl<WorkflowNodebaseMapper, WorkflowNodebase> implements IWorkflowNodebaseService {

}
