package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.WorkflowBilldetailtable;
import com.weaver.versionupgrade.entity.mapper.WorkflowBilldetailtableMapper;
import com.weaver.versionupgrade.entity.service.IWorkflowBilldetailtableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-10
 */
@Service
public class WorkflowBilldetailtableServiceImpl extends ServiceImpl<WorkflowBilldetailtableMapper, WorkflowBilldetailtable> implements IWorkflowBilldetailtableService {

}
