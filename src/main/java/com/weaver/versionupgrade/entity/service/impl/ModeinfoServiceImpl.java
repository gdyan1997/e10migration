package com.weaver.versionupgrade.entity.service.impl;

import com.weaver.versionupgrade.entity.entity.Modeinfo;
import com.weaver.versionupgrade.entity.mapper.ModeinfoMapper;
import com.weaver.versionupgrade.entity.service.IModeinfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Wy
 * @since 2024-04-22
 */
@Service
public class ModeinfoServiceImpl extends ServiceImpl<ModeinfoMapper, Modeinfo> implements IModeinfoService {

}
