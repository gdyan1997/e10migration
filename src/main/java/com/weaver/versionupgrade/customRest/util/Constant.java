package com.weaver.versionupgrade.customRest.util;


import com.weaver.versionupgrade.util.SyncConstant;

/**
 * @author cx
 * @date 2022/3/2
 */
public class Constant {
    public static final String SUCCESS = "success";
    public static final String FAIL = "failure";
    /**数据库类型**/
    public static final String MYSQL = "mysql";
    public static final String SQLSERVER = "sqlserver";
    public static final String ORACLE = "oracle";
    public static final String POSTGRESQL = "postgresql";


    /***执行状态***/
    public static final int STEP_0 = 0;// 未开始
    public static final int STEP_1 = 1;// 进行中
    public static final int STEP_2 = 2;// 完成
    public static final int STEP_3 = 3;// 失败
    public static final int STEP_4 = 4;//单独执行的规则

    /***转换规则初始化执行状态***/
    public static final int INIT_0 = 0;// 未开始
    public static final int INIT_1 = 1;// 进行中
    public static final int INIT_2 = 2;// 完成

    public static final String DB_SOURCE = SyncConstant.DB_SOURCE;//源库
    public static final String DB_TARGET = SyncConstant.DB_TARGET;//源库
}
