package com.weaver.versionupgrade.customRest.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * @author cx
 * @date 2022/4/2
 **/
public class TablePaginationOperation {


    /**
     * 兼容无参数的方法
     * @param sql
     * @param dbtype
     * @param args
     * @return
     */
    public static  List<String> paginationPrepareStatementSql(String sql, String dbtype, Object ...args) {
        return paginationPrepareStatementSql( sql, dbtype,new ArrayList<>(),args);
    }

    /**
     *
     * @param sql
     * @param dbtype
     * @param args
     * @return
     */
    public static List<String> paginationPrepareStatementSql(String sql, String dbtype,List<Object> params, Object ...args) {

        //解析参数
        List<Object> paramList = new ArrayList();
        for (Object param : args) {
            if ((param instanceof Vector)) {
                paramList.addAll(Collections.list(((Vector) param).elements()));
                break;
            } else if ((param instanceof List)) {
                paramList.addAll((List) param);
            } else {//动态参数
                if (param == null) {
                    paramList.add(null);
                } else {
                    paramList.add(param);
                }
            }
        }
        params.addAll(paramList);

        //需要执行的全部的sql
        List<String>  sqllist=new ArrayList<>();
        sqllist.add(sql);
        return sqllist;
    }

    /**
     * 获取分页的sql
     * @param sql
     * @param dbTypeName
     * @return
     * @throws Exception
     */
    public static List<String> paginationSql(String sql, String dbTypeName) throws Exception {
        List<String>  sqllist=new ArrayList<>();
        sqllist.add(sql);
        return sqllist;
    }

}
