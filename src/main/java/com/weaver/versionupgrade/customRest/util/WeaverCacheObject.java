package com.weaver.versionupgrade.customRest.util;

import java.io.Serializable;
import java.util.Collections;
import java.util.Vector;

/**
 * 缓存对象类
 */
public class WeaverCacheObject implements Serializable {


    /**
     * 值
     */
    private Vector array;

    /**
     * 列名 数组
     */
    private String columnName[];

    /**
     * 列类型 数组
     */
    private int columnType[];


    public Vector getArray() {
        return array;
    }

    public void setArray(Vector array) {
        this.array =array;
    }

    public String[] getColumnName() {
        return columnName;
    }

    public void setColumnName(String[] columnName) {
        this.columnName = columnName;
    }

    public int[] getColumnType() {
        return columnType;
    }

    public void setColumnType(int[] columnType) {
        this.columnType = columnType;
    }
}
