package com.weaver.versionupgrade.customRest.db;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.commons.lang3.StringUtils;


import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cx
 * @date 2023/9/7
 **/
public class DBPools {
    private ConcurrentHashMap<String, DruidDataSource> poolsMap = new ConcurrentHashMap();

    public ConcurrentHashMap<String, DruidDataSource> getPoolsMap () {
        return poolsMap ;
    }

    public void setPoolsMap(ConcurrentHashMap<String, DruidDataSource> poolsMap ) {
        this.poolsMap = poolsMap ;
    }

    public void addPools(String url,DruidDataSource dataSource  ) {
        if(StringUtils.isNotBlank(url) && dataSource != null  && !poolsMap.containsKey(dataSource)){
            poolsMap.put(url, dataSource);
        }
    }
}
