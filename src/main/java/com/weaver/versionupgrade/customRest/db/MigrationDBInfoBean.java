package com.weaver.versionupgrade.customRest.db;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author cx
 * @date 2023/9/4
 **/
public class MigrationDBInfoBean {
    /**
     * 产品类型
     */
    public String productType;

    /**
     * 部署类型
     */
    public String deployType;


    /**
     * 单体库配置
     */
    public Map<String, String> dbInfo = new ConcurrentHashMap<>();

    public MigrationDBInfoBean() {

    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getDeployType() {
        return deployType;
    }

    public void setDeployType(String deployType) {
        this.deployType = deployType;
    }

    public Map<String, String> getDbInfo() {
        return dbInfo;
    }

    public void setDbInfo(Map<String, String> dbInfo) {
        this.dbInfo = dbInfo;
    }
}
