package com.weaver.versionupgrade.customRest.db;

/**
 * @author cx
 * @date 2023/9/7
 **/
public class DBUTKey {
    private String useType = "";//使用类型
    private String type = "";//源库/目标库类型

    public DBUTKey(String useType, String type) {
        this.useType = useType;
        this.type = type;
    }


    @Override
    public int hashCode() {
        return this.useType.toLowerCase().hashCode() + this.type.toLowerCase().hashCode();
    }
    @Override
    public String toString() {
        return "{useType:"+this.useType.toLowerCase() +",type:"+ this.type.toLowerCase()+"}";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        DBUTKey s = (DBUTKey) obj;
        return this.useType.toLowerCase().equals(s.useType.toLowerCase())
            && this.type.toLowerCase().equals(s.type.toLowerCase());
    }
    public String getUseType() {
        return useType;
    }

    public void setUseType(String useType) {
        this.useType = useType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
