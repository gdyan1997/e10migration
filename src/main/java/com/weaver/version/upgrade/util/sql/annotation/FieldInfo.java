package com.weaver.version.upgrade.util.sql.annotation;

import java.lang.annotation.*;

/**
 * 建模表单无需迁移配置注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface FieldInfo {
    String dbName();

    String name();// 可配置多个表名使用逗号分隔

    boolean isSelect() default false;// 可配置多个表名使用逗号分隔

    String mappingJson() default "";// 可配置多个表名使用逗号分隔
}