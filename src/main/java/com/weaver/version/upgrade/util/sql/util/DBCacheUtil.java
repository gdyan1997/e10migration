package com.weaver.version.upgrade.util.sql.util;

import com.weaver.version.upgrade.util.sql.cache.SqlCache;

import java.util.*;

/**
 * @author ygd2020
 * @date 2023-2-7 17:23
 * Description:  数据库相关系统关键字  weaver-common-form/weaver-common-form-physical/src/main/java/com/weaver/common/form/physical/util/PhysicalTableDataUtils.java
 **/
public class DBCacheUtil {

    public static HashSet<String> sysfieldnames = new HashSet<>();


    /**
     * MYSQL 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] MYSQL_KEYS = new String[]{"ADD", "ALL", "ALTER", "ANALYZE", "AND", "AS", "ASC", "ASENSITIVE", "BEFORE", "BETWEEN", "BIGINT", "BINARY", "BLOB", "BOTH", "BY", "CALL", "CASCADE", "CASE", "CHANGE", "CHAR", "CHARACTER", "CHECK", "COLLATE", "COLUMN", "CONDITION", "CONSTRAINT", "CONTINUE", "CONVERT", "CREATE", "CROSS", "CUBE", "CUME_DIST", "CURRENT_DATE", "CURRENT_TIME", "CURRENT_TIMESTAMP", "CURRENT_USER", "CURSOR", "DATABASE", "DATABASES", "DAY_HOUR", "DAY_MICROSECOND", "DAY_MINUTE", "DAY_SECOND", "DEC", "DECIMAL", "DECLARE", "DEFAULT", "DELAYED", "DELETE", "DENSE_RANK", "DESC", "DESCRIBE", "DETERMINISTIC", "DISTINCT", "DISTINCTROW", "DIV", "DOUBLE", "DROP", "DUAL", "EACH", "ELSE", "ELSEIF", "EMPTY", "ENCLOSED", "ESCAPED", "EXCEPT", "EXISTS", "EXIT", "EXPLAIN", "FALSE", "FETCH", "FIRST_VALUE", "FLOAT", "FLOAT4", "FLOAT8", "FOR", "FORCE", "FOREIGN", "FROM", "FULLTEXT", "FUNCTION", "GENERATED", "GET", "GRANT", "GROUP", "GROUPING", "GROUPS", "HAVING", "HIGH_PRIORITY", "HOUR_MICROSECOND", "HOUR_MINUTE", "HOUR_SECOND", "IF", "IGNORE", "IN", "INDEX", "INFILE", "INNER", "INOUT", "INSENSITIVE", "INSERT", "INT", "INT1", "INT2", "INT3", "INT4", "INT8", "INTEGER", "INTERVAL", "INTO", "IO_AFTER_GTIDS", "IO_BEFORE_GTIDS", "IS", "ITERATE", "JOIN", "JSON_TABLE", "KEY", "KEYS", "KILL", "LAG", "LAST_VALUE", "LATERAL", "LEAD", "LEADING", "LEAVE", "LEFT", "LIKE", "LIMIT", "LINEAR", "LINES", "LOAD", "LOCALTIME", "LOCALTIMESTAMP", "LOCK", "LONG", "LONGBLOB", "LONGTEXT", "LOOP", "LOW_PRIORITY", "MASTER_BIND", "MASTER_SSL_VERIFY_SERVER_CERT", "MATCH", "MAXVALUE", "MEDIUMBLOB", "MEDIUMINT", "MEDIUMTEXT", "MIDDLEINT", "MINUTE_MICROSECOND", "MINUTE_SECOND", "MOD", "MODIFIES", "NATURAL", "NOT", "NO_WRITE_TO_BINLOG", "NTH_VALUE", "NTILE", "NULL", "NUMERIC", "OF", "ON", "OPTIMIZE", "OPTIMIZER_COSTS", "OPTION", "OPTIONALLY", "OR", "ORDER", "OUT", "OUTER", "OUTFILE", "OVER", "PARTITION", "PARTITIONING", "PARTITIONS", "PASSWORD", "PASSWORD_LOCK_TIME", "PATH", "PERCENT_RANK", "PERSIST6", "PERSIST_ONLY6", "PHASE", "PLUGIN", "PLUGINS", "PLUGIN_DIR", "POINT", "POLYGON", "PORT", "PRECEDES", "PRECEDING", "PRECISION", "PREPARE", "PRESERVE", "PREV", "PRIMARY", "PRIVILEGES", "PRIVILEGE_CHECKS_USER", "PROCEDURE", "PROCESS", "PROCESSLIST", "PROFILE", "PROFILES", "PROXY", "PURGE", "RANGE", "RANK", "READ", "READS", "READ_WRITE", "REAL", "RECURSIVE", "REFERENCES", "REGEXP", "RELEASE", "RENAME", "REPEAT", "REPLACE", "REQUIRE", "RESIGNAL", "RESTRICT", "RETURN", "REVOKE", "RIGHT", "RLIKE", "ROW", "ROWS", "ROW_NUMBER", "SCHEMA", "SCHEMAS", "SECOND_MICROSECOND", "SELECT", "SENSITIVE", "SEPARATOR", "SET", "SHOW", "SIGNAL", "SMALLINT", "SPATIAL", "SPECIFIC", "SQL", "SQLEXCEPTION", "SQLSTATE", "SQLWARNING", "SQL_BIG_RESULT", "SQL_CALC_FOUND_ROWS", "SQL_NO_CACHE", "SQL_SMALL_RESULT", "SSL", "STARTING", "STORED", "STRAIGHT_JOIN", "SYSTEM", "TABLE", "TERMINATED", "THEN", "TINYBLOB", "TINYINT", "TINYTEXT", "TO", "TRAILING", "TRIGGER", "TRUE", "UNDO", "UNION", "UNIQUE", "UNLOCK", "UNSIGNED", "UPDATE", "USAGE", "USE", "USING", "UTC_DATE", "UTC_TIME", "UTC_TIMESTAMP", "VALUES", "VARBINARY", "VARCHAR", "VARCHARACTER", "VARYING", "VIRTUAL", "WHEN", "WHERE", "WHILE", "WINDOW", "WITH", "WRITE", "XOR", "YEAR_MONTH", "ZEROFILL"};
    /**
     * POSTGRESQL 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] POSTGRESQL_KEYS = new String[]{"ENCKEY","ALL", "ANALYSE", "ANALYZE", "AND", "ANY", "ARRAY", "AS", "ASC", "BINARY", "BOTH", "CASE", "CHECK", "COLLATE", "COLLATION", "COLUMN", "CONSTRAINT", "CREATE", "CROSS", "CURRENT_DATE", "CURRENT_TIMESTAMP", "CURRENT_USER", "DEFAULT", "DESC", "DISTINCT", "DO", "ELSE", "END", "EXCEPT", "FALSE", "FETCH", "FOR", "FOREIGN", "FROM", "FULL", "GRANT", "GROUP", "HAVING", "IN", "INNER", "INTO", "IS", "JOIN", "LATERAL", "LEADING", "LEFT", "LIKE", "LIMIT", "LOCALTIME", "LOCALTIMESTAMP", "NATURAL", "NOT", "NULL", "OFFSET", "ON", "ONLY", "OR", "ORDER", "OUTER", "PRIMARY", "REFERENCES", "RETURNING", "RIGHT", "SELECT", "SOME", "TABLE", "THEN", "TO", "TRAILING", "TRUE", "UNION", "UNIQUE", "USER", "USING", "WHEN", "WHERE", "WINDOW", "WITH", "ARRAY", "EXCEPT", "LATERAL"};
    /**
     * ORACLE 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] ORACLE_KEYS = new String[]{"ACCESS", "ADD", "ALL", "ALTER", "AND", "ANY", "AS", "ASC", "AUDIT", "BEGIN", "BETWEEN", "BY", "CHAR", "CHECK", "CLUSTER", "COLUMN", "COMMENT", "COMPRESS", "CONNECT", "CREATE", "CURRENT", "DATE", "DECIMAL", "DECLARE", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DROP", "ELSE", "EXCLUSIVE", "EXISTS", "EXTERNAL", "FILE", "FLOAT", "FOR", "FROM", "GRANT", "GROUP", "HAVING", "IDENTIFIED", "IMMEDIATE", "IN", "INCREMENT", "INDEX", "INITIAL", "INSERT", "INTEGER", "INTERSECT", "INTO", "IS", "LEVEL", "LIKE", "LOCK", "LONG", "MAXEXTENTS", "MINUS", "MLSLABEL", "MODE", "MODIFY", "NOAUDIT", "NOCOMPRESS", "NOT", "NOWAIT", "NULL", "NUMBER", "OF", "OFF", "OFFLINE", "ON", "ONLINE", "OPTION", "OR", "ORA_ROWSCN", "ORDER", "OVERFLOW", "PCTFREE", "PRIOR", "PUBLIC", "RAW", "RENAME", "RESOURCE", "REVOKE", "ROW", "ROWID", "ROWNUM", "ROWS", "SELECT", "SESSION", "SET", "SHARE", "SIZE", "SMALLINT", "START", "SUCCESSFUL", "SYNONYM", "SYSDATE", "TABLE", "THEN", "TO", "TRIGGER", "UID", "UNION", "UNIQUE", "UPDATE", "USER", "VALIDATE", "VALUES", "VARCHAR", "VARCHAR2", "VIEW", "WHENEVER", "WHERE", "WITH"};

    /**
     * SQLSERVER 数据库 关键字
     *
     * @author mcl
     * @date 2023/2/8 10:20
     */
    public static String[] SQLSERVER_KEYS = new String[]{"TOP","ADD", "EXTERNAL", "PROCEDURE", "ALL", "FETCH", "PUBLIC", "ALTER", "FILE", "RAISERROR", "AND", "FILLFACTOR", "READ", "ANY", "FOR", "READTEXT", "AS", "FOREIGN", "RECONFIGURE", "ASC", "FREETEXT", "REFERENCES", "AUTHORIZATION", "FREETEXTTABLE", "BACKUP", "FROM", "RESTORE", "BEGIN", "FULL", "RESTRICT", "BETWEEN", "FUNCTION", "RETURN", "BREAK", "GOTO", "REVERT", "BROWSE", "GRANT", "REVOKE", "BULK", "GROUP", "RIGHT", "BY", "HAVING", "ROLLBACK", "CASCADE", "HOLDLOCK", "ROWCOUNT", "CASE", "IDENTITY", "ROWGUIDCOL", "CHECK", "IDENTITY_INSERT", "RULE", "CHECKPOINT", "IDENTITYCOL", "SAVE", "CLOSE", "IF", "SCHEMA", "CLUSTERED", "IN", "SECURITYAUDIT", "COALESCE", "INDEX", "SELECT", "COLLATE", "INNER", "COLUMN", "INSERT", "COMMIT", "INTERSECT", "COMPUTE", "INTO", "SESSION_USER", "CONSTRAINT", "IS", "SET", "CONTAINS", "JOIN", "SETUSER", "CONTAINSTABLE", "KEY", "SHUTDOWN", "CONTINUE", "KILL", "SOME", "CONVERT", "LEFT", "STATISTICS", "CREATE", "LIKE", "SYSTEM_USER", "CROSS", "LINENO", "TABLE", "CURRENT", "TABLESAMPLE", "CURRENT_DATE", "MERGE", "TEXTSIZE", "CURRENT_TIME", "NATIONAL", "THEN", "CURRENT_TIMESTAMP", "NOCHECK", "TO", "CURRENT_USER", "TRANSACTION", "CURSOR", "DEALLOCATE", "UNION", "NATIONAL", "UNIQUE", "DECLARE", "UPDATE", "DEFAULT", "DELETE", "USER", "DESC", "NOT", "NULL", "NULLIF", "VALUES", "VARYING", "DISTINCT", "OF", "VIEW", "ON", "WHEN", "DOUBLE", "DROP", "OPEN", "WHERE", "ELSE", "OPTION", "WITH", "END", "OR", "END-EXEC", "ORDER", "ESCAPE", "OUTER", "EXCEPT", "ABSOLUTE", "NULL", "PRIMARY", "REWRITE_OR_ERROR", "SIZE", "TRIGGER", "TRUNCATE", "USE"};



    /**
     * 固定列名称  不可重复
     *
     * @author mcl
     * @date 2022/2/16 17:24
     */
    public static final String[] FIX_COLUMN_NAMES = new String[]{"ID", "FORM_DATA_ID", "DATA_INDEX", "CREATE_TIME", "UPDATE_TIME", "TENANT_KEY", "IS_DELETE", "DELETE_TYPE", "TEMP_COLUMN", "FT", "FT0", "FT1", "FT2", "FT3", "FT4", "FT5", "FT6", "FT7", "FT8", "FT9", "FT10", "FD", "FT_STATUS"};





    public static String DB_TYPE;
    //数据库连接符号
    public static String DB_CONCAT_SYMBOL;
    public static String DB_ISNULL_FUN;
    public static String DB_SUBSTR_FUN;

    public static String DB_MYSQL_SCHEMA = "";


    public static HashSet<String> cubeSysfieldname = new HashSet<String>()//eb表单关键字
    {{

        this.add("ID");
        this.add("TENANT_KEY");
        this.add("OBJ_ID");
        this.add("NAME");
        this.add("FORM_DATA_ID");
        this.add("FORM_TABLE_ID");
        this.add("IS_FLOW");
        this.add("FLOW_ID");
        this.add("DATA_STATUS");
        this.add("CURRENT_STEP");
        this.add("FLOW_STATUS");
        this.add("CREATOR");
        this.add("CREATE_TIME");
        this.add("UPDATE_TIME");
        this.add("DELETE_TYPE");
        this.add("IS_TOP");
        this.add("CLASSIFICATION");
        this.add("CLASSIFICATION_EXPIRE");
        this.add("UPDATER");
        this.add("DN_FIRST");
        this.add("MULTI_PATH_ID");
        this.add("MULTI_TASK_ID");
        this.add("FLOW_SYSTEM_NUMBER");
        this.add("SOURCE_FROM");
        this.add("DATA_INDEX");
        this.add("IS_DELETE");
    }};



    public static HashSet<String> fixFieldNames = new HashSet<String>() {{// 需要原值返回的字段名  e9e10 都存在并且概念一致
        this.add("ID");
    }};


    public static HashMap<String, String> canReplaceFieldNames = new HashMap<String, String>() {{//属于 e9系统字段，并且能替换e10系统字段
        // 建模字段 可替换的
        this.put("MODEDATACREATER", "creator");//数据创建人
        this.put("MODEDATACREATEDATE", "create_time");//创建时间
        this.put("MODEDATAMODIFIER", "updater");//更新人
        this.put("MODEDATAMODIFYDATETIME", "update_time");//更新时间


    }};




    public static void initDBFields() {

        String dbtype = SqlCache.getDbType();

        List<String> sysnames = new ArrayList<>();


        if ("oracle".equalsIgnoreCase(dbtype)) {
            sysnames = Arrays.asList(ORACLE_KEYS);
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            sysnames = Arrays.asList(MYSQL_KEYS);
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            sysnames = Arrays.asList(POSTGRESQL_KEYS);
        } else {
            sysnames = Arrays.asList(SQLSERVER_KEYS);
        }

        sysfieldnames = new HashSet<>(sysnames);//数据库关键字

        sysfieldnames.addAll(Arrays.asList(FIX_COLUMN_NAMES));//表单引擎关键字

        sysfieldnames.addAll(cubeSysfieldname);// eb表单关键字


    }


    public static boolean isinit = false;


    /**
     * 是否缓存
     *
     * @param
     * @return
     */
    public static boolean isloadCache() {

        if (!isinit) {
            initCache();
        }
        return true;
    }

    /**
     * 初始化建模有效的数据
     *
     * @param
     * @return
     */
    public static synchronized void initCache() {
        if (isinit) {
            return;
        }

        initDBFields();


        isinit = true;
    }

    public static HashSet<String> getSysFieldNames() {

        isloadCache();


        return sysfieldnames;

    }

    public static String  getSourceDB_TYPE() {

        isloadCache();


        return DB_TYPE;

    }
    public static String  getSourceDB_ISNULL_FUN() {

        isloadCache();


        return DB_ISNULL_FUN;

    }


}
