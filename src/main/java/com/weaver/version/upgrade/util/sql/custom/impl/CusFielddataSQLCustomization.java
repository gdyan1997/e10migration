package com.weaver.version.upgrade.util.sql.custom.impl;

import com.weaver.version.upgrade.util.sql.custom.SQLCustomization;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.matrixFieldIdMapping;

/**
 * @author wujiahao
 * @date 2024/12/12 下午4:26
 */
public class CusFielddataSQLCustomization implements SQLCustomization {
    @Override
    public String transformTableName(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {

        TransUtil.addTransErrorMsg("自定义字段表 cus_fielddata 在E9多个模块使用，请确认E10是否需要调整!!!" );
        //hr_userinfo
        List<String> hrmcuscardinfoFiledList=new ArrayList<>();
        //employee
        List<String> hrmemployeedefinedFiledList=new ArrayList<>();
        for (String fieldName : usedColumns) {
            if (matrixFieldIdMapping.containsKey(TransUtil.getTenantKey() + "_cus_" + fieldName)) {
                Map<String, Object> stringObjectMap = (Map<String, Object>) matrixFieldIdMapping.get(TransUtil.getTenantKey() + "_cus_" + fieldName);
                if (stringObjectMap.containsKey("-1")) {
                    hrmemployeedefinedFiledList.add(fieldName);
                } else {
                    stringObjectMap.keySet().stream().findFirst().ifPresent(scopeid -> {
                        if (scopeid.startsWith("-")) {
                            scopeid = scopeid.substring(1);
                        }
                        hrmcuscardinfoFiledList.add(fieldName + "_" + scopeid + " as " + fieldName);
                    });
                }
                //存在多个字段
                if (stringObjectMap.size() > 1) {
                    Iterator<String> iterator = stringObjectMap.keySet().iterator();
                    String msg = "";
                    while (iterator.hasNext()) {
                        String msg_tmp = "";
                        String scopeid = iterator.next();
                        Map<String, String> map = (Map<String, String>) stringObjectMap.get(scopeid);
                        String hrm_fieldlable = map.get("hrm_fieldlable");
                        String formlabel = map.get("formlabel");
                        String prefix = "分类:" +  formlabel+ " 字段名:" + hrm_fieldlable+" E10存储在";
                        if ("-1".equalsIgnoreCase(scopeid)) {
                            msg_tmp += "表hrmemployeedefined中字段" + fieldName + " ";
                        } else {
                            if (scopeid.startsWith("-")) {
                                scopeid = scopeid.substring(1);
                            }
                            msg_tmp += "表hrmcuscardinfo字段" + fieldName + "_" + scopeid + " ";
                        }
                        msg += prefix + msg_tmp;
                    }
                    TransUtil.addTransErrorMsg("cus_fielddata 表中字段=>" + fieldName + "迁移到E10在多处存储,请手动确认sql! " +
                            "" + msg);
                }
            }
        }
        String newSql=sourceTableName;
        String fromSql="";
        String queryField="";
        if(hrmcuscardinfoFiledList.size()>0&&hrmemployeedefinedFiledList.size()==0){
            queryField+="select  t1.id as id ,t1.delete_type,t1.tenant_key ,t3.";
            queryField+= StringUtils.join(hrmcuscardinfoFiledList, ",t3.");
            fromSql=" FROM employee t1\n" +
                    "         LEFT JOIN hr_userinfo t2 " +
                    "         ON t1.id = t2.user\n" +
                    "         left join hrmcuscardinfo t3 on t2.form_data = t3.form_data_id ";
            newSql="("+queryField+fromSql+")";
        }else if(hrmemployeedefinedFiledList.size()>0&&hrmcuscardinfoFiledList.size()==0){

            queryField+="select  t1.id as id ,t1.delete_type,t1.tenant_key,t2.";
            queryField+=StringUtils.join(hrmemployeedefinedFiledList, ",t2.");
            fromSql=" from    employee t1\n" +
                    "         left join hrmemployeedefined t2 on t1.formdata = t2.form_data_id ";
            newSql="("+queryField+fromSql+")";

        }else if(hrmemployeedefinedFiledList.size()>0&&hrmcuscardinfoFiledList.size()>0){
            queryField += "select  t1.id as id ,t1.delete_type,t1.tenant_key, t2. ";
            queryField += StringUtils.join(hrmemployeedefinedFiledList, ",t2.");
            queryField += ",t4.";
            queryField += StringUtils.join(hrmcuscardinfoFiledList, ",t4.");
            fromSql = "  from employee t1\n" +
                    "    left join hrmemployeedefined t2 on t1.formdata = t2.form_data_id\n" +
                    "    left join hr_userinfo t3 on t1.id = t3.user\n" +
                    "    left join hrmcuscardinfo t4 on t4.form_data_id = t3.form_data ";
            newSql = "(" + queryField + fromSql + ")";
        }
        return newSql;
    }

    @Override
    public String transformJoinTable(String e9tableName, HashMap<String, String> conditionMap, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformJoinStatement(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformColumnName(String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformConditionValue(String sourceValue, String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }
}
