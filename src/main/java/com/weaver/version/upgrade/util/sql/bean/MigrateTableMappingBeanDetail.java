package com.weaver.version.upgrade.util.sql.bean;

import com.weaver.version.upgrade.util.sql.annotation.FieldInfo;

/**
 * @author wujiahao
 * @date 2024/11/21 上午10:51
 */
public class MigrateTableMappingBeanDetail {

    //detail
    @FieldInfo(dbName="e10_tablename",name = "字段对应E10表名称")
    private String e10TableName;

    @FieldInfo(dbName="e9_tablefield",name = "E9字段")
    private String e9TableField;

    @FieldInfo(dbName="e10_tablefield",name = "E10字段")
    private String e10TableField;


    /**
     * {"原值无需转换":"3","当前表主键":"1","外键字段":"2","自定义值转换":"4"}
     */
    @FieldInfo(dbName="field_type",name = "字段值转换设置",isSelect = true,mappingJson = "{\"原值无需转换\":\"3\",\"当前表主键\":\"1\",\"外键字段\":\"2\",\"自定义值转换\":\"4\"}")
    private String fieldType;

    /**
     * {"无需特殊处理":"0","需要截取出日期":"1","需要截取出时间":"2","改写成子查询":"3","改写成子查询且需要行专列（使用逗号分隔）":"4","改写成子查询且需要行专列(默认前后拼接逗号)":"5"}
     */
    @FieldInfo(dbName="zdzhsz",name = "字段转换设置",isSelect = true,mappingJson = "{\"无需特殊处理\":\"0\",\"需要截取出日期\":\"1\",\"需要截取出时间\":\"2\",\"改写成子查询\":\"3\",\"改写成子查询且需要行专列（使用逗号分隔）\":\"4\",\"改写成子查询且需要行专列(默认前后拼接逗号)\":\"5\"}")
    private String zdzhsz;


    /**
     * {"可以完全替换":"0","需要参考描述调整":"1"}
     */
    @FieldInfo(dbName="sfkywqth",name = "是否可以完全替换",isSelect = true,mappingJson = "{\"可以完全替换\":\"0\",\"需要参考描述调整\":\"1\"}")
    private String sfkywqth;


    @FieldInfo(dbName="zdgnms",name = "字段功能描述")
    private String zdgnms;


    /**
     * {"来源于E10主表":"0","需要改写FROM TABLE":"1"}
     */
    @FieldInfo(dbName="join_type",name = "数据来源",isSelect = true,mappingJson = "{\"来源于E10主表\":\"0\",\"需要改写FROM TABLE\":\"1\"}")
    private String joinType;


    @FieldInfo(dbName="tmp",name = "替换描述")
    private String tmp;


    @FieldInfo(dbName="join_condition",name = "连接条件")
    private String joinCondition;


    @FieldInfo(dbName="zcx",name = "子查询")
    private String sqlQuery;


    @FieldInfo(dbName="json_mapping",name = "自定义值转换设置")
    private String jsonMapping;

    @FieldInfo(dbName="relevancytabl",name = "外键对应E10表")
    private String relevancytabl;


    @FieldInfo(dbName="relevancyfiel",name = "外键对应E10字段")
    private String relevancyfiel;

    @FieldInfo(dbName="e9relevancyta",name = "外键对应E9表")
    private String e9relevancyta;

    @FieldInfo(dbName="e9relevancyfi",name = "外键对应E9字段")
    private String e9relevancyfi;

    public String getZdzhsz() {
        return zdzhsz;
    }

    public void setZdzhsz(String zdzhsz) {
        this.zdzhsz = zdzhsz;
    }

    public String getE10TableName() {
        return e10TableName;
    }

    public void setE10TableName(String e10TableName) {
        this.e10TableName = e10TableName;
    }

    public String getE9TableField() {
        return e9TableField;
    }

    public void setE9TableField(String e9TableField) {
        this.e9TableField = e9TableField;
    }

    public String getE10TableField() {
        return e10TableField;
    }

    public void setE10TableField(String e10TableField) {
        this.e10TableField = e10TableField;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getSfkywqth() {
        return sfkywqth;
    }

    public void setSfkywqth(String sfkywqth) {
        this.sfkywqth = sfkywqth;
    }

    public String getZdgnms() {
        return zdgnms;
    }

    public void setZdgnms(String zdgnms) {
        this.zdgnms = zdgnms;
    }

    public String getJoinType() {
        return joinType;
    }

    public void setJoinType(String joinType) {
        this.joinType = joinType;
    }

    public String getTmp() {
        return tmp;
    }

    public void setTmp(String tmp) {
        this.tmp = tmp;
    }

    public String getJoinCondition() {
        return joinCondition;
    }

    public void setJoinCondition(String joinCondition) {
        this.joinCondition = joinCondition;
    }

    public String getJsonMapping() {
        return jsonMapping;
    }

    public void setJsonMapping(String jsonMapping) {
        this.jsonMapping = jsonMapping;
    }

    public String getRelevancytabl() {
        return relevancytabl;
    }

    public void setRelevancytabl(String relevancytabl) {
        this.relevancytabl = relevancytabl;
    }

    public String getRelevancyfiel() {
        return relevancyfiel;
    }

    public void setRelevancyfiel(String relevancyfiel) {
        this.relevancyfiel = relevancyfiel;
    }

    public String getE9relevancyta() {
        return e9relevancyta;
    }

    public void setE9relevancyta(String e9relevancyta) {
        this.e9relevancyta = e9relevancyta;
    }

    public String getE9relevancyfi() {
        return e9relevancyfi;
    }

    public void setE9relevancyfi(String e9relevancyfi) {
        this.e9relevancyfi = e9relevancyfi;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }
}
