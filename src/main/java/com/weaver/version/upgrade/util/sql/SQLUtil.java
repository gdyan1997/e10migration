package com.weaver.version.upgrade.util.sql;


import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.parser.ParserException;
import com.alibaba.druid.util.JdbcConstants;
import com.alibaba.fastjson.JSONObject;
import com.weaver.version.upgrade.util.sql.bean.DataConnBeanChild;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBeanDetail;
import com.weaver.version.upgrade.util.sql.bean.SqlBean;
import com.weaver.version.upgrade.util.sql.bean.SqlReplaceParam;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.version.upgrade.util.sql.enums.SqlEnumVisitor;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.errorList;


public class SQLUtil {

    private static final Logger logger = LoggerFactory.getLogger(SQLUtil.class);


    //栈 用于存储表中涉及到的字段。
    public static Queue<List<String>> queue = new LinkedList();

    /**
     * 返回sql替换结果
     */
    public static Map<String, Object> sqlResultInfo = new HashMap<>();



    //判断sql 最终查询结果，给自定义浏览按钮用某个字段最终属于那个表。给yhq用

    /**
     * 给yhq用
     *
     * @param sql
     * @param fieldName
     * @return
     */
    public static Map<String, Object> getFieldTableBrowserMenu(String sql, String fieldName) {
        //System.out.println();
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setSourceCode(sql);
        sqlReplaceParam.setBrowserFieldName(fieldName.toLowerCase().trim());
        Map<String, Object> map = replaceSql(sqlReplaceParam);
        Map<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("browserMenuTable", map.get("browserMenuTable"));
        objectObjectHashMap.put("browserMenuField", map.get("browserMenuField"));
        return objectObjectHashMap;
    }


    /**
     * @param sql
     * @param dbtype
     * @param targetDbType
     * @return targetCode =>替换后的sql
     * transStatus => 转换信息
     * transMsg   =>替换信息  给ygd用。
     */
    public static Map<String, Object> replaceSqlResultMapYgd(String sql) {
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setFirstQueryField(true);
        sqlReplaceParam.setSourceCode(sql);
        return replaceSql(sqlReplaceParam);
    }


    public static String replaceSql(String sql, String dbtype) {
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setFirstQueryField(false);
        sqlReplaceParam.setSourceCode(sql);
        sqlReplaceParam.setDbtype(dbtype);
        return String.valueOf(replaceSql(sqlReplaceParam).get("targetCode"));
    }


    public static String replaceSqlbyDataSource(String sql, String e9datasouceid) {
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setFirstQueryField(false);
        sqlReplaceParam.setSourceCode(sql);
        sqlReplaceParam.setDatasetId(e9datasouceid);

        return String.valueOf(replaceSql(sqlReplaceParam).get("targetCode"));
    }

    /**
     * 替换sql
     *
     * @param sql
     * @return
     */
    public static String replaceSql(String sql) {
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setFirstQueryField(false);
        sqlReplaceParam.setSourceCode(sql);
        return String.valueOf(replaceSql(sqlReplaceParam).get("targetCode"));


    }


    /**
     * @param sql
     * @param dbtype
     * @param targetDbType
     * @return targetCode =>替换后的sql
     * transStatus => 转换信息
     * transMsg   =>替换信息
     */
    public static Map<String, Object> replaceSqlResultMap(String sql) {
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setFirstQueryField(false);
        sqlReplaceParam.setSourceCode(sql);
        return replaceSql(sqlReplaceParam);
    }

    public static Map<String, Object> replaceSqlResultMap(String sql, String dbtype, String targetDbType) {
        SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
        sqlReplaceParam.setDbtype(dbtype);
        sqlReplaceParam.setTargetDbType(targetDbType);
        sqlReplaceParam.setSourceCode(sql);
        return replaceSql(sqlReplaceParam);
    }

    /**
     * @param list 替换list字符串组合的sql片段
     * @return 给yhq用
     */
    public synchronized static Map<String, Object> replaceSql(List<String> list, Map<String, String> transMap) {


        List list_bak = new ArrayList(list);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("sqlList", list_bak);
        sqlResultInfo.clear();

        //走标准的替换逻辑
        if (list.size() == 1) {
            SqlReplaceParam sqlReplaceParam = new SqlReplaceParam();
            sqlReplaceParam.setFirstQueryField(false);
            sqlReplaceParam.setSourceCode(list.get(0));
            Map<String, Object> map = replaceSqlResultMap(sqlReplaceParam);
            if (!map.get("status").equals("0")) {
                list_bak.set(0, String.valueOf(map.get("targetCode")));
                resultMap.put("transMsg", sqlResultInfo.get("transMsg"));
            } else {
                SqlCustomParser sqlParser = new SqlCustomParser();
                resultMap.put("transMsg", sqlResultInfo.get("transMsg"));
                resultMap.put("sqlList", sqlParser.convertSql(list_bak, transMap));
                return resultMap;
            }
        }

        if (list.size() > 1) {
            sqlResultInfo.clear();
            SqlCustomParser sqlParser = new SqlCustomParser();
            List<String> list_tmp = sqlParser.convertSql(list, transMap);
            //封装转换信息
            buildTransInfo();
            resultMap.put("sqlList", list_tmp);
            resultMap.put("transMsg", sqlResultInfo.get("transMsg"));
            return resultMap;
        }

        return resultMap;
    }


    /**
     * 用于sql替换率统计
     */
    public static Integer totalReplacement = 0;
    public static Integer fullReplacement = 0;
    public static Integer partialUnreplacedSQL = 0;
    public static Integer incorrectSQL = 0;
    public static Integer nonStandardFields = 0;


    public static Integer dmlTotalReplacement = 0;
    public static Integer dmlFullReplacement = 0;
    public static Integer dmlPartialUnreplacedSQL = 0;
    public static Integer dmlIncorrectSQL = 0;
    public static Integer dmlNonStandardFields = 0;


    public static Map<String, Integer> methodUesCount = new ConcurrentHashMap<>();

    /*    totalReplacement - 表示总量的变量
        /**
         *
         * @param sql
         * @param dbtype
         * @param targetDbType
         * @return
         *  targetCode =>替换后的sql
         *  transStatus => 转换信息
         *  transMsg   =>替换信息
         */
    public synchronized static Map<String, Object> replaceSql(SqlReplaceParam sqlReplaceParam) {
        try {

            if (StringUtils.isBlank(sqlReplaceParam.getSourceCode())) {
                Map<String, Object> tt = new HashMap<>();
                tt.put("targetCode", sqlReplaceParam.getSourceCode());
                tt.put("sourceCode", sqlReplaceParam.getSourceCode());

                tt.put("connBean", SqlCache.getShuJuYuanBeanByPointId(sqlReplaceParam.getDatasetId(), sqlReplaceParam.isDataWarehouse()));
                return tt;
            }
            Map<String, Object> map = replaceSqlResultMap(sqlReplaceParam);
            String status = TransUtil.null2String(map.get("status"));


            boolean isQuery = (boolean) sqlResultInfo.get("isQuery");

            if (isQuery) {
                totalReplacement++;
            } else {
                dmlTotalReplacement++;
            }

            String transStatus;
            if (status.equals("0")) {
                transStatus = "不是正确的sql";
                if (isQuery) {
                    incorrectSQL++;
                } else {
                    dmlIncorrectSQL++;
                }
            } else if (status.equals("1")) {
                transStatus = "sql未完全替换";
                if (isQuery) {
                    partialUnreplacedSQL++;
                } else {
                    dmlPartialUnreplacedSQL++;
                }
            } else if (status.equals("2")) {
                transStatus = "完全替换";
                if (isQuery) {
                    fullReplacement++;
                } else {
                    dmlFullReplacement++;
                }
            } else if (status.equals("3")) {
                transStatus = "解析sql过程中报错，如需要替换请找武佳豪确认!";
            } else if (status.equals("4")) {
                transStatus = "sql中存在表或字段不是oa标准表，请确认是否需要调整";
                if (isQuery) {
                    nonStandardFields++;
                } else {
                    dmlNonStandardFields++;
                }
            } else {
                transStatus = "不是正确的sql";
                if (isQuery) {
                    incorrectSQL++;
                } else {
                    dmlIncorrectSQL++;
                }
            }


            //简单
            String info="简单";
            HashSet tableSet = (HashSet) sqlResultInfo.getOrDefault("tableSet", new HashSet<>());
            HashSet tableField = (HashSet) sqlResultInfo.getOrDefault("tableField", new HashSet<>());


            if (tableSet.size()<2&&tableField.size()<8){
                info="简单";
            }else if (tableSet.size()<3&&tableField.size()<10){
                info="一般";
            }else {
                info="复杂";
            }

            map.put("transStatus", transStatus+"|||"+info);

            if (!isQuery) {
                map.put("transStatus", "dml请一一确认，替换只做参考"+"|||"+info);
            }






            return map;
        } catch (Exception e) {
            logger.error(" replaceSql user or userids is empty ",e);
        }

        Map<String, Object> tt = new HashMap<>();
        tt.put("targetCode", sqlReplaceParam.getSourceCode());
        tt.put("sourceCode", sqlReplaceParam.getSourceCode());
        return tt;
    }


    /**
     * 替换完整sql
     *
     * @param sql
     * @param datasourceid
     * @return
     */
    private synchronized static Map<String, Object> replaceSqlResultMap(SqlReplaceParam sqlReplaceParam) {

        try {


            sqlResultInfo.clear();


            String sql = sqlReplaceParam.getSourceCode();

            // 编译正则表达式
            Pattern pattern = Pattern.compile("^\\s*(UPDATE|DELETE|INSERT)\\s", Pattern.CASE_INSENSITIVE);
            // 创建匹配器
            Matcher matcher = pattern.matcher(sql);
            if (matcher.find()) {
                sqlResultInfo.put("isQuery", false);
            }else {
                sqlResultInfo.put("isQuery", true);
            }




            sql = sql.trim();
            String dbtype = sqlReplaceParam.getDbtype();
            if (StringUtils.isBlank(dbtype)) {
                dbtype = SqlCache.getDbType();
            }
            String targetDbType = sqlReplaceParam.getTargetDbType();
            if (StringUtils.isBlank(targetDbType)) {
                targetDbType = SqlCache.getTargetDbType();
            }
            String fieldName = sqlReplaceParam.getBrowserFieldName();
            //给ygd用
            if (sqlReplaceParam.getFirstQueryField() == true) {
                sqlResultInfo.put("firstQueryField", "true");
            }

            //给ygd用
            if (sqlReplaceParam.isDataWarehouse() == true) {
                sqlResultInfo.put("isDataWarehouse", "true");
            }
            //给ygd用
            if (sqlReplaceParam.isCubeGetOrderAndContCondition() == true) {
                sqlResultInfo.put("cubeGetOrderAndContCondition", "true");
            }

            sqlResultInfo.put("connBeanWorkFlow", new HashSet<>());
            sqlResultInfo.put("connBeanDataModel", new HashSet<>());


            if (sqlReplaceParam.isDataWarehouse() == true) {
                sqlResultInfo.put("connBean", SqlCache.getDataConnBeanDataModel("weaver-workflow-core-service"));
            }else {
                sqlResultInfo.put("connBean", SqlCache.getDataConnBeanWorkFlow("weaver-workflow-core-service"));
            }

            Map<String, Object> otherMap = sqlReplaceParam.getOtherMap();
            if (otherMap   == null) {

                otherMap=new HashMap<>();


            }


            sqlResultInfo.put("otherMap", otherMap);

            if (fieldName != null) {
                sqlResultInfo.put("fieldName", fieldName);
            }
            if (sql.equalsIgnoreCase("clearCache") || sql.equalsIgnoreCase("clear")) {
                SqlCache.clearCache();
            }

            //Map<String,Object> resultMap=new HashMap<>();
            sqlResultInfo.put("targetCode", sql);
            sqlResultInfo.put("sourceCode", sql);
            queue.clear();
            SqlCache.initCache();
            //0 不是正确的sql  1.存在部分数据未替换  2.完全替换 3 解析sql 过程中报错请联系武佳豪  4 sql中存在表或字段不是oa标准表。
            sqlResultInfo.put("status", "0");

            sqlResultInfo.put("sourceDbType", dbtype);
            sqlResultInfo.put("targetDbType", targetDbType);





            if (sqlReplaceParam.isSelectItem()){
                try {
                    SqlBean sqlBean = new SqlBean(sql);
                    SQLStatement sqlStatement = SQLUtils.parseStatements(sqlBean.getSqlNew(), dbtype).get(0);
                    SqlEnumVisitor.getVisitors(dbtype).getSelectList(sqlStatement);
                }catch (Exception e){


                }
            }






            //如果是外部数据源，并且需要获取首列
            if (sqlReplaceParam.getDatasetId() != null
                    && !SqlCache.isSystemData(sqlReplaceParam.getDatasetId())
                    && sqlReplaceParam.getFirstQueryField()
            ) {
                try {
                    SqlBean sqlBean = new SqlBean(sql);

                    sqlResultInfo.put("status", "2");
                    SQLStatement sqlStatement = SQLUtils.parseStatements(sqlBean.getSqlNew(), dbtype).get(0);
                    SqlEnumVisitor.getVisitors(dbtype).addFirstQueryField(sqlStatement);
                    sql = SQLUtils.toSQLString(sqlStatement, getJDBC(dbtype));
                    sql = sqlBean.getResultSql(sql);
                    sqlResultInfo.put("targetCode", sql);
                    sqlResultInfo.put("connBean", SqlCache.getShuJuYuanBeanByPointId(sqlReplaceParam.getDatasetId(), sqlReplaceParam.isDataWarehouse()));

                    return new HashMap<>(sqlResultInfo);
                } catch (Exception e) {
                    logger.error("sql",e);
                    sqlResultInfo.put("targetCode", sqlResultInfo.get("sourceCode"));
                    sqlResultInfo.put("connBean", SqlCache.getShuJuYuanBeanByPointId(sqlReplaceParam.getDatasetId(), sqlReplaceParam.isDataWarehouse()));

                    return new HashMap<>(sqlResultInfo);
                }
            }

            if (!SqlCache.isSystemData(sqlReplaceParam.getDatasetId())) {


                sqlResultInfo.put("connBean", SqlCache.getShuJuYuanBeanByPointId(sqlReplaceParam.getDatasetId(), sqlReplaceParam.isDataWarehouse()));

                sqlResultInfo.put("status", "2");
                return new HashMap<>(sqlResultInfo);
            }

            //正则判断不是一sql,走特殊替换
            if (PATTERN.matcher(sql.trim()).matches()) {
                noSQLReplace(sql);
                return new HashMap<>(sqlResultInfo);
            }

            String sourceCode = String.valueOf(sqlResultInfo.get("sourceCode"));
            SqlBean sqlBean = new SqlBean(sql);
            final String sqlNew_tmp = sqlBean.getSqlNew();
            // 通过这个方法将所有的占位符先用雪花id替换，记录下雪花id与占位符的对应关系
            // 拿到没有占位符的sql
            String sqlNew = null;
            try {
                //进行sql替换
                String replacedSql = getNewSql(sqlNew_tmp, dbtype, targetDbType);

                //sql跨库转换
                //replacedSql = SqlCache.convertSQL(replacedSql);
                //还原占位符
                replacedSql = sqlBean.getResultSql(replacedSql);

                sourceCode = String.valueOf(sqlResultInfo.get("sourceCode"));
                //还原占位符
                sourceCode = sqlBean.getResultSql(sourceCode);

                sqlNew = replacedSql;
            } catch (Exception e) {
                ///logger.error(" wjhsql ={}  ",sql);
                logger.error(" replaceSql  ",e);
                //e.printStackTrace();
                String targetCode = (String) sqlResultInfo.get("targetCode");
                if (e instanceof ParserException) {
                    //走一下sql片段替换逻辑
                    SqlCustomParser sqlParser = new SqlCustomParser();
                    List<String> list = new ArrayList<>();
                    list.add(targetCode);
                    Map<String, String> transMap = new HashMap<>();
                    list = sqlParser.convertSql(list, transMap);
                    targetCode = list.get(0);
                    sqlResultInfo.put("status", "0");
                } else {
                    sqlResultInfo.put("status", "3");
                }
                //sql跨库转换
                targetCode = SqlCache.convertSQL(targetCode);

                sqlResultInfo.put("targetCode", sqlBean.getResultSql(targetCode));
                sqlResultInfo.put("sourceCode", sqlBean.getResultSql(sourceCode));
                errorList.add(sql);
                buildTransInfo();
                return new HashMap<>(sqlResultInfo);
            }
            sqlResultInfo.put("targetCode", sqlNew);
            sqlResultInfo.put("sourceCode", sourceCode);


            //存为未完全替换
            if (sqlResultInfo.containsKey("transErrorMsg")) {
                //0 解析报错  1.存在部分数据为替换  2.完全替换
                sqlResultInfo.put("status", "1");
            } else {
                sqlResultInfo.put("status", "2");
            }

            if (sqlResultInfo.containsKey("customTable")
                    || sqlResultInfo.containsKey("customField")) {
                sqlResultInfo.put("status", "4");
            }

            buildTransInfo();
        } catch (Throwable e) {
            logger.error("wjhsql{}", e);
        }
        return new HashMap<>(sqlResultInfo);
    }


    public static void buildTransInfo() {
        String transMsg = "";

        //判断是否存在不是oa中的表
        if (sqlResultInfo.containsKey("customTable") || sqlResultInfo.containsKey("customField")) {
            transMsg += "存在表或字段不是oa中标准表: " + "\n";
            HashSet hashSet_Table = (HashSet) sqlResultInfo.get("customTable");
            HashSet hashSet_field = (HashSet) sqlResultInfo.get("customField");

            if (hashSet_Table != null) {
                for (Object msg : hashSet_Table) {
                    transMsg += msg + "\n";
                }
            }
            if (hashSet_field != null) {
                for (Object msg : hashSet_field) {
                    transMsg += msg + "\n";
                }
            }

        }


        if (sqlResultInfo.containsKey("transErrorMsg")) {
            HashSet hashSet = (HashSet) sqlResultInfo.get("transErrorMsg");
            if (!transMsg.equals("")) {
                transMsg += "\n";
            }
            transMsg += "存在表或字段没有转换: " + "\n";
            for (Object msg : hashSet) {
                transMsg += msg + "\n";
            }
        }

        if (sqlResultInfo.containsKey("transInfoMsg")) {
            HashSet hashSet = (HashSet) sqlResultInfo.get("transInfoMsg");
            if (!transMsg.equals("")) {
                transMsg += "\n";
            }
            transMsg += "转换信息打印: " + "\n";
            for (Object msg : hashSet) {
                transMsg += msg + "\n";
            }
        }
        sqlResultInfo.put("transMsg", transMsg);



        String defaultService = "weaver-workflow-core-service";
        /**
         * 获取默认数据源
         */
        if (sqlResultInfo.containsKey("isDataWarehouse")) {
            HashSet<DataConnBeanChild> hashSet = (HashSet<DataConnBeanChild>) sqlResultInfo.get("connBeanDataModel");
            if (hashSet.size() > 0) {
                DataConnBeanChild dataConnBeanDataModel = SqlCache.getDataConnBeanDataModel(defaultService);
                //存在流程，优先流程
                if (hashSet.contains(dataConnBeanDataModel)) {
                    sqlResultInfo.put("connBean", dataConnBeanDataModel);
                } else {
                    Iterator<DataConnBeanChild> iterator = hashSet.iterator();
                    while (iterator.hasNext()) {
                        DataConnBeanChild connBean = iterator.next();
                        if (!"weaver-basic-web-service".equalsIgnoreCase(connBean.getConn_name())
                                || !"weaver-hrm-service".equalsIgnoreCase(connBean.getConn_name())) {
                            sqlResultInfo.put("connBean", connBean);
                        }
                    }
                }
            } else {
                sqlResultInfo.put("connBean", SqlCache.getDataConnBeanDataModel(defaultService));
            }
        } else {
            HashSet<DataConnBeanChild> hashSet = (HashSet<DataConnBeanChild>) sqlResultInfo.get("connBeanWorkFlow");
            if (hashSet.size() > 0) {
                DataConnBeanChild dataConnBeanDataModel = SqlCache.getDataConnBeanWorkFlow(defaultService);
                //存在流程，优先流程
                if (hashSet.contains(dataConnBeanDataModel)) {
                    sqlResultInfo.put("connBean", dataConnBeanDataModel);
                } else {
                    Iterator<DataConnBeanChild> iterator = hashSet.iterator();
                    while (iterator.hasNext()) {
                        DataConnBeanChild connBean = iterator.next();
                        if (!"weaver-basic-web-service".equalsIgnoreCase(connBean.getConn_name())
                                || !"weaver-hrm-service".equalsIgnoreCase(connBean.getConn_name())) {
                            sqlResultInfo.put("connBean", connBean);
                        }
                    }
                }

            } else {
                sqlResultInfo.put("connBean", SqlCache.getDataConnBeanWorkFlow(defaultService));
            }
        }
    }

    /**
     * 根据表名获取e10对应的表名
     *
     * @param tableName
     * @return
     */


    /**
     * 解析本库sql，外部数据源的无需解析sql，提取出来涉及到的表和字段
     *
     * @param sql
     * @return
     */
    private static String getNewSql(String sql, String dbType, String targetDBType) {


        String new_sql = "";
        //sql = sql.replace("\\n", " ").replace("\\t", " ").replace("\n", " ").replace("\t", " ");
        List<SQLStatement> sqlStatements = SQLUtils.parseStatements(sql, dbType);
        //获取格式化的sql
        sqlResultInfo.put("sourceCode", SQLUtils.toSQLString(sqlStatements, getJDBC(dbType)));
        sqlResultInfo.put("targetCode", SQLUtils.toSQLString(sqlStatements, getJDBC(dbType)));

        for (SQLStatement sqlStatement : sqlStatements) {
            SqlEnumVisitor.getVisitors(dbType).process(sqlStatement);
        }



        //跨库转换可能需要异常。
        try {
            new_sql = SQLUtils.toSQLString(sqlStatements, getJDBC(targetDBType));
        } catch (Exception e) {
            logger.error(" replaceSql  ",e);
            //如果原数据源无法正确输出，使用tardbtype输出
            new_sql = SQLUtils.toSQLString(sqlStatements, getJDBC(dbType));
        }


        //sql跨库转换
         new_sql = SqlCache.convertSQL(new_sql);


        String resultDbtype=targetDBType;
        //判断用原始数据库是否能正确解析
        try {
            SQLUtils.parseStatements(new_sql, targetDBType);
        } catch (Exception e) {
            logger.error(" replaceSql  ",e);
            resultDbtype = dbType;
        }


        try {
            //重新生成sql结构树，处理数据库保留字段
            List<SQLStatement> sqlStatements_tmp = SQLUtils.parseStatements(new_sql, resultDbtype);
            for (SQLStatement sqlStatement : sqlStatements_tmp) {
                SqlEnumVisitor.getVisitors(resultDbtype).processKeywords(sqlStatement);
            }


            try {
                new_sql = SQLUtils.toSQLString(sqlStatements_tmp, getJDBC(targetDBType));
            } catch (Exception e) {
                logger.error(" replaceSql  {}", e);
                //如果原数据源无法正确输出，使用tardbtype输出
                new_sql = SQLUtils.toSQLString(sqlStatements_tmp, getJDBC(dbType));
            }

        } catch (Exception e) {

            logger.error(" replaceSql  {}",e);

        }
        return new_sql;

    }

    public static com.alibaba.druid.DbType getJDBC(String dbType) {
        com.alibaba.druid.DbType jdbcConstants = JdbcConstants.MYSQL;
        if (dbType.indexOf("mysql") > -1) {
            jdbcConstants = JdbcConstants.MYSQL;
        } else if (dbType.indexOf("oracle") > -1) {
            jdbcConstants = JdbcConstants.ORACLE;
        } else if (dbType.indexOf("postgresql") > -1) {
            jdbcConstants = JdbcConstants.POSTGRESQL;
        } else if (dbType.indexOf("sqlserver") > -1) {
            jdbcConstants = JdbcConstants.SQL_SERVER;
        }
        return jdbcConstants;


    }







    public static void noSQLReplace(String sql) {
        Matcher matcher = PATTERN.matcher(sql.trim());
        if (matcher.matches()) {
            String tablename = matcher.group(1);
            String fieldname = matcher.group(2);
            if (fieldname==null){
                if (FieldMappingCache.tablefiledMappingMap.containsKey(tablename)){
                    sqlResultInfo.put("targetCode", FieldMappingCache.replaceTable(tablename));
                    TransUtil.addTransMsg("将表"+tablename+"替换成"+FieldMappingCache.replaceTable(tablename));
                    buildTransInfo();
                    return;
                }
                sqlResultInfo.put("transMsg", "E10未找到该表替换关系");
            }else {
                if (FieldMappingCache.tablefiledMappingMap.containsKey(tablename)) {

                    FieldMappingCache.tablefiledMappingMap.get(tablename).getMigrateTableMappingBeanDetails().stream().filter(x->fieldname.equalsIgnoreCase(x.getE9TableField())).findFirst().ifPresent(new Consumer<MigrateTableMappingBeanDetail>() {
                        @Override
                        public void accept(MigrateTableMappingBeanDetail migrateTableMappingBeanDetail) {
                            sqlResultInfo.put("targetCode", migrateTableMappingBeanDetail.getE10TableName() + "." + migrateTableMappingBeanDetail.getE10TableField());
                            TransUtil.addTransMsg("将表 "+tablename+"替换成"+migrateTableMappingBeanDetail.getE10TableName()+",将列 "+fieldname+"替换成"+migrateTableMappingBeanDetail.getE10TableField()+"。");
                            buildTransInfo();
                        }
                    });

/*                    List<MigrateTableMappingBean> list = FieldMappingCache.tablefiledMappingMap.get(tablename);
                    for (MigrateTableMappingBean migrateTableMappingBean : list) {
                        if (migrateTableMappingBean.getE9TableField().equalsIgnoreCase(fieldname)) {
                            sqlResultInfo.put("targetCode", migrateTableMappingBean.getE10TableName() + "." + migrateTableMappingBean.getE10TableField());
                            TransUtil.addTransMsg("将表 "+tablename+"替换成"+migrateTableMappingBean.getE10TableName()+",将列 "+fieldname+"替换成"+migrateTableMappingBean.getE10TableField()+"。");
                            buildTransInfo();
                            return;
                        }
                    }*/



                }
                sqlResultInfo.put("transMsg", "E10对应表中未找到该字段替换关系");
            }
        }
    }


    /**
     * 判断是否是 表明 或者 表明.列表
     * @param input
     * @return
     */
    private static final Pattern PATTERN = Pattern.compile("^([a-zA-Z_][a-zA-Z0-9_]*)(?:\\.([a-zA-Z_][a-zA-Z0-9_]*))?$");









}





