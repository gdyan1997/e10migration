package com.weaver.version.upgrade.util.sql.visitor.oracle;

import com.alibaba.druid.sql.ast.expr.SQLBetweenExpr;
import com.alibaba.druid.sql.ast.expr.SQLIdentifierExpr;
import com.alibaba.druid.sql.ast.expr.SQLPropertyExpr;
import com.alibaba.druid.sql.ast.statement.SQLCreateViewStatement;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleSelectTableReference;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitorAdapter;
import com.alibaba.druid.sql.visitor.SQLASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.version.upgrade.util.sql.visitor.mysql.DatabaseFieldMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.mysql.TableMysqlVisitor;

/**
 * @author wujiahao
 * @date 2024/6/3 下午5:41
 * sql保留字段处理
 */
public class DatabaseFieldOracleVisitor extends OracleASTVisitorAdapter {
    @Override
    public boolean visit(SQLPropertyExpr expr) {
        return new DatabaseFieldMysqlVisitor().visit(expr);

    }
    @Override
    public boolean visit(SQLIdentifierExpr expr) {
        return new DatabaseFieldMysqlVisitor().visit(expr);

    }

    /**
     * 重写表名字的拦截器
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(OracleSelectTableReference x) {
        return new DatabaseFieldMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLExprTableSource x) {
        return new DatabaseFieldMysqlVisitor().visit(x);
    }
    public boolean visit(SQLCreateViewStatement x) {
        return new DatabaseFieldMysqlVisitor().visit(x);
    }

}
