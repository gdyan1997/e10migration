package com.weaver.version.upgrade.util.sql.bean;

import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;

public class TableBeanHasAlias {

    /**
     * 别名
     */
    private String tableName;
    private String tableAlias;
    private TableBean tableBean;

    private SQLExprTableSource sqlExprTableSource;

    public TableBeanHasAlias() {
    }

    public TableBeanHasAlias(String tableName, String tableAlias) {
        this.tableName = tableName;
        this.tableAlias = tableAlias;
    }

    public TableBeanHasAlias(String tableAlias, TableBean tableBean) {
        this.tableAlias = tableAlias;
        this.tableBean = tableBean;
    }

    public SQLExprTableSource getSqlExprTableSource() {
        return sqlExprTableSource;
    }

    public void setSqlExprTableSource(SQLExprTableSource sqlExprTableSource) {
        this.sqlExprTableSource = sqlExprTableSource;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public TableBean getTableBean() {
        return tableBean;
    }

    public void setTableBean(TableBean tableBean) {
        this.tableBean = tableBean;
    }
}
