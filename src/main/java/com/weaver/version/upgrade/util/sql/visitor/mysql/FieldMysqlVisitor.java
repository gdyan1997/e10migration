package com.weaver.version.upgrade.util.sql.visitor.mysql;

import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.ast.statement.MySqlInsertStatement;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.bean.TableBeanHasAlias;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.util.TransUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.workflowTableMap;

/**
 * @author wujiahao
 * @date 2023/12/14 14:57
 */
public class FieldMysqlVisitor extends MySqlASTVisitorAdapter {

    @Override
    public boolean visit(SQLIdentifierExpr x) {
        TransUtil.transSQLExpr(x,x);
        return super.visit(x);
    }


    @Override
    public boolean visit(SQLPropertyExpr x) {

        TransUtil.transSQLExpr(x,x);
        return super.visit(x);
    }
    @Override
    public boolean visit(SQLAllColumnExpr x) {
        TransUtil.transSQLExpr(x,x);
        return super.visit(x);
    }




    /**
     * 重写查询字段的拦截器,没有别名。的那种字段，有别名。的用下面的口子获取
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLSelectItem x) {


/*
        // 这种就是单纯的字段类型
        if (x.getExpr() instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr expr = (SQLIdentifierExpr) x.getExpr();
            String fieldName = expr.getName().toLowerCase();
            String oldFieldName = expr.getName();
            //替换字段名称
            if (x.getParent() instanceof SQLSelectQueryBlock) {
                List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x.getParent());

                String newFieldName = FieldMappingCache.replaceTableFiled(tableBeanHasAliases, fieldName, true);
                //处理时间切割
                //newFieldName=TransUtil.dateSplit(tableBeanHasAliases,fieldName,newFieldName);
                expr.setName(newFieldName);
                //成功替换了字段，才添加别名
                if (x.getAlias() == null && !newFieldName.equalsIgnoreCase(fieldName)) {
                    x.setAlias(oldFieldName);
                }
                //处理case when
                TransUtil.exprToCaseWhen(x,tableBeanHasAliases,oldFieldName);
            }

            //带别名或表名称。
        } else if (x.getExpr() instanceof SQLPropertyExpr) {

            SQLPropertyExpr expr = (SQLPropertyExpr) x.getExpr();
            String owner = expr.getOwnernName().toLowerCase();
            String oldFieldName = expr.getName().toLowerCase();
            String oldFieldName_tmp = expr.getName();

            if (x.getParent() instanceof SQLSelectQueryBlock) {
                List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x.getParent());

                List<TableBeanHasAlias> collect = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                String newFieldName = FieldMappingCache.replaceTableFiled(collect, oldFieldName, false);
                //处理时间切割
               // newFieldName=TransUtil.dateSplit(tableBeanHasAliases,oldFieldName,newFieldName);
                expr.setName(newFieldName);
                //成功替换了字段，才添加别名
                if (x.getAlias() == null && !newFieldName.equalsIgnoreCase(oldFieldName)) {
                    x.setAlias(oldFieldName_tmp);
                }
                if(newFieldName.startsWith("'E10无此字段")){
                    SQLIdentifierExpr sqlIdentifierExpr = new SQLIdentifierExpr(newFieldName);
                    x.setExpr(sqlIdentifierExpr);
                }
                //处理case when
                TransUtil.exprToCaseWhen(x,tableBeanHasAliases,oldFieldName_tmp);
            }
            //方法 CONCAT(a.lastname, '-', b.departmentname,lastname) name
        } else if (x.getExpr() instanceof SQLMethodInvokeExpr) {

        } else if (x.getExpr() instanceof SQLAllColumnExpr) {
            TransUtil.addTransErrorMsg("sql 查询字段中存在 * 请手动确认需要的字段");
        }else if (x.getExpr() instanceof SQLBinaryOpExpr){
            //this.visit(x);
        }else if (x.getExpr() instanceof SQLCharExpr){
            //this.visit(x);
        }
*/

        return super.visit(x);
    }


    //流程主表更新特殊处理  处理request

    public void visitWf(SQLBinaryOpExpr x) {
        HashSet<String> hashSet = workflowTableMap.get(TransUtil.getTenantKey());

        //判断是否是更新sql
        if (!TransUtil.isUpdateSql(x)) {
            return;
        }

        boolean isReplace = false;
        boolean isLeft = false;

        if (x.getOperator() instanceof SQLBinaryOperator) {
            SQLExpr left = x.getLeft();
            SQLExpr right = x.getRight();
            List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x.getParent());

            if (left instanceof SQLPropertyExpr) {
                SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) left;
                String owner = sqlPropertyExpr.getOwnernName().toLowerCase();
                tableBeanHasAliases = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                String oldFieldName = sqlPropertyExpr.getName().toLowerCase();
                if (oldFieldName.equalsIgnoreCase("requestid")) {
                    String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                    //是流程主表需要替换
                    if (hashSet.contains(fieldInTable + "|wf|")) {
                        sqlPropertyExpr.setName("form_data_id");
                        isReplace = true;
                        isLeft = true;
                    }
                }
            } else if (left instanceof SQLIdentifierExpr) {
                SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) left;
                String oldFieldName = sqlIdentifierExpr.getName().toLowerCase();
                if (oldFieldName.equalsIgnoreCase("requestid")) {
                    String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                    //是流程主表需要替换
                    if (hashSet.contains(fieldInTable + "|wf|")) {
                        sqlIdentifierExpr.setName("form_data_id");
                        isReplace = true;
                        isLeft = true;
                    }
                }
            }


            if (right instanceof SQLPropertyExpr) {
                SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) right;
                String owner = sqlPropertyExpr.getOwnernName().toLowerCase();
                String oldFieldName = sqlPropertyExpr.getName().toLowerCase();
                tableBeanHasAliases = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());

                if (oldFieldName.equalsIgnoreCase("requestid")) {
                    String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                    //是流程主表需要替换
                    if (hashSet.contains(fieldInTable + "|wf|")) {
                        sqlPropertyExpr.setName("form_data_id");
                        isReplace = true;
                        isLeft = false;
                    }
                }
            } else if (right instanceof SQLIdentifierExpr) {
                SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) right;
                String oldFieldName = sqlIdentifierExpr.getName().toLowerCase();
                if (oldFieldName.equalsIgnoreCase("requestid")) {
                    String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                    //是流程主表需要替换
                    if (hashSet.contains(fieldInTable + "|wf|")) {
                        sqlIdentifierExpr.setName("form_data_id");
                        isLeft = false;
                        isReplace = true;
                    }
                }
            }


            if (isReplace) {
                //记录原始操作符好
                SQLBinaryOperator operator = x.getOperator();
                x.setOperator(SQLBinaryOperator.Equality);
                // 构建子查询的 Where 子句
                SQLBinaryOpExpr sqlBinaryOpExpr = null;
                SQLIdentifierExpr column = new SQLIdentifierExpr("requestid");
                if (isLeft) {
                    sqlBinaryOpExpr = new SQLBinaryOpExpr(column, x.getRight(), operator);
                } else {
                    sqlBinaryOpExpr = new SQLBinaryOpExpr(x.getLeft(), column, operator);
                }
                SQLSelect sqlSelect = TransUtil.buildFormDataSelect(sqlBinaryOpExpr);
                SQLQueryExpr sqlQueryExpr = new SQLQueryExpr();
                sqlQueryExpr.setSubQuery(sqlSelect);
                if (isLeft) {
                    x.setRight(sqlQueryExpr);
                } else {
                    x.setLeft(sqlQueryExpr);
                }

                //将最外层修改成 in
                SQLInListExpr sqlInListExpr = new SQLInListExpr();
                sqlInListExpr.setNot(false);
                sqlInListExpr.setExpr(isLeft?x.getLeft():x.getRight());
                List<SQLExpr> list = new ArrayList<>();
                list.add(isLeft?x.getRight():x.getLeft());
                sqlInListExpr.setTargetList(list);
                TransUtil.replaceSQLExpr(x,sqlInListExpr);
            }


        }


    }


    /**
     * 条件查询列表替换。
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLBinaryOpExpr x) {
/*
        if (x.getOperator() instanceof SQLBinaryOperator) {
            SQLBinaryOperator sqlBinaryOperator = x.getOperator();
            List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x.getParent());


            //==
            for (int i = 0; i < 2; i++) {
                SQLExpr left = null;
                if (i == 0) {
                    left = x.getLeft();
                } else {
                    left = x.getRight();
                }
                if (left instanceof SQLPropertyExpr) {
                    SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) left;
                    String owner = sqlPropertyExpr.getOwnernName().toLowerCase();
                    String oldFieldName = sqlPropertyExpr.getName().toLowerCase();
                    List<TableBeanHasAlias> collect = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                    String newFieldName = FieldMappingCache.replaceTableFiled(collect, oldFieldName, false);
                    sqlPropertyExpr.setName(newFieldName);
                } else if (left instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) left;
                    String oldFieldName = sqlIdentifierExpr.getName().toLowerCase();
                    String newFieldName = FieldMappingCache.replaceTableFiled(tableBeanHasAliases, oldFieldName, true);
                    sqlIdentifierExpr.setName(newFieldName);
                    //是数字参数
                } else if (left instanceof SQLIntegerExpr) {

                } else if (left instanceof SQLCharExpr) {

                } else if (left instanceof SQLMethodInvokeExpr) {

                } else {



                }

            }

        }
*/

        visitWf(x);


        return super.visit(x);

    }


    public void visitWf(SQLInListExpr x) {

        if (!TransUtil.isUpdateSql(x)) {
            return;
        }
        List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x.getParent());
        HashSet<String> hashSet = workflowTableMap.get(TransUtil.getTenantKey());

        SQLExpr column = x.getExpr();
        List<SQLExpr> targetList = x.getTargetList();

        //更新语句，判断
        if (column instanceof SQLPropertyExpr) {
            SQLPropertyExpr expr = (SQLPropertyExpr) column;
            String owner = expr.getOwnernName().toLowerCase();
            String oldFieldName = expr.getName().toLowerCase();
            tableBeanHasAliases = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> tableBeanHasAlias.getTableName().equalsIgnoreCase(owner) || tableBeanHasAlias.getTableAlias().equalsIgnoreCase(owner)
            ).collect(Collectors.toList());
            if (oldFieldName.equalsIgnoreCase("requestid")) {
                String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                //是流程主表需要替换
                if (hashSet.contains(fieldInTable + "|wf|")) {
                    expr.setName("form_data_id");
                    SQLInSubQueryExpr sqlInSubQueryExpr = new SQLInSubQueryExpr();
                    sqlInSubQueryExpr.setExpr(expr);
                    sqlInSubQueryExpr.setNot(false);
                    SQLInListExpr sqlInListExpr = new SQLInListExpr();
                    sqlInListExpr.setNot(x.isNot());
                    sqlInListExpr.setExpr(new SQLIdentifierExpr("requestid"));
                    sqlInListExpr.setTargetList(targetList);
                    SQLSelect sqlSelect = TransUtil.buildFormDataSelect(sqlInListExpr);
                    sqlInSubQueryExpr.setSubQuery(sqlSelect);
                    TransUtil.replaceSQLExpr(x, sqlInSubQueryExpr);
                }

            }
        } else if (column instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr expr = (SQLIdentifierExpr) column;
            String oldFieldName = expr.getName();
            if (oldFieldName.equalsIgnoreCase("requestid")) {
                String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                //是流程主表需要替换
                if (hashSet.contains(fieldInTable + "|wf|")) {
                    expr.setName("form_data_id");
                    SQLInSubQueryExpr sqlInSubQueryExpr = new SQLInSubQueryExpr();
                    sqlInSubQueryExpr.setExpr(expr);
                    sqlInSubQueryExpr.setNot(false);
                    SQLInListExpr sqlInListExpr = new SQLInListExpr();
                    sqlInListExpr.setNot(x.isNot());
                    sqlInListExpr.setExpr(new SQLIdentifierExpr("requestid"));
                    sqlInListExpr.setTargetList(targetList);
                    SQLSelect sqlSelect = TransUtil.buildFormDataSelect(sqlInListExpr);
                    sqlInSubQueryExpr.setSubQuery(sqlSelect);
                    TransUtil.replaceSQLExpr(x, sqlInSubQueryExpr);
                }
            }
        }
    }

    /**
     * 条件查询列表替换。   in  notin   like   not like
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLInListExpr x) {
 /*       SQLExpr column = x.getExpr();

        TransUtil.transSQLExpr(column,x);*/
        visitWf(x);

        return super.visit(x);

    }

    /**
     * in  () 右侧是一个表达式
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLInSubQueryExpr x) {
 /*       SQLExpr column = x.getExpr();
        TransUtil.transSQLExpr(column,x);
*/
        visitWf(x);
        return super.visit(x);
    }


    public boolean visitWf(SQLInSubQueryExpr x) {
        if (!TransUtil.isUpdateSql(x)) {
            return true;
        }

        SQLExpr column = x.getExpr();
        List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x.getParent());
        HashSet<String> hashSet = workflowTableMap.get(TransUtil.getTenantKey());

        //更新语句，判断
        if (column instanceof SQLPropertyExpr) {
            SQLPropertyExpr expr = (SQLPropertyExpr) column;
            String owner = expr.getOwnernName().toLowerCase();
            String oldFieldName = expr.getName().toLowerCase();
            tableBeanHasAliases = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> tableBeanHasAlias.getTableName().equalsIgnoreCase(owner) || tableBeanHasAlias.getTableAlias().equalsIgnoreCase(owner)
            ).collect(Collectors.toList());
            if (oldFieldName.equalsIgnoreCase("requestid")) {
                String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                //是流程主表需要替换
                if (hashSet.contains(fieldInTable + "|wf|")) {
                    expr.setName("form_data_id");
                    SQLInSubQueryExpr sqlInSubQueryExpr = new SQLInSubQueryExpr();
                    sqlInSubQueryExpr.setExpr(expr);
                    sqlInSubQueryExpr.setNot(false);
                    SQLInSubQueryExpr sqlInSubQueryExpr_tmp = new SQLInSubQueryExpr();
                    sqlInSubQueryExpr_tmp.setNot(x.isNot());
                    sqlInSubQueryExpr_tmp.setExpr(new SQLIdentifierExpr("requestid"));
                    sqlInSubQueryExpr_tmp.setSubQuery(x.getSubQuery());
                    SQLSelect sqlSelect = TransUtil.buildFormDataSelect(sqlInSubQueryExpr_tmp);
                    sqlInSubQueryExpr.setSubQuery(sqlSelect);
                    TransUtil.replaceSQLExpr(x, sqlInSubQueryExpr);
                }

            }
        } else if (column instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr expr = (SQLIdentifierExpr) column;
            String oldFieldName = expr.getName();
            if (oldFieldName.equalsIgnoreCase("requestid")) {
                String fieldInTable = FieldMappingCache.getFieldInTable(tableBeanHasAliases, oldFieldName, false);
                //是流程主表需要替换
                if (hashSet.contains(fieldInTable + "|wf|")) {
                    expr.setName("form_data_id");
                    SQLInSubQueryExpr sqlInSubQueryExpr = new SQLInSubQueryExpr();
                    sqlInSubQueryExpr.setExpr(expr);
                    sqlInSubQueryExpr.setNot(false);
                    SQLInSubQueryExpr sqlInSubQueryExpr_tmp=new SQLInSubQueryExpr();
                    sqlInSubQueryExpr_tmp.setNot(x.isNot());
                    sqlInSubQueryExpr_tmp.setExpr(new SQLIdentifierExpr("requestid"));
                    sqlInSubQueryExpr_tmp.setSubQuery(x.getSubQuery());
                    SQLSelect sqlSelect = TransUtil.buildFormDataSelect(sqlInSubQueryExpr_tmp);
                    sqlInSubQueryExpr.setSubQuery(sqlSelect);
                    TransUtil.replaceSQLExpr(x, sqlInSubQueryExpr);
                }
            }
        }
        return super.visit(x);
    }




      /**
         * 重写查询字段的拦截器,没有别名。的那种字段，有别名。的用下面的口子获取
         *
         * @param x
         * @return
     */
    @Override
    public boolean visit(SQLUpdateSetItem x) {
/*        SQLExpr column = x.getColumn();
        TransUtil.transSQLExpr(column,x);*/
        return super.visit(x);
    }

    @Override
    public boolean visit(SQLCaseExpr item) {

/*
        if (item.containsAttribute("caseWhen")){
            return true;
        }
        item.getItems().forEach(item_tmp -> TransUtil.transSQLExpr(item_tmp.getValueExpr(), item));
        SQLExpr valueExpr = item.getValueExpr();
        SQLExpr elseExpr = item.getElseExpr();
        TransUtil.transSQLExpr(valueExpr,item);
        TransUtil.transSQLExpr(elseExpr,item);
        // 这种就是单纯的字段类型*/
        return super.visit(item);
    }



    /**
     * 替换mysql 插入列
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(MySqlInsertStatement x) {
        //x.getColumns().forEach(item -> TransUtil.transSQLExpr(item,x));
        return super.visit(x);
    }




    /**
     *
     * 所有数据库类型都会进去
     * @param x
     * @return  mysql
     */
    @Override
    public boolean visit(SQLCastExpr x) {
/*        SQLExpr expr = x.getExpr();
        TransUtil.transSQLExpr(expr,x);*/
        return super.visit(x);
    }


    @Override
    public boolean visit(SQLMethodInvokeExpr x) {
/*        String methodName = x.getMethodName();
        x.getArguments().forEach(argument -> TransUtil.transSQLExpr(argument,x));*/
        return super.visit(x);
    }


    /**
     * 重写字段的拦截器
     *
     * @param x
     * @return
     */

    /**
     * 重写字段的拦截器,目前好像不写别名的group ，上边的不处理,用下边的加强一下。
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLSelectGroupByClause x) {
/*        x.getItems().forEach(sqlExpr -> TransUtil.transSQLExpr(sqlExpr,x));
        //下边 sqlAggregateExpr  拦截器处理
        if (x.getHaving() instanceof SQLBinaryOpExpr) {

        }*/
        return super.visit(x);
    }


    /**
     * 重写查询字段的拦截器,没有别名。的那种字段，有别名。的用下面的口子获取
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLSelectOrderByItem x) {
        // 这种就是单纯的字段类型
/*
        if (x.getExpr() instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr expr = (SQLIdentifierExpr) x.getExpr();
            if (x.getParent() instanceof SQLOrderBy) {
                SQLOrderBy sqlOrderBy = (SQLOrderBy) x.getParent();
                if (sqlOrderBy.getParent() instanceof SQLSelectQueryBlock) {

                    String oldFieldName = expr.getName().toLowerCase();
                    List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(sqlOrderBy.getParent());

                    String newFieldName = FieldMappingCache.replaceTableFiled(tableBeanHasAliases, oldFieldName, true);
                    expr.setName(newFieldName);

                }
            }

        } else if (x.getExpr() instanceof SQLPropertyExpr) {
            SQLPropertyExpr expr = (SQLPropertyExpr) x.getExpr();
            String owner = expr.getOwnernName().toLowerCase();
            String oldFieldName = expr.getName().toLowerCase();

            if (x.getParent() instanceof SQLOrderBy) {
                SQLOrderBy sqlOrderBy = (SQLOrderBy) x.getParent();
                if (sqlOrderBy.getParent() instanceof SQLSelectQueryBlock) {
                    List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(sqlOrderBy.getParent());
                    List<TableBeanHasAlias> collect = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                    String newFieldName = FieldMappingCache.replaceTableFiled(collect, oldFieldName, false);
                    expr.setName(newFieldName);
                }
            }


        }
*/
        return super.visit(x);
    }


    /**
     * beetween
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLBetweenExpr x) {
/*        SQLExpr column = x.getTestExpr();
        SQLExpr beginExpr = x.getBeginExpr();
        SQLExpr endExpr = x.getEndExpr();
        TransUtil.transSQLExpr(column,x);
        TransUtil.transSQLExpr(beginExpr,x);
        TransUtil.transSQLExpr(endExpr,x);*/
        return super.visit(x);
    }

    /**
     * (聚合)函数
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLAggregateExpr x) {
        //x.getArguments().forEach(argument -> TransUtil.transSQLExpr(argument,x));
        return super.visit(x);
    }
}
