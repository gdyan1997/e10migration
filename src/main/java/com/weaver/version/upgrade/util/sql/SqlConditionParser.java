package com.weaver.version.upgrade.util.sql;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.expr.SQLBinaryOpExpr;
import com.alibaba.druid.sql.ast.statement.SQLSelectQueryBlock;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/10/8 下午5:28
 */
@Deprecated
public class SqlConditionParser {
    private static final Logger logger = LoggerFactory.getLogger(SqlConditionParser.class);

    public static SQLBinaryOpExpr getSQLCondition(String sql) {

        SQLBinaryOpExpr sqlBinaryOpExpr = null;

        try {


        // 使用 Druid 解析 SQL 语句0.
        List<SQLStatement> sqlStatements = SQLUtils.parseStatements(sql, SqlCache.getDbType());

        // 遍历解析出的 SQL 语句
        for (SQLStatement statement : sqlStatements) {
            if (statement instanceof SQLSelectStatement) {
                SQLSelectStatement selectStatement = (SQLSelectStatement) statement;
                // 获取 SELECT 语句中的 WHERE 子句
                SQLSelectQueryBlock queryBlock = (SQLSelectQueryBlock) selectStatement.getSelect().getQuery();
                SQLExpr where = queryBlock.getWhere();
                if (where == null) {
                    //System.out.println("该 SQL 没有 WHERE 子句。");
                    continue;
                }
                // 判断 WHERE 子句是否是 SQLBinaryOpExpr
                if (where != null ) {
                    SQLBinaryOpExpr binaryExpr = (SQLBinaryOpExpr) where;
                    // 将 WHERE 子句的二元表达式转换为多叉树
                    sqlBinaryOpExpr = convertToNaryTree(binaryExpr);
                    // 打印多叉树结构
                    //System.out.println("解析后的多叉树结构：");
                } else {
                    //System.out.println("无法解析 WHERE 子句：" + where);
                }
            }
        }
        }catch (Exception e ){
            logger.error("sql", e);
        }

        return sqlBinaryOpExpr;
    }
    public static void main(String[] args) {
        // 示例 SQL 语句
        String sql = "SELECT * FROM hrmre WHERE id =2 and id not in (213) and id like '213'";

        SQLBinaryOpExpr sqlCondition = SqlConditionParser.getSQLCondition(sql);
        if (sqlCondition == null) {
            //解析异常或者没有解析到条件

        } else if (sqlCondition instanceof TreeNode) {
            //有多个条件树

        } else if (sqlCondition instanceof SQLBinaryOpExpr) {
            //只有一个条件

        }
    }

    // 多叉树节点类
    static class TreeNode extends SQLBinaryOpExpr {
        List<Object> children;      // 子节点列表

        // 构造函数
        public TreeNode() {
            this.children = new ArrayList<>();
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "children=" + children +
                    '}';
        }


    }


    // 将 SQLBinaryOpExpr 转换为多叉树
    private static SQLBinaryOpExpr convertToNaryTree(SQLBinaryOpExpr expr) {
        if (!(expr.getRight() instanceof SQLBinaryOpExpr) && !(expr.getLeft() instanceof SQLBinaryOpExpr)) {
            return expr;
        }


        // 创建当前表达式的根节点
        TreeNode root = new TreeNode();
        root.setOperator(expr.getOperator());
        // 处理左子表达式
        if (expr.getLeft() instanceof SQLBinaryOpExpr) {
            root.children.add(convertToNaryTree((SQLBinaryOpExpr) expr.getLeft()));
        }else {
            root.children.add(expr.getLeft());
        }

        // 处理右子表达式
        if (expr.getRight() instanceof SQLBinaryOpExpr) {
            root.children.add(convertToNaryTree((SQLBinaryOpExpr) expr.getRight()));
        }else {
            root.children.add(expr.getRight());
        }


        return root;
    }
}
