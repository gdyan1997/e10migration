package com.weaver.version.upgrade.util.sql.visitor.sqlServer;

import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.sqlserver.visitor.SQLServerASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.TableMysqlVisitor;

/**
 * @author wujiahao
 * @date 2023/12/14 15:50
 */
public class TableSqlServerVisitor extends SQLServerASTVisitorAdapter {


    /**
     * 重写表名字的拦截器
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLExprTableSource x) {
        return new TableMysqlVisitor().visit(x);
    }


}
