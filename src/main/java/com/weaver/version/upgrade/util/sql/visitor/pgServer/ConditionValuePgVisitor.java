package com.weaver.version.upgrade.util.sql.visitor.pgServer;

import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.postgresql.ast.stmt.PGSelectQueryBlock;
import com.alibaba.druid.sql.dialect.postgresql.ast.stmt.PGSelectStatement;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.ConditionValueMysqlVisitor;

/**
 * @author wujiahao
 * @date 2023/12/18 10:16
 *   主要做条件中的值，替换成E10新id
 */
public class ConditionValuePgVisitor extends PGASTVisitorAdapter {


    @Override
    public boolean visit(PGSelectQueryBlock x) {
        return new ConditionValueMysqlVisitor().visitBase(x);
    }


    @Override
    public boolean visit(SQLBinaryOpExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }


    @Override
    public boolean visit(SQLExprTableSource x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLInListExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLWithSubqueryClause.Entry x) {

        return new ConditionValueMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLCreateViewStatement x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }



    @Override
    //pg比较特殊拦截得使用下边的
    public boolean visit(SQLSelectStatement x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }
    @Override
    //pg比较特殊拦截得使用这个
    public boolean visit(PGSelectStatement x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }


    @Override
    public boolean visit(SQLSelectItem x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLMethodInvokeExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    public boolean visit(SQLBetweenExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLCaseExpr x) {

        return new ConditionValueMysqlVisitor().visit(x);
    }
}
