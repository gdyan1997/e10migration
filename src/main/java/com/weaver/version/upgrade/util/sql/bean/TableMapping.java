package com.weaver.version.upgrade.util.sql.bean;

/**
 * @author wujiahao
 * @date 2023/12/18 11:10
 */
public class TableMapping {
    //来源表
    private String sourceTable;
    //来源字段
    private String sourcefield;


    private  String oldTableName;
    private  String tableName;

    private  String e10fieldName;


    private  String fieldName;

    private  String fieldType;

    public String getOldTableName() {
        return oldTableName;
    }

    public void setOldTableName(String oldTableName) {
        this.oldTableName = oldTableName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getE10fieldName() {
        return e10fieldName;
    }

    public void setE10fieldName(String e10fieldName) {
        this.e10fieldName = e10fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public void setSourceTable(String sourceTable) {
        this.sourceTable = sourceTable;
    }

    public String getSourcefield() {
        return sourcefield;
    }

    public void setSourcefield(String sourcefield) {
        this.sourcefield = sourcefield;
    }

    @Override
    public String toString() {
        return "TableMapping{" +
                "sourceTable='" + sourceTable + '\'' +
                ", sourcefield='" + sourcefield + '\'' +
                ", oldTableName='" + oldTableName + '\'' +
                ", tableName='" + tableName + '\'' +
                ", e10fieldName='" + e10fieldName + '\'' +
                ", fieldName='" + fieldName + '\'' +
                ", fieldType='" + fieldType + '\'' +
                '}';
    }
}
