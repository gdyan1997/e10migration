package com.weaver.version.upgrade.util.sql.visitor.mysql;

import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.ast.statement.MySqlSelectQueryBlock;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.bean.TableBeanHasAlias;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.version.upgrade.util.sql.util.TransUtil;

import java.util.*;
import java.util.stream.Collectors;

import static com.weaver.version.upgrade.util.sql.SQLUtil.methodUesCount;
import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;

/**
 * @author wujiahao
 * @date 2023/12/18 10:16
 * 主要做条件中的值，替换成E10新id
 */
public class ConditionValueMysqlVisitor extends MySqlASTVisitorAdapter {

    /**
     * 功能将内嵌查询
     * @param x
     * @return
     */
    public boolean visitBase(SQLSelectQueryBlock x) {

        //String sqlSelectSql=x.getParent().toString();
        List<SQLSelectItem> selectList = x.getSelectList();
        List<SQLSelectItem> collect_new = new ArrayList<>();
        boolean outerQuerySelectItem = TransUtil.isOuterQuerySelectItem(x);
        boolean isReplace = TransUtil.isReplace(x);
        if (!isReplace){
            return super.visit(x);
        }
        for (SQLSelectItem sqlSelectItem : selectList) {

            int size = collect_new.size();

            if (sqlSelectItem.getExpr() instanceof SQLPropertyExpr){
                SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlSelectItem.getExpr();
                String name = sqlPropertyExpr.getName();
                String getOwner = sqlPropertyExpr.getOwnernName();
                if ("*".equals(name)){
                    //查询外侧子查询使用了多少列
                    List<String> tableUseField = TransUtil.getTableUseField(sqlSelectItem.getParent());
                    tableUseField.stream().forEach(fieldName -> {
                        boolean isAdd=true;
                        for (SQLSelectItem selectItem : selectList) {
                            String alias = selectItem.getAlias();
                            //存在别名
                            if (alias==null){
                                if (selectItem.getExpr() instanceof SQLPropertyExpr){
                                    SQLPropertyExpr sqlPropertyExpr_tmp = (SQLPropertyExpr) selectItem.getExpr();
                                    String simpleName = sqlPropertyExpr_tmp.getSimpleName();
                                    if (fieldName.equalsIgnoreCase(simpleName)){
                                        isAdd=false;
                                        break;
                                    }
                                }else if (selectItem.getExpr() instanceof SQLIdentifierExpr){
                                    SQLIdentifierExpr sqlIdentifierExpr_tmp = (SQLIdentifierExpr) selectItem.getExpr();
                                    String simpleName = sqlIdentifierExpr_tmp.getSimpleName();
                                    if (fieldName.equalsIgnoreCase(simpleName)){
                                        isAdd=false;
                                        break;
                                    }
                                }
                            }else {
                                if (fieldName.equalsIgnoreCase(alias)){
                                    isAdd=false;
                                    break;
                                }
                            }

                        }
                        if (isAdd){
                            //String columnFromTable = TransUtil.getColumnFromTable(sqlSelectSql, fieldName);
                            //if (StringUtils.isNotBlank(columnFromTable)) {
                                SQLSelectItem selectItem = sqlSelectItem.clone();
                                selectItem.setExpr(new SQLPropertyExpr(getOwner,fieldName));
                                selectItem.setParent(sqlSelectItem.getParent());
                                collect_new.add(selectItem);
                            //}
                        }
                    });
                    //没有接出来，把原始的selecet * 添加进去
                    if (collect_new.size()==size){
                        collect_new.add(sqlSelectItem);
                    }
                }else {
                    collect_new.add(sqlSelectItem);
                }
            }else if (sqlSelectItem.getExpr() instanceof SQLAllColumnExpr){
                SQLAllColumnExpr sqlAllColumnExpr = (SQLAllColumnExpr) sqlSelectItem.getExpr();
                if(outerQuerySelectItem){
                    collect_new.add(sqlSelectItem);
                }else {
                    //查询外侧子查询使用了多少列
                    List<String> tableUseField = TransUtil.getTableUseField(sqlSelectItem.getParent());


                    tableUseField.stream().forEach(fieldName -> {
                        boolean isAdd=true;


                        for (SQLSelectItem selectItem : selectList) {
                            String alias = selectItem.getAlias();
                            //存在别名
                            if (alias==null){
                                if (selectItem.getExpr() instanceof SQLPropertyExpr){
                                    SQLPropertyExpr sqlPropertyExpr_tmp = (SQLPropertyExpr) selectItem.getExpr();
                                    String simpleName = sqlPropertyExpr_tmp.getSimpleName();
                                    if (fieldName.equalsIgnoreCase(simpleName)){
                                        isAdd=false;
                                        break;
                                    }
                                }else if (selectItem.getExpr() instanceof SQLIdentifierExpr){
                                    SQLIdentifierExpr sqlIdentifierExpr_tmp = (SQLIdentifierExpr) selectItem.getExpr();
                                    String simpleName = sqlIdentifierExpr_tmp.getSimpleName();
                                    if (fieldName.equalsIgnoreCase(simpleName)){
                                        isAdd=false;
                                        break;
                                    }
                                }
                            }else {
                                if (fieldName.equalsIgnoreCase(alias)){
                                    isAdd=false;
                                    break;
                                }
                            }

                        }
                        if (isAdd){
                            //String columnFromTable = TransUtil.getColumnFromTable(sqlSelectSql, fieldName);
                            //if (StringUtils.isNotBlank(columnFromTable)) {
                                SQLSelectItem selectItem = sqlSelectItem.clone();
                                selectItem.setExpr(new SQLIdentifierExpr(fieldName));
                                selectItem.setParent(sqlSelectItem.getParent());
                                collect_new.add(selectItem);
                           // }
                        }
                    });

                }
            }else {
                collect_new.add(sqlSelectItem);
            }

            //没有接出来，把原始的selecet * 添加进去
            if (collect_new.size()==size){
                collect_new.add(sqlSelectItem);
            }
        }

        selectList.clear();;
        selectList.addAll(collect_new);

        return super.visit(x);
    }
    @Override
    public boolean visit(MySqlSelectQueryBlock x) {
        return this.visitBase(x);
    }

    @Override
    public boolean visit(SQLSelectItem x) {
/*        //给jr用
        if (x.getAlias() != null) {
            List<String> aliasList = (List<String>) sqlResultInfo.get("aliasList");
            if (aliasList instanceof List) {
                aliasList.add(x.getAlias());
            }else {
                List<String> newAliasList = new ArrayList<>();
                newAliasList.add(x.getAlias());
                sqlResultInfo.put("aliasList", newAliasList);
            }
        }*/
        //给ygd使用
        //查询sql查询的第一个字段，如果有别名，获取别名，如果没有设置一个别名。
        TransUtil.addFirstQueryField(x);
        return true;
    }



    @Override
    public boolean visit(SQLBinaryOpExpr x) {

        if (x.getOperator() instanceof SQLBinaryOperator) {
            SQLBinaryOperator sqlBinaryOperator = x.getOperator();
            if (TransUtil.isComparisonOperator(sqlBinaryOperator.getName())) {
                List<TableBeanHasAlias> fromTableList = TransUtil.getFromTableList(x.getParent());


                //解决E7 环境流程条件，只处理sqlserver和oracle数据库
                try {
                    if (sqlBinaryOperator.getName().equalsIgnoreCase("not like") || sqlBinaryOperator.getName().equalsIgnoreCase("like")) {
                        if (SqlCache.getDbType().equalsIgnoreCase("sqlserver")) {
                            if (x.getLeft() instanceof SQLBinaryOpExpr && x.getRight() instanceof SQLCharExpr) {
                                SQLBinaryOpExpr sqlBinaryOpExpr = (SQLBinaryOpExpr) x.getLeft();
                                SQLExpr left = sqlBinaryOpExpr.getLeft();
                                if (left instanceof SQLBinaryOpExpr) {
                                    SQLBinaryOpExpr sqlBinaryOpExpr_left = (SQLBinaryOpExpr) left;
                                    SQLExpr right = sqlBinaryOpExpr_left.getRight();
                                    if (right instanceof SQLMethodInvokeExpr) {
                                        SQLMethodInvokeExpr sqlMethodInvokeExpr_left = (SQLMethodInvokeExpr) right;
                                        List<SQLExpr> arguments = sqlMethodInvokeExpr_left.getArguments();
                                        if (arguments.size() > 1) {
                                            SQLValuableExpr newValue = TransUtil.getNewValue(fromTableList, arguments.get(1), (SQLValuableExpr) x.getRight());
                                            x.setRight(newValue);


                                            TransUtil.customWorkFlowBrowser(arguments.get(1),x.getRight(),x);
                                            return true;
                                        }
                                    }

                                }

                            }
                        }


                        if (SqlCache.getDbType().equalsIgnoreCase("oracle")) {
                            if (x.getLeft() instanceof SQLMethodInvokeExpr && x.getRight() instanceof SQLCharExpr) {
                                SQLMethodInvokeExpr sqlMethodInvokeExpr = (SQLMethodInvokeExpr) x.getLeft();
                                if (sqlMethodInvokeExpr.getArguments().size() > 1) {
                                    SQLExpr sqlExpr = sqlMethodInvokeExpr.getArguments().get(0);
                                    if (sqlExpr instanceof SQLMethodInvokeExpr) {
                                        SQLMethodInvokeExpr sqlMethodInvokeExpr_left = (SQLMethodInvokeExpr) sqlExpr;
                                        if (sqlMethodInvokeExpr_left.getArguments().size() > 1) {
                                            List<SQLExpr> arguments = sqlMethodInvokeExpr_left.getArguments();
                                            SQLValuableExpr newValue = TransUtil.getNewValue(fromTableList, arguments.get(1), (SQLValuableExpr) x.getRight());
                                            x.setRight(newValue);


                                            TransUtil.customWorkFlowBrowser(arguments.get(1),x.getRight(),x);
                                            return true;
                                        }
                                    }
                                }

                            }
                        }


                    }
                } catch (Exception e) {
                }


                SQLExpr left = x.getLeft();

                if ((x.getLeft() instanceof SQLCharExpr || x.getLeft() instanceof SQLIntegerExpr)
                        && (x.getRight() instanceof SQLPropertyExpr || x.getRight() instanceof SQLIdentifierExpr)
                ) {
                    SQLValuableExpr newValue = TransUtil.getNewValue(fromTableList, x.getRight(), (SQLValuableExpr) x.getLeft());
                    x.setLeft(newValue);
                    //流程浏览按钮判断特殊处理
                    TransUtil.customWorkFlowBrowser(x.getRight(),x.getLeft(),x);
                } else if ((x.getRight() instanceof SQLCharExpr || x.getRight() instanceof SQLIntegerExpr)
                        && (x.getLeft() instanceof SQLPropertyExpr || x.getLeft() instanceof SQLIdentifierExpr)
                ) {
                    SQLValuableExpr newValue = TransUtil.getNewValue(fromTableList, x.getLeft(), (SQLValuableExpr) x.getRight());
                    x.setRight(newValue);

                    //流程浏览按钮判断特殊处理
                    TransUtil.customWorkFlowBrowser(x.getLeft(),x.getRight(),x);

                }
            }
        }
        return true;
    }


    /**
     * 条件查询列表替换。   in  notin
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLInListExpr x) {
        SQLExpr column = x.getExpr();
        if (!(column instanceof SQLPropertyExpr) && !(column instanceof SQLIdentifierExpr)) {
            return true;
        }
        List<SQLExpr> targetList = x.getTargetList();
        boolean not = x.isNot();
        List<TableBeanHasAlias> fromTableList = TransUtil.getFromTableList(x.getParent());

        for (int i = targetList.size() - 1; i >= 0; i--) {
            SQLExpr sqlExpr = targetList.get(i);
            if ((sqlExpr instanceof SQLCharExpr || sqlExpr instanceof SQLIntegerExpr)){
                SQLValuableExpr newValue = TransUtil.getNewValue(fromTableList, column, (SQLValuableExpr) sqlExpr);
                targetList.set(i,newValue);
            }
        }
        return true;
    }


    /**
     * 处理 这种情况case when
     * select case id
     * when 355 then 'qweqwe'
     * when 2 then 'qweqwe'
     * when 485 then 'qweqwe'
     * else id end  from hrmresource
     *
     * SELECT WS.SELECTNAME,
     *        WS.SELECTVALUE,
     *        case BILLID
     *            when 1 then 'wjh'
     *            when '-1' then 'hhh'
     *            when 'aaa' then 'cc'
     *            when '1231' then 'bb'
     *            else 'bbb' end as tpm1,
     *        case
     *            when BILLID = 1 then 'wjh'
     *            when BILLID = '-1' then 'hhh'
     *            when BILLID = 'aaa' then 'cc'
     *            when BILLID = '1231' then 'bb'
     *            else 'bbb' end as tpm2
     * FROM WORKFLOW_BILLFIELD WB
     *          LEFT JOIN WORKFLOW_SELECTITEM WS ON WB.ID = WS.FIELDID
     * WHERE BILLID = '-13fe'
     *   and WB.BILLID = -13
     *   and '-13' = BILLID
     *   and '-13' = WB.BILLID
     *   AND FIELDNAME = 'lzxz2'
     *   AND WB.id = '12w313'
     *   AND WB.id = '213'
     *   AND WB.id = 2131
     *   and BILLID in ('-13', 13, '-13g', '13')
     *
     *
     * select case maritalstatus when '0' then 'wjh' when '0' then 'hhh' when '1' then 'cc' when 1 then 'bb' else 'bbb' end,
     *        case
     *            when maritalstatus = '0' then 'wjh'
     *            when t1.SUBCOMPANYID1 = '2' then 'hhh'
     *            when t1.id = '1' then 'hhh'
     *            when t1.DEPARTMENTID = '1' then 'cc'
     *            when maritalstatus = 1 then 'bb'
     *            else 'bbb' end as ttt
     * from hrmresource t1
     * where maritalstatus = '0'
     *   and '0' = maritalstatus
     *   and t1.maritalstatus = '0'
     *   and '0' = t1.maritalstatus
     *   and maritalstatus in ('0', 0, '0', '0')
     *   and t1.SUBCOMPANYID1 = 2
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLCaseExpr x) {
        List<SQLCaseExpr.Item> items = x.getItems();

        SQLExpr column = x.getValueExpr();
        if (!(column instanceof SQLPropertyExpr) && !(column instanceof SQLIdentifierExpr)) {
            return true;
        }
        List<TableBeanHasAlias> fromTableList = TransUtil.getFromTableList(x.getParent());
        for (int i = 0; i < items.size(); i++) {
            SQLCaseExpr.Item item = items.get(i);
            SQLExpr conditionExpr = item.getConditionExpr();
            if ((conditionExpr instanceof SQLCharExpr || conditionExpr instanceof SQLIntegerExpr)
            ) {
                SQLValuableExpr newValue = TransUtil.getNewValue(fromTableList, column, (SQLValuableExpr) conditionExpr);
                item.setConditionExpr(newValue);
            }
        }

        return true;
    }


    /**
     * 替换between 值转换
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLBetweenExpr x) {

        List<TableBeanHasAlias> fromTableList = TransUtil.getFromTableList(x.getParent());

        SQLExpr column = x.getTestExpr();
        String oldFieldName = null;
        if (column instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) column;
            oldFieldName = sqlPropertyExpr.getName().toLowerCase();
            String owner = sqlPropertyExpr.getOwnernName().toLowerCase();
            TransUtil.filterTable(owner, fromTableList);
        } else if (column instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr sqlPropertyExpr = (SQLIdentifierExpr) column;
            oldFieldName = sqlPropertyExpr.getName().toLowerCase();
            if (oldFieldName.contains(".")) {
                TransUtil.filterTable(oldFieldName, fromTableList);
            }
        } else {
            return true;
        }


        SQLExpr beginExpr = x.getBeginExpr();
        SQLExpr endExpr = x.getEndExpr();

        if (beginExpr instanceof SQLCharExpr) {
            SQLCharExpr sqlCharExpr = (SQLCharExpr) beginExpr;
            final String old_text = sqlCharExpr.getText();
            final String new_text = TransUtil.getNewValue(fromTableList, oldFieldName, sqlCharExpr.getText());
            sqlCharExpr.setText(new_text);
            if (!old_text.equals(new_text)) {
                TransUtil.addTransMsg("将sql中条件值=>" + old_text + "替换为=>" + new_text);
            }
        } else if (beginExpr instanceof SQLIntegerExpr) {
            SQLIntegerExpr sqlIntegerExpr = (SQLIntegerExpr) beginExpr;
            String old_number = String.valueOf(sqlIntegerExpr.getNumber());
            String new_aLong = String.valueOf(TransUtil.getNewValue(fromTableList, oldFieldName, String.valueOf(sqlIntegerExpr.getNumber())));
            sqlIntegerExpr.setNumber(Long.valueOf(new_aLong));
            if (!old_number.equals(new_aLong)) {
                TransUtil.addTransMsg("将sql中条件值=>" + old_number + "替换为=>" + new_aLong);
            }
        }
        if (endExpr instanceof SQLCharExpr) {
            SQLCharExpr sqlCharExpr = (SQLCharExpr) endExpr;
            final String old_text = sqlCharExpr.getText();
            final String new_text = TransUtil.getNewValue(fromTableList, oldFieldName, sqlCharExpr.getText());
            sqlCharExpr.setText(new_text);
            if (!old_text.equals(new_text)) {
                TransUtil.addTransMsg("将sql中条件值=>" + old_text + "替换为=>" + new_text);
            }
        } else if (endExpr instanceof SQLIntegerExpr) {
            SQLIntegerExpr sqlIntegerExpr = (SQLIntegerExpr) endExpr;
            String old_number = String.valueOf(sqlIntegerExpr.getNumber());
            String new_aLong = String.valueOf(TransUtil.getNewValue(fromTableList, oldFieldName, String.valueOf(sqlIntegerExpr.getNumber())));
            sqlIntegerExpr.setNumber(Long.valueOf(new_aLong));
            if (!old_number.equals(new_aLong)) {
                TransUtil.addTransMsg("将sql中条件值=>" + old_number + "替换为=>" + new_aLong);
            }
        }

        return true;
    }



    @Override
    public boolean visit(SQLExprTableSource x) {
        List<String> tableUseField = TransUtil.getTableUseField(x);
        Map<String, Object> attributes = x.getAttributes();
        attributes.put("tableUseField", tableUseField);
        attributes.put("isE9table",1 );
//        x.getAttributes()
//        SQLUtil.queue.add(tableUseField);
        return true;
    }
	
	
    @Override
    public boolean visit(SQLCreateViewStatement x) {
        sqlResultInfo.put("sqlType", "view");
        return true;
    }


    @Override
    public boolean visit(SQLWithSubqueryClause.Entry x) {
        String sqlType = (String) Optional.ofNullable(sqlResultInfo.get("sqlType")).orElse("");
        if (sqlType.equals("view")) {
            List viewSQLWithAlse = (List) Optional.ofNullable(sqlResultInfo.get("viewSQLWithAlse")).orElse(new ArrayList<String>());
            if (x.getAlias()!=null){
                viewSQLWithAlse.add(x.getAlias().toLowerCase());
                sqlResultInfo.put("viewSQLWithAlse", viewSQLWithAlse);
            }
        }
        return true;
    }


    @Override
    /**
     * 抓取SQL使用的方法,用于SQL分析
     */
    public boolean visit(SQLMethodInvokeExpr x) {
        String methodName = x.getMethodName();
        Integer countInt = methodUesCount.getOrDefault(methodName.toLowerCase(), 0);
        methodUesCount.put(methodName.toLowerCase(), countInt + 1);
        return super.visit(x);
    }


    /**
     * 给ygd专用
     * @param sqlSelectStatement
     * @return
     */
    @Override
    public boolean visit(SQLSelectStatement sqlSelectStatement) {

        if (!sqlResultInfo.containsKey("cubeGetOrderAndContCondition")) {
            return true;
        }

        List<SQLExpr> sqlExprList = new ArrayList<>();
        Optional.ofNullable(sqlSelectStatement)
                .map(x -> x.getSelect())
                .map(x -> x.getQuery())
                .filter(x -> x instanceof SQLSelectQueryBlock).
                map(query -> (SQLSelectQueryBlock) query).
                map(query -> query.getWhere()).
                ifPresent(sqlExpr_tmp -> sqlExprList.add(sqlExpr_tmp));

        Optional.ofNullable(sqlSelectStatement)
                .map(x -> x.getSelect())
                .map(x -> x.getQuery())
                .filter(x -> x instanceof SQLSelectQueryBlock).
                map(query -> (SQLSelectQueryBlock) query).
                map(query -> query.getOrderBy()).map(x -> x.getItems()).
                ifPresent(items -> sqlExprList.addAll(items.stream().map(x -> x.getExpr()).collect(Collectors.toList()))
                );

        List<SQLExpr> resultList_tmp = new ArrayList<>();
        //全部拆成最基础的 SQLExpr
        sqlExprList.forEach(sqlExpr -> TransUtil.getSqlExpr(sqlExpr, resultList_tmp));
        List<String> cubeUseFieldList = resultList_tmp.stream().map(sqlExpr -> {
            if (sqlExpr instanceof SQLIdentifierExpr) {
                SQLIdentifierExpr sqlExpr_tmpx = (SQLIdentifierExpr) sqlExpr;
                return sqlExpr_tmpx.getName();
            } else if (sqlExpr instanceof SQLPropertyExpr) {
                SQLPropertyExpr sqlExpr_tmpx = (SQLPropertyExpr) sqlExpr;
                return sqlExpr_tmpx.getName();
            }
            return null;
        }).filter(x -> x == null ? false : true).map(x -> x.toLowerCase()).distinct().collect(Collectors.toList());
        sqlResultInfo.put("cubeUseField", cubeUseFieldList);
        return true;
    }

}
