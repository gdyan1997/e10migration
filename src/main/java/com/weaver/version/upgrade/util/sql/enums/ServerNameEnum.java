package com.weaver.version.upgrade.util.sql.enums;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

public enum ServerNameEnum {

    hr(1L, "local-hr", "weaver-hr-service","人事",199391L,"hrm"),
    crm(2L, "local-crm", "weaver-crm-service","CRM",192066L,"crm"),
    tenant(3L, "local-tenant", "weaver-tenant-service","租戶",275282L,"tenant"),
    eteams(4L, "local-eteams", "weaver-hrm-service","HRM",275283L,"hrm"),
    blog(5L, "local-blog", "weaver-blog-service","日报",199397L,"blog"),
    fna(6L, "local-fna", "weaver-fna-expense-service","电子费控",275284L,"fna"),
    doc(7L, "local-doc", "weaver-doc-service","文档",192078L,"document"),
    formreport(8L, "local-formreport", "weaver-formreport-service","业务表单",192076L,"formdatareport"),
    project(9L, "local-project", "weaver-project-service","任务",192711L,"mainline"),
    ebuilderform(10L, "local-ebuilderform", "weaver-ebuilder-form-service","e-Builder表单",192080L,"ebuilderform"),
    signcenter(11L, "local-signcenter", "weaver-signcenter-service","电子签",275285L,"signcontract"),
    bigData(12L, "local-bigData", "weaver-bigdata-platform","大数据",275286L,"datamodel"),
    workflowList(13L, "local-workflow-list", "weaver-workflow-list-service","工作流程",275320L,"workflow"),
    workflow(14L, "local-workflow-report", "weaver-workflow-report-service","流程",192079L,"workflow"),
    elog(15L, "local-elog", "weaver-elog-service","日志",195996L,"elog"),
    meeting(16L, "local-meeting", "weaver-meeting-service","会议",196858L,"meeting"),
    schedule(17L, "local-schedule", "weaver-basic-schedule-service","基础定时模块",286246L,"base"),
    wrGoal(18L, "local-wr-goal", "weaver-wr-goal-service","OKR",275287L,"goal"),
    attend(20L, "local-attend", "weaver-attend-service","考勤",275290L,"attend"),
    portrait(22L, "local-portrait", "weaver-portrait-service","组织画像",275291L,"portrait"),
    wrPerformance(24L, "local-wr-performance", "weaver-wr-performance-service","绩效",275292L,"kpi"),
    edcreportd(29L, "local-edcreportd", "weaver-edcreportd-service","e-Builder报表",198866L,""),
    esbSetting(30L, "local-esb-setting", "weaver-esb-setting-service","ESB",275293L,"esb"),
    ebuilderApp(31L, "local-ebuilder-app", "weaver-ebuilder-app-service","e-Builder应用",275294L,"ebuilder"),
    customer(33L, "local-customer", "weaver-customer-service-service","客服",275295L,"customerservice"),
    calendar(34L, "local-calendar", "weaver-calendar-service","日程",275296L,"calendar"),
    file(36L, "local-file", "weaver-file-service","文件",90870L,"file"),
    odoc(37L, "local-odoc", "weaver-odoc-service","公文管理",275297L,"odoc"),
    crmMarket(38L, "local-crm-market", "weaver-crm-market-service","SCRM",275299L,"scrm"),
    cowork(39L, "local-cowork", "weaver-cowork-service","cowork",275300L,"cowork"),
    datasource(40L, "local-datasource", "weaver-datasource-service","数据源",275301L,""),
    wrPlan(41L, "local-wr-plan", "weaver-wr-plan-service","计划报告",275302L,"workreport"),
    interfaces(42L, "local-interfaces", "weaver-interfaces-service","技术文档扫描",275303L,"openapi"),
    i18n(44L, "local-i18n", "weaver-i18n-service","i18n",275304L,"i18n"),
    datasecurity(45L, "local-datasecurity", "weaver-datasecurity","表单加密",275305L,"datasecurity"),
    passport(46L, "local-passport", "weaver-passport-service","登录服务",275306L,"passport"),
    salary(47L, "local-salary", "weaver-salary-report","薪酬",275307L,"hrmsalary"),
    incBiz(50L, "local-inc-biz", "weaver-inc-biz-service","发票云",275308L,"inc"),
    my(53L, "local-my", "weaver-my-service","代办事项",275309l,"my"),
    ebuilderContract(54L, "local-ebuilder-contract", "weaver-ebuilder-contract-service","合同管理",286246L,"ebcontract"),
    securityFramework(57L, "local-security-framework", "weaver-security-framework-service","系统安全",275310l,"security"),
    intunifyauthServerBase(58L, "local-intunifyauth-server-base", "weaver-intunifyauth-server-base-service","统一认证",275311l,"intunifyauthserver"),
    odocexchange(59L, "local-odocexchange", "weaver-odocexchange-service","公文交换",275312l,"odocexchange"),
    archiveCore(60L, "local-archive-core", "weaver-archive-core-service","档案管理",275313l,"archive"),
    basicOnlineWeb(62L, "local-basic-online-web", "weaver-basic-online-web-service","招聘管理",275314L,""),
    esearchSearch(63L, "local-esearch-search", "weaver-esearch-search-service","微搜",275315L,"esearch"),
    intunifytodoServerConfig(64L, "local-intunifytodo-server-config", "weaver-intunifytodo-server-config-service","统一审批",275316L,"intunifytodos"),
    intunifytodoClientConfig(65L, "local-intunifytodo-client-config", "weaver-intunifytodo-client-config-service","代办推送",275317L,""),
    componentWeb(66L, "local-component-web", "weaver-component-web-service","公共数据源",275317L,"common"),

    //    ebuilderFormRuntime(67L, "local-ebuilder-form-runtime", "weaver-ebuilder-form-runtime-service","e-Builder应用建模"),
    ebuilderRuntime(68L, "local-ebuilder-runtime", "weaver-ebuilder-runtime-service","e-Builder应用建模",275319L,""),
    wfsdkEbuilder(69L, "local-wfsdk-ebuilder", "weaver-wfsdk-ebuilder-service","工作流程",275320L,"workflow"),
    governComs(70L, "local-govern-coms", "weaver-govern-coms-service","政务应用",275321L,""),
    cloudstore(71L, "local-cloudstore", "weaver-cloudstore-service","云商店",275322L,"cloudstore"),
    incBook(72L, "local-inc-book", "weaver-inc-book-service","发票云",275308L,"inc"),
    hrmSalary(73L, "local-hrm-salary", "weaver-hrm-salary","薪酬管理",275323l,"hrm"),
    signature(87L, "local-signature", "weaver-signature-service","签名",286247L,"signcontract"),

    flow(-1L,"local-flow", "weaver-workflow-core-service","流程",192079l,"workflow"),
    recruit(-2L, "local-recruit", "weaver-recruit-service","招聘管理",275314l,"");

    private final Long connId;      //提供给E9迁移固定死的
    private final String connName;
    private final String serverName;
    private final String desc;
    private final Long labelId;
    private final String module;


    ServerNameEnum(Long connId, String connName, String serverName, String desc, Long labelId, String module) {
        this.connId = connId;
        this.connName = connName;
        this.serverName = serverName;
        this.desc = desc;
        this.labelId = labelId;
        this.module = module;
    }

    public static ServerNameEnum getEnumByServer(String serverName) {
        ServerNameEnum enums = null;
        if (StringUtils.isNotBlank(serverName)) {
            for (ServerNameEnum value : ServerNameEnum.values()) {
                if (value.getServerName().equals(serverName)) {
                    enums = value;
                }
            }
        }
        return enums;
    }

    public static ServerNameEnum getEnumById(Long id) {
        ServerNameEnum enums = null;
        if (id != null) {
            for (ServerNameEnum value : ServerNameEnum.values()) {
                if (value.getConnId().longValue() == id.longValue()) {
                    enums = value;
                }
            }
        }
        return enums;
    }

    public static ServerNameEnum getEnumByModule(String module) {
        ServerNameEnum enums = null;
        if (StringUtils.isNotBlank(module)) {
            for (ServerNameEnum value : ServerNameEnum.values()) {
                if (value.getModule().equalsIgnoreCase(module)) {
                    enums = value;
                }
            }
        }
        return enums;
    }


    public static String getModuleById(Long id) {
        String module = null;
        if (id !=null) {
            for (ServerNameEnum value : ServerNameEnum.values()) {
                if (value.getConnId().equals(id)) {
                    module = value.getModule();
                }
            }
        }
        return module;
    }

    public Long getConnId() {
        return connId;
    }

    public String getConnName() {
        return connName;
    }

    public String getServerName() {
        return serverName;
    }

    public String getDesc() {
        return desc;
    }

    public Long getLabelId() {
        return labelId;
    }

    public String getModule() {
        return module;
    }

    public static JSONArray workJosnArrary = JSONObject.parseArray("[\n" +
            "  {\n" +
            "    \"application\": \"weaver-formreport-service\",\n" +
            "    \"application_name\": \"业务表单\",\n" +
            "    \"id\": 1\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-report-serviceworkflowreport\",\n" +
            "    \"application_name\": \"流程\",\n" +
            "    \"id\": 2\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-project-servicetask\",\n" +
            "    \"application_name\": \"任务\",\n" +
            "    \"id\": 3\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-elog-service\",\n" +
            "    \"application_name\": \"日志\",\n" +
            "    \"id\": 4\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-meeting-service\",\n" +
            "    \"application_name\": \"会议\",\n" +
            "    \"id\": 5\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-blog-service\",\n" +
            "    \"application_name\": \"日报\",\n" +
            "    \"id\": 6\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-wr-goal-service\",\n" +
            "    \"application_name\": \"OKR\",\n" +
            "    \"id\": 7\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-list-service\",\n" +
            "    \"application_name\": \"流程列表\",\n" +
            "    \"id\": 8\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-attend-service\",\n" +
            "    \"application_name\": \"考勤\",\n" +
            "    \"id\": 9\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-crm-service\",\n" +
            "    \"application_name\": \"CRM\",\n" +
            "    \"id\": 10\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-portrait-service\",\n" +
            "    \"application_name\": \"组织画像\",\n" +
            "    \"id\": 11\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-report-serviceworkflow_report\",\n" +
            "    \"application_name\": \"工作流报告\",\n" +
            "    \"id\": 12\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-wr-performance-service\",\n" +
            "    \"application_name\": \"绩效\",\n" +
            "    \"id\": 13\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-project-service\",\n" +
            "    \"application_name\": \"项目\",\n" +
            "    \"id\": 14\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-basic-schedule-service\",\n" +
            "    \"application_name\": \"基础定时模块\",\n" +
            "    \"id\": 16\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-doc-service\",\n" +
            "    \"application_name\": \"文档\",\n" +
            "    \"id\": 17\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-hrm-service\",\n" +
            "    \"application_name\": \"HRM\",\n" +
            "    \"id\": 18\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-page-service\",\n" +
            "    \"application_name\": \"页面\",\n" +
            "    \"id\": 19\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-designer-service\",\n" +
            "    \"application_name\": \"页面设计器\",\n" +
            "    \"id\": 20\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-edcreportd-service\",\n" +
            "    \"application_name\": \"e-Builder报表\",\n" +
            "    \"id\": 21\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esb-setting-service\",\n" +
            "    \"application_name\": \"esb\",\n" +
            "    \"id\": 22\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-app-service\",\n" +
            "    \"application_name\": \"e-Builder应用\",\n" +
            "    \"id\": 23\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-form-service\",\n" +
            "    \"application_name\": \"e-Builder表单\",\n" +
            "    \"id\": 24\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esearch-data-service\",\n" +
            "    \"application_name\": \"搜索服务\",\n" +
            "    \"id\": 25\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-customer-service-service\",\n" +
            "    \"application_name\": \"客服\",\n" +
            "    \"id\": 26\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-calendar-service\",\n" +
            "    \"application_name\": \"日程\",\n" +
            "    \"id\": 27\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-doc-servicedocument\",\n" +
            "    \"application_name\": \"文档\",\n" +
            "    \"id\": 28\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-file-service\",\n" +
            "    \"application_name\": \"文件\",\n" +
            "    \"id\": 30\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-odoc-service\",\n" +
            "    \"application_name\": \"公文管理\",\n" +
            "    \"id\": 32\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-crm-market-service\",\n" +
            "    \"application_name\": \"SCRM\",\n" +
            "    \"id\": 33\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-cowork-service\",\n" +
            "    \"application_name\": \"协作区\",\n" +
            "    \"id\": 34\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-datasource-service\",\n" +
            "    \"application_name\": \"数据源\",\n" +
            "    \"id\": 35\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-wr-plan-service\",\n" +
            "    \"application_name\": \"计划报告\",\n" +
            "    \"id\": 36\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-interfaces-service\",\n" +
            "    \"application_name\": \"技术文档扫描\",\n" +
            "    \"id\": 37\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-excelformula-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 38\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-core-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 39\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-form-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 40\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-pathdef-service\",\n" +
            "    \"application_name\": \"\",\n" +
            "    \"id\": 41\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-prints-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 42\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-signcenter-service\",\n" +
            "    \"application_name\": \"电子签\",\n" +
            "    \"id\": 43\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-i18n-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 44\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esb-setting-serviceesb\",\n" +
            "    \"application_name\": \"动作流监控\",\n" +
            "    \"id\": 46\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-datasecurity\",\n" +
            "    \"application_name\": \"表单加密\",\n" +
            "    \"id\": 47\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-salary-report\",\n" +
            "    \"application_name\": \"薪酬\",\n" +
            "    \"id\": 49\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-hr-service\",\n" +
            "    \"application_name\": \"人事\",\n" +
            "    \"id\": 50\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esb-setting-serviceesbCustom\",\n" +
            "    \"application_name\": \"动作流\",\n" +
            "    \"id\": 51\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-biz-service\",\n" +
            "    \"application_name\": \"发票云\",\n" +
            "    \"id\": 52\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-tenant-service\",\n" +
            "    \"application_name\": \"租户\",\n" +
            "    \"id\": 53\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-contract-servicecmdatauFJXHS\",\n" +
            "    \"application_name\": \"绩效核算\",\n" +
            "    \"id\": 54\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-component-manager-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 55\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-contract-service\",\n" +
            "    \"application_name\": \"EB合同管理\",\n" +
            "    \"id\": 57\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-doccompare-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 58\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"openapi-auth-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 59\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-open-api\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 61\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-edcreport-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 62\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebatch-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 63\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-placard-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 64\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-escheduler-admin-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 65\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-escheduler-user-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 66\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-my-service\",\n" +
            "    \"application_name\": \"待办事项 \",\n" +
            "    \"id\": 67\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-mc-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 68\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-voice-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 70\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-datarule-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 71\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-flow-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 72\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-crmext-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 73\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esb-server-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 76\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-coms-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 77\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-fna-expense-service\",\n" +
            "    \"application_name\": \"电子费控\",\n" +
            "    \"id\": 78\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-baseserver-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 79\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-basic-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 80\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-salary-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 81\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-portal-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 82\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-bcw-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 83\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-authority\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 84\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-buildapply-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 85\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-sms-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 86\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-signature-service\",\n" +
            "    \"application_name\": \"签名\",\n" +
            "    \"id\": 87\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-em-base-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 88\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-em-msg-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 89\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-openwechat-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 90\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-eb-main-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 91\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-eb-demo-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 92\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-mail-base-service\",\n" +
            "    \"application_name\": \"邮件\",\n" +
            "    \"id\": 93\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-mail-receive-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 94\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-mail-send-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 95\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-data-model-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 96\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-example-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 97\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intlogin-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 98\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ecode-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 99\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifytodo-client-engine-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 100\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifytodo-server-engine-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 102\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ldap-auth-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 103\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ldap-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 104\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ldap-syncin-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 105\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ldap-syncout-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 106\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intexchange-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 107\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-hrm-salary\",\n" +
            "    \"application_name\": \"薪酬\",\n" +
            "    \"id\": 108\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-cloudstore-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 109\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-hr-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 110\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-hr-sync-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 111\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-client-cas-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 112\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-client-oauth-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 113\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-interfaces-doc-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 114\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-server-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 115\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-security-framework-service\",\n" +
            "    \"application_name\": \"系统安全\",\n" +
            "    \"id\": 116\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intlogin-element\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 117\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-url-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 118\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-front-monitor-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 119\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intmail-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 120\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-data-search-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 121\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-techdoc-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 122\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-statistics-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 123\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-scene-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 124\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-common-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 125\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intcenter-common-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 126\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-architecture-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 127\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-server-base-service\",\n" +
            "    \"application_name\": \"统一认证中心\",\n" +
            "    \"id\": 128\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intmail-tencent-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 129\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-element-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 130\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-bpm-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 131\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-cusapp-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 132\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-edcapp-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 133\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-proxy\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 134\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-fna-enterprise-bank-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 135\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-edcapp-statistics-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 136\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-contentmoderation-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 137\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-client-webseal-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 138\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esb-kit-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 139\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intias-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 140\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-publishkit-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 141\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-adapter-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 142\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-async-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 143\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-data-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 145\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-file-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 146\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-log-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 147\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-mail-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 148\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-ocr-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 149\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-parser-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 150\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-inc-third-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 151\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-fna-voucher-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 152\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-yjs-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 153\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-hook-manager-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 154\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-proxy-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 155\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-data-datalab-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 156\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intias-core-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 157\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-iconfont-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 158\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weapp-iconfont-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 159\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-datacenter-data-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 160\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-odocexchange-service\",\n" +
            "    \"application_name\": \"数据交换中心\",\n" +
            "    \"id\": 161\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-customized-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 162\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-govern-coms-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 163\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-govern-business-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 164\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intdevice-web-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 165\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-archive-core-service\",\n" +
            "    \"application_name\": \"档案管理\",\n" +
            "    \"id\": 166\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-recruit-service\",\n" +
            "    \"application_name\": \"招聘管理\",\n" +
            "    \"id\": 167\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-basic-online-web-service\",\n" +
            "    \"application_name\": \"基础在线服务\",\n" +
            "    \"id\": 168\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-framework-configcenter-proxy-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 169\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-version-upgrade-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 170\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-file-aspose\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 171\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esearch-search-service\",\n" +
            "    \"application_name\": \"微搜\",\n" +
            "    \"id\": 172\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"ebuilder-installer-service\",\n" +
            "    \"application_name\": \"\",\n" +
            "    \"id\": 173\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-authresp-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 174\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-customer-servicecsReportTable\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 175\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifytodo-server-config-service\",\n" +
            "    \"application_name\": \"统一审批中心\",\n" +
            "    \"id\": 176\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifytodo-client-config-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 177\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-component-web-service\",\n" +
            "    \"application_name\": \"公共数据源\",\n" +
            "    \"id\": 178\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-analyze-service\",\n" +
            "    \"application_name\": \"数据分析\",\n" +
            "    \"id\": 179\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-editor-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 180\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-wfsdk-ebuilder-service\",\n" +
            "    \"application_name\": \"流程伴生服务\",\n" +
            "    \"id\": 181\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-fna-expense-servicefexs\",\n" +
            "    \"application_name\": \"财务模块\",\n" +
            "    \"id\": 182\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-core-serviceSubRequest\",\n" +
            "    \"application_name\": \"子流程\",\n" +
            "    \"id\": 200\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ai-application-service\",\n" +
            "    \"application_name\": \"智能化服务\",\n" +
            "    \"id\": 201\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-installer-service\",\n" +
            "    \"application_name\": \"导入系统\",\n" +
            "    \"id\": 202\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-ebuilder-packing-service\",\n" +
            "    \"application_name\": \"导出系统\",\n" +
            "    \"id\": 203\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-i18n-common-service\",\n" +
            "    \"application_name\": null,\n" +
            "    \"id\": 205\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-pathdef-serviceWfpPermissionTransfer\",\n" +
            "    \"application_name\": \"流程字段权限历史数据迁移\",\n" +
            "    \"id\": 206\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-passport-service\",\n" +
            "    \"application_name\": \"登录服务\",\n" +
            "    \"id\": 1888\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-workflow-report-serviceworkflowFormReport\",\n" +
            "    \"application_name\": \"工作流表单\",\n" +
            "    \"id\": 2002\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-intunifyauth-server-base-service\",\n" +
            "    \"application_name\": \"统一认证中心\",\n" +
            "    \"id\": 840314071030654963\n" +
            "  },\n" +
            "  {\n" +
            "    \"application\": \"weaver-esb-setting-serviceesbConnect\",\n" +
            "    \"application_name\": \"动作流\",\n" +
            "    \"id\": 1058965908263886889\n" +
            "  }\n" +
            "]");


}
