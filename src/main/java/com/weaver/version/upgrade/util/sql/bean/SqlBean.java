package com.weaver.version.upgrade.util.sql.bean;

import com.weaver.version.upgrade.util.sql.util.TransUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;

/**
 * @author wujiahao
 * @date 2024/7/29 下午3:49
 */
public class SqlBean {


    /**
     * 传入的sql
     */
    private String sourceSql;


    /**
     * 用于解析sql
     */
    private String sqlNew;


    private List<String> numList;


    private Map<String, String> holderNum = new HashMap<>();



    public SqlBean(String sourceSql) {
        this.sourceSql = sourceSql;
    }


    public static void main(String[] args) {
        String sqlNew1 = new SqlBean("update zcctest07251134 set testa=[${testa}],testf=[${testf}] where testc =[${testc}]").getSqlNew();

        //System.out.println(sqlNew1);

    }
    /**
     *
     * @return  获取语法正确的sql
     */
    public String getSqlNew() {

        List<String> holderList = new ArrayList<>();

        String sqlNew = sourceSql;
        String sqlNew_tmp = sourceSql;


        {



            // 取出$$这种占位符参数
            String regex1 = "\\[\\$\\{[A-Za-z0-9\\_\\.]+\\}\\]";
            Matcher matcher1 = Pattern.compile(regex1).matcher(sqlNew);
            while (matcher1.find()) {
                String group = matcher1.group(0);
                if (!holderList.contains(group)) {
                    holderList.add(group);
                    sqlNew = sqlNew.replace(group, "_wjh_");
                }
            }
        }



        // 取出$$这种占位符参数
        String regex1 = "\\$[A-Za-z0-9\\_\\.]+\\$";
        Matcher matcher1 = Pattern.compile(regex1).matcher(sqlNew);
        while (matcher1.find()) {
            String group = matcher1.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sqlNew = sqlNew.replace(group, "_wjh_");
            }
        }

        // 取出$$这种占位符参数
        String regex1_ = "\\[\\{[A-Za-z0-9\\_\\.]+\\}\\]";
        Matcher matcher1_ = Pattern.compile(regex1_).matcher(sqlNew);
        while (matcher1_.find()) {
            String group = matcher1_.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sqlNew = sqlNew.replace(group, "_wjh_");
            }
        }


        // 取出#{?userid}这种系统参数
//        String regex2 = "\\{[A-Za-z0-9\\_\\.\\?]+\\}";
        String regex3 = "#\\{[A-Za-z0-9_\\.\\?\\u4e00-\\u9fa5]+\\}";
        Matcher matcher3 = Pattern.compile(regex3).matcher(sqlNew);
        while (matcher3.find()) {
            String group = matcher3.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sqlNew = sqlNew.replace(group, "_wjh_");

            }
        }


        // 取出{?userid}这种系统参数
//        String regex2 = "\\{[A-Za-z0-9\\_\\.\\?]+\\}";
        String regex2 = "\\{[A-Za-z0-9_\\.\\?\\u4e00-\\u9fa5]+\\}";
        Matcher matcher2 = Pattern.compile(regex2).matcher(sqlNew);
        while (matcher2.find()) {
            String group = matcher2.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sqlNew = sqlNew.replace(group, "_wjh_");
            }
        }


        List<String> numList = new ArrayList<>();

        for (String holder : holderList) {
            String num = TransUtil.generateSnowId();
            numList.add(num);
            sqlNew_tmp = sqlNew_tmp.replace(holder, num);
            holderNum.put(num, holder);
        }

        //处理问号
        {
            String num = TransUtil.generateSnowId();
            numList.add(num);
            sqlNew_tmp = sqlNew_tmp.replace("?", num);
            holderNum.put(num, "?");
        }


        this.sqlNew=sqlNew_tmp;
        sqlResultInfo.put("numList", numList);

        return this.sqlNew;
    }


    public String getResultSql(String replacedSql) {
        //Map<String,String> holderMap = (Map<String, String>) replaceResult.get("holderNum");

        for (String num : holderNum.keySet()) {
            String holder = holderNum.get(num);
            replacedSql = replacedSql.replace(num, holder);
        }
        return replacedSql;
    }


    public String getSourceSql() {
        return sourceSql;
    }

}
