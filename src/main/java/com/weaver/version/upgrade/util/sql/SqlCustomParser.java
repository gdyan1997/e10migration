package com.weaver.version.upgrade.util.sql;

import com.weaver.version.upgrade.util.sql.bean.TableBeanHasAlias;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;

/**
 * @author wujiahao
 * @date 2024/1/30 16:53
 * 解析器无法解析的SQL 自定义解析。
 */
public class SqlCustomParser {

    private static Logger logger = LoggerFactory.getLogger(SqlCustomParser.class);




    /**
     * sql中涉及的所有表
     */
    List<TableBeanHasAlias> list = new ArrayList<>();


    int fromIndex = 0;

    public List<TableBeanHasAlias> parseTables(String sql) {
        String sql_tmp = sql.toLowerCase();
        while (indexOf(sql_tmp, fromIndex).index > 0) {
            // Extract table name
            extractTableName(sql_tmp);
        }

        return list;
    }

    private void extractTableName(String original) {

        // 匹配表名和别名的正则表达式
        Pattern pattern = Pattern.compile("\\b([a-zA-Z_][a-zA-Z0-9_]*)(?:\\s+(?:AS\\s+|\\s*)([a-zA-Z_][a-zA-Z0-9_]*))?", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(original.substring(fromIndex));

        int endLength = 0;
        if (matcher.find()) {
            String tablename = matcher.group(1);
            String asName = matcher.group(2);

            if ("where".equalsIgnoreCase(asName)
                    || "order".equalsIgnoreCase(asName)
                    || "group".equalsIgnoreCase(asName)
            ) {
                asName = null;
            }
            list.add(new TableBeanHasAlias(tablename, asName));
            endLength = matcher.end();
            fromIndex = endLength + fromIndex;
        }

        do {
            Pattern pattern_join = Pattern.compile("^\\s*,\\s*([a-zA-Z_][a-zA-Z0-9_]*)(?:\\s+(?:AS\\s+|\\s*)([a-zA-Z_][a-zA-Z0-9_]*))?", Pattern.CASE_INSENSITIVE);
            Matcher matcher_join = pattern_join.matcher(original.substring(fromIndex));
            if (matcher_join.find()) {
                String tablename = matcher_join.group(1);
                String asName = matcher_join.group(2);
                if ("where".equalsIgnoreCase(asName)
                        || "order".equalsIgnoreCase(asName)
                        || "group".equalsIgnoreCase(asName)
                ) {
                    asName = null;
                }
                list.add(new TableBeanHasAlias(tablename, asName));
                fromIndex = endLength + fromIndex;
            } else {
                break;
            }
        } while (true);
    }

    private KeywordIndex indexOf(String original, int startIndex) {


        // 匹配表名和别名的正则表达式
        Pattern pattern = Pattern.compile("\\b(FROM|JOIN|UPDATE|INTO|delete)\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(original.substring(fromIndex));

        while (matcher.find()) {
            String tableName = matcher.group(1);
            int end = matcher.end();
            fromIndex = fromIndex + end;
            return new KeywordIndex(tableName.toLowerCase(), end);
        }

        return new KeywordIndex("", 0);
    }


    public List<String> convertSql(List<String> list, Map<String, String> transMap) {

        try {


            SqlCache.initCache();


            Map<String, String> holderMap_tmp = new HashMap<>();


            List<String> sqlList = list.stream().map(new Function<String, String>() {
                @Override
                public String apply(String sql) {
                    Map<String, Object> replaceResult = replaceAllHolder(sql);
                    Map<String, String> holderMap = (Map<String, String>) replaceResult.get("holderNum");
                    holderMap_tmp.putAll(holderMap);

                    return TransUtil.null2String(replaceResult.get("sqlNew"));
                }

            }).collect(Collectors.toList());

            String sql_tmp = "";
            for (Object o : sqlList) {
                sql_tmp += o;
            }


            String sql_tmp_new_test = sql_tmp;


            List<TableBeanHasAlias> tableList = parseTables(sql_tmp);


            String sql_tmp_lower = sql_tmp.toLowerCase();

            //判断sql属性
            Pattern pattern = Pattern.compile("\\s*(select|delete|update|insert)", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(sql_tmp);

            if (matcher.find()) {
                String type = matcher.group(1);


                //匹配sql中的所有条件(select insert update)
                //Matcher matcher_condition = Pattern.compile("(\\s*((?:(?:[a-zA-Z_][a-zA-Z0-9_]*)\\.)?([a-zA-Z_][a-zA-Z0-9_]*))\\s*([=<>!]+)\\s*((?:(?:[a-zA-Z_][a-zA-Z0-9_]*)\\.)?([a-zA-Z_][a-zA-Z0-9_]*))\\s*)", Pattern.CASE_INSENSITIVE).matcher(sql_tmp);
                Matcher matcher_condition = Pattern.compile("(((?:(?:[a-zA-Z_][a-zA-Z0-9_]*)\\.)?(?:[a-zA-Z_][a-zA-Z0-9_]*|'(?:[^']|'')*'|\\d+))\\s*([=<>!]+)\\s*((?:(?:[a-zA-Z_][a-zA-Z0-9_]*)\\.)?(?:[a-zA-Z_][a-zA-Z0-9_]*|'(?:[^']|'')*'|\\d+)))", Pattern.CASE_INSENSITIVE).matcher(sql_tmp);
                //条件替换
                while (matcher_condition.find()) {


                    String fullCondition = matcher_condition.group(1);
                    String fullCondition_new = matcher_condition.group(1);
                    String leftField = matcher_condition.group(2).trim();
                    String operator = matcher_condition.group(3);
                    String rightField = matcher_condition.group(4).trim();


                    if (leftField.contains(".")) {
                        String[] split1 = leftField.split("\\.");
                        String owner = split1[0];
                        String fieldName = split1[1];

                        List<TableBeanHasAlias> collect = tableList.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                        String newFieldName = FieldMappingCache.replaceTableFiled(collect, fieldName, false);
                        if (!newFieldName.equalsIgnoreCase(fieldName)) {
                            fullCondition_new = fullCondition_new.replaceAll(fieldName + "\\b", newFieldName);

                        }
                    } else {
                        String[] split1 = leftField.split("\\.");
                        String fieldName = split1[0];
                        String newFieldName = FieldMappingCache.replaceTableFiled(tableList, fieldName, false);
                        if (!newFieldName.equalsIgnoreCase(fieldName)) {
                            fullCondition_new = fullCondition_new.replaceAll(fieldName + "\\b", newFieldName);
                        }
                    }

                    if (rightField.contains(".")) {
                        String[] split1 = rightField.split("\\.");
                        String owner = split1[0];
                        String fieldName = split1[1];

                        List<TableBeanHasAlias> collect = tableList.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                        String newFieldName = FieldMappingCache.replaceTableFiled(collect, fieldName, false);
                        if (!newFieldName.equalsIgnoreCase(fieldName)) {
                            fullCondition_new = fullCondition_new.replaceAll(fieldName + "\\b", newFieldName);
                        }
                    } else {
                        String[] split1 = rightField.split("\\.");
                        String fieldName = split1[0];
                        String newFieldName = FieldMappingCache.replaceTableFiled(tableList, fieldName, false);
                        if (!newFieldName.equalsIgnoreCase(fieldName)) {
                            fullCondition_new = fullCondition_new.replaceAll(fieldName + "\\b", newFieldName);
                        }
                    }

                    final String fullCondition_new_tmp = fullCondition_new;

                    sql_tmp_new_test = sql_tmp_new_test.replace(fullCondition, fullCondition_new_tmp);

                    sqlList = sqlList.stream().map(sql_tmp1 -> sql_tmp1.replace(fullCondition, fullCondition_new_tmp)).collect(Collectors.toList());
                }


                //不处理子查询
                if (type.equalsIgnoreCase("select")) {

                    final int selectIndex = sql_tmp_lower.indexOf("select");
                    final int fromIndex = sql_tmp_lower.lastIndexOf("from");
                    if (selectIndex > -1 && fromIndex > -1) {


                        final String substring = sql_tmp.substring(selectIndex + 6, fromIndex);
                        final String[] split = substring.split(",");
                        //需要替换的字符串
                        List<String> replaceSting = new ArrayList<>();
                        for (String sqlSelectField : split) {
                            //普通查询(就是一个列明) as  别名
                            Matcher matcher1 = Pattern.compile("^\\s*(?:([a-zA-Z_][a-zA-Z0-9_]*)\\.)?\\s*([a-zA-Z_][a-zA-Z0-9_]*)(?:\\s*(?:as)?\\s+([a-zA-Z_][a-zA-Z0-9_]*))?\\s*$", Pattern.CASE_INSENSITIVE).matcher(sqlSelectField);
                            if (matcher1.find()) {
                                String owner = matcher1.group(1);
                                String fieldName = matcher1.group(2);
                                String aliasName = matcher1.group(3);

                                if (matcher1.group(1) == null) {
                                    String newFieldName = FieldMappingCache.replaceTableFiled(tableList, fieldName, false);
                                    if (!newFieldName.equalsIgnoreCase(fieldName)) {
                                        String sqlSelectField_new = sqlSelectField.replaceAll(fieldName + "\\b", newFieldName);
                                        if (aliasName == null && transMap == null) {
                                            //查询字段的替换，暂时不添加别名
                                            sqlSelectField_new += " as " + fieldName + " ";
                                        }

                                        final String sqlSelectField_new_tmp = sqlSelectField_new;

                                        transMap.put(sqlSelectField.toLowerCase().trim(), sqlSelectField_new_tmp.toLowerCase().trim());

                                        sql_tmp_new_test = sql_tmp_new_test.replace(sqlSelectField, sqlSelectField_new_tmp);

                                        sqlList = sqlList.stream().map(sql_tmp1 -> sql_tmp1.replace(sqlSelectField, sqlSelectField_new_tmp)).collect(Collectors.toList());
                                    }
                                } else {
                                    List<TableBeanHasAlias> collect = tableList.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                                    String newFieldName = FieldMappingCache.replaceTableFiled(collect, fieldName, false);
                                    if (!newFieldName.equalsIgnoreCase(fieldName)) {
                                        String sqlSelectField_new = sqlSelectField.replaceAll(fieldName + "\\b", newFieldName);
                                        if (aliasName == null && transMap == null) {
                                            //查询字段的替换，暂时不添加别名
                                            sqlSelectField_new += " as " + fieldName + " ";
                                        }
                                        final String sqlSelectField_new_tmp = sqlSelectField_new;
                                        sql_tmp_new_test = sql_tmp_new_test.replace(sqlSelectField, sqlSelectField_new_tmp);
                                        transMap.put(sqlSelectField.toLowerCase().trim(), sqlSelectField_new_tmp.toLowerCase().trim());


                                        sqlList = sqlList.stream().map(sql_tmp1 -> sql_tmp1.replace(sqlSelectField, sqlSelectField_new_tmp)).collect(Collectors.toList());

                                    }
                                }
                            }
                        }


                        //order by
                        int oderby = -1;
                        Matcher matcher_orderby = Pattern.compile("\\s+order\\s+by\\s+", Pattern.CASE_INSENSITIVE).matcher(sql_tmp);
                        while (matcher_orderby.find()) {
                            oderby = matcher_orderby.end();
                        }

                        if (oderby > -1) {
                            String orderby = sql_tmp.substring(oderby);
                            String orderby_old = orderby;

                            final String[] split_tt = orderby.split(",");
                            for (String order_by : split_tt) {

                                Matcher orderField = Pattern.compile("\\s*([a-zA-Z_][a-zA-Z0-9_]*\\.)?([a-zA-Z_][a-zA-Z0-9_]*)\\s*(asc|desc)?", Pattern.CASE_INSENSITIVE).matcher(order_by);

                                if (orderField.find()) {
                                    String owner = orderField.group(1);
                                    String fieldName = orderField.group(2);
                                    if (owner != null) {
                                        List<TableBeanHasAlias> collect = tableList.stream().filter(tableBeanHasAlias -> owner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || owner.equalsIgnoreCase(tableBeanHasAlias.getTableName())).collect(Collectors.toList());
                                        String newFieldName = FieldMappingCache.replaceTableFiled(collect, fieldName, false);
                                        if (!newFieldName.equalsIgnoreCase(fieldName)) {
                                            String sqlSelectField_new = order_by.replaceAll(fieldName + "\\b", newFieldName);
                                            orderby = orderby.replaceFirst(order_by, sqlSelectField_new);
                                        }
                                    } else {
                                        String newFieldName = FieldMappingCache.replaceTableFiled(tableList, fieldName, false);
                                        if (!newFieldName.equalsIgnoreCase(fieldName)) {
                                            String sqlSelectField_new = order_by.replaceAll(fieldName + "\\b", newFieldName);
                                            orderby = orderby.replaceFirst(order_by, sqlSelectField_new);
                                        }
                                    }
                                }
                            }
                            final String orderby_tmp = orderby;
                            sql_tmp_new_test = sql_tmp_new_test.replace(orderby_old, orderby_tmp);
                            sqlList = sqlList.stream().map(sql_tmp1 -> sql_tmp1.replace(orderby_old, orderby_tmp)).collect(Collectors.toList());

                        }


                    }


                    //(insert update delete 只处理简单的单表)
                } else if (type.equalsIgnoreCase("delete")) {

                    String sql = "DELETE FROM table_name WHERE condition;";

                    // 解析 DELETE 语句
                    Pattern pattern_delete = Pattern.compile("\\b([a-zA-Z_][a-zA-Z0-9_])*\\b", Pattern.CASE_INSENSITIVE);
                    Matcher matcher_delete = pattern_delete.matcher(sql);

                    if (matcher_delete.find()) {
                        final String tablename = matcher_delete.group(1);
                        final List<TableBeanHasAlias> collect = tableList.stream().filter(tableBeanHasAlias -> tableBeanHasAlias.getTableName().equalsIgnoreCase(tablename)).collect(Collectors.toList());
                        if (collect.size() > 0) {


                        }
                    }


                } else if (type.equalsIgnoreCase("update")) {

                    // 解析 UPDATE 语句
                    Pattern pattern_update = Pattern.compile("UPDATE\\s+([a-zA-Z_][a-zA-Z0-9_]*)\\s+SET\\s+([^;]+)\\s*WHERE\\s+([^;]+)", Pattern.CASE_INSENSITIVE);
                    Matcher matcher_update = pattern_update.matcher(sql_tmp);
                    if (matcher_update.find()) {
                        String tableName = matcher_update.group(1);
                        String setClause = matcher_update.group(2);
                        String condition = matcher_update.group(3);

                        logger.info("Table Name: " + tableName);
                        logger.info("Set Clause: " + setClause);
                        logger.info("Condition: " + condition);

                        list.stream().map(sql -> sql.replaceAll("\\s+" + tableName + "\\s+", FieldMappingCache.replaceTable(tableName))).collect(Collectors.toList());
                    }

                } else if (type.equalsIgnoreCase("insert")) {


                    Pattern pattern_insert = Pattern.compile("\\bINSERT\\s+INTO\\s+([a-zA-Z_][a-zA-Z0-9_]*)\\s*\\((.*?)\\)\\s*VALUES", Pattern.CASE_INSENSITIVE);

                    Matcher matcher_insert = pattern_insert.matcher(sql_tmp);
                    if (matcher_insert.find()) {
                        String tableName = matcher_insert.group(1).trim();
                        String fieldNames = matcher_insert.group(2);
                        final String[] fields = fieldNames.split(",");

                        for (String fieldName : fields) {
                            //list.stream().map(sql -> sql.replaceAll("\\s+"+fieldName+"\\s+",FieldMappingCache.replaceTableFiled(tableName, fieldName))).collect(Collectors.toList());
                        }


                        //插入语句默认只处理单表，替换表明
                        // list.stream().map(sql -> sql.replaceAll("\\s+"+tableName+"\\s+",FieldMappingCache.replaceTable(tableName, "", ""))).collect(Collectors.toList());
                    }
                }


                //替换表名
                sqlList = sqlList.stream().map(sql_tmp1 -> {
//                        Matcher tableMatcher = Pattern.compile("(?:\\s+|,)?(([a-zA-Z_][a-zA-Z0-9_]*)(?:\\s+as)?(\\s+[a-zA-Z][a-zA-Z0-9_]*)?(?!\\s+hrmresource\\b)(?:\\s+|,)?)", Pattern.CASE_INSENSITIVE).matcher(sql_tmp1);
                    Matcher tableMatcher = Pattern.compile("(\\s*(?:\\bFROM\\b|\\bJOIN\\b|\\bUPDATE\\b)?,?\\s*([a-zA-Z_][a-zA-Z0-9_]*)(?:\\s*\\.)?(?:(?:\\s+as|\\s*)?(\\b[a-zA-Z][a-zA-Z0-9_]*\\b))?)", Pattern.CASE_INSENSITIVE).matcher(sql_tmp1);

                    //from hrmresource aaa
                    //from hrmresource where
                    //from hrmresource  aa ,hrmdepartment aa
                    //from hrmresource   ,hrmdepartment aa
                    //from hrmresource   ,hrmdepartment
                    //join hrmresource   ,hrmdepartment
                    //update hrmresource

                    while (tableMatcher.find()) {
                        //匹配的表明整体
                        final String group = tableMatcher.group(1);

                        final String tableName = tableMatcher.group(2);

                        String aliasName = tableMatcher.group(3);

                        if (aliasName != null) {
                            if ("where".equalsIgnoreCase(aliasName.trim())
                                    || "order".equalsIgnoreCase(aliasName.trim())
                                    || "group".equalsIgnoreCase(aliasName.trim())
                                    || "set".equalsIgnoreCase(aliasName.trim())
                            ) {
                                aliasName = null;
                            }
                        }


                        final List<TableBeanHasAlias> collect = tableList.stream().filter(tableBeanHasAlias -> tableBeanHasAlias.getTableName().equalsIgnoreCase(tableName)).collect(Collectors.toList());
                        if (collect.size() > 0) {
                            if (aliasName == null && !type.equalsIgnoreCase("insert")) {
                                String new_tableName = FieldMappingCache.replaceTable(tableName);
                                if (!new_tableName.equalsIgnoreCase(tableName)) {
                                    String group_new = group.replace(tableName, new_tableName + " as  " + tableName);
                                    sql_tmp1 = sql_tmp1.replaceFirst(group, group_new);

                                }
                            } else {
                                String new_tableName = FieldMappingCache.replaceTable(tableName);
                                if (!new_tableName.equalsIgnoreCase(tableName)) {
                                    String group_new = group.replaceFirst(tableName, new_tableName);
                                    sql_tmp1 = sql_tmp1.replaceFirst(group, group_new);
                                }
                            }
                        }
                    }
                    return sql_tmp1;
                }).collect(Collectors.toList());
            }


            {
                final Set<String> keySet = holderMap_tmp.keySet();
                for (String key : keySet) {
                    sql_tmp_new_test = sql_tmp_new_test.replace(key, holderMap_tmp.get(key));
                }
                logger.info("打印最终sql开始");
                logger.info(sql_tmp_new_test);
                logger.info("打印最终sql结束");
            }

            logger.info("分别打印替换的list");

            //还原编号
            sqlList = sqlList.stream().map(sql_tmp1 -> {
                final Set<String> keySet = holderMap_tmp.keySet();
                for (String key : keySet) {
                    sql_tmp1 = sql_tmp1.replace(key, holderMap_tmp.get(key));
                }
                logger.info(sql_tmp1);
                if (SqlCache.getDbType().equals("sqlserver")) {
                    sql_tmp1 = sql_tmp1.replace("eteams.", "eteams.dbo.");
                }
                return sql_tmp1;
            }).collect(Collectors.toList());


            return sqlList;

        } catch (Exception e) {

            logger.error("sql",e);

            return list;
        }

    }


    static class KeywordIndex {
        private String keyword;
        private int index;

        public KeywordIndex(String keyword, int endIndex) {
            this.keyword = keyword;
            this.index = endIndex;
        }

        // Getter 方法

        public String getKeyword() {
            return keyword;
        }


    }

    /**
     * 先将sql中的占位符用随机数代替
     *
     * @param sql
     * @return
     */
    public static Map<String, Object> replaceAllHolder(String sql) {
        Map<String, Object> result = new HashMap<>();
        Map<String, String> holderNum = new HashMap<>();
        List<String> holderList = new ArrayList<>();

        String sqlNew = sql;
        // 取出$$这种占位符参数
        String regex1 = "\\$[A-Za-z0-9\\_\\.]+\\$";
        Matcher matcher1 = Pattern.compile(regex1).matcher(sql);
        while (matcher1.find()) {
            String group = matcher1.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sql = sql.replace(group, "_ttttf");
            }
        }


        // 取出#{?userid}这种系统参数
//        String regex2 = "\\{[A-Za-z0-9\\_\\.\\?]+\\}";
        String regex3 = "#\\{[A-Za-z0-9_\\.\\?\\u4e00-\\u9fa5]+\\}";
        Matcher matcher3 = Pattern.compile(regex3).matcher(sql);
        while (matcher3.find()) {
            String group = matcher3.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sql = sql.replace(group, "_ttttf");

            }
        }


        // 取出{?userid}这种系统参数
//        String regex2 = "\\{[A-Za-z0-9\\_\\.\\?]+\\}";
        String regex2 = "\\{[A-Za-z0-9_\\.\\?\\u4e00-\\u9fa5]+\\}";
        Matcher matcher2 = Pattern.compile(regex2).matcher(sql);
        while (matcher2.find()) {
            String group = matcher2.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
                sql = sql.replace(group, "_ttttf");
            }
        }


        List<String> numList = new ArrayList<>();

        for (String holder : holderList) {
            String num = TransUtil.generateSnowId();
            numList.add(num);
            sqlNew = sqlNew.replace(holder, num);
            holderNum.put(num, holder);
        }
        {
            String num = TransUtil.generateSnowId();
            numList.add(num);
            sqlNew = sqlNew.replace("?", num);
            holderNum.put(num, "?");
        }


        result.put("sqlNew", sqlNew);
        result.put("holderNum", holderNum);
        sqlResultInfo.put("numList", numList);
        return result;
    }





}
