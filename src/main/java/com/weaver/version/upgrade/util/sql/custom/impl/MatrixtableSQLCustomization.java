package com.weaver.version.upgrade.util.sql.custom.impl;

import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.custom.SQLCustomization;
import com.weaver.version.upgrade.util.sql.util.TransUtil;

import java.util.HashMap;
import java.util.List;

/**
 * @author wujiahao
 * @date 2024/12/12 下午4:31
 */
public class MatrixtableSQLCustomization implements SQLCustomization {
    @Override
    public String transformTableName(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        String newTableName = FieldMappingCache.buildMatrixE10sql(usedColumns, sourceTableName);
        if (newTableName != null) {
            TransUtil.addTransMsg("将E9表" + sourceTableName + "替换为子查询=>" + newTableName);
        }
        return newTableName;
    }

    @Override
    public String transformJoinTable(String e9tableName, HashMap<String, String> conditionMap, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformJoinStatement(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformColumnName(String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformConditionValue(String sourceValue, String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }
}
