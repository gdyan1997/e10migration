package com.weaver.version.upgrade.util.sql.cache;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.weaver.version.upgrade.util.sql.annotation.FieldInfo;
import com.weaver.version.upgrade.util.sql.bean.DataConnBeanChild;
import com.weaver.version.upgrade.util.sql.bean.FdLinkageSqlConvertBean;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBean;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBeanDetail;
import com.weaver.version.upgrade.util.sql.enums.ServerNameEnum;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import com.weaver.versionupgrade.customRest.db.DBUtil;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
//import com.weaver.versionupgrade.util.PropertiesUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.DBCacheUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil;
import com.weaver.versionupgrade.e10Migration.modules.cube.util.ValidObjCacheUtil;
import com.weaver.versionupgrade.util.PropertiesUtil;
import com.weaver.versionupgrade.util.decoder.decoderUtil;
import general.SecurityHelper;
import idtoolutil.Constant;
import idtoolutil.E10NewIdUtil;
import idtoolutil.IdGenerator;
import idtoolutil.NumberEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import weaver.general.BaseBean;
import weaver.general.Util;
import weaver.init.InitWeaverServer;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.tablefiledMappingMap;
import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.workflowTableMap;
import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.e9SysFieldSet;
import static com.weaver.version.upgrade.util.sql.enums.ServerNameEnum.workJosnArrary;
import static com.weaver.versionupgrade.e10Migration.modules.cube.util.E10CubeUtil.getE10MainTablename;

/**
 * @author wujiahao
 * @date 2024/1/10 11:13
 */
@Component
public class SqlCache {


    @Autowired
    private Environment environment_tmp;

    private static Environment environment;

    public static void FdLinkageSqlConvert(FdLinkageSqlConvertBean fdLinkageSqlConvertBean) {
    }

 private static final Logger logger = LoggerFactory.getLogger(SqlCache.class);

    @PostConstruct
    void init() {
        environment = environment_tmp;
    }


    public static synchronized void clearCache() {
        //清除表映射关系
        FieldMappingCache.tablefiledMappingMap = null;

        if (FieldMappingCache.workflowTableMap != null) {
            FieldMappingCache.workflowTableMap.remove(TransUtil.getTenantKey());
        }
        //清除矩阵缓存
        FieldMappingCache.matrixFieldIdMapping.clear();
    }


    /**
     * 初始化迁移字段对应关系
     */
    public static synchronized void initCache() {


        //缓存字段映射关系
        if (tablefiledMappingMap == null) {
            cacheFieldMapping();
        }
        if (workflowTableMap == null || !workflowTableMap.containsKey(TransUtil.getTenantKey())) {
            HashSet<String> tableFieldSet = new HashSet<>();

            new BaseBean().writeLog("初始化缓存");
            //缓存流程主表
            cacheWorkFlowTable(tableFieldSet);
            cacheSysAlltabel(tableFieldSet);
            //缓存矩阵信息
            initMatrixFieldInfo();
            //放入缓存
            workflowTableMap.put(TransUtil.getTenantKey(), tableFieldSet);
            new BaseBean().writeLog("初始化缓存结束="+workflowTableMap.size());
        }


    }

    private static void cacheSysAlltabel(HashSet<String> tableFieldSet) {


 

        //dataSync
        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        String sql = weaver.rule.RuleCheckUtil.getAllTableColumns(sourceDBUtil.getDbtype());
        sourceDBUtil.executeQuery(sql);

        while (sourceDBUtil.next()) {
            String e9_tablename = TransUtil.null2String(sourceDBUtil.getString("tablename")).trim().toLowerCase();
            String e9_fieldname = TransUtil.null2String(sourceDBUtil.getString("fieldname")).trim().toLowerCase();
            tableFieldSet.add(e9_tablename + "|sys|" + e9_fieldname);
            //存一份表的对应关系
            tableFieldSet.add(e9_tablename + "|sys|");

        }


    }

    /**
     * 获取矩阵字段信息
     *
     * @param ruleid
     * @param matrixid
     * @return
     */
    public static void initMatrixFieldInfo() {




        //dataSync

        Map<String, String> resultMap = new ConcurrentHashMap<>();
        SourceDBUtil rs = new SourceDBUtil();
        //获取矩阵字段和id对应关系
        Map<String, String> map = new HashMap<>();
        SourceDBUtil rs2 = new SourceDBUtil();
        rs.executeQuery("select browsertypeid, fieldname, fieldtype, t1.id as id , browservalue, matrixid,ISSYSTEM\n" +
                "from matrixfieldinfo t1\n" +
                "         left join matrixinfo t2 on t1.MATRIXID = t2.id\n");
        while (rs.next()) {
            String fieldname = TransUtil.null2String(rs.getString("fieldname")).trim().toLowerCase();
            String e9fieldid = TransUtil.null2String(rs.getString("id"));
            String matrixid = TransUtil.null2String(rs.getString("matrixid"));
            String fieldtype = TransUtil.null2String(rs.getString("fieldtype"));
            String ISSYSTEM = TransUtil.null2String(rs.getString("ISSYSTEM"));
            //获取E9部门和分部字段id
            if (ISSYSTEM.equals("1")) {
                rs2.executeQuery("select  a.fieldid from  hrm_formfield a left join htmllabelinfo b on a.fieldlabel=b.indexid left join  hrm_fieldgroup c on c.id=a.groupid where b.languageid=7 and c.grouptype in(4) and (a.issystem is null or a.issystem=0) and a.fieldname=?"
                        , fieldname);
                if (rs2.next()) {
                    e9fieldid = TransUtil.null2String(rs2.getString("fieldid"));
                }

                resultMap.put(TransUtil.getTenantKey()+matrixid.toLowerCase() + "$system", "");
            }
            if (ISSYSTEM.equals("2")) {
                rs2.executeQuery("select  a.fieldid from  hrm_formfield a left join htmllabelinfo b on a.fieldlabel=b.indexid left join  hrm_fieldgroup c on c.id=a.groupid where b.languageid=7 and c.grouptype in(5) and (a.issystem is null or a.issystem=0) and a.fieldname=?"
                        , fieldname);
                if (rs2.next()) {
                    e9fieldid = TransUtil.null2String(rs2.getString("fieldid"));
                }
                resultMap.put(TransUtil.getTenantKey()+matrixid.toLowerCase() + "$system", "");
            }
            resultMap.put(TransUtil.getTenantKey()+matrixid + "_" + fieldname, e9fieldid + "_" + fieldtype);
        }


        FieldMappingCache.matrixFieldIdMapping.putAll(resultMap);



        rs2.executeQuery("select t2.fieldname, t1.scopeid,HRM_FIELDLABLE, t3.FORMLABEL\n" +
                "from cus_formfield t1\n" +
                "         left join\n" +
                "     cus_formdict t2 on t1.fieldid = t2.id\n" +
                "         left join\n" +
                "     cus_treeform t3 on t1.SCOPEID = t3.id\n" +
                "where t1.SCOPE in ('HrmCustomFieldByInfoType')\n" +
                "order by fieldname asc");
        while (rs2.next()){
            Map map_new = (Map) Optional.ofNullable(FieldMappingCache.matrixFieldIdMapping.get(TransUtil.getTenantKey() + "_cus_" + rs2.getString(1))).orElse(new HashMap<>());
            String scopeid = rs2.getString(2);
            String hrm_fieldlable = rs2.getString(3);
            String formlabel = rs2.getString(4);
            HashMap<String,String> fieldInfo= new HashMap<>();
            fieldInfo.put("hrm_fieldlable", hrm_fieldlable);
            fieldInfo.put("formlabel", Util.formatMultiLang(formlabel));
            map_new.put(scopeid,fieldInfo);
            FieldMappingCache.matrixFieldIdMapping.put(TransUtil.getTenantKey() + "_cus_" + rs2.getString(1),map_new);
        }

//        //是否开启了多租户
//        List<Map<String, Object>> list_tmp_2 = I18nContextUtil.getBean(CustomSqlDao.class).executeSelectSql("select  e9_fieldname,e9_tablename\n" +
//                "from upgrade_formfield_info\n" +
//                "where  source_module = 'deployType'  and tenant_key='"+ SecurityUtil.ecodeForSql(TransUtil.getTenantKey())+"'");
//        // 使用 Stream API 将 List 转换为 Map（以 e9_fieldname 作为键，整个 Map 对象作为值）
//        Map<String, String> resultMap_2 = list_tmp_2.stream()
//                .collect(Collectors.toMap(map ->  TransUtil.getTenantKey()+"deployType", map -> map.get("e9_tablename").toString()));


      //是否开启了多租户
/*        List<Map<String, Object>> list_microservicesname = I18nContextUtil.getBean(CustomSqlDao.class).executeSelectSql("select e9_fieldname,e9_tablename\n" +
                "from upgrade_formfield_info where source_module='microservicesname'  and tenant_key='" + SecurityUtil.ecodeForSql(TransUtil.getTenantKey()) + "'");
        // 使用 Stream API 将 List 转换为 Map（以 e9_fieldname 作为键，整个 Map 对象作为值）
        if (list_microservicesname.size() > 0) {
            Map<String, String> resultMap__microservicesname = list_microservicesname.stream()
                    .collect(Collectors.toMap(map -> TransUtil.null2String(map.get("e9_fieldname")), map -> TransUtil.null2String(map.get("e9_tablename"))));
            FieldMappingCache.matrixFieldIdMapping.putAll(resultMap__microservicesname);
        }*/

        SourceDBUtil sourceDBUtil=new SourceDBUtil();

        sourceDBUtil.executeQuery("select v_key,v_value\n" +
                "from e10transclazzvarmap where v_key like 'databasename&%'");

        while (sourceDBUtil.next()){
            String v_key = sourceDBUtil.getString(1);
            String v_value = sourceDBUtil.getString(2);
            String[] split = v_key.split("&");
            if (split.length>1){
                FieldMappingCache.matrixFieldIdMapping.put(split[1].toLowerCase(),v_value);
            }
        }

		FieldMappingCache.matrixFieldIdMapping.put(TransUtil.getTenantKey()+"deployType", ValidObjCacheUtil.getDeployType()+"");

    }


    private static void cacheFieldMapping() {

/*
        //version
        List<MigrateTableMappingBean> tableFieldInfo = migrateTableMappingDao.getTableFieldMapping();
        tablefiledMappingMap = tableFieldInfo.stream()
                .collect(Collectors.groupingBy(MigrateTableMappingBean::getE9Table));
*/

/*

        PropertiesUtil propertiesUtil = new PropertiesUtil();
        String confFileName = "sqlreplace.properties";
        String propertiespath = ConfigUtil.getConfigDir() + confFileName;
        propertiesUtil.load(new File(propertiespath));
        //dataSync

        //初始化需要的配置与执行环境
        String fieldName = propertiesUtil.get("e9toe10fieldmapping");
*/


        final String fieldName = new BaseBean().getPropValue("sqlreplace", "e9toe10fieldmapping");

        //tablefiledMappingMap = JSONObject.parseObject(fieldName, new TypeReference<Map<String, List<MigrateTableMappingBean>>>() {
        //});


        final String e9sysfield = new BaseBean().getPropValue("sqlreplace", "e9sysfield");

        //缓存E9标准字段
        e9SysFieldSet = JSONObject.parseObject(e9sysfield, new TypeReference<HashSet<String>>() {
        });

        new SqlCache().initFieldMapping();
    }


    public void initFieldMapping() {
        //String sqlReplace = InitConfig.getConfigFile("sqlreplace.xlsx");

        String sqlReplace = ConfigUtil.getConfigFile("sqlreplace.xlsx");

        //String sqlReplace = "C:\\work\\E10Upgrade\\datasyn\\DataSync\\src\\main\\resources\\sqlreplace.xlsx";
        try (FileInputStream fis = new FileInputStream(sqlReplace); Workbook workbook = new XSSFWorkbook(fis);) {
            Sheet sheet = workbook.getSheetAt(0);
            int lastRowNum = sheet.getLastRowNum();
            Map<String, Integer> columnNumber = new HashMap<>();
            Row row_tmp = sheet.getRow(1);
            for (int num = row_tmp.getFirstCellNum(); num <= row_tmp.getLastCellNum(); num++) {
                Cell cell = row_tmp.getCell(num);
                if (cell != null) {
                    columnNumber.put(cell.toString(), num);
                }
            }
            List<CellRangeAddress> mergedRegions = sheet.getMergedRegions();
            //List<MigrateTableMappingBean> list = new ArrayList<>();


            Map<String, MigrateTableMappingBean> tableMappingBeanHashMap_tmp = new HashMap<>();

            for (int i = 2; i < lastRowNum; i++) {
                Row row = sheet.getRow(i);
                if (row == null) {
                    continue;
                }


                MigrateTableMappingBeanDetail migrateTableMappingBeanDetail = new MigrateTableMappingBeanDetail();
                //明细表字段
                Field[] fieldsDetail = MigrateTableMappingBeanDetail.class.getDeclaredFields();
                for (Field fieldAnnotation : fieldsDetail) {
                    FieldInfo annotation = fieldAnnotation.getAnnotation(FieldInfo.class);
                    if (annotation == null) {
                        continue;
                    }
                    String name = annotation.name();
                    Integer number = columnNumber.get(name);
                    String fieldName = fieldAnnotation.getName();

//                    System.out.println(number);
//                    System.out.println(name);
//                    System.out.println(fieldName);
                    Cell cell = row.getCell(number);
                    if (cell == null) {
                        continue;
                    }
                    String stringCellValue = TransUtil.null2String(getMergedCellValue(sheet, mergedRegions, cell));

                    // 获取真实字段名 通过反射设置属性
                    Field field = MigrateTableMappingBeanDetail.class.getDeclaredField(fieldName);
                    field.setAccessible(true); // 设置私有属性可访问
                    //处理下拉框转换值
                    boolean select = annotation.isSelect();
                    if (select) {
                        String mappingJson = annotation.mappingJson().toLowerCase();
                        JSONObject jsonObject = JSONObject.parseObject(mappingJson);
                        if (jsonObject.containsKey(stringCellValue.toLowerCase())) {
                            stringCellValue = jsonObject.getString(stringCellValue);
                        }
                    }
                    field.set(migrateTableMappingBeanDetail, stringCellValue); // 设置属性值
                }


                //初始化主表
                MigrateTableMappingBean tableMappingBean = new MigrateTableMappingBean();
                Field[] fields = MigrateTableMappingBean.class.getDeclaredFields(); // 获取实体类的所有属性，返回Field数组
                for (Field fieldAnnotation : fields) {
                    FieldInfo annotation = fieldAnnotation.getAnnotation(FieldInfo.class);
                    if (annotation == null) {
                        continue;
                    }
                    String name = annotation.name();
                    Integer number = columnNumber.get(name);
                    String fieldName = fieldAnnotation.getName();

//                    System.out.println(number);
//                    System.out.println(name);
//                    System.out.println(fieldName);
                    Cell cell = row.getCell(number);

                    if (cell == null) {
                        continue;
                    }
                    String stringCellValue = TransUtil.null2String(getMergedCellValue(sheet, mergedRegions, cell));


                    // 获取真实字段名 通过反射设置属性
                    Field field = MigrateTableMappingBean.class.getDeclaredField(fieldName);
                    field.setAccessible(true); // 设置私有属性可访问
                    //处理下拉框转换值
                    boolean select = annotation.isSelect();
                    if (select) {
                        String mappingJson = annotation.mappingJson().toLowerCase();
                        JSONObject jsonObject = JSONObject.parseObject(mappingJson);
                        if (jsonObject.containsKey(stringCellValue)) {
                            stringCellValue = jsonObject.getString(stringCellValue.toLowerCase());
                        }
                    }
                    field.set(tableMappingBean, stringCellValue); // 设置属性值
                }
                MigrateTableMappingBean migrateTableMappingBean = tableMappingBeanHashMap_tmp.getOrDefault(tableMappingBean.getE9Table().toLowerCase(), tableMappingBean);
                List<MigrateTableMappingBeanDetail> migrateTableMappingBeanDetails = migrateTableMappingBean.getMigrateTableMappingBeanDetails();
                if (migrateTableMappingBeanDetails == null) {
                    migrateTableMappingBeanDetails = new ArrayList<>();
                    migrateTableMappingBean.setMigrateTableMappingBeanDetails(migrateTableMappingBeanDetails);
                }
                //存在明细表，否则不给值
                if (migrateTableMappingBeanDetail.getE9TableField() != null) {
                    migrateTableMappingBeanDetails.add(migrateTableMappingBeanDetail);
                }
                tableMappingBeanHashMap_tmp.put(tableMappingBean.getE9Table().toLowerCase(), migrateTableMappingBean);

            }


            tablefiledMappingMap = tableMappingBeanHashMap_tmp;
            //System.out.println(JSONObject.toJSONString(tablefiledMappingMap));
        } catch (Exception e) {
            logger.error("sql", e);
        }
    }
    /**
     * 获取单元格的值。如果单元格是合并单元格的一部分，返回合并单元格的第一个单元格的值。
     *
     * @param sheet         当前工作表
     * @param mergedRegions 合并单元格的列表
     * @param cell          当前单元格
     * @return 单元格的值
     */
    private static String getMergedCellValue(Sheet sheet, List<CellRangeAddress> mergedRegions, Cell cell) {
        String stringCellValue="";
        for (CellRangeAddress range : mergedRegions) {
            if (range.isInRange(cell.getRowIndex(), cell.getColumnIndex())) {
                // 如果单元格在合并区域中，返回合并区域第一个单元格的值
                Row firstRow = sheet.getRow(range.getFirstRow());
                Cell firstCell = firstRow.getCell(range.getFirstColumn());
                stringCellValue= getCellValue(firstCell);
                break;
            }
        }

        if (StringUtils.isBlank(stringCellValue)) {
            // 如果不是合并单元格，直接返回单元格的值
            stringCellValue=getCellValue(cell);
        }
        //转为小写
        if (!TransUtil.isCusSQL(stringCellValue)) {
            stringCellValue = stringCellValue.toLowerCase();
        }
        return stringCellValue;
    }

    /**
     * 获取单元格的值，处理不同类型的单元格
     *
     * @param cell 单元格
     * @return 单元格的值
     */
    private static String getCellValue(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue().trim();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return cell.getDateCellValue().toString().trim();
                } else {
                    return String.valueOf(cell.getNumericCellValue()).trim();
                }
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue()).trim();
            case FORMULA:
                return cell.getCellFormula();
            case BLANK:
                return "";
            default:
                return "UNKNOWN";
        }
    }

    public static void main(String[] args) {
        try {
            new SqlCache().initFieldMapping();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void cacheWorkFlowTable(HashSet<String> tableFieldSet) {





        //datasync
        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        sourceDBUtil.executeQuery("select t1.workflowname, t2.tablename, t3.tablename\n" +
                "from workflow_base t1\n" +
                "         left join workflow_bill t2 on t1.isbill = 1 and t1.formid = t2.id\n" +
                "         left join workflow_billdetailtable t3  on  t3.billid = t2.id");

        while (sourceDBUtil.next()) {
            if (StringUtils.isNotBlank(sourceDBUtil.getString(2))) {
                tableFieldSet.add(sourceDBUtil.getString(2).toLowerCase() + "|wf|");
            }
            if (StringUtils.isNotBlank(sourceDBUtil.getString(3))) {
                tableFieldSet.add(sourceDBUtil.getString(3).toLowerCase() + "|wf_det|");
            }
        }
        sourceDBUtil.executeQuery("select t1.modename, t2.tablename, t3.tablename,t1.isdelete\n" +
                "from modeinfo t1\n" +
                "         left join workflow_bill t2 on t1.formid = t2.id\n" +
                "         left join workflow_billdetailtable t3  on  t3.billid = t2.id\n" +
                "where FORMID not in (select FORMID from modeformextend)");
        while (sourceDBUtil.next()) {

            if (sourceDBUtil.getString("isdelete").equals("1")) {
                continue;
            }

            if (StringUtils.isNotBlank(sourceDBUtil.getString(2))) {
                tableFieldSet.add(sourceDBUtil.getString(2).toLowerCase() + "|cube|");
            }
            if (StringUtils.isNotBlank(sourceDBUtil.getString(3))) {
                tableFieldSet.add(sourceDBUtil.getString(3).toLowerCase() + "|cube_det|");
            }
        }
        sourceDBUtil.executeQuery("select t1.id, t2.tablename, t3.tablename\n" +
                "from edc_form_page t1\n" +
                "         left join workflow_bill t2 on t1.formid = t2.id\n" +
                "         left join workflow_billdetailtable t3  on  t3.billid = t2.id");
        while (sourceDBUtil.next()) {
            if (StringUtils.isNotBlank(sourceDBUtil.getString(2))) {
                tableFieldSet.add(sourceDBUtil.getString(2).toLowerCase() + "|edc|");
            }
            if (StringUtils.isNotBlank(sourceDBUtil.getString(3))) {
                tableFieldSet.add(sourceDBUtil.getString(3).toLowerCase() + "|edc_det|");
            }
        }


    }

    public static String getNewTableID(String tableName, String fieldName, String oldTableName, String oldContent) {
        // return weaver.trans.base.TransUtil.getNewTableID( tableName, fieldName, oldTableName, oldContent);


        if (E10NewIdUtil.isUseE9idToE10id(tableName, oldTableName)) {
            return IdGenerator.transE10PrimaryInfo(tableName, oldTableName, oldContent);
        }

        return oldContent;
    }


    public static volatile String tenant_key = null;

    public  synchronized static String getTenantKey() {

        if (tenant_key == null) {
            synchronized (SqlCache.class) {
                if (tenant_key == null) {
/*                    PropertiesUtil propertiesUtil = new PropertiesUtil();
                    String confFileName = "sqlreplace.properties";
                    String propertiespath = ConfigUtil.getConfigDir() + confFileName;
                    propertiesUtil.load(new File(propertiespath));
                    String tenant_key_tmp = propertiesUtil.get("tenantkey");*/
                    final String tenant_key_tmp = new BaseBean().getPropValue("tenant", "tenantkey");
                    if (TransUtil.null2String(tenant_key_tmp).equals("")) {
                        tenant_key = "E10租户key";
                    } else {
                        tenant_key = tenant_key_tmp;
                    }

                    if (tenant_key.equalsIgnoreCase("E10租户key")||tenant_key.equalsIgnoreCase("e9toe10test")) {

                        SourceDBUtil sourceDBUtil = new SourceDBUtil();

                        String e9toe10numberinfo = DBUtil.getCheckTableExistSql("finkdb_paramsetting", sourceDBUtil.getDbtype());
                        sourceDBUtil.execute(e9toe10numberinfo);
                        if (sourceDBUtil.next()) {
                            String count = sourceDBUtil.getString("count");
                            if ("1".equalsIgnoreCase(count)){
                                sourceDBUtil.executeQuery("select tenant_key,deploytype from finkdb_paramsetting");
                                if (sourceDBUtil.next()) {
                                    tenant_key = sourceDBUtil.getString("tenant_key");
                                }
                            }
                        }

                    }
                }
            }
        }
        return tenant_key;
    }

    public static String startNum() {

/*        PropertiesUtil propertiesUtil = new PropertiesUtil();
        String confFileName = "sqlreplace.properties";
        String propertiespath = ConfigUtil.getConfigDir() + confFileName;
        propertiesUtil.load(new File(propertiespath));
        String startNum = propertiesUtil.get("startNum");*/

        SourceDBUtil sourceDBUtil = new SourceDBUtil();


        String e9toe10numberinfo = DBUtil.getCheckTableExistSql("e9toe10numberinfo", sourceDBUtil.getDbtype());


        final String startNum = new BaseBean().getPropValue("tenant", "startNum");

        if (TransUtil.null2String(startNum).equals("")||TransUtil.null2String(startNum).equals("10")) {



            sourceDBUtil.execute(e9toe10numberinfo);
            if (sourceDBUtil.next()){
                String count = sourceDBUtil.getString("count");
                if ("1".equalsIgnoreCase(count)){
                    sourceDBUtil.executeQuery("select tenant_key,tenantkeynumber\n" +
                            "from E9toe10numberinfo  where tenantkeynumber is not null");
                    if (sourceDBUtil.next()) {
                        return sourceDBUtil.getString("tenantkeynumber");
                    }
                }
            }

            return "10";
        }
        return startNum;
    }

    static volatile String keyNumber = null;


    public static String getTenantKeyNumber() {


        if (keyNumber == null) {
            synchronized (SqlCache.class) {
                if (keyNumber == null) {
                    SourceDBUtil sourceDBUtil = new SourceDBUtil();

                        sourceDBUtil.executeQuery("select tenant_key,tenantkeynumber\n" + "from E9toe10numberinfo  where tenantkeynumber is not null");
                        if (sourceDBUtil.next()) {
                            keyNumber = sourceDBUtil.getString("tenantkeynumber");
                        } else {
                            keyNumber = "10";
                    }
                }

            }
        }
        return keyNumber;
    }

    public static String generateSnowId() {
        return weaver.idtool.IdGenerator.generateSnowId();
    }


    public static String getDbType() {
        //return WeaDatabaseIdProvider.databaseId;
        return new SourceDBUtil().getDbtype();
    }

  public   static String propValue = null;

    /**
     * 目标数据库类型
     *
     * @return
     */
    public static String getTargetDbType() {
        //return WeaDatabaseIdProvider.databaseId;


        if (propValue == null) {
            BaseBean baseBean = new BaseBean();
            propValue = baseBean.getPropValue("tenant", "targetdbtype").toLowerCase();
        }
        if (StringUtils.isBlank(propValue)) {
            return new SourceDBUtil().getDbtype().toLowerCase();
        } else {
            return propValue.toLowerCase();
        }
    }


    static volatile Map<String, Boolean> dataSourceMap = null;

    /**
     * 根据数据源id，判断是否是自己系统还是外部数据源
     * <p>
     * 1.标识是   空字符串/local/$ECOLOGY_SYS_LOCAL_POOLNAME   直接默认当系统数据源处理
     * 2.数据源中 ip地址为 127.0.0.7/localhost/或和E9数据库中weaver.properties配置ip一致  且查询数据库名称与weaver.properties同一个数据库。当系统数据源处理
     *
     * @param dataSourceId
     * @return
     */
    public static boolean isSystemData(String dataSourceId) {

        try {
            if (dataSourceId == null ||
                    dataSourceId.equals("")
                    || dataSourceId.equalsIgnoreCase("local")
                    || dataSourceId.equalsIgnoreCase("localhost")
                    || dataSourceId.equalsIgnoreCase("$ECOLOGY_SYS_LOCAL_POOLNAME")) {
                return true;
            }
            if (dataSourceMap == null) {
                synchronized (SqlCache.class) {
                    if (dataSourceMap == null) {

                        String ip="127.0.0.1";
                        try {
                            InetAddress inetAddress = InetAddress.getLocalHost();
                            ip = inetAddress.getHostAddress();
                            new BaseBean().writeLog("ip="+ip,8888);
                            System.out.println("ip="+ip);
                        }catch ( Exception e){

                            e.printStackTrace();
                        }


                        SourceDBUtil SourceDBUtil = new SourceDBUtil();
                        Map<String, String> dbInfo = SourceDBUtil.getDBInfo();



                        String linkUrl = "";
                        try {
                            String propertiespath = InitWeaverServer.databaseurl;
                            InputStream is = null;
                            Properties propertiesUtil = new Properties();
                            is = new BufferedInputStream(new FileInputStream(propertiespath));
                            propertiesUtil.load(is);
                            linkUrl = decoderUtil.getEncoderValue(com.weaver.versionupgrade.util.Util.null2String(propertiesUtil.get("ecology.url")));

                        }catch (Exception e){

                        }

                        String dBname = getDatabaseName(linkUrl);



                        SourceDBUtil rs = new SourceDBUtil();
                        rs.executeQuery("select * from datasourcesetting ");
                        Map<String, Boolean> dataSourceMap_tmp = new HashMap<>();
                        while (rs.next()) {
                            boolean flag = false;
                            String POINTID = Util.null2String(rs.getString("POINTID"));// 是否使用url
                            String iscluster = Util.null2String(rs.getString("iscluster"));// 是否使用url
                            String TYPE = Util.null2String(rs.getString("TYPE"));// 是否使用url
                            if ("2".equals(iscluster)) {// url模式,应该可以截取第一个问号前的来比较
                                String url = Util.null2String(rs.getString("url"));// url
                                String urlNew = url;
                                if (urlNew.indexOf("?") > -1) {
                                    urlNew = url.substring(0, url.indexOf("?"));
                                }
                                if (linkUrl.startsWith(urlNew)) {
                                    flag = true;
                                }
                                if (url.indexOf("127.0.0.1") > -1 || url.indexOf("localhost") > -1) {
                                    if (url.indexOf(dBname) > -1) {
                                        flag = true;
                                    } else if ("dm".equals(TYPE)) {
                                        flag = true;
                                    }
                                }
                            } else {
                                String host = Util.null2String(rs.getString("host"));// url
                                String port = Util.null2String(rs.getString("port"));// url
                                String dbname = Util.null2String(rs.getString("dbname"));// url
                                // 判断一下是否是加密数据，要不要解密
                                String key = "ecology";
                                String encodeStr = "/c4Q2hAVXFc=";
                                String db_host = host.indexOf(encodeStr) > -1 ? SecurityHelper.decrypt(key, host) : host;
                                String db_port = port.indexOf(encodeStr) > -1 ? SecurityHelper.decrypt(key, port) : port;
                                String db_dbname = dbname.indexOf(encodeStr) > -1 ? SecurityHelper.decrypt(key, dbname) : dbname;
                                String url = db_host + ":" + db_port + "/" + db_dbname;
                                if (linkUrl.indexOf(url) > -1) {
                                    flag = true;
                                }
                                if (linkUrl.contains(db_host)&&linkUrl.contains(db_port)&&linkUrl.contains(db_dbname)) {
                                    flag = true;
                                }




                                if (db_host.equals(ip) && db_dbname.equals(dBname)) {
                                    flag = true;
                                }

                                if (db_host.equals("127.0.0.1") && db_dbname.equals(dBname)) {
                                    flag = true;
                                }
                                if (db_host.equals("localhost") && db_dbname.equals(dBname)) {
                                    flag = true;
                                }

                                if ("dm".equals(TYPE)) {
                                    if (db_host.equals("127.0.0.1") || db_host.equals("localhost"))
                                        flag = true;
                                }

                            }
                            new BaseBean().writeLog("数据源标识=" + POINTID + " 是否系统数据源=" + flag,8888);
                            System.out.println("数据源标识=" + POINTID + " 是否系统数据源=" + flag);
                            dataSourceMap_tmp.put(POINTID, flag);
                        }
                        dataSourceMap = dataSourceMap_tmp;
                    }
                }

            }
            if (dataSourceMap.containsKey(dataSourceId)) {
                return dataSourceMap.get(dataSourceId);
            }
            return false;
            //return null;
        } catch (Exception e) {
            e.printStackTrace();
            return true ;
        }
    }

    /**
     * 通过 e9 表名  获取可能涉及到的e10 ebuilder表单
     *
     * @param e9_tablename e9表名
     * @Author ygd2020
     * @Date 17:30 2024-1-2
     * @Return key e10 微服务，value List<String> 涉及到的e10 ebuilder 数据库表单名称
     **/
    public static Set<String> getWorkflowE10TablenameByE9Tablename(String e9_tablename) {

        SourceDBUtil rs = new SourceDBUtil();
        String servicename = "";
        Set<String> tables = new HashSet<>();
        // 可能涉及到的主表
        rs.executeQuery("select t2.id , t2.tablename from workflow_base t , workflow_bill t2 where t.formid=t2.id and t.isbill=1 and lower(t2.tablename) = ? ", e9_tablename.toLowerCase());
        while (rs.next()) {
            String e9_formid = rs.getString("id");
            //生成E10表名
            String e10tablename = getTableName(e9_formid, e9_tablename, "1", "0", "");
//            servicename=WorkflowUtil.getServiceName( e9_formid ,1);
            tables.add(e10tablename);
        }

        // 可能涉及到的明细表
        // 可能涉及到的主表
        rs.executeQuery(" select t2.billid ,t2.id, t2.tablename from workflow_base t , workflow_billdetailtable t2 where t.formid=t2.billid and t.isbill=1 and lower(t2.tablename) = ? ", e9_tablename.toLowerCase());
        while (rs.next()) {
            String e9_formid = rs.getString("billid");
            String e9_subformid = rs.getString("id");
//            servicename=WorkflowUtil.getServiceName( e9_formid ,1);
            //生成E10表名
            String e10tablename = getTableName(e9_subformid, e9_tablename, "1", "1", "");
            tables.add(e10tablename);
        }


        return tables;
    }

    /**
     * 生成E10表信息规则
     *
     * @param e9_formid    主表单id 或 明细表id 根据 isdetial 判断 (老表单 只有主表单formid)
     * @param e9_tablename E9 主表数据库名称，或者 E9 明细表数据库名称 (用于非合体环境使用E9原表名)
     * @param isbill       1 是否 自定义表单 或 老表单
     * @param isdetial     1 明细表，反之主表  （isdetail 为1表示明细表，此e9_formid 含义为明细表id，反之代表是主表id）
     * @param groupid      老表单明细的序号 （isbill为1时此参数可不传）
     * @return
     */
    public static String getTableName(String e9_formid, String e9_tablename, String isbill, String isdetial, String groupid) {
        return getTableName(e9_formid, e9_tablename, isbill, isdetial, groupid, "");
    }

    /**
     * 生成E10表信息规则
     *
     * @param e9_formid    主表单id 或 明细表id 根据 isdetial 判断 (老表单 只有主表单formid)
     * @param e9_tablename E9 主表数据库名称，或者 E9 明细表数据库名称 (用于非合体环境使用E9原表名)
     * @param isbill       1 是否 自定义表单 或 老表单
     * @param isdetial     1 明细表，反之主表  （isdetail 为1表示明细表，此e9_formid 含义为明细表id，反之代表是主表id）
     * @param groupid      老表单明细的序号 （isbill为1时此参数可不传）
     * @param suffix_str   后缀，请尽可能精简不超过5位（oracle数据库表名不允许超过30位）
     * @return
     */
    public static String getTableName(String e9_formid, String e9_tablename, String isbill, String isdetial, String groupid, String suffix_str) {
        String tablename = "";
        if (isbill.equals("1")) { //自定义表单
            if (isdetial.equals("1")) {//明细表
                if (ValidObjCacheUtil.getDeployType() == 0) { //本地部署billmxtb
                    tablename = e9_tablename + (!suffix_str.equals("") ? "_" + suffix_str : "");//formtable_main_99999_dt_10_后缀
                } else {
                    //格式：billmxtb_999999_后缀_tch71ow08v
                    tablename = "billmxtb_" + Math.abs(Util.getIntValue(e9_formid)) + (!suffix_str.equals("") ? "_" + suffix_str : "") + "_" + Constant.TENANT; //明细表formid 非主表单id (云部署，防止表名长度问题，自动生成不用原表名)
                }
            } else {
                //主表
                if (ValidObjCacheUtil.getDeployType() == 0) { //本地部署
                    tablename = e9_tablename + (!suffix_str.equals("") ? "_" + suffix_str : "");//formtable_main_9_1000_后缀
                } else {//自定义表单会存在：单据 与 自定义2中情况 一种是 ID 大于0  一种是 ID小于0  所以下面的拼接table需要不一样避免重复。比如：-100 或 100 生成table会有重复
                    if (Util.getIntValue(e9_formid) > 0) { //单据，单据的formid 大于0 ，自定义表单 formid 小于0  (云部署，防止表名长度问题，自动生成不用原表名)
                        tablename = "billftab_" + Math.abs(Util.getIntValue(e9_formid)) + (!suffix_str.equals("") ? "_" + suffix_str : "") + "_" + Constant.TENANT;//格式：billftab_100_后缀_tch71ow08v
                    } else {
                        tablename = "ftab_" + Math.abs(Util.getIntValue(e9_formid)) + (!suffix_str.equals("") ? "_" + suffix_str : "") + "_" + Constant.TENANT;//格式：formtb_100000_后缀_tch71ow08v
                    }
                }
            }
        } else { //老表单 -老表单E9没有固定表名，固E10通过固定格式 拼接生成
            if (isdetial.equals("1")) {
                //明细表
                if (ValidObjCacheUtil.getDeployType() == 0) { //本地部署  (老表单没有固定表名，自动生成表名)
                    tablename = "mxtb_" + Math.abs(Util.getIntValue(e9_formid)) + "_dt_" + groupid + (!suffix_str.equals("") ? "_" + suffix_str : "");//格式：mxtb_100_dt_1_后缀
                } else {
                    tablename = "mxtb_" + Math.abs(Util.getIntValue(e9_formid)) + "_dt_" + groupid + (!suffix_str.equals("") ? "_" + suffix_str : "") + "_" + Constant.TENANT;//格式：mxtb_100_dt_1_tch71ow08v_后缀
                }

            } else {
                //主表
                if (ValidObjCacheUtil.getDeployType() == 0) { //本地部署
                    tablename = "ftb_" + Math.abs(Util.getIntValue(e9_formid)) + (!suffix_str.equals("") ? "_" + suffix_str : "");//格式：ftb_100_后缀
                } else {
                    tablename = "ftb_" + Math.abs(Util.getIntValue(e9_formid)) + (!suffix_str.equals("") ? "_" + suffix_str : "") + "_" + Constant.TENANT;//格式：ftb_100_后缀_tch71ow08v
                }
            }
        }

        return tablename;
    }


    /**
     * 通过 e9 表名  获取可能涉及到的e10 ebuilder表单
     *
     * @param e9_tablename e9表名
     * @Author ygd2020
     * @Date 17:30 2024-1-2
     * @Return key e10 微服务，value List<String> 涉及到的e10 ebuilder 数据库表单名称
     **/
    public static Set<String> getCubeE10TablenameByE9Tablename(String e9_tablename) {

        SourceDBUtil rs = new SourceDBUtil();

        Set<String> tables = new HashSet<>();

        // 可能涉及到的主表
        rs.executeQuery("select modeinfo.id as modeid,isdelete\n" +
                "from modeinfo\n" +
                "         join workflow_bill on workflow_bill.id = formid\n" +
                "where lower(tablename) = ?", e9_tablename.toLowerCase());

        while (rs.next()) {
            String modeid = rs.getString("modeid");
            String isdelete = rs.getString("isdelete");

            if (isdelete.equals("1")) {
                continue;
            }

            if (ValidObjCacheUtil.isValidMode(modeid)) {

                tables.add(E10CubeUtil.getE10MainTablename(modeid, e9_tablename));
            } else {
                tables.add(e9_tablename);
            }

        }

        // 可能涉及到的明细表

        rs.executeQuery("select modeinfo.id as modeid,workflow_billdetailtable.id as e9_sub_form_id,isdelete\n" +
                "from modeinfo\n" +
                "         join workflow_billdetailtable on workflow_billdetailtable.billid = formid\n" +
                "where lower(tablename) = ?", e9_tablename.toLowerCase());

        while (rs.next()) {
            String modeid = rs.getString("modeid");

            String isdelete = rs.getString("isdelete");

            if (isdelete.equals("1")) {
                continue;
            }

            String e9_sub_form_id = rs.getString("e9_sub_form_id");
            if (ValidObjCacheUtil.isValidMode(modeid)) {// 有效的
                tables.add(E10CubeUtil.getE10DetailTablename(modeid, e9_sub_form_id, e9_tablename));
            } else {
                tables.add(e9_tablename);
            }

        }



        return tables;
    }


    /**
     * @param tableName
     * @return
     */
    public static String getE10TableName(String tableName) {

        Set<String> tables = new HashSet<>();


        tables.addAll(getWorkflowE10TablenameByE9Tablename(tableName));
        tables.addAll(getCubeE10TablenameByE9Tablename(tableName));

        if (tables.isEmpty()) {
            new BaseBean().writeLog("e9tablename " + tableName + "并没有解析到e10 表名 ");

            return tableName;
        }
        if (tables.size() > 1) {
            new BaseBean().writeLog("e9tablename " + tableName + " 查到多个表名 》》》 " + tables);
        }

        tableName = StringUtils.join(tables, ",");


        return tableName;
    }

    ;


    public static String getE10FieldName(String tablename,String fieldName, boolean isDetail) {

        if (fieldName.equalsIgnoreCase("formmodeid")
                || fieldName.equalsIgnoreCase("modedatacreatertype")
                || fieldName.equalsIgnoreCase("modedatacreatetime")
                || fieldName.equalsIgnoreCase("form_biz_id")
                || fieldName.equalsIgnoreCase("modeuuid")
        ) {
            fieldName = "'E10无此字段" + fieldName + "'";
            TransUtil.addTransMsg("E9表单字段名称:" + fieldName + " 在E10没有对应字段,请手动确认");
        }
        if (fieldName.equalsIgnoreCase("requestId")) {
            if (!workflowTableMap.get(TransUtil.getTenantKey()).contains(tablename + "|wf|")) {
                fieldName = "'E10无此字段" + fieldName + "'";
                TransUtil.addTransMsg("E9表单字段名称:" + fieldName + " 在E10没有对应字段,请手动确认");
            } else {
                return fieldName;
            }
        }


        //mainid  ->form_data_id

        if (isDetail && fieldName.equalsIgnoreCase("mainid")) {
            return "form_data_id";
        }

        //id
        if (DBCacheUtil.fixFieldNames.contains(fieldName.toUpperCase())) {// 需要原值返回的字段名  e9e10 都存在并且概念一致
            return fieldName;
        }


        HashSet<String> sysFieldNames = DBCacheUtil.getSysFieldNames();// e10 数据库关键字，表单关键字

        if (sysFieldNames.contains(fieldName.toUpperCase())) {
            return fieldName + "_e10";
        }

        if (DBCacheUtil.canReplaceFieldNames.containsKey(fieldName.toUpperCase())) { //  属于 e9系统字段，并且能替换e10系统字段
            return DBCacheUtil.canReplaceFieldNames.get(fieldName.toUpperCase());
        }

        return fieldName;
    }

    public static Map<String, String> microservicesnameMap_tmp = new HashMap<>();

    /**
     * 根据E10表名称获取对应的  字段联动 数据源连接id
     *
     * @param E10TableName
     * @return
     */
    public static DataConnBeanChild getDataConnBeanWorkFlow(String e10TableName) {
        initCache();
        String defaultService = "weaver-workflow-core-service";
        //TargetMiDBUtil targetMiDBUtil = new TargetMiDBUtil("weaver-workflow-core-service");
        if ("weaver-workflow-core-service".equalsIgnoreCase(e10TableName)
                || "weaver-ebuilder-form-service".equalsIgnoreCase(e10TableName)
                || "weaver-formreport-service".equalsIgnoreCase(e10TableName)) {
            for (Object json : workJosnArrary) {
                JSONObject jsonObject = (JSONObject) json;
                if (e10TableName.equalsIgnoreCase(jsonObject.getString("application"))) {
                    DataConnBeanChild dataConnBean = new DataConnBeanChild(String.valueOf(jsonObject.get("id")), Util.null2String(jsonObject.get("application_name")), SqlCache.getTargetDbType());
                    return dataConnBean;
                }
            }
        } else if (microservicesnameMap_tmp.containsKey(e10TableName)) {
            String server = microservicesnameMap_tmp.get(e10TableName);
            for (Object json : workJosnArrary) {
                JSONObject jsonObject = (JSONObject) json;
                if (server.equalsIgnoreCase(jsonObject.getString("application"))) {
                    DataConnBeanChild dataConnBean = new DataConnBeanChild(String.valueOf(jsonObject.get("id")), Util.null2String(jsonObject.get("application_name")),  SqlCache.getTargetDbType());
                    return dataConnBean;
                }
            }

        }


/*
        for (Object json : workJosnArrary) {
            JSONObject jsonObject = (JSONObject) json;
            if (defaultService.equalsIgnoreCase(jsonObject.getString("application"))) {
                DataConnBean dataConnBean = new DataConnBean(String.valueOf(jsonObject.get("id")), Util.null2String(jsonObject.get("application_name")), targetMiDBUtil.getDbtype());

                return dataConnBean;
            }
        }
*/
        return null;
    }


    /**
     * 根据E10表名称获取对应的  字段联动 数据源连接id
     *
     * @param
     * @return
     */
    public static DataConnBeanChild getDataConnBeanDataModel(String e10TableName) {

        initCache();
        String defaultService = "weaver-workflow-core-service";
        if ("weaver-workflow-core-service".equalsIgnoreCase(e10TableName)
                || "weaver-ebuilder-form-service".equalsIgnoreCase(e10TableName)
                || "weaver-formreport-service".equalsIgnoreCase(e10TableName)) {
            ServerNameEnum enumByServer = ServerNameEnum.getEnumByServer(e10TableName);
            DataConnBeanChild dataConnBean = new DataConnBeanChild(String.valueOf(enumByServer.getConnId()), enumByServer.getConnName(),  SqlCache.getTargetDbType());
            return dataConnBean;
        } else if (microservicesnameMap_tmp.containsKey(e10TableName)) {
            String Server = microservicesnameMap_tmp.get(e10TableName);
            ServerNameEnum enumByServer = ServerNameEnum.getEnumByServer(Server);
            if (enumByServer == null) {
                enumByServer = ServerNameEnum.getEnumByServer(defaultService);
            }
            DataConnBeanChild dataConnBean = new DataConnBeanChild(String.valueOf(enumByServer.getConnId()), enumByServer.getConnName(),  SqlCache.getTargetDbType());
            return dataConnBean;
        } /*else {
            ServerNameEnum enumByServer = ServerNameEnum.getEnumByServer(defaultService);
            DataConnBean dataConnBean = new DataConnBean(String.valueOf(enumByServer.getConnId()), enumByServer.getConnName(), targetMiDBUtil.getDbtype());
            return dataConnBean;
        }*/


        return null;

    }
    ;

    public static String convertSQL(String replacedSql) {

        String replacedSql_tmp=replacedSql;


            return replacedSql;

    }

    public static String replaceEdcTableName(String tableName) {
        if (tableName.startsWith("edc_uf_")) {
            if (ValidObjCacheUtil.getDeployType() == 0) { //单租户用原名

            } else {
                return tableName.replaceFirst("edc_uf_", "edc_").replaceFirst("EDC_UF_", "EDC_") + "_" + Constant.TENANT;
            }
        }
        return tableName;
    }

    /**
     * 判断是不是流程浏览按钮
     * @param oldTable
     * @param oldFieldName
     * @return
     */
    public static boolean isWorkFlowBrowser(String oldTable, String oldFieldName) {


        SourceDBUtil sourceDBUtil=new SourceDBUtil();
        sourceDBUtil.executeQuery(" select workflow_billfield.id as fieldid,fieldhtmltype, type, fielddbtype\n" +
                "from workflow_billfield  join workflow_bill on BILLID = workflow_bill.ID\n" +
                "where (lower(TABLENAME) = ? or lower(DETAILTABLE) = ?)  \n" +
                "  and lower(fieldname) = ?  and workflow_billfield.fieldhtmltype=3",oldTable,oldTable,oldFieldName);

        if (sourceDBUtil.next()){
            return true;
        }
        return false;
    }
    public static String getE10BillFieldValue(String oldTable, String oldFieldName, String oldValue) {


        //表单建模，流程，edc同样处理逻辑
        if ("id".equalsIgnoreCase(oldFieldName)) {
            return TransUtil.getNewTableID(oldTable, "id", oldTable, oldValue);
        }
        if ("requestid".equalsIgnoreCase(oldFieldName)) {
            return TransUtil.getNewTableID("wfc_requestbase", "requestid", "workflow_requestbase", oldValue);
        }
        if ("modedatamodifier".equalsIgnoreCase(oldFieldName)) {
            return TransUtil.getNewTableID("employee", "id", "hrmresource", oldValue);
        }
        if ("modedatamodifier".equalsIgnoreCase(oldFieldName)) {
            return TransUtil.getNewTableID("employee", "id", "hrmresource", oldValue);
        }

/*        SourceDBUtil sourceDBUtil=new SourceDBUtil();
        sourceDBUtil.executeQuery("select workflow_billfield.id as fieldid,fieldhtmltype, type, fielddbtype\n" +
                "from workflow_billfield\n" +
                "         join workflow_bill on BILLID = workflow_bill.ID\n" +
                "where (lower(TABLENAME) = ? or lower(DETAILTABLE) = ?)\n" +
                "  and lower(fieldname) = ?",oldTable,oldTable,oldFieldName);
        if (sourceDBUtil.next()){
            String fieldhtmltype = sourceDBUtil.getString("fieldhtmltype");
            String field_id = sourceDBUtil.getString("fieldid");
            String type = sourceDBUtil.getString("type");
            String fielddbtype = sourceDBUtil.getString("fielddbtype");

            Map<String,Object> other=new HashMap<>();
            if (fieldhtmltype.equals("3") && (type.equals("161") || type.equals("162") || type.equals("256") || type.equals("257"))) {
                other.put("fielddbtype", fielddbtype);
            }
            FieldValueEntity e10FieldValueNew = FormFieldUtil.getE10FieldValueNew(field_id, oldValue, fieldhtmltype, type, other, false);
            return e10FieldValueNew.getFieldvalue();
        }*/
        return oldValue;
    }

    public static String getTargetDBFieldName(String name) {


        return formateSqlField(name, SqlCache.getTargetDbType());
    }

    /**
     * 数据库列名关键字处理
     *
     * @param sqlField
     * @param dbType
     * @return
     */
    public static String formateSqlField(String sqlField, String dbType) {
        return formateSqlField(sqlField, dbType, false);
    }

    /**
     * 字段格式化
     *
     * @param sqlField
     * @param dbType
     * @param isMatchCase 大小写敏感
     * @return
     */
    public static String formateSqlField(String sqlField, String dbType, boolean isMatchCase) {
        if (sqlField != null) {
            String sqlFieldtemp = sqlField.toUpperCase();
            if (getDBKeysArray(dbType).contains(sqlFieldtemp)) {
                if (dbType.equalsIgnoreCase(Constant.SQLSERVER)) {
                    sqlField = "[" + sqlField + "]";
                } else if (dbType.equalsIgnoreCase(Constant.ORACLE)) {
                    if (isMatchCase) {
                        sqlField = "\"" + sqlField + "\"";
                    } else {
                        sqlField = "\"" + sqlField.toUpperCase() + "\"";
                    }
                } else if (dbType.equalsIgnoreCase("postgresql")) {
//                    if(isMatchCase){
//                        sqlField = "\"" + sqlField + "\"";
//                    }else{
                    sqlField = "\"" + sqlField.toLowerCase() + "\"";
//                    }
                } else {
                    sqlField = "`" + sqlField + "`";
                }
            }
        }
        return sqlField;
    }

    /**
     * 获取各个数据的关键字信息
     *
     * @param dbtype
     * @return
     */
    public static List<String> getDBKeysArray(String dbtype) {

        if ("oracle".equalsIgnoreCase(dbtype)) {
            return ORACLE_KEYS;
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            return MYSQL_KEYS;
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return POSTGRESQL_KEYS;
        } else {
            return SQLSERVER_KEYS;
        }
    }



    public static volatile Set<String> system_view=null;
    public static volatile Set<String> local_view=new HashSet<>();


    public static volatile Map<String,Integer> custom_view_useCount=new HashMap<>();


    public synchronized static boolean issystemView(String tableName) {

        if (system_view == null) {
            String system_view_tmp = new BaseBean().getPropValue("sqlreplace", "system_view");
            system_view = Arrays.stream(system_view_tmp.split(",")).map(s -> s.trim().toLowerCase()).collect(Collectors.toSet());
            com.weaver.versionupgrade.customRest.util.SourceDBUtil rs_tt = new com.weaver.versionupgrade.customRest.util.SourceDBUtil();
            rs_tt.executeQuery(SqlCache.getE9CustomViewSql());
            while (rs_tt.next()) {
                String TABLE_NAME = rs_tt.getString("TABLE_NAME").toLowerCase();
                local_view.add(TABLE_NAME);
            }

        }
        if (local_view.contains(tableName)) {
            if (!system_view.contains(tableName)) {
                TransUtil.addCustomTable("表=>" + tableName + " 为自定义的视图，升级后请重新创建视图或调整sql");
                if (custom_view_useCount.containsKey(tableName)) {
                    custom_view_useCount.put(tableName,custom_view_useCount.get(tableName)+1);
                }else {
                    custom_view_useCount.put(tableName,1);
                }
            } else {
                TransUtil.addCustomTable("表=>" + tableName + " 为E9标准的视图，升级后请重新创建视图或调整sql");
            }
            return true;
        }
        return false;

    }

    public static String getE9CustomViewSql() {
        com.weaver.versionupgrade.customRest.util.SourceDBUtil sourceDBUtil = new com.weaver.versionupgrade.customRest.util.SourceDBUtil();
        String sql = "";
        String dbtype = sourceDBUtil.getDbtype().toLowerCase();


        if ("oracle".equalsIgnoreCase(dbtype)) {
            sql = "SELECT OWNER AS TABLE_SCHEMA, VIEW_NAME AS TABLE_NAME, TEXT AS TEXT " +
                    "FROM ALL_VIEWS " +
                    "WHERE OWNER = (SELECT SYS_CONTEXT('USERENV', 'CURRENT_SCHEMA') FROM DUAL)";

        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            sql = "SELECT TABLE_SCHEMA, TABLE_NAME, VIEW_DEFINITION AS TEXT " +
                    "FROM INFORMATION_SCHEMA.VIEWS " +
                    "WHERE TABLE_SCHEMA = (SELECT DATABASE())";

        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            sql = "SELECT TABLE_SCHEMA, TABLE_NAME, VIEW_DEFINITION AS TEXT " +
                    "FROM INFORMATION_SCHEMA.VIEWS " +
                    "WHERE TABLE_SCHEMA = CURRENT_SCHEMA";
        } else {
            sql = "SELECT TABLE_SCHEMA, TABLE_NAME, VIEW_DEFINITION AS TEXT " +
                    "FROM INFORMATION_SCHEMA.VIEWS " +
                    "WHERE TABLE_CATALOG = DB_NAME()";
        }


        return sql;
    }




    /**
     * Oracle关键字
     */
    private static List<String> ORACLE_KEYS = Arrays.asList(DBCacheUtil.ORACLE_KEYS);


    /**
     * Mysql关键字
     */
    private static List<String> MYSQL_KEYS = Arrays.asList(DBCacheUtil.MYSQL_KEYS);

    /**
     * postgresql关键字
     */
    private static List<String> POSTGRESQL_KEYS = Arrays.asList(DBCacheUtil.POSTGRESQL_KEYS);

    /**
     * Sqlserver关键字
     */
    private static List<String> SQLSERVER_KEYS = Arrays.asList(DBCacheUtil.SQLSERVER_KEYS);


    public static String getDatabaseName(String jdbcUrl) {
        String database = null;
        if (org.apache.commons.lang.StringUtils.isBlank(jdbcUrl)) {
            throw new IllegalArgumentException("Invalid JDBC url.");
        } else {
            jdbcUrl = jdbcUrl.toLowerCase();
            if (jdbcUrl.startsWith("jdbc:impala")) {
                jdbcUrl = jdbcUrl.replace(":impala", "");
            }

            int pos1;
            if (jdbcUrl.startsWith("jdbc:") && (pos1 = jdbcUrl.indexOf(58, 5)) != -1) {
                String connUri = jdbcUrl.substring(pos1 + 1);
                int pos;
                if (connUri.startsWith("//")) {
                    if ((pos = connUri.indexOf(47, 2)) != -1) {
                        database = connUri.substring(pos + 1);
                    } else if ((pos = connUri.indexOf(61)) != -1) {
                        database = connUri.substring(pos + 1);
                    } else {
                        database = connUri;
                    }
                } else if (connUri.indexOf("thin") > -1) {
                    if ((pos = connUri.indexOf(47)) != -1) {
                        database = connUri.substring(pos + 1);
                    } else {
                        database = connUri;
                    }
                } else {
                    database = connUri;
                }

                if (database.contains("?")) {
                    database = database.substring(0, database.indexOf("?"));
                }

                if (database.contains(";")) {
                    database = database.substring(0, database.indexOf(";"));
                }

                if (org.apache.commons.lang.StringUtils.isBlank(database)) {
                    throw new IllegalArgumentException("Invalid JDBC url.");
                } else {
                    return database;
                }
            } else {
                throw new IllegalArgumentException("Invalid JDBC url.");
            }
        }
    }
	
	    public static DataConnBeanChild getShuJuYuanBeanByPointId(String pointID, boolean isDataWarehouse) {


        return new DataConnBeanChild("","","");


    }
}
