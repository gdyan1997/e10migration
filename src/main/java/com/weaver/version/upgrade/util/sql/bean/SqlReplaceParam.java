package com.weaver.version.upgrade.util.sql.bean;


import java.util.Map;

/**
 * @author wujiahao
 * @date 2024/1/16 11:00
 */
public class SqlReplaceParam {
    private String sourceCode;
    private String targetCode;
    private DataConnBeanChild dataConnBean;
    private String dbtype;
    private String targetDbType;


    private String browserFieldName;

    //ygd 那边使用
    private boolean firstQueryField=false;

    //ygd 那边使用
    private boolean cubeGetOrderAndContCondition=false;

    private boolean selectItem=false;

    private String datasetId=null;


    //E10是否数仓数据源
    private boolean isDataWarehouse=false;


    public String getTargetCode() {
        return targetCode;
    }

    public SqlReplaceParam setTargetCode(String targetCode) {
        this.targetCode = targetCode;
        return this;
    }

    private Map<String,Object> otherMap=null;

    public boolean isDataWarehouse() {
        return isDataWarehouse;
    }

    public void setDataWarehouse(boolean dataWarehouse) {
        isDataWarehouse = dataWarehouse;
    }

    public boolean isFirstQueryField() {
        return firstQueryField;
    }

    public String getDatasetId() {
        return datasetId;
    }

    public void setDatasetId(String datasetId) {
        this.datasetId = datasetId;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getDbtype() {
        return dbtype;
    }

    public void setDbtype(String dbtype) {
        this.dbtype = dbtype;
    }

    public String getTargetDbType() {
        return targetDbType;
    }

    public void setTargetDbType(String targetDbType) {
        this.targetDbType = targetDbType;
    }

    public String getBrowserFieldName() {
        return browserFieldName;
    }

    public void setBrowserFieldName(String browserFieldName) {
        this.browserFieldName = browserFieldName;
    }

    public boolean getFirstQueryField() {
        return firstQueryField;
    }

    public boolean isCubeGetOrderAndContCondition() {
        return cubeGetOrderAndContCondition;
    }

    public void setCubeGetOrderAndContCondition(boolean cubeGetOrderAndContCondition) {
        this.cubeGetOrderAndContCondition = cubeGetOrderAndContCondition;
    }

    public void setFirstQueryField(boolean firstQueryField) {
        this.firstQueryField = firstQueryField;
    }

    public boolean isSelectItem() {
        return selectItem;
    }

    public void setSelectItem(boolean selectItem) {
        this.selectItem = selectItem;
    }

    public Map<String, Object> getOtherMap() {
        return otherMap;
    }

    public void setOtherMap(Map<String, Object> otherMap) {
        this.otherMap = otherMap;
    }



    public String getDataConnBeanId() {

        if (dataConnBean == null) {
            return "";
        }


        return dataConnBean.getId();
    }

    public String getDataConnBeanName() {

        if (dataConnBean == null) {
            return "";
        }


        return dataConnBean.getConn_name();
    }

    public DataConnBeanChild getDataConnBean() {
        return dataConnBean;
    }

    public SqlReplaceParam setDataConnBean(DataConnBeanChild dataConnBean) {
        this.dataConnBean = dataConnBean;
        return this;
    }

}
