package com.weaver.version.upgrade.util.sql.bean;


import java.util.List;

public class TableBean {
    private String tableName;

    private List<String> filedList;

    //private List<MigrateTableMappingResponse> filedMappingMap=new ArrayList<>();

    public TableBean(String tableName) {
        this.tableName = tableName;
        // 还是得先把所有表的字段捞出来
        //this.filedList = SQLUtil.getSelectItemWithTable(tableName);
        //构建E9 E10字段对应关系
//        Map<String, MigrateTableMappingResponse> map = FieldMappingCache.tablefiledMappingMap.get(tableName);
//        if (map==null){
//            filedMappingMap=new HashMap<>();
//        }else {
//            filedMappingMap=map;
//        }
    }

    public String getTableName() {
        return tableName;
    }


    public List<String> getFiledList() {
        return filedList;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setFiledList(List<String> filedList) {
        this.filedList = filedList;
    }
}
