package com.weaver.version.upgrade.util.sql.bean;

import com.weaver.version.upgrade.util.sql.SQLUtil;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ygd2020
 * @date 2024-2-19 14:04
 * Description:
 **/
public class FdLinkageSqlConvertBean {

    String id;
    String datasourcename;//数据源信息

    List<TableBean> tables;// 表名

    String sqlwhere;// 查询条件/关联条件

    List<FieldBean> selectFields;// 查询字段

    public  String sql;// 赋值字段


    List<FieldBean> setFields;// 赋值字段

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static void main(String[] args) {

        SourceDBUtil rs = new SourceDBUtil();
        rs.executeQuery("select * from modeDataInputmain where entryid=49  order by id");

        FdLinkageSqlConvertBean fdLinkageSqlConvertBean = new FdLinkageSqlConvertBean();
        if (rs.next()) {

            fdLinkageSqlConvertBean.setDatasourcename(TransUtil.null2String(rs.getString("datasourcename")));

            fdLinkageSqlConvertBean.setSqlwhere(TransUtil.null2String(rs.getString("whereclause")));


            String datainput_main_id = TransUtil.null2String(rs.getString("id"));

            syncLinkageSourceConfig(fdLinkageSqlConvertBean, datainput_main_id);

        }


        //SQLUtil.FdLinkageSqlConvert(fdLinkageSqlConvertBean);

    }


    private static void syncLinkageSourceConfig(FdLinkageSqlConvertBean fdLinkageSqlConvertBean, String datainput_main_id) {



        SourceDBUtil rs = new SourceDBUtil();
        List<FdLinkageSqlConvertBean.TableBean> tables = new ArrayList<>();
        rs.executeQuery("select * from modeDataInputtable where datainputid='" + datainput_main_id + "' order by id ");
        while (rs.next()) {
            String tablename = TransUtil.null2String(rs.getString("tablename"));//表
            String alias = TransUtil.null2String(rs.getString("alias"));//别名
            String id = TransUtil.null2String(rs.getString("id"));//别名
            TableBean tableBean = fdLinkageSqlConvertBean.new TableBean(tablename, alias,id);
            tables.add(tableBean);

            syncLinkageCondition(fdLinkageSqlConvertBean, tableBean, id);
            syncLinkageValue(fdLinkageSqlConvertBean, tableBean, id);

        }
        fdLinkageSqlConvertBean.setTables(tables);

    }

    private static void syncLinkageCondition(FdLinkageSqlConvertBean fdLinkageSqlConvertBean, TableBean tableBean, String tableid) {

        SourceDBUtil rs = new SourceDBUtil();
        List<FdLinkageSqlConvertBean.FieldBean> conditionfields = new ArrayList<>(fdLinkageSqlConvertBean.getSelectFields());

        rs.executeQuery("select modeDataInputfield.id, dbfieldname, pagefieldname,conditions,tablename,tableid,pagefieldindex from modeDataInputfield left join modeDataInputtable on" +
                " modeDataInputtable.id = modeDataInputfield.tableid where modeDataInputfield.TABLEID='" + tableid + "' and type=1  order by modeDataInputfield.id");
        while (rs.next()) {
            String dbfieldname = TransUtil.null2String(rs.getString("dbfieldname"));//数据库字段
            String pagefieldname = TransUtil.null2String(rs.getString("pagefieldname"));//表单字段/固定值
            String conditions = TransUtil.null2String(rs.getString("conditions"));//0 等于  1属于
            String tablename = TransUtil.null2String(rs.getString("tablename"));//表名
//            String tableid = TransUtil.null2String(rs.getString("tableid"));//表
            String pagefieldindex = TransUtil.null2String(rs.getString("pagefieldindex"));//表
            String id = TransUtil.null2String(rs.getString("id"));//表

            conditionfields.add(fdLinkageSqlConvertBean.new FieldBean(tableBean.getAlias(), dbfieldname, pagefieldname, conditions, tablename, pagefieldindex, id,""));

        }

        fdLinkageSqlConvertBean.setSelectFields(conditionfields);

    }

    private static void syncLinkageValue(FdLinkageSqlConvertBean fdLinkageSqlConvertBean, TableBean tableBean, String tableid) {

        SourceDBUtil rs = new SourceDBUtil();
        List<FdLinkageSqlConvertBean.FieldBean> setfields = new ArrayList<>(fdLinkageSqlConvertBean.getSetFields());

        rs.executeQuery("select modeDataInputfield.id, dbfieldname, pagefieldname,conditions,tablename,tableid,pagefieldindex from modeDataInputfield left join modeDataInputtable on" +
                " modeDataInputtable.id = modeDataInputfield.tableid where modeDataInputfield.TABLEID='" + tableid + "' and type=2  order by modeDataInputfield.id");
        while (rs.next()) {
            String dbfieldname = TransUtil.null2String(rs.getString("dbfieldname"));//数据库字段
            String pagefieldname = TransUtil.null2String(rs.getString("pagefieldname"));//表单字段/固定值
            String conditions = TransUtil.null2String(rs.getString("conditions"));//0 等于  1属于
            String tablename = TransUtil.null2String(rs.getString("tablename"));//表名
            String pagefieldindex = TransUtil.null2String(rs.getString("pagefieldindex"));//表
            String id = TransUtil.null2String(rs.getString("id"));//表


            setfields.add(fdLinkageSqlConvertBean.new FieldBean(tableBean.getAlias(), dbfieldname, pagefieldname, conditions, tablename, pagefieldindex, id,""));

        }

        fdLinkageSqlConvertBean.setSetFields(setfields);

    }


    public class TableBean {
        String tablename;
        String alias;
        String id;

        public TableBean(String tablename, String alias,String id) {
            this.tablename = tablename;

            if (StringUtils.isBlank(alias)) {
                this.alias = tablename;
            } else {
                this.alias = alias;
            }
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTablename() {
            return tablename + " " + alias;
        }

        public String getTable() {
            return tablename;
        }

        public void setTablename(String tablename) {
            this.tablename = tablename;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }
    }


    public class FieldBean {
      public   String fieldname;
        public String belongtable;

        String orderid;

        String pagefieldname  ;//表单字段/固定值
        String conditions ;//0 等于  1属于
        String tablename  ;//表名

        String pagefieldindex  ;//表
        String id ;//表

        public String getPagefieldname() {
            return pagefieldname;
        }

        public void setPagefieldname(String pagefieldname) {
            this.pagefieldname = pagefieldname;
        }

        public String getConditions() {
            return conditions;
        }

        public void setConditions(String conditions) {
            this.conditions = conditions;
        }

        public String getTablename() {
            return tablename;
        }

        public void setTablename(String tablename) {
            this.tablename = tablename;
        }

        public String getPagefieldindex() {
            return pagefieldindex;
        }

        public String getOrderid() {
            return orderid;
        }

        public void setOrderid(String orderid) {
            this.orderid = orderid;
        }

        public void setPagefieldindex(String pagefieldindex) {
            this.pagefieldindex = pagefieldindex;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public FieldBean(String belongtable, String fieldname, String pagefieldname, String conditions, String tablename, String pagefieldindex, String id,String orderid) {
            this.fieldname = fieldname;
            this.belongtable = belongtable;
            this.pagefieldname = pagefieldname;
            this.conditions = conditions;
            this.tablename = tablename;
            this.pagefieldindex = pagefieldindex;
            this.id = id;
            this.orderid = orderid;
        }


        public String getFieldname() {
            return fieldname;
        }

        public void setFieldname(String fieldname) {
            this.fieldname = fieldname;
        }

        public String getBelongtable() {
            return belongtable;
        }

        public void setBelongtable(String belongtable) {
            this.belongtable = belongtable;
        }
    }

    public String getSql() {


        if (tables == null) {
            return "";
        }

        String formsql = " form " + tables.stream().map(m ->  m.getTable() + " " + m.getAlias()).collect(Collectors.joining(","));

        List<FieldBean> backfields = new ArrayList<>();

        if (selectFields != null) {
            backfields.addAll(selectFields);
        }

        if (setFields != null) {
            backfields.addAll(setFields);
        }

        backfields = backfields.stream().distinct().collect(Collectors.toList());

        String backfieldsstr = backfields.stream().map(field -> field.getBelongtable()+"."+field.getFieldname() + " as " + field.getBelongtable()+"_"+field.getFieldname() ).collect(Collectors.joining(","));


        if (backfieldsstr.isEmpty()) {
            backfieldsstr = "*";
        }

        if (StringUtils.isNotBlank(sqlwhere)) {
            formsql += " where " + sqlwhere;
        }
        return "select " + backfieldsstr + formsql;


    }

    public String getDatasourcename() {
        return datasourcename;
    }

    public void setDatasourcename(String datasourcename) {
        this.datasourcename = datasourcename;
    }

    public List<TableBean> getTables() {
        return tables;
    }

    public void setTables(List<TableBean> tables) {
        this.tables = tables;
    }

    public String getSqlwhere() {
        return sqlwhere;
    }

    public void setSqlwhere(String sqlwhere) {
        this.sqlwhere = sqlwhere;
    }

    public List<FieldBean> getSelectFields() {
        return selectFields==null?new ArrayList<>():selectFields;

    }

    public void setSelectFields(List<FieldBean> selectFields) {
        this.selectFields = selectFields;
    }

    public List<FieldBean> getSetFields() {
        return setFields==null?new ArrayList<>():setFields;
    }

    public void setSetFields(List<FieldBean> setFields) {
        this.setFields = setFields;
    }

}

