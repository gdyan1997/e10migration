package com.weaver.version.upgrade.util.sql.visitor.pgServer;

import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.postgresql.ast.stmt.PGInsertStatement;
import com.alibaba.druid.sql.dialect.postgresql.ast.stmt.PGSelectQueryBlock;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.FieldMysqlVisitor;
import com.weaver.version.upgrade.util.sql.util.TransUtil;

import java.util.Optional;

/**
 * @author wujiahao
 * @date 2023/12/14 15:08
 */
public class FieldPgServerVisitor extends PGASTVisitorAdapter {
    /**
     * 重写查询字段的拦截器,没有别名。的那种字段，有别名。的用下面的口子获取
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLSelectItem x) {
        return new FieldMysqlVisitor().visit(x);

    }


    /**
     * 条件查询列表替换。
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLBinaryOpExpr x) {

        return new FieldMysqlVisitor().visit(x);


    }


    /**
     * 重写查询字段的拦截器,没有别名。的那种字段，有别名。的用下面的口子获取
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLUpdateSetItem x) {
        return new FieldMysqlVisitor().visit(x);

    }


    /**
     * 替换mysql 插入列
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLInsertStatement x) {
        return new FieldMysqlVisitor().visit(x);

    }


    /**
     * 重写字段的拦截器,目前好像不写别名的group ，上边的不处理,用下边的加强一下。
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLSelectGroupByClause x) {


        return new FieldMysqlVisitor().visit(x);
    }


    //处理order by pg，比较特殊
    @Override
    public boolean visit(PGSelectQueryBlock x) {
        Optional.ofNullable(x).
                map(or -> or.getOrderBy()).
                map(or -> or.getItems()).ifPresent(sqlSelectOrderByItems -> {
                    for (SQLSelectOrderByItem sqlSelectOrderByItem : sqlSelectOrderByItems) {
                        TransUtil.transSQLExpr(sqlSelectOrderByItem.getExpr(), x);
                    }
                });
        return true;
    }


    /**
     * 重写查询字段的拦截器,没有别名。的那种字段，有别名。的用下面的口子获取
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLSelectOrderByItem x) {
        return new FieldMysqlVisitor().visit(x);
    }



    /**
     * PG  insert  只有这个可以拦截
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(PGInsertStatement x) {
/*        List<SQLExpr> columns = x.getColumns();
        // 这种就是只有单纯的字段类型，不带别名
        for (SQLExpr column : columns) {
            if (column instanceof SQLIdentifierExpr) {
                SQLIdentifierExpr expr = (SQLIdentifierExpr) column;
                String fieldName = expr.getName();
                List<TableBeanHasAlias> tableBeanHasAliases = new java.util.ArrayList<>();
                TableBeanHasAlias tableBeanHasAlias=new TableBeanHasAlias();
                tableBeanHasAlias.setTableName(x.getTableName().getSimpleName());
                tableBeanHasAliases.add(tableBeanHasAlias);
                String newFieldName = FieldMappingCache.replaceTableFiled(tableBeanHasAliases, fieldName, false);
                expr.setName(newFieldName);
            }
        }*/
        return true;
    }

    @Override
    public boolean visit(SQLInListExpr x) {
        return new FieldMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLInSubQueryExpr x) {

        return new FieldMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLCaseExpr x) {

        return new FieldMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLMethodInvokeExpr x) {

        return new FieldMysqlVisitor().visit(x);
    }


    /**
     * 替换mysql 插入列
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLCastExpr x) {
        return new FieldMysqlVisitor().visit(x);
    }

    /**
     * 替换mysql 插入列
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLAggregateExpr x) {
        return new FieldMysqlVisitor().visit(x);
    }

    /**
     * 替换mysql 插入列
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLBetweenExpr x) {
        return new FieldMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLPropertyExpr x) {
        return new FieldMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLIdentifierExpr x) {
        return new FieldMysqlVisitor().visit(x);
    }


}
