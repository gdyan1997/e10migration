package com.weaver.version.upgrade.util.sql.bean;

/**
 * @author wujiahao
 * @date 2023/12/8 16:03
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//


public class FieldInfo {
    public String tableName = "";
    public String fieldName = "";

    public FieldInfo(String tableName, String fieldName) {
        this.tableName = tableName;
        this.fieldName = fieldName;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public int hashCode() {
        return this.tableName.toLowerCase().hashCode() + this.fieldName.toLowerCase().hashCode();
    }

    public String toString() {
        return "{tableName:" + this.tableName.toLowerCase() + ",fieldName:" + this.fieldName.toLowerCase() + "}";
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(obj instanceof FieldInfo)) {
            return false;
        } else {
            FieldInfo s = (FieldInfo)obj;
            return this.tableName.toLowerCase().equals(s.tableName.toLowerCase()) && this.fieldName.toLowerCase().equals(s.fieldName.toLowerCase());
        }
    }
}
