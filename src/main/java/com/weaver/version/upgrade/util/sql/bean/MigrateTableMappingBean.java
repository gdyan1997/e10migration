package com.weaver.version.upgrade.util.sql.bean;
import com.weaver.version.upgrade.util.sql.annotation.FieldInfo;

import java.util.List;

/**
 * @program: weaver-version-upgrade
 * @description: 连表查询视图类
 * @author: yhq
 * @create: 2023-11-07
 **/


public class MigrateTableMappingBean {
    public MigrateTableMappingBean() {
    }

    @FieldInfo(dbName="e9_table",name = "E9表名称")
    private String e9Table;

    /**
     * 替换暂时不使用
     */
    @FieldInfo(dbName="ssmk",name = "所属模块",isSelect = false)
    private String ssmk;


    @FieldInfo(dbName="sfqr",name = "是否确认",isSelect = true,mappingJson = "{\"否\":\"1\",\"是\":\"0\"}")
    private String 	sfqr;



    @FieldInfo(dbName="sfwqth",name = "是否完全替换",isSelect = true,mappingJson = "{\"否\":\"0\",\"是\":\"1\"}")
    private String 	sfwqth;




    @FieldInfo(dbName="thms",name = "替换描述")
    private String 	thms;



    @FieldInfo(dbName="e10_table",name = "E10表名称")
    private String 	e10Table;



    @FieldInfo(dbName="table_type",name = "表类型",isSelect = true,mappingJson = "{\"设置表\":\"0\",\"业务表\":\"2\"}")
    private String 	tableType;


    @FieldInfo(dbName="remark",name = "表描述")
    private String 	remark;


    @FieldInfo(dbName="default_condition",name = "默认条件")
    private String defaultCondition;


    /**
     * 字段映射明细
     */
    private List<MigrateTableMappingBeanDetail> migrateTableMappingBeanDetails;


    public List<MigrateTableMappingBeanDetail> getMigrateTableMappingBeanDetails() {
        return migrateTableMappingBeanDetails;
    }

    public void setMigrateTableMappingBeanDetails(List<MigrateTableMappingBeanDetail> migrateTableMappingBeanDetails) {
        this.migrateTableMappingBeanDetails = migrateTableMappingBeanDetails;
    }

    public String getE9Table() {
        return e9Table;
    }

    public void setE9Table(String e9Table) {
        this.e9Table = e9Table;
    }

    public String getSsmk() {
        return ssmk;
    }

    public void setSsmk(String ssmk) {
        this.ssmk = ssmk;
    }

    public String getSfqr() {
        return sfqr;
    }

    public void setSfqr(String sfqr) {
        this.sfqr = sfqr;
    }

    public String getSfwqth() {
        return sfwqth;
    }

    public void setSfwqth(String sfwqth) {
        this.sfwqth = sfwqth;
    }

    public String getThms() {
        return thms;
    }

    public void setThms(String thms) {
        this.thms = thms;
    }

    public String getE10Table() {
        return e10Table;
    }

    public void setE10Table(String e10Table) {
        this.e10Table = e10Table;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDefaultCondition() {
        return defaultCondition;
    }

    public void setDefaultCondition(String defaultCondition) {
        this.defaultCondition = defaultCondition;
    }




}
