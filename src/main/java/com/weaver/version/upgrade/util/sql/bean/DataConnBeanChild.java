package com.weaver.version.upgrade.util.sql.bean;

import com.alibaba.fastjson.JSONObject;

import java.util.Objects;

/**
 * @author wujiahao
 * @date 2024/12/4 上午9:32
 */
public class DataConnBeanChild {
    private String id;
    private String conn_name;
    private String dbtype;

    /*
    封装三个参数组成json
     */
    private JSONObject connJson;

    public DataConnBeanChild(String id, String conn_name, String dbtype) {
        this.id = id;
        this.conn_name = conn_name;
        this.dbtype = dbtype;

        JSONObject dbData = new JSONObject();
        dbData.put("id", id);
        dbData.put("content", conn_name);
        dbData.put("dbType", dbtype);
        this.connJson = dbData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConn_name() {
        return conn_name;
    }

    public void setConn_name(String conn_name) {
        this.conn_name = conn_name;
    }

    public String getDbtype() {
        return dbtype;
    }

    public void setDbtype(String dbtype) {
        this.dbtype = dbtype;
    }

    public JSONObject getConnJson() {
        return connJson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataConnBeanChild that = (DataConnBeanChild) o;
        return Objects.equals(id, that.id) && Objects.equals(conn_name, that.conn_name) && Objects.equals(dbtype, that.dbtype) && Objects.equals(connJson, that.connJson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, conn_name, dbtype, connJson);
    }



}
