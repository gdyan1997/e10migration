package com.weaver.version.upgrade.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
/**
 * @version 1.0
 * @author ludifu 加密工具类
 */
public class SecurityHelper {
	/**
	 * 系统集成日志对象，用于统一输出日志到系统集成日志文件中。
	 */
	private static Logger newlog = LoggerFactory.getLogger(SecurityHelper.class);

	/**
	 * 序列化对象
	 */
	private final static int ITERATIONS = 20;
	/**
	 * 密钥
	 */
	public final static String KEY = "ecology";


	/**
	 * 加密方法
	 * @param key 密钥
	 * @param plainText 明文
	 * @return 密文
	 */
	public static String encrypt(String key, String plainText){
		String encryptTxt = "";
		try {
			byte[] salt = new byte[8];
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(key.getBytes());
			byte[] digest = md.digest();
			for (int i = 0; i < 8; i++) {
				salt[i] = digest[i];
			}
			PBEKeySpec pbeKeySpec = new PBEKeySpec(key.toCharArray());
			SecretKeyFactory keyFactory = SecretKeyFactory
					.getInstance("PBEWithMD5AndDES");
			SecretKey skey = keyFactory.generateSecret(pbeKeySpec);
			PBEParameterSpec paramSpec = new PBEParameterSpec(salt, ITERATIONS);
			Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
			cipher.init(Cipher.ENCRYPT_MODE, skey, paramSpec);
			byte[] cipherText = cipher.doFinal(plainText.getBytes());
			String saltString = new String(Base64.encode(salt));
			String ciphertextString = new String(Base64.encode(cipherText));
			return saltString + ciphertextString;
		} catch (Exception e) {
			e.printStackTrace();
			newlog.error(e.getMessage(),e);
		}
		return "";
	}
	/**
	 * 加密空串
	 * @param key 密码
	 * @return 密文
	 */
	public static String encrypt(String key){
		String encryptTxt = "";
		String plainText = "";
		try {
			byte[] salt = new byte[8];
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(key.getBytes());
			byte[] digest = md.digest();
			for (int i = 0; i < 8; i++) {
				salt[i] = digest[i];
			}
			PBEKeySpec pbeKeySpec = new PBEKeySpec(key.toCharArray());
			SecretKeyFactory keyFactory = SecretKeyFactory
					.getInstance("PBEWithMD5AndDES");
			SecretKey skey = keyFactory.generateSecret(pbeKeySpec);
			PBEParameterSpec paramSpec = new PBEParameterSpec(salt, ITERATIONS);
			Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
			cipher.init(Cipher.ENCRYPT_MODE, skey, paramSpec);
			byte[] cipherText = cipher.doFinal(plainText.getBytes());
			String saltString = new String(Base64.encode(salt));
			String ciphertextString = new String(Base64.encode(cipherText));
			return saltString + ciphertextString;
		} catch (Exception e) {
			e.printStackTrace();
			newlog.error(e.getMessage(),e);
		}
		return "";
	}
	/**
	 * 测试加密
	 * @param key 明文
	 * @return 密文
	 */
	public static String encryptSimple(String key){
		return encrypt("wEAver2014",key);
	}
	/**
	 * 解密
	 * @param key 密钥
	 * @param encryptTxt 密文
	 * @return 明文
	 */
	public static String decrypt(String key, String encryptTxt){
		int saltLength = 12;
		try {
			if(encryptTxt.length()<saltLength){
				return encryptTxt;
			}
			String salt = encryptTxt.substring(0, saltLength);
			String ciphertext = encryptTxt.substring(saltLength, encryptTxt
					.length());
			byte[] saltarray = Base64.decode(salt.getBytes());
			byte[] ciphertextArray = Base64.decode(ciphertext.getBytes());
			PBEKeySpec keySpec = new PBEKeySpec(key.toCharArray());
			SecretKeyFactory keyFactory = SecretKeyFactory
					.getInstance("PBEWithMD5AndDES");
			SecretKey skey = keyFactory.generateSecret(keySpec);
			PBEParameterSpec paramSpec = new PBEParameterSpec(saltarray,
					ITERATIONS);
			Cipher cipher = Cipher.getInstance("PBEWithMD5AndDES");
			cipher.init(Cipher.DECRYPT_MODE, skey, paramSpec);
			byte[] plaintextArray = cipher.doFinal(ciphertextArray);
			return new String(plaintextArray);
		} catch (Exception e) {
			e.printStackTrace();
			newlog.error(e.getMessage(),e);
		}
		return "";
	}
	/**
	 * 解密
	 * @param key 密文
	 * @return 明文
	 */
	public static String decryptSimple(String key)
	{
		return decrypt("wEAver2014",key);
	}
	/**
	 * 测试主方法
	 * @param args 参数
	 * @return
	 */
	public static void main(String[] args) {
//		String plainTxt = MD5Coder.stringMD5("test")+"="+MD5Coder.stringMD5("test");//明文
		String encryptTxt = "";//密文
		try {
			/*
			encryptTxt = encrypt("weaververify", plainTxt);
			
			plainTxt = decrypt("weaververify", "MQH5E9uKXes=NSG7V8+zZx2mb+qf9mkxupzLihsHY+ema/5MYWNa/+DWh4Utrgt4Z+YQ8vf0ZU6l00LskJxOgvSZl8T85PScBL0Rxb2f2py8");
			
			
			 String str1 = "E:/workspace/ecology_70/ecology/filesystem/updatetemp/A5641221564035190986/ecology";
			 String str2 = "E:/workspace/ecology_70/ecology/filesystem/updatetemp/A5641221564035190986";
			 String str3 = str1.replaceAll(str2, "");
			 /*MD5 MD5 = new MD5();
			
			*/
			Class clazz = Class.forName("weaver.general.SecurityHelper");
			Object object = clazz.newInstance();
			Class [] paramtype = new Class[1];
			paramtype[0] = String.class;
			Method method = clazz.getMethod("encrypt", paramtype);
			Object [] paramvalue = new Object[1];
			paramvalue[0] = "123";
			String temp123 = (String)method.invoke(object, paramvalue);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

}