package com.weaver.version.upgrade.util.sql.visitor.oracle;

import com.alibaba.druid.sql.ast.expr.SQLMethodInvokeExpr;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleSelectTableReference;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.TableMysqlVisitor;

import java.util.HashMap;

/**
 * @author wujiahao
 * @date 2023/12/14 15:50
 */
public class TableOracleVisitor extends OracleASTVisitorAdapter {


    /**
     * 重写表名字的拦截器
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(OracleSelectTableReference x) {
        return new TableMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLExprTableSource x) {
        return new TableMysqlVisitor().visit(x);
    }

    public static HashMap<String,Integer> hashMap=new HashMap();
    @Override
    public boolean visit(SQLMethodInvokeExpr x ) {
        String methodName = x.getMethodName().toLowerCase();

        if (hashMap.containsKey(methodName)){
             Integer count = (Integer) hashMap.get(methodName);
            hashMap.put(methodName,++count);
        }else {
            hashMap.put(methodName,1);
        }
        //x.setMethodName("wjhhhhcc");
        return true;
    }
}
