package com.weaver.version.upgrade.util.sql.util;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.*;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.ast.statement.MySqlInsertStatement;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleInsertStatement;
import com.alibaba.druid.sql.dialect.postgresql.ast.stmt.PGInsertStatement;
import com.alibaba.druid.sql.dialect.sqlserver.ast.stmt.SQLServerInsertStatement;
import com.alibaba.druid.sql.parser.SQLStatementParser;
import com.alibaba.fastjson.JSONObject;
import com.weaver.version.upgrade.util.sql.bean.DataConnBeanChild;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBeanDetail;
import com.weaver.version.upgrade.util.sql.bean.TableBeanHasAlias;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;
import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.*;

/**
 * @author wujiahao
 * @date 2023/12/18 9:37
 */

public class TransUtil {

    private static final Logger logger = LoggerFactory.getLogger(TransUtil.class);

    /**
     * 获取查询字段所属表
     *
     * @param new_sql
     * @param fieldName
     * @return
     */
    public static String getColumnFromTable(String new_sql, String fieldName) {
        //重新生成sql结构树，处理数据库保留字段
        Map<String, Object> browserMenuTable = getBrowserMenuTable(new_sql, fieldName);
        if (browserMenuTable != null && browserMenuTable.containsKey("browserMenuTable")) {
            return String.valueOf(browserMenuTable.get("browserMenuTable"));
        }
        return null;
    }


    public static Map<String, Object> getBrowserMenuTable(String new_sql, String searchFieldName) {
        List<SQLStatement> sqlStatements_tmp = SQLUtils.parseStatements(new_sql, SqlCache.getDbType());
        Map<String, Object> resultMap = new HashMap<>();

        if (StringUtils.isBlank(searchFieldName)) {
            return resultMap;
        }
        SQLSelectQueryBlock sqlSelectQueryBlock = Optional.of(sqlStatements_tmp.get(0)).filter(x -> x instanceof SQLSelectStatement).
                map(x -> (SQLSelectStatement) x).
                map(sqlSelectStatement -> sqlSelectStatement.getSelect())
                .map(sqlSelect -> sqlSelect.getQuery()).
                filter(query -> query instanceof SQLSelectQueryBlock).
                map(query -> (SQLSelectQueryBlock) query).orElse(null);
        if (sqlSelectQueryBlock != null) {
            return getFieldTable(sqlSelectQueryBlock.getFrom(), searchFieldName);
        }
        return resultMap;
    }

    private static Map<String, Object> getFieldTable(SQLTableSource sqlTableSource, String searchFieldName) {


        SQLObject parent = sqlTableSource.getParent();

        List<SQLSelectItem> selectList = null;
        int det = 0;
        while (true) {
            det++;
            if (parent instanceof SQLSelectQueryBlock) {
                SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) parent;
                selectList = sqlSelectQueryBlock.getSelectList();
                break;
            } else {
                parent = parent.getParent();
            }
            if (det > 100) {
                return null;
            }
        }

        //是一个表
        if (sqlTableSource instanceof SQLExprTableSource) {
            SQLExprTableSource sqlExprTableSource = (SQLExprTableSource) sqlTableSource;

            String tableName = sqlExprTableSource.getName().getSimpleName();
            String tableAlias = sqlExprTableSource.getAlias();
            if (tableAlias == null) {
                tableAlias = tableName;
            }

            for (SQLSelectItem sqlSelectItem : selectList) {
                SQLExpr expr = sqlSelectItem.getExpr();
                String alias = sqlSelectItem.getAlias();
                if (expr instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) expr;
                    String fieldName = sqlIdentifierExpr.getName();
                    if (alias == null) {
                        if (searchFieldName.equals(fieldName)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    } else {
                        if (searchFieldName.equalsIgnoreCase(alias)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    }

                } else if (expr instanceof SQLPropertyExpr) {
                    SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) expr;
                    String fieldName = sqlPropertyExpr.getName();
                    String ownerName = sqlPropertyExpr.getOwnernName();

                    //字段和当前表明不相等，无需比较。
                    if (!ownerName.equalsIgnoreCase(tableAlias)) {
                        continue;
                    }

                    //能进来的不可能有这种情况
                    if ("*".equals(fieldName)) {
                        return getDBTableField(tableName, searchFieldName);
                    }
                    if (alias == null) {
                        if (searchFieldName.equals(fieldName)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    } else {
                        if (searchFieldName.equalsIgnoreCase(alias)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    }
                } else if (expr instanceof SQLAllColumnExpr) {
                    return getDBTableField(tableName, searchFieldName);
                }
            }
        } else if (sqlTableSource instanceof SQLJoinTableSource) {
            SQLJoinTableSource sqlJoinTableSource = (SQLJoinTableSource) sqlTableSource;
            SQLTableSource left = sqlJoinTableSource.getLeft();
            SQLTableSource right = sqlJoinTableSource.getRight();
            Map<String, Object> fieldTableLeft = getFieldTable(left, searchFieldName);
            if (fieldTableLeft != null) {
                return fieldTableLeft;
            }
            Map<String, Object> fieldTableRight = getFieldTable(right, searchFieldName);
            if (fieldTableRight != null) {
                return fieldTableRight;
            }
        } else if (sqlTableSource instanceof SQLSubqueryTableSource) {
            SQLSubqueryTableSource sqlSubqueryTableSource = (SQLSubqueryTableSource) sqlTableSource;
            String tableAlias = sqlSubqueryTableSource.getAlias();

            SQLTableSource sqlExprTableSource = Optional.ofNullable(sqlSubqueryTableSource).
                    map(x -> x.getSelect()).
                    map(x -> x.getQuery()).
                    filter(x -> x instanceof SQLSelectQueryBlock).
                    map(x -> (SQLSelectQueryBlock) x).
                    map(x -> x.getFrom()).orElse(null);

            if (sqlExprTableSource != null) {
                for (SQLSelectItem sqlSelectItem : selectList) {
                    SQLExpr expr = sqlSelectItem.getExpr();
                    String alias = sqlSelectItem.getAlias();
                    if (expr instanceof SQLIdentifierExpr) {
                        SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) expr;
                        String fieldName = sqlIdentifierExpr.getName();
                        if (alias == null) {
                            if (searchFieldName.equals(fieldName)) {
                                //return getTableField(tableName,fieldName);
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        } else {
                            if (searchFieldName.equalsIgnoreCase(alias)) {
                                //return getTableField(tableName,fieldName);
                                return getFieldTable(sqlExprTableSource, fieldName);

                            }
                        }
                    } else if (expr instanceof SQLPropertyExpr) {
                        SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) expr;
                        String fieldName = sqlPropertyExpr.getName();
                        String ownerName = sqlPropertyExpr.getOwnernName();
                        //字段和当前表明不相等，无需比较。
                        if (!ownerName.equalsIgnoreCase(tableAlias)) {
                            continue;
                        }
                        //能进来的不可能有这种情况
                        if ("*".equals(fieldName)) {
                            //return getTableField(tableName,alias);
                            return getFieldTable(sqlExprTableSource, searchFieldName);

                        }
                        if (alias == null) {
                            if (searchFieldName.equals(fieldName)) {
                                //return getTableField(tableName,fieldName);
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        } else {
                            if (searchFieldName.equalsIgnoreCase(alias)) {
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        }
                    } else if (expr instanceof SQLAllColumnExpr) {
                        return getFieldTable(sqlExprTableSource, searchFieldName);
                    }
                }
            }

        } else if (sqlTableSource instanceof SQLWithSubqueryClause.Entry) {
            SQLWithSubqueryClause.Entry entry = (SQLWithSubqueryClause.Entry) sqlTableSource;

        }
        return null;
    }

    private static Map<String, Object> getDBTableField(String tableName, String fieldName) {
        Map<String, Object> resultMap = new HashMap<>();
        if (fieldExistsInTable(tableName, fieldName)) {
            resultMap.put("browserMenuTable", tableName);
            resultMap.put("browserMenuField", fieldName);
        } else {
            return null;
        }
        return resultMap;
    }


    //替换where中的 当前对象

    /**
     * @param x          旧的条件
     * @param newSQLExpr 构建好的新的条件
     */
    public static void replaceSQLExpr(SQLExpr x, SQLExpr newSQLExpr) {
        if (x.getParent() instanceof SQLUpdateStatement) {
            SQLUpdateStatement sqlUpdateStatement = (SQLUpdateStatement) x.getParent();
            sqlUpdateStatement.setWhere(newSQLExpr);
        } else if (x.getParent() instanceof SQLBinaryOpExpr) {
            SQLBinaryOpExpr sqlBinaryOpExpr1 = (SQLBinaryOpExpr) x.getParent();
            if (sqlBinaryOpExpr1.getLeft().equals(x)) {
                sqlBinaryOpExpr1.setLeft(newSQLExpr);
            } else if (sqlBinaryOpExpr1.getRight().equals(x)) {
                sqlBinaryOpExpr1.setRight(newSQLExpr);
            }
        }
    }

    public static SQLSelect buildFormDataSelect(SQLExpr sqlwhere) {
        // 构建子查询的 Select 查询块
        SQLSelectQueryBlock queryBlock = new SQLSelectQueryBlock();
        SQLSelect select = new SQLSelect();
        select.setQuery(queryBlock);
        // 构建子查询的 From 子句
        SQLIdentifierExpr table = new SQLIdentifierExpr("wfc_form_data");
        queryBlock.setFrom(table);
        queryBlock.addWhere(sqlwhere);
        // 构建子查询的 Select 列
        SQLIdentifierExpr selectColumn = new SQLIdentifierExpr("dataid");
        queryBlock.addSelectItem(selectColumn);
        SQLQueryExpr sqlQueryExpr = new SQLQueryExpr();
        sqlQueryExpr.setSubQuery(select);
        return select;
    }


    public static boolean isUpdateSql(SQLObject x) {


        SQLObject parent_tmp = x;
        //只处理更新
        for (int i = 0; i < 50; i++) {
            if (parent_tmp instanceof SQLSelectQueryBlock) {
                return false;
            } else if (parent_tmp instanceof SQLDeleteStatement) {
                return false;
            } else if (parent_tmp instanceof SQLUpdateStatement) {
                return true;

            }
            if (parent_tmp instanceof SQLObject) {
                parent_tmp = parent_tmp.getParent();
            } else {
                break;
            }
        }

        return false;
    }


    public static List<TableBeanHasAlias> getFromTableList(SQLObject parent) {
        return getFromTableList(parent, false);
    }


    /**
     * @param parent
     * @param existsEnd 找到exists就截至了，不网上找了
     * @return
     */
    public static List<TableBeanHasAlias> getFromTableList(SQLObject parent, boolean existsEnd) {
        List<TableBeanHasAlias> tableBeanHasAliases = new ArrayList<>();
        SQLObject parent_tmp = parent;
        for (int i = 0; i < 150; i++) {
            if (parent_tmp instanceof SQLSelectQueryBlock) {
                SQLTableSource from = ((SQLSelectQueryBlock) parent_tmp).getFrom();
                traverse(from, tableBeanHasAliases);
            } else if (parent_tmp instanceof SQLDeleteStatement) {
                SQLTableSource from = ((SQLDeleteStatement) parent_tmp).getTableSource();
                traverse(from, tableBeanHasAliases);
            } else if (parent_tmp instanceof SQLInsertStatement) {
                SQLExprTableSource from = ((SQLInsertStatement) parent_tmp).getTableSource();
                traverse(from, tableBeanHasAliases);
            } else if (parent_tmp instanceof SQLUpdateStatement) {
                SQLTableSource from = ((SQLUpdateStatement) parent_tmp).getTableSource();
                traverse(from, tableBeanHasAliases);
            } else if (parent_tmp instanceof SQLSubqueryTableSource) {
                break;
            } else if (existsEnd && parent_tmp instanceof SQLExistsExpr) {
                //处理到第一个exists 截至，不再往上寻找了。
                break;
            }

            if (parent_tmp instanceof SQLObject) {
                parent_tmp = parent_tmp.getParent();
            } else {
                break;
            }
        }
        return tableBeanHasAliases;

    }


    private static void traverse(SQLTableSource tableSource, List<TableBeanHasAlias> tableBeanHasAliases) {
        Queue<SQLTableSource> queue = new LinkedList<>();
        queue.add(tableSource);

        int depth = 0;

        while (!queue.isEmpty()) {
            SQLTableSource currentTableSource = queue.poll();
            //防止死循环，加一个最大深度
            if (++depth > 150) {
                break;
            }
            // 如果是SQLExprTableSource对象，就获取它的别名，并添加到列表中
            if (currentTableSource instanceof SQLExprTableSource) {
                SQLExprTableSource exprTableSource = (SQLExprTableSource) currentTableSource;
                String alias = exprTableSource.getAlias();
                final SQLName name = exprTableSource.getName();
                TableBeanHasAlias tableBeanHasAlias = new TableBeanHasAlias();
                tableBeanHasAlias.setTableName(name.getSimpleName());
                tableBeanHasAlias.setSqlExprTableSource(exprTableSource);
                if (alias == null) {
                    tableBeanHasAlias.setTableAlias(name.getSimpleName());
                } else {
                    tableBeanHasAlias.setTableAlias(alias);

                }
                tableBeanHasAliases.add(tableBeanHasAlias);
            }
            // 如果是SQLJoinTableSource对象，就将其左表和右表加入队列
            else if (currentTableSource instanceof SQLJoinTableSource) {
                SQLJoinTableSource joinTableSource = (SQLJoinTableSource) currentTableSource;
                queue.add(joinTableSource.getLeft());
                queue.add(joinTableSource.getRight());
            } else if (currentTableSource instanceof SQLSubqueryTableSource) {
                //不解析子表，如果解析出来外边查询也需要替换了

            }
        }
    }


    private static final Pattern INTEGER_PATTERN = Pattern.compile("^-?\\d+$");

    public static SQLValuableExpr getNewValue(List<TableBeanHasAlias> fromTableList, SQLExpr sqlExpr, SQLValuableExpr sqlValuableExpr) {


        //获取字段名
        String oldFieldName = "";
        if (sqlExpr instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlExpr;
            oldFieldName = sqlPropertyExpr.getName().toLowerCase();
            String owner = sqlPropertyExpr.getOwnernName().toLowerCase();
            TransUtil.filterTable(owner, fromTableList);
        } else if (sqlExpr instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr sqlPropertyExpr = (SQLIdentifierExpr) sqlExpr;
            oldFieldName = sqlPropertyExpr.getName().toLowerCase();
            if (oldFieldName.contains(".")) {
                TransUtil.filterTable(oldFieldName, fromTableList);
            }
        } else {
            return sqlValuableExpr;
        }


        String new_text="";

        if (sqlValuableExpr instanceof SQLCharExpr) {
            SQLCharExpr sqlCharExpr = (SQLCharExpr) sqlValuableExpr;
            final String old_text = sqlCharExpr.getText();

             //这句再想想呢
            try {

                if (sqlValuableExpr.getParent() instanceof SQLBinaryOpExpr) {
                    SQLBinaryOpExpr sqlBinaryOpExpr = (SQLBinaryOpExpr) sqlValuableExpr.getParent();
                    SQLBinaryOperator operator = sqlBinaryOpExpr.getOperator();
                    //处理一下like和 not like
                    if ("like".equalsIgnoreCase(operator.getName())
                            || "not like".equalsIgnoreCase(operator.getName())) {
                        String text = sqlCharExpr.getText();

                        Pattern pattern = Pattern.compile("%,(\\d+),%");
                        Matcher matcher = pattern.matcher(text);
                        if (matcher.find()) {
                            String group = matcher.group(1);
                            String newValue = TransUtil.getNewValue(fromTableList, oldFieldName, group);
                            sqlCharExpr.setText("%," + newValue + ",%");

                        }
                        return sqlValuableExpr;
                    }
                }
            } catch (Exception e) {
            }


            new_text = TransUtil.getNewValue(fromTableList, oldFieldName, sqlCharExpr.getText());
            if (!old_text.equals(new_text)) {
                TransUtil.addTransMsg("将sql中条件值=>" + old_text + "替换为=>" + new_text);
                sqlCharExpr.setText(new_text);
                //转换之前不是数字，转换后变成数字  删除前后 id='aaa'  id=222
/*                if (INTEGER_PATTERN.matcher(new_text).matches() &&
                        !INTEGER_PATTERN.matcher(old_text).matches()) {
                    sqlValuableExpr = new SQLIntegerExpr(Long.valueOf(new_text));
                } else {
                    sqlCharExpr.setText(new_text);
                }*/
            }
        } else if (sqlValuableExpr instanceof SQLIntegerExpr) {
            SQLIntegerExpr sqlIntegerExpr = (SQLIntegerExpr) sqlValuableExpr;
            String old_number = String.valueOf(sqlIntegerExpr.getNumber());
             new_text = String.valueOf(TransUtil.getNewValue(fromTableList, oldFieldName, String.valueOf(sqlIntegerExpr.getNumber())));
            if (!old_number.equals(new_text)) {
                TransUtil.addTransMsg("将sql中条件值=>" + old_number + "替换为=>" + new_text);
                sqlValuableExpr = new SQLCharExpr(new_text);
                //转换之前是数字，转换后变成非数字  删除前后 id=222 id='aaa'
/*                if (!INTEGER_PATTERN.matcher(new_text).matches()) {
                    sqlValuableExpr = new SQLCharExpr(new_text);
                } else {
                    sqlIntegerExpr.setNumber(Long.valueOf(new_text));
                }*/



            }
        }


        //给郑涛单独处理 城市，改成like
        //120501680000000025
        //100001530000000000
        //100511190000001623
        if(sqlExpr.getParent() instanceof SQLBinaryOpExpr){
            if (Pattern.compile("^.{2}(050168|000153|051119).{10}$").matcher(new_text).matches()) {
                sqlValuableExpr = new SQLCharExpr("%"+new_text);
                SQLBinaryOpExpr sqlBinaryOpExpr=(SQLBinaryOpExpr) sqlExpr.getParent();


                SQLObject parent = sqlExpr.getParent();
                if (parent instanceof SQLBinaryOpExpr){
                    SQLBinaryOpExpr sqlBinaryOpExpr2 = (SQLBinaryOpExpr) parent;
                    SQLBinaryOperator operator = sqlBinaryOpExpr2.getOperator();
                    if (operator.getName().equalsIgnoreCase("<>")){
                        sqlBinaryOpExpr.setOperator(SQLBinaryOperator.NotLike);
                    }else {
                        sqlBinaryOpExpr.setOperator(SQLBinaryOperator.Like);
                    }
                }else {
                    sqlBinaryOpExpr.setOperator(SQLBinaryOperator.Like);

                }





            }
        }

        return sqlValuableExpr;
    }


    public static String getNewValue(List<TableBeanHasAlias> fromTableList, String oldFieldName, String oldValue) {


        if (StringUtils.isBlank(oldValue)) {
            return oldValue;
        }
//        //被替换的新id 不替换
//        if (oldValue.matches("\\d+(\\.\\d+)?") && oldValue.length() > 16) {
//            return oldValue;
//        }
        for (TableBeanHasAlias tableBeanHasAlias : fromTableList) {
            String oldTable = tableBeanHasAlias.getTableName().toLowerCase();

            boolean fieldInTable = fieldExistsInTable(oldTable, oldFieldName);
            //是当前表
            if (fieldInTable) {

                MigrateTableMappingBeanDetail migrateTableMappingBean = FieldMappingCache.getTableMapping(oldFieldName, oldTable);
                //没有找到映射关系
                if (migrateTableMappingBean == null) {

                    Optional<MigrateTableMappingBeanDetail> first = Optional.empty();
                    if (tablefiledMappingMap.containsKey(oldTable)) {
                        first = tablefiledMappingMap.get(oldTable).getMigrateTableMappingBeanDetails().stream().filter(x -> "{$other$}".equalsIgnoreCase(x.getE9TableField())).findFirst();
                    }
                    if (workflowTableMap.get(TransUtil.getTenantKey()).contains(oldTable + "|wf|")
                            || workflowTableMap.get(TransUtil.getTenantKey()).contains(oldTable + "|cube|")
                            || workflowTableMap.get(TransUtil.getTenantKey()).contains(oldTable + "|cube_det|")
                            || workflowTableMap.get(TransUtil.getTenantKey()).contains(oldTable + "|wf_det|")
                            || workflowTableMap.get(TransUtil.getTenantKey()).contains(oldTable + "|edc_det|")
                            || workflowTableMap.get(TransUtil.getTenantKey()).contains(oldTable + "|edc|")
                    ) {
                        /* 被替换的新id 不替换 */
                        if (oldValue.matches("\\d+(\\.\\d+)?") && oldValue.length() > 16) {
                            return oldValue;
                        }
                        if (!oldValue.matches("\\d+(\\.\\d+)?")) {
                            return oldValue;
                        }
                        return SqlCache.getE10BillFieldValue(oldTable, oldFieldName, oldValue);
                    } else if (first.isPresent()) {
                        MigrateTableMappingBeanDetail migrateTableMappingBeanDetail = first.get();
                        String jsonMapping = migrateTableMappingBeanDetail.getJsonMapping();
                        if (TransUtil.isCusSQL(jsonMapping) && "4".equals(migrateTableMappingBeanDetail.getFieldType())) {
                            return TransUtil.getCusSQLFieldValue(tableBeanHasAlias.getSqlExprTableSource(), oldFieldName, oldValue, jsonMapping);
                        }
                    }

                } else {
                    if ("2".equals(migrateTableMappingBean.getFieldType()) || "1".equals(migrateTableMappingBean.getFieldType())) {
//                        if (oldValue.matches("\\d+(\\.\\d+)?")) {
                        if (oldValue.matches("\\d+(\\.\\d+)?") && oldValue.length() > 16) {
                            return oldValue;
                        }
                        if (oldValue.matches("(?i)^[a-zA-Z0-9]+$")) {
                            return getNewTableID(migrateTableMappingBean.getRelevancytabl(), migrateTableMappingBean.getE9relevancyfi(), migrateTableMappingBean.getE9relevancyta(), oldValue);
                        }

                    } else if ("4".equals(migrateTableMappingBean.getFieldType())) {

                        String jsonMapping = migrateTableMappingBean.getJsonMapping();
                        //String jsonMapping = tableMapping.getJsonMapping();
                        if (TransUtil.isCusSQL(jsonMapping)) {
                            return TransUtil.getCusSQLFieldValue(tableBeanHasAlias.getSqlExprTableSource(), oldFieldName, oldValue, jsonMapping);

                        }
                        JSONObject jsonObject = JSONObject.parseObject(jsonMapping);
                        if (jsonObject.containsKey(oldValue)) {
                            return String.valueOf(jsonObject.get(oldValue));
                        } else if (jsonObject.containsKey("default")) {
                            String aDefault = String.valueOf(jsonObject.get("default"));

                            if (!"default".equalsIgnoreCase(aDefault)) {
                                return String.valueOf(jsonObject.get("default"));
                            }
                            return oldValue;

                        }
                    }
                }
            }
        }
        return oldValue;
    }


    /**
     * 没有别名的
     *
     * @param sqlExpr
     * @param x
     */
    public static void transSQLExpr(SQLExpr sqlExpr, SQLObjectImpl x) {





        List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x);
        if (tableBeanHasAliases.size() == 0) {
            return;
        }

        //表明不替换
        if (sqlExpr.getParent() instanceof SQLExprTableSource) {
            return;
        }

        //a.field 不处理
        if (sqlExpr.getParent() instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlExpr.getParent();
            if (sqlPropertyExpr.getOwner() == sqlExpr) {
                return;
            }
        }


        //判断是否是最外层 selectItem
        boolean selectItem = false;
        SQLSelectItem sqlSelectItem = null;
        if (sqlExpr.getParent() instanceof SQLSelectItem) {
            sqlSelectItem = (SQLSelectItem) sqlExpr.getParent();
            selectItem = true;
        }


        //移除无用的条件
        if (sqlExpr.getParent() instanceof SQLBinaryOpExpr) {
            SQLBinaryOpExpr sqlBinaryOpExpr = (SQLBinaryOpExpr) sqlExpr.getParent();
            Map<String, String> fieldTable = getFieldTable(sqlExpr);
            if (fieldTable != null) {
                String tablename = TransUtil.null2String(fieldTable.get("tablename"));
                String fieldname = TransUtil.null2String(fieldTable.get("fieldname"));

                //cus_fielddata
                if (tablename.equalsIgnoreCase("cus_fielddata")
                        && (fieldname.equalsIgnoreCase("scope")
                        || fieldname.equalsIgnoreCase("scopeid")
                )) {
                    sqlBinaryOpExpr.getAttributes().put("remove", "true");
                }

                //workflow_form
                if (tablename.equalsIgnoreCase("workflow_form")
                        && (fieldname.equalsIgnoreCase("billid")
                        || fieldname.equalsIgnoreCase("billid")
                )) {
                    sqlBinaryOpExpr.getAttributes().put("remove", "true");
                }

            }
        }


        if (sqlExpr.getParent() instanceof SQLCaseExpr) {
            SQLCaseExpr sqlCaseExpr = (SQLCaseExpr) sqlExpr.getParent();
            if (sqlCaseExpr.containsAttribute("caseWhen")) {
                return;
            }
        }


        boolean orderBy = false;
        if (sqlExpr.getParent() instanceof SQLSelectOrderByItem) {
            orderBy = true;
        }

        //List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(x);
        String oldFieldName = "";

        if (sqlExpr instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlExpr;
            String ownernName = sqlPropertyExpr.getOwnernName();
            oldFieldName = sqlPropertyExpr.getName();
        }else if (sqlExpr instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) sqlExpr;
            oldFieldName = sqlIdentifierExpr.getName();
        }



        if (sqlExpr instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlExpr;
            String ownernName = sqlPropertyExpr.getOwnernName();
            oldFieldName = sqlPropertyExpr.getName();
            tableBeanHasAliases = tableBeanHasAliases.
                    stream().
                    filter(tableBeanHasAlias -> tableBeanHasAlias.getTableName().equalsIgnoreCase(ownernName)
                            || tableBeanHasAlias.getTableAlias().equalsIgnoreCase(ownernName)
                    ).collect(Collectors.toList());
            replaceSQLExprNew(sqlExpr, replaceTableFiledNew(tableBeanHasAliases, sqlExpr));
            if (selectItem && !oldFieldName.equals("*")) {
                if (sqlSelectItem.getAlias() == null) {
                    sqlSelectItem.setAlias(oldFieldName);
                }
                //case when
                TransUtil.exprToCaseWhen(sqlSelectItem, tableBeanHasAliases, oldFieldName);
            }

        } else if (sqlExpr instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) sqlExpr;
            oldFieldName = sqlIdentifierExpr.getName();

            replaceSQLExprNew(sqlExpr, replaceTableFiledNew(tableBeanHasAliases, sqlExpr));
            if (selectItem) {
                if (sqlSelectItem.getAlias() == null) {
                    sqlSelectItem.setAlias(oldFieldName);
                }
                TransUtil.exprToCaseWhen(sqlSelectItem, tableBeanHasAliases, oldFieldName);
            }

        } else if (sqlExpr instanceof SQLAllColumnExpr) {
            TransUtil.addTransErrorMsg("存在 select * 未指定查询列，请手动确认!!!");

        } /*else if (sqlExpr instanceof SQLMethodInvokeExpr) {


        } else if (sqlExpr instanceof SQLIntegerExpr) {

        } else if (sqlExpr instanceof SQLAllColumnExpr) {

        } else if (sqlExpr instanceof SQLCharExpr) {

        }*/


    }


    public static void replaceSQLExprNew(SQLExpr sqlExpr, SQLExpr newSQLExpr) {
        if (sqlExpr.equals(newSQLExpr)) {
            return;
        }

        String snowId = TransUtil.generateSnowId();
        sqlExpr.getAttributes().put(snowId, "");
        if (sqlExpr.getParent() instanceof SQLSelectOrderByItem) {
            SQLSelectOrderByItem sqlSelectOrderByItem = (SQLSelectOrderByItem) sqlExpr.getParent();
            sqlSelectOrderByItem.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLBinaryOpExpr) {
            SQLBinaryOpExpr sqlBinaryOpExpr = (SQLBinaryOpExpr) sqlExpr.getParent();
            sqlBinaryOpExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLServerInsertStatement) {


        } else if (sqlExpr.getParent() instanceof PGInsertStatement) {


        } else if (sqlExpr.getParent() instanceof MySqlInsertStatement) {


        } else if (sqlExpr.getParent() instanceof SQLUpdateSetItem) {
            SQLUpdateSetItem sqlUpdateSetItem = (SQLUpdateSetItem) sqlExpr.getParent();
            sqlUpdateSetItem.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLInSubQueryExpr) {
            SQLInSubQueryExpr sqlInSubQueryExpr = (SQLInSubQueryExpr) sqlExpr.getParent();
            SQLExpr sqlExpr_tmp = sqlInSubQueryExpr.getExpr();
            if (sqlExpr_tmp.containsAttribute(snowId)) {
                sqlInSubQueryExpr.setExpr(newSQLExpr);
            }
        } else if (sqlExpr.getParent() instanceof SQLInListExpr) {
            SQLInListExpr sqlInListExpr = (SQLInListExpr) sqlExpr.getParent();
            SQLExpr sqlExpr_tmp = sqlInListExpr.getExpr();
            if (sqlExpr_tmp.containsAttribute(snowId)) {
                sqlInListExpr.setExpr(newSQLExpr);
            }
        } else if (sqlExpr.getParent() instanceof SQLBetweenExpr) {
            SQLBetweenExpr sqlBetweenExpr = (SQLBetweenExpr) sqlExpr.getParent();
            sqlBetweenExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLCaseExpr) {
            SQLCaseExpr sqlCaseExpr = (SQLCaseExpr) sqlExpr.getParent();
            sqlCaseExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLSelectGroupByClause) {
            SQLSelectGroupByClause sqlSelectGroupByClause = (SQLSelectGroupByClause) sqlExpr.getParent();
            List<SQLExpr> items = sqlSelectGroupByClause.getItems();
            for (int i = 0; i < items.size(); i++) {
                SQLExpr sqlExpr_tmp = items.get(i);
                if (sqlExpr_tmp.containsAttribute(snowId)) {
                    items.set(i, newSQLExpr);
                }
            }

            Optional.ofNullable(sqlSelectGroupByClause.getHaving()).ifPresent(new Consumer<SQLExpr>() {
                @Override
                public void accept(SQLExpr sqlExpr) {
                    if (sqlExpr.containsAttribute(snowId)) {
                        sqlSelectGroupByClause.setHaving(newSQLExpr);
                    }
                }
            });

        } else if (sqlExpr.getParent() instanceof SQLSelectItem) {
            SQLSelectItem sqlSelectItem = (SQLSelectItem) sqlExpr.getParent();
            sqlSelectItem.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof OracleInsertStatement) {


        } else if (sqlExpr.getParent() instanceof PGInsertStatement) {


        } else if (sqlExpr.getParent() instanceof SQLMethodInvokeExpr) {
            SQLMethodInvokeExpr sqlMethodInvokeExpr = (SQLMethodInvokeExpr) sqlExpr.getParent();
            sqlMethodInvokeExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLCastExpr) {
            SQLCastExpr sqlCastExpr = (SQLCastExpr) sqlExpr.getParent();
            sqlCastExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLAggregateExpr) {
            SQLAggregateExpr sqlAggregateExpr = (SQLAggregateExpr) sqlExpr.getParent();
            sqlAggregateExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLCaseExpr) {
            SQLCaseExpr sqlCaseExpr = (SQLCaseExpr) sqlExpr.getParent();
            sqlCaseExpr.replace(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLCaseExpr.Item) {
            SQLCaseExpr.Item sqlCaseExpr = (SQLCaseExpr.Item) sqlExpr.getParent();
            sqlCaseExpr.replace(sqlExpr, newSQLExpr);
        }else if (sqlExpr.getParent() instanceof SQLQueryExpr) {
            SQLQueryExpr sqlCaseExpr = (SQLQueryExpr) sqlExpr.getParent();
            //sqlCaseExpr.re(sqlExpr, newSQLExpr);
        } else if (sqlExpr.getParent() instanceof SQLSelectQueryBlock) {
            SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) sqlExpr.getParent();

            sqlSelectQueryBlock.replace(sqlExpr, newSQLExpr);


        }
    }


    /**
     * 获取SQLExpr 涉及的列
     *
     * @param sqlExpr
     * @param resultList
     */
    public static void getSqlExpr(SQLExpr sqlExpr_tmp, List<SQLExpr> resultList) {


        Queue<SQLExpr> sqlExprQueue = new LinkedList<>();
        sqlExprQueue.add(sqlExpr_tmp);
        while (!sqlExprQueue.isEmpty()) {
            SQLExpr sqlExpr = sqlExprQueue.poll();
            if (sqlExpr == null) {
                continue;
            }
            if (sqlExpr instanceof SQLBinaryOpExpr) {
                SQLBinaryOpExpr sqlBinaryOpExpr = (SQLBinaryOpExpr) sqlExpr;
                SQLExpr exprRight = sqlBinaryOpExpr.getRight();
                SQLExpr exprLeft = sqlBinaryOpExpr.getLeft();
                sqlExprQueue.add(exprRight);
                sqlExprQueue.add(exprLeft);
            } else if (sqlExpr instanceof SQLAggregateExpr) {
                SQLAggregateExpr sqlAggregateExpr = (SQLAggregateExpr) sqlExpr;
                sqlExprQueue.addAll(sqlAggregateExpr.getArguments());
            } else if (sqlExpr instanceof SQLMethodInvokeExpr) {
                SQLMethodInvokeExpr expr_tmp = (SQLMethodInvokeExpr) sqlExpr;
                List<SQLExpr> arguments = expr_tmp.getArguments();
                sqlExprQueue.addAll(arguments);
            } else if (sqlExpr instanceof SQLCaseExpr) {
                SQLCaseExpr caseExpr = (SQLCaseExpr) sqlExpr;
                Optional.of(sqlExpr).
                        map(x -> (SQLCaseExpr) x).
                        map(x -> x.getItems()).ifPresent(items -> sqlExprQueue.addAll(items.stream().map(x -> x.getConditionExpr()).collect(Collectors.toList())));
                SQLExpr elseExpr = caseExpr.getElseExpr();
                sqlExprQueue.add(elseExpr);
                SQLExpr valueExpr = caseExpr.getValueExpr();
                sqlExprQueue.add(valueExpr);

            } else if (sqlExpr instanceof SQLInListExpr) {
                SQLInListExpr sqlInListExpr = (SQLInListExpr) sqlExpr;
                sqlExprQueue.add(sqlInListExpr.getExpr());
            } else if (sqlExpr instanceof SQLCastExpr) {
                SQLCastExpr sqlInListExpr = (SQLCastExpr) sqlExpr;
                sqlExprQueue.add(sqlInListExpr.getExpr());
            } else if (sqlExpr instanceof SQLExistsExpr) {
                SQLExistsExpr sqlExistsExpr = (SQLExistsExpr) sqlExpr;
                Optional.ofNullable(sqlExistsExpr.subQuery)
                        .map(sqlSelect -> sqlSelect.getQuery())
                        .filter(sqlSelect -> sqlSelect instanceof SQLSelectQueryBlock)
                        .map(sqlSelect -> (SQLSelectQueryBlock) sqlSelect)
                        .map(sqlSelectQueryBlock -> sqlSelectQueryBlock.getWhere()).ifPresent(sqlExprExists -> {
                            List<SQLExpr> list_tmp = new ArrayList();
                            TransUtil.getSqlExpr(sqlExprExists, list_tmp);
                            List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(sqlExprExists, true);
                            //过滤 exists涉及到的表
                            for (SQLExpr expr : list_tmp) {
                                if (expr instanceof SQLIdentifierExpr) {
                                    SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) expr;
                                    String fileName = sqlIdentifierExpr.getName();
                                    List<TableBeanHasAlias> newTableBeanHasAliases = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> fieldExistsInTable(tableBeanHasAlias.getTableName(), fileName) ? true : false).collect(Collectors.toList());
                                    //外部使用，保留
                                    if (newTableBeanHasAliases.size() == 0) {
                                        resultList.add(expr);
                                    }
                                } else if (expr instanceof SQLPropertyExpr) {
                                    SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) expr;
                                    String fileName = sqlPropertyExpr.getName();
                                    String ownernName = sqlPropertyExpr.getOwnernName();
                                    List<TableBeanHasAlias> newTableBeanHasAliases = tableBeanHasAliases.stream().filter(tableBeanHasAlias -> (ownernName.equalsIgnoreCase(tableBeanHasAlias.getTableAlias()) || ownernName.equalsIgnoreCase(tableBeanHasAlias.getTableName())) ? true : false).collect(Collectors.toList());
                                    //外部使用，保留
                                    if (newTableBeanHasAliases.size() == 0) {
                                        resultList.add(expr);
                                    }
                                } else {
                                    resultList.add(expr);
                                }
                            }
                        });

            } else if (sqlExpr instanceof SQLIdentifierExpr) {
                resultList.add(sqlExpr);
            } else if (sqlExpr instanceof SQLPropertyExpr) {
                resultList.add(sqlExpr);
            } else if (sqlExpr instanceof SQLAllColumnExpr) {
                resultList.add(sqlExpr);
            } else {
                resultList.add(sqlExpr);
            }
        }


    }


    /**
     * 根据别名过滤表
     *
     * @param owner         列明或则列别名
     * @param fromTableList
     */
    public static void filterTable(String owner, List<TableBeanHasAlias> fromTableList) {

        //表明+列明
        if (owner.contains(".")) {
            owner = owner.substring(owner.indexOf(".") + 1);
        }
        if (!owner.equals("")) {
            String finalOwner = owner;
            fromTableList.removeIf(tableBeanHasAlias ->
                    (!finalOwner.equalsIgnoreCase(tableBeanHasAlias.getTableAlias())
                            && !finalOwner.equalsIgnoreCase(tableBeanHasAlias.getTableName())
                    )
            );
        }
    }


    /**
     * 获取表中用所有字段
     *
     * @param
     * @return
     */
    public static List<String> getTableUseField(SQLObject x) {


        try {


            List<SQLExpr> sqlExprList = new ArrayList<>();
            //别名

            SQLObject x_tmp = x.getParent();

            SQLObject x_tmp_tmp = x;


            int i = 0;
            while (true) {
                if (i++ > 100) {
                    break;
                }
                if (x_tmp instanceof SQLSelectQueryBlock
                        || x_tmp instanceof SQLUpdateStatement
                        || x_tmp instanceof SQLDeleteStatement
                        || x_tmp instanceof SQLInsertStatement
                ) {
                    break;
                }

                //保存from SQLJoinTableSource
                if (x_tmp instanceof SQLJoinTableSource) {
                    SQLJoinTableSource sqlJoinTableSource = (SQLJoinTableSource) x_tmp;
                    sqlExprList.add(sqlJoinTableSource.getCondition());
                }

                //保存from SQLJoinTableSource
                if (!(x_tmp instanceof SQLJoinTableSource)) {
                    x_tmp_tmp = x_tmp;
                }

                x_tmp = x_tmp.getParent();
            }


            String alias = "";
            String tableName = "";

            //子查询
            if (x_tmp_tmp instanceof SQLSubqueryTableSource) {
                SQLSubqueryTableSource sqlSubqueryTableSource = (SQLSubqueryTableSource) x_tmp_tmp;
                alias = sqlSubqueryTableSource.getAlias();
                tableName = sqlSubqueryTableSource.getAlias();

            } else if (x_tmp_tmp instanceof SQLExprTableSource) {
                SQLExprTableSource sqlExprTableSource = (SQLExprTableSource) x_tmp_tmp;
                alias = sqlExprTableSource.getAlias();
                tableName = Optional.ofNullable(sqlExprTableSource)
                        .map(tt -> tt.getName())
                        .map(tt -> tt.getSimpleName()).orElse(alias);
            }


            if (x_tmp instanceof SQLSelectQueryBlock) {
                SQLSelectQueryBlock parent = (SQLSelectQueryBlock) x_tmp;
                List<SQLSelectItem> selectList = parent.getSelectList();
                if (selectList != null) {
                    //sqlselect
                    for (SQLSelectItem sqlSelectItem : selectList) {
                        sqlExprList.add(sqlSelectItem.getExpr());
                    }
                }
                //sqlwhere
                SQLExpr sqlwhere = parent.getWhere();
                sqlExprList.add(sqlwhere);
                //order by
                SQLOrderBy orderBy = parent.getOrderBy();
                if (orderBy != null) {
                    List<SQLSelectOrderByItem> orderByItems = orderBy.getItems();
                    for (SQLSelectOrderByItem orderByItem : orderByItems) {
                        sqlExprList.add(orderByItem.getExpr());
                    }
                }
                //groupBy
                SQLSelectGroupByClause groupBy = parent.getGroupBy();
                if (groupBy != null) {
                    List<SQLExpr> items = groupBy.getItems();
                    sqlExprList.addAll(items);

                    SQLExpr having = groupBy.getHaving();
                    if (having != null) {
                        sqlExprList.add(having);
                    }
                }
                //join 中 on 后边的
                if (parent.getFrom() instanceof SQLJoinTableSource) {
                    SQLJoinTableSource sqlJoinTableSource = (SQLJoinTableSource) parent.getFrom();
                    sqlExprList.add(sqlJoinTableSource.getCondition());
                }
            } else if (x_tmp instanceof SQLUpdateStatement) {
                //List<SQLExpr> sqlExprList = new ArrayList<>();
                SQLUpdateStatement sqlUpdateStatement = (SQLUpdateStatement) x_tmp;
                List<SQLUpdateSetItem> sqlUpdateSetItems = sqlUpdateStatement.getItems();
                sqlExprList.addAll(sqlUpdateSetItems.stream().map(sqlUpdateSetItem -> sqlUpdateSetItem.getColumn()).collect(Collectors.toList()));
                SQLExpr where = sqlUpdateStatement.getWhere();
                sqlExprList.add(where);
            } else if (x_tmp instanceof SQLDeleteStatement) {
                SQLDeleteStatement sqlDeleteStatement = (SQLDeleteStatement) x_tmp;
                SQLExpr where = sqlDeleteStatement.getWhere();
                sqlExprList.add(where);
            } else if (x_tmp instanceof SQLInsertStatement) {
                SQLInsertStatement sqlInsertStatement = (SQLInsertStatement) x_tmp;
                List<SQLExpr> columns = sqlInsertStatement.getColumns();
                sqlExprList.addAll(columns);
            }


            List<String> resultList = new ArrayList<>();

            List<SQLExpr> resultList_tmp = new ArrayList<>();

            //全部拆成最基础的 SQLExpr
            sqlExprList.forEach(sqlExpr -> TransUtil.getSqlExpr(sqlExpr, resultList_tmp));


            for (SQLExpr sqlExpr : resultList_tmp) {
                if (sqlExpr instanceof SQLAllColumnExpr) {
                    //List<String> tableUseField = getTableUseField(x_tmp);
                    //resultList.addAll(tableUseField);
                } else if (sqlExpr instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr expr = (SQLIdentifierExpr) sqlExpr;
                    String fieldName = expr.getName().toLowerCase();
                    //判断表里是否有这个字段
                    resultList.add(fieldName);
                } else if (sqlExpr instanceof SQLPropertyExpr) {
                    SQLPropertyExpr expr = (SQLPropertyExpr) sqlExpr;
                    String owner = expr.getOwnernName().toLowerCase();
                    String fieldName = expr.getName().toLowerCase();
                    if (owner.equalsIgnoreCase(alias) || owner.equalsIgnoreCase(tableName)) {
                        resultList.add(fieldName);
                    }
                } else {

                }

            }
            //return resultList.stream().distinct().filter(fieldName -> FieldMappingCache.isSystemField(tableName,fieldName)).map(fieldName -> fieldName.toLowerCase()).collect(Collectors.toList());
            return resultList.stream().map(fieldName -> fieldName.toLowerCase()).distinct().collect(Collectors.toList());
        } catch (Exception e) {

            logger.error("sql", e);

        }

        return new ArrayList<>();
    }


    @Deprecated
    public static Map<String, String> extractConditions(SQLExprTableSource x, String aliasName) {

        Map<String, String> conditionMap = new HashMap<>();
        SQLObject x_tmp = x.getParent();
        int i = 0;
        while (x_tmp instanceof SQLJoinTableSource) {
            if (i++ > 100) {
                break;
            }
            x_tmp = x_tmp.getParent();
        }


        if (x_tmp instanceof SQLSelectQueryBlock) {

            SQLSelectQueryBlock x_tmp_tmp = (SQLSelectQueryBlock) x_tmp;
            final SQLExpr where = x_tmp_tmp.getWhere();
            if (where instanceof SQLBinaryOpExpr) {
                extractConditionsFromBinaryOpExpr((SQLBinaryOpExpr) where, conditionMap, aliasName);
            }
        }

        return conditionMap;
    }

    @Deprecated
    private static void extractConditionsFromBinaryOpExpr(SQLBinaryOpExpr binaryOpExpr, Map<String, String> conditionMap, String aliasName) {
        if (binaryOpExpr.getLeft() instanceof SQLIdentifierExpr
                || binaryOpExpr.getLeft() instanceof SQLPropertyExpr
                || binaryOpExpr.getRight() instanceof SQLPropertyExpr
                || binaryOpExpr.getRight() instanceof SQLIdentifierExpr
        ) {
            if (binaryOpExpr.getLeft() instanceof SQLName && binaryOpExpr.getRight() instanceof SQLValuableExpr) {
                //SQLName exprLeft = (SQLName) binaryOpExpr.getLeft();
                String simpleName = "";

                if (binaryOpExpr.getLeft() instanceof SQLPropertyExpr) {
                    SQLPropertyExpr exprLeft = (SQLPropertyExpr) binaryOpExpr.getLeft();
                    String owne = exprLeft.getOwnernName().toLowerCase();
                    simpleName = exprLeft.getName().toLowerCase();
                    if (!aliasName.equalsIgnoreCase("")) {
                        if (!owne.equals(aliasName)) {
                            return;
                        }
                    }
                } else if (binaryOpExpr.getLeft() instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr exprLeft = (SQLIdentifierExpr) binaryOpExpr.getLeft();
                    simpleName = exprLeft.getSimpleName();
                }

                if (simpleName.equalsIgnoreCase("scope") || simpleName.equalsIgnoreCase("scopeid")) {
                    if (binaryOpExpr.getRight() instanceof SQLCharExpr
                            || binaryOpExpr.getRight() instanceof SQLIntegerExpr) {
                        if (binaryOpExpr.getRight() instanceof SQLCharExpr) {
                            SQLCharExpr right = (SQLCharExpr) binaryOpExpr.getRight();
                            conditionMap.put(simpleName.toLowerCase(), right.getText());
                        } else if (binaryOpExpr.getRight() instanceof SQLIntegerExpr) {
                            SQLIntegerExpr right = (SQLIntegerExpr) binaryOpExpr.getRight();
                            conditionMap.put(simpleName.toLowerCase(), right.getNumber() + "");
                        }
                    }
                }

            } else if (binaryOpExpr.getRight() instanceof SQLName && binaryOpExpr.getLeft() instanceof SQLValuableExpr) {
                SQLName exprLeft = (SQLName) binaryOpExpr.getRight();
                final String simpleName = exprLeft.getSimpleName();
                if (simpleName.equalsIgnoreCase("scope") || simpleName.equalsIgnoreCase("scopeid")) {
                    if (binaryOpExpr.getLeft() instanceof SQLCharExpr
                            || binaryOpExpr.getLeft() instanceof SQLIntegerExpr) {
                        if (binaryOpExpr.getLeft() instanceof SQLCharExpr) {
                            SQLCharExpr right = (SQLCharExpr) binaryOpExpr.getLeft();
                            conditionMap.put(simpleName.toLowerCase(), right.getText());
                        } else if (binaryOpExpr.getLeft() instanceof SQLIntegerExpr) {
                            SQLIntegerExpr right = (SQLIntegerExpr) binaryOpExpr.getLeft();
                            conditionMap.put(simpleName.toLowerCase(), right.getNumber() + "");
                        }
                    }
                }
            }
        }

        if (binaryOpExpr.getLeft() instanceof SQLBinaryOpExpr) {
            extractConditionsFromBinaryOpExpr((SQLBinaryOpExpr) binaryOpExpr.getLeft(), conditionMap, aliasName);
        }

        if (binaryOpExpr.getRight() instanceof SQLBinaryOpExpr) {
            extractConditionsFromBinaryOpExpr((SQLBinaryOpExpr) binaryOpExpr.getRight(), conditionMap, aliasName);
        }
    }

    /**
     * 查询输入列，满足条件的，转换成case when
     *
     * @param
     * @return
     */
    public static void exprToCaseWhen(SQLSelectItem x, List<TableBeanHasAlias> tableBeanHasAliases, String fieldName) {

        //判断是不是该字段

        final String fieldName_tmp = fieldName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        //不需要 返回 表明+别名
        if (fieldName.indexOf(".") > -1) {
            fieldName = fieldName.substring(fieldName.lastIndexOf(".") + 1);
        }
        String alias = x.getAlias();

        //找到是那个表
        for (TableBeanHasAlias tableBeanHasAlias : tableBeanHasAliases) {
            if (fieldExistsInTable(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase())) {
                //List<MigrateTableMappingBean> migrateTableMappingBeans = tablefiledMappingMap.getOrDefault(tableBeanHasAlias.getTableName().toLowerCase(), new ArrayList<>());
                if (tablefiledMappingMap.containsKey(tableBeanHasAlias.getTableName().toLowerCase())) {


                    tablefiledMappingMap.get(tableBeanHasAlias.getTableName().toLowerCase()).getMigrateTableMappingBeanDetails().
                            stream().filter
                                    (tableMappingBeanDetail -> fieldName_tmp.equalsIgnoreCase(tableMappingBeanDetail.getE9TableField())).findFirst().ifPresent(
                                    migrateTableMappingBean -> {
                                        if ("4".equalsIgnoreCase(migrateTableMappingBean.getFieldType())) {
                                            try {
                                                //String e9TableField = tableBeanHasAlias.getTableName();
                                                String jsonMapping = migrateTableMappingBean.getJsonMapping();
                                                JSONObject jsonObject = JSONObject.parseObject(jsonMapping);
                                                Set<String> set = jsonObject.keySet();
                                                SQLCaseExpr sqlCaseExpr = new SQLCaseExpr();


                                                SQLExpr expr = x.getExpr();
                                                    /*

                                                                            if (alias==null){
                                                                                // 设置被条件字段，例如sex
                                                                                SQLExpr conditionField = new SQLIdentifierExpr(migrateTableMappingBean.getE10TableField().toLowerCase());
                                                                                sqlCaseExpr.setValueExpr(conditionField);
                                                                            }else {
                                                                                // 设置被条件字段，例如sex
                                                                                SQLExpr conditionField =  new SQLPropertyExpr(alias,migrateTableMappingBean.getE10TableField().toLowerCase());
                                                                                sqlCaseExpr.setValueExpr(conditionField);
                                                                            }
                                                    */

                                                sqlCaseExpr.setValueExpr(expr);


                                                // 正则表达式用于判断是否为数字
                                                Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
                                                // 构建SQLCaseExpr对象
                                                for (String key : set) {
                                                    if (!key.equals("default")) {
                                                        String value = jsonObject.getString(key);
                                                        SQLValuableExpr condition;
                                                        if (pattern.matcher(value).matches()) {
                                                            condition = new SQLIntegerExpr(Integer.parseInt(value));
                                                        } else {
                                                            condition = new SQLCharExpr(value);
                                                        }
                                                        SQLValuableExpr result;
                                                        if (pattern.matcher(key).matches()) {
                                                            result = new SQLIntegerExpr(Integer.parseInt(key));
                                                        } else {
                                                            result = new SQLCharExpr(key);
                                                        }

                                                        SQLCaseExpr.Item item = new SQLCaseExpr.Item();
                                                        item.setConditionExpr(condition);
                                                        item.setValueExpr(result);
                                                        sqlCaseExpr.addItem(item);
                                                    }
                                                }

                                                // 处理默认值
                                                if (jsonObject.containsKey("default")) {
                                                    String defaultValue = jsonObject.getString("default");
                                                    SQLValuableExpr defaultExpr;
                                                    if (pattern.matcher(defaultValue).matches()) {
                                                        defaultExpr = new SQLIntegerExpr(Integer.parseInt(defaultValue));
                                                    } else {
                                                        defaultExpr = new SQLCharExpr(defaultValue);
                                                    }
                                                    sqlCaseExpr.setElseExpr(defaultExpr);
                                                    if (defaultValue.equalsIgnoreCase("default")) {
                                                        sqlCaseExpr.setElseExpr(new SQLIdentifierExpr(migrateTableMappingBean.getE10TableField().toLowerCase()));
                                                    }
                                                } else {
                                                    sqlCaseExpr.setElseExpr(new SQLIdentifierExpr(migrateTableMappingBean.getE10TableField().toLowerCase()));
                                                }

                                                // 设置SQL表达式到目标对象
                                                x.setExpr(sqlCaseExpr);

                                                if (x.getAlias() == null) {
                                                    x.setAlias(fieldName_tmp);
                                                }
                                                Map<String, Object> attributes = sqlCaseExpr.getAttributes();
                                                attributes.put("caseWhen", "");
                                            } catch (Exception e) {
                                                logger.error("sql", e);
                                                //logger.error("sql", e);;
                                            }
                                        }


                                    }

                            );


                }
                break;
            }
        }
    }


    public static String getTenantKey() {
        //return util.Constant.TENANT;
        return SqlCache.getTenantKey();
    }


    public static String null2String(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    public static String getNewTableID(String tableName, String fieldName, String oldTableName, String oldContent) {

        //占位符不替换
        List<String> numList = Optional.ofNullable(sqlResultInfo).
                map(x -> x.get("numList")).
                map(x -> (List<String>) x).
                orElse(new ArrayList<>());
        for (String numId : numList) {
            if (oldContent.contains(numId)) {
                return oldContent;
            }
        }
        //weaver.trans.base.TransUtil.getNewTableID( tableName, fieldName, oldTableName, oldContent);
        //部分映射关系会存在下列情况，特殊 处理一下。
        //INSERT INTO version_upgrade.migrate_tablefield_mapping (relevancytable, relevancyfield, e9relevancytable, e9relevancyfield) VALUES ('wfp_node(id)', 'wfpf_node(id)', 'workflow_nodebase(id)', 'workflow_freenode(id)');

        return SqlCache.getNewTableID(tableName.replace("(id)", ""), fieldName.replace("(id)", ""), oldTableName.replace("(id)", ""), oldContent);
    }

    public static String generateSnowId() {


        return SqlCache.generateSnowId();
    }

    public static SQLExpr dateSplit(List<TableBeanHasAlias> tableName, String fieldName, SQLExpr SQLExpr_tmp) {

        final String e9fieldName = fieldName.toLowerCase();

        SQLExpr SQLExpr = SQLExpr_tmp.clone();

        //不需要 返回 表明+别名
        if (fieldName.indexOf(".") > -1) {
            fieldName = fieldName.substring(fieldName.lastIndexOf(".") + 1);
        }
        for (TableBeanHasAlias tableBeanHasAlias : tableName) {
            final String e9TableName = tableBeanHasAlias.getTableName().toLowerCase();

            if (fieldExistsInTable(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase())) {


                if (tablefiledMappingMap.containsKey(e9TableName)) {


                    Optional<MigrateTableMappingBeanDetail> first =
                            tablefiledMappingMap.get(tableBeanHasAlias.getTableName().toLowerCase()).getMigrateTableMappingBeanDetails().
                                    stream().filter
                                            (tableMappingBeanDetail -> e9fieldName.equalsIgnoreCase(tableMappingBeanDetail.getE9TableField())).findFirst();
                    if (first.isPresent()) {
                        MigrateTableMappingBeanDetail migrateTableMappingBean = first.get();
                        if ("1".equalsIgnoreCase(migrateTableMappingBean.getZdzhsz()) || "2".equalsIgnoreCase(migrateTableMappingBean.getZdzhsz())) {
                            if ("sqlserver".equalsIgnoreCase(String.valueOf(sqlResultInfo.get("targetDbType")))) {
                                SQLCastExpr sqlCastExpr = new SQLCastExpr();
                                if ("1".equalsIgnoreCase(migrateTableMappingBean.getZdzhsz())) {
                                    sqlCastExpr.setExpr(SQLExpr.clone());
                                    sqlCastExpr.setDataType(new SQLDataTypeImpl("date"));
                                } else {
                                    sqlCastExpr.setExpr(SQLExpr);
                                    sqlCastExpr.setDataType(new SQLDataTypeImpl("time"));
                                }
                                return sqlCastExpr;


                            } else if ("oracle".equalsIgnoreCase(String.valueOf(sqlResultInfo.get("targetDbType")))) {


                                SQLMethodInvokeExpr sqlMethodInvokeExpr = new SQLMethodInvokeExpr();
                                if ("1".equalsIgnoreCase(migrateTableMappingBean.getZdzhsz())) {
                                    sqlMethodInvokeExpr.setMethodName("TRUNC");
                                    sqlMethodInvokeExpr.addArgument(SQLExpr);
                                } else {
                                    sqlMethodInvokeExpr.setMethodName("TO_CHAR");
                                    sqlMethodInvokeExpr.addArgument(SQLExpr);
                                    sqlMethodInvokeExpr.addArgument(new SQLCharExpr("HH24:MI:SS"));
                                }
//                            return migrateTableMappingBean.getFieldType() == 5 ? "TRUNC(" + newFieldName + ")" : "TO_CHAR(" + newFieldName + " , 'HH24:MI:SS')";
                                return sqlMethodInvokeExpr;
                            } else if ("postgresql".equalsIgnoreCase(String.valueOf(sqlResultInfo.get("targetDbType")))) {
                                SQLCastExpr sqlCastExpr = new SQLCastExpr();
                                if ("1".equalsIgnoreCase(migrateTableMappingBean.getZdzhsz())) {
                                    sqlCastExpr.setExpr(SQLExpr);
                                    sqlCastExpr.setDataType(new SQLDataTypeImpl("date"));
                                } else {
                                    sqlCastExpr.setExpr(SQLExpr);
                                    sqlCastExpr.setDataType(new SQLDataTypeImpl("time"));
                                }
                                return sqlCastExpr;

                            } else if ("mysql".equalsIgnoreCase(String.valueOf(sqlResultInfo.get("targetDbType")))) {
                                SQLCastExpr sqlCastExpr = new SQLCastExpr();
                                if ("1".equalsIgnoreCase(migrateTableMappingBean.getZdzhsz())) {
                                    sqlCastExpr.setExpr(SQLExpr);
                                    sqlCastExpr.setDataType(new SQLDataTypeImpl("date"));
                                } else {
                                    sqlCastExpr.setExpr(SQLExpr);
                                    sqlCastExpr.setDataType(new SQLDataTypeImpl("time"));
                                }
                                return sqlCastExpr;

                            }
                        }

                    }
                }
            }
        }
        return SQLExpr;
    }


    /**
     * 判断是否最外层查询
     *
     * @param x
     * @return
     */
    public static boolean isOuterQuerySelectItem(SQLSelectQueryBlock x) {
        if (x == null) {
            return false;
        }
        SQLObject parent = x.getParent();
        if (parent instanceof SQLSelect) {
            SQLSelect select = (SQLSelect) parent;
            SQLObject grandParent = select.getParent();
            if (grandParent instanceof SQLSelectStatement) {
                return true;
            }
        }
        return false;
    }


    /**
     * 判断是否可以替换
     *
     * @param x
     * @return
     */
    public static boolean isReplace(SQLSelectQueryBlock x) {
        if (x == null) {
            return false;
        }
        SQLObject parent = x.getParent();

        while (true) {
            if (parent instanceof SQLSelectQueryBlock) {
                SQLSelectQueryBlock selectQueryBlock = (SQLSelectQueryBlock) parent;
                //最外层查询
                long count = selectQueryBlock.getSelectList().stream().filter(sqlSelectItem -> {
                    if (sqlSelectItem.getExpr() instanceof SQLAllColumnExpr) {
                        return true;
                    }
                    if (sqlSelectItem.getExpr() instanceof SQLPropertyExpr) {
                        SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlSelectItem.getExpr();
                        return "*".equals(sqlPropertyExpr.getName()) ? true : false;
                    }
                    return false;
                }).count();
                if (count == 0) {
                    return true;
                }
            }
            if (parent.getParent() == null || parent.getParent() == parent) {
                break;
            }
            parent = parent.getParent();
        }
        return false;
    }

    public static void addTransErrorMsg(String msg) {


        HashSet<String> hashSet = (HashSet<String>) Optional.ofNullable(sqlResultInfo.get("transErrorMsg")).orElse(new HashSet());

        hashSet.add(msg);
        sqlResultInfo.put("transErrorMsg", hashSet);
    }


    public static void addTransMsg(String msg) {


        HashSet hashSet = (HashSet) Optional.ofNullable(sqlResultInfo.get("transInfoMsg")).orElse(new HashSet());
        hashSet.add(msg);
        sqlResultInfo.put("transInfoMsg", hashSet);
    }

    public static void addCustomTable(String msg) {
        HashSet hashSet = (HashSet) Optional.ofNullable(sqlResultInfo.get("customTable")).orElse(new HashSet());
        hashSet.add(msg);
        sqlResultInfo.put("customTable", hashSet);
    }

    public static void addCustomField(String msg) {

        HashSet hashSet = (HashSet) Optional.ofNullable(sqlResultInfo.get("customField")).orElse(new HashSet());
        hashSet.add(msg);
        sqlResultInfo.put("customField", hashSet);
    }


    public static void addDataConn(String tablename) {
        addDataConnBeanWorkFlow(tablename);
        addDataConnBeanDataModel(tablename);
    }


    private static void addDataConnBeanWorkFlow(String tablename) {
        DataConnBeanChild dataConnBeanDataModel = SqlCache.getDataConnBeanWorkFlow(tablename);
        HashSet<DataConnBeanChild> hashSet = (HashSet) Optional.ofNullable(sqlResultInfo.get("connBeanWorkFlow")).orElse(new HashSet<DataConnBeanChild>());
        if (dataConnBeanDataModel != null) {
            hashSet.add(dataConnBeanDataModel);
        }
        sqlResultInfo.put("connBeanWorkFlow", hashSet);
    }

    private static void addDataConnBeanDataModel(String tablename) {
        DataConnBeanChild dataConnBeanDataModel = SqlCache.getDataConnBeanDataModel(tablename);
        HashSet<DataConnBeanChild> hashSet = (HashSet) Optional.ofNullable(sqlResultInfo.get("connBeanDataModel")).orElse(new HashSet<DataConnBeanChild>());
        if (dataConnBeanDataModel != null) {
            hashSet.add(dataConnBeanDataModel);
        }
        sqlResultInfo.put("connBeanDataModel", hashSet);
    }


    public static void addFirstQueryField(SQLSelectItem x) {

        SQLObject parent = x.getParent();
        String firstQueryField = TransUtil.null2String(sqlResultInfo.get("firstQueryField"));
        SQLSelectItem sqlSelectItemTmp = x;
        // Example method to process the SQLSelectItem
        if (firstQueryField.equals("true")) {

            Map<String, Object> objectMap = (Map<String, Object>) sqlResultInfo.getOrDefault("otherMap", new HashMap<>());

            if (objectMap.containsKey("sourcetype") && "workflow".equalsIgnoreCase(TransUtil.null2String(objectMap.get("sourcetype")))) {
                if (parent instanceof SQLSelectQueryBlock) {
                    SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) parent;
                    List<SQLSelectItem> selectList = sqlSelectQueryBlock.getSelectList();
                    if (selectList.size() > 1) {
                        sqlSelectItemTmp = selectList.get(1);
                    } else {
                        sqlSelectItemTmp = selectList.get(0);
                    }
                }
            }
            String alias = sqlSelectItemTmp.getAlias();
            SQLExpr expr = sqlSelectItemTmp.getExpr();
            if (!(expr instanceof SQLAllColumnExpr)) {
                if (alias == null) {
                    sqlSelectItemTmp.setAlias("content");
                    sqlResultInfo.put("firstQueryField", "content");
                } else {
                    sqlResultInfo.put("firstQueryField", alias);
                }
            } else {
                sqlResultInfo.put("firstQueryField", "");
            }
        }
    }


    /***
     * 获取到sql条件
     * @param sqlObject
     * @param condition
     * @return
     */
    public static void setCondition(SQLObject sqlObject, SQLBinaryOpExpr condition) {

        if (sqlObject instanceof SQLSelectQueryBlock) {
            SQLSelectQueryBlock sqlSelectQueryBlock = ((SQLSelectQueryBlock) sqlObject);
            sqlSelectQueryBlock.addWhere(condition);
        } else if (sqlObject instanceof SQLDeleteStatement) {
            SQLDeleteStatement sqlSelectQueryBlock = ((SQLDeleteStatement) sqlObject);
            sqlSelectQueryBlock.addWhere(condition);
        } else if (sqlObject instanceof SQLUpdateStatement) {
            SQLUpdateStatement sqlSelectQueryBlock = ((SQLUpdateStatement) sqlObject);
            sqlSelectQueryBlock.addWhere(condition);
        } else if (sqlObject instanceof SQLJoinTableSource) {
            SQLJoinTableSource sqlJoinTableSource = ((SQLJoinTableSource) sqlObject);
            if (SQLJoinTableSource.JoinType.CROSS_JOIN == sqlJoinTableSource.getJoinType() ||
                    SQLJoinTableSource.JoinType.COMMA == sqlJoinTableSource.getJoinType()
            ) {
                setCondition(sqlJoinTableSource.getParent(), condition);
            } else {
                sqlJoinTableSource.addCondition(condition);
            }
        } else {

        }
    }


    /***
     * 获取到sql条件
     * @param sqlObject
     * @param condition
     * @return
     */
    public static void removeCondition(SQLObject sqlObject) {

        if (sqlObject instanceof SQLSelectQueryBlock) {
            SQLSelectQueryBlock sqlSelectQueryBlock = ((SQLSelectQueryBlock) sqlObject);
            if (sqlSelectQueryBlock.getWhere() instanceof SQLBinaryOpExpr) {

                SQLBinaryOpExpr where = (SQLBinaryOpExpr) sqlSelectQueryBlock.getWhere();
                if (where != null) {
                    if (where.containsAttribute("remove")) {
                        sqlSelectQueryBlock.setWhere(null);
                    } else {
                        deleteCondition(where);
                    }
                }
            }


        } else if (sqlObject instanceof SQLDeleteStatement) {
            SQLDeleteStatement sqlSelectQueryBlock = ((SQLDeleteStatement) sqlObject);


            if (sqlSelectQueryBlock.getWhere() instanceof SQLBinaryOpExpr) {
                SQLBinaryOpExpr where = (SQLBinaryOpExpr) sqlSelectQueryBlock.getWhere();
                if (where != null) {
                    if (where.containsAttribute("remove")) {
                        sqlSelectQueryBlock.setWhere(null);
                    } else {
                        deleteCondition(where);
                    }
                }
            }
        } else if (sqlObject instanceof SQLUpdateStatement) {
            SQLUpdateStatement sqlSelectQueryBlock = ((SQLUpdateStatement) sqlObject);
            if (sqlSelectQueryBlock.getWhere() instanceof SQLBinaryOpExpr) {

                SQLBinaryOpExpr where = (SQLBinaryOpExpr) sqlSelectQueryBlock.getWhere();
                if (where != null) {
                    if (where.containsAttribute("remove")) {
                        sqlSelectQueryBlock.setWhere(null);
                    } else {
                        deleteCondition(where);
                    }
                }
            }

        } else if (sqlObject instanceof SQLJoinTableSource) {
            SQLJoinTableSource sqlJoinTableSource = ((SQLJoinTableSource) sqlObject);
            if (SQLJoinTableSource.JoinType.CROSS_JOIN == sqlJoinTableSource.getJoinType() ||
                    SQLJoinTableSource.JoinType.COMMA == sqlJoinTableSource.getJoinType()
            ) {
                removeCondition(sqlJoinTableSource.getParent());
            } else {
                //sqlJoinTableSource.addCondition(condition);
            }
        }
    }

    public static void deleteCondition(SQLBinaryOpExpr sqlBinaryOpExpr) {
        SQLExpr left = sqlBinaryOpExpr.getLeft();
        SQLExpr right = sqlBinaryOpExpr.getRight();

        if (left instanceof SQLBinaryOpExpr) {
            SQLBinaryOpExpr sqlBinaryOpExpr_tmp = (SQLBinaryOpExpr) left;
            if (sqlBinaryOpExpr_tmp.containsAttribute("remove")) {
                SQLObject parent = sqlBinaryOpExpr.getParent();
                if (parent instanceof SQLBinaryOpExpr) {
                    SQLBinaryOpExpr parentSQLBinaryOpExpr = (SQLBinaryOpExpr) parent;
                    parentSQLBinaryOpExpr.replace(sqlBinaryOpExpr, sqlBinaryOpExpr.getRight());
                }
            } else {
                deleteCondition(sqlBinaryOpExpr_tmp);
            }
        }
        if (right instanceof SQLBinaryOpExpr) {
            SQLBinaryOpExpr sqlBinaryOpExpr_tmp = (SQLBinaryOpExpr) right;
            if (sqlBinaryOpExpr_tmp.containsAttribute("remove")) {
                SQLObject parent = sqlBinaryOpExpr.getParent();
                if (parent instanceof SQLBinaryOpExpr) {
                    SQLBinaryOpExpr parentSQLBinaryOpExpr = (SQLBinaryOpExpr) parent;
                    parentSQLBinaryOpExpr.replace(sqlBinaryOpExpr, sqlBinaryOpExpr.getLeft());
                }
            } else {
                deleteCondition(sqlBinaryOpExpr_tmp);
            }
        }
    }


    public static String getAliasName(SQLExprTableSource x, String tableName) {
        if (StringUtils.isNotBlank(x.getAlias())) {
            return x.getAlias() + ".";
        } else {
            return tableName + ".";
        }
    }


    private static Set<String> comparisonOperators = new HashSet<>(Arrays.asList(
            "=", ">", ">=", "<", "<=", "<>", "!<", "!=", "!>", "like", "not like"
    ));

    // 检查操作符是否为指定的 SQL 操作符
    public static boolean isComparisonOperator(String operator) {

        return comparisonOperators.contains(operator.toLowerCase());
    }


    // 检查操作符是否为指定的 SQL 操作符
    public static void getSelectItemJr(SQLStatement sqlStatement) {


        Optional.ofNullable(sqlStatement).
                filter(x -> x instanceof SQLSelectStatement).
                map(x -> (SQLSelectStatement) x).map(x -> x.getSelect()).ifPresent(sqlSelect -> {
                    SQLSelectQuery query = sqlSelect.getQuery();

                    int i = 0;
                    while (true) {
                        if (i++ > 100) {
                            return;
                        }
                        List<SQLSelectItem> selectList = null;
                        if (query instanceof SQLSelectQueryBlock) {
                            SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) query;
                            selectList = sqlSelectQueryBlock.getSelectList();
                            if (selectList.size() == 1 && selectList.get(0).getExpr() instanceof SQLAllColumnExpr) {
                                SQLTableSource from = sqlSelectQueryBlock.getFrom();
                                if (from instanceof SQLSubqueryTableSource) {
                                    SQLSubqueryTableSource sqlJoinTableSource = (SQLSubqueryTableSource) from;
                                    SQLSelect select = sqlJoinTableSource.getSelect();
                                    query = select.getQuery();
                                } else if (from instanceof SQLJoinTableSource) {
                                    //不会走到这一层
                                    return;
                                }
                            } else {
                                List<String> aliasList = new ArrayList<>();
                                for (SQLSelectItem sqlSelectItem : selectList) {
                                    String alias = sqlSelectItem.getAlias();
                                    if (StringUtils.isNotBlank(alias)) {
                                        aliasList.add(alias);
                                        //System.out.println(alias);
                                        sqlResultInfo.put("aliasList", aliasList);
                                    }
                                }
                                return;
                            }
                        }

                    }
                });
    }


    public static Map<String, String> getFieldTable(SQLExpr sqlExpr) {
        HashMap<String, String> result = new HashMap<>();
        List<TableBeanHasAlias> tableBeanHasAliases = TransUtil.getFromTableList(sqlExpr);
        //移除无用的条件
        if (sqlExpr.getParent() instanceof SQLBinaryOpExpr) {
            if (sqlExpr instanceof SQLPropertyExpr) {
                SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlExpr;
                String ownernName = sqlPropertyExpr.getOwnernName();
                String oldFieldName = sqlPropertyExpr.getName();
                //获取表明
                tableBeanHasAliases = tableBeanHasAliases.
                        stream().
                        filter(tableBeanHasAlias -> tableBeanHasAlias.getTableName().equalsIgnoreCase(ownernName)
                                || tableBeanHasAlias.getTableAlias().equalsIgnoreCase(ownernName)
                        ).collect(Collectors.toList());
                for (TableBeanHasAlias tableBeanHasAlias : tableBeanHasAliases) {
                    if (FieldMappingCache.isSystemField(tableBeanHasAlias.getTableName(), oldFieldName)) {
                        result.put("tablename", tableBeanHasAlias.getTableName().toLowerCase());
                        result.put("fieldname", oldFieldName.toLowerCase());
                        return result;
                    }
                }
            } else if (sqlExpr instanceof SQLIdentifierExpr) {
                SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) sqlExpr;
                String oldFieldName = sqlIdentifierExpr.getName();
                for (TableBeanHasAlias tableBeanHasAlias : tableBeanHasAliases) {
                    if (FieldMappingCache.isSystemField(tableBeanHasAlias.getTableName(), oldFieldName)) {

                        result.put("tablename", tableBeanHasAlias.getTableName().toLowerCase());
                        result.put("fieldname", oldFieldName.toLowerCase());


                        return result;
                    }
                }
            }
        }


        return new HashMap<>();
    }


    public static String getTenantKeyNumber() {
        return SqlCache.getTenantKeyNumber();
    }


    /**
     * 只解析单表，简单条件
     *
     * @param sqlExprTableSource
     * @return
     */
    public static HashMap<String, String> getConditionMap(SQLExprTableSource sqlExprTableSource) {
        HashMap<String, String> conditionMap = new HashMap<>();


        try {

            if (sqlExprTableSource == null) {
                return conditionMap;
            }
            //视图名称、不替换
            //表明是数字 特殊写法不解析
            if (sqlExprTableSource.getParent() instanceof SQLCreateViewStatement
                    || sqlExprTableSource.getExpr() instanceof SQLIntegerExpr
                    || sqlExprTableSource.getExpr() instanceof SQLMethodInvokeExpr) {
                return conditionMap;
            }
            SQLExpr where = null;
            if (sqlExprTableSource.getParent() instanceof SQLSelectQueryBlock) {
                SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) sqlExprTableSource.getParent();
                where = sqlSelectQueryBlock.getWhere();
            }
            if (sqlExprTableSource.getParent() instanceof SQLJoinTableSource) {
                SQLJoinTableSource sqlJoinTableSource = (SQLJoinTableSource) sqlExprTableSource.getParent();
                if (sqlJoinTableSource.getParent() instanceof SQLSelectQueryBlock) {
                    SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) sqlExprTableSource.getParent();
                    where = sqlSelectQueryBlock.getWhere();
                }
            }
            if (where instanceof SQLBinaryOpExpr) {
                convertToNaryTree((SQLBinaryOpExpr) where, conditionMap, sqlExprTableSource);
            }
        } catch (Exception e) {
            logger.error("sql", e);;
        }

        return conditionMap;
    }


    // 将 SQLBinaryOpExpr 转换为多叉树
    private static void convertToNaryTree(SQLBinaryOpExpr expr, Map<String, String> conditionMap, SQLExprTableSource sqlExprTableSource) {
        SQLBinaryOperator operator = expr.getOperator();

        //继续递归
        if (expr.getLeft() instanceof SQLBinaryOpExpr && expr.getRight() instanceof SQLBinaryOpExpr) {
            if (operator != SQLBinaryOperator.BooleanAnd) {
                return;
            }
            convertToNaryTree((SQLBinaryOpExpr) expr.getLeft(), conditionMap, sqlExprTableSource);
            convertToNaryTree((SQLBinaryOpExpr) expr.getRight(), conditionMap, sqlExprTableSource);
            return;
        }
        SQLExpr leftExp = null;
        SQLExpr rightExp = null;


        //只处理等于
        if (operator != SQLBinaryOperator.Equality) {
            return;
        }
        //###只记录一侧为字符一侧为数字或者字符
        //左侧条件，右侧值
        if ((expr.getLeft() instanceof SQLPropertyExpr || expr.getLeft() instanceof SQLIdentifierExpr)
                && (expr.getRight() instanceof SQLCharExpr || expr.getRight() instanceof SQLIntegerExpr)) {
            leftExp = expr.getLeft();
            rightExp = expr.getRight();
        }

        //右侧值左侧条件
        if ((expr.getRight() instanceof SQLPropertyExpr || expr.getRight() instanceof SQLIdentifierExpr)
                && (expr.getLeft() instanceof SQLCharExpr || expr.getLeft() instanceof SQLIntegerExpr)) {
            rightExp = expr.getLeft();
            leftExp = expr.getRight();
        }


        //获取当前字段可能来源表
        List<TableBeanHasAlias> fromTableList = TransUtil.getFromTableList(expr);
        String fieldName = "";
        String fieldValue = "";

        if (leftExp instanceof SQLPropertyExpr) {
            fieldName = ((SQLPropertyExpr) leftExp).getName();
        }

        if (leftExp instanceof SQLIdentifierExpr) {
            fieldName = ((SQLIdentifierExpr) leftExp).getName();
        }


        if (rightExp instanceof SQLCharExpr) {
            fieldValue = ((SQLCharExpr) rightExp).getText();
        }

        if (rightExp instanceof SQLIntegerExpr) {
            fieldValue = ((SQLIntegerExpr) rightExp).getNumber().toString();
        }
        //根据别名过滤一下
        if (leftExp instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) leftExp;
            TransUtil.filterTable(sqlPropertyExpr.getOwnernName(), fromTableList);
        }
        for (TableBeanHasAlias tableBeanHasAlias : fromTableList) {


            final  String  oldTableName = Optional.ofNullable(sqlExprTableSource)
                    .map(y -> y.getName())
                    .map(z -> z.getSimpleName())
                    .orElse("");


            if (tableBeanHasAlias.getTableName().equalsIgnoreCase(oldTableName)) {
                if (FieldMappingCache.fieldExistsInTable(tableBeanHasAlias.getTableName(), fieldName)) {
                    conditionMap.put(fieldName.toLowerCase(), fieldValue);
                }
            }
        }


    }


    /**
     * 获取E10对应表明方法
     * //{$com.weaver.version.upgrade.util.sql.custom.SQLCustomizationDemo#transformTableName$}
     *
     * @param e9TableName  E9表明
     * @param conditionMap E9所属表涉及到的简单条件MAP
     * @param sqlRegex     配置的E10表明,支持正则
     * @return
     */
    public static String getCusSQLTableName(SQLExprTableSource sqlExprTableSource, String sqlRegex) {

        try {
            HashMap<String, String> conditionMap = TransUtil.getConditionMap(sqlExprTableSource);
            final String e9TableName = Optional.ofNullable(sqlExprTableSource)
                    .map(y -> y.getName())
                    .map(z -> z.getSimpleName())
                    .map(String::toLowerCase)
                    .orElse("");
            List<String> tableUseField = new ArrayList<>();

            Map<String, Object> attributes = sqlExprTableSource.getAttributes();
            if (attributes.containsKey("tableUseField")) {
                tableUseField = ((List<String>) attributes.get("tableUseField")).stream().distinct().filter(fieldName -> FieldMappingCache.isSystemField(e9TableName, fieldName)).map(fieldName -> fieldName.toLowerCase()).collect(Collectors.toList());
                ;
            }


            // 创建 Matcher 对象
            Matcher matcher = Pattern.compile("(\\{\\$(.*?)\\$\\})").matcher(sqlRegex);
            // 查找匹配项
            while (matcher.find()) {
                // 获取匹配的组（即 ${} 中的内容）
                String regexString = matcher.group(2);
                String string = matcher.group(1);
                String[] split = regexString.split("#");
                if (split.length > 1) {
                    String className = split[0];
                    String methodName = split[1];
                    Class<?> myClass = Class.forName(className);

                    Method method = myClass.getMethod(methodName, String.class, HashMap.class, List.class);
                    boolean isStatic = Modifier.isStatic(method.getModifiers());
                    String result = (String) method.invoke(isStatic ? null : myClass.getDeclaredConstructor().newInstance(), e9TableName, conditionMap, tableUseField);
                    sqlRegex = sqlRegex.replace(string, result);
                }
            }
        } catch (Exception e) {
            logger.error("sql", e);;
        }
        return sqlRegex;
    }


    /**
     * 获取E10对应字段名称方法
     */
    public static String getCusSQLFieldName(SQLExprTableSource sqlExprTableSource, String e9FieldName, String sqlRegex) {
        try {
            HashMap<String, String> conditionMap = TransUtil.getConditionMap(sqlExprTableSource);
            final String e9TableName = Optional.ofNullable(sqlExprTableSource)
                    .map(y -> y.getName())
                    .map(z -> z.getSimpleName())
                    .map(String::toLowerCase)
                    .orElse("");
            List<String> tableUseField = new ArrayList<>();

            Map<String, Object> attributes = sqlExprTableSource.getAttributes();
            if (attributes.containsKey("tableUseField")) {
                tableUseField = ((List<String>) attributes.get("tableUseField")).stream().distinct().filter(fieldName -> FieldMappingCache.isSystemField(e9TableName, fieldName)).map(fieldName -> fieldName.toLowerCase()).collect(Collectors.toList());
                ;
            }
            // 创建 Matcher 对象
            Matcher matcher = Pattern.compile("(\\{\\$(.*?)\\$\\})").matcher(sqlRegex);
            // 查找匹配项
            while (matcher.find()) {
                // 获取匹配的组（即 ${} 中的内容）
                String regexString = matcher.group(2);
                String string = matcher.group(1);

                String[] split = regexString.split("#");
                if (split.length > 1) {
                    String className = split[0];
                    String methodName = split[1];
                    Class<?> myClass = Class.forName(className);
                    Method method = myClass.getMethod(methodName, String.class, String.class, HashMap.class, List.class);
                    boolean isStatic = Modifier.isStatic(method.getModifiers());

                    String result = (String) method.invoke(isStatic ? null : myClass.getDeclaredConstructor().newInstance(), e9FieldName, e9TableName, conditionMap, tableUseField);
                    sqlRegex = sqlRegex.replace(string, result);
                }
            }
        } catch (Exception e) {
            logger.error("sql", e);;
        }
        return sqlRegex;
    }

    /**
     * 获取E10对应字段名称方法
     *
     * @param e9TableName  E9表明
     * @param e9FieldName  E9字段名
     * @param conditionMap E9所属表涉及到的简单条件MAP
     * @param sql          配置的E10字段名,支持正则
     * @return
     */
    public static String getCusSQLFieldValue(SQLExprTableSource sqlExprTableSource, String e9FieldName, String e9Value, String sqlRegex) {


        try {
            HashMap<String, String> conditionMap = TransUtil.getConditionMap(sqlExprTableSource);
            final String e9TableName = Optional.ofNullable(sqlExprTableSource)
                    .map(y -> y.getName())
                    .map(z -> z.getSimpleName())
                    .map(String::toLowerCase)
                    .orElse("");
            List<String> tableUseField = new ArrayList<>();
            Map<String, Object> attributes = sqlExprTableSource.getAttributes();
            if (attributes.containsKey("tableUseField")) {
                tableUseField = ((List<String>) attributes.get("tableUseField")).stream().distinct().filter(fieldName -> FieldMappingCache.fieldExistsInTable(e9TableName, fieldName)).map(fieldName -> fieldName.toLowerCase()).collect(Collectors.toList());
                ;
            }
            // 创建 Matcher 对象
            Matcher matcher = Pattern.compile("(\\{\\$(.*?)\\$\\})").matcher(sqlRegex);
            // 查找匹配项
            while (matcher.find()) {
                // 获取匹配的组（即 ${} 中的内容）
                String regexString = matcher.group(2);
                String[] split = regexString.split("#");
                String string = matcher.group(1);

                if (split.length > 1) {
                    String className = split[0];
                    String methodName = split[1];
                    Class<?> myClass = Class.forName(className);
                    Method method = myClass.getMethod(methodName, String.class, String.class, String.class, HashMap.class, List.class);
                    boolean isStatic = Modifier.isStatic(method.getModifiers());
                     e9Value = (String) method.invoke(isStatic ? null : myClass.getDeclaredConstructor().newInstance(), e9Value, e9FieldName, e9TableName, conditionMap, tableUseField);
                }
            }
        } catch (Exception e) {
            logger.error("sql", e);;
        }
        return e9Value;
    }


    /**
     * 判断是否存在占位符
     *
     * @param sql
     * @return
     */
    public static boolean isUpdateFormSQL(String tableName) {
        final String tableNameFinal = tableName.toLowerCase();
        if (tablefiledMappingMap.containsKey(tableNameFinal)) {
            if (tablefiledMappingMap.get(tableNameFinal).getMigrateTableMappingBeanDetails().stream().filter(MigrateTableMappingBeanDetail -> "1".equalsIgnoreCase(MigrateTableMappingBeanDetail.getJoinType())).count() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断是否存在占位符
     *
     * @param sql
     * @return
     */
    public static boolean isCusSQL(String sql) {
        return Pattern.compile("\\{\\$(.*?)\\$\\}").matcher(sql).find();
    }

    public static  SQLQueryExpr buildSQLSelect(String sql) {
        SQLStatementParser parser = new SQLStatementParser(sql);
        List<SQLStatement> statementList = parser.parseStatementList();
        for (SQLStatement statement : statementList) {
            SQLSelectStatement selectStatement = (SQLSelectStatement) statement;
            SQLSelect select = selectStatement.getSelect();
            SQLQueryExpr queryExpr = new SQLQueryExpr(select);
            return queryExpr;
        }
        return null;
    }


    /**
     *
     * @param sqlExpr
     * @param sqlValue
     * @param x
     * 当浏览按钮 3 ，且是流程
     * E9:
     *     xgqq=''
     *     xgqq NOT LIKE ''
     *
     *
     *
     * E10:
     *     (xgqq='' OR xgqq is null)
     *     (xgqq NOT LIKE ‘’ or xgqq is null)
     */
    public static void customWorkFlowBrowser(SQLExpr sqlExpr, SQLExpr sqlValue, SQLBinaryOpExpr x) {


        List<TableBeanHasAlias> fromTableList = TransUtil.getFromTableList(x.getParent());

        try {


            Map<String, Object> objectMap = (Map<String, Object>) sqlResultInfo.getOrDefault("otherMap", new HashMap<>());
            if (objectMap.containsKey("sourcetype")) {
                if ("workflow".equalsIgnoreCase(String.valueOf(objectMap.get("sourcetype")))) {
                    String oldFieldName = "";
                    if (sqlExpr instanceof SQLPropertyExpr) {
                        SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) sqlExpr;
                        oldFieldName = sqlPropertyExpr.getName().toLowerCase();
                        String owner = sqlPropertyExpr.getOwnernName().toLowerCase();
                        TransUtil.filterTable(owner, fromTableList);
                    } else if (sqlExpr instanceof SQLIdentifierExpr) {
                        SQLIdentifierExpr sqlPropertyExpr = (SQLIdentifierExpr) sqlExpr;
                        oldFieldName = sqlPropertyExpr.getName().toLowerCase();
                        if (oldFieldName.contains(".")) {
                            TransUtil.filterTable(oldFieldName, fromTableList);
                        }
                    }

                    for (TableBeanHasAlias tableBeanHasAlias : fromTableList) {
                        String oldTable = tableBeanHasAlias.getTableName().toLowerCase();
                        boolean fieldInTable = fieldExistsInTable(oldTable, oldFieldName);
                        //是当前表
                        if (fieldInTable) {
                            if (("=".equalsIgnoreCase(x.getOperator().getName()) && sqlValue instanceof SQLCharExpr && "".equalsIgnoreCase(((SQLCharExpr) sqlValue).getText())) || "NOT LIKE".equalsIgnoreCase(x.getOperator().getName())) {
                                if (SqlCache.isWorkFlowBrowser(oldTable, oldFieldName)) {

                                    SQLBinaryOpExpr newSqlBinaryOpExpr = new SQLBinaryOpExpr();
                                    newSqlBinaryOpExpr.setOperator(SQLBinaryOperator.BooleanOr);
                                    SQLBinaryOpExpr newSqlBinaryRight = new SQLBinaryOpExpr();
                                    SQLExpr clone_tmp = sqlExpr.clone();
                                    newSqlBinaryRight.setLeft(clone_tmp);
                                    newSqlBinaryRight.setOperator(SQLBinaryOperator.Is);
                                    newSqlBinaryRight.setRight(new SQLNullExpr());
                                    SQLBinaryOpExpr clone = x.clone();
                                    newSqlBinaryOpExpr.setLeft(clone);
                                    newSqlBinaryOpExpr.setRight(newSqlBinaryRight);
                                    TransUtil.replaceSQLExprNew(x, newSqlBinaryOpExpr);
                                }
                            }

                            break;
                        }
                    }

                }
            }
        } catch (Exception e) {

        }
    }





    /**
     * 解决示例：
     * MySQL：
     * select (SELECT CONCAT(',', GROUP_CONCAT(id), ',')
     *         FROM emp_link
     *         WHERE cid = hrmresource.id
     *           AND relation = 'othersenior'
     * ) as  managerstr
     * from employee hrmresource;
     * PostgreSQL:
     * select (SELECT ',' || STRING_AGG(id::TEXT, ',') || ','
     *         FROM emp_link
     *         WHERE cid = hrmresource.id
     *           AND relation = 'othersenior') as managerstr
     * from employee hrmresource;
     * Oracle:
     * select (SELECT ',' || LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) || ','
     *         FROM emp_link
     *         WHERE cid = employee.id
     *           AND relation = 'othersenior'
     * ) as managerstr
     * from employee hrmresource;
     * SQLserver
     * select (SELECT ',' + STRING_AGG(id, ',') + ','
     *         FROM emp_link
     *         WHERE cid = employee.id
     *           AND relation = 'othersenior') as managerstr
     * from employee hrmresource;
     * @param sql
     * @return
     * 将子查询改成join ,分隔
     * ob (默认前后有逗号)
     */
    public static void buildConcat(SQLQueryExpr sqlQueryExpr, boolean bo) {

        SQLSelect subQuery = sqlQueryExpr.getSubQuery();
        SQLSelectQuery query = subQuery.getQuery();
        if (query instanceof SQLSelectQueryBlock) {
            SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) query;
            List<SQLSelectItem> selectList = sqlSelectQueryBlock.getSelectList();
            if (selectList == null||selectList.size()!=1) {
                return;
            }

            SQLSelectItem sqlSelectItem = selectList.get(0);
            SQLExpr expr = sqlSelectItem.getExpr();
            String fieldName = expr.toString();
            SQLIdentifierExpr expr_new=new SQLIdentifierExpr();
            String targetDbType = SqlCache.getTargetDbType();
            sqlSelectItem.setExpr(expr_new);
            if ("mysql".equalsIgnoreCase(targetDbType)) {
                //  CONCAT(',', GROUP_CONCAT(id), ',')
                //  GROUP_CONCAT(id)
                if (bo){
                    expr_new.setName("CONCAT(',', GROUP_CONCAT("+fieldName+"), ',')");
                }else {
                    expr_new.setName(" GROUP_CONCAT("+fieldName+")");
                }
            } else if ("oracle".equalsIgnoreCase(targetDbType)) {
                sqlResultInfo.put("isConvertDB",true);

                // ',' || LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) || ','
                //  LISTAGG(id, ',') WITHIN GROUP (ORDER BY id)
                if (bo){
                    expr_new.setName("',' || LISTAGG("+fieldName+", ',') WITHIN GROUP (ORDER BY "+fieldName+") || ','");
                }else {
                    expr_new.setName(" LISTAGG("+fieldName+", ',') WITHIN GROUP (ORDER BY "+fieldName+")");
                }
            } else if ("postgresql".equalsIgnoreCase(targetDbType)) {
                //',' || STRING_AGG(id::TEXT, ',') || ','
                //STRING_AGG(id::TEXT, ',')
                if (bo){
                    expr_new.setName("',' || STRING_AGG("+fieldName+"::TEXT, ',') || ','");
                }else {
                    expr_new.setName(" STRING_AGG("+fieldName+"::TEXT, ',')");
                }
            } else if ("sqlserver".equalsIgnoreCase(targetDbType)) {
                //',' + STRING_AGG(id, ',') + ','
                // STRING_AGG(id, ',')
                if (bo){
                    expr_new.setName("',' + STRING_AGG("+fieldName+", ',') + ','");
                }else {
                    expr_new.setName("STRING_AGG("+fieldName+", ',')");
                }
            }
        }
    }







    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {

        //buildSQLSelect("select ID ,(select tt from hrmresource) as ttt from hrmresource");




    }
}




