package com.weaver.version.upgrade.util.sql.visitor.sqlServer;

import com.alibaba.druid.sql.ast.expr.SQLIdentifierExpr;
import com.alibaba.druid.sql.ast.expr.SQLPropertyExpr;
import com.alibaba.druid.sql.ast.statement.SQLCreateViewStatement;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.sqlserver.visitor.SQLServerASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.DatabaseFieldMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.oracle.DatabaseFieldOracleVisitor;

/**
 * @author wujiahao
 * @date 2024/6/3 下午5:41
 * sql保留字段处理
 */
public class DatabaseFieldSqlServerVisitor extends SQLServerASTVisitorAdapter {
    @Override
    public boolean visit(SQLPropertyExpr expr) {
        return new DatabaseFieldMysqlVisitor().visit(expr);
    }
    @Override
    public boolean visit(SQLIdentifierExpr expr) {
        return new DatabaseFieldMysqlVisitor().visit(expr);
    }

    @Override
    public boolean visit(SQLExprTableSource x) {
        return new DatabaseFieldOracleVisitor().visit(x);
    }
    public boolean visit(SQLCreateViewStatement x) {
        return new DatabaseFieldMysqlVisitor().visit(x);
    }
}
