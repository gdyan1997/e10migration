package com.weaver.version.upgrade.util.sql.visitor.oracle;

import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleSelectQueryBlock;
import com.alibaba.druid.sql.dialect.oracle.ast.stmt.OracleSelectTableReference;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.ConditionValueMysqlVisitor;

/**
 * @author wujiahao
 * @date 2023/12/18 10:16
 *   主要做条件中的值，替换成E10新id
 */
public class ConditionValueOracleVisitor extends OracleASTVisitorAdapter {


    @Override
    public boolean visit(OracleSelectQueryBlock x) {
        return new ConditionValueMysqlVisitor().visitBase(x);
    }


    @Override
    public boolean visit(SQLBinaryOpExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    /**
     * 重写表名字的拦截器
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(OracleSelectTableReference x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }


    @Override
    public boolean visit(SQLExprTableSource x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLInListExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLWithSubqueryClause.Entry x) {

        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLCreateViewStatement x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLSelectStatement x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }


    @Override
    public boolean visit(SQLSelectItem x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }

    @Override
    public boolean visit(SQLMethodInvokeExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }
    public boolean visit(SQLBetweenExpr x) {
        return new ConditionValueMysqlVisitor().visit(x);
    }
    @Override
    public boolean visit(SQLCaseExpr x) {

        return new ConditionValueMysqlVisitor().visit(x);
    }

}
