package com.weaver.version.upgrade.util.sql.visitor.pgServer;

import com.alibaba.druid.sql.ast.expr.SQLIdentifierExpr;
import com.alibaba.druid.sql.ast.expr.SQLPropertyExpr;
import com.alibaba.druid.sql.ast.statement.SQLCreateViewStatement;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGASTVisitorAdapter;
import com.alibaba.druid.sql.visitor.SQLASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.version.upgrade.util.sql.visitor.mysql.DatabaseFieldMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.mysql.TableMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.oracle.DatabaseFieldOracleVisitor;

/**
 * @author wujiahao
 * @date 2024/6/3 下午5:41
 * sql保留字段处理
 */
public class DatabaseFieldPgVisitor extends PGASTVisitorAdapter {
    @Override
    public boolean visit(SQLPropertyExpr expr) {
        return new DatabaseFieldMysqlVisitor().visit(expr);

    }
    @Override
    public boolean visit(SQLIdentifierExpr expr) {
        return new DatabaseFieldMysqlVisitor().visit(expr);

    }

    public boolean visit(SQLExprTableSource x) {
        return new DatabaseFieldMysqlVisitor().visit(x);
    }
    public boolean visit(SQLCreateViewStatement x) {
        return new DatabaseFieldMysqlVisitor().visit(x);
    }

}
