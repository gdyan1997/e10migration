package com.weaver.version.upgrade.util.sql;
import com.alibaba.druid.sql.ast.SQLName;
import com.alibaba.druid.sql.ast.expr.*;
import com.github.vertical_blank.sqlformatter.SqlFormatter;
import com.weaver.version.upgrade.util.SecurityHelper;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.util.PropertiesUtil;
import com.weaver.versionupgrade.util.Util;
import idtoolutil.IdGenerator;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.util.deparser.StatementDeParser;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.SQLObject;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.ast.statement.SQLJoinTableSource;
import com.alibaba.druid.sql.ast.statement.SQLSelectItem;
import com.alibaba.druid.sql.ast.statement.SQLSelectQueryBlock;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.alibaba.druid.sql.ast.statement.SQLSubqueryTableSource;
import com.alibaba.druid.sql.ast.statement.SQLTableSource;
import com.alibaba.druid.sql.ast.statement.SQLWithSubqueryClause;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 只给yhq用
 */
public class BrowserSQLUtil {
    //栈 用于存储表中涉及到的字段。
    public static Queue<List<String>> queue = new LinkedList();
    /**
     * 返回sql替换结果
     */
    public static Map<String, Object> sqlResultInfo = new HashMap<>();
    private static Logger logger = LoggerFactory.getLogger(BrowserSQLUtil.class);
    /**
     * 判断是否是本地数据源
     *
     * @param dataSourceId
     * @return
     */
    static volatile Map<String, Boolean> dataSourceMap = null;

    /**
     * 是否为视图
     *
     * @param tableName
     * @return
     */
    public static boolean checkIsView(String tableName) {
        try {
            boolean flag = false;
            SourceDBUtil rs = new SourceDBUtil();
            String sql = "";
            switch (rs.getDbtype().toLowerCase()) {
                case "mysql":
                    sql = "select * from information_schema.TABLES where table_name =? ";
                    break;
                case "oracle":
                    sql = "SELECT * FROM all_views WHERE view_name = UPPER(?)";
                    break;
                case "sqlserver":
                    sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ?";
                    break;
                case "postgresql":
                    sql = "SELECT * FROM information_schema.tables WHERE table_name = ?";
                    break;
                default:
                    return false;
            }
            rs.executeQuery(sql, tableName);
            //反过来
            if (rs.getDbtype().equalsIgnoreCase("oracle")) {
                //有则为视图
                return rs.next();
            } else {
                if (rs.next()) {
                    String tableType = Util.null2String(rs.getString("TABLE_TYPE"));
                    if ("VIEW".equals(tableType.toUpperCase())) {
                        flag = true;
                    }
                } else {
                    flag = false; // 连表都不是
                }
            }
            return flag;
        } catch (RuntimeException e) {
            System.out.println(" checkIsView e.getMessage() = " + e.getMessage());
            return false;
        }
    }

    //判断sql 最终查询结果，给自定义浏览按钮用某个字段最终属于那个表。
    public static synchronized Map<String, Object> getFieldTableBrowserMenu(String sql, String fieldName) {

        Map<String, Object> browserMenuTable = new HashMap<>();
        try {
            String sqlNOHolder = String.valueOf(replaceAllHolder(sql).get("sqlNew"));
            //进行sql替换
            browserMenuTable = getBrowserMenuTable(sqlNOHolder, fieldName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (browserMenuTable == null) {
            browserMenuTable = new HashMap<>();
            browserMenuTable.put("browserMenuTable", "");
            browserMenuTable.put("browserMenuField", "");
        }

        return browserMenuTable;
    }

    public static Map<String, Object> getBrowserMenuTable(String new_sql, String searchFieldName) {
        SourceDBUtil sourceDBUtil = new SourceDBUtil();
        List<SQLStatement> sqlStatements_tmp = SQLUtils.parseStatements(new_sql, sourceDBUtil.getDbtype());
        Map<String, Object> resultMap = new HashMap<>();

        if (StringUtils.isBlank(searchFieldName)) {
            return resultMap;
        }
        SQLSelectQueryBlock sqlSelectQueryBlock = Optional.of(sqlStatements_tmp.get(0)).filter(x -> x instanceof SQLSelectStatement).
                                                          map(x -> (SQLSelectStatement) x).
                                                          map(sqlSelectStatement -> sqlSelectStatement.getSelect())
                                                          .map(sqlSelect -> sqlSelect.getQuery()).
                                                          filter(query -> query instanceof SQLSelectQueryBlock).
                                                          map(query -> (SQLSelectQueryBlock) query).orElse(null);
        if (sqlSelectQueryBlock != null) {

            List<SQLSelectItem> selectList = sqlSelectQueryBlock.getSelectList();

            boolean isSearchField = false;
            for (SQLSelectItem sqlSelectItem : selectList) {
                if (sqlSelectItem.getAlias() == null) {
                    SQLExpr expr = sqlSelectItem.getExpr();
                    if (expr instanceof SQLIdentifierExpr) {
                        SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) expr;
                        String name = sqlIdentifierExpr.getName();
                        if (searchFieldName.equalsIgnoreCase(name) || "*".equalsIgnoreCase(name)) {
                            isSearchField = true;
                        }
                    } else if (expr instanceof SQLPropertyExpr) {
                        SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) expr;
                        if (searchFieldName.equalsIgnoreCase(sqlPropertyExpr.getName())) {
                            isSearchField = true;
                        }
                    } else if (expr instanceof SQLAllColumnExpr) {
                        isSearchField = true;
                    }
                } else if (sqlSelectItem.getAlias().equalsIgnoreCase(searchFieldName)) {
                    isSearchField = true;
                }
            }
            SQLTableSource from = sqlSelectQueryBlock.getFrom();
            //只处理单表场景
            if (from instanceof SQLExprTableSource) {
                if (!isSearchField) {
                    selectList.add(new SQLSelectItem(new SQLIdentifierExpr(searchFieldName)));
                }
            }

            return getFieldTable(sqlSelectQueryBlock.getFrom(), searchFieldName);
        }
        return resultMap;
    }

    private static Map<String, Object> getFieldTable(SQLTableSource sqlTableSource, String searchFieldName) {

        SQLObject parent = sqlTableSource.getParent();

        List<SQLSelectItem> selectList = null;
        int det = 0;
        while (true) {
            det++;
            if (parent instanceof SQLSelectQueryBlock) {
                SQLSelectQueryBlock sqlSelectQueryBlock = (SQLSelectQueryBlock) parent;
                selectList = sqlSelectQueryBlock.getSelectList();
                break;
            } else {
                parent = parent.getParent();
            }
            if (det > 100) {
                return null;
            }
        }

        //是一个表
        if (sqlTableSource instanceof SQLExprTableSource) {
            SQLExprTableSource sqlExprTableSource = (SQLExprTableSource) sqlTableSource;

            String tableName = sqlExprTableSource.getName().getSimpleName();
            String tableAlias = sqlExprTableSource.getAlias();
            if (tableAlias == null) {
                tableAlias = tableName;
            }

            for (SQLSelectItem sqlSelectItem : selectList) {
                SQLExpr expr = sqlSelectItem.getExpr();
                String alias = sqlSelectItem.getAlias();

                //别名不相等
                if (alias != null && !searchFieldName.equalsIgnoreCase(alias)) {
                    continue;
                }
                if (expr instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) expr;
                    String fieldName = sqlIdentifierExpr.getName();
                    if (alias == null) {
                        if (searchFieldName.equalsIgnoreCase(fieldName)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    } else {
                        if (searchFieldName.equalsIgnoreCase(alias)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    }
                } else if (expr instanceof SQLPropertyExpr) {
                    SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) expr;
                    String fieldName = sqlPropertyExpr.getName();
                    String ownerName = sqlPropertyExpr.getOwnernName();

                    //字段和当前表明不相等，无需比较。
                    if (!ownerName.equalsIgnoreCase(tableAlias)) {
                        continue;
                    }

                    //能进来的不可能有这种情况
                    if ("*".equals(fieldName)) {
                        return getDBTableField(tableName, searchFieldName);
                    }
                    if (alias == null) {
                        if (searchFieldName.equalsIgnoreCase(fieldName)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    } else {
                        if (searchFieldName.equalsIgnoreCase(alias)) {
                            return getDBTableField(tableName, fieldName);
                        }
                    }
                } else if (expr instanceof SQLAllColumnExpr) {
                    return getDBTableField(tableName, searchFieldName);
                } else if (expr instanceof SQLAggregateExpr) {
                    SQLAggregateExpr sqlAggregateExpr = (SQLAggregateExpr) expr;
                    List<SQLExpr> arguments = sqlAggregateExpr.getArguments();
                    if (arguments.size() == 1) {
                        sqlSelectItem.setExpr(arguments.get(0));
                        return getFieldTable(sqlTableSource, searchFieldName);
                    } else {
                        return null;
                    }
                } else if (expr instanceof SQLCastExpr) {
                    SQLCastExpr sqlCastExpr = (SQLCastExpr) expr;
                    sqlSelectItem.setExpr(sqlCastExpr.getExpr());
                    return getFieldTable(sqlTableSource, searchFieldName);
                } else {
                    //别名相同但不是可以判断的类型返回 null
                    if (sqlSelectItem.getAlias() != null && sqlSelectItem.getAlias().equalsIgnoreCase(searchFieldName)) {
                        return null;
                    }
                }
            }
        } else if (sqlTableSource instanceof SQLJoinTableSource) {
            SQLJoinTableSource sqlJoinTableSource = (SQLJoinTableSource) sqlTableSource;
            SQLTableSource left = sqlJoinTableSource.getLeft();
            SQLTableSource right = sqlJoinTableSource.getRight();
            Map<String, Object> fieldTableLeft = getFieldTable(left, searchFieldName);
            if (fieldTableLeft != null) {
                return fieldTableLeft;
            }
            Map<String, Object> fieldTableRight = getFieldTable(right, searchFieldName);
            if (fieldTableRight != null) {
                return fieldTableRight;
            }
        } else if (sqlTableSource instanceof SQLSubqueryTableSource) {
            SQLSubqueryTableSource sqlSubqueryTableSource = (SQLSubqueryTableSource) sqlTableSource;
            String tableAlias = sqlSubqueryTableSource.getAlias();

            SQLTableSource sqlExprTableSource = Optional.ofNullable(sqlSubqueryTableSource).
                                                        map(x -> x.getSelect()).
                                                        map(x -> x.getQuery()).
                                                        filter(x -> x instanceof SQLSelectQueryBlock).
                                                        map(x -> (SQLSelectQueryBlock) x).
                                                        map(x -> x.getFrom()).orElse(null);

            if (sqlExprTableSource != null) {
                for (SQLSelectItem sqlSelectItem : selectList) {
                    SQLExpr expr = sqlSelectItem.getExpr();
                    String alias = sqlSelectItem.getAlias();
                    if (expr instanceof SQLIdentifierExpr) {
                        SQLIdentifierExpr sqlIdentifierExpr = (SQLIdentifierExpr) expr;
                        String fieldName = sqlIdentifierExpr.getName();
                        if (alias == null) {
                            if (searchFieldName.equalsIgnoreCase(fieldName)) {
                                //return getTableField(tableName,fieldName);
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        } else {
                            if (searchFieldName.equalsIgnoreCase(alias)) {
                                //return getTableField(tableName,fieldName);
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        }
                    } else if (expr instanceof SQLPropertyExpr) {
                        SQLPropertyExpr sqlPropertyExpr = (SQLPropertyExpr) expr;
                        String fieldName = sqlPropertyExpr.getName();
                        String ownerName = sqlPropertyExpr.getOwnernName();
                        //字段和当前表明不相等，无需比较。
                        if (!ownerName.equalsIgnoreCase(tableAlias)) {
                            continue;
                        }
                        //能进来的不可能有这种情况
                        if ("*".equals(fieldName)) {
                            //return getTableField(tableName,alias);
                            return getFieldTable(sqlExprTableSource, searchFieldName);
                        }
                        if (alias == null) {
                            if (searchFieldName.equalsIgnoreCase(fieldName)) {
                                //return getTableField(tableName,fieldName);
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        } else {
                            if (searchFieldName.equalsIgnoreCase(alias)) {
                                return getFieldTable(sqlExprTableSource, fieldName);
                            }
                        }
                    } else if (expr instanceof SQLAllColumnExpr) {
                        return getFieldTable(sqlExprTableSource, searchFieldName);
                    } else if (expr instanceof SQLAggregateExpr) {
                        if (sqlSelectItem.getAlias() == null) {
                            continue;
                        }
                        SQLAggregateExpr sqlAggregateExpr = (SQLAggregateExpr) expr;
                        List<SQLExpr> arguments = sqlAggregateExpr.getArguments();
                        if (arguments.size() == 1 && arguments.get(0) instanceof SQLName) {
                            SQLName sqlName = (SQLName) arguments.get(0);
                            return getFieldTable(sqlExprTableSource, sqlName.getSimpleName());
                        } else {
                            return null;
                        }
                    } else if (expr instanceof SQLCastExpr) {
                        if (sqlSelectItem.getAlias() == null) {
                            continue;
                        }
                        SQLCastExpr sqlCastExpr = (SQLCastExpr) expr;
                        if (sqlCastExpr.getExpr() instanceof SQLName) {
                            SQLName sqlName = (SQLName) sqlCastExpr.getExpr();
                            return getFieldTable(sqlExprTableSource, sqlName.getSimpleName());
                        } else {
                            return null;
                        }
                    } else {
                        //别名相同但不是可以判断的类型返回 null
                        if (sqlSelectItem.getAlias() != null && sqlSelectItem.getAlias().equalsIgnoreCase(searchFieldName)) {
                            return null;
                        }
                    }
                }
            }
        } else if (sqlTableSource instanceof SQLWithSubqueryClause.Entry) {
            SQLWithSubqueryClause.Entry entry = (SQLWithSubqueryClause.Entry) sqlTableSource;
        }
        return null;
    }

    private static Map<String, Object> getDBTableField(String tableName, String fieldName) {
        Map<String, Object> resultMap = new HashMap<>();
        if (isFieldInTable(tableName, fieldName)) {
            resultMap.put("browserMenuTable", tableName);
            resultMap.put("browserMenuField", fieldName);
        } else {
            return null;
        }
        return resultMap;
    }

    /**
     * 先将sql中的占位符用随机数代替
     *
     * @param sql
     * @return
     */
    public static Map<String, Object> replaceAllHolder(String sql) {
        Map<String, Object> result = new HashMap<>();
        Map<String, String> holderNum = new HashMap<>();
        List<String> holderList = new ArrayList<>();
        String sqlNew = sql;
        // 取出$$这种占位符参数
        String regex1 = "\\$[A-Za-z0-9\\_\\.]+\\$";
        Matcher matcher1 = Pattern.compile(regex1).matcher(sql);
        while (matcher1.find()) {
            String group = matcher1.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
            }
        }
        // 取出{?userid}这种系统参数
//        String regex2 = "\\{[A-Za-z0-9\\_\\.\\?]+\\}";
        String regex2 = "\\{[A-Za-z0-9_\\.\\?\\u4e00-\\u9fa5]+\\}";
        Matcher matcher2 = Pattern.compile(regex2).matcher(sql);
        while (matcher2.find()) {
            String group = matcher2.group(0);
            if (!holderList.contains(group)) {
                holderList.add(group);
            }
        }
        for (String holder : holderList) {
            String num = String.valueOf(IdGenerator.getUUID());
            sqlNew = sqlNew.replace(holder, num);
            holderNum.put(num, holder);
        }
        {
            String num = String.valueOf(IdGenerator.getUUID());
            sqlNew = sqlNew.replace("?", num);
            holderNum.put(num, "?");
        }
        result.put("sqlNew", sqlNew);
        result.put("holderNum", holderNum);
        return result;
    }

    public static String null2String(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    /**
     * @param tableName
     * @param fieldName
     * @return
     */
    public static boolean isFieldInTable(String tableName, String fieldName) {
        //存在小数点，截取小数点后边的
        if (fieldName.contains(".")) {
            fieldName = fieldName.substring(fieldName.indexOf(".") + 1);
        }
        initCache();
        //找不到去库里找
        return workflowTableMap
                .get("11111")
                .contains(
                        tableName.toLowerCase() + "|sys|" + fieldName.toLowerCase()) ? true : false;
    }

    public static String getAllTableColumns(String dbtype) {
        if ("oracle".equalsIgnoreCase(dbtype)) {
            return "select TABLE_NAME as tablename，COLUMN_NAME as fieldname from  USER_TAB_COLUMNS ";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            return "select table_name as tablename,column_name as fieldname from information_schema.columns  where  TABLE_SCHEMA = (select database())  ";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return "select table_name as  tablename,column_name as fieldname from information_schema.columns WHERE table_schema=(SELECT current_schema())  order by tablename ";
        } else {
            return " select object_name(id) as tablename,name as fieldname  from syscolumns ";
        }
    }

    public static Map<String, HashSet<String>> workflowTableMap = new ConcurrentHashMap<>();

    public static synchronized void initCache() {
        if (workflowTableMap.size() == 0) {
            HashSet<String> tableFieldSet = new HashSet<>();
            //放入缓存
            workflowTableMap.put("11111", tableFieldSet);
            //dataSync
            SourceDBUtil sourceDBUtil = new SourceDBUtil();
            String sql = getAllTableColumns(sourceDBUtil.getDbtype());
            sourceDBUtil.executeQuery(sql);
            while (sourceDBUtil.next()) {
                String e9_tablename = null2String(sourceDBUtil.getString("tablename"))
                        .trim()
                        .toLowerCase();
                String e9_fieldname = null2String(sourceDBUtil.getString("fieldname"))
                        .trim()
                        .toLowerCase();
                tableFieldSet.add(e9_tablename + "|sys|" + e9_fieldname);
                //存一份表的对应关系
                tableFieldSet.add(e9_tablename + "|sys|");
            }
        }
    }

    public static String printSql(String sql) {
        try {
            if (StringUtils.isBlank(sql)) {
                return "";
            }
            String formattedSql = SqlFormatter.format(sql);
            return formattedSql.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return sql;
        }
    }

}
