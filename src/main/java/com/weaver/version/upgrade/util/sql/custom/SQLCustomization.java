package com.weaver.version.upgrade.util.sql.custom;

import java.util.HashMap;
import java.util.List;

/**
 * SQL自定义转换接口
 * 提供针对SQL中的表名、字段名及条件值的定制化转换方法。
 *
 * @author wujiahao
 * @date 2024/11/20 下午1:54
 */
public interface SQLCustomization {

    /**
     * 自定义E10表名转换
     * 用于主表字段  ”E10表名称	“ 自定义java类获取值。
     *
     * @param sourceTableName      源系统中的表名
     * @param conditions           SQL中解析出针对E9表的简单条件
     * @param usedColumns          SQL中使用到E9表的所有的列
     * @return                     返回E9表对应E10主表名称
     */
    String transformTableName(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns);

    /**
     * 自定义join语句的表名
     * 用于明细表字段  ”字段对应E10表名称“ 自定义java类获取值。
     *
     * @param e9tableName
     * @param conditionMap
     * @param usedColumns
     * @return 返回E10字段真实表明
     */
    String transformJoinTable(String e9tableName, HashMap<String, String> conditionMap, List<String> usedColumns);
    /**
     * 自定义join语句的转换
     * 用于明细表字段  ”自定义值转换设置“ 自定义java类获取值。
     *
     * @param sourceTableName      源系统中的表名
     * @param conditions           SQL中解析出针对E9表的简单条件
     * @param usedColumns          SQL中使用到E9表的所有的列
     * @return                     目标系统中的自定义sql关联条件
     */
    String transformJoinStatement(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns);

    /**
     * 自定义SQL字段名转换
     * 用于明细表字段  ”E10字段“ 自定义java类获取值。
     *
     * @param sourceColumnName     源系统中的字段名
     * @param sourceTableName      SQL中解析出针对E9表的简单条件
     * @param conditions           转换时需要的条件映射
     * @param usedColumns          SQL中使用到E9表的所有的列
     * @return                     目标系统中的自定义字段名
     */
    String transformColumnName(String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns);

    /**
     * 自定义SQL条件值转换
     * 用于明细表字段  ”自定义值转换设置“ 自定义java类获取值。
     *
     * 用于SQL中值E10转换
     * @param sourceValue          源系统中的条件值
     * @param sourceColumnName     源系统中的字段名
     * @param sourceTableName      源系统中的表名
     * @param conditions           SQL中解析出针对E9表的简单条件
     * @param usedColumns          SQL中使用到E9表的所有的列
     * @return                     目标系统中的自定义条件值
     */
    String transformConditionValue(String sourceValue, String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns);
}
