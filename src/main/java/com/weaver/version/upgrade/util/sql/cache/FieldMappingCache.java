package com.weaver.version.upgrade.util.sql.cache;

import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.ast.statement.SQLSelectItem;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBean;
import com.weaver.version.upgrade.util.sql.bean.MigrateTableMappingBeanDetail;
import com.weaver.version.upgrade.util.sql.bean.TableBeanHasAlias;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;


/**
 * @author wujiahao
 * @date 2023/12/8 16:05
 */
public class FieldMappingCache {
    private static final Logger logger = LoggerFactory.getLogger(FieldMappingCache.class);


    /**
     * 表映射关系
     */
    public volatile static Map<String, MigrateTableMappingBean> tablefiledMappingMap;

    /**
     * 标准e9 所有表字段映射关系
     */
    public static HashSet<String> e9SysFieldSet;


    /**
     * 用于判断是否是建模或者流程表
     */
    public static Map<String, HashSet<String>> workflowTableMap = new ConcurrentHashMap<>();


    /**
     * 存储一列特殊字段迁移方法
     */
    public static Map<String, Object> matrixFieldIdMapping = new ConcurrentHashMap<>();



    public static String replaceTable(String tableName) {
        SQLExprTableSource sqlExprTableSource=new SQLExprTableSource();
        sqlExprTableSource.setExpr(tableName);
        return replaceTable(sqlExprTableSource);
    }
    /**
     * 获取E10表明
     *
     * @param tableName
     * @param
     * @param
     * @return
     */
    public static String replaceTable(SQLExprTableSource sqlExprTableSource  ) {

          String  tableName = Optional.ofNullable(sqlExprTableSource)
                .map(y -> y.getName())
                .map(z -> z.getSimpleName())
                .map(String::toLowerCase)
                .orElse("");


        final String oldTableName = tableName;

        if (tableName.equalsIgnoreCase("wfc_form_data")) {
            return tableName;
        }

        //oracle 特殊处理
        if (tableName.equalsIgnoreCase("dual")) {
            return tableName;
        }

        Map<String, Object> attributes = sqlExprTableSource.getAttributes();
        List<String> tableUseField = new ArrayList<>();
        if (attributes.containsKey("tableUseField")) {
            tableUseField = ((List<String>) attributes.get("tableUseField")).stream().distinct().filter(fieldName -> FieldMappingCache.isSystemField(oldTableName, fieldName)).map(fieldName -> fieldName.toLowerCase()).collect(Collectors.toList());
        }



        String newTableName = tableName;
        HashSet<String> hashSet = workflowTableMap.get(TransUtil.getTenantKey());
        //流程表或者建模表，或者流程建模明细表，直接返回原值。
        if(hashSet.contains(tableName + "|edc_det|")
                || hashSet.contains(tableName + "|edc|")){
            newTableName= SqlCache.replaceEdcTableName(tableName);
            return newTableName;

        } else if (hashSet.contains(tableName + "|wf|")
                || hashSet.contains(tableName + "|cube|")
                || hashSet.contains(tableName + "|cube_det|")
                || hashSet.contains(tableName + "|wf_det|")
        ) {
            if ((hashSet.contains(tableName + "|wf|") && hashSet.contains(tableName + "|cube|")) || (hashSet.contains(tableName + "|cube_det|") && hashSet.contains(tableName + "|wf_det|"))) {
                TransUtil.addTransErrorMsg("表->" + tableName + "流程和建模均有使用，E10将其进行了拆分 "+SqlCache.getE10TableName(tableName)+"，请手动确认替换逻辑 ");
                newTableName = tableName;
            } else {
                newTableName = SqlCache.getE10TableName(tableName);
            }

            if (newTableName.contains(",")) {
                TransUtil.addTransErrorMsg("E9 多个建模使用同一张表单E10将数据拆分到表 " + newTableName + " 请手动确认! ");
                newTableName = newTableName.split(",")[0];
            }
            if (!newTableName.equalsIgnoreCase(tableName)) {
                TransUtil.addTransMsg("将E9表" + tableName + "替换为=>" + newTableName);
            }
            sqlResultInfo.put("e9tableName="+newTableName,tableName);

//            if (hashSet.contains(tableName + "|wf|")&&!hashSet.contains(tableName + "|cube|") && (tableUseField.contains("requestid") || tableUseField.contains("*"))) {
//                TransUtil.addTransMsg("给流程表单=>" + tableName + "关联了wfc_form_data");
//                newTableName = "(select b.*,a.requestid\n" +
//                        "      from wfc_form_data a,\n" +
//                        "           " + newTableName +
//                        "  b    where a.dataid = b.form_data_id and a.delete_type=0 and a.tenant_key='" + TransUtil.getTenantKey() + "')";
//            }
   return newTableName;

        }

        //矩阵
        if (tableName.startsWith("matrixtable")) {
            newTableName= FieldMappingCache.buildMatrixE10sql(tableUseField, tableName);
        }
        //替换了
        if (!newTableName.equalsIgnoreCase(tableName)){
            return newTableName;
        }


        if (tablefiledMappingMap.containsKey(tableName)) {
            MigrateTableMappingBean migrateTableMappingBean = tablefiledMappingMap.get(tableName);
            String e10Table = migrateTableMappingBean.getE10Table();
            if (TransUtil.isCusSQL(e10Table)) {
                e10Table = TransUtil.getCusSQLTableName(sqlExprTableSource,  e10Table);
            }
            TransUtil.addTransMsg("将标准E9表" + oldTableName + "替换为=>" + e10Table);
            return e10Table;
        }

        SqlCache.issystemView(tableName);



        ///不是oa新建的表，手动确认。
        if (!isSysTable(tableName)) {
            //TransUtil.addCustomTable("表=>" + tableName + " 不是oa中的表,请手动确认");
            if (SqlCache.issystemView(tableName)) {
            } else {
                TransUtil.addCustomTable("表=>" + tableName + " 不是oa中的标准表,请手动确认");
            }
            return tableName;
        } else if (tableName.startsWith("workflow_form") || tableName.startsWith("cus_fielddata")) {
            TransUtil.addTransErrorMsg("表" + tableName + "替换逻辑复杂,请参考以下文档地址进行替换->https://www.e-cology.com.cn/sp/file/filePreview/100500130029763169");
        } else if (tableName.equalsIgnoreCase("fnainvoiceledger") || tableName.equalsIgnoreCase("motor_VehicleInvoice") || tableName.equalsIgnoreCase("second_carInvoice") || tableName.equalsIgnoreCase("machineInvoice") || tableName.equalsIgnoreCase("taxiInvoice") || tableName.equalsIgnoreCase("trainInvoice") || tableName.equalsIgnoreCase("carInvoice") || tableName.equalsIgnoreCase("airInvoice") || tableName.equalsIgnoreCase("airDtlInvoice") || tableName.equalsIgnoreCase("tollInvoice")) {
            TransUtil.addTransErrorMsg("表=>" + tableName + " 是发票云中的表，E10无法替换，需要配置成ESB动作流带出发票信息 ,请参考以下文档地址进行替换->https://www.e-cology.com.cn/sp/file/filePreview/100500130029763169");
        } else {
            TransUtil.addTransErrorMsg("表=>" + tableName + " 在E10中未找到对应关系,请手动确认");
        }


        if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|sys|")) {
            if (tableCount.containsKey(tableName)) {
                Integer integer = tableCount.get(tableName);
                tableCount.put(tableName, 1 + integer);
            } else {
                tableCount.put(tableName, 1);
            }
        }
        return tableName;
    }


    public static String replaceTableFiled(String tableName, String fieldName) {
        SQLExprTableSource sqlExprTableSource=new SQLExprTableSource();
        sqlExprTableSource.setSimpleName(tableName);
        return replaceTableFiled(tableName, fieldName, sqlExprTableSource);
    }

    /***
     * 获取E10新列明
     * @param tableName
     * @param fieldName
     * @return
     */
    public static String replaceTableFiled(String tableName, String fieldName,  SQLExprTableSource sqlExprTableSource) {
        tableName = tableName.toLowerCase();
        fieldName = fieldName.toLowerCase();


        //记录出现了多少个字段，用于计算SQL复杂度
        HashSet tableSet = (HashSet) sqlResultInfo.getOrDefault("tableField", new HashSet<>());
        tableSet.add(tableName+"///"+fieldName);
        sqlResultInfo.put("tableField", tableSet);

        if (fieldName.equalsIgnoreCase("*")) {
            TransUtil.addTransErrorMsg("存在 select * 未指定查询列，请手动确认!!!");
            return fieldName;
        }


        if (tableName.equalsIgnoreCase("dual")) {
            return fieldName;
        }
        if (tableName.startsWith("matrixtable")) {
            return fieldName;
        }
        if (tableName.equalsIgnoreCase("dual") || tableName.equalsIgnoreCase("hrmdepartmentdefined") || tableName.equalsIgnoreCase("hrmsubcompanydefined")) {
            return fieldName;
        }






        if (tableName.equalsIgnoreCase("cus_fielddata")) {

            if (matrixFieldIdMapping.containsKey(TransUtil.getTenantKey() + "_cus_" + fieldName)) {


            } else {
                if (!isSystemField(tableName, fieldName)) {
                    TransUtil.addCustomField("表=" + tableName + "中字段=>" + fieldName + " 在不是oa中标准字段,请手动确认");
                    return fieldName;
                } else {
                    TransUtil.addTransMsg("E9表cus_fielddata 对应E10表中不存在字段" + fieldName + "请确认！！！");
                }

            }
            return fieldName;
        }
        //流程表或者建模表，或者流程建模明细表，直接返回原值。
        if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|wf|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|cube|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|cube_det|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|wf_det|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|edc_det|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|edc|")) {

            final String oldFieldName = fieldName;
            if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|cube_det|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|wf_det|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|edc_det|")) {
                fieldName = SqlCache.getE10FieldName(tableName, fieldName, true);
            } else {
                fieldName = SqlCache.getE10FieldName(tableName, fieldName, false);
            }


            if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|cube|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|edc|")) {
                if (fieldName.equalsIgnoreCase("requestid")) {
                    fieldName = "'E10无此字段" + fieldName + "'";
                }
            }

            if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|edc_det|") || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|edc|")) {
//                //1.mainid改成FORM_DATA_ID
//                2.sort,taskid,uuid,formmodeid,modedatacreatertype,modedatacreatetime 无
//
//                3.other_field开头的字段无
//                4._lng结尾的要改成_long
                if (fieldName.equalsIgnoreCase("taskid") || fieldName.equalsIgnoreCase("uuid") || fieldName.startsWith("other_field")) {
                    fieldName = "'E10无此字段" + fieldName + "'";
                    TransUtil.addTransMsg("E9表单字段名称:" + fieldName + " 在E10没有对应字段,请手动确认");
                }
                fieldName = fieldName.replaceAll("_lng\\b", "_long");
                if (fieldName.equalsIgnoreCase("sort")) {
                    fieldName = "DATA_INDEX";
                }
            }


            if (!oldFieldName.equalsIgnoreCase(fieldName)) {
                TransUtil.addTransMsg("E10关键字变更,将E9表" + tableName + "中字段=> " + oldFieldName + " 替换为=>" + fieldName);
            }
            return fieldName;
        }
        //标准替换逻辑
        if (tablefiledMappingMap.containsKey(tableName)) {

            MigrateTableMappingBean list1 = tablefiledMappingMap.get(tableName);
            List<MigrateTableMappingBeanDetail> migrateTableMappingBeanDetails = list1.getMigrateTableMappingBeanDetails();


            for (MigrateTableMappingBeanDetail migrateTableMappingBeanDetail : migrateTableMappingBeanDetails) {
                if (fieldName.equalsIgnoreCase(migrateTableMappingBeanDetail.getE9TableField())) {
                    boolean isQuery = (boolean) Optional.ofNullable(sqlResultInfo.get("isQuery")).orElse(true);
                    String e10TableField = migrateTableMappingBeanDetail.getE10TableField();


                    if (TransUtil.isCusSQL(e10TableField)) {
                        e10TableField = TransUtil.getCusSQLFieldName(sqlExprTableSource,fieldName,e10TableField );
                    }


                    if ("1".equalsIgnoreCase(migrateTableMappingBeanDetail.getJoinType())&& !isQuery) {
                        TransUtil.addTransErrorMsg("将标准E9表" + tableName + "中字段=> " + fieldName + " 替换为=>" + e10TableField + " 此字段E10进行了拆分，此字段E10所属表为" + migrateTableMappingBeanDetail.getE10TableName() + "请手动确认!");
                    } else {
                        TransUtil.addTransMsg("将标准E9表" + tableName + "中字段=> " + fieldName + " 替换为=>" + e10TableField);
                    }

                    return e10TableField;
                }
            }
        }

        if (fieldExistsInTable(tableName, fieldName)){
            //处理 if ("{$other$}".equalsIgnoreCase(fieldName))
            if (tablefiledMappingMap.containsKey(tableName)) {
                Optional<MigrateTableMappingBeanDetail> first = tablefiledMappingMap.get(tableName).getMigrateTableMappingBeanDetails().stream().filter(x -> "{$other$}".equalsIgnoreCase(x.getE9TableField())).findFirst();
                if (first.isPresent()) {
                    MigrateTableMappingBeanDetail migrateTableMappingBeanDetail = first.get();
                    if (TransUtil.isCusSQL(migrateTableMappingBeanDetail.getE10TableField())) {
                        return TransUtil.getCusSQLFieldName(sqlExprTableSource, fieldName, migrateTableMappingBeanDetail.getE10TableField());
                    }
                }
                TransUtil.addTransErrorMsg("表=" + tableName + "中字段=>" + fieldName + " 在E10中未找到对应关系,请手动确认");
            }
        }

        if (!isSystemField(tableName, fieldName)) {
            TransUtil.addCustomField("表=" + tableName + "中字段=>" + fieldName + " 在不是oa中标准字段,请手动确认");
            return fieldName;
        }

        //记录一些替换信息
        if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|sys|" + fieldName)) {
            //转换数据专区
            if (FieldCount.containsKey(tableName + "^" + fieldName)) {
                Integer integer = FieldCount.get(tableName + "^" + fieldName);
                FieldCount.put(tableName + "^" + fieldName, 1 + integer);
            } else {
                FieldCount.put(tableName + "^" + fieldName, 1);
            }
        }

        return fieldName;
    }

    /**
     * 替换字段节点
     * @param tableName
     * @param sqlExpr
     * @return
     */
    public static SQLExpr replaceTableFiledNew(List<TableBeanHasAlias> tableNames, SQLExpr sqlExpr) {




        String fieldName = "";
        if (sqlExpr instanceof SQLPropertyExpr) {
            SQLPropertyExpr sqlExpr_tmp = (SQLPropertyExpr) sqlExpr;
            fieldName = sqlExpr_tmp.getName().toLowerCase();
        } else if (sqlExpr instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr sqlExpr_tmp = (SQLIdentifierExpr) sqlExpr;
            fieldName = sqlExpr_tmp.getName().toLowerCase();
        }


        for (TableBeanHasAlias tableBeanHasAlias : tableNames) {
            if ("*".equals(fieldName)) {
                break;
            }
            String tableName = tableBeanHasAlias.getTableName().toLowerCase();


            if (fieldExistsInTable(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase())) {


                //记录出现了多少个字段
                //记录出现了多少个字段，用于计算SQL复杂度
                HashSet tableSet = (HashSet) sqlResultInfo.getOrDefault("tableField", new HashSet<>());
                tableSet.add(tableBeanHasAlias.getTableName().toLowerCase()+"///"+fieldName);
                sqlResultInfo.put("tableField", tableSet);

                //流程表单特殊判断
                String tableName_tmp = tableBeanHasAlias.getTableName().toLowerCase();
                if (workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|wf|")
                        || workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|wf_det|")){
                    tableName_tmp="formtable_main_";
                }


                //处理子查询
                if (tablefiledMappingMap.containsKey(tableName_tmp)) {
                    String finalFieldName = fieldName;
                    Optional<MigrateTableMappingBeanDetail> first = tablefiledMappingMap.get(tableName_tmp)
                            .getMigrateTableMappingBeanDetails().stream()
                            .filter(x -> finalFieldName.equalsIgnoreCase(x.getE9TableField())).findFirst();
                    if(first.isPresent()) {
                        MigrateTableMappingBeanDetail migrateTableMappingBeanDetail = first.get();
                        String fieldType = migrateTableMappingBeanDetail.getZdzhsz();
                        if (("3".equals(fieldType) || "4".equals(fieldType) || "5".equals(fieldType))
                                && TransUtil.isCusSQL(migrateTableMappingBeanDetail.getSqlQuery())) {
                            String jsonMapping = migrateTableMappingBeanDetail.getSqlQuery();




                            jsonMapping = jsonMapping.replaceAll("\\{\\$(.*?)\\$\\}", tableBeanHasAlias.getTableAlias());
                            SQLQueryExpr sqlQueryExpr = TransUtil.buildSQLSelect(jsonMapping);
                            if ("3".equals(fieldType)) {
                                //使用子查询
                            } else if ("4".equals(fieldType)) {
                                //行转列使用逗号分隔
                                TransUtil.buildConcat(sqlQueryExpr, false);
                            } else if ("5".equals(fieldType)) {
                                //最外层拼接逗号
                                TransUtil.buildConcat(sqlQueryExpr, true);
                            }
                            return sqlQueryExpr;
                        }
                    }
                }





                String fieldNameNew = replaceTableFiled(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase(),tableBeanHasAlias.getSqlExprTableSource());

                SQLExpr sqlExprNew = null;

                String tableAlias = tableBeanHasAlias.getTableAlias();
                //处理表单建模取消了的字段
                if (fieldNameNew.startsWith("'E10无此字段")) {
                    sqlExprNew = new SQLIdentifierExpr(fieldNameNew);
                } else if (sqlExpr instanceof SQLPropertyExpr) {
                    SQLPropertyExpr sqlExpr_tmp = (SQLPropertyExpr) sqlExpr;
                    sqlExpr_tmp.setName(fieldNameNew);
                    sqlExprNew = sqlExpr_tmp;
                } else if (sqlExpr instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr sqlExpr_tmp = (SQLIdentifierExpr) sqlExpr;
                    sqlExpr_tmp.setName(fieldNameNew);
                    sqlExprNew = new SQLPropertyExpr(tableAlias, sqlExpr_tmp.getName());
                }

                //需要分隔时间字符串
                if (sqlExpr.getParent() instanceof SQLSelectItem || sqlExpr.getParent() instanceof SQLMethodInvokeExpr || sqlExpr.getParent() instanceof SQLBinaryOpExpr) {
                    return TransUtil.dateSplit(tableNames, fieldName, sqlExprNew);
                }

                return sqlExprNew;
            }
        }

        if (!"*".equals(fieldName)) {
            TransUtil.addTransErrorMsg("字段" + fieldName + "数据库不存在请确认！！！");
        }
        return sqlExpr;
    }


    /**
     * @param tableName
     * @param fieldName
     * @param isAlias   返回结果是否拼接别名
     * @return
     */
    public static String replaceTableFiled(List<TableBeanHasAlias> tableName, String fieldName, boolean isAlias) {

        //不需要 返回 表明+别名
        if (fieldName.indexOf(".") > -1) {
            fieldName = fieldName.substring(fieldName.lastIndexOf(".") + 1);
        }

        for (TableBeanHasAlias tableBeanHasAlias : tableName) {
            if (fieldExistsInTable(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase())) {
                String fieldNameNew = replaceTableFiled(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase(),tableBeanHasAlias.getSqlExprTableSource());
                if (!fieldNameNew.equalsIgnoreCase(fieldName)) {
                    String tableAlias = tableBeanHasAlias.getTableAlias();
                    if (isAlias) {
                        if (tableAlias == null || "".equals(tableAlias)) {
                            return tableBeanHasAlias.getTableName() + "." + fieldNameNew;
                        } else {
                            return tableAlias + "." + fieldNameNew;
                        }
                    }
                    return fieldNameNew;
                }
            }
        }
        return fieldName;
    }


    public static String getFieldInTable(List<TableBeanHasAlias> tableName, String fieldName, boolean isAlias) {


        //不需要 返回 表明+别名
        if (fieldName.indexOf(".") > -1) {
            fieldName = fieldName.substring(fieldName.lastIndexOf(".") + 1);
        }

        for (TableBeanHasAlias tableBeanHasAlias : tableName) {
            if (fieldExistsInTable(tableBeanHasAlias.getTableName().toLowerCase(), fieldName.toLowerCase())) {
                return tableBeanHasAlias.getTableName().toLowerCase();
            }
        }
        return null;
    }


    /**
     * 判断E9表里是否存在此字段
     * @param tableName
     * @param fieldName
     * @return
     */
    public static boolean fieldExistsInTable(String tableName, String fieldName) {
        tableName = tableName.toLowerCase();
        fieldName = fieldName.toLowerCase();
        //存在小数点，截取小数点后边的
        if (fieldName.contains(".")) {
            fieldName = fieldName.substring(fieldName.indexOf(".") + 1);
        }
        //先去映射关系中找
        if (tablefiledMappingMap.containsKey(tableName)) {
            String finalFieldName1 = fieldName;
            long count = tablefiledMappingMap.get(tableName).getMigrateTableMappingBeanDetails().stream().filter(x -> finalFieldName1.equalsIgnoreCase(x.getE9TableField())).count();
            if (count>0) {
                return true;
            }
            //return tablefiledMappingMap.get(tableName).stream().anyMatch(bean -> finalFieldName.equalsIgnoreCase(bean.getE9TableField()));
        }
        //找不到去库里找
        return workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName.toLowerCase() + "|sys|" + fieldName.toLowerCase()) ? true : false;

    }

    /**
     * 判断是不是oa中新建的表
     *
     * @param tableName
     * @return
     */
    public static boolean isSysTable(String tableName) {
        tableName = tableName.toLowerCase().trim();
        if (e9SysFieldSet.contains(tableName + "|sys|")) {
            return true;
        }
        HashSet<String> hashSet = workflowTableMap.get(TransUtil.getTenantKey());
        if ((hashSet.contains(tableName + "|wf|") || hashSet.contains(tableName + "|cube|")) || (hashSet.contains(tableName + "|cube_det|") || hashSet.contains(tableName + "|wf_det|"))) {
            return true;
        }
        if (tableName.startsWith("edc_") || tableName.startsWith("ecme_tableextend_") || tableName.startsWith("wr_individualitem_data_") || tableName.startsWith("htmllabelinfo_") || tableName.startsWith("transidmap") || tableName.startsWith("e10configtranstable") || tableName.startsWith("htmllabelindex_") || tableName.startsWith("e9_method_info")) {
            return true;
        }
        return false;
    }

    /**
     * 判断是不是oa中新建的字段
     *
     * @param tableName
     * @return
     */

    public static boolean isSystemField(String tableName, String fieldName) {
        tableName = tableName.toLowerCase().trim();
        fieldName = fieldName.toLowerCase().trim();


        //先去映射关系中找
        if (tablefiledMappingMap.containsKey(tableName)) {
            String finalFieldName1 = fieldName;
            long count = tablefiledMappingMap.get(tableName).getMigrateTableMappingBeanDetails().stream().filter(x -> finalFieldName1.equalsIgnoreCase(x.getE9TableField())).count();
            if (count>0) {
                return true;
            }
        }
        if (e9SysFieldSet.contains(tableName + "|sys|" + fieldName)) {
            return true;
        }

//        HashSet<String> hashSet = workflowTableMap.get(TransUtil.getTenantKey());
//        if ((hashSet.contains(tableName + "|wf|") || hashSet.contains(tableName + "|cube|"))
//                || (hashSet.contains(tableName + "|cube_det|")
//                || hashSet.contains(tableName + "|wf_det|"))) {
//            return true;
//        }
//        if (tableName.startsWith("edc_")
//                || tableName.startsWith("ecme_tableextend_")) {
//            return true;
//        }

        return false;
    }


    /***
     * 获取id替换新id映射关系。
     * @param fieldName
     * @param tabelName
     * @return
     */
    public static MigrateTableMappingBeanDetail getTableMapping(String fieldName, String tabelName) {
        tabelName = tabelName.toLowerCase().trim();
        fieldName = fieldName.toLowerCase().trim();
        final String finalFieldName = fieldName;



        if (tablefiledMappingMap.containsKey(tabelName)) {
            MigrateTableMappingBean migrateTableMappingBean = tablefiledMappingMap.get(tabelName);

            Optional<MigrateTableMappingBeanDetail> firstMi = tablefiledMappingMap.get(tabelName)
                    .getMigrateTableMappingBeanDetails()
                    .stream().filter(x-> finalFieldName.equalsIgnoreCase(x.getE9TableField())).filter(migrateTableMappingBeanDetail ->
                            "1".equalsIgnoreCase(migrateTableMappingBeanDetail.getFieldType())
                                    || "2".equalsIgnoreCase(migrateTableMappingBeanDetail.getFieldType())
                                    || "4".equalsIgnoreCase(migrateTableMappingBeanDetail.getFieldType())).findFirst();
            if (firstMi.isPresent()) {
                return firstMi.get();
            }
            //只维护了表对应关系
            if (fieldName.equalsIgnoreCase("id")) {
                MigrateTableMappingBeanDetail migrateTableMappingBeanDetail = new MigrateTableMappingBeanDetail();
                migrateTableMappingBeanDetail.setE10TableName(TransUtil.null2String(migrateTableMappingBean.getE10Table()));
                migrateTableMappingBeanDetail.setE9relevancyfi("id");
                migrateTableMappingBeanDetail.setE9relevancyta(TransUtil.null2String(migrateTableMappingBean.getE9Table()));
                migrateTableMappingBeanDetail.setRelevancyfiel("id");
                migrateTableMappingBeanDetail.setRelevancytabl(TransUtil.null2String(migrateTableMappingBean.getE10Table()));
                return migrateTableMappingBeanDetail;
            }
        }

        return null;
    }

    /**
     * 构建FormSQL及子查询
     *
     * @return
     */
    public static String buildFromSql(List<String> list, SQLExprTableSource sqlExprTableSource) {


        if (list == null || list.size() == 0) {
            return null;
        }

        final  String  oldTableNameLower = Optional.ofNullable(sqlExprTableSource)
                .map(y -> y.getName())
                .map(z -> z.getSimpleName())
                .map(String::toLowerCase)
                .orElse("");



        // 去重
        List<String>  list_tmp = list.stream().map(s -> s.contains(".") ? s.substring(s.indexOf(".") + 1) : s).filter(x->fieldExistsInTable(oldTableNameLower,x)).distinct().collect(Collectors.toList());
        if (tablefiledMappingMap.containsKey(oldTableNameLower)) {
            MigrateTableMappingBean migrateTableMappingBean = tablefiledMappingMap.get(oldTableNameLower);
            List<MigrateTableMappingBeanDetail> migrateTableMappingBeanDetails = migrateTableMappingBean.getMigrateTableMappingBeanDetails().stream().filter(t -> (t.getJoinType() != null && "1".equalsIgnoreCase(t.getJoinType()))).collect(Collectors.toList());
            if (migrateTableMappingBeanDetails.size() > 0) {
                String selectSql = "";
                String fromSql = "";





                for (MigrateTableMappingBeanDetail migrateTableMappingBeanDetail : migrateTableMappingBeanDetails) {
                    //获取是替换后的值（判断是否是替换字段）
                    if (list_tmp.contains(migrateTableMappingBeanDetail.getE9TableField())) {
                        list_tmp.remove(migrateTableMappingBeanDetail.getE9TableField());

                        String e10Tablename = migrateTableMappingBeanDetail.getE10TableName();


                        String e10_tablefield = migrateTableMappingBeanDetail.getE10TableField();
                        String e9_tablefield = migrateTableMappingBeanDetail.getE9TableField();

                        if (TransUtil.isCusSQL(e10_tablefield)) {
                            e10_tablefield = TransUtil.getCusSQLFieldName(sqlExprTableSource, e9_tablefield, e10_tablefield);
                        }

                        if (TransUtil.isCusSQL(e10Tablename)) {
                            e10Tablename = TransUtil.getCusSQLTableName(sqlExprTableSource, e10Tablename);
                        }
                        String join_condition = migrateTableMappingBeanDetail.getJoinCondition();
                        //自定义FromSQL
                        if (TransUtil.isCusSQL(join_condition)) {
                            join_condition = TransUtil.getCusSQLTableName(sqlExprTableSource, join_condition);
                        }

                        //selectSql += e10Tablename + "." + e10_tablefield + " as " + e9_tablefield+",";
                        selectSql += e10Tablename + "." + e10_tablefield + ",";
                        if (!fromSql.contains(join_condition)) {
                            fromSql += join_condition + " ";
                        }
                    }
                }


                //处理 {$other$}
                Optional<MigrateTableMappingBeanDetail> first = tablefiledMappingMap.get(oldTableNameLower).getMigrateTableMappingBeanDetails().stream().filter(x -> "{$other$}".equalsIgnoreCase(x.getE9TableField())).findFirst();
                if (first.isPresent()) {
                    MigrateTableMappingBeanDetail migrateTableMappingBeanDetail = first.get();

                    List<MigrateTableMappingBeanDetail> migrateTableMappingBeanDetails_tmp = migrateTableMappingBean.getMigrateTableMappingBeanDetails();


                    //移除能找到字段映射关系的
                    List<String> collect = list_tmp.stream().filter(fieldName -> {
                        for (MigrateTableMappingBeanDetail tableMappingBeanDetail : migrateTableMappingBeanDetails_tmp) {
                            if (tableMappingBeanDetail.getE9TableField().equalsIgnoreCase(fieldName)) {
                                return false;
                            }
                        }
                        return true;
                    }).collect(Collectors.toList());



                    for (String fieldName : collect) {
                        String join_condition = migrateTableMappingBeanDetail.getJoinCondition();
                        String e10_tablefield = migrateTableMappingBeanDetail.getE10TableField();
                        String e10Tablename = migrateTableMappingBeanDetail.getE10TableName();
                        if (TransUtil.isCusSQL(e10_tablefield)) {
                            e10_tablefield = TransUtil.getCusSQLFieldName(sqlExprTableSource, fieldName, e10_tablefield);
                        }
                        if (TransUtil.isCusSQL(e10Tablename)) {
                            e10Tablename = TransUtil.getCusSQLTableName(sqlExprTableSource, e10Tablename);
                        }
                        //自定义FromSQL
                        if (TransUtil.isCusSQL(join_condition)) {
                            join_condition = TransUtil.getCusSQLTableName(sqlExprTableSource, join_condition);
                        }
                        selectSql += e10Tablename + "." + e10_tablefield + ",";
                        if (!fromSql.contains(join_condition)) {
                            fromSql += join_condition + " ";
                        }
                    }
                }


                if (!selectSql.equals("") && !fromSql.equals("")) {
                    String newTable = migrateTableMappingBean.getE10Table();
                    if (TransUtil.isCusSQL(newTable)) {
                        newTable = TransUtil.getCusSQLTableName(sqlExprTableSource,  newTable);
                    }


                    //移除最后一个逗号
                    selectSql = selectSql.substring(0, selectSql.length() - 1);
                    selectSql = "select " + newTable + ".*," + selectSql + " from ";
                    fromSql = newTable + " " + fromSql;
                    String resultSql = "(" + selectSql + fromSql + ")";
                    //resultSql = resultSql.replace("{tenant_key}", "'" + TransUtil.getTenantKey() + "'");
                    return resultSql;
                }


            }

        }

        return null;

    }


    /**
     * 构建替换sql
     *
     * @return
     */
    public static String buildMatrixE10sql(List<String> list, String tableName) {


        if (list.size() == 0) {
            return tableName;
        }
        // 去重
        list = list.stream().map(s -> s.contains(".") ? s.substring(s.indexOf(".") + 1) : s).distinct().collect(Collectors.toList());

/*        # 分部矩阵或者部门矩阵
        SELECT matrix_data_id,
        GROUP_CONCAT(CASE WHEN matrix_value_config_id = 130524440000000100 THEN relate_id END) AS 'test002',
        GROUP_CONCAT(CASE WHEN matrix_value_config_id = 130524440000000116 THEN relate_id  END) AS '补充0817003',
        ROUP_CONCAT(CASE WHEN matrix_value_config_id = 130524440000000128 THEN relate_id  END) AS 'tm004'
        from eteams.hrm_matrix_value_data
        where matrix_data_id = 130001980000000018
        group by matrix_data_id*/
        //系统矩阵  只有一个条件字段


        try {


            String targetDbType = (String) sqlResultInfo.get("targetDbType");


            //            map.put(matrixid+"_"+fieldname,e9fieldid);

            String sql = "";
            if (tableName.contains(tableName.split("_")[1].toLowerCase() + "$system")) {
                sql += "(select matrix_data_id as  id\n";
                for (String field_name : list) {

                    String fieldID_tmp = String.valueOf(matrixFieldIdMapping.get(TransUtil.getTenantKey() + tableName.split("_")[1] + "_" + field_name));
                    String[] fieldArray = fieldID_tmp.split("_");

                    String sql_tmp = "";
                    //后续修改
                    String e10fielid = TransUtil.getNewTableID("formfield_upgrade", "id", "hrm_formfield", fieldArray[0]);
                    if (targetDbType.equalsIgnoreCase("mysql")) {
                        sql_tmp += ",GROUP_CONCAT(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END) AS " + field_name + "\n";
                    } else if (targetDbType.equalsIgnoreCase("oracle")) {
                        sql_tmp += ",LISTAGG(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END,',') WITHIN GROUP (ORDER BY relate_id) AS  " + field_name + "\n";
                    } else if (targetDbType.equalsIgnoreCase("postgresql")) {
                        sql_tmp += ",STRING_AGG(CAST(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END as VARCHAR),',') AS " + field_name + "\n";
                    } else if (targetDbType.equalsIgnoreCase("sqlserver")) {
                        sql_tmp += ",STRING_AGG(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END,',') AS " + field_name + "\n";
                    }
                    sql += sql_tmp;
                }
                sql += "from eteams.hrm_matrix_value_data\n";
                sql += "where delete_type=0 and  tenant_key={tenant_key} and  matrix_data_id=?\n";
                sql += "group by matrix_data_id)";
                sql = sql.replace("{tenant_key}", "'" + TransUtil.getTenantKey() + "'");

                TransUtil.addTransMsg("请设置 部门或分部id matrix_data_id=?");
                return sql;
            } else {

/*

            SELECT *
                    FROM (
                            (SELECT matrix_data_id,
                            GROUP_CONCAT(CASE
                                    WHEN matrix_condition_config_id = 130002430000000015
                                    THEN relate_id END) AS jzbm,
                            GROUP_CONCAT(CASE
                                    WHEN matrix_condition_config_id = 130002430000000017
                                    THEN relate_id END) AS rlzy,
                            GROUP_CONCAT(CASE
                                    WHEN matrix_condition_config_id = 130002430000000019
                                    THEN relate_id END) AS kh
                            FROM eteams.hrm_matrix_condition_data
                            WHERE matrix_id = 130524200000000004
                            GROUP BY matrix_data_id) condtion_data
            LEFT JOIN
            (SELECT matrix_data_id,
                    GROUP_CONCAT(CASE WHEN matrix_value_config_id = 130002440000000016 THEN relate_id END) AS dry,
            GROUP_CONCAT(CASE WHEN matrix_value_config_id = 130002440000000018 THEN relate_id END) AS dry2
            FROM eteams.hrm_matrix_value_data
            WHERE matrix_id = 130524200000000004
            GROUP BY matrix_data_id) value_data
                    ON
            value_data.matrix_data_id = condtion_data.matrix_data_id);
*/


                String matrix_id = TransUtil.getNewTableID("hrm_matrix", "id", "matrixinfo", tableName.split("_")[1]);


                String condtion_data = "";

                condtion_data += "(select matrix_data_id \n";

                String value_data = "";
                value_data += "(select matrix_data_id \n";

                for (String field_name : list) {

                    //肯定能拿到，如果拿不到说明没有正确读取库。
                    String fieldID_tmp = String.valueOf(matrixFieldIdMapping.get(TransUtil.getTenantKey() + tableName.split("_")[1] + "_" + field_name));
                    String[] fieldArray = fieldID_tmp.split("_");
                    String e10fielid = "";
                    if (fieldArray[1].equals("1")) {//取值
                        e10fielid = TransUtil.getNewTableID("hrm_matrix_value_config", "id", "matrixfieldinfo", fieldArray[0]);
                    } else {
                        e10fielid = TransUtil.getNewTableID("hrm_matrix_condition_config", "id", "matrixfieldinfo", fieldArray[0]);
                    }

                    if (fieldArray[1].equals("1")) {
                        if (targetDbType.equalsIgnoreCase("mysql")) {
                            value_data += ",GROUP_CONCAT(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END) AS " + field_name + "\n";
                        } else if (targetDbType.equalsIgnoreCase("oracle")) {
                            value_data += ",LISTAGG(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END,',') WITHIN GROUP (ORDER BY relate_id) AS " + field_name + "\n";
                        } else if (targetDbType.equalsIgnoreCase("postgresql")) {
                            value_data += ",STRING_AGG(CAST(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END as VARCHAR),',') AS " + field_name + "\n";
                        } else if (targetDbType.equalsIgnoreCase("sqlserver")) {
                            value_data += ",STRING_AGG(CASE WHEN matrix_value_config_id = " + e10fielid + " THEN relate_id END,',') AS " + field_name + "\n";
                        }
                    } else {
                        if (targetDbType.equalsIgnoreCase("mysql")) {
                            condtion_data += ",GROUP_CONCAT(CASE WHEN matrix_condition_config_id = " + e10fielid + " THEN relate_id END) AS " + field_name + "\n";
                        } else if (targetDbType.equalsIgnoreCase("oracle")) {
                            condtion_data += ",LISTAGG(CASE WHEN matrix_condition_config_id = " + e10fielid + " THEN relate_id END,',') WITHIN GROUP (ORDER BY relate_id) AS " + field_name + "\n";
                        } else if (targetDbType.equalsIgnoreCase("postgresql")) {
                            condtion_data += ",STRING_AGG(CAST(CASE WHEN matrix_condition_config_id = " + e10fielid + " THEN relate_id END as VARCHAR),',') AS " + field_name + "\n";
                        } else if (targetDbType.equalsIgnoreCase("sqlserver")) {
                            condtion_data += ",STRING_AGG(CASE WHEN matrix_condition_config_id = " + e10fielid + " THEN relate_id END,',') AS " + field_name + "\n";
                        }
                    }

                }
                condtion_data += "          FROM eteams.hrm_matrix_condition_data\n" + "                            WHERE matrix_id = " + matrix_id + "\n" + "                            and tenant_key = {tenant_key}\n" +

                        "                            GROUP BY matrix_data_id)";
                value_data += "   FROM eteams.hrm_matrix_value_data\n" + "            WHERE matrix_id = " + matrix_id + "\n" + "            and tenant_key = {tenant_key}\n" + "            GROUP BY matrix_data_id)\n";
                //拼接最终sql (mysql 不支持全连接，拼接出来效果复杂一点)
                String resultSql = "(select " + StringUtils.join(list, ",") + " from  " + condtion_data + "    condtion_data   left join  " + value_data + "   value_data  " + " on  value_data.matrix_data_id = condtion_data.matrix_data_id  )";
                resultSql = resultSql.replace("{tenant_key}", "'" + TransUtil.getTenantKey() + "'");

                return resultSql;

            }
        } catch (Exception e) {

            logger.error("sql", e);
            TransUtil.addTransErrorMsg("矩阵替换过程中报错，请手动替换!!!");
            return null;
        }

    }


    public static Map<String, Integer> tableCount = new ConcurrentHashMap<String, Integer>();
    public static Map<String, Integer> FieldCount = new ConcurrentHashMap<>();
    public static List<String> errorList = new CopyOnWriteArrayList<>();

}
