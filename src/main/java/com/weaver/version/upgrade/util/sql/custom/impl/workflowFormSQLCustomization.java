package com.weaver.version.upgrade.util.sql.custom.impl;

import com.weaver.version.upgrade.util.sql.custom.SQLCustomization;
import com.weaver.version.upgrade.util.sql.util.TransUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;

/**
 * @author wujiahao
 * @date 2024/12/12 下午4:36
 */
public class workflowFormSQLCustomization implements SQLCustomization {
    @Override
    public String transformTableName(String tableName, HashMap<String, String> conditions, List<String> usedColumns) {
        String newTableName="";
        Map<String,Object> objectMap = (Map<String, Object>) sqlResultInfo.get("otherMap");
        if (objectMap != null) {
            if ("workflow_form".equalsIgnoreCase(tableName)) {
                newTableName = "(select b.*,a.requestid\n" +
                        "      from wfc_form_data a,\n" +
                        "           " + objectMap.get("main_tablename") +
                        "  b    where a.dataid = b.form_data_id and a.delete_type=0 and a.tenant_key='" + TransUtil.getTenantKey() + "')";
            } else {
                newTableName= String.valueOf(objectMap.get("detail_tablename"));
            }
        }
        //只处理人力资源浏览按钮
        return newTableName;
    }

    @Override
    public String transformJoinTable(String e9tableName, HashMap<String, String> conditionMap, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformJoinStatement(String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformColumnName(String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }

    @Override
    public String transformConditionValue(String sourceValue, String sourceColumnName, String sourceTableName, HashMap<String, String> conditions, List<String> usedColumns) {
        return "";
    }
}
