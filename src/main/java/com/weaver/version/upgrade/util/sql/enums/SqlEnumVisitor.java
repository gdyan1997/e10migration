package com.weaver.version.upgrade.util.sql.enums;

import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLSelectItem;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitorAdapter;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGASTVisitorAdapter;
import com.alibaba.druid.sql.dialect.sqlserver.visitor.SQLServerASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.visitor.mysql.ConditionValueMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.mysql.DatabaseFieldMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.mysql.FieldMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.mysql.TableMysqlVisitor;
import com.weaver.version.upgrade.util.sql.visitor.oracle.ConditionValueOracleVisitor;
import com.weaver.version.upgrade.util.sql.visitor.oracle.DatabaseFieldOracleVisitor;
import com.weaver.version.upgrade.util.sql.visitor.oracle.FieldOracleVisitor;
import com.weaver.version.upgrade.util.sql.visitor.oracle.TableOracleVisitor;
import com.weaver.version.upgrade.util.sql.visitor.pgServer.ConditionValuePgVisitor;
import com.weaver.version.upgrade.util.sql.visitor.pgServer.DatabaseFieldPgVisitor;
import com.weaver.version.upgrade.util.sql.visitor.pgServer.FieldPgServerVisitor;
import com.weaver.version.upgrade.util.sql.visitor.pgServer.TablePGVisitor;
import com.weaver.version.upgrade.util.sql.visitor.sqlServer.ConditionValueSqlServerVisitor;
import com.weaver.version.upgrade.util.sql.visitor.sqlServer.DatabaseFieldSqlServerVisitor;
import com.weaver.version.upgrade.util.sql.visitor.sqlServer.FieldSqlServerVisitor;
import com.weaver.version.upgrade.util.sql.visitor.sqlServer.TableSqlServerVisitor;
import com.weaver.version.upgrade.util.sql.util.TransUtil;

/**
 * @author wujiahao
 * @date 2024/7/26 上午11:25
 */
public enum SqlEnumVisitor {


    MYSQL("mysql") {
        @Override
        public void process(SQLStatement sqlStatement) {
            sqlStatement.accept(new ConditionValueMysqlVisitor());
            sqlStatement.accept(new FieldMysqlVisitor());
            sqlStatement.accept(new TableMysqlVisitor());
        }
        @Override
        public void processKeywords(SQLStatement sqlStatement) {
            sqlStatement.accept(new DatabaseFieldMysqlVisitor());
        }

        @Override
        public void addFirstQueryField(SQLStatement sqlStatement) {
            sqlStatement.accept(new MySqlASTVisitorAdapter() {
                @Override
                public boolean visit(SQLSelectItem x) {
                    TransUtil.addFirstQueryField(x);
                    return true;
                }
            });
        }

        @Override
        public void getSelectList(SQLStatement sqlStatement) {
            TransUtil.getSelectItemJr(sqlStatement);
        }
    },
    ORACLE("oracle") {
        @Override
        public void process(SQLStatement sqlStatement) {
            sqlStatement.accept(new ConditionValueOracleVisitor());
            sqlStatement.accept(new FieldOracleVisitor());
            sqlStatement.accept(new TableOracleVisitor());
        }

        @Override
        public void processKeywords(SQLStatement sqlStatement) {
            sqlStatement.accept(new DatabaseFieldOracleVisitor());
        }
        @Override
        public void addFirstQueryField(SQLStatement sqlStatement) {
            sqlStatement.accept(new OracleASTVisitorAdapter() {
                @Override
                public boolean visit(SQLSelectItem x) {
                    TransUtil.addFirstQueryField(x);
                    return true;
                }
            });
        }

        @Override
        public void getSelectList(SQLStatement sqlStatement) {
            TransUtil.getSelectItemJr(sqlStatement);
        }
    },
    POSTGRESQL("postgresql") {
        @Override
        public void process(SQLStatement sqlStatement) {
            sqlStatement.accept(new ConditionValuePgVisitor());
            sqlStatement.accept(new FieldPgServerVisitor());
            sqlStatement.accept(new TablePGVisitor());
        }

        @Override
        public void processKeywords(SQLStatement sqlStatement) {
            sqlStatement.accept(new DatabaseFieldPgVisitor());
        }
        @Override
        public void addFirstQueryField(SQLStatement sqlStatement) {
            sqlStatement.accept(new PGASTVisitorAdapter() {
                @Override
                public boolean visit(SQLSelectItem x) {
                    TransUtil.addFirstQueryField(x);
                    return true;
                }
            });
        }

        @Override
        public void getSelectList(SQLStatement sqlStatement) {
            TransUtil.getSelectItemJr(sqlStatement);

        }

    },
    SQLSERVER("sqlServer") {
        @Override
        public void process(SQLStatement sqlStatement) {
            sqlStatement.accept(new ConditionValueSqlServerVisitor());
            sqlStatement.accept(new FieldSqlServerVisitor());
            sqlStatement.accept(new TableSqlServerVisitor());
        }
        @Override
        public void processKeywords(SQLStatement sqlStatement) {
            sqlStatement.accept(new DatabaseFieldSqlServerVisitor());
        }
        @Override
        public void addFirstQueryField(SQLStatement sqlStatement) {
            sqlStatement.accept(new SQLServerASTVisitorAdapter() {
                @Override
                public boolean visit(SQLSelectItem x) {
                    TransUtil.addFirstQueryField(x);
                    return true;
                }
            });
        }

        @Override
        public void getSelectList(SQLStatement sqlStatement) {

            TransUtil.getSelectItemJr(sqlStatement);

        }
    };

    private final String jdbcConstant;

    SqlEnumVisitor(String jdbcConstant) {
        this.jdbcConstant = jdbcConstant;
    }



    public abstract void process(SQLStatement sqlStatement);

    public abstract void processKeywords(SQLStatement sqlStatement);


    public abstract void addFirstQueryField(SQLStatement sqlStatement);


    public abstract void getSelectList(SQLStatement sqlStatement);

    public static SqlEnumVisitor getVisitors(String dbType) {
        switch (dbType.toLowerCase()) {
            case "oracle":
                return ORACLE;
            case "postgresql":
                return POSTGRESQL;
            case "sqlserver":
                return SQLSERVER;
            default:
                return MYSQL;
        }
    }


}
