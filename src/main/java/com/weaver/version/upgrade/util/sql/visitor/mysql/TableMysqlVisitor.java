package com.weaver.version.upgrade.util.sql.visitor.mysql;

import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;
import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.*;


/**
 * @author wujiahao
 * @date 2023/12/14 15:50
 */
public class TableMysqlVisitor extends MySqlASTVisitorAdapter {





    /**
     * 重写表名字的拦截器
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLExprTableSource x) {

        List<String> tableUseField = new ArrayList<>();



        Map<String, Object> attributes = x.getAttributes();
        if (attributes.containsKey("tableUseField")){
            tableUseField= (List<String>) attributes.get("tableUseField");
        }
        if (!attributes.containsKey("isE9table")){
            return true;
        }
        //视图名称、不替换
        //表明是数字 特殊写法不解析
        if (x.getParent() instanceof SQLCreateViewStatement||
                x.getExpr()instanceof SQLIntegerExpr){
            return true;
        }

        final  String  oldTable = Optional.ofNullable(x)
                .map(y -> y.getName())
                .map(z -> z.getSimpleName())
                .orElse("");

        final  String  oldTableName = Optional.ofNullable(x)
                .map(y -> y.getName())
                .map(z -> z.getSimpleName())
                .map(String::toLowerCase)
                .orElse("");

        String tableName = oldTableName;


        //添加表明
        HashSet tableSet = (HashSet) sqlResultInfo.getOrDefault("tableSet", new HashSet<>());
        tableSet.add(oldTable.toLowerCase());
        sqlResultInfo.put("tableSet", tableSet);





        //视图起的别名，不是真是表,不需要替换
        String sqlType = (String) Optional.ofNullable(sqlResultInfo.get("sqlType")).orElse("");
        if ("view".equalsIgnoreCase(sqlType)) {
            List viewSQLWithAlse = (List) Optional.ofNullable(sqlResultInfo.get("viewSQLWithAlse")).orElse(new ArrayList<String>());
            if (viewSQLWithAlse.contains(oldTableName.toLowerCase())) {
                return true;
            }
        }
        //特殊写法不解析
        if (x.getExpr() instanceof SQLMethodInvokeExpr) {
            SQLMethodInvokeExpr sqlMethodInvokeExpr = (SQLMethodInvokeExpr) x.getExpr();
            //FROM OPENQUERY(SAP,
            //               'select a.MEINS from "SAPABAP1"."MARA" as a left join "SAPABAP1"."MAKT
            String methodName = sqlMethodInvokeExpr.getMethodName();
            TransUtil.addTransErrorMsg("sql中 from存在特殊写法，方法名称为" + methodName + "请手动替换");
            return true;
        }


        String newTableName = tableName;
        //查询sql替换方式
        if (x.getParent() instanceof SQLSelectQueryBlock || x.getParent() instanceof SQLJoinTableSource) {
            //是否改写fromSQL
            if (TransUtil.isUpdateFormSQL(tableName)) {
                //该表使用的所有列（目前只处理查询情况）
                String newSql = FieldMappingCache.buildFromSql(tableUseField, x);
                //替换成功
                if (newSql != null) {
                    TransUtil.addTransMsg("将E9表" + tableName + "替换为子查询=>" + newSql);
                    newTableName = newSql;
                }

                //流程老表单特殊处理
            }
        }

        if (newTableName.equalsIgnoreCase(tableName)){
            newTableName = FieldMappingCache.replaceTable(x);

        }

        //表明进行了转换
        if (!newTableName.equalsIgnoreCase(tableName)) {
            //记录一下旧表和新表的关系
            if (!newTableName.equalsIgnoreCase(oldTableName)) {
                sqlResultInfo.put("table-" + newTableName, oldTableName);
            }
            //别名
            String aliasName = x.getAlias();
            //插入语句不替换别名
            if (!(x.getParent() instanceof SQLInsertStatement)) {
                //如何不存在别名，并且表明被替换了。
                if (aliasName == null && !tableName.equalsIgnoreCase(newTableName)) {
                    //老表明设置为别名
                    x.setAlias(oldTable);
                }
            }
            //重新设置表明
            x.setExpr(newTableName);
        }


        //替换完成后增加默认条件
        //{$$}.TASK_TYPE=(select id FROM project.task_form_link where NAME='项目任务')
        Optional.ofNullable(tablefiledMappingMap.get(oldTable.toLowerCase())).ifPresent(migrateTableMappingBean -> {
            String defaultCondition = migrateTableMappingBean.getDefaultCondition();
            if (StringUtils.isNotBlank(defaultCondition)) {
                String alias = x.getAlias();
                String sql = "select * from  tt_rr_nn   ";
                if (alias == null || alias.equals("")) {
                    sql += oldTable;
                    alias = oldTable;
                } else {
                    sql += alias;
                }
                sql += " where ";
                if (StringUtils.isNotBlank(defaultCondition)) {
                    defaultCondition = defaultCondition.replaceAll("\\{\\$(.*?)\\$\\}", alias);
                    sql += defaultCondition;
                    try {
                        SQLQueryExpr sqlQueryExpr = TransUtil.buildSQLSelect(sql);
                        SQLSelect subQuery = sqlQueryExpr.getSubQuery();
                        SQLSelectQuery query = subQuery.getQuery();
                        if (query instanceof SQLSelectQueryBlock) {
                            SQLExpr where = ((SQLSelectQueryBlock) query).getWhere();
                            if (where instanceof SQLBinaryOpExpr) {
                                TransUtil.setCondition(x.getParent(), (SQLBinaryOpExpr) where);
                            }
                        }
                    } catch (Exception e) {

                    }
                }
            }
        });


        //删除或者更新语句默认增加1=0的条件。
         if (x.getParent() instanceof SQLUpdateStatement||x.getParent() instanceof SQLDeleteStatement){
             if (isOaTable(tableName)) {
                 SQLBinaryOpExpr sqlBinaryOpExpr = new SQLBinaryOpExpr(
                         new SQLIntegerExpr(1),
                         new SQLIntegerExpr(0),
                         SQLBinaryOperator.Equality);
                 TransUtil.setCondition(x.getParent(), sqlBinaryOpExpr);
             }
        }


        Map<String, Object> objectMap = (Map<String, Object>) sqlResultInfo.getOrDefault("otherMap", new HashMap<>());
        if (objectMap == null) {
            objectMap=new HashMap<>();
        }
        //ygd那边要求如果穿了此参数不需要加 delete_type 和tenant_key
        if (!objectMap.containsKey("removeTenantKey")) {
            //是oa中的表才添加 delete_type 及tenant_key
            if (isOaTable(tableName)) {
                SQLBinaryOpExpr sqlBinaryOpExpr = new SQLBinaryOpExpr(new SQLIdentifierExpr(TransUtil.getAliasName(x, tableName) + "delete_type"), new SQLIntegerExpr(0), SQLBinaryOperator.Equality);
                SQLBinaryOpExpr sqlBinaryOpExpr1 = new SQLBinaryOpExpr(new SQLIdentifierExpr(TransUtil.getAliasName(x, tableName) + "tenant_key"), new SQLCharExpr(TransUtil.getTenantKey()), SQLBinaryOperator.Equality);
                SQLBinaryOpExpr condition = new SQLBinaryOpExpr(sqlBinaryOpExpr, SQLBinaryOperator.BooleanAnd, sqlBinaryOpExpr1);
                //设置delete_type 和 tenant_key
                TransUtil.setCondition(x.getParent(), condition);
            }
        }


        TransUtil.removeCondition(x.getParent());
        return true;
    }


    private static boolean isOaTable(String tableName) {
        return  workflowTableMap.get(TransUtil.getTenantKey()).contains(tableName + "|sys|") ||
                tablefiledMappingMap.containsKey(tableName) ||
                "wfc_form_data".equalsIgnoreCase(tableName);
    }
    ;


}
