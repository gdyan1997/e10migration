package com.weaver.version.upgrade.util.sql.visitor.pgServer;

import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.visitor.mysql.TableMysqlVisitor;

/**
 * @author wujiahao
 * @date 2023/12/14 15:50
 */
public class TablePGVisitor extends PGASTVisitorAdapter {


    /**
     * 重写表名字的拦截器
     *
     * @param x
     * @return
     */
    @Override
    public boolean visit(SQLExprTableSource x) {
        return new TableMysqlVisitor().visit(x);
    }


}
