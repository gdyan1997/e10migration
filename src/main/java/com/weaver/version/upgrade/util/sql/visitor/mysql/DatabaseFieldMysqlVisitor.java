package com.weaver.version.upgrade.util.sql.visitor.mysql;

import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.expr.*;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitorAdapter;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.version.upgrade.util.sql.cache.SqlCache;
import com.weaver.version.upgrade.util.sql.util.TransUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

import static com.weaver.version.upgrade.util.sql.SQLUtil.sqlResultInfo;
import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.tablefiledMappingMap;
import static com.weaver.version.upgrade.util.sql.cache.FieldMappingCache.workflowTableMap;

/**
 * @author wujiahao
 * @date 2024/6/3 下午5:41
 * sql保留字段处理
 */
public class DatabaseFieldMysqlVisitor extends MySqlASTVisitorAdapter {


    private static List<String> eteamsTables = Arrays.asList(StringUtils.split("safety_account_autolock,safety_login_setting,safety_auxiliary_login,hrm_privacy_setting,hrm_privacy_range_details,hrm_company_virtual,safety_password_policy,hrm_privacy_personal_option,hrm_browser_field,hrm_offspace,configuration,hrm_config_set,ipconfig_list,network_segment_manage,network_segment_reference,ipconfig_permission,compvirtual_ph_cpvt_share,hrm_code_rule,hrm_code_rule_detail,hrm_code_rule_reserved,department,depart_link,hrm_jobtype_set,position_link,hrm_jobset,position,grade,hrm_jobcall,administrative_area,i18n_measure_setting,hrm_code_rule_record,employee,avatar,hrm_extra_organization,emp_dep_link,emp_posn_link,dimission_log,user_secrecy_status,user_secrecy_info,hrm_card_setting,emp_link,emp_attr_link,employee_virtual,personal_setting,personal_token_bind,employee_manager,hrm_matrix,hrm_matrix_condition_config,hrm_matrix_value_config,hrm_matrix_mtr,hrm_matrix_mtr_user,channel,channel_user_link,channel_ph_channel_share,hrm_empchange,hrm_empchange_user,hrm_empchange_userdetail,hrm_empchange_notice,hrm_field,employee_external,employee_external_link,hrm_contact_sort,emp_memory,hrm_matrix_mtr_condition,hrm_user_field,hrm_matrix_data,log,online_conn_log,browser_scope_hrmdetail,hrm_sa_condition,hrm_logictable_config,hrm_field_group_detial,hrm_field_option,hrm_employee_custom_data,login_type_target", ","));

    //处理sql关键字
    @Override
    public boolean visit(SQLPropertyExpr expr) {
        expr.setName(SqlCache.getTargetDBFieldName(expr.getName()));

        expr.setOwner(SqlCache.getTargetDBFieldName(expr.getOwnerName()));


        return true;
    }

    //处理sql关键字
    @Override
    public boolean visit(SQLIdentifierExpr expr) {
        expr.setName(SqlCache.getTargetDBFieldName(expr.getName()));
        return true;
    }

    //处理sql关键字
    public boolean visit(SQLExprTableSource x) {

        String oldTableName = Optional.ofNullable(x)
                .map(y -> y.getName())
                .map(z -> z.getSimpleName())
                .map(String::toLowerCase)
                .orElse("");


        //处理漏网之鱼
        String finalOldTableName = oldTableName;
        Optional.ofNullable(TransUtil.getTableUseField(x)).ifPresent(useStrings -> {
            if (!useStrings.contains("tenant_key")) {
                if (StringUtils.isNotBlank(finalOldTableName)) {
                    String tableName = Optional.ofNullable(x)
                            .map(y -> y.getName())
                            .map(z -> z.getSimpleName())
                            .orElse("");



//                    long count = tablefiledMappingMap.values()
//                            .stream()
//                            .flatMap(List::stream).filter(tablefiledMappingMaps -> finalOldTableName.equalsIgnoreCase(tablefiledMappingMaps.getE10TableName())).count();

                    long count = tablefiledMappingMap.values()
                            .stream().
                            filter(xt1 -> xt1.getMigrateTableMappingBeanDetails().stream().filter(detail -> finalOldTableName.equalsIgnoreCase(detail.getE10TableName())).count() > 0)
                            .count();

                    if (count == 0) {
                        if (FieldMappingCache.matrixFieldIdMapping.containsKey(finalOldTableName)) {
                            count=1;
                        }
                        if (eteamsTables.contains(finalOldTableName.toLowerCase())){
                            count=1;
                        }
                    }

                    Map<String, Object> objectMap = (Map<String, Object>) sqlResultInfo.getOrDefault("otherMap", new HashMap<>());
                    if (objectMap == null) {
                        objectMap=new HashMap<>();
                    }
                    //ygd那边要求如果穿了此参数不需要加 delete_type 和tenant_key
                    if (objectMap.containsKey("removeTenantKey")) {
                        count=0;
                    }


                    if (count > 0&&!tableName.equalsIgnoreCase("user_info_login_param")) {

                        String tenantKey = TransUtil.getTenantKey();

                        if ("user_info_password".equalsIgnoreCase(finalOldTableName)) {
                            tenantKey="all_teams";
                        }



                        SQLBinaryOpExpr sqlBinaryOpExpr = new SQLBinaryOpExpr(
                                new SQLIdentifierExpr(TransUtil.getAliasName(x,tableName) + "delete_type"),
                                new SQLIntegerExpr(0),
                                SQLBinaryOperator.Equality);
                        SQLBinaryOpExpr sqlBinaryOpExpr1 = new SQLBinaryOpExpr(
                                new SQLIdentifierExpr(TransUtil.getAliasName(x,tableName) + "tenant_key"),
                                new SQLCharExpr(tenantKey),
                                SQLBinaryOperator.Equality);
                        SQLBinaryOpExpr condition = new SQLBinaryOpExpr(sqlBinaryOpExpr, SQLBinaryOperator.BooleanAnd, sqlBinaryOpExpr1);
                        //设置delete_type 和 tenant_key
                        TransUtil.setCondition(x.getParent(), condition);
                    }
                }
            }
        });
        SQLExpr expr = x.getExpr();
        //添加teams.
        if (expr instanceof SQLPropertyExpr) {
            SQLPropertyExpr expr_temp = (SQLPropertyExpr) expr;
            String name = expr_temp.getName();
            String ownernName = expr_temp.getOwnernName();
            if (ownernName.startsWith("eteams")) {

            } else if (eteamsTables.contains(name)) {
                expr_temp.setOwner("eteams");
            } else {
                x.setExpr(new SQLIdentifierExpr(name));
            }
        } else if (expr instanceof SQLIdentifierExpr) {
            SQLIdentifierExpr expr_temp = (SQLIdentifierExpr) expr;
            String name = expr_temp.getName();
            if (eteamsTables.contains(name)) {
                x.setExpr(new SQLPropertyExpr("eteams", name));
            }
        }








        String e9tableName="whhhh";
        if (sqlResultInfo.containsKey("e9tableName="+oldTableName)){
            e9tableName= String.valueOf(sqlResultInfo.get("e9tableName="+oldTableName));
        }


        //如果是建模，edc,ebc
        if (workflowTableMap.get(TransUtil.getTenantKey()).contains(e9tableName + "|cube_det|") ||
                workflowTableMap.get(TransUtil.getTenantKey()).contains(e9tableName + "|cube|")
                ||e9tableName.startsWith("uf_")
                ||(oldTableName.indexOf("_eb_")>0&&(oldTableName.startsWith("ftab_")||oldTableName.startsWith("billmxtb_")))
        ) {
            TransUtil.addDataConn("weaver-ebuilder-form-service");
            getBillDBName(x, expr,  "weaver-ebuilder-form-service");
        } else if (workflowTableMap.get(TransUtil.getTenantKey()).contains(e9tableName + "|wf_det|")
                || workflowTableMap.get(TransUtil.getTenantKey()).contains(e9tableName + "|wf|")
                ||  e9tableName.startsWith("formtable_main_")
                ||oldTableName.startsWith("ftab_")||oldTableName.startsWith("billmxtb_")
        ) {
            TransUtil.addDataConn("weaver-workflow-core-service");

            getBillDBName(x, expr,  "weaver-workflow-core-service");
        } else if (workflowTableMap.get(TransUtil.getTenantKey()).contains(e9tableName + "|edc_det|")
                || workflowTableMap.get(TransUtil.getTenantKey()).contains(e9tableName + "|edc|")
                || oldTableName.startsWith("edc_uf_table")
        ) {
            TransUtil.addDataConn("weaver-formreport-service");
            getBillDBName(x, expr, "weaver-formreport-service");
        } else {

            if (FieldMappingCache.matrixFieldIdMapping.containsKey(oldTableName)) {
                TransUtil.addDataConn(oldTableName);
                String dataBase = String.valueOf(FieldMappingCache.matrixFieldIdMapping.get(oldTableName));
                if (StringUtils.isBlank(dataBase)) {
                    dataBase = "ecology";
                }
                if (expr instanceof SQLPropertyExpr) {
                    SQLPropertyExpr expr_temp = (SQLPropertyExpr) expr;
                    String name = expr_temp.getName();
                    if (!eteamsTables.contains(name)
                            && !"ecology".equalsIgnoreCase(dataBase)
                            && !"ecology10".equalsIgnoreCase(dataBase)
                    ) {
                        expr_temp.setOwner(dataBase);
                    }
                } else if (expr instanceof SQLIdentifierExpr) {
                    SQLIdentifierExpr expr_temp = (SQLIdentifierExpr) expr;
                    String name = expr_temp.getName();
                    if (!eteamsTables.contains(name)
                            && !"ecology".equalsIgnoreCase(dataBase)
                            && !"ecology10".equalsIgnoreCase(dataBase)
                    )
                        x.setExpr(new SQLPropertyExpr(dataBase, name));
                }

            }
        }



        //sqlserver加上  dbo
        if (TransUtil.null2String(sqlResultInfo.get("targetDbType")).equalsIgnoreCase("sqlserver")) {
            SQLExpr expr_tmp = x.getExpr();
            if (expr_tmp instanceof SQLPropertyExpr) {
                SQLPropertyExpr expr_temp = (SQLPropertyExpr) expr_tmp;
                String name = expr_temp.getName();
                String ownernName = expr_temp.getOwnernName();
                if (StringUtils.isNotBlank(ownernName)) {
                    x.setExpr(new SQLPropertyExpr(ownernName+".dbo", name));
                }

            }
        }




        expr = x.getExpr();
        if (oldTableName.equalsIgnoreCase("hrmcuscardinfo")
                || oldTableName.equalsIgnoreCase("hrmemployeedefined")
        ) {
            if (FieldMappingCache.matrixFieldIdMapping.containsKey(TransUtil.getTenantKey() + "deployType")) {
                if("1".equals(String.valueOf(FieldMappingCache.matrixFieldIdMapping.get(TransUtil.getTenantKey() + "deployType")))){
                    if (expr instanceof SQLPropertyExpr) {
                        SQLPropertyExpr expr_temp = (SQLPropertyExpr) expr;
                        String name = expr_temp.getName();
                        expr_temp.setName(name + "_" + TransUtil.getTenantKeyNumber());
                    } else if (expr instanceof SQLIdentifierExpr) {
                        SQLIdentifierExpr expr_temp = (SQLIdentifierExpr) expr;
                        String name = expr_temp.getName();
                        expr_temp.setName(name + "_" + TransUtil.getTenantKeyNumber());
                    }
                }
            }
        }


        return true;
    }


    //处理sql关键字
    @Override
    public boolean visit(SQLCreateViewStatement expr) {
        Optional.ofNullable(expr)
                .map(SQLCreateViewStatement::getSubQuery)
                .map(SQLSelect::getQuery)
                .filter(query -> query instanceof SQLSelectQueryBlock)
                .map(query -> (SQLSelectQueryBlock) query)
                .map(SQLSelectQueryBlock::getSelectList)
                .ifPresent(sqlSelectItems -> {
                    sqlSelectItems.add(new SQLSelectItem(new SQLIdentifierExpr("0"), "delete_type"));
                    sqlSelectItems.add(new SQLSelectItem(new SQLCharExpr(TransUtil.null2String(TransUtil.getTenantKey())), "tenant_key"));
                });

        return true;
    }


    private void getBillDBName(SQLExprTableSource x, SQLExpr expr, String serverName) {

            String dataBase = String.valueOf(FieldMappingCache.matrixFieldIdMapping.get(serverName));
            if (StringUtils.isBlank(dataBase)||"null".equalsIgnoreCase(dataBase)) {
                dataBase = "ecology";
            }
            if (expr instanceof SQLIdentifierExpr) {
                SQLIdentifierExpr expr_temp = (SQLIdentifierExpr) expr;
                String name = expr_temp.getName();
                if (!"ecology".equalsIgnoreCase(dataBase)
                        && !"ecology10".equalsIgnoreCase(dataBase)
                ) {
                    x.setExpr(new SQLPropertyExpr(dataBase, name));
                }
            }


    }


}
