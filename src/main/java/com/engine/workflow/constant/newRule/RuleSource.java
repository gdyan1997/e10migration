package com.engine.workflow.constant.newRule;

import java.util.HashMap;
import java.util.Map;

/**
 * 规则来源
 */
public enum RuleSource {

    RULE_REPOSITORY(0, 531619), //风险规则库
    NODE_CHECK(1, 529961),      //节点校验
    AUTO_APPROVE(2, 529205);    //智能审批


    private static Map<Integer, RuleSource> ruleSourceMap = new HashMap<>();

    static {
        for (RuleSource ruleSource : RuleSource.values()) {
            ruleSourceMap.put(ruleSource.type, ruleSource);
        }
    }

    public static RuleSource getRuleSourceType(int type) {
        return ruleSourceMap.get(type);
    }

    /**
     * 值类型
     */
    protected int type;

    protected int label;

    RuleSource(int type, int label) {
        this.type = type;
        this.label = label;
    }

    public int getType() {
        return type;
    }

    public int getLabel() {
        return label;
    }
}
