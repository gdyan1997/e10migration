package com.engine.workflow.util;


import com.engine.workflow.entity.workflowOvertime.OverTimeSettingsEntity;
import weaver.conn.RecordSet;
import weaver.general.BaseBean;
import weaver.general.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WorkflowOvertimeSettingsUtil {


    public static OverTimeSettingsEntity getOverTimeSettingsEntity() {
        String sql = "select isOpen,scanRate,isSkipWorkingDay,changestatus from workflow_Settings where id = 1 ";
        OverTimeSettingsEntity entity = new OverTimeSettingsEntity();
        BaseBean baseBean = new BaseBean();
        RecordSet rs = new RecordSet();

        rs.executeQuery(sql);

        //超时设置表中有数据从数据库表读
        if (rs.next()) {
            entity.setId(1);
            entity.setIsOpen(Util.getIntValue(rs.getString("isOpen"), 0));
            entity.setScanRate(Util.getIntValue(rs.getString("scanRate"), 0));
            entity.setIsSkipWorkingDay(Util.getIntValue(rs.getString("isSkipWorkingDay"), 0));
            entity.setChangestatus(Util.getIntValue(rs.getString("changestatus"), 0));
        } else {
            //超时设置表中没数据从配置文件读并把数据保存到数据库表中
            String overtimeset = baseBean.getPropValue("weaver", "ecology.overtime");
            String changestatus = baseBean.getPropValue("weaver", "ecology.changestatus");
            if (!overtimeset.equals("")) {
                int overtimesets = Util.getIntValue(overtimeset, 1);
                entity.setScanRate(overtimesets);
            }
            //默认开启
            entity.setIsOpen(1);
            int isSkipWorkingDay = Util.getIntValue(baseBean.getPropValue("workflowovertime", "WORKFLOWOVERTIMETEMP"), 0);
            entity.setIsSkipWorkingDay(isSkipWorkingDay);
            entity.setChangestatus(Util.getIntValue(changestatus, 0));
            saveOrUpdateSettings(entity);
        }
        return entity;
    }


    public static synchronized boolean saveOrUpdateSettings(OverTimeSettingsEntity entity) {
        try {
            int isOpen = entity.getIsOpen();
            int scanRate = entity.getScanRate();
            int isSkipWorkingDay = entity.getIsSkipWorkingDay();
            int changestatus = Util.getIntValue(entity.getChangestatus(), 0);
            String sql = "select 1 from workflow_Settings where id = 1 ";
            RecordSet rs = new RecordSet();

            rs.executeQuery(sql);


            if (rs.next()) {
                sql = "update  workflow_Settings set isOpen = " + isOpen + ",scanRate= " + scanRate + ",isSkipWorkingDay = " + isSkipWorkingDay + ",changestatus=" + changestatus + " where id =1";
                rs.executeUpdate(sql);
            } else {
                sql = "insert into workflow_Settings(id,isOpen,scanRate,isSkipWorkingDay,changestatus) values(1," + isOpen + "," + scanRate + "," + isSkipWorkingDay + "," + changestatus + ")";
                rs.executeUpdate(sql);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }


}
