package com.engine.workflow.entity.workflowOvertime;

public class OverTimeSettingsEntity {
    public int id;//id
    public int isOpen;//是否开启超时
    public int  scanRate;//超时扫描频率
    public int isSkipWorkingDay;//是否跳过非工作日
    public int changestatus;//是否跳过非工作日

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(int isOpen) {
        this.isOpen = isOpen;
    }

    public int getScanRate() {
        return scanRate;
    }

    public void setScanRate(int scanRate) {
        this.scanRate = scanRate;
    }

    public int getIsSkipWorkingDay() {
        return isSkipWorkingDay;
    }

    public void setIsSkipWorkingDay(int isSkipWorkingDay) {
        this.isSkipWorkingDay = isSkipWorkingDay;
    }

    public String getChangestatus()  {//很多地方都是用""空字符串判断是否设置这个属性，故修改返回一个字符串
        return changestatus == 0 ? "" : changestatus+"";
    }

    public void setChangestatus(int changestatus) {
        this.changestatus = changestatus;
    }
}
