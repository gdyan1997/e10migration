package com.engine.workflow.biz;


import com.engine.workflow.constant.newRule.RuleSource;
import weaver.conn.RecordSet;


/**
 * 节点字段校验公共类
 */
public class NodeFieldCheckBiz {

    public boolean isOpenNodeFieldCheckInfo(int workflowId, int nodeId) {

        RecordSet rs = new RecordSet();

        //是否开启节点字段校验
        boolean isOpenNodeCheck = false;
        rs.executeQuery("select openNodeCheck from workflow_flownode where workflowid = ? and nodeid = ?", workflowId, nodeId);
        if (rs.next()) {
            isOpenNodeCheck = rs.getInt(1) == 1;
        }

        if (isOpenNodeCheck) {
            //获取当前配置的所有规则映射
            rs.executeQuery("select mapid from newrule_maplist where mapsrc = ? and srcid = ?", RuleSource.NODE_CHECK.getType(), nodeId);

            //打开了校验开关，但没配置规则，视为未开启，不做校验
            if (rs.getCounts() <= 0) {
                isOpenNodeCheck = false;
            }

            /**
             * 需要计算校验用到的字段，需考虑SQL和函数中用到的字段
             */
//            while (rs.next()) {
//                String mapid = rs.getString(1);
//                if("".equals(mapid)) continue;
//                rsInner.executeQuery("select objvalue from ")
//            }
        }

        return isOpenNodeCheck;
    }

}
