package com.engine.workflowDesign.entity;
/*
 * Created on 2006-05-18
 * Copyright (c) 2001-2006 泛微软件
 * 泛微协同商务系统，版权所有。
 *
 */

import java.awt.*;
import java.awt.geom.Line2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * 封装工作流对象的类. 它实现了<code>Serializable</code>接口以便可以通过对象流传递.
 * @author liuyu
 * @version 1.0
 */
public class Workflow implements Serializable {
    /** 进行模糊比较时可以忽略的最小距离 */
    final public static int DISTANCE = 9;
    /** 是否没有设置任何位置信息 */
    public boolean hasNoLayout = true;
    /** 是否全部设置了位置信息 */
    public boolean hasFullLayout = true;
    /** 全部节点的集合 */
    public Vector nodes = new Vector();
    /** 全部流向的集合 */
    public Vector lines = new Vector();
    /** 最大深度 */
    public int maxLevel = -1;
    /** 工作流的id */
    public int id = -1;
    /** 所占区域右下角的坐标, 即最大横坐标和纵坐标 */
    public Point maxPos = new Point();
    /** 是否需要重新登录的标志位 */
    public boolean needRedirect = false;
    /**表单或单据ID*/
    private String formID ="";
    /**表单或单据*/
    private int isBill = 0;
    /**是否为门户工作流*/
    private int isCust = 0;
    /**工作流名称*/
    private String workflowName ="";
    /**
     * 是否自由流程
     */
    private boolean isFree = false;
    /**
     * 是否开启表单修改日志
     */
    private boolean isModifyLog;
    /**
     * 流程分组
     */
    private List groups = new ArrayList();

    /**
     * 说明文本
     */
    private List<TextInfo> textInfos = new ArrayList();

    /**
     * 是否开启流程二次认证
     */
    private boolean isSecondAuth = false;

    /** 缺省构造函数 */
    public Workflow() {
    }

    /**
     * 返回最大深度.
     * @return 取得最大深度
     */
    public int getMaxLevel() {
        return maxLevel;
    }

    /**
     * 返回所占区域右下角的坐标, 即最大横坐标和纵坐标.
     * @return 所占区域右下角的坐标, 即最大横坐标和纵坐标
     */
    public Point getMaxPos() {
        return maxPos;
    }

    /**
     * 增加一个节点. 此方法将刷新最大位置, 是否有全部位置信息和是否没有任何位置信息的标志.
     * @param aNode 要增加的节点
     */
    public void addNode(WorkflowNode aNode) {
        nodes.add(aNode);
        refreshMaxPos(aNode);
        if (aNode.x < 0 || aNode.y < 0) {
            hasFullLayout = false;
        } else {
            hasNoLayout = false;
        }
    }

    /**
     * 增加一条流向. 此方法将刷新最大位置, 是否有全部位置信息和是否没有任何位置信息的标志.
     * @param aLine
     */
    public void addLine(WorkflowLine aLine) {
        lines.add(aLine);
        refreshMaxPos(aLine);
        if (aLine.fromPointId < 0 || aLine.toPointId < 0) {
            hasFullLayout = false;
        } else {
            hasNoLayout = false;
        }
    }

    /**
     * 根据节点的id查找节点.
     * @param id 要查找的id
     * @return WorkflowNode 查找到的节点
     */
    public WorkflowNode findNode(int id) {
        for (int i=0;i<nodes.size();i++) {
            WorkflowNode aNode = getNode(i);
            if (aNode.id == id) {
                return aNode;
            }
        }
        return null;
    }

    /**
     * 取得指定位置的节点.
     * @param index 指定节点的索引
     */
    public WorkflowNode getNode(int index) {
        return (WorkflowNode)nodes.get(index);
    }

    /**
     * 取得指定位置的流向.
     * @param index 指定流向的索引
     */
    public WorkflowLine getLine(int index) {
        return (WorkflowLine)lines.get(index);
    }

    /**
     * 取得位于指定层次的节点.
     * @param level 指定的层次
     * @return Vector 位于该层次的节点集合
     */
    public Vector getNodesByLevel(int level) {
        Vector result = new Vector();
        for (int i=0;i<nodes.size();i++) {
            WorkflowNode aNode = getNode(i);
            if (aNode.level == level) {
                result.add(aNode);
            }
        }
        return result;
    }

    /**
     * 取得整个工作流的开始节点.
     * @return 整个工作流的开始节点
     */
    public WorkflowNode getStartNode() {
        for (int i=0;i<nodes.size();i++) {
            if (getNode(i).isStartNode()) {
                return getNode(i);
            }
        }
        return null;
    }

    /**
     * 清理对象间的相互引用关系.
     */
    public void clearObjRef() {
        int i;
        for (i=0;i<nodes.size();i++) {
            WorkflowNode node = getNode(i);
            node.inLines.clear();
            node.outLines.clear();
        }
        for (i=0;i<lines.size();i++) {
            WorkflowLine line = getLine(i);
            line.fromNode = null;
            line.toNode = null;
        }
    }

    /**
     * 重建对象间的相互引用关系.
     */
    public void buildObjRef() {
        int i;
        Vector temp = new Vector();
        for (i=0;i<lines.size();i++) {
            WorkflowLine aLine = getLine(i);
            aLine.fromNode = findNode(aLine.fromNodeId);
            aLine.toNode = findNode(aLine.toNodeId);
            if (aLine.fromNode != null) {
                aLine.fromNode.outLines.add(aLine);
            } else {
                temp.add(aLine);
            }
            if (aLine.toNode != null) {
                aLine.toNode.inLines.add(aLine);
            } else {
                temp.add(aLine);
            }
        }
        for (i=0;i<temp.size();i++) {
            lines.remove(temp.get(i));
        }
    }

    public void addHelpText(TextInfo textInfo) {
        this.textInfos.add(textInfo);
    }

    /**
     * 重新计算各个节点的层次.
     */
    public void buildTree() {
        buildTree(getStartNode(), 0);
    }

    /**
     * 检查并自动设置位置信息.
     * 本方法区分两种情况:完全没有设置位置信息和只有部分位置信息的情况. 两种情况将使用不
     * 同的自动布局方法来设置位置信息.
     * 只有部分位置信息的情况下从最大位置(或当指定位置大于最大位置时, 从指定位置开始)从左到右排列各个部门.
     * @param startx 自动排列时的开始横坐标
     * @param starty 自动排列时的开始纵坐标
     * @param rowSpace 自动排列时的行间距
     * @param columnSpace 自动排列时的列间距
     */
    public void checkAndAutosetLayout(int startx, int starty, int rowSpace, int columnSpace) {
        if (hasNoLayout) {
            //System.out.println("autosetLayout");
            autosetLayout(startx, starty, rowSpace, columnSpace);
        } else if (!hasFullLayout) {
            if (maxPos.y > starty) {
                autosetPart(startx, maxPos.y + rowSpace, columnSpace);
            } else {
                autosetPart(startx, starty + rowSpace, columnSpace);
            }
        }
    }

    /**
     * 自动设置位置信息, 适用于完全没有设置位置信息的情况. 本方法将刷新最大位置.
     * @param startx 自动排列时的开始横坐标
     * @param starty 自动排列时的开始纵坐标
     * @param rowSpace 自动排列时的行间距
     * @param columnSpace 自动排列时的列间距
     */
    public void autosetLayout(int startx, int starty, int rowSpace, int columnSpace) {
        starty = starty > (rowSpace/2+10)?starty:(rowSpace/2+10);
        //System.out.println("autosetLayoutForNode");
        autosetLayoutForNode(startx, starty, rowSpace, columnSpace, getStartNode());
        //System.out.println("autosetLayoutForLine");
        autosetLayoutForLine(rowSpace, columnSpace);
    }

    /**
     * 自动设置位置信息, 适用于只有部分位置信息的情况. 本方法将刷新最大位置.
     * @param startx 自动排列时的开始横坐标
     * @param starty 自动排列时的开始纵坐标
     * @param columnSpace 自动排列时的列间距
     */
    public void autosetPart(int startx, int starty, int columnSpace) {
        int i, j;
        int currx = startx;
        for (i=0;i<nodes.size();i++) {
            WorkflowNode node = getNode(i);
            if (node.x < 0 || node.y < 0) {
                node.x = currx;
                node.y = starty;
                currx = currx + node.getWidth() + columnSpace;
                for (j=0;j<node.inLines.size();j++) {
                    ((WorkflowLine)node.inLines.get(j)).toPointId = 8;
                }
                for (j=0;j<node.outLines.size();j++) {
                    ((WorkflowLine)node.outLines.get(j)).fromPointId = 0;
                }
            }
        }
        if (currx - columnSpace > maxPos.x) maxPos.x = currx - columnSpace;
    }

    /**
     * 判断两个点是否大致相等.
     * @param p1 第一个点
     * @param p2 第二个点
     * @return 指定点是否大致相等
     */
    public static boolean approximateEqual(Point p1, Point p2) {
        if (p1.distance(p2) <= DISTANCE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断一个点是否大致位于一条线段上.
     * @param x1 线段的起始点的横坐标
     * @param y1 线段的起始点的纵坐标
     * @param x2 线段的终止点的横坐标
     * @param y2 线段的终止点的纵坐标
     * @param x3 指定点的横坐标
     * @param y3 指定点的纵坐标
     * @return 一个点是否大致位于一条线段上
     */
    public static boolean approximateIntersect(int x1, int y1, int x2, int y2, int x3, int y3) {
        if (Line2D.Double.ptSegDist(x1, y1, x2, y2, x3, y3) <= DISTANCE) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 针对一个节点刷新最大位置.
     */
    protected void refreshMaxPos(WorkflowNode aNode) {
        if (aNode.x + aNode.getWidth() > maxPos.x) maxPos.x = aNode.x + aNode.getWidth();
        if (aNode.y + aNode.getHeight() > maxPos.y) maxPos.y = aNode.y + aNode.getHeight();
    }

    /**
     * 针对一个流向刷新最大位置.
     */
    protected void refreshMaxPos(WorkflowLine aLine) {
        for (int i=0;i<aLine.getValidCtrlPointCount();i++) {
            Point p = aLine.getCtrlPoint(i);
            if (p.x > maxPos.x) maxPos.x = p.x;
            if (p.y > maxPos.y) maxPos.y = p.y;
        }
    }

    /**
     * 计算指定节点及其之下的所有子节点的层次. 一个节点位于多个层次时取最高层次.
     * @param node 指定节点
     * @param level 当前层次
     */
    protected void buildTree(WorkflowNode node, int level) {
        int i;
        if (node == null) return;
        if (node.level < 0 || node.level > level) {
            node.level = level;
            Vector forwards = node.getForwardNodes();
            for (i=0;i<forwards.size();i++) {
                buildTree((WorkflowNode)forwards.get(i), level+1);
            }
        }
    }

    /**
     * 自动排列节点的位置. 排列的原则是节点按照层次高低从左向右排列, 同层节点从上往下排列.
     * @param startx 开始位置横坐标
     * @param starty 开始位置纵坐标
     * @param rowSpace 行间距
     * @param columnSpace 列间距
     * @param node 要设置的节点
     */
    protected void autosetLayoutForNode(int startx, int starty, int rowSpace, int columnSpace, WorkflowNode node) {
        int i;

        if (node != null) {
            node.x = startx;
            node.y = starty;
            refreshMaxPos(node);
            Vector forwards = node.getForwardNodes();
            int curry = starty;
            for (i=0;i<forwards.size();i++) {
                WorkflowNode fnode = (WorkflowNode)forwards.get(i);
                if (fnode.level == node.level+1) {
                    autosetLayoutForNode(startx+node.getWidth()+columnSpace, curry, rowSpace, columnSpace, fnode);
                    curry = curry + fnode.getHeight() + rowSpace;
                }
            }
        }
    }

    /**
     * 自动排列流向的形状(即控制点)和连接点位置.
     * @param rowSpace 行间距
     * @param columnSpace 列间距
     */
    protected void autosetLayoutForLine(int rowSpace, int columnSpace) {
        for (int i=0;i<lines.size();i++) {
            WorkflowLine line = getLine(i);
            WorkflowNode from = line.getFromNode();
            WorkflowNode to = line.getToNode();
            if (from.y > to.y) {
                /* 如果流向起始点在终止点下方, 注意屏幕坐标系同笛卡儿坐标系y轴方向相反 */
                if (from.x < to.x) {
                    /* 如果流向起始点在终止点左方 */
                    line.fromPointId = 15;
                    line.toPointId = 7;
                    line.controlPoints.clear();
                    line.controlPoints.add(new Point(from.x+from.getWidth()+columnSpace/2, from.getConnectionPoint(15).y));
                    line.controlPoints.add(new Point(from.x+from.getWidth()+columnSpace/2, to.getConnectionPoint(7).y));
                } else if (from.x > to.x) {
                    /* 如果流向起始点在终止点右方 */
                    line.fromPointId = 9;
                    line.toPointId = 1;
                    line.controlPoints.clear();
                    line.controlPoints.add(new Point(from.x-columnSpace/2, from.getConnectionPoint(9).y));
                    line.controlPoints.add(new Point(from.x-columnSpace/2, to.getConnectionPoint(1).y));
                } else {
                    /* 如果流向起始点和终止点在同一列 */
                    line.fromPointId = 12;
                    line.toPointId = 4;
                    line.controlPoints.clear();
                }
            } else if (from.y < to.y) {
                /* 如果流向起始点在终止点上方 */
                if (from.x < to.x) {
                    /* 如果流向起始点在终止点左方 */
                    line.fromPointId = 1;
                    line.toPointId = 9;
                    line.controlPoints.clear();
                    line.controlPoints.add(new Point(from.x+from.getWidth()+columnSpace/2, from.getConnectionPoint(1).y));
                    line.controlPoints.add(new Point(from.x+from.getWidth()+columnSpace/2, to.getConnectionPoint(9).y));
                } else if (from.x > to.x) {
                    /* 如果流向起始点在终止点右方 */
                    line.fromPointId = 7;
                    line.toPointId = 15;
                    line.controlPoints.clear();
                    line.controlPoints.add(new Point(from.x-columnSpace/2, from.getConnectionPoint(7).y));
                    line.controlPoints.add(new Point(from.x-columnSpace/2, to.getConnectionPoint(15).y));
                } else {
                    /* 如果流向起始点和终止点在同一列 */
                    line.fromPointId = 4;
                    line.toPointId = 12;
                    line.controlPoints.clear();
                }
            } else {
                /* 如果流向起始点和终止点在同一高度 */
                if (from.x < to.x) {
                    /* 如果流向起始点在终止点左方 */
                    line.fromPointId = 0;
                    line.toPointId = 8;
                    line.controlPoints.clear();
                } else if (from.x > to.x) {
                    /* 如果流向起始点在终止点右方 */
                    line.fromPointId = 11;
                    line.toPointId = 13;
                    line.controlPoints.clear();
                    line.controlPoints.add(new Point(from.getConnectionPoint(11).x, from.y-rowSpace/2));
                    line.controlPoints.add(new Point(to.getConnectionPoint(13).x, from.y-rowSpace/2));
                } else {
                    /* 如果流向起始点和终止点在同一列, 此时起始点和终止点重合, 流向将绕一个小圈回来 */
                    line.fromPointId = 1;
                    line.toPointId = 3;
                    line.controlPoints.clear();
                    line.controlPoints.add(new Point(from.x+from.getWidth()+columnSpace/2, from.getConnectionPoint(1).y));
                    line.controlPoints.add(new Point(from.x+from.getWidth()+columnSpace/2, from.y+from.getHeight()+rowSpace/2));
                    line.controlPoints.add(new Point(to.getConnectionPoint(3).x, from.y+from.getHeight()+rowSpace/2));
                }
            }
            refreshMaxPos(line);
        }
    }

    /**
     * 获取表单或单据ID
     * @return
     */
    public String getFormID() {
        return formID;
    }
    /**
     * 设置表单或单据ID
     * @param formID
     */
    public void setFormID(String formID) {
        this.formID = formID;
    }

    /**
     * 获取是否为表单或者单据
     * @return
     */
    public int getIsBill() {
        return isBill;
    }

    /**
     * 设置为表单或者单据
     * @param isBill
     */
    public void setIsBill(int isBill) {
        this.isBill = isBill;
    }

    /**
     * 获取是否为门户工作流
     * @return
     */
    public int getIsCust() {
        return isCust;
    }

    /**
     * 设置是否为门户工作流
     * @param isCust
     */
    public void setIsCust(int isCust) {
        this.isCust = isCust;
    }
    /**
     * 获取工作流名称
     * @return
     */
    public String getWorkflowName() {
        return workflowName;
    }

    /**
     * 设置工作流名称
     * @param workflowName
     */
    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public List getGroups() {
        return groups;
    }

    public void setGroups(List groups) {
        this.groups = groups;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isModifyLog() {
        return isModifyLog;
    }

    public void setModifyLog(boolean modifyLog) {
        isModifyLog = modifyLog;
    }

    public List<TextInfo> getTextInfos() {
        return textInfos;
    }

    public void setTextInfos(List<TextInfo> textInfo) {
        this.textInfos = textInfo;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public boolean isSecondAuth() {
        return isSecondAuth;
    }

    public void setSecondAuth(boolean secondAuth) {
        isSecondAuth = secondAuth;
    }
}