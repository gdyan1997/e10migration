package com.engine.workflowDesign.entity;
/*
* Created on 2006-05-18
* Copyright (c) 2001-2006 泛微软件
* 泛微协同商务系统，版权所有。
* 
*/

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * 封装工作流流向对象的类. 它实现了<code>Serializable</code>接口以便可以通过对象流传递.
 * @author liuyu
 * @version 1.0
 */

public class WorkflowLine implements Serializable {
    /** 流向起点索引常量 */
    static final public int INDEX_FROM = -2;
    /** 流向终点索引常量 */
    static final public int INDEX_TO = -3;
    /** 流向id号 */
    public int id = -1;
    /** 起点节点id */
    public int fromNodeId = -1;
    /** 终点节点id */
    public int toNodeId = -1;
    /** 出口顺序 */
    public int linkorder = -1;
    /** 起点连接点索引 */
    public int fromPointId = -1;
    /** 终点连接点索引 */
    public int toPointId = -1;
    /** 起点节点对象 */
    public WorkflowNode fromNode = null;
    /** 终点节点对象 */
    public WorkflowNode toNode = null;
    /** 控制点集合 */
    public Vector controlPoints = new Vector();
    /** 终点箭头两翼长度 */
    static final public int WING_LENGTH = 8;
    /** 终点箭头两翼与流向线的夹角 */
    static final public double WING_ANGLE = Math.PI / 6;
    
    /**出口名称*/
    private String lineName ="";
    
    /**出口提示信息*/
    private String remindMsg = "";
    
    /**是否生成编号 1 : 是  0或其他 ：否*/
    private String isBuildCodeString ="0";
    private String isMustpass = "0";//分叉是否必须能过 1：是，0或其它：否
    private String isReject = "0";//是否退回 1：是，0或其它：否
    
    private boolean hasRole = false; //是否有出口附加规则
    
    private String condition = ""; //出口条件
    
    private int nodePassHour =0; //出口通过最大小时数
    private int nodePassMinute = 0; //出口通过最大分钟数
    
    private int startDirection = 0; //出口起点方向
    
    private int endDirection = 0; //出口结束方向
    
    //新版流程图坐标集合(E8)
    private String newPoints = "";

    //E9新版流程图坐标集合
    private String e9Points = "";
    
    /*出口的各种属性字段 by cyril on 2008-12-17 for td:9573*/
    public Map attrMap = new HashMap();

    /** E9新版流程图属性 **/

    private String style = "";//新版流程图的style属性

    private double entryX = 0;

    private double entryY = 0;

    private double exitX = 0;

    private double exitY = 0;
    
    /** 缺省构造函数 */
    public WorkflowLine() {
    }
    
    /**
     * 取得流向的id号.
     * @return 流向的id号
     */
    public int getId() {
        return id;
    }
    
    /**
     * 取得起始节点对象.
     * @return 起始节点对象
     */
    public WorkflowNode getFromNode() {
        return fromNode;
    }
    
    /**
     * 取得终止节点对象.
     * @return 终止节点对象
     */
    public WorkflowNode getToNode() {
        return toNode;
    }
    
    /**
     * 取得起始节点的连接点.
     * @return 起始节点的连接点
     */
    public Point getFromPoint() {
        return fromNode.getConnectionPoint(fromPointId);
    }
    
    /**
     * 取得终止节点的连接点.
     * @return 终止节点的连接点
     */
    public Point getToPoint() {
        return toNode.getConnectionPoint(toPointId);
    }
    
    /**
     * 取得最大控制点数目. 由于数据库表的缘故, 这里只能保存5个控制点.
     * @return 最大控制点数目
     */
    public static int getMaxCtrlPointCount() {
        return 5;
    }
    
    /**
     * 取得实际的控制点数目.
     * @return 实际的控制点数目
     */
    public int getValidCtrlPointCount() {
        return controlPoints.size();
    }
    
    /**
     * 判断本流向是否和指定点相撞, 并返回相撞点相对于已有控制点的位置. 
     * 判断是否相撞采用模糊判断法, 模糊匹配的方法由Workflow的{@link Workflow#approximateIntersect <code>approximateIntersect()</code>方法}规定.
     * @param x 指定点的横坐标
     * @param y 指定点的纵坐标
     * @return 相撞点相对于已有控制点的位置
     */
    public int isHited(int x, int y) {
        Point start;
        Point end;
        start = getFromPoint();
        if (controlPoints.size() > 0) {
            for (int i=0;i<controlPoints.size();i++) {
                end = getCtrlPoint(i);
                if (Workflow.approximateIntersect(start.x, start.y, end.x, end.y, x, y)) {
                    return i;
                }
                start = end;
            }
        }
        end = getToPoint();
        if (Workflow.approximateIntersect(start.x, start.y, end.x, end.y, x, y)) {
            return controlPoints.size();
        } else {
            return -1;
        }
    }

    /**
     * 在指定的位置增加一个控制点
     * @param index 指定位置的索引
     * @param x 控制点的横坐标
     * @param y 控制点的纵坐标
     */
    public void addCtrPoint(int index, int x, int y) {
        controlPoints.add(index, new Point(x, y));
    }

    /**
     * 模糊查找一个控制点并返回它的索引.
     * 模糊比较每一个控制点的坐标, 模糊匹配的方法由Workflow的{@link Workflow#approximateEqual <code>approximateEqual()</code>方法}规定.
     * 连接线的起点和终点坐标也将参与比较.
     * @param x 指定的横坐标
     * @param y 指定的纵坐标
     * @return 查找到的第一个符合条件的控制点的索引, 如果没有查到则返回-1
     */
    public int searchCtrlPoint(int x, int y) {
        Point p;
        Point p1 = new Point(x, y);
        for (int i=0;i<controlPoints.size();i++) {
            p = getCtrlPoint(i);
            if (Workflow.approximateEqual(p, p1)) {
                return i;
            }
        }
        if (Workflow.approximateEqual(getFromPoint(), p1)) return INDEX_FROM;
        if (Workflow.approximateEqual(getToPoint(), p1)) return INDEX_TO;
        return -1;
    }
    
    /**
     * 查找距离指定点最近的控制点并返回它的索引. 
     * @param x 指定的横坐标的
     * @param y 指定的纵坐标
     * @return 距离最近的控制点的索引
     */
    public int searchNearestCtrlPoint(int x, int y) {
        Point p;
        Point p1 = new Point(x, y);
        double distance = Double.MAX_VALUE;
        double temp;
        int result = -1;
        
        for (int i=0;i<controlPoints.size();i++) {
            p = getCtrlPoint(i);
            temp = p.distance(p1);
            if (temp < distance) {
                distance = temp;
                result = i;
            }
        }
        
        temp = getFromPoint().distance(p1);
        if (temp < distance) {
            distance = temp;
            result = INDEX_FROM;
        }
        
        temp = getToPoint().distance(p1);
        if (temp < distance) {
            distance = temp;
            result = INDEX_TO;
        }
        
        return result;
    }
    
    /**
     * 返回指定的控制点.
     * @param index 指定点的索引
     * @return 控制点
     */
    public Point getCtrlPoint(int index) {
        if (index == INDEX_FROM) {
            return getFromPoint();
        } else if (index == INDEX_TO) {
            return getToPoint();
        } else if (index >= 0 && index <controlPoints.size()) {
            return (Point)controlPoints.get(index);
        } else {
            return null;
        }
    }
    
    /**
     * 移动指定控制点到指定位置.
     * @param index 指定控制点的索引
     * @param x 指定位置横坐标
     * @param y 指定位置纵坐标
     */
    public void moveCtrlPoint(int index, int x, int y) {
        int newPoint;
        switch (index) {
            case INDEX_FROM:
                newPoint = fromNode.searchConnectionPoint(x, y);
                if (newPoint >= 0) {
                    fromPointId = newPoint;
                }
                break;
            case INDEX_TO:
                newPoint = toNode.searchConnectionPoint(x, y);
                if (newPoint >= 0) {
                    toPointId = newPoint;
                }
                break;
            default:
                if (index >= 0 && index < controlPoints.size()) {
                    Point p = getCtrlPoint(index);
                    p.x = x;
                    p.y = y;
                }
       }
    }
    
    /**
     * 删除指定的控制点.
     * @param index 指定控制点的索引
     */
    public void deleteCtrlPoint(int index) {
        controlPoints.remove(index);
    }
    
    /**
     * 计算某个点到原点的连线和x轴正向的夹角.
     * @param x 指定点的横坐标
     * @param y 指定点的纵坐标
     */
    public double calcAngle(double x, double y) {
        y = -y;
        if (x == 0.0) {
            return y>0?(-Math.PI/2):(Math.PI/2);
        } else {
            return x>0?Math.PI+Math.atan(y/x):Math.atan(y/x);
        }
    }

    /**
     * 在指定的绘图环境下绘制自身.
     * @param g 指定的绘图环境
     */
    public void drawSelf(Graphics g) {
        Point start;
        Point end;
        if (fromNode != null && toNode != null) {
            /* 绘制各段连线 */
            start = fromNode.getConnectionPoint(fromPointId);
            if (controlPoints.size() > 0) {
                for (int i=0;i<controlPoints.size();i++) {
                    end = (Point)controlPoints.get(i);
                    g.drawLine(start.x, start.y, end.x, end.y);
                    start = end;
                }
            }
            end = toNode.getConnectionPoint(toPointId);
            g.drawLine(start.x, start.y, end.x, end.y);
            
            /* 绘制箭头 */
            double a1 = calcAngle(start.x-end.x, start.y-end.y);
            AffineTransform trans = new AffineTransform();
            Point ps = new Point(-WING_LENGTH, 0);
            Point pt = new Point();
            
            trans.rotate(a1 + WING_ANGLE);
            trans.deltaTransform(ps, pt);
            g.drawLine(pt.x+end.x, end.y-pt.y, end.x, end.y);
            
            trans.rotate(-a1 - WING_ANGLE);
            trans.rotate(a1 - WING_ANGLE);
            trans.deltaTransform(ps, pt);
            g.drawLine(pt.x+end.x, end.y-pt.y, end.x, end.y);
        }
    }
    /**
     * 获取出口名称
     * @return 出口名称
     */
	public String getLineName() {
		return lineName;
	}

	/**
	 * 设置出口名称
	 * @param lineName 出口名称
	 */
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}

	/**
	 * 获取出口提示信息
	 * @return 出口提示信息
	 */
	public String getRemindMsg() {
		return remindMsg;
	}

	/**
	 * 设置出口提示信息
	 * @param remindMsg 出口提示信息
	 */
	public void setRemindMsg(String remindMsg) {
		this.remindMsg = remindMsg;
	}
	
	/**
	 * 获取是否生成编号
	 * @return 是否生成编号 1：是  0或其他：否
	 */
	public String getIsBuildCodeString() {
		return isBuildCodeString;
	}

	/**
	 * 设置是否生成编号
	 * @param isBuildCodeString 是否生成编号 1：是  0或其他：否
	 */
	public void setIsBuildCodeString(String isBuildCodeString) {
		this.isBuildCodeString = isBuildCodeString;
	}

	public Map getAttrMap() {
		return attrMap;
	}

	public void setAttrMap(Map attrMap) {
		this.attrMap = attrMap;
	}

	public String getIsMustpass() {
		return isMustpass;
	}

	public void setIsMustpass(String isMustpass) {
		this.isMustpass = isMustpass;
	}

	/**
	 * 获取是否退回
	 * @return
	 */
	public String getIsReject() {
		return isReject;
	}

	/**
	 * 设置是否退回
	 * @param isReject
	 */
	public void setIsReject(String isReject) {
		this.isReject = isReject;
	}

	/**
	 * 是否有出口附加规则
	 * @return
	 */
	public boolean isHasRole() {
		return hasRole;
	}

	/**
	 * 设置是否有出口附加规则
	 * @param hasRole
	 */
	public void setHasRole(boolean hasRole) {
		this.hasRole = hasRole;
	}

	/**
	 * 获取出口条件信息
	 * @return
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * 设置出口条件信息
	 * @param condition
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	
	/**
	 * 得到节点出口通过最大小时数
	 * @return 节点出口通过最大小时数
	 */
	public int getNodePassHour() {
		return nodePassHour;
	}

	/**
	 * 设置节点出口通过最大小时数
	 * @param nodePassHour 节点出口通过最大小时数
	 */
	public void setNodePassHour(int nodePassHour) {
		this.nodePassHour = nodePassHour;
	}

	/**
	 * 得到节点出口通过最大分钟数
	 * @return 节点出口通过最大分钟数
	 */
	public int getNodePassMinute() {
		return nodePassMinute;
	}

	/**
	 * 设置节点出口通过最大分钟数
	 * @param nodePassMinute 节点出口通过最大分钟数
	 */
	public void setNodePassMinute(int nodePassMinute) {
		this.nodePassMinute = nodePassMinute;
	}

	public int getStartDirection() {
		return startDirection;
	}

	public void setStartDirection(int startDirection) {
		this.startDirection = startDirection;
	}

	public int getEndDirection() {
		return endDirection;
	}

	public void setEndDirection(int endDirection) {
		this.endDirection = endDirection;
	}

	public String getNewPoints() {
		return newPoints;
	}

	public void setNewPoints(String newPoints) {
		this.newPoints = newPoints;
	}

	public int getFromNodeId() {
		return fromNodeId;
	}

	public void setFromNodeId(int fromNodeId) {
		this.fromNodeId = fromNodeId;
	}

	public int getToNodeId() {
		return toNodeId;
	}

	public void setToNodeId(int toNodeId) {
		this.toNodeId = toNodeId;
	}

	public int getLinkorder() {
		return linkorder;
	}

	public void setLinkorder(int linkorder) {
		this.linkorder = linkorder;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFromPointId() {
		return fromPointId;
	}

	public void setFromPointId(int fromPointId) {
		this.fromPointId = fromPointId;
	}

	public int getToPointId() {
		return toPointId;
	}

	public void setToPointId(int toPointId) {
		this.toPointId = toPointId;
	}

	public Vector getControlPoints() {
		return controlPoints;
	}

	public void setControlPoints(Vector controlPoints) {
		this.controlPoints = controlPoints;
	}

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public double getEntryX() {
        return entryX;
    }

    public void setEntryX(double entryX) {
        this.entryX = entryX;
    }

    public double getEntryY() {
        return entryY;
    }

    public void setEntryY(double entryY) {
        this.entryY = entryY;
    }

    public double getExitX() {
        return exitX;
    }

    public void setExitX(double exitX) {
        this.exitX = exitX;
    }

    public double getExitY() {
        return exitY;
    }

    public void setExitY(double exitY) {
        this.exitY = exitY;
    }

    public String getE9Points() {
        return e9Points;
    }

    public void setE9Points(String e9Points) {
        this.e9Points = e9Points;
    }
}