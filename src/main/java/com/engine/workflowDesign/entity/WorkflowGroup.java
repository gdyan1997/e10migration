package com.engine.workflowDesign.entity;

import java.io.Serializable;

/*
 * Created on 2013-01-14
 * Copyright (c) 2001-2006 泛微软件
 * 泛微协同商务系统，版权所有。
 * 
 */

/**
 * 工作流分组对象信息类
 * @author liuyu
 * @version 1.0
 */
public class WorkflowGroup implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 分组id
	 */
	private int id; 
	
	/**
	 * 组所属工作流id
	 */
	private int workflowid; 
	
	/**
	 * 组名
	 */
	private String groupname;
	
	/**
	 * 组方向（0：分横向，1：纵向）
	 */
	private double direction;
	
	/**
	 * 组x坐标
	 */
	private double x;
	
	/**
	 * 组y坐标
	 */
	private double y;
	
	/**
	 * 组宽度
	 */
	private double width;
	
	/**
	 * 组高度
	 */
	private double height;

	/**
	 * 是否是新建
	 */
	private boolean isNew = false;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWorkflowid() {
		return workflowid;
	}

	public void setWorkflowid(int workflowid) {
		this.workflowid = workflowid;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	public double getDirection() {
		return direction;
	}

	public void setDirection(double direction) {
		this.direction = direction;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

}
