package com.engine.workflowDesign.entity;
/*
* Created on 2006-05-18
* Copyright (c) 2001-2006 泛微软件
* 泛微协同商务系统，版权所有。
* 
*/

import java.awt.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * 封装工作流节点对象的类. 它实现了<code>Serializable</code>接口以便可以通过对象流传递.
 * @author liuyu
 * @version 1.0
 */
public class WorkflowNode implements Serializable {
    
    /** 左上角横坐标 */
    public int x = -1;
    /** 左上角纵坐标 */
    public int y = -1;
    /** 流入流向对象集合 */
    public Vector inLines = new Vector();
    /** 流出流向对象集合 */
    public Vector outLines = new Vector();
    /** 节点id */
    public int id = -1;
    /** 节点名称 */
    public String name = "";
    /** 节点类型 */
    public String nodeType = "";
    /** 节点顺序 */
    public int nodeorder = -1;
    /** 节点所处层次 */
    public int level = -1;
    /** 节点宽度的一半. 原有程序使用这种计算方法来输出到JSP页面上, 为了保持一致, 这里也使用宽度的一半来计算 */
    final static public int NODE_X_SIZE = 60;
    /** 节点高度的一半. 原有程序使用这种计算方法来输出到JSP页面上, 为了保持一致, 这里也使用高度的一半来计算 */
    final static public int NODE_Y_SIZE = 40;
    /**节点属性*/
    private String nodeAttribute ="";
    private int passnum;//通过的分支数量
    
//start_td19600 
    private boolean hasNodePro = false;
    private boolean hasCusRigKey = false;
    private boolean hasNodeBefAddOpr = false;
    private boolean hasNodeAftAddOpr = false;
    private boolean hasLogViewSco = false;
    private boolean hasNodeForFie = false;
    private boolean hasOvertimeSet = false;
    // end_td19600
    
    //liuzy 20140903 E8 标题显示设置、签字意见设置、转发设置、自由流转设置
    private boolean hasOperateTitle = false;
    private boolean hasOperateSign = false;
    private boolean hasOperateForward = false;
    private boolean hasOperateFreewf = false;
    private boolean hasOperateSubwf = false;
    private boolean hasOperateException = false;
    private boolean selectNextFlow = false;
	private boolean hasSecondAuth = false;      //是否有二次提交认证的设置
    private boolean nodeFieldCheck = false; //合规字段校验
    /*节点的各种属性字段 by cyril on 2008-12-17 for td:9573*/
    public Map attrMap = new HashMap();

    private String style = "";//新版流程图的style属性

    /** 缺省构造函数. */
    public WorkflowNode() {
    }
    
    /**
     * 返回节点的id号.
     * @return 节点的id号
     */
    public int getId() {
        return id;
    }
    
    /**
     * 返回节点的名称.
     * @return 节点的名称
     */
    public String getName() {
        return name;
    }
    
    /**
     * 返回节点的类型.
     * @return 节点的类型
     */
    public String getNodeType() {
        return nodeType;
    }
    
    /**
     * 返回节点的顺序.
     * @return 节点的顺序
     */
    public int getNodeorder() {
        return nodeorder;
    }
    
    /**
     * 返回节点的层次.
     * @return 节点的层次
     */
    public int getLevel() {
        return level;
    }
    
    /**
     * 返回节点的宽度.
     * @return 节点的宽度
     * 93*93   创建归档
     * 110*100  审批
     * 108*70 处理
     */
    public int getWidth() {
//        return NODE_X_SIZE * 2;
        if (this.nodeType.equals("0") || this.nodeType.equals("3")) {
            return 93;
        } else if(this.nodeType.equals("1")) {
            return 110;
        } else if (this.nodeType.equals("2")) {
            return 108;
        }
        return NODE_X_SIZE * 2;
    }

    
    /**
     * 返回节点的高度.
     * @return 节点的高度
     * 93*93   创建归档
     * 110*100  审批
     * 108*70 处理
     */
    public int getHeight() {
        if (this.nodeType.equals("0") || this.nodeType.equals("3")) {
            return 93;
        } else if(this.nodeType.equals("1")) {
            return 100;
        } else if (this.nodeType.equals("2")) {
            return 70;
        } else {
            return NODE_Y_SIZE * 2;
        }
    }
    
    /**
     * 在指定的绘图环境中绘制自身.
     * @param g 绘图环境
     */
    public void drawSelf(Graphics g) {
        g.drawRect(x, y, getWidth(), getHeight());
        g.drawString(name, x+5, y+15);
    }
    
    /**
     * 取得指定连接点的坐标. 右边中线连接点的索引是0, 沿顺时针循环依次是0-15.
     * @param index 指定连接点索引
     * @return 指定连接点坐标
     */
    public Point getConnectionPoint(int index) {
        switch (index) {
            case 0: return new Point(x+getWidth(), y+getHeight()*2/4);
            case 1: return new Point(x+getWidth(), y+getHeight()*3/4);
            case 2: return new Point(x+getWidth(), y+getHeight());
            case 3: return new Point(x+getWidth()*3/4, y+getHeight());
            case 4: return new Point(x+getWidth()*2/4, y+getHeight());
            case 5: return new Point(x+getWidth()*1/4, y+getHeight());
            case 6: return new Point(x, y+getHeight());
            case 7: return new Point(x, y+getHeight()*3/4);
            case 8: return new Point(x, y+getHeight()*2/4);
            case 9: return new Point(x, y+getHeight()*1/4);
            case 10: return new Point(x, y);
            case 11: return new Point(x+getWidth()*1/4, y);
            case 12: return new Point(x+getWidth()*2/4, y);
            case 13: return new Point(x+getWidth()*3/4, y);
            case 14: return new Point(x+getWidth(), y);
            case 15: return new Point(x+getWidth(), y+getHeight()*1/4);
            default: return new Point(x+getWidth(), y+getHeight()*2/4);
        }
    }
    
    /**
     * 根据坐标查找符合的连接点.
     * @param x 横坐标
     * @param y 纵坐标
     * @return 查找到的连接点, 未找到返回<code>null</code>
     */
    public Point findConnectionPoint(int x, int y) {
        int index = searchConnectionPoint(x, y);
        if (index >= 0) {
            return getConnectionPoint(index);
        } else {
            return null;
        }
    }
    
    /** 
     * 根据坐标查找符合的连接点的索引.
     * @param x 横坐标
     * @param y 纵坐标
     * @return 查找到的连接点的索引, 未找到返回<code>-1</code>
     */
    public int searchConnectionPoint(int x, int y) {
        Point p1 = new Point(x, y);
        for (int i=0;i<16;i++) {
            Point p2 = getConnectionPoint(i);
            if (Workflow.approximateEqual(p1, p2)) {
                return i;
            }
        }
        return -1;
    }
    
    /**
     * 判断指定的坐标是否在自身范围内.
     * @param x 横坐标
     * @param y 纵坐标
     * @return 是否在自身范围内
     */
    public boolean isOnMe(int x, int y) {
        if (this.x <= x && this.y <= y && this.x+getWidth() >= x && this.y+getHeight() >= y) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 返回自身是否是开始节点.
     * @return 自身是否是开始节点
     */
    public boolean isStartNode() {
        if (nodeType.equals("0")) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * 取得所有前趋节点.
     * @return 所有前趋节点的集合
     */
    public Vector getForwardNodes() {
        Vector result = new Vector();
        for (int i=0;i<outLines.size();i++) {
            result.add(((WorkflowLine)outLines.get(i)).getToNode());
        }
        return result;
    }
    
    /**
     * 取得所有后继节点.
     * @return 所有后继节点的集合
     */
    public Vector getBackwardNodes() {
        Vector result = new Vector();
        for (int i=0;i<inLines.size();i++) {
            result.add(((WorkflowLine)inLines.get(i)).getFromNode());
        }
        return result;
    }

    /**
     * 取得节点属性
     * @return 
     */
	public String getNodeAttribute() {
		return nodeAttribute;
	}

	/**
	 * 设置节点属性
	 * @param nodeAttribute
	 */
	public void setNodeAttribute(String nodeAttribute) {
		this.nodeAttribute = nodeAttribute;
	}

	public Map getAttrMap() {
		return attrMap;
	}

	public void setAttrMap(Map attrMap) {
		this.attrMap = attrMap;
	}

	public int getPassnum() {
		return passnum;
	}

	public void setPassnum(int passnum) {
		this.passnum = passnum;
	}
		//start_td19600
	public boolean isHasNodePro() {
		return hasNodePro;
	}

	public void setHasNodePro(boolean hasNodePro) {
		this.hasNodePro = hasNodePro;
	}

	public boolean isHasCusRigKey() {
		return hasCusRigKey;
	}

	public void setHasCusRigKey(boolean hasCusRigKey) {
		this.hasCusRigKey = hasCusRigKey;
	}

	public boolean isHasNodeBefAddOpr() {
		return hasNodeBefAddOpr;
	}

	public void setHasNodeBefAddOpr(boolean hasNodeBefAddOpr) {
		this.hasNodeBefAddOpr = hasNodeBefAddOpr;
	}

	public boolean isHasNodeAftAddOpr() {
		return hasNodeAftAddOpr;
	}

	public void setHasNodeAftAddOpr(boolean hasNodeAftAddOpr) {
		this.hasNodeAftAddOpr = hasNodeAftAddOpr;
	}

	public boolean isHasLogViewSco() {
		return hasLogViewSco;
	}

	public void setHasLogViewSco(boolean hasLogViewSco) {
		this.hasLogViewSco = hasLogViewSco;
	}

	public boolean isHasNodeForFie() {
		return hasNodeForFie;
	}

	public void setHasNodeForFie(boolean hasNodeForFie) {
		this.hasNodeForFie = hasNodeForFie;
	}
	//end_td19600

	public void setId(int id) {
		this.id = id;
	}
	
	public void setNodeorder(int nodeorder) {
		this.nodeorder = nodeorder;
	}

	//liuzy 20140903 E8 标题显示设置、签字意见设置、转发设置、自由流转设置
	public boolean isHasOperateTitle() {
		return hasOperateTitle;
	}

	public void setHasOperateTitle(boolean hasOperateTitle) {
		this.hasOperateTitle = hasOperateTitle;
	}

	public boolean isHasOperateSign() {
		return hasOperateSign;
	}

	public void setHasOperateSign(boolean hasOperateSign) {
		this.hasOperateSign = hasOperateSign;
	}

	public boolean isHasOperateForward() {
		return hasOperateForward;
	}

	public void setHasOperateForward(boolean hasOperateForward) {
		this.hasOperateForward = hasOperateForward;
	}

	public boolean isHasOperateFreewf() {
		return hasOperateFreewf;
	}

	public void setHasOperateFreewf(boolean hasOperateFreewf) {
		this.hasOperateFreewf = hasOperateFreewf;
	}

	public boolean isHasOperateSubwf() {
		return hasOperateSubwf;
	}

	public void setHasOperateSubwf(boolean hasOperateSubwf) {
		this.hasOperateSubwf = hasOperateSubwf;
	}

	public boolean isHasOperateException() {
		return hasOperateException;
	}

	public void setHasOperateException(boolean hasOperateException) {
		this.hasOperateException = hasOperateException;
	}

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public boolean isHasOvertimeSet() {
        return hasOvertimeSet;
    }

    public void setHasOvertimeSet(boolean hasOvertimeSet) {
        this.hasOvertimeSet = hasOvertimeSet;
    }

    public boolean isSelectNextFlow() {
        return selectNextFlow;
    }

    public void setSelectNextFlow(boolean selectNextFlow) {
        this.selectNextFlow = selectNextFlow;
    }

 	public boolean isHasSecondAuth() {
        return hasSecondAuth;
    }

    public void setHasSecondAuth(boolean hasSecondAuth) {
        this.hasSecondAuth = hasSecondAuth;
    }

    public boolean isNodeFieldCheck() {
        return nodeFieldCheck;
    }

    public void setNodeFieldCheck(boolean nodeFieldCheck) {
        this.nodeFieldCheck = nodeFieldCheck;
    }

}