package com.engine.workflowDesign.biz;

import com.cloudstore.dev.api.util.TextUtil;
import com.engine.workflow.biz.NodeFieldCheckBiz;
import com.engine.workflowDesign.entity.*;
import dm.jdbc.util.StringUtil;
import weaver.conn.RecordSet;
import weaver.general.TimeUtil;
import weaver.general.Util;
import weaver.workflow.layout.FreeWorkflowNode;
import weaver.workflow.workflow.WFManager;

import java.util.List;
import java.util.*;
public class LayoutDataBiz {

    private static String XML_ATTRIBUTE_NEW_POINTS ="newPoints";
    private static String XML_ATTRIBUTE_REMINDMSG = "remindMsg";//出口提示信息
    private static String XML_ATTRIBUTE_ISBUILDCODE = "isBuildCode";//是否生成编号
    private static String XML_ATTRIBUTE_ISREJECT = "isreject";//是否可退回
    private static String XML_ATTRIBUTE_HASROLE = "hasRole";//出口是否有附加规则
    private static String XML_ATTRIBUTE_HASCONDITION = "hasCondition"; // 出口是否有条件
    //startTD19600
    private static String XML_ATTRIBUTE_HASNODEPRO = "hasNodePro";//节点是否有节点属性
    private static String XML_ATTRIBUTE_HASCUSRIGKEY = "hasCusRigKey";//节点是否有自定义右键按钮
    private static String XML_ATTRIBUTE_HASNODEBEFADDOPR = "hasNodeBefAddOpr";//节点是否有节点前附加操作
    private static String XML_ATTRIBUTE_HASNODEAFTADDOPR = "hasNodeAftAddOpr";//节点是否有节点后附加操作
    private static String XML_ATTRIBUTE_HASLOGVIEWSCO = "hasLogViewSco";//节点是否有日志查看范围
    private static String XML_ATTRIBUTE_HASNODEFORFIE ="hasNodeForFie";//节点是否有节点表单字段
    //endTd19600
    //liuzy 20140903 E8 标题显示设置、签字意见设置、转发设置、自由流转设置
    private static String XML_ATTRIBUTE_HASOPERATETITLE="hasOperateTitle";
    private static String XML_ATTRIBUTE_HASOPERATESIGN="hasOperateSign";
    private static String XML_ATTRIBUTE_HASOPERATEFORWARD="hasOperateForward";
    private static String XML_ATTRIBUTE_HASOPERATEFREEWF="hasOperateFreewf";
    private static String XML_ATTRIBUTE_HASOPERATESUBWF="hasOperateSubwf";
    private static String XML_ATTRIBUTE_HASOPERATEEXCEPTION="hasOperateException";
    private static String XML_ATTRIBUTE_HASSELECTNEXTFLOW = "selectNextFlow";
    private static String XML_ATTRIBUTE_HASSOVERTIMESET = "overtimeSet";
    private static String XML_ATTRIBUTE_NODEFIELDCHECK = "nodeFieldCheck";

    private static String XML_ATTRIBUTE_HASSECONDAUTH = "hasSecondAuth";        //是否设置了二次认证

    private static String XML_ATTRIBUTE_ISMUSTPASS = "ismustpass";//分叉是否必须能过 1：是，0或其它：否

    private static String XML_ATTRIBUTE_STARTDIRECTION ="startDirection"; //出口起点方向
    private static String XML_ATTRIBUTE_ENDDIRECTION ="endDirection"; //出口结束方向

    private static int MAX_WIDTH = 1000;//屏幕宽度
    private static int NODE_SPACE = 50;//节点间隔
    private static int NODE_LEFT_PANDING = 25;//节点左侧空间

    private boolean isFromForm = false;
    private boolean backstageReadOnly = false;

    public boolean isFromForm() {
        return isFromForm;
    }

    public void setFromForm(boolean fromForm) {
        isFromForm = fromForm;
    }

    public boolean isBackstageReadOnly() {
        return backstageReadOnly;
    }

    public void setBackstageReadOnly(boolean backstageReadOnly) {
        this.backstageReadOnly = backstageReadOnly;
    }

    /**
     * 获取工作流信息
     */
    public Workflow readWorkflowFromDB(String type, String id, int requestid) {

        //判断流程是否为自由流程
        String isFree = "";
        Map<String, Map<String, Integer>> freeNodePositions = new HashMap<>();
        if (isFromForm) {
            WFManager wfManager = new WFManager();
            wfManager.setWfid(Util.getIntValue(id));
            try {
                wfManager.getWfInfo();
            } catch (Exception e) {
                e.printStackTrace();
            }
            isFree = wfManager.getIsFree();
            if( isFree.equals("1") ){
                freeNodePositions = FreeWorkflowNode.getNodePositions(id, requestid+"");
            }
        }

        Workflow result = new Workflow();
        RecordSet rs = new RecordSet();
        //取得最大的存储坐标点
        rs.executeSql("select max(drawypos) as drawypos from workflow_nodebase where id in (select nodeid from workflow_flownode where workflowid = " + id + ")");
        int maxy = 0;
        if (rs.next()) maxy = rs.getInt("drawypos");
        if (maxy == -1) maxy = 0;
        if (maxy > 0) {
            maxy += NODE_SPACE;
        }
        int cx = NODE_LEFT_PANDING;
        int cy = NODE_LEFT_PANDING + maxy;
        boolean isReverse = false;
        /**工作流基本信息*/
        rs.execute("select * from workflow_base where id=" + id);
        while (rs.next()) {
            result.setFormID(rs.getString("formid"));
            result.setIsBill(rs.getInt("isbill"));
            result.setIsCust(rs.getInt("iscust"));
            result.id = rs.getInt("id");
            result.setWorkflowName(rs.getString("workflowname"));
            result.setModifyLog("1".equals(rs.getString("ismodifylog")));
            result.setFree("1".equals(rs.getString("isFree")));
        }

        String sql = "select checktype,dataptnmode from systemset";
        rs.executeQuery(sql);
        if (rs.next()) {
            String checktype = Util.null2String(rs.getString("checktype")).trim();
            String dataptnmode = Util.null2String(rs.getString("dataptnmode")).trim();

            if (!"".equals(checktype) || !"".equals(dataptnmode)) {
                result.setSecondAuth(true);     //开启了二次认证
            }
        }

        /** 工作流分组信息 **/
        List groups = new ArrayList();

        rs.execute("select * from workflow_groupinfo where workflowid=" + id + " order by direction, y, x");
        while (rs.next()) {
            WorkflowGroup wg = new WorkflowGroup();
            wg.setId(rs.getInt("id"));
            wg.setWorkflowid(rs.getInt("workflowid"));
            wg.setGroupname(rs.getString("groupname"));
            wg.setDirection(rs.getInt("direction"));
            wg.setX(rs.getDouble("x"));
            wg.setY(rs.getDouble("y"));
            wg.setWidth(rs.getDouble("width"));
            wg.setHeight(rs.getDouble("height"));
            groups.add(wg);
        }
        result.setGroups(groups);

        /** 描述文字信息 **/
        rs.executeQuery("select * from workflow_textInfo where workflowid = ?", id);
        while (rs.next()) {
            TextInfo textInfo = new TextInfo();
            textInfo.setId(Util.getIntValue(Util.null2String(rs.getString("id"))));
            textInfo.setWorkflowId(Util.getIntValue(id));
            textInfo.setTextValue(Util.null2String(rs.getString("textvalue")));
            textInfo.setWidth(Util.null2String(rs.getString("width")));
            textInfo.setHeight(Util.null2String(rs.getString("height")));
            textInfo.setxPoint(Util.null2String(rs.getString("x")));
            textInfo.setyPoint(Util.null2String(rs.getString("y")));
            result.addHelpText(textInfo);
        }

        int changestatus = 0;
        rs.executeQuery("select * from workflow_Settings");
        if(rs.next()){
            changestatus = rs.getInt("changestatus");
        }

        /**工作流节点信息*/
        rs.executeSql("select t1.*,t2.* from workflow_nodebase t1, workflow_flownode t2 where (t1.IsFreeNode is null or t1.IsFreeNode!='1' or (t1.IsFreeNode ='1' and requestid = "+requestid+")) and t2.workflowid = " + id + " and t1.id = t2.nodeid order by t1.id");
        while (rs.next()) {
            WorkflowNode node = new WorkflowNode();
            node.id = rs.getInt("id");

            if (!isFromForm) {
                //liuzy E8节点属性拆分为标题显示设置、签字意见设置、转发设置、自由流转设置
                String nodetitle = Util.null2String(rs.getString("nodetitle"));
                if (!nodetitle.equals("")) {
                    node.setHasOperateTitle(true);
                }
                int isFormSignature = Util.getIntValue(rs.getString("isFormSignature"), 0);
                int issignmustinput = Util.getIntValue(rs.getString("issignmustinput"), 0);
                int ishideinput = Util.getIntValue(rs.getString("ishideinput"), 0);
                int ishidearea = Util.getIntValue(rs.getString("ishidearea"), 0);
                int isfeedback = Util.getIntValue(rs.getString("isfeedback"), 0);
                if (isFormSignature == 1 || issignmustinput == 1 || ishideinput == 1 || ishidearea == 1 || (changestatus == 1 && isfeedback == 1)) {
                    node.setHasOperateSign(true);
                }
                int IsPendingForward = Util.getIntValue(rs.getString("IsPendingForward"), 0);
                int IsWaitForwardOpinion = Util.getIntValue(rs.getString("IsWaitForwardOpinion"), 0);
                int IsSubmitedOpinion = Util.getIntValue(rs.getString("IsSubmitedOpinion"), 0);
                int IsSubmitForward = Util.getIntValue(rs.getString("IsSubmitForward"), 0);
                int IsBeForwardSubmit = Util.getIntValue(rs.getString("IsBeForwardSubmit"), 0);
                int IsBeForwardModify = Util.getIntValue(rs.getString("IsBeForwardModify"), 0);
                int IsBeForwardPending = Util.getIntValue(rs.getString("IsBeForwardPending"), 0);
                int IsShowPendingForward = Util.getIntValue(rs.getString("IsShowPendingForward"), 0);
                int IsShowWaitForwardOpinion = Util.getIntValue(rs.getString("IsShowWaitForwardOpinion"), 0);
                int IsShowBeForward = Util.getIntValue(rs.getString("IsShowBeForward"), 0);
                int IsShowSubmitedOpinion = Util.getIntValue(rs.getString("IsShowSubmitedOpinion"), 0);
                int IsShowSubmitForward = Util.getIntValue(rs.getString("IsShowSubmitForward"), 0);
                int IsShowBeForwardSubmit = Util.getIntValue(rs.getString("IsShowBeForwardSubmit"), 0);
                int IsShowBeForwardModify = Util.getIntValue(rs.getString("IsShowBeForwardModify"), 0);
                int IsShowBeForwardPending = Util.getIntValue(rs.getString("IsShowBeForwardPending"), 0);
                int IsAlreadyForward = Util.getIntValue(rs.getString("IsAlreadyForward"), 0); //允许已办事宜转发
                if (IsPendingForward == 1 || IsWaitForwardOpinion == 1 || IsSubmitedOpinion == 1 || IsSubmitForward == 1
                        || IsBeForwardSubmit == 1 || IsBeForwardModify == 1 || IsBeForwardPending == 1 || IsShowPendingForward == 1
                        || IsShowWaitForwardOpinion == 1 || IsShowBeForward == 1 || IsShowSubmitedOpinion == 1
                        || IsShowSubmitForward == 1 || IsShowBeForwardSubmit == 1 || IsShowBeForwardModify == 1
                        || IsShowBeForwardPending == 1 || IsAlreadyForward == 1) {
                    node.setHasOperateForward(true);
                }
                int IsFreeWorkflow = Util.getIntValue(rs.getString("IsFreeWorkflow"), 0);
                if (IsFreeWorkflow == 1) {
                    node.setHasOperateFreewf(true);
                }
                //子流程设置 liuzy
                int issubwfAllEnd = Util.getIntValue(rs.getString("issubwfAllEnd"), 0);
                int issubwfremind = Util.getIntValue(rs.getString("issubwfremind"), 0);
                int subwffreeforword = Util.getIntValue(rs.getString("subwffreeforword"), 0);
                if (issubwfAllEnd == 1 || issubwfremind == 1 || subwffreeforword == 1) {
                    node.setHasOperateSubwf(true);
                }
                //流转异常设置 liuzy
                int useExceptionHandle = Util.getIntValue(rs.getString("useExceptionHandle"), 0);
                if (useExceptionHandle == 1) {
                    node.setHasOperateException(true);
                }
                //指定流转设置
                int selectNextFlow = Util.getIntValue(rs.getString("selectNextFlow"));
                if (selectNextFlow == 1) {
                    node.setSelectNextFlow(true);
                }

                //流程二次认证设置
                int isEnableIdCheck = Util.getIntValue(rs.getString("isEnableIdCheck"), 0);     //二次身份认证
                int isEnableDtaPtn = Util.getIntValue(rs.getString("isEnableDtaPtn"), 0);       //数据保护设置
                if (isEnableIdCheck == 1 || isEnableDtaPtn == 1) {
                    node.setHasSecondAuth(true);
                }


                /** 自定义右键按钮的判断 */
                String tmpIsButtonName = "0";
                String submitName7 = "";
                String submitName8 = "";
                String submitName9 = "";
                String forwardName7 = "";
                String forwardName8 = "";
                String forwardName9 = "";
                String saveName7 = "";
                String saveName8 = "";
                String saveName9 = "";
                String rejectName7 = "";
                String rejectName8 = "";
                String rejectName9 = "";
                String forsubName7 = "";
                String forsubName8 = "";
                String forsubName9 = "";
                String ccsubName7 = "";
                String ccsubName8 = "";
                String ccsubName9 = "";
                String haswfrm = "";
                String hassmsrm = "";
                String hasnoback = "";
                String hasback = "";
                String hasfornoback = "";
                String hasforback = "";
                String hasccnoback = "";
                String hasccback = "";
                String hasovertime = "";

                String ifchangstatus = "";

                RecordSet rsName = new RecordSet();
                rsName.executeSql("select * from workflow_nodecustomrcmenu where wfid=" + id + " and nodeid=" + node.id);
                if (rsName.next()) {
                    submitName7 = Util.null2String(rsName.getString("submitName7"));
                    submitName8 = Util.null2String(rsName.getString("submitName8"));
                    submitName9 = Util.null2String(rsName.getString("submitName9"));
                    forwardName7 = Util.null2String(rsName.getString("forwardName7"));
                    forwardName8 = Util.null2String(rsName.getString("forwardName8"));
                    forwardName9 = Util.null2String(rsName.getString("forwardName9"));
                    saveName7 = Util.null2String(rsName.getString("saveName7"));
                    saveName8 = Util.null2String(rsName.getString("saveName8"));
                    saveName9 = Util.null2String(rsName.getString("saveName9"));
                    rejectName7 = Util.null2String(rsName.getString("rejectName7"));
                    rejectName8 = Util.null2String(rsName.getString("rejectName8"));
                    rejectName9 = Util.null2String(rsName.getString("rejectName9"));
                    forsubName7 = Util.null2String(rsName.getString("forsubName7"));
                    forsubName8 = Util.null2String(rsName.getString("forsubName8"));
                    forsubName9 = Util.null2String(rsName.getString("forsubName9"));
                    ccsubName7 = Util.null2String(rsName.getString("ccsubName7"));
                    ccsubName8 = Util.null2String(rsName.getString("ccsubName8"));
                    ccsubName9 = Util.null2String(rsName.getString("ccsubName9"));
                    haswfrm = Util.null2String(rsName.getString("haswfrm"));
                    hassmsrm = Util.null2String(rsName.getString("hassmsrm"));
                    hasnoback = Util.null2String(rsName.getString("hasnoback"));
                    hasback = Util.null2String(rsName.getString("hasback"));
                    hasfornoback = Util.null2String(rsName.getString("hasfornoback"));
                    hasforback = Util.null2String(rsName.getString("hasforback"));
                    hasccnoback = Util.null2String(rsName.getString("hasccnoback"));
                    hasccback = Util.null2String(rsName.getString("hasccback"));
                    hasovertime = Util.null2String(rsName.getString("hasovertime"));
                }
                if ((!"".equals(forwardName7) || !"".equals(forwardName8) || !"".equals(forwardName9) || !"".equals(saveName7) || !"".equals(saveName8) || !"".equals(saveName9) || !"".equals(rejectName7) || !"".equals(rejectName8) || !"".equals(rejectName9) || "1".equals(haswfrm) || "1".equals(hassmsrm) || "1".equals(hasnoback) || "1".equals(hasback) || "1".equals(hasfornoback) || "1".equals(hasforback) || "1".equals(hasccnoback) || "1".equals(hasccback) || "1".equals(hasovertime)) && !"".equals(ifchangstatus)) {
                    tmpIsButtonName = "1";
                } else if ((!"".equals(forwardName7) || !"".equals(forwardName8) || !"".equals(forwardName9) || !"".equals(saveName7) || !"".equals(saveName8) || !"".equals(saveName9) || !"".equals(rejectName7) || !"".equals(rejectName8) || !"".equals(rejectName9) || !"".equals(submitName7) || !"".equals(submitName8) || !"".equals(submitName9) || !"".equals(forsubName7) || !"".equals(forsubName8) || !"".equals(forsubName9) || !"".equals(ccsubName7) || !"".equals(ccsubName8) || !"".equals(ccsubName9) || "1".equals(haswfrm) || "1".equals(hassmsrm) || "1".equals(hasovertime)) && "".equals(ifchangstatus)) {
                    tmpIsButtonName = "1";
                }
                RecordSet rsname2 = new RecordSet();
                String isTriDiffWorkflow = null;
                rsname2.executeSql("select isTriDiffWorkflow from workflow_base where id=" + id);
                if (rsname2.next()) {
                    isTriDiffWorkflow = Util.null2String(rsname2.getString("isTriDiffWorkflow"));
                }
                if (!"1".equals(isTriDiffWorkflow)) {
                    isTriDiffWorkflow = "0";
                }
                String subwfSetTableName = "Workflow_SubwfSet";
                if ("1".equals(isTriDiffWorkflow)) {
                    subwfSetTableName = "Workflow_TriDiffWfDiffField";
                }
                String triSubwfName7 = null;
                String triSubwfName8 = null;
                rsname2.executeSql("select triSubwfName7,triSubwfName8 from Workflow_TriSubwfButtonName where workflowId=" + id + " and nodeId=" + node.id + " and subwfSetTableName='" + subwfSetTableName + "'");
                while (rsname2.next()) {
                    triSubwfName7 = Util.null2String(rsname2.getString("triSubwfName7"));
                    triSubwfName8 = Util.null2String(rsname2.getString("triSubwfName8"));
                    if (!triSubwfName7.equals("") || !triSubwfName8.equals("")) {
                        tmpIsButtonName = "1";
                        break;
                    }
                }
                node.setHasCusRigKey("1".equals(tmpIsButtonName) ? true : false);

                /** 节点前附加操作的判断 */
                String sql2 = "select objid from workflow_addinoperate where workflowid = " + id + " and isnode=1 and ispreadd = '1' and fieldid is not null and type <> '3'";
                String hasPreRolesIds = "";
                rsname2.executeSql(sql2);
                while (rsname2.next()) {
                    hasPreRolesIds += "," + rsname2.getString("objid");
                }
                //zzl默认显示绿色钩子，表示有节点后操作
                sql2 = "select w_nodeid from int_BrowserbaseInfo where w_fid = " + id + " and ispreoperator=1 and w_enable=1 and isdelete = 0";
                rsname2.executeSql(sql2);
                while (rsname2.next()) {
                    hasPreRolesIds += "," + rsname2.getString("w_nodeid");
                }
                //zzl-end

                sql2 = "select nodeid from workflowactionset where workflowid = " + id + " and ispreoperator = 1 and nodelinkid <= 0 ";
                rsname2.execute(sql2);
                while(rsname2.next()){
                    hasPreRolesIds += ","+rsname2.getString("nodeid");
                }

                if (hasPreRolesIds.indexOf(node.id + "") != -1) {
                    node.setHasNodeBefAddOpr(true);
                } else {
                    node.setHasNodeBefAddOpr(false);
                }

                /** 节点后附件操作的判断 */
                String sql3 = "select objid from workflow_addinoperate where workflowid = " + id + " and isnode=1 and ispreadd = '0' and type <> '3'";
                String hasRolesIds = "";
                rsname2.executeSql(sql3);
                while (rsname2.next()) {
                    hasRolesIds += "," + rsname2.getString("objid");
                }

                //zzl默认显示绿色钩子，表示有节点后操作
                sql3 = "select w_nodeid from int_BrowserbaseInfo where w_fid = " + id + " and ispreoperator=0 and w_enable=1 and isdelete = 0";
                rsname2.executeSql(sql3);
                while (rsname2.next()) {
                    hasRolesIds += "," + rsname2.getString("w_nodeid");
                }

                sql3 = "select nodeid from workflowactionset where workflowid = " + id + " and ispreoperator = 0 and nodelinkid <= 0 ";
                rsname2.execute(sql3);
                while(rsname2.next()){
                    hasRolesIds += ","+rsname2.getString("nodeid");
                }

                if (hasRolesIds.indexOf(node.id + "") != -1) {
                    node.setHasNodeAftAddOpr(true);
                } else {
                    node.setHasNodeAftAddOpr(false);
                }

                /** 超时设置 **/
                rsname2.executeQuery("select id from workflow_nodeOverTime where workflowid = ? and nodeid = ? and (requestid is null)", result.getId(), node.getId());
                if (rsname2.next()) {
                    node.setHasOvertimeSet(true);
                }

                /** 日志查看范围 */
                String tmpViewIds = Util.null2String(rs.getString("viewnodeids"));
                if (!"".equals(tmpViewIds) && tmpViewIds != null) {
                    node.setHasLogViewSco(true);
                } else {
                    node.setHasLogViewSco(false);
                }

                /** 合规校验 */
                node.setNodeFieldCheck(new NodeFieldCheckBiz().isOpenNodeFieldCheckInfo(result.getId(), node.getId()));
            }
            //endTd19600
            //处理界面添加的节点
            int x = rs.getInt("drawxpos");
            int y = rs.getInt("drawypos");
            if (x == -1 && y == -1) {
                x = cx;
                y = cy;
                //折行处理
                if (isReverse) cx -= NODE_SPACE + WorkflowNode.NODE_X_SIZE * 2;
                else cx += NODE_SPACE + WorkflowNode.NODE_X_SIZE * 2;
                if (cx > MAX_WIDTH) {
                    isReverse = true;
                    cx -= NODE_SPACE + WorkflowNode.NODE_X_SIZE * 2;
                    cy += NODE_SPACE + WorkflowNode.NODE_Y_SIZE * 2;
                }
                if (cx < NODE_LEFT_PANDING) {
                    isReverse = false;
                    cx = NODE_LEFT_PANDING;
                    cy += NODE_SPACE + WorkflowNode.NODE_Y_SIZE * 2;
                }
            } else {
                x = x - WorkflowNode.NODE_X_SIZE;
                y = y - WorkflowNode.NODE_Y_SIZE;
            }
            node.x = x;
            node.y = y;
            node.nodeType = rs.getString("nodetype");
            if(isFromForm || backstageReadOnly) {//只读进来，不用加密
                node.name = rs.getString("nodename");
            } else {
                node.name = TextUtil.toBase64ForMultilang(rs.getString("nodename"));
            }

            if (freeNodePositions.get(node.id + "") != null) {
                node.x = freeNodePositions.get(node.id + "").get("x");
                node.y = freeNodePositions.get(node.id + "").get("y");
            }

            node.setStyle(Util.null2String(rs.getString("drawstyle")));
            node.setPassnum(Util.getIntValue(rs.getString("passnum"), 0));//分支数
            node.setNodeAttribute(rs.getString("nodeattribute"));
            result.addNode(node);
        }

        /**工作流出口信息*/
        isReverse = false;

        String hasRolesIds = ""; //所有有附加规则的出口id
        if (!isFromForm) {
            rs.execute("select objid from workflow_addinoperate where workflowid = " + id + " and isnode=0 and type <> 3");
            while (rs.next()) {
                hasRolesIds += "," + rs.getString("objid");
            }
            rs.executeQuery("select nodelinkid from workflowactionset where workflowid = ? and (nodeid = 0 or nodeid is null or nodeid = -1)",id);
            while (rs.next()) {
                hasRolesIds += "," + rs.getString("nodelinkid");
            }
            rs.executeQuery("select nodelinkid from int_BrowserbaseInfo where w_fid = ? and nodelinkid <>0 and w_enable=1 and isdelete = 0",id);
            while (rs.next()) {
                hasRolesIds += "," + rs.getString("nodelinkid");
            }
        }

        RecordSet statement = new RecordSet();
        try {
            statement.executeQuery("select * from workflow_nodelink where wfrequestid is null and EXISTS(select 1 from workflow_nodebase b where workflow_nodelink.nodeid=b.id and (IsFreeNode is null or IsFreeNode !='1' or (isfreenode = '1' and requestid = "+requestid+"))) and EXISTS(select 1 from workflow_nodebase b where workflow_nodelink.destnodeid=b.id and (b.IsFreeNode is null or b.IsFreeNode !='1' or (b.IsFreeNode = '1' and b.requestid = "+requestid+"))) and workflowid = " + id + " order by linkorder, nodeid, id");

            while (statement.next()) {
                WorkflowLine line = new WorkflowLine();
                line.id = statement.getInt("id");
                line.fromNodeId = statement.getInt("nodeid");
                line.toNodeId = statement.getInt("destnodeid");

//                line.setCondition(RuleBusiness.getConditionCnGz(line.getId(), 1, user));
                line.setNodePassHour(statement.getInt("nodepasshour"));
                line.setNodePassMinute(statement.getInt("nodepassminute"));
                //设置出口是否有附加规则
                if ((","+hasRolesIds+",").indexOf("," +line.id + ",") != -1) {
                    line.setHasRole(true);
                }
                //处理界面添加的出口
                WorkflowNode node = result.findNode(line.fromNodeId);
                WorkflowNode toNode = result.findNode(line.toNodeId);
                int fp = statement.getInt("directionfrom");
                int tp = statement.getInt("directionto");

                if(node == null || toNode == null){
                    continue;
                }

                if (node.getId() == toNode.getId()) {//自己连自己的情况
                    fp = 0;
                    tp = 4;
                }

                if(fp == -1 && tp == -1) {
                    int xAbs = Math.abs(node.x - toNode.x);
                    int yAbs = Math.abs(node.y - toNode.y);
                    if(xAbs >= yAbs) {
                        if (node.x > toNode.x && node.x - toNode.x > node.getWidth()) {
                            fp = 8;
                            tp = 0;
                        } else if(node.x < toNode.x && toNode.x - node.x > node.getWidth()) {
                            fp = 0;
                            tp = 8;
                        } else {
                            fp = 0;
                            tp = 8;
                        }
                    } else {
                        if (node.y > toNode.y && node.y - toNode.y > node.getHeight()) {
                            fp = 12;
                            tp = 4;
                        } else if(node.y < toNode.y && toNode.y - node.y > node.getHeight()) {
                            fp = 4;
                            tp = 12;
                        } else {
                            fp = 0;
                            tp = 8;
                        }
                    }
                }


//                    //折行处理
//                    if ((node.x + NODE_SPACE + WorkflowNode.NODE_X_SIZE * 2) > MAX_WIDTH || (node.y > (NODE_LEFT_PANDING + maxy) && isReverse == true)) {
//                        fp = 4;
//                        tp = 12;
//                        if (isReverse) isReverse = false;
//                        else isReverse = true;
//                    } else {
//                        fp = 0;
//                        tp = 8;
//                    }
                line.fromPointId = fp;
                line.toPointId = tp;
                if (isFromForm || backstageReadOnly) {
                    line.setLineName(statement.getString("linkname"));
                    line.setRemindMsg(statement.getString("tipsinfo"));
                } else {
                    line.setLineName(TextUtil.toBase64ForMultilang(statement.getString("linkname")));
                    line.setRemindMsg(TextUtil.toBase64ForMultilang(statement.getString("tipsinfo")));
                }
                line.setIsBuildCodeString(statement.getString("isBulidCode"));
                String ismustpass = Util.null2String(statement.getString("ismustpass"));
                if (!ismustpass.equals("")) line.setIsMustpass(statement.getString("ismustpass"));//分叉是否必须通过
                String isReject = Util.null2String(statement.getString("isreject"));
                if (!isReject.equals("")) line.setIsReject(statement.getString("isreject"));//是否退回
//                int i, x, y;
//                for (i = 1; i <= 5; i++) {
//                    x = statement.getInt("x" + i);
//                    y = statement.getInt("y" + i);
//                    if (x > 0 && y > 0) {
//                        line.controlPoints.add(new Point(x, y));
//                    }
//                }

                /**添加开始点和结束点*/
//                Point point = node.getConnectionPoint(line.fromPointId);
//                line.controlPoints.add(0, point);
//                node = result.findNode(line.toNodeId);
//                point = node.getConnectionPoint(line.toPointId);
//                line.controlPoints.add(line.controlPoints.size(), point);

                line.setStartDirection(Util.getIntValue(statement.getString("startDirection"), -1)); //起点方向
                line.setEndDirection(Util.getIntValue(statement.getString("endDirection"), -1)); //结束节点方向
                line.setNewPoints(Util.null2String(statement.getString("points")));
                line.setE9Points(Util.null2String(statement.getString("e9points")));
                line.setStyle(Util.null2String(statement.getString("drawstyle")));

                if(line.getStyle().indexOf("exitX") == -1 || line.getStyle().indexOf("exitY") == -1 || line.getStyle().indexOf("entryX") == -1 || line.getStyle().indexOf("entryY") == -1) {
                    line.setStyle("");//没有线条进出口方向的为错误的style，清空style，重新生成
                }

                if ("".equals(line.getStyle())) {//没有style表示是老数据，需生成出口方向
                    /**
                     * 新老数据转换规则
                     * 老           新
                     * 0    --->   x = 0.5  y = 1
                     *
                     * 90   --->   x = 0    y = 0.5
                     *
                     * 180  --->   x = 0.5  y = 0
                     *
                     * -90  --->   x = 1   y = 0.5
                     */
                    switch (line.getStartDirection()) {
                        case 0 : {
                            line.setExitX(0.5);
                            line.setExitY(1);
                            break;
                        }
                        case 90 : {
                            line.setExitX(0);
                            line.setExitY(0.5);
                            break;
                        }
                        case 180 : {
                            line.setExitX(0.5);
                            line.setExitY(0);
                            break;
                        }
                        case -90 : {
                            line.setExitX(1);
                            line.setExitY(0.5);
                            break;
                        }
                    }
                    switch (line.getEndDirection()) {
                        case 0 : {
                            line.setEntryX(0.5);
                            line.setEntryY(1);
                            break;
                        }
                        case 90 : {
                            line.setEntryX(0);
                            line.setEntryY(0.5);
                            break;
                        }
                        case 180 : {
                            line.setEntryX(0.5);
                            line.setEntryY(0);
                            break;
                        }
                        case -90 : {
                            line.setEntryX(1);
                            line.setEntryY(0.5);
                            break;
                        }
                    }

                    //E8老数据中没有direction，需调用方法生成
                    if((line.getEndDirection() == -1 && line.getStartDirection() == -1) || result.isFree()) {
                        getDirection(line.fromPointId,false,line);
                        getDirection(line.toPointId,true, line);
                    }
                }

//                if ("1".equals(isFree)) {
//                    WorkflowNode fromNodeEntity = result.findNode(line.fromNodeId);
//                    WorkflowNode toNodeEntity = result.findNode(line.toNodeId);
//                    if (fromNodeEntity.getNodeType().equals("0") && toNodeEntity.getNodeType().equals("3")) {
//                        line.setEntryX(0.5);
//                        line.setEntryY(0);
//                        line.setExitX(0.5);
//                        line.setExitY(0);
//                    } else {
//                        line.setEntryX(0);
//                        line.setEntryY(0.5);
//                        line.setExitX(1);
//                        line.setExitY(0.5);
//                    }
//                }

                result.addLine(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //流程签出
        if (Util.null2String(type).equals("edit")) {
            this.wfCheckStatus("checkout", id, 1);
        }

        return result;
    }

    /**
     * 流程签入签出
     *
     * @param type checkout:签出;checkin:签入
     * @param id   流程ID
     */
    public static boolean wfCheckStatus(String type, String id, int userId) {
        RecordSet rs = new RecordSet();
        //签出工作流

        if (Util.null2String(type).equals("checkout")) {
             rs.executeUpdate("update workflow_base set isEdit=?,editor=?,editdate=?,edittime=?,checkOutHeartBeatTime=? where id=?",
                    "1", userId, TimeUtil.getCurrentDateString(),TimeUtil.getCurrentTimeString().substring(11), TimeUtil.getCurrentTimeString(), id);
        } else {
            if (rs.getDBType().equals("mysql") || "postgresql".equals(rs.getDBType())) {
                  rs.executeSql("update workflow_base set isEdit='0',editor=null where id=" + id);
            } else {
                  rs.executeSql("update workflow_base set isEdit='0',editor='' where id=" + id);
            }
        }
        return true;
    }

    /**
     * 获取连接点方向
     *
     */
    public boolean getDirection(int index, boolean isEntry, WorkflowLine line) {
        switch (index) {
            case 0: return getDirection(1, 0.5, isEntry, line);
            case 1: return getDirection(1, 0.75, isEntry, line);
            case 2: return getDirection(1, 1, isEntry, line);
            case 3: return getDirection(0.75, 1, isEntry, line);
            case 4: return getDirection(0.5, 1, isEntry, line);
            case 5: return getDirection(0.25, 1, isEntry, line);
            case 6: return getDirection(0, 1, isEntry, line);
            case 7: return getDirection(0, 0.75, isEntry, line);
            case 8: return getDirection(0, 0.5, isEntry, line);
            case 9: return getDirection(0, 0.25, isEntry, line);
            case 10: return getDirection(0, 0, isEntry, line);
            case 11: return getDirection(0.25, 0, isEntry, line);
            case 12: return getDirection(0.5, 0, isEntry, line);
            case 13: return getDirection(0.75, 0, isEntry, line);
            case 14: return getDirection(1, 0, isEntry, line);
            case 15: return getDirection(1, 0.25, isEntry, line);
            default: return getDirection(1, 0.5, isEntry, line);
        }
    }



    public boolean getDirection(double x, double y, boolean isEntry, WorkflowLine line) {
        int direction = -1;
        if (x == 0.5 && y == 1) {
            direction = 0;
        } else if (x == 0 && y == 0.5) {
            direction = 90;
        } else if (x == 0.5 && y == 0) {
            direction = 180;
        } else if (x == 1 && y == 0.5) {
            direction = -90;
        }
        if (isEntry) {
            line.setEntryX(x);
            line.setEntryY(y);
            line.setEndDirection(direction);
        } else {
            line.setExitX(x);
            line.setExitY(y);
            line.setStartDirection(direction);
        }
        return true;
    }


}
