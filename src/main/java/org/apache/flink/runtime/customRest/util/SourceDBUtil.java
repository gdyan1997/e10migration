package org.apache.flink.runtime.customRest.util;

/**
 * @author wujiahao
 * @date 2024/1/19 15:02
 */
public class SourceDBUtil extends com.weaver.versionupgrade.customRest.util.SourceDBUtil {
    /**
     * 判断数据库中表是否存在的sql
     * @param tablename
     * @param dbtype
     * @return
     */
    public static String getCheckTableExitSql(String tablename, String dbtype) {//表名是否存在
        if (dbtype.equals("oracle")) {
            return "select count(1)  as count from user_tables t where table_name= upper('" + tablename + "')";
        } else if (dbtype.equals("sqlserver")) {
            return "select count(1) as count from sysObjects where Id=OBJECT_ID(N'" + tablename + "') and xtype='U'";
        } else if(dbtype.equals("postgresql")){
            return  "select count(*) as count from information_schema.tables where table_schema=(SELECT current_schema())\n" +
                    " and table_type='BASE TABLE' and table_name=lower('"+tablename+"')";
        } else {
            return "SELECT COUNT(*) as count FROM information_schema.TABLES WHERE table_name = '" + tablename + "' and TABLE_SCHEMA = (select database())";
        }
    }
}
