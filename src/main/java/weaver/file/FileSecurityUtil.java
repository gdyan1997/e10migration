package weaver.file;

import java.io.File;
import java.lang.reflect.Method;

/**
 * Created by wcc on 2019/12/5.
 */
public class FileSecurityUtil {

    /**
     * 删除文件
     * 当且仅当是一个文件时才执行删除
     * @param file 需要删除的文件
     */
	static Method addFileStatus = null;
	static Method removeFileStatus = null;
    public static boolean deleteFile(File file) {
        if (file == null || !file.exists()) {
            return false;
        }
        //TODO
        //先做备份，移动到一个“回收站”
        //然后定期清理回收站即可。
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if(addFileStatus == null){
        	try{
        		Class dFAClazz = classLoader.loadClass("weaver.monitor.WeaverFileSecurityManager");
        		Object deleteFileAgent = dFAClazz.newInstance();
        		addFileStatus = dFAClazz.getMethod("SetFileUtilStatus");
        	}catch( Throwable e){

        	}
        }
        try {
			addFileStatus.invoke(null);
		} catch (Throwable e1) {
			
		}   
        
        boolean status = file.delete();
        if(removeFileStatus == null){
			try{
					Class dFAClazz = classLoader.loadClass("weaver.monitor.WeaverFileSecurityManager");
			        removeFileStatus = dFAClazz.getMethod("removeFileUtilStatus");
		    }catch( Throwable e){
		    	
		    }
		}
        try{
        	removeFileStatus.invoke(null);
        } catch (Throwable e1) {
			
		}
        return status;
    }

    /**
     * 删除文件
     * 当且仅当是一个文件时才执行删除
     * @param filePath 需要删除的文件路径
     */
    public static boolean deleteFile(String filePath) {
        if (filePath == null || "".equals(filePath)) {
            return false;
        }

        return deleteFile(new File(filePath));
    }
}
