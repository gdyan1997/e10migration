package weaver.general;


import weaver.conn.RecordSet;
import weaver.systeminfo.SystemEnv;

import java.util.*;


public class BrowserTypeListService {


    /**
     * 取得字段页面表现形式
     *
     * @param htmltype 字段页面类型
     * @return 字段页面表现形式
     */
    public static String getHTMLType(String htmltype, String e9type) {

        int languageId = 7;

        String htmltypename = "";
        if (htmltype.equals("1"))
            htmltypename = SystemEnv.getHtmlLabelName("688", languageId);
        if (htmltype.equals("2"))
            htmltypename = SystemEnv.getHtmlLabelName("689", languageId);
        if (htmltype.equals("3"))
            htmltypename = SystemEnv.getHtmlLabelName("695", languageId);
        if (htmltype.equals("4"))
            htmltypename = SystemEnv.getHtmlLabelName("691", languageId);
        if (htmltype.equals("5")) {
            htmltypename = SystemEnv.getHtmlLabelName("690", languageId);

            if ("1".equals(e9type)) {
                htmltypename += "-" + SystemEnv.getHtmlLabelName("383051", languageId);
            } else if ("2".equals(e9type)) {
                htmltypename += "-" + SystemEnv.getHtmlLabelName("127058", languageId);
            } else if ("3".equals(e9type)) {
                htmltypename += "-" + SystemEnv.getHtmlLabelName("127059", languageId);
            }

        }
        if (htmltype.equals("6"))
            htmltypename = SystemEnv.getHtmlLabelName("17616", languageId);
        if (htmltype.equals("7"))
            htmltypename = SystemEnv.getHtmlLabelName("21691", languageId);
        if (htmltype.equals("8"))
            htmltypename = SystemEnv.getHtmlLabelName("82477", languageId);
        if (htmltype.equals("9"))
            htmltypename = SystemEnv.getHtmlLabelName("125583", languageId);
        return htmltypename;
    }

    /**
     * 取得字段类型名
     *
     * @param type 字段类型
     * @return 字段类型名
     */
    public static String getFieldType(String htmltype, String type, String e9fieldid)   {
        RecordSet rs = new RecordSet();

        int languageId = 7;

        String typename = "";
        if (htmltype.equals("1")) {
            if (type.equals("1"))
                typename = SystemEnv.getHtmlLabelName("608", languageId);
            if (type.equals("2"))
                typename = SystemEnv.getHtmlLabelName("696", languageId);
            if (type.equals("3"))
                typename = SystemEnv.getHtmlLabelName("697", languageId);
            if (type.equals("4"))
                typename = SystemEnv.getHtmlLabelName("18004", languageId);
            if (type.equals("5"))
                typename = SystemEnv.getHtmlLabelName("22395", languageId);
        }
        if (htmltype.equals("2"))
            typename = SystemEnv.getHtmlLabelName("689", languageId);
        if (htmltype.equals("3")) {
            typename = getBrowserData(type);
        }
        if (htmltype.equals("4"))
            typename = SystemEnv.getHtmlLabelName("691", languageId);
        if (htmltype.equals("5")) {
            rs.executeSql("select listorder,selectname from workflow_SelectItem where isbill=1 and (cancel IS NULL OR cancel='0' OR cancel='') and fieldid=" + e9fieldid + " order by listorder,selectvalue ");
            while (rs.next()) {
                if (!typename.equals("")) typename += "　";
                typename += rs.getString("selectname");
            }
        }
        if (htmltype.equals("6")) {
            if (type.equals("1"))
                typename = SystemEnv.getHtmlLabelName("20798", languageId);
            if (type.equals("2"))
                typename = SystemEnv.getHtmlLabelName("20001", languageId);
        }

        if (htmltype.equals("7")) {
            if (type.equals("1"))
                typename = SystemEnv.getHtmlLabelName("21692", languageId);
            if (type.equals("2"))
                typename = SystemEnv.getHtmlLabelName("21693", languageId);
        }
        if (htmltype.equals("8")) {
            typename = "";
        }
        if (htmltype.equals("9")) {
            typename = SystemEnv.getHtmlLabelName(22981, languageId);
        }
        return typename;
    }

    public static String getBrowserData(String e9type)  {
        Map<String, Object> apidatas = new HashMap<String, Object>();

        int languageid = 7;


        RecordSet recordSet = new RecordSet();


        String sql = "select w.typeid as groupid,w.id as itemid,w.labelid as itemlabel,w.orderid as orderid from  workflow_browserurl w left join HtmlLabelInfo h on w.labelid=h.indexid where h.languageid=" + languageid +
                " and w.id  in (" +
                e9type + ") and w.useable = 1  ";

        String orderBy = " order by groupid,orderid desc ";

        recordSet.executeQuery(sql + orderBy);


        if (recordSet.next()) {

            return SystemEnv.getHtmlLabelName(recordSet.getInt("itemlabel"), languageid);
        }


        return e9type;
    }

}
