package weaver.rule;

public class RuleCheckUtil {


    /**
     * 获取所table信息的sql
     * @param dbtype
     * @return
     */
    public static String getAllTableColumns(String dbtype){
        if ("oracle".equalsIgnoreCase(dbtype)) {
            return "select TABLE_NAME as tablename，COLUMN_NAME as fieldname from  USER_TAB_COLUMNS ";
        } else if ("mysql".equalsIgnoreCase(dbtype)) {//mysql使用concat拼接字符串，这个使用的时候需要注意,  select  concat('姓名：','张三',...)支持1个或者多个参数,
            return "select table_name as tablename,column_name as fieldname from information_schema.columns  where  TABLE_SCHEMA = (select database())  ";
        } else if ("postgresql".equalsIgnoreCase(dbtype.toLowerCase())) {
            return "select table_name as  tablename,column_name as fieldname from information_schema.columns WHERE table_schema=(SELECT current_schema())  order by tablename ";
        } else {
            return " select object_name(id) as tablename,name as fieldname  from syscolumns ";
        }
    }



}
