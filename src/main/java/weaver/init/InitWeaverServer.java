package weaver.init;


import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.customRest.util.SourceDBUtil;
import idtoolutil.Constant;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import weaver.conn.RecordSet;
import weaver.general.BaseBean;
import weaver.general.Util;

import java.io.File;
import java.net.URL;

/**
 * @author ygd2020
 * @date 2024-1-18 9:10
 * Description:
 **/
public class InitWeaverServer {
    public void init(ConfigurableApplicationContext applicationContext) {

        ConfigUtil.initConfig();
        initDb(applicationContext);
        System.out.println("数据库文件配置地址=" + InitWeaverServer.databaseurl);
        initOaversion();

        Constant.initConfig();// 初始化租户 配置信息
        new BaseBean().writeLog(ConfigUtil.getConfigDir());

    }

    private void initOaversion() {

        SourceDBUtil rs = new SourceDBUtil();

        rs.executeQuery("select CVERSION   from license");
        if (rs.next()) {
            oaversion = rs.getString("CVERSION");
        }

    }

    public static void main(String[] args) {
        String rootPath = "/weaver/E9TOE10/weaver/weaver-rasp/e10Migration/conf/";
        int indexOf = rootPath.indexOf("e10Migration");
        String substring = rootPath.substring(0, indexOf);

        System.out.println(substring);
    }


    public static String databaseurl = null;
    public static String oawebinfopath = null;
    public static String oaversion = "";

    private void initDb(ConfigurableApplicationContext applicationContext) {


        String confFileName = "weaver.properties";
        String propertiespath = ConfigUtil.getConfigDir() + confFileName;
        File file = new File(propertiespath);

        new BaseBean().writeLog("判断项目下是有weaver.properties 文件 propertiespath=" + propertiespath);
        System.out.println("判断项目下是有weaver.properties 文件 propertiespath=" + propertiespath);


        //判断项目下是有weaver.properties 文件
        if (file.exists()) {
            databaseurl = propertiespath;
            System.out.println("databaseurl=" + databaseurl);
            new BaseBean().writeLog("databaseurl=" + databaseurl);
            return;
        }

        //读取配置
        BaseBean baseBean = new BaseBean();
        String url = Util.null2String(baseBean.getPropValue("database", "url"), "").trim();
        if (!url.equals("")) {
            System.out.println("url=" + url);
            File file_tmp = new File(url);
            if (file_tmp.exists()) {
                databaseurl = url;
                System.out.println("读取配置文件 databaseurl=" + url);
                new BaseBean().writeLog("读取配置文件 databaseurl=" + url);

                return;
            } else {
                System.out.println("配置的文件地址不正确databaseurl=" + url);
                new BaseBean().writeLog("配置的文件地址不正确databaseurl=" + url);
            }
        }


        String rootPath = ConfigUtil.getConfigDir();
        System.out.println("配置文件路径" + rootPath);
        new BaseBean().writeLog("配置文件路径" + rootPath);

        if (rootPath.indexOf("e10Migration") > -1) {
            int indexOf = rootPath.indexOf("e10Migration");
            String substring = rootPath.substring(0, indexOf);
            String filepath = substring + "ecology" + file.separator + "WEB-INF" + file.separator + "prop" + file.separator + "weaver.properties";
            final File file1 = new File(filepath);
            if (file1.exists()) {
                oawebinfopath = substring + "ecology" + file.separator + "WEB-INF" + file.separator + "prop" + file.separator;
                databaseurl = filepath;
                System.out.println("自动获取指定路径路径 filepath= " + filepath);
                new BaseBean().writeLog("自动获取指定路径路径 filepath= " + filepath);
                return;
            }

        }

        new BaseBean().writeLog("请指定weaver.properties文件路径,再重新启动");
        System.out.println("请指定weaver.properties文件路径,再重新启动");


        applicationContext.close();
        System.exit(1);


    }
}
