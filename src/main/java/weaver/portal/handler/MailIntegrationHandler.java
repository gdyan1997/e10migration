package weaver.portal.handler;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description: 邮箱集成元素集成，这边注意E9只能选择邮箱类型，E10需要指定到具体的邮箱配置。
 **/
public class MailIntegrationHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("SELECT * FROM  MailEleSetting WHERE eid=?", data.get("id"));
            if (rs.next()) {
                String setting = rs.getString("setting");
                HashMap settingMap = JSONObject.parseObject(setting, HashMap.class);
                String eContentType = Util.null2String(settingMap.get("eContentType"));
                if ("1".equals(eContentType)) {
                    results.add(data.get("title") + "元素需要设置内容来源，邮箱类型(阿里邮箱)。请先在【E10后台-邮箱集成】中维护好相关的配置。");
                }else if("2".equals(eContentType)){
                    results.add(data.get("title") + "元素需要设置内容来源，邮箱类型(网易邮箱)。请先在【E10后台-邮箱集成】中维护好相关的配置。");
                }
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
