package weaver.portal.handler;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class MailIntegrationTXHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            results.add( data.get("title") + "元素需要设置内容来源。请先在【E10后台-邮箱集成】中维护好相关的腾讯邮箱配置。" );
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
