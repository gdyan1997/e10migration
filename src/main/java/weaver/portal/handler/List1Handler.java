package weaver.portal.handler;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class List1Handler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("select f.id,f.eid,f.REPORTID,f.searchtitle,f.morehref,f.DISORDER,m.customname,m.appid,m.modeid from formmodeelement f left JOIN mode_customsearch m on  m.id=f.reportId  where f.eid =? order by f.DISORDER", data.get("id"));
            while (rs.next()) {
                //元素信息
                String tabId = Util.null2String(rs.getString("id"));
                String reportid = Util.null2String(rs.getString("REPORTID"));
                String searchtitle = Util.formatMultiLang(rs.getString("searchtitle"));
                String morehref = Util.null2String(rs.getString("morehref"));

                //建模信息
                String customname = Util.formatMultiLang(rs.getString("customname"));//查询名称

                if("".equals(reportid)) continue;
                results.add("页签(" + searchtitle + ")选择的建模列表是:" + customname + ("".equals(morehref) ? "" : (",more页面地址：" + morehref )));
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
