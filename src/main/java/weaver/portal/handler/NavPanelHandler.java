package weaver.portal.handler;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class NavPanelHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String, Object> result) {
        LinkedList<String> results = new LinkedList<String>();
        String from_type = Util.null2String(data.get("from"));//组件来源：PC（pc端）  synergy（协同区）  Mobile（移动端）

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            //获取元素的内容来源
            if ("synergy".equals(from_type)) {
                rs.executeQuery("select * from hpElement_quickentrysetting where eid = ? order by ordernum", data.get("id"));
            } else {
                rs.executeQuery("select * from hpElement_quickentrysetting where eid = ? and usertype=3 order by ordernum", data.get("id"));
            }
            while (rs.next()) {
                String title = Util.formatMultiLang(rs.getString("title"));
                String link = rs.getString("link");
                if (!"".equals(link)) {
                    results.add(title + "-相关链接：" + link + "。");
                }
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
