package weaver.portal.handler;

import com.weaver.version.upgrade.util.sql.SQLUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class BarHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("select eid,tabid,tabtitle,sqlwhere,ordernum from hpNewsTabInfo where eid =? order by ordernum", data.get("id"));
            while (rs.next()) {
                String sqlwhere = Util.null2String(rs.getString("sqlwhere"));
                String tabtitle = Util.formatMultiLang(rs.getString("tabtitle"));

                if (!"".equals(sqlwhere)) {
                    String[] arySqlWhere = sqlwhere.split("\\^,\\^", -1);
                    if (arySqlWhere.length > 6) {
                        //数据源
                        String reportSqlDataset = Util.null2String(arySqlWhere[4].trim(),"默认数据源");
                        //sql
                        String reportFormSql = arySqlWhere[5].trim();
                        if(!"".equals(reportFormSql))
                            results.add("页签(" + tabtitle + ")原始SQL:" + reportFormSql +"，所属数据源ID：" + reportSqlDataset);

                        Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(reportFormSql);
                        // 先获取原先的值
                        String targetCode = "".equals(Util.null2String(sqlResultMap.get("targetCode")))?"":("SQL语句:"+Util.null2String(sqlResultMap.get("targetCode"))+"\r\n");
                        String transStatus = "".equals(Util.null2String(sqlResultMap.get("transStatus")))?"":("替换状态:"+Util.null2String(sqlResultMap.get("transStatus"))+"\r\n");
                        String transMsg = "".equals(Util.null2String(sqlResultMap.get("transMsg")))?"":("替换信息:"+Util.null2String(sqlResultMap.get("transMsg"))+"\r\n");

                        result.put("targetCode", Util.null2String(result.get("targetCode")) + targetCode);
                        result.put("transStatus", Util.null2String(result.get("transStatus")) + transStatus);
                        result.put("transMsg", Util.null2String(result.get("transMsg")) + transMsg);
                    }
                }
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
