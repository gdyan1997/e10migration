package weaver.portal.handler;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class MobileNavPanelHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("select * from hpElement_quickentrysetting_m where eid = ? ", data.get("id"));
            while (rs.next()) {
                String title = Util.formatMultiLang(rs.getString("title"));
                String link = rs.getString("link");
                if (!"".equals(link)) {
                    results.add(title + "-相关链接：" + link + "。");
                }
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
