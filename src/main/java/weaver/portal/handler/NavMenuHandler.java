package weaver.portal.handler;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class NavMenuHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("select a.value,a.name,menucenter.menuname from hpElementSetting a left join menucenter on menucenter.id = a.value where a.eid =? and a.name in ('menuIds') ", data.get("id"));
            if (rs.next()) {

                if ("menuIds".equals(rs.getString("name"))) {
                    String menuIds = Util.null2String(rs.getString("value"));
                    String menuname = Util.null2String(rs.getString("menuname"));
                    if (!"".equals(menuIds.trim()))
                        results.add("请按照E9的自定义菜单:" + menuname + "(" + menuIds + ") 进行设置！");
                }

            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
