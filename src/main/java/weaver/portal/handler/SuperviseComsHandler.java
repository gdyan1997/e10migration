package weaver.portal.handler;

import com.weaver.version.upgrade.util.sql.SQLUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class SuperviseComsHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        return "E10不支持督察督办元素，考虑通过列表配置！";
    }

    @Override
    public String getData() {
        return null;
    }
}
