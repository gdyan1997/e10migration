package weaver.portal.handler;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class CustomPageHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("select eid,tabid,tabtitle,sqlwhere,ordernum from hpNewsTabInfo where eid =? order by ordernum", data.get("id"));
            while (rs.next()) {
                String sqlwhere = rs.getString("sqlwhere");
                String tabtitle = Util.formatMultiLang(rs.getString("tabtitle"));
                //引用地址
                String strAddr = "";
                //More地址
                String strMore = "";
                if (!"".equals(sqlwhere)) {
                    String[] arySqlWhere = sqlwhere.split("\\^,\\^", -1);
                    if (arySqlWhere.length > 3) {
                        //设置链接地址
                        strAddr = arySqlWhere[0];
                        strMore = arySqlWhere[1];
                        if (!"".equals(strAddr)) {
                            results.add("页签(" + tabtitle + ")中引用地址:" + strAddr +
                                    ("".equals(strMore)?"":("\r\nMore地址:" + strMore)) );
                        }
                    }
                }
            }
        }

        return StringUtils.join(results,"\r\n");
    }

    @Override
    public String getData() {
        return null;
    }

}
