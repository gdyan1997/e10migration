package weaver.portal.handler;

import org.apache.commons.lang3.StringUtils;
import org.apache.flink.runtime.customRest.util.SourceDBUtil;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class AudioHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            rs.executeQuery("select value,name from hpElementSetting  where eid =? and name in ('audioSrc') ", data.get("id"));
            if (rs.next()) {

                if ("audioSrc".equals(rs.getString("name"))) {
                    String url = Util.null2String(rs.getString("value"));
                    if(!"".equals(url.trim()))
                        results.add(data.get("title") + "链接:" + url + "重新上传！");
                }

            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
