package weaver.portal.handler;

import com.weaver.version.upgrade.util.sql.SQLUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class TimelineHandler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String,Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            RecordSet rs1 = new RecordSet(false);
            rs.executeQuery("select * from hpelement_timeline where eid=? ", data.get("id"));
            while (rs.next()) {
                String tabid = rs.getString("tabid");
                String title = Util.formatMultiLang(rs.getString("title"));
                String moreurl = rs.getString("moreurl");
                //数据来源 1、手动录入 2、自定义SQL
                String datasourcetype = rs.getString("datasourcetype");
                if ("2".equals(datasourcetype)) { // 自定义SQL
                    String customSql = "select * from hpnewstabinfo where eid=? and tabid=? ";
                    rs1.executeQuery(customSql, data.get("id"), Util.getIntValue(tabid));
                    if (rs1.next()) {
                        //^,^^,^^,^^,^^,^select TOP 10 STARTDATE,lastname from hrmresource^,^1^,^^,^
                        String sqlWhere = Util.null2String(rs1.getString("sqlWhere"));
                        if (!"".equals(sqlWhere)) {
                            String[] arySqlWhere = sqlWhere.split("\\^,\\^", -1);
                            if (arySqlWhere.length > 5) {
                                String sqlContent = Util.null2String(arySqlWhere[5].trim(), "未定义");
                                results.add("页签(" + title + ")相关自定义SQL：" + sqlContent + ("".equals(moreurl) ? "" : (",more页面地址：" + moreurl + ".")));

                                Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(sqlContent);
                                // 先获取原先的值
                                String targetCode = "".equals(Util.null2String(sqlResultMap.get("targetCode")))?"":("SQL语句:"+Util.null2String(sqlResultMap.get("targetCode"))+"\r\n");
                                String transStatus = "".equals(Util.null2String(sqlResultMap.get("transStatus")))?"":("替换状态:"+Util.null2String(sqlResultMap.get("transStatus"))+"\r\n");
                                String transMsg = "".equals(Util.null2String(sqlResultMap.get("transMsg")))?"":("替换信息:"+Util.null2String(sqlResultMap.get("transMsg"))+"\r\n");

                                result.put("targetCode", Util.null2String(result.get("targetCode")) + targetCode);
                                result.put("transStatus", Util.null2String(result.get("transStatus")) + transStatus);
                                result.put("transMsg", Util.null2String(result.get("transMsg")) + transMsg);

                            }
                        }
                    }
                }
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
