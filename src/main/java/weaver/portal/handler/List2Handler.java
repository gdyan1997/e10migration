package weaver.portal.handler;

import com.weaver.version.upgrade.util.sql.SQLUtil;
import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.portal.interfaces.HpElementHandler;

import java.util.LinkedList;
import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:38
 * Description:
 **/
public class List2Handler implements HpElementHandler {

    @Override
    public String handle(Map<String, Object> data, Map<String, Object> result) {
        LinkedList<String> results = new LinkedList<String>();

        if (data != null) {
            RecordSet rs = new RecordSet(false);
            RecordSet rs1 = new RecordSet(false);
            rs.executeQuery("select EID,TABID,TITLE,TYPE from hpOutDataTabSetting where eid =? ", data.get("id"));
            while (rs.next()) {
                //元素信息
                String tabId = Util.null2String(rs.getString("TABID"));
                String title = Util.formatMultiLang(rs.getString("TITLE"));
                String type = rs.getString("TYPE");
                if ("1".equals(type)) {//	已有数据集成
                    rs1.executeQuery("select h.*,a.showname from hpOutDataSettingAddr h left join datashowset a on h.sourceid = a.id where h.eid=? and h.tabid =? ", data.get("id"), tabId);
                    if (rs1.next()) {
                        String showname = rs1.getString("showname");
                        if (!"".equals(showname)) {
                            results.add("页签(" + title + ")选择的数据类型【已有数据集成】:" + showname + "(数据来源：后端-数据展现集成),可以通过E10中配置数据SQL视图来展示");
                        }
                    }
                } else if ("2".equals(type)) {//自定义方式
                    rs1.executeQuery("select h.* from hpOutDataSettingDef h where h.eid=? and h.tabid =? ", data.get("id"), tabId);

                    if (rs1.next()) {
                        String pattern = rs1.getString("PATTERN");
                        String wsaddress = Util.null2String(rs1.getString("WSADDRESS"), "未定义");
                        String source = Util.null2String(rs1.getString("SOURCE"), "本地数据源");
                        String area = Util.null2String(rs1.getString("AREA"), "未定义");
                        if ("1".equals(pattern)) {//数据来源:数据库
                            results.add("页签(" + title + ")选择的数据类型【自定义方式-数据库】:" + source + ",数据SQL:" + area + ",可以通过E10中配置数据SQL视图后在元素中选择来展示。" +
                                    "链接地址：" + wsaddress + " 可以通过元素的事件来配置。");

                            Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(area);
                            // 先获取原先的值
                            String targetCode = "".equals(Util.null2String(sqlResultMap.get("targetCode"))) ? "" : ("SQL语句:" + Util.null2String(sqlResultMap.get("targetCode")) + "\r\n");
                            String transStatus = "".equals(Util.null2String(sqlResultMap.get("transStatus"))) ? "" : ("替换状态:" + Util.null2String(sqlResultMap.get("transStatus")) + "\r\n");
                            String transMsg = "".equals(Util.null2String(sqlResultMap.get("transMsg"))) ? "" : ("替换信息:" + Util.null2String(sqlResultMap.get("transMsg")) + "\r\n");

                            result.put("targetCode", Util.null2String(result.get("targetCode")) + targetCode);
                            result.put("transStatus", Util.null2String(result.get("transStatus")) + transStatus);
                            result.put("transMsg", Util.null2String(result.get("transMsg")) + transMsg);


                        } else if ("2".equals(pattern)) {//数据来源:WebService
                            String wsmethod = Util.null2String(rs1.getString("WSMETHOD"), "未定义");
                            String wspara = Util.null2String(rs1.getString("WSPARA"), "未定义");
                            results.add("页签(" + title + ")选择的数据类型【自定义方式-WebService】:" + area + ",方法:" + wsmethod + ",E10请通过配置ESB的方式来处理" +
                                    "，链接地址：" + wsaddress + " 可以通过元素的事件来配置。");
                        } else if ("3".equals(pattern)) {//数据来源:自定义地址
                            results.add("页签(" + title + ")选择的数据类型【自定义方式-自定义地址】:" + area +
                                    "，链接地址：" + wsaddress + "。E10暂不支持配置该类型的数据！！！");
                        }
                    }

                    rs1.executeQuery("select * from hpoutdatasettingfield where eid =?  and TABID=?  ", data.get("id"), tabId);
                    while (rs1.next()) {
                        String transql = Util.null2String(rs1.getString("TRANSQL")).trim();
                        String showfieldname = Util.null2String(rs1.getString("SHOWFIELDNAME"));
                        if ("".equals(transql)) continue;
                        results.add("页签(" + title + ")字段:" + showfieldname + ",转换方法" + transql);
                        Map<String, Object> sqlResultMap = SQLUtil.replaceSqlResultMap(transql);

                        // 先获取原先的值
                        String targetCode = "".equals(Util.null2String(sqlResultMap.get("targetCode"))) ? "" : ("转换方法-SQL语句:" + Util.null2String(sqlResultMap.get("targetCode")) + "\r\n");
                        String transStatus = "".equals(Util.null2String(sqlResultMap.get("transStatus"))) ? "" : ("转换方法-替换状态:" + Util.null2String(sqlResultMap.get("transStatus")) + "\r\n");
                        String transMsg = "".equals(Util.null2String(sqlResultMap.get("transMsg"))) ? "" : ("转换方法-替换信息:" + Util.null2String(sqlResultMap.get("transMsg")) + "\r\n");

                        result.put("targetCode", Util.null2String(result.get("targetCode")) + targetCode);
                        result.put("transStatus", Util.null2String(result.get("transStatus")) + transStatus);
                        result.put("transMsg", Util.null2String(result.get("transMsg")) + transMsg);

                    }


                }
            }
        }

        return StringUtils.join(results, "\r\n");
    }

    @Override
    public String getData() {
        return null;
    }
}
