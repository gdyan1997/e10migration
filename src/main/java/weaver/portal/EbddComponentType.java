package weaver.portal;

/**
 * E9/E10门户元素枚举类
 * E9无法对应的元素：6 未读文档 ；
 * User: wy
 * Date: 2022-6-20
 * Time: 17:05
 * Description:
 **/
public enum EbddComponentType {
    //E9/E10 RSS阅读器
    RSSReader("1","RSSReader","RSS阅读器",true, "PC"),
    //E9 新建流程 > 新建审批
    WorkflowNew("addwf","WorkflowNew","新建审批",true, "PC"),
    //E9 最新客户 > 客户列表，勾选（按类型搜索，新分配给我的客户）
    CustomerListTpl("11","CustomerListTpl","客户列表",false, "PC"),
    //E9 文档中心 > 文档列表，勾选（按条件搜索）
    DocumentListTpl("7","DocumentListTpl","文档列表",true, "PC"),
    //E9/E10 文档内容
    DocumentContent("25","DocumentContent","文档内容",false, "PC"),
    //E9 流程中心 > E10 审批列表
    WorkflowListTpl("8","WorkflowListTpl","流程列表",true, "PC"),
    //E9/E10通讯录
    AddressBook("contacts","AddressBook","通讯录",false, "PC"),
    //E9/E10最新会议
    Newest("12","Newest","最新会议",false, "PC"),
    //E9/E10我的协作
    MyCollaboration("13","MyCollaboration","我的协作",true, "PC"),
    //E9 未读文档 > 文档列表，勾选（按类型搜索，未读文档）==》改造成未读文档元素
    DocumentListTplUnRead("6","DocumentListTpl","文档列表",false, "PC"),
    //E9/E10 当日计划
    DayPlan("15","DayPlan","当日计划",false, "PC"),
    //E9 我的邮件 / E10 邮件列表
    EmailListTpl("16","EmailListTpl","邮件列表",true, "PC"),
    //E9 我的项目 / E10 项目
    ProjectListTpl("10","ProjectListTpl","项目",true, "PC"),
    //E9/E10 期刊中心
    PeriodicalCenter("18","PeriodicalCenter","期刊中心",false, "PC"),
    //E9/E10 股票元素
    Stock("19","Stock","股票",false, "PC"),
    //自定义页面
    CustomPage("29","CustomPage","自定义页面",true, "PC"),
    //音频
    Audio("audio","Audio","音频",false, "PC"),
    //E9 微博动态 / E10 日报动态
    BlogStatus("blogStatus","BlogStatusTpl","日报动态",false, "PC"),
    //E9 收藏元素 / E10 关注事项
    FollowList("favourite","FollowListTpl","关注事项",false, "PC"),
    //E9 Flash元素 / E10 视频
    Video("Flash","Video","视频",false, "PC"),
    //E9 自定义菜单 / E10 导航菜单
    NavMenu("menu","NavMenu","导航菜单",false, "PC"),
    //E9 图片元素 /E10 图片
    Picture("picture","Picture","图片",false, "PC"),
    //E9/E10 幻灯片
    Slide("Slide","Slide","幻灯片",false, "PC"),
    //E9 图表元素 / E10 柱状图、堆积主图、条形图、堆积条图...共23种
    Bar("reportForm","Bar","柱状图",true, "PC"),
    //E9 搜索元素 / E10 搜索框
    SearchGlobal("searchengine","SearchGlobal","搜索框",false, "PC"),
    //E9 视频元素 / E10 视频
    Video1("video","Video","视频",false, "PC"),
    //E9/E10天气元素
    Weather("weather","Weather","天气",false, "PC"),
    //E9/E10便签元素
    Scratchpad("scratchpad","Scratchpad","便签元素",false, "PC"),
    //E9/E10 个人数据
    PersonalData("DataCenter","PersonalData","个人数据",false, "PC"),
    //E9 日程日历/E10 日历
    Calendar("MyCalendar","Calendar","日历",false, "PC"),
    //E9/E10 集成登录
    IntegratedLogin("outterSys","IntegratedLogin","集成登录",false, "PC"),
    //E9 多图元素/E10 幻灯片
    Slide1("imgSlide","Slide","幻灯片",false, "PC"),
    //E9 图片元素/E10幻灯片
    Slide2("picture","Slide","幻灯片",false, "PC"),
    //E9快捷入口/E10导航面板
    NavPanel("quickEntry", "NavPanel", "导航面板",  false, "PC"),
    //E9建模查询中心/E10列表
    List1("FormModeCustomSearch", "List", "列表",  true, "PC"),
    //E9外部数据元素/E10列表
    List2("OutData", "List", "列表",  true, "PC"),
    //E9会议日历/E10日历
    Calendar1("MeetingCalendar", "Calendar", "日历",  false, "PC"),
    //E9绩效元素/E10考核列表
    AssessmentListTpl("Custom_1560394820001", "AssessmentListTpl", "考核列表",  true, "PC"),
    //E9 公告元素/E10 公告
    Bulletin("newNotice","Bulletin","公告",false, "PC"),
    //E9 我的任务/E10 任务列表
    TaskListTpl("processTask","TaskListTpl","任务列表",false, "PC"),
    //E9/E10 人员看板
    StaffPanel("Custom_StaffKanban","StaffPanel","人员看板",false, "PC"),
    //E9 腾讯邮箱/E10 邮箱集成
    MailIntegrationTX("Custom_1551072213608","MailIntegration","邮箱集成",false, "PC"),
    //E9 CoreMail邮箱/E10 邮箱集成
    MailIntegrationCM("Custom_CoreMailNew","MailIntegration","邮箱集成",false, "PC"),
    //E9 邮箱集成/E10 邮箱集成
    MailIntegration("Custom_1551082223609","MailIntegration","邮箱集成",false, "PC"),
    //E9 督察督办元素——本身是多tab组件，升级E10直接作为单组件/E10 审批督办
    //E9 督察督办元素/E10 列表元素
    SuperviseComs("Custom_GovernElement","List","审批督办",true, "PC"),
    //E9 任务元素/E10 任务列表
    TaskListTpl1("Custom_1560394820000","TaskListTpl","任务列表",true, "PC"),
    //E9 报告元素/E10 计划报告
    PlanReport("Custom_1560394820002","PlanReport","计划报告",true, "PC"),
    //E9 个人报账信息元素/E10 个人报账信息
    ExpenseAssistant("Custom_ExpenseAssistantElement","ExpenseAssistant","个人报账信息",true, "PC"),
    //E9 公文中心/E10 公文列表
    OdocCenter("odocCenter","OdocCenter","公文列表",true, "PC"),
    //E9/E10 排行榜
    RankingList("RankingList","RankingList","排行榜",true, "PC"),
    //E9/E10 时间轴
    Timeline("timeLine","Timeline","时间轴",true, "PC"),
    //E9/E10 统一认证中心
    UnifiedCertificationCenter("Custom_1551082223608","UnifiedCertificationCenter","统一认证中心",false, "PC"),
    //E9/E10 企业信息集成元素
    EnterpriseInfo("Custom_EnterpriseElement","EnterpriseInfo","统一认证中心",true, "PC"),


    //=============================================移动元素=============================================
    //知识
    MobileDocumentListTpl("7","DocumentListTpl","文档列表",true, "MB"),
    //流程中心
    MobileWorkflowListTpl("8", "WorkflowListTpl", "审批列表",  true, "MB"),
    //E9图片元素 E10幻灯片
    MobileSlide("mobilePicture", "Slide", "幻灯片",  false, "MB"),
    //E9快捷入口元素  E10导航面板元素
    MobileNavPenal("mobileQuickEntry", "NavPanel", "导航面板",  false, "MB"),
    //E9 图表元素 / E10 柱状图、堆积主图、条形图、堆积条图...共23种
    MobileBar("reportForm","Bar","柱状图",true, "MB"),
    //E9/E10 排行榜
    MobileRankingList("RankingList","RankingList","排行榜",true, "MB"),
    //E9/E10 时间轴
    MobileTimeline("timeLine","Timeline","时间轴",true, "MB"),
    MobileCustomPage("29","CustomPage","自定义页面",true, "MB")
    ;




    //ebaseid e9的hpBaseElement元素表的id
    String id;
    //E10的ebdd_component元素表的type
    String type;
    //E10的ebdd_component元素表的name
    String name;
    //E9所属模块
    String fromModule;
    //可变tab
    boolean tabable;

    EbddComponentType(String id, String type, String name, boolean tabable, String fromModule) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.tabable = tabable;
        this.fromModule = fromModule;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean getTabable() {
        return tabable;
    }

    public String getFromModule() {
        return fromModule;
    }

    public static EbddComponentType getComponentById(String ebaseid) {
        for (EbddComponentType value : EbddComponentType.values()) {
            if (ebaseid.equals(value.getId()) && "PC".equals(value.getFromModule()))
                return value;
        }
        return null;

    }
    public static EbddComponentType getComponentById4mobile(String ebaseid) {
        for (EbddComponentType value : EbddComponentType.values()) {
            if (ebaseid.equals(value.getId()) && "MB".equals(value.getFromModule()))
                return value;
        }
        return null;
    }
}
