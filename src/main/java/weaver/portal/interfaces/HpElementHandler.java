package weaver.portal.interfaces;

import java.util.Map;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:30
 * Description: 处理元素的接口
 **/
public interface HpElementHandler {
    String handle(Map<String ,Object> data, Map<String,Object> result);
    String getData();
}
