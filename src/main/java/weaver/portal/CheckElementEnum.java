package weaver.portal;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 13:40
 * Description:
 **/
public enum CheckElementEnum {

    //自定义页面
    CustomPage("29","CustomPage","自定义页面","需要确认并重新设置【链接地址】","weaver.portal.handler.CustomPageHandler"),
    //音频
    Audio("audio","Audio","音频元素","需要重新设置【音频来源】上传文件","weaver.portal.handler.AudioHandler"),
    //E9 自定义菜单 / E10 导航菜单
    NavMenu("menu","NavMenu","自定义菜单","E10没有自定义菜单设置，这边需要手动添加【导航项】，并且设置菜单的【事件动作】","weaver.portal.handler.NavMenuHandler"),
    //E9 图表元素 / E10 柱状图、堆积主图、条形图、堆积条图...共23种
    Bar("reportForm","Bar","图表元素","需要检查生成在E10数仓的SQL视图中的设置以及元素上【维度】【指标】【数据过滤】等","weaver.portal.handler.BarHandler"),
    //E9建模查询中心/E10列表
    List1("FormModeCustomSearch", "List", "建模查询中心", "E9是配置的是建模查询列表，E10是EB表单，需要检查【显示字段】【数据过滤】【排序字段】等", "weaver.portal.handler.List1Handler"),
    //E9外部数据元素/E10列表
    List2("OutData", "List", "外部数据元素", "E9数据来源浏览框、自定义SQL，E10目前需要通过配置数仓SQL视图来实现，并且使用列表元素作做展示。", "weaver.portal.handler.List2Handler"),
    //E9 腾讯邮箱/E10 邮箱集成
    MailIntegrationTX("Custom_1551072213608","MailIntegration","腾讯邮箱","E10邮箱集成数据来源【后台-邮箱集成】需要手动先维护好腾讯邮箱，然后在元素中【内容来源】选择对应的邮箱集成名称","weaver.portal.handler.MailIntegrationTXHandler"),
    //E9 CoreMail邮箱/E10 邮箱集成
    MailIntegrationCM("Custom_CoreMailNew","MailIntegration","CoreMail邮箱(E9)","E10邮箱集成数据来源【后台-邮箱集成】需要手动先维护好CoreMail邮箱，然后在元素中【内容来源】选择对应的邮箱集成名称","weaver.portal.handler.MailIntegrationCMHandler"),
    //E9 邮箱集成/E10 邮箱集成
    MailIntegration("Custom_1551082223609","MailIntegration","邮箱集成","E10邮箱集成数据来源【后台-邮箱集成】需要手动先维护好，然后在元素中【内容来源】选择对应的邮箱集成名称","weaver.portal.handler.MailIntegrationHandler"),
    //E9快捷入口/E10导航面板
    NavPanel("quickEntry", "NavPanel", "快捷入口", "需要设置【菜单设置-事件配置】中的URL", "weaver.portal.handler.NavPanelHandler"),
    //E9/E10 排行榜
    RankingList("RankingList","RankingList","排行榜","自定义SQL模式下：在数仓中维护SQL视图，并配置元素【数据源】【排行对象】【排行数据】","weaver.portal.handler.RankingListHandler"),
    //E9/E10 时间轴
    Timeline("timeLine","Timeline","时间轴元素","自定义SQL模式下：在数仓中维护SQL视图，并配置元素【数据源】【坐标】【内容】","weaver.portal.handler.TimelineHandler"),
    SuperviseComs("Custom_GovernElement","List","审批督办","E10督查督办已经转成EB应用，并且没有督察督办元素了，可以通过列表元素来配置显示。","weaver.portal.handler.SuperviseComsHandler"),;

    CheckElementEnum(String id, String type, String name, String description, String dataHandler) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.dataHandler = dataHandler;
    }

    //ebaseid e9的hpBaseElement元素表的id
    String id;
    //E10的ebdd_component元素表的type
    String type;
    //E10的ebdd_component元素表的name
    String name;
    //手动调整描述
    String description;
    //获取需要设置的数据
    String dataHandler;
    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDataHandler() {
        return dataHandler;
    }

    public static CheckElementEnum getExist(String id) {
        for (CheckElementEnum checkElementEnum : CheckElementEnum.values()) {
            if (checkElementEnum.getId().equals(id)) {
                return checkElementEnum;
            }
        }
        return null;
    }
}
