package weaver.portal;

import weaver.portal.interfaces.HpElementHandler;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:49
 * Description:
 **/
public class HpElementFactory {
    private HpElementHandler hpElementHandler;
    public HpElementFactory(CheckElementEnum checkElementEnum) {
        try {
            if ("".equals(checkElementEnum.getDataHandler()))
                hpElementHandler = null;
            Class<HpElementHandler> hpComponentHandlerClass = (Class<HpElementHandler>) Class.forName(checkElementEnum.getDataHandler());
            hpElementHandler = hpComponentHandlerClass.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public HpElementHandler getHpElementHandler() {
        return hpElementHandler;
    }
}
