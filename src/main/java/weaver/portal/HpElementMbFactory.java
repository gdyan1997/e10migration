package weaver.portal;

import weaver.portal.interfaces.HpElementHandler;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 16:49
 * Description:
 **/
public class HpElementMbFactory {
    private HpElementHandler hpElementHandler;
    public HpElementMbFactory(CheckElementMbEnum checkElementMbEnum) {
        try {
            if ("".equals(checkElementMbEnum.getDataHandler()))
                hpElementHandler = null;
            Class<HpElementHandler> hpComponentHandlerClass = (Class<HpElementHandler>) Class.forName(checkElementMbEnum.getDataHandler());
            hpElementHandler = hpComponentHandlerClass.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public HpElementHandler getHpElementHandler() {
        return hpElementHandler;
    }
}
