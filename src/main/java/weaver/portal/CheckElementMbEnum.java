package weaver.portal;

/**
 * User: wy
 * Date: 2024/3/25
 * Time: 13:40
 * Description:
 **/
public enum CheckElementMbEnum {

    //E9快捷入口/E10导航面板
    MobileNavPenal("mobileQuickEntry", "NavPanel", "快捷入口", "需要设置【菜单设置-事件配置】中的URL", "weaver.portal.handler.MobileNavPanelHandler"),
    //E9/E10 排行榜
    MobileRankingList("RankingList","RankingList","排行榜","自定义SQL模式下：在数仓中维护SQL视图，并配置元素【数据源】【排行对象】【排行数据】","weaver.portal.handler.RankingListHandler"),
    //E9/E10 时间轴
    MobileTimeline("timeLine","Timeline","时间轴元素","自定义SQL模式下：在数仓中维护SQL视图，并配置元素【数据源】【坐标】【内容】","weaver.portal.handler.TimelineHandler"),//自定义页面
    MobileCustomPage("29","CustomPage","自定义页面","需要确认并重新设置【链接地址】","weaver.portal.handler.CustomPageHandler"),
    //E9 图表元素 / E10 柱状图、堆积主图、条形图、堆积条图...共23种
    MobileBar("reportForm","Bar","图表元素","需要检查生成在E10数仓的SQL视图中的设置以及元素上【维度】【指标】【数据过滤】等","weaver.portal.handler.BarHandler"),
    ;

    CheckElementMbEnum(String id, String type, String name, String description, String dataHandler) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.dataHandler = dataHandler;
    }

    //ebaseid e9的hpBaseElement元素表的id
    String id;
    //E10的ebdd_component元素表的type
    String type;
    //E10的ebdd_component元素表的name
    String name;
    //手动调整描述
    String description;
    //获取需要设置的数据
    String dataHandler;
    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDataHandler() {
        return dataHandler;
    }

    public static CheckElementMbEnum getExist(String id) {
        for (CheckElementMbEnum checkElementEnum : CheckElementMbEnum.values()) {
            if (checkElementEnum.getId().equals(id)) {
                return checkElementEnum;
            }
        }
        return null;
    }
}
