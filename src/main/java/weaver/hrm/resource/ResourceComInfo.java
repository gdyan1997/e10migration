package weaver.hrm.resource;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ygd2020
 * @date 2024-1-18 9:16
 * Description:
 **/
public class ResourceComInfo {


    public  static  volatile Map<String,String> resourceMap=null;



    public String getLastname(String userid) {
        if (StringUtils.isBlank(userid)) {
            return "";
        }

        if (resourceMap==null){
            synchronized (ResourceComInfo.class){
                if (resourceMap==null){

               Map<String,String> resourceMap_tmp=new HashMap<>();

                RecordSet rs = new RecordSet();
                rs.executeQuery("\n" +
                        "select id,lastname\n" +
                        "from hrmresource\n" +
                        "union all\n" +
                        "select id,lastname\n" +
                        "from hrmresourcemanager" );
                while (rs.next()) {
                    resourceMap_tmp.put(rs.getString("id"),rs.getString("lastname"));
                }
                resourceMap=resourceMap_tmp;
            }
            }
        }
        return Util.null2String(resourceMap.get(userid));
    }

}
