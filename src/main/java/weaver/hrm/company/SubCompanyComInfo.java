package weaver.hrm.company;

import org.apache.commons.lang3.StringUtils;
import weaver.conn.RecordSet;
import weaver.general.Util;
import weaver.hrm.resource.ResourceComInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ygd2020
 * @date 2024-1-18 9:24
 * Description:
 **/
public class SubCompanyComInfo {


    public  static  volatile Map<String,String> subCompanyComMap=null;

    public String getSubCompanyname(String id) {

        if (StringUtils.isBlank(id)) {
            return "";
        }

        if (subCompanyComMap==null){
            synchronized (SubCompanyComInfo.class){
                if (subCompanyComMap==null){
                    Map<String,String> subCompanyComMap_tmp=new HashMap<>();


                    RecordSet recordSet = new RecordSet();

                    recordSet.executeQuery("select id,subcompanyname\n" +
                            "from hrmsubcompany\n" +
                            "union all\n" +
                            "select id,subcompanyname\n" +
                            "from hrmsubcompanyvirtual\n");

                    while (recordSet.next()) {
                        subCompanyComMap_tmp.put(recordSet.getString("id"), Util.processBody(recordSet.getString("subcompanyname")));

                    }
                    subCompanyComMap=subCompanyComMap_tmp;
                }
            }}


        return Util.null2String(subCompanyComMap.get(id) );
    }
}
