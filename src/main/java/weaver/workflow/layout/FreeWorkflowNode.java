package weaver.workflow.layout;

import weaver.conn.RecordSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自由流程节点信息管理
 * @author wanglu
 * @date 2014-09-10
 */
public class FreeWorkflowNode {
	private final static int yInit = 80;		//初始位置y
	private final static int xInit = 100; 		//初始位置x
	private final static int width = 120;		//宽度
	private final static int height = 80;		//高度
	private final static int xStep = 160;		//x轴每次增加
	private final static int yStep = 160; 		//y轴每次增加
	private final static int rowCapacity = 5;	//每行存放几个节点
	
	/**
	 * 获取自由流程全部节点的位置
	 * @param workflowId 流程编号
	 * @param requestId 请求编号 
	 * @return
	 */
	public static Map<String, Map<String, Integer>> getNodePositions(String workflowId, String requestId){
		List<String> orderedNodes = getOrderedNodes(workflowId, requestId);
		Map<String, Map<String, Integer>> points = getNodePoints(orderedNodes);
		
		Map<String, Map<String, Integer>> positions = new HashMap<String,Map<String,Integer>>();
		for(int i = 0; i < orderedNodes.size(); i++){
			String nodeId = orderedNodes.get(i);
			Map<String, Integer> position = new HashMap<String, Integer>();
			
			position.put("width", width);
			position.put("height", height);
			position.put("x", xInit + xStep * points.get(nodeId).get("x"));
			position.put("y", yInit + yStep * points.get(nodeId).get("y"));
			
			positions.put(orderedNodes.get(i), position);
		}
		return positions;
	}
	
	/**
	 * 自由流程信息保存后，批量更新节点出口方向
	 * @param workflowId 流程编号
	 * @param requestId 请求编号 
	 */
	public static void updateNodeLinkDirection(String workflowId, String requestId){
		List<String> orderedNodes = getOrderedNodes(workflowId, requestId);
		Map<String, Map<String, Integer>> points = getNodePoints(orderedNodes);
		
		//如果节点的数量小于三个，说明只有创建和归档节点，因为这两个节点是所有请求的公共节点
		//所以不需要更新它们的位置信息，直接返回即可 
		if( orderedNodes.size() < 3 ){
			return;
		}
		
		RecordSet rs = new RecordSet();
		for(int i = 0; i < orderedNodes.size() - 1; i++){
			String from = orderedNodes.get(i);
			String to = orderedNodes.get(i + 1 );
			
			Map<String, Integer> fromNodePoint = points.get(from);
			Map<String, Integer> toNodePoint = points.get(to);
			/*
			 * 分为三种情况进行计算
			 * 两个节点在同x轴；
			 * 两个节点在同y轴；
			 * 两个节点在不同x轴和y轴；
			 */
			int out = 0; //出向
			int in = 0;	//入向
			if( fromNodePoint.get("x").equals(toNodePoint.get("x")) ){
				int yDiff = toNodePoint.get("y") - fromNodePoint.get("y");
				if( yDiff > 0 ){
					out = 0;
					in = 180;
				}else if(yDiff < 0 ){
					out = 180;
					in = 0;
				}
			}else if(fromNodePoint.get("y").equals(toNodePoint.get("y"))){
				int xDiff = toNodePoint.get("x") - fromNodePoint.get("x");
				if( xDiff > 0 ){
					out = -90;
					in = 90;
				}else if(xDiff < 0 ){
					out = 90;
					in = -90;
				}
			}else{
				//此种情况在目前自由流程的流程图绘制方法下是不存在的
			}
			
			//System.out.println("from:" + from + ",to:" + to + ",out:" + out + ",in:" + in);
			
			//将方向信息更新到数据库
			String sql = "update workflow_nodelink set startDirection=" + out + ",endDirection=" + in
				+ " where workflowid=" + workflowId + " and nodeid=" + from + " and destnodeid=" + to;
			
			//System.out.println("sql:" + sql);
			
			rs.executeSql(sql);
		}
		
		String startNodeId = orderedNodes.get(0);
		String endNodeId = orderedNodes.get(orderedNodes.size() - 1);
		
		//更新创建节点到归档节点的出口方向
		String sql = "update workflow_nodelink set startDirection=90,endDirection=0"
			+ " where workflowid=" + workflowId + " and nodeid=" + startNodeId + " and destnodeid=" + endNodeId;
		rs.executeSql(sql);
	}
	
	/**
	 * 获取自由流程全部节点的坐标，坐标体系参考屏幕像素体系
	 * @param orderedNodes
	 * @return
	 */
	private static Map<String, Map<String, Integer>> getNodePoints(List<String> orderedNodes){
		Map<String, Map<String, Integer>> points = new HashMap<String,Map<String,Integer>>();
		for(int i = 0; i < orderedNodes.size(); i++){
			Map<String, Integer> point = new HashMap<String, Integer>();
			
			int row = i / rowCapacity;
			int column = row % 2 == 0 ? (i % rowCapacity) : (rowCapacity - 1 - (i % rowCapacity));
			
			point.put("x", column);
			point.put("y", row);
			
			points.put(orderedNodes.get(i), point);
		}
		return points;
	}
	
	/**
	 * 获取按照出口顺序排好序的请求的全部节点
     * @param workflowId 流程编号
	 * @param requestId 请求编号 
	 * @return
	 */
	private static List<String> getOrderedNodes(String workflowId, String requestId){
		//查询请求全部的节点（包括创建节点、归档节点和自由节点）
		String sql = "select wf.nodeid,wf.nodetype from workflow_flownode wf,workflow_nodebase wn" 
			+ " where (wn.isfreenode is null or wn.isfreenode !='1')" 
			+ " 	and wf.nodeid = wn.id and wf.workflowid = " + workflowId
			+ " union"
			+ " select id nodeid, '-1' nodetype from workflow_nodebase where requestid = " + requestId;
		
		RecordSet rs = new RecordSet();
		rs.executeSql(sql);
		
		String startNodeId = "0";	//创建节点编号 
		String endNodeId = "0";		//归档节点节点编号
		int requestNodesCount = 0; 	//请求全部节点数量（包括创建节点、归档节点和自由节点）
		String allNodeIdStr = new String(); 	//全部节点编号拼接字符串，作为in条件
		
		while( rs.next() ){
			String nodeId = rs.getString("nodeid");
			String nodeType = rs.getString("nodetype");
			
			if( "0".equals(nodeType) ){
				startNodeId = nodeId;
			}else if( "3".equals(nodeType) ){
				endNodeId = nodeId;
			}
			
			allNodeIdStr += (nodeId + ",");
			requestNodesCount++;
		}
		
		if( allNodeIdStr.length() > 0 ){
			allNodeIdStr = allNodeIdStr.substring(0, allNodeIdStr.length() - 1);
		}
		
		//查询请求全部节点之间的出口信息
		sql = "select nodeid from_node, destnodeid to_node from workflow_nodelink"
			+ " where nodeid in (" + allNodeIdStr + ") and destnodeid  in (" + allNodeIdStr + ")";
		
		//如果请求全部节点的数量大于2，说明存在自由节点。
		//此时，需排除创建节点到归档节点之前的默认出口
		if(requestNodesCount > 2){
			sql += (" and id not in( select id from workflow_nodelink"
				+" where nodeid = "+startNodeId+" and destnodeid = "+endNodeId+")");
		}
		
		rs.executeSql(sql);
		
		List<String> orderedNodes = new ArrayList<String>(requestNodesCount);
		Map<String,String> links = new HashMap<String,String>();
		while( rs.next() ){
			links.put(rs.getString("from_node"), rs.getString("to_node"));
		}
		
		//默认放入创建节点
		orderedNodes.add( startNodeId );
		//从创建节点节点开始
		String step = startNodeId;
		//出口条件为：当前节点不为归档节点
		while(step != null && !step.equals(endNodeId) ){
			String nextNodeId = links.get( step );
			orderedNodes.add(nextNodeId);
			
			step = nextNodeId;
		}
		return orderedNodes;
	}
}
