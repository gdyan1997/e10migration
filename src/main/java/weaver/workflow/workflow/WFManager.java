package weaver.workflow.workflow;

/*
 * Created on 2006-05-18
 * Copyright (c) 2001-2006 泛微软件
 * 泛微协同商务系统，版权所有。
 *
 */
import com.google.common.base.Strings;
import weaver.conn.RecordSet;
import weaver.general.BaseBean;
import weaver.general.Util;
import weaver.hrm.company.SubCompanyComInfo;

import java.io.BufferedReader;
import java.io.Serializable;
import java.io.Writer;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Description: 流程基本信息类
 *
 * @author zjf
 * @version 1.0
 */

public class WFManager extends BaseBean implements Serializable {

    private static final long serialVersionUID = 806812148902448035L;

    private int wfid = 0;
    private int formid = 0;
    private String wfname = "";
    private String wfdes = "";
    private int typeid = 0;
    private int oldtypeid = 0;
    private String isbill = "";
    private String iscust = "";
    private int helpdocid = 0;
    // add by xhheng @20050204 for TD 1534，记录是否已经创建工作流
    private int isused = 0;
    private String isImportDetail = "0";

    public String getIsImportDetail() {
        return isImportDetail;
    }

    public void setIsImportDetail(String isImportDetail) {
        this.isImportDetail = isImportDetail;
    }

    private String action = "";
    private String isvalid = "1";
    private String needmark = "";
    // add by xhheng @ 2005/01/24 for 消息提醒 Request06
    private String messageType = "";
    // add by xwj 20051101 for td2965
    private String mailMessageType = "";

    // 微信提醒START(QC:98106)
    private String chatsType = "";
    private String chatsAlertType = "";
    private String notRemindifArchived = "";
    // 微信提醒END(QC:98106)
    private String archiveNoMsgAlert = ""; // 归档节点不需短信提醒
    private String archiveNoMailAlert = ""; // 归档节点不需邮件提醒
    // 禁止附件批量下载
    private String forbidAttDownload = "";
    // 是否跟随文档关联人赋权
    private String docRightByOperator = "";
    // add by xhheng @ 20050302 for TD 1545
    private String multiSubmit = "";
    // add by xhheng @ 20050303 for TD 1689
    private String defaultName = "";
    // add by xhheng @ 20050317 for 附件上传
    private String docCategory = "";
    private String isannexUpload = "";
    private int isOpenSignLocation = 1;
    private String annexdocCategory = "";
    private String docPath = "";
    private int subCompanyId2 = -1;
    private String IsTemplate = "";
    private int Templateid = 0;
    private String isaffirmance = "";
    private String isSaveCheckForm = ""; // 流程保存是否验证必填
    private String showUploadTab = "";
    private String isSignDoc = "";
    private String showDocTab = "";
    private String isSignWorkflow = "";
    private String showWorkflowTab = "";
    // added by pony on 2006-04-13 for TD 4109
    private int isUse = 1;// 限制重复提交功能是否启用
    private int Utype = 1;// 用户类型
    private int Ttype = 1;// 时间类型
    // added end.
    private String isremak = "";
    private String orderbytype = "";// 流程审批意见排序方式 1，倒序 2，正序
    private String isShowChart = "";// 提交流程后是否显示流程图页面
    private String showChartUrl = "";
    private String isShowOnReportInput = "0"; // 1：是，0或其它：否
    // added by pony on 2006-06-15 for td4527
    private int catelogType = 0;// 附件上传目录类型 0：固定目录 1：选择目录
    private int selectedCateLog = 0;// 所选择目录的对应的id
    // added end.

    // added by pony on 2006-06-26 for td4611
    private int docRightByHrmResource = 0;// 是否按人力资源字段附权。默认为不启用。
    // added end.

    private int titleFieldId = -1;// 标题字段id
    private int keywordFieldId = -1;// 主题词字段id

    private String nosynfields;// 不需同步字段

    private String SAPSource;// SAP数据源

    private String isModifyLog = "1";// added by cyril on 2008-07-14 for

    private String isShowModifyLog = "0";//流程参与人是否可查看表单日志
    // td:8835

    private String ShowDelButtonByReject = "0";// 退回创建节点是否可删除 1:是,0或其它:否

    private String specialApproval = "0";// 是否特批件 1:是,0或其他:否
    private String Frequency = "0";// 次数
    private String Cycle = "1";// 周期

    private String isimportwf = "0";// 新建时是否可导入流程
    private String importReadOnlyField = ""; // 允许导入数据到只读字段
    private String fieldNotImport = ""; // 无需导入字段
    private String wfdocpath = "";// 流程保存为文档的路径
    private String wfdocowner = "";// 流程保存為文檔的所有者
    private String wfdocownertype = "";// TD14723
    private String wfdocownerfieldid = "";// TD14723
    // added by cyril on 2008-12-23 for td:9573
    private String isEdit = "0";// 是否正在图形化编辑
    private int editor = -1;// 当前编辑人
    private String editdate = "";// 编辑日期
    private String edittime = "";// 编辑时间
    private String isshared = "";
    private String isforwardrights = "";
    private String candelacc = "0";// 是否允许创建人删除附件
    private String isrejectremind = "0";// 退回是否提醒
    private String ischangrejectnode = "0";// 退回时是否可设置提醒节点
    private String newdocpath = ""; // 流程中文档字段新建时的默认目录
    private String issignview = "0";// 是否允许查看先关流程签字意见
    private String allowViewEmShareLog = "0";//允许查看分享流程的意见
    private String isselectrejectnode = "0";// 退回时是否可选择退回节点 td30785

    private String isneeddelacc = "0"; // 设置是否流程删除时相关的附件 qc46085
    private String smsAlertsType = "0"; // 设置短信提醒方式
    // 转发操作时是否给接收者默认赋值
    private String isForwardReceiveDef = "0";

    private String isTriDiffWorkflow = "0"; // 触发类型 add by liaodong for qc61523
    // in 2013-11-12 start
    private int dsporder = 0;
    private String isFree = "0"; // is free workflow : add by wanglu
    // 2014-09-02
    private String isoverrb = "0";//归档收回
    private String isoveriv = "0";//归档干预
    private String custompage = "";

    private String isAutoApprove = "0"; //允许自动批准
    private String isAutoCommit = "0"; //允许自动提交
    private String autoFlowRequestlogTrail = "1"; //自动处理时在签字意见留痕
    private String isAutoRemark = "1"; //自动填写户最后一次手动操作的意见
    private String isOnlyOneAutoApprove = "1"; //仅当后续节点操作者为本人一人时自动处理
    private int submittype = 0;
    private String islockworkflow = "0";//流程锁定
    private String isshowsrc;//显示意见来源
    private String limitvalue;//限制附件上传格式
    private String locknodes;//流程编辑锁定
    private String titletemplate;//标题模版
    private String cus_titletemplate;
    private String reqLevelColorJson = "";//紧急程度颜色信息
    private String freewftype;  //自由流程的模式，1：简易模式，2：高级模式
    private String titleset;// 1、自定义模式  0或空为默认模式

    private int remindscope = 0;//节点设置
    private String isSmsRemind = "0";//是否开启短信提醒
    private String isWeChatRemind = "0";//是否开启微信提醒
    private String isEmailRemind = "0";//是否开启邮件提醒
    private String isDefaultSmsRemind = "0";//短信默认提醒
    private String isDefaultWeChatRemind = "0";//微信默认提醒
    private String isDefaultEmailRemind = "0";//邮件默认提醒
    private String isArchiveNoRemind = "0";//归档节点不需提醒
    private String isCCNoRemind = "0"; //抄送人不需提醒
    private String isChoseReminder = "0";//由操作者选择提醒接收人
    private String alterRemindNodesType = "0";//允许修改提醒的节点类型：0：全部，1：选择
    private String alterRemindNodes = ""; //选择提醒的节点id，用“,”间隔
    private String defaultNameRuleType = ""; //标题设置规则
    private String hrmConditionShowType = "";//人力资源条件显示设置
    private String isOpenCommunication = "";//是否启用相关交流
    private String isShowSignCommunicate = "";//交流内容显示在流转意见中
    private String isExpendCommunicate = "";//默认展开交流
    private String docfiles = "";  //流程存为文档，需要的附件
    private String errRemindType = "";//流程错误提醒人类型
    private String errRemindObjIds=  "";//流程错误提醒人id
    private String showUploader = "0";//显示附件辅助字段，附件上传人,默认不启用
    private String showUploadTime  = "0";//显示附件辅助字段，附件上传时间,默认不启用
    private int printHelpField = 0;//附件辅助字段不打印,默认不启用
    private String mobileUrl = "";//移动端自定义页面地址
    private int hideFileSize = 0; //打印时隐藏附件大小，默认不启用
    private String prohibitBatchForward = "0";//禁止批量转发
    private int fileSecFormat = 0; //附件密级显示格式
	private String isencryptshare = ""; //是否开启流程加密
	private String encryptrange = "";  //加密分享范围
    private String stopInChart = "";  //提交后是否停留在流程图
	private String stopInForm = "";  //提交后是否停留在流转情况页面
    private String formRangeType;//自由流程对应表单选择范围 0 选择 1 排除选择
    private String formRange;


    public String getStopInChart() {
        return stopInChart;
    }

    public void setStopInChart(String stopInChart) {
        this.stopInChart = stopInChart;
    }

    public String getStopInForm() {
        return stopInForm;
    }

    public void setStopInForm(String stopInForm) {
        this.stopInForm = stopInForm;
    }

    public String getIsencryptshare() {
        return isencryptshare;
    }

    public void setIsencryptshare(String isencryptshare) {
        this.isencryptshare = isencryptshare;
    }

    public String getEncryptrange() {
        return encryptrange;
    }

    public void setEncryptrange(String encryptrange) {
        this.encryptrange = encryptrange;
    }

    public String getFormRangeType() {
        return formRangeType;
    }

    public void setFormRangeType(String formRangeType) {
        this.formRangeType = formRangeType;
    }

    public String getFormRange() {
        return formRange;
    }

    public void setFormRange(String formRange) {
        this.formRange = formRange;
    }

    public int getRemindscope() {
        return remindscope;
    }

    public void setRemindscope(int remindscope) {
        this.remindscope = remindscope;
    }

    public int getHideFileSize() {
        return hideFileSize;
    }

    public void setHideFileSize(int hideFileSize) {
        this.hideFileSize = hideFileSize;
    }

    //邮件审批设置项
    private int isEMailApprove;
    private int nodeScope;
    private String nodes;

    public int getIsEMailApprove() {
        return isEMailApprove;
    }

    public int getNodeScope() {
        return nodeScope;
    }

    public String getNodes() {
        return nodes;
    }

    public String getShowUploader() {
        return showUploader;
    }

    public void setShowUploader(String showUploader) {
        this.showUploader = showUploader;
    }

    public String getShowUploadTime() {
        return showUploadTime;
    }

    public void setShowUploadTime(String showUploadTime) {
        this.showUploadTime = showUploadTime;
    }

    public int getPrintHelpField() {
        return printHelpField;
    }

    public void setPrintHelpField(int printHelpField) {
        this.printHelpField = printHelpField;
    }

    public String getErrRemindType() {
        return errRemindType;
    }

    public void setErrRemindType(String errRemindType) {
        this.errRemindType = errRemindType;
    }

    public String getErrRemindObjIds() {
        return errRemindObjIds;
    }

    public void setErrRemindObjIds(String errRemindObjIds) {
        this.errRemindObjIds = errRemindObjIds;
    }

    public String getIsOpenCommunication() {
        return isOpenCommunication;
    }

    public void setIsOpenCommunication(String isOpenCommunication) {
        this.isOpenCommunication = isOpenCommunication;
    }

    public String getHrmConditionShowType() {
        return hrmConditionShowType;
    }

    public void setHrmConditionShowType(String hrmConditionShowType) {
        this.hrmConditionShowType = hrmConditionShowType;
    }

    public String getDefaultNameRuleType() {
        return defaultNameRuleType;
    }

    public void setDefaultNameRuleType(String defaultNameRuleType) {
        this.defaultNameRuleType = defaultNameRuleType;
    }

    public String getIsSmsRemind() {
        return isSmsRemind;
    }

    public void setIsSmsRemind(String isSmsRemind) {
        this.isSmsRemind = isSmsRemind;
    }

    public String getIsWeChatRemind() {
        return isWeChatRemind;
    }

    public void setIsWeChatRemind(String isWeChatRemind) {
        this.isWeChatRemind = isWeChatRemind;
    }

    public String getIsEmailRemind() {
        return isEmailRemind;
    }

    public void setIsEmailRemind(String isEmailRemind) {
        this.isEmailRemind = isEmailRemind;
    }

    public String getIsDefaultSmsRemind() {
        return isDefaultSmsRemind;
    }

    public void setIsDefaultSmsRemind(String isDefaultSmsRemind) {
        this.isDefaultSmsRemind = isDefaultSmsRemind;
    }

    public String getIsDefaultWeChatRemind() {
        return isDefaultWeChatRemind;
    }

    public void setIsDefaultWeChatRemind(String isDefaultWeChatRemind) {
        this.isDefaultWeChatRemind = isDefaultWeChatRemind;
    }

    public String getIsDefaultEmailRemind() {
        return isDefaultEmailRemind;
    }

    public void setIsDefaultEmailRemind(String isDefaultEmailRemind) {
        this.isDefaultEmailRemind = isDefaultEmailRemind;
    }

    public String getIsArchiveNoRemind() {
        return isArchiveNoRemind;
    }

    public void setIsArchiveNoRemind(String isArchiveNoRemind) {
        this.isArchiveNoRemind = isArchiveNoRemind;
    }

    public String getIsCCNoRemind() {
        return isCCNoRemind;
    }

    public void setIsCCNoRemind(String isCCNoRemind) {
        this.isCCNoRemind = isCCNoRemind;
    }

    public String getIsChoseReminder() {
        return isChoseReminder;
    }

    public void setIsChoseReminder(String isChoseReminder) {
        this.isChoseReminder = isChoseReminder;
    }

    public String getAlterRemindNodesType() {
        return alterRemindNodesType;
    }

    public void setAlterRemindNodesType(String alterRemindNodesType) {
        this.alterRemindNodesType = alterRemindNodesType;
    }

    public String getAlterRemindNodes() {
        return alterRemindNodes;
    }

    public void setAlterRemindNodes(String alterRemindNodes) {
        this.alterRemindNodes = alterRemindNodes;
    }

    public String getTitletemplate() {
        return titletemplate;
    }

    public void setTitletemplate(String titletemplate) {
        this.titletemplate = titletemplate;
    }

    public String getCus_titletemplate() {
        return cus_titletemplate;
    }

    public void setCus_titletemplate(String cus_titletemplate) {
        this.cus_titletemplate = cus_titletemplate;
    }

    public String getReqLevelColorJson() {
        return reqLevelColorJson;
    }

    public void setReqLevelColorJson(String reqLevelColorJson) {
        this.reqLevelColorJson = reqLevelColorJson;
    }

    public String getLimitvalue() {
        return limitvalue;
    }

    public void setLimitvalue(String limitvalue) {
        this.limitvalue = limitvalue;
    }

    public String getIsshowsrc() {
        return isshowsrc;
    }

    public void setIsshowsrc(String isshowsrc) {
        this.isshowsrc = isshowsrc;
    }

    public String getIsTriDiffWorkflow() {
        return isTriDiffWorkflow;
    }

    public void setIsTriDiffWorkflow(String isTriDiffWorkflow) {
        this.isTriDiffWorkflow = isTriDiffWorkflow;
    }

    // end

    public String getSmsAlertsType() {
        return smsAlertsType;
    }

    public void setSmsAlertsType(String smsAlertsType) {
        this.smsAlertsType = smsAlertsType;
    }

    public String getIssignview() {
        return issignview;
    }

    public void setIssignview(String issignview) {
        this.issignview = issignview;
    }

    public String getAllowViewEmShareLog() {
        return allowViewEmShareLog;
    }

    public void setAllowViewEmShareLog(String allowViewEmShareLog) {
        this.allowViewEmShareLog = allowViewEmShareLog;
    }

    public String getNewdocpath() {
        return newdocpath;
    }

    public void setNewdocpath(String newdocpath) {
        this.newdocpath = newdocpath;
    }

    public String getIsEdit() {
        return isEdit;
    }

    public void setIsEdit(String isEdit) {
        this.isEdit = isEdit;
    }

    public int getEditor() {
        return editor;
    }

    public void setEditor(int editor) {
        this.editor = editor;
    }

    public String getEditdate() {
        return editdate;
    }

    public void setEditdate(String editdate) {
        this.editdate = editdate;
    }

    public String getEdittime() {
        return edittime;
    }

    public void setEdittime(String edittime) {
        this.edittime = edittime;
    }

    public String getWfdocowner() {
        return wfdocowner;
    }

    public void setWfdocowner(String wfdocowner) {
        this.wfdocowner = wfdocowner;
    }

    /**
     * @return the isoverrb
     */
    public String getIsoverrb() {
        return isoverrb;
    }

    /**
     * @param isoverrb the isoverrb to set
     */
    public void setIsoverrb(String isoverrb) {
        this.isoverrb = isoverrb;
    }

    /**
     * @return the isoveriv
     */
    public String getIsoveriv() {
        return isoveriv;
    }

    /**
     * @param isoveriv the isoveriv to set
     */
    public void setIsoveriv(String isoveriv) {
        this.isoveriv = isoveriv;
    }


    public String getLocknodes() {
        return locknodes;
    }

    public void setLocknodes(String locknodes) {
        this.locknodes = locknodes;
    }

    public String getFreewftype() {
        return freewftype;
    }

    public void setFreewftype(String freewftype) {
        this.freewftype = freewftype;
    }


    public String getTitleset() {
        return titleset;
    }

    public void setTitleset(String titleset) {
        this.titleset = titleset;
    }

    public int getIsOpenSignLocation() {
        return isOpenSignLocation;
    }

    public void setIsOpenSignLocation(int isOpenSignLocation) {
        this.isOpenSignLocation = isOpenSignLocation;
    }

    public String getIsShowSignCommunicate() {
        return isShowSignCommunicate;
    }

    public void setIsShowSignCommunicate(String isShowSignCommunicate) {
        this.isShowSignCommunicate = isShowSignCommunicate;
    }

    public String getIsExpendCommunicate() {
        return isExpendCommunicate;
    }

    public void setIsExpendCommunicate(String isExpendCommunicate) {
        this.isExpendCommunicate = isExpendCommunicate;
    }

    public String getMobileUrl() {
        return mobileUrl;
    }

    public void setMobileUrl(String mobileUrl) {
        this.mobileUrl = mobileUrl;
    }

    public String getProhibitBatchForward() {
        return prohibitBatchForward;
    }

    public void setProhibitBatchForward(String prohibitBatchForward) {
        this.prohibitBatchForward = prohibitBatchForward;
    }

    public int getFileSecFormat() {
        return fileSecFormat;
    }

    public void setFileSecFormat(int fileSecFormat) {
        this.fileSecFormat = fileSecFormat;
    }

    public WFManager() {

    }

    /**
     * 重置参数
     */
    public void reset() {
        formid = 0;
        typeid = 0;
        wfid = 0;
        wfname = "";
        wfdes = "";
        action = "";
        isbill = "";
        iscust = "";
        helpdocid = 0;
        isvalid = "1";
        needmark = "";
        messageType = "0";
        mailMessageType = "0";// added xwj for td2965 20051101

        // 微信提醒START(QC:98106)
        chatsType = "0"; // add by fyg @ 20140319 for 微信提醒
        chatsAlertType = "0"; // add by fyg @ 20140319 for 微信提醒
        notRemindifArchived = "0"; // add by fyg @ 20140319 for 微信提醒
        // 微信提醒END(QC:98106)

        archiveNoMsgAlert = "";
        archiveNoMailAlert = "";
        forbidAttDownload = "0";
        docRightByOperator = "0";
        IsTemplate = "";
        Templateid = 0;
        catelogType = 0;
        selectedCateLog = 0;
        docRightByHrmResource = 0;
        isaffirmance = "";
        isSaveCheckForm = "";
        isremak = "";
        isShowChart = "";
        showChartUrl = "";
        orderbytype = "";
        isShowOnReportInput = "";
        isannexUpload = "";
        annexdocCategory = "";
        isModifyLog = "1";
        isShowModifyLog = "0";
        ShowDelButtonByReject = "0";

        specialApproval = "0";
        Frequency = "0";
        Cycle = "1";

        isimportwf = "0";
        importReadOnlyField = "";
        fieldNotImport = "";
        wfdocpath = "";
        wfdocowner = "";
        isEdit = "0";
        editor = -1;
        editdate = "";
        edittime = "";
        showUploadTab = "";
        isSignDoc = "";
        showDocTab = "";
        isSignWorkflow = "";
        showWorkflowTab = "";
        candelacc = "";
        isshared = "";
        isforwardrights = "";
        isrejectremind = "0";
        ischangrejectnode = "0";
        wfdocownertype = "";
        wfdocownerfieldid = "";
        newdocpath = "";
        issignview = "0";
        isselectrejectnode = "0";
        isImportDetail = "0";
        nosynfields = "";
        isneeddelacc = "0";
        SAPSource = "";
        smsAlertsType = "0";
        isTriDiffWorkflow = "0"; // add by liaodong for qc61523 in 2013-11-12
        // start
        dsporder = 0;
        isFree = "0";
        isoveriv = "0";
        isoverrb = "0";
        custompage = "";
        isAutoApprove = "0";
        isAutoCommit = "0";
        autoFlowRequestlogTrail = "1";
        isAutoRemark = "1";
        isOnlyOneAutoApprove = "1";
        submittype = 0;
        islockworkflow = "0";
        isshowsrc = "0";
        locknodes = "";
        titletemplate = "";
        cus_titletemplate = "";
        reqLevelColorJson = "";
        titleset = "0";

        isSmsRemind = "0";//是否开启短信提醒
        isWeChatRemind = "0";//是否开启微信提醒
        isEmailRemind = "0";//是否开启邮件提醒
        isDefaultSmsRemind = "0";//短信默认提醒
        isDefaultWeChatRemind = "0";//微信默认提醒
        isDefaultEmailRemind = "0";//邮件默认提醒
        isArchiveNoRemind = "0";//归档节点不需提醒
        isCCNoRemind = "0"; //抄送人不需提醒
        isChoseReminder = "0";//由操作者选择提醒接收人
        alterRemindNodesType = "0";//允许修改提醒的节点类型：0：全部，1：选择
        alterRemindNodes = ""; //选择提醒的节点id，用“,”间隔
        prohibitBatchForward = "0";
        fileSecFormat = 0;
		isencryptshare = "";
        encryptrange = "";
    }

    public int getSubmittype() {
        return submittype;
    }

    public void setSubmittype(int submittype) {
        this.submittype = submittype;
    }

    public String getNosynfields() {
        return this.nosynfields;
    }

    public void setNosynfields(String nosynfields_) {
        this.nosynfields = nosynfields_;
    }

    /**
     * 获得退回时是否可选择退回节点
     *
     * @return
     */
    public String getIsSelectrejectNode() {
        return isselectrejectnode;

    }

    /**
     * 设置退回时是否可选择退回节点
     *
     * @param isselectrejectnode
     */
    public void setIsSelectrejectNode(String isselectrejectnode) {
        this.isselectrejectnode = isselectrejectnode;
    }

    /**
     * 获得是否允许签字意见关联文档
     *
     * @return
     */
    public String getSignDoc() {
        return isSignDoc;
    }

    /**
     * 设置是否允许签字意见关联文档
     *
     * @param signDoc
     */
    public void setSignDoc(String signDoc) {
        isSignDoc = signDoc;
    }

    /**
     * 获得是否显示相关附件Tab
     *
     * @return
     */
    public String getShowUploadTab() {
        return showUploadTab;
    }

    /**
     * 设置是否显示相关附件Tab
     *
     * @param showUploadTab
     */
    public void setShowUploadTab(String showUploadTab) {
        this.showUploadTab = showUploadTab;
    }

    /**
     * 获得是否显示相关文档tab
     *
     * @return
     */
    public String getShowDocTab() {
        return showDocTab;
    }

    /**
     * 设置是否显示相关文档tab
     *
     * @param showDocTab
     */
    public void setShowDocTab(String showDocTab) {
        this.showDocTab = showDocTab;
    }

    /**
     * 获得转发权限
     *
     * @return
     */
    public String getIsforwardRights() {
        return isforwardrights;
    }

    /**
     * 设置转发权限
     *
     * @param oid
     */
    public void setIsforwardRights(String oid) {
        this.isforwardrights = oid;
    }

    public String getIsshared() {
        return isshared;
    }

    public void setIsshared(String isshared) {
        this.isshared = isshared;
    }

    /**
     * 获得是否允许签字意见关联流程
     *
     * @return
     */
    public String getSignWorkflow() {
        return isSignWorkflow;
    }

    /**
     * 设置是否允许签字意见关联流程
     *
     * @param signWorkflow
     */
    public void setSignWorkflow(String signWorkflow) {
        isSignWorkflow = signWorkflow;
    }

    /**
     * 获得是否显示相关流程Tab
     *
     * @return
     */
    public String getShowWorkflowTab() {
        return showWorkflowTab;
    }

    /**
     * 设置是否显示相关流程tab
     *
     * @param showWorkflowTab
     */
    public void setShowWorkflowTab(String showWorkflowTab) {
        this.showWorkflowTab = showWorkflowTab;
    }

    /**
     * 获得是否记录表单修改日志
     *
     * @return 0:不记录 1:记录
     */
    public String getIsModifyLog() {
        return isModifyLog;
    }

    public void setIsModifyLog(String isModifyLog) {
        this.isModifyLog = isModifyLog;
    }

    public String getIsShowModifyLog() {
        return isShowModifyLog;
    }

    public void setIsShowModifyLog(String isShowModifyLog) {
        this.isShowModifyLog = isShowModifyLog;
    }

    /**
     * 获得是否提交确认
     *
     * @return 1：提交确认
     */
    public String getIsaffirmance() {
        return isaffirmance;
    }

    /**
     * 设置是否提交确认
     *
     * @param isaffirmance
     */
    public void setIsaffirmance(String isaffirmance) {
        this.isaffirmance = isaffirmance;
    }

    /**
     * 获得流程保存是否验证必填
     *
     * @return the isSaveCheckForm
     */
    public String getIsSaveCheckForm() {
        return isSaveCheckForm;
    }

    /**
     * 设置流程保存是否验证必填
     *
     * @param isSaveCheckForm the isSaveCheckForm to set
     */
    public void setIsSaveCheckForm(String isSaveCheckForm) {
        this.isSaveCheckForm = isSaveCheckForm;
    }

    /**
     * 得到流程摸板ID
     *
     * @return 流程摸板ID
     */
    public int getTemplateid() {
        return Templateid;
    }

    /**
     * 设置流程摸板ID
     *
     * @param templateid 流程摸板ID
     */
    public void setTemplateid(int templateid) {
        Templateid = templateid;
    }

    /**
     * 得到是否是流程摸板
     *
     * @return 流程摸板ID
     */
    public String getIsTemplate() {
        return IsTemplate;
    }

    public String getIsremak() {
        return isremak;
    }

    /**
     * 得到是否显示于报表填报
     *
     * @return 是否显示于报表填报
     */
    public String getIsShowOnReportInput() {
        return isShowOnReportInput;
    }

    /**
     * 设置是否是流程摸板
     *
     * @param isTemplate 是否是流程摸板
     */
    public void setIsTemplate(String isTemplate) {
        IsTemplate = isTemplate;
    }

    /**
     * 设置提交类型
     *
     * @param action 提交类型
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * 得到流程ID
     *
     * @return 流程ID
     */
    public int getWfid() {
        return wfid;
    }

    /**
     * 设置流程摸板ID
     *
     * @param oid
     */
    public void setWfid(int oid) {
        this.wfid = oid;
    }

    public void setIsremak(String oid) {
        this.isremak = oid;
    }

    /**
     * 设置是否显示于报表填报
     *
     * @param oid
     */
    public void setIsShowOnReportInput(String oid) {
        this.isShowOnReportInput = oid;
    }

    /**
     * 得到表单ID
     *
     * @return 表单ID
     */
    public int getFormid() {
        return formid;
    }

    /**
     * 设置表单ID
     *
     * @param oid 表单ID
     */
    public void setFormid(int oid) {
        this.formid = oid;
    }

    /**
     * 得到流程类型ID
     *
     * @return 流程类型ID
     */
    public int getTypeid() {
        return typeid;
    }

    /**
     * 设置流程类型ID
     *
     * @param oid 流程类型ID
     */
    public void setTypeid(int oid) {
        this.typeid = oid;
    }

    /**
     * 设置原流程类型ID
     *
     * @param oid 流程类型ID
     */
    public void setOldTypeid(int oid) {
        this.oldtypeid = oid;
    }

    /**
     * 得到流程帮助文档ID
     *
     * @return 流程帮助文档ID
     */
    public int getHelpdocid() {
        return helpdocid;
    }

    /**
     * 设置流程帮助文档ID
     *
     * @param oid 流程帮助文档ID
     */
    public void setHelpdocid(int oid) {
        this.helpdocid = oid;
    }

    /**
     * 得到流程名称
     *
     * @return 流程名称
     */
    public String getWfname() {
        return wfname;
    }

    /**
     * 设置流程名称
     *
     * @param orderno 流程名称
     */
    public void setWfname(String orderno) {
        this.wfname = orderno;
    }

    /**
     * 得到是否是单据
     *
     * @return 否是单据
     */
    public String getIsBill() {
        return isbill;
    }

    /**
     * 设置是否是单据
     *
     * @param orderno 是否是单据0/1
     */
    public void setIsBill(String orderno) {
        this.isbill = orderno;
    }

    /**
     * 得到是否为门户工作流
     *
     * @return 是否为门户工作流（0/1）
     */
    public String getIsCust() {
        return iscust;
    }

    /**
     * 设置是否为门户工作流
     *
     * @param orderno 是否为门户工作流（0/1）
     */
    public void setIsCust(String orderno) {
        this.iscust = orderno;
    }

    /**
     * 得到流程描述
     *
     * @return 流程描述
     */
    public String getWfdes() {
        return wfdes;
    }

    /**
     * 设置流程描述
     *
     * @param orderno 流程描述
     */
    public void setWfdes(String orderno) {
        this.wfdes = orderno;
    }

    /**
     * 得到流程是否有效
     *
     * @return 流程是否有效
     */
    public String getIsValid() {
        return isvalid;
    }

    /**
     * 流程是否有效
     *
     * @param isvalid 流程是否有效
     */
    public void setIsValid(String isvalid) {
        this.isvalid = isvalid;
    }

    /**
     * 得到流程是否需要签意见
     *
     * @return 流程是否需要签意见
     */
    public String getNeedMark() {
        return needmark;
    }

    /**
     * 设置流程是否需要签意见
     *
     * @param needmark 流程是否需要签意见
     */
    public void setNeedMark(String needmark) {
        this.needmark = needmark;
    }

    /**
     * 得到流程提醒类型
     *
     * @return 流程提醒类型
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * 设置流程提醒类型据
     *
     * @param messageType 流程提醒类型
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * 得到流程邮件提醒类型据
     *
     * @return 邮件提醒类型据
     */
    public String getMailMessageType() {
        return mailMessageType;
    }

    /**
     * 设置流程邮件提醒类型据
     *
     * @param mailMessageType 流程邮件提醒类型据
     */
    public void setMailMessageType(String mailMessageType) {
        this.mailMessageType = mailMessageType;
    }

    /**
     * 得到归档节点不需短信提醒
     *
     * @return the archiveNoMsgAlert
     */
    public String getArchiveNoMsgAlert() {
        return archiveNoMsgAlert;
    }

    /**
     * 设置归档节点不需短信提醒
     *
     * @param archiveNoMsgAlert the archiveNoMsgAlert to set
     */
    public void setArchiveNoMsgAlert(String archiveNoMsgAlert) {
        this.archiveNoMsgAlert = archiveNoMsgAlert;
    }

    /**
     * 得到归档节点不需邮件提醒
     *
     * @return the archiveNoMailAlert
     */
    public String getArchiveNoMailAlert() {
        return archiveNoMailAlert;
    }

    /**
     * 设置归档节点不需邮件提醒
     *
     * @param archiveNoMailAlert the archiveNoMailAlert to set
     */
    public void setArchiveNoMailAlert(String archiveNoMailAlert) {
        this.archiveNoMailAlert = archiveNoMailAlert;
    }

    /**
     * 得到流程是否可以批量提交
     *
     * @return 流程是否可以批量提交
     */
    public String getMultiSubmit() {
        return multiSubmit;
    }

    /**
     * 设置流程是否可以批量提交
     *
     * @param multiSubmit 流程是否可以批量提交
     */
    public void setMultiSubmit(String multiSubmit) {
        this.multiSubmit = multiSubmit;
    }

    /**
     * 得到流程缺省名称
     *
     * @return 流程缺省名称
     */
    public String getDefaultName() {
        return defaultName;
    }

    /**
     * 设置流程缺省名称
     *
     * @param defaultName 流程缺省名称/1
     */
    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    /**
     * 获得退回创建节点是否可删除
     *
     * @return
     */
    public String getShowDelButtonByReject() {
        return ShowDelButtonByReject;
    }

    /**
     * 获得是否特批件
     *
     * @return
     */
    public String getSpecialApproval() {
        return specialApproval;
    }

    /**
     * 设置是否特批件
     *
     * @param specialApproval
     */
    public void setSpecialApproval(String specialApproval) {
        this.specialApproval = specialApproval;
    }

    /**
     * 设置退回创建节点是否可删除
     *
     * @param showDelButtonByReject
     */
    public void setShowDelButtonByReject(String showDelButtonByReject) {
        ShowDelButtonByReject = showDelButtonByReject;
    }

    /**
     * 获得退回是否提醒
     *
     * @return
     */
    public String getIsrejectremind() {
        return isrejectremind;
    }

    /**
     * 设置退回是否提醒
     *
     * @param isrejectremind
     */
    public void setIsrejectremind(String isrejectremind) {
        this.isrejectremind = isrejectremind;
    }

    /**
     * 获得退回时是否可设置提醒节点
     *
     * @return
     */
    public String getIschangrejectnode() {
        return ischangrejectnode;
    }

    /**
     * 设置退回时是否可设置提醒节点
     *
     * @param ischangrejectnode
     */
    public void setIschangrejectnode(String ischangrejectnode) {
        this.ischangrejectnode = ischangrejectnode;
    }

    /**
     * 获得新建时是否可导入流程
     *
     * @return
     */
    public String getIsImportwf() {
        return isimportwf;
    }

    /**
     * 设置新建时是否可导入流程
     *
     * @param isimportwf
     */
    public void setIsImportwf(String isimportwf) {
        this.isimportwf = isimportwf;
    }

    /**
     * 获得是否允许导入数据到只读字段
     *
     * @return importReadOnlyField
     */
    public String getImportReadOnlyField() {
        return importReadOnlyField;
    }

    /**
     * 设置是否允许导入数据到只读字段
     *
     * @return
     */
    public void setImportReadOnlyField(String importReadOnlyField) {
        this.importReadOnlyField = importReadOnlyField;
    }

    /**
     * 获得无需导入字段
     *
     * @return
     */
    public String getFieldNotImport() {
        return fieldNotImport;
    }

    /**
     * 设置无需导入字段
     *
     * @param
     */
    public void setFieldNotImport(String fieldNotImport) {
        this.fieldNotImport = fieldNotImport;
    }

    public void setDsporder(int dsporder) {
        this.dsporder = dsporder;
    }

    public int getDsporder() {
        return this.dsporder;
    }

    public void setIsFree(String isFree) {
        this.isFree = isFree;
    }

    public String getIsFree() {
        return this.isFree;
    }

    public void setCustompage(String custompage) {
        this.custompage = custompage;
    }

    public String getCustompage() {
        return this.custompage;
    }

    public String getIsAutoApprove() {
        return this.isAutoApprove;
    }

    public void setIsAutoApprove(String isAutoApprove) {
        this.isAutoApprove = isAutoApprove;
    }

    public String getIsAutoCommit() {
        return this.isAutoCommit;
    }

    public void setIsAutoCommit(String isAutoCommit) {
        this.isAutoCommit = isAutoCommit;
    }

    public String getIsAutoRemark() {
        return isAutoRemark;
    }

    public void setIsAutoRemark(String isAutoRemark) {
        this.isAutoRemark = isAutoRemark;
    }

    public String getAutoFlowRequestlogTrail() {
        return autoFlowRequestlogTrail;
    }

    public void setAutoFlowRequestlogTrail(String autoFlowRequestlogTrail) {
        this.autoFlowRequestlogTrail = autoFlowRequestlogTrail;
    }

    public String getIsOnlyOneAutoApprove() {
        return isOnlyOneAutoApprove;
    }

    public void setIsOnlyOneAutoApprove(String isOnlyOneAutoApprove) {
        this.isOnlyOneAutoApprove = isOnlyOneAutoApprove;
    }

    public String getIslockworkflow() {
        return islockworkflow;
    }

    public void setIslockworkflow(String islockworkflow) {
        this.islockworkflow = islockworkflow;
    }

    public String getDocfiles() {
        return docfiles;
    }

    public void setDocfiles(String docfiles) {
        this.docfiles = docfiles;
    }




    /**
     * 得到是否已经创建工作流
     *
     * @return 返回 是否已经创建工作流
     */
    public int getIsused() {
        return isused;
    }

    /**
     * 设置是否已经创建工作流
     *
     * @param isused 是否已经创建工作流
     */
    public void setIsused(int isused) {
        this.isused = isused;
    }

    /**
     * 得到流程文档目录
     *
     * @return 返回 流程文档目录
     */
    public String getDocCategory() {
        return docCategory;
    }

    /**
     * 得到是否允许签字意见上传附件
     *
     * @return 返回 是否允许签字意见上传附件
     */
    public String getIsAnnexUpload() {
        return isannexUpload;
    }

    /**
     * 得到是否允许创建人删除附件
     *
     * @return 返回 是否允许创建人删除附件
     */
    public String getCanDelAcc() {
        return candelacc;
    }

    /**
     * 得到流程签字意见附件文档目录
     *
     * @return 返回 流程签字意见附件文档目录
     */
    public String getAnnexDocCategory() {
        return annexdocCategory;
    }

    /**
     * 设置流程文档目录用
     *
     * @param docCategory 流程文档目录
     */
    public void setDocCategory(String docCategory) {
        this.docCategory = docCategory;
    }

    /**
     * 设置是否允许创建人删除附件
     *
     * @param candelacc 是否允许创建人删除附件
     */
    public void setCanDelAcc(String candelacc) {
        this.candelacc = candelacc;
    }

    /**
     * 设置是否允许签字意见上传附件
     *
     * @param IsAnnexUpload 是否允许签字意见上传附件
     */
    public void setIsAnnexUpload(String IsAnnexUpload) {
        this.isannexUpload = IsAnnexUpload;
    }

    /**
     * 设置流程签字意见附件文档目录用
     *
     * @param docCategory 流程签字意见附件文档目录
     */
    public void setAnnexDocCategory(String docCategory) {
        this.annexdocCategory = docCategory;
    }

    /**
     * 得到流程文档路径
     *
     * @return 返回 流程文档路径
     */
    public String getDocPath() {
        return docPath;
    }

    /**
     * 设置流程文档路径
     *
     * @param docPath 流程文档路径
     */
    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    /**
     * 设置分部ID
     *
     * @param id 分部ID
     */
    public void setSubCompanyId2(int id) {
        this.subCompanyId2 = id;
    }

    /**
     * 得到分部ID
     *
     * @return 分部ID
     */
    public int getSubCompanyId2() {
        return this.subCompanyId2;
    }

    /**
     * 得到是否跟随文档关联人赋权
     *
     * @return 是否跟随文档关联人赋权
     */
    public String getDocRightByOperator() {
        return docRightByOperator;
    }

    /**
     * 设置是否跟随文档关联人赋权
     *
     * @param docRightByOperator 是否跟随文档关联人赋权
     */
    public void setDocRightByOperator(String docRightByOperator) {
        this.docRightByOperator = docRightByOperator;
    }



    /**
     * 得到限制重复提交功能是否启用
     *
     * @return 限制重复提交功能是否启用判断
     */
    public int getIsUse() {
        return isUse;
    }

    /**
     * 设置限制重复提交功能是否启用
     *
     * @param isUse 限制重复提交功能是否启用
     */
    public void setIsUse(int isUse) {
        this.isUse = isUse;
    }

    /**
     * 得到时间类型
     *
     * @return 时间类型
     */
    public int getTtype() {
        return Ttype;
    }

    /**
     * 设置时间类型
     *
     * @param ttype 时间类型
     */
    public void setTtype(int ttype) {
        Ttype = ttype;
    }

    /**
     * 得到用户类型
     *
     * @return 用户类型
     */
    public int getUtype() {
        return Utype;
    }

    /**
     * 设置用户类型
     *
     * @param utype 用户类型
     */
    public void setUtype(int utype) {
        Utype = utype;
    }

    /**
     * 得到附件上传目录类型 0：固定目录 1：选择目录
     *
     * @return 附件上传目录类型 0：固定目录 1：选择目录
     */
    public int getCatelogType() {
        return catelogType;
    }

    /**
     * 设置附件上传目录类型 0：固定目录 1：选择目录
     *
     * @param catelogType 附件上传目录类型 0：固定目录 1：选择目录
     */
    public void setCatelogType(int catelogType) {
        this.catelogType = catelogType;
    }

    /**
     * 得到所选择目录的对应的id
     *
     * @return 所选择目录的对应的id
     */
    public int getSelectedCateLog() {
        return selectedCateLog;
    }

    /**
     * 设置所选择目录的对应的id
     *
     * @param selectedCateLog 所选择目录的对应的id
     */
    public void setSelectedCateLog(int selectedCateLog) {
        this.selectedCateLog = selectedCateLog;
    }

    /**
     * 得到是否按人力资源字段附权的值.
     *
     * @return docRightByHrmResource
     */
    public int getDocRightByHrmResource() {
        return docRightByHrmResource;
    }

    /**
     * 设置是否按人力资源字段附权的值.
     *
     * @param docRightByHrmResource
     */
    public void setDocRightByHrmResource(int docRightByHrmResource) {
        this.docRightByHrmResource = docRightByHrmResource;
    }

    /**
     * 得到标题字段id.
     *
     * @return titleFieldId
     */
    public int getTitleFieldId() {
        return titleFieldId;
    }

    /**
     * 设置标题字段id.
     *
     * @param titleFieldId
     */
    public void setTitleFieldId(int titleFieldId) {
        this.titleFieldId = titleFieldId;
    }

    /**
     * 得到主题词字段id.
     *
     * @return keywordFieldId
     */
    public int getKeywordFieldId() {
        return keywordFieldId;
    }

    /**
     * 设置主题词字段id.
     *
     * @param keywordFieldId
     */
    public void setKeywordFieldId(int keywordFieldId) {
        this.keywordFieldId = keywordFieldId;
    }

    public String getIsShowChart() {
        return isShowChart;
    }

    public void setIsShowChart(String isShowChart) {
        this.isShowChart = isShowChart;
    }

    public String getShowChartUrl() {
        return showChartUrl;
    }

    public void setShowChartUrl(String showChartUrl) {
        this.showChartUrl = showChartUrl;
    }

    public String getOrderbytype() {
        return orderbytype;
    }

    public void setOrderbytype(String orderbytype) {
        this.orderbytype = orderbytype;
    }

    public String getWfdocpath() {
        return wfdocpath;
    }

    public void setWfdocpath(String wfdocpath) {
        this.wfdocpath = wfdocpath;
    }

    public String getWfdocownertype() {
        return wfdocownertype;
    }

    public void setWfdocownertype(String wfdocownertype) {
        this.wfdocownertype = wfdocownertype;
    }

    public String getWfdocownerfieldid() {
        return wfdocownerfieldid;
    }

    public void setWfdocownerfieldid(String wfdocownerfieldid) {
        this.wfdocownerfieldid = wfdocownerfieldid;
    }

    public String getCycle() {
        return Cycle;
    }

    public void setCycle(String cycle) {
        Cycle = cycle;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    /**
     * 得到是否禁止流程附件批量下载的值
     *
     * @return forbidAttDownload
     */
    public String getForbidAttDownload() {
        return forbidAttDownload;
    }

    /**
     * 设置是否禁止流程附件批量下载
     *
     * @param forbidAttDownload 是否禁止流程附件批量下载
     */
    public void setForbidAttDownload(String forbidAttDownload) {
        this.forbidAttDownload = forbidAttDownload;
    }

    public String getIsneeddelacc() {
        return isneeddelacc;
    }

    public void setIsneeddelacc(String isneeddelacc) {
        this.isneeddelacc = isneeddelacc;
    }

    public String getSAPSource() {
        return SAPSource;
    }

    public void setSAPSource(String source) {
        SAPSource = source;
    }

    public String getIsForwardReceiveDef() {
        return isForwardReceiveDef;
    }

    public void setIsForwardReceiveDef(String isForwardReceiveDef) {
        this.isForwardReceiveDef = isForwardReceiveDef;
    }

    // 微信提醒START(QC:98106)
    public String getChatsType() {
        return chatsType;
    }

    public void setChatsType(String chatsType) {
        this.chatsType = chatsType;
    }

    public String getChatsAlertType() {
        return chatsAlertType;
    }

    public void setChatsAlertType(String chatsAlertType) {
        this.chatsAlertType = chatsAlertType;
    }

    public String getNotRemindifArchived() {
        return notRemindifArchived;
    }

    public void setNotRemindifArchived(String notRemindifArchived) {
        this.notRemindifArchived = notRemindifArchived;
    }

    public static int ToZero(int i) {
        return i == -1 ? 0 : i;
    }

    public void getWfInfo() throws Exception {
        // //得到订单的信息，在修改和显示详细信息时使用
        String sql = "select * from workflow_base where id=?";
        RecordSet statement = new RecordSet();
        try {
            statement.executeQuery(sql, this.wfid);
            if (!statement.next()) {
                return;
            }

            this.setWfid(statement.getInt("id"));

            this.setWfname(Util
                    .null2String(statement.getString("workflowname")));
            this
                    .setWfdes(Util.null2String(statement
                            .getString("workflowdesc")));
            this.setTypeid(statement.getInt("workflowtype"));
            this.setFormid(statement.getInt("formid"));
            this.setIsBill(Util.null2String(statement.getString("isbill")));
            this.setIsCust(Util.null2String(statement.getString("iscust")));
            this.setHelpdocid(Util.getIntValue(
                    statement.getString("helpdocid"), 0));
            this.setIsValid(Util.null2String(statement.getString("isvalid")));
            this.setNeedMark(Util.null2String(statement.getString("needmark")));
            this.setMessageType(Util.null2String(statement
                    .getString("messageType")));
            this.setMultiSubmit(Util.null2String(statement
                    .getString("multiSubmit")));
            this.setDefaultName(Util.null2String(statement
                    .getString("defaultName")));
            this.setDocCategory(Util.null2String(statement
                    .getString("docCategory")));
            this.setDocPath(Util.null2String(statement.getString("docPath")));
            this.subCompanyId2 = statement.getInt("subcompanyid");
            this.setMailMessageType(Util.null2String(statement
                    .getString("mailMessageType")));// added by xwj for td2965
            // 20051101
            this.setArchiveNoMsgAlert(Util.null2String(statement
                    .getString("archiveNoMsgAlert")));
            this.setArchiveNoMailAlert(Util.null2String(statement
                    .getString("archiveNoMailAlert")));
            this.setForbidAttDownload(Util.null2String(statement
                    .getString("forbidAttDownload")));
            this.setDocRightByOperator(Util.null2String(statement
                    .getString("docRightByOperator")));
            this.setIsTemplate(Util.null2String(statement
                    .getString("isTemplate")));
            this.setTemplateid(Util.getIntValue(statement
                    .getString("Templateid"), 0));
            this.setCatelogType(Util.getIntValue(statement
                    .getString("catelogType"), 0));
            this.setSelectedCateLog(Util.getIntValue(statement
                    .getString("selectedCateLog"), 0));
            this.setDocRightByHrmResource(Util.getIntValue(statement
                    .getString("docRightByHrmResource")));
            this.setIsaffirmance(Util.null2String(statement
                    .getString("needaffirmance")));
            this.setIsSaveCheckForm(Util.null2String(statement
                    .getString("isSaveCheckForm")));
            this.setIsremak(Util.null2String(statement.getString("isremarks")));
            this.setIsAnnexUpload(Util.null2String(statement
                    .getString("isannexUpload")));
            this.setAnnexDocCategory(Util.null2String(statement
                    .getString("annexdoccategory")));
            this.setIsShowOnReportInput(Util.null2String(statement
                    .getString("isShowOnReportInput")));
            this.setTitleFieldId(Util.getIntValue(statement
                    .getString("titleFieldId")));
            this.setKeywordFieldId(Util.getIntValue(statement
                    .getString("keywordFieldId")));
            this.setIsShowChart(Util.null2String(statement
                    .getString("isshowchart")));
            this.setShowChartUrl(Util.null2String(statement.getString("showChartUrl")));
            this.setOrderbytype(Util.null2String(statement
                    .getString("orderbytype")));
            this.setIsModifyLog(Util.null2String(statement
                    .getString("isModifyLog")));
            this.setIsShowModifyLog(Util.null2String(statement
                    .getString("isShowModifyLog")));
            this.setShowDelButtonByReject(Util.null2String(statement
                    .getString("ShowDelButtonByReject")));

            this.setSpecialApproval(Util.null2String(statement
                    .getString("specialApproval")));
            this.setFrequency(Util
                    .null2String(statement.getString("Frequency")));
            this.setCycle(Util.null2String(statement.getString("Cycle")));

            this.setIsImportwf(Util.null2String(statement
                    .getString("isimportwf")));
            this.setImportReadOnlyField(Util.null2String(statement.getString("importReadOnlyField")));
            this.setFieldNotImport(Util.null2String(statement
                    .getString("fieldNotImport")));
            this.setWfdocpath(Util
                    .null2String(statement.getString("wfdocpath")));
            this.setWfdocowner(Util.null2String(statement
                    .getString("wfdocowner")));
            this.setWfdocownertype(""
                    + Util
                    .getIntValue(statement.getString("wfdocownertype"),
                            0));
            this.setWfdocownerfieldid(""
                    + Util.getIntValue(
                    statement.getString("wfdocownerfieldid"), 0));
            this.setIsEdit(Util.null2String(statement.getString("isEdit")));
            this.setEditor(statement.getInt("editor"));
            this.setEditdate(Util.null2String(statement.getString("editdate")));
            this.setEdittime(Util.null2String(statement.getString("edittime")));
            this.setShowUploadTab(Util.null2String(statement
                    .getString("showUploadTab")));
            this.setSignDoc(Util.null2String(statement.getString("isSignDoc")));
            this.setShowDocTab(Util.null2String(statement
                    .getString("showDocTab")));
            this.setSignWorkflow(Util.null2String(statement
                    .getString("isSignWorkflow")));
            this.setShowWorkflowTab(Util.null2String(statement
                    .getString("showWorkflowTab")));
            this.setCanDelAcc(Util
                    .null2String(statement.getString("candelacc")));
            this.setIsshared(Util.null2String(statement.getString("isshared")));
            this.setIsforwardRights(Util.null2String(statement
                    .getString("isforwardrights")));
            this.setIsrejectremind(Util.null2String(statement
                    .getString("isrejectremind")));
            this.setIsSelectrejectNode(Util.null2String(statement
                    .getString("isselectrejectnode")));
            this.setIsImportDetail(Util.null2String(statement
                    .getString("isImportDetail")));
            this.setIschangrejectnode(Util.null2String(statement
                    .getString("ischangrejectnode")));
            this.setNewdocpath(Util.null2String(statement
                    .getString("newdocpath")));
            this.setIssignview(Util.null2String(statement
                    .getString("issignview")));
            this.setAllowViewEmShareLog(Util.null2String(statement.getString("allowViewEmShareLog")));
            this.setNosynfields(Util.null2String(statement
                    .getString("nosynfields")));
            this.setIsneeddelacc(Util.null2String(statement
                    .getString("isneeddelacc")));
            this.setSAPSource(Util
                    .null2String(statement.getString("SAPSource")));
            this.setSmsAlertsType(Util.null2String(statement
                    .getString("smsAlertsType")));
            this.setIsForwardReceiveDef(Util.null2String(statement
                    .getString("forwardReceiveDef")));

            // 微信提醒START(QC:98106)
            this.setChatsType(Util
                    .null2String(statement.getString("chatsType")));
            this.setChatsAlertType(Util.null2String(statement
                    .getString("chatsAlertType")));
            this.setNotRemindifArchived(Util.null2String(statement
                    .getString("notRemindifArchived")));
            // 微信提醒END(QC:98106)

            this.setDsporder(Util.getIntValue(statement.getString("dsporder"), 0));
            this.setIsFree(Util.null2String(statement.getString("isfree")));
            this.setIsoverrb(Util.null2String(statement.getString("isoverrb")));
            this.setIsoveriv(Util.null2String(statement.getString("isoveriv")));
            this.setCustompage(Util.null2String(statement.getString("custompage")));
            this.setIsAutoApprove(Util.null2s(statement.getString("isAutoApprove").trim(), "0"));
            this.setIsAutoCommit(Util.null2s(statement.getString("isAutoCommit").trim(), "0"));
            this.setAutoFlowRequestlogTrail(Util.null2s(statement.getString("autoFlowRequestlogTrail").trim(), "1"));
            this.setIsAutoRemark(Util.null2s(statement.getString("isAutoRemark").trim(), "1"));
            this.setIsOnlyOneAutoApprove(Util.null2s(statement.getString("isOnlyOneAutoApprove").trim(), "1"));
            this.setSubmittype(Util.getIntValue(statement.getString("submittype"), 0));
            this.setIslockworkflow(Util.null2s(statement.getString("islockworkflow"), "0"));
            this.setIsshowsrc(Util.null2s(statement.getString("isshowsrc"), "0"));
            this.setLimitvalue(Util.null2String(statement.getString("limitvalue")));
            this.setTitletemplate(Util.null2String(statement.getString("titletemplate")));
//            this.setCus_titletemplate(Util.null2String(statement.getString("cus_titletemplate")));
            this.setReqLevelColorJson(Util.null2String(statement.getString("reqLevelColorJson")));
            setLocknodes(Util.null2String(statement.getString("locknodes")));
            this.setFreewftype(Util.null2String(statement.getString("freewftype")));
            setTitleset(Util.null2String(statement.getString("titleset")));

//            this.setWorkflowRemindType(Util.null2String(statement.getString("workflowremindtype")));
//            this.setDefaultRemindType(Util.null2String(statement.getString("defaultRemindType")));
//            this.setArchiveNoRemind(Util.null2String(statement.getString("archiveNoRemind")));
//            this.setCCNoRemind(Util.null2String(statement.getString("CCNoRemind")));
//            this.setChoseReminder(Util.null2String(statement.getString("choseReminder")));
            this.setIsSmsRemind(Util.null2String(statement.getString("isSmsRemind")));
            this.setIsWeChatRemind(Util.null2String(statement.getString("isWechatRemind")));
            this.setIsEmailRemind(Util.null2String(statement.getString("isEmailRemind")));
            this.setIsDefaultSmsRemind(Util.null2String(statement.getString("isDefaultSmsRemind")));
            this.setIsDefaultWeChatRemind(Util.null2String(statement.getString("isDefaultWechatRemind")));
            this.setIsDefaultEmailRemind(Util.null2String(statement.getString("isDefaultEmailRemind")));
            this.setIsArchiveNoRemind(Util.null2String(statement.getString("isArchiveNoRemind")));
            this.setIsCCNoRemind(Util.null2String(statement.getString("isCCNoRemind")));
            this.setIsChoseReminder(Util.null2String(statement.getString("isChoseReminder")));
            this.setAlterRemindNodesType(Util.null2String(statement.getString("alterRemindNodesType")));
            this.setAlterRemindNodes(Util.null2String(statement.getString("alterRemindNodes")));
            this.setDefaultNameRuleType(Util.null2String(statement.getString("defaultnameRuleType")));
            this.setHrmConditionShowType(Util.null2String(statement.getString("hrmConditionShowType")));
            this.setOldTypeid(Util.getIntValue(Util.null2String(statement.getString("workflowtype"))));
            this.setIsOpenCommunication(Util.null2String(statement.getString("isOpenCommunication")));
            this.setIsShowSignCommunicate(Util.null2String(statement.getString("isShowSignCommunicate")));
            this.setIsExpendCommunicate(Util.null2String(statement.getString("isExpendCommunicate")));
            this.setDocfiles(Util.null2String(statement.getString("docfiles")));
            this.setErrRemindType(Util.null2String(statement.getString("errorRemindType")));
            this.setErrRemindObjIds(Util.null2String(statement.getString("errorRemindObjids")));
            this.setIsOpenSignLocation(Util.getIntValue(Util.null2String(statement.getString("isOpenSignLocation")), 1));
            this.setMobileUrl(Util.null2String(statement.getString("mobileUrl")));
            this.setShowUploader(Util.null2String(statement.getString("showUploader")));
            this.setShowUploadTime (Util.null2String(statement.getString("showUploadTime")));
            this.setPrintHelpField(Util.getIntValue(Util.null2String(statement.getString("printHelpField")), 0));
            this.setHideFileSize(Util.getIntValue(Util.null2String(statement.getString("hideFileSize")), 0));
            this.setProhibitBatchForward(Util.null2String(statement.getString("prohibitbatchforward")));
            this.setRemindscope(Util.getIntValue(statement.getString("remindScope"), 0));
            this.setFileSecFormat(Util.getIntValue(statement.getString("fileSecFormat"), 0));
            this.setStopInChart(Util.null2String(statement.getString("stopInChart"),"0"));
            this.setStopInForm(Util.null2String(statement.getString("stopInForm"),"0"));
            this.setFormRangeType(Util.null2String(statement.getString("formrangetype"),"2"));
            this.setFormRange(Util.null2String(statement.getString("formrange"),"2"));

            statement.executeQuery("select isEMailApprove,nodeScope,nodes from wf_emailapprove_set where workflowId = ?", this.wfid);
            statement.next();
            this.isEMailApprove = Util.getIntValue(Util.null2String(statement.getString("isEMailApprove")), 0);
            this.nodeScope = Util.getIntValue(Util.null2String(statement.getString("nodeScope")), 0);
            this.nodes = Util.null2String(statement.getString("nodes"));

        } catch (Exception e) {
            writeLog(e);
            throw e;
        } finally {

        }
    }

}
