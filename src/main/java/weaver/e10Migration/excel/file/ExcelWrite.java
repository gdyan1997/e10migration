package weaver.e10Migration.excel.file;


import com.weaver.versionupgrade.e10Migration.service.ExportWorkflowExcel;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import weaver.e10Migration.excel.file.bean.ExcelFunctionBean;
import weaver.e10Migration.excel.file.bean.ExcelWriteEntity;
import weaver.general.BaseBean;
import weaver.general.Util;

import java.io.FileOutputStream;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @param
 * @Author ygd2020
 * @Date 9:27 2023-1-15
 * @Return
 **/
public class ExcelWrite {


    String PATH = "";
    String MODULE = "";
    int rows = 0;//当前行号
    int rowLen = 5;//每个功能之间空多少

    public ExcelWrite(String path, String module) {
        this.PATH = path;
        this.MODULE = module;
        execlInit();
    }

    public void execlInit() {
        Sheet sheet = workbook.createSheet("目录");
        formatColumnWidth(sheet);
        resetRows();
    }

    HashSet<String> allheet = new HashSet<>();
    LinkedHashMap<String, String> validsheet = new LinkedHashMap<>();


    CellStyle bodystyle = null;
    CellStyle centerheaderstyle = null;
    CellStyle leftheaderstyle = null;

    public void setAllSheetnames(String sheetname) {
        allheet.add(sheetname);
    }

    public void setBodystyle(CellStyle bodystyle) {
        this.bodystyle = bodystyle;
    }

    public Workbook workbook = new XSSFWorkbook();

    public static String getUUID(int len) {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, len);
    }


    public static String removeSpecialChar(String sheetname) {

        // Excel sheet名称限制长度为31个字符
        if (sheetname.length() > 31) {
            sheetname = sheetname.substring(0, 28) + getUUID(2);
        }

        // 替换掉Excel工作表名称中不允许的字符
        // 注意，Excel中不允许的字符可能更多，此处仅列出部分常见禁止字符
        String invalidChars = "/\\?*[]:;\'\",=`~!@#$%^&(){}|<>-+（）【】"; // 以及其他可能的非法字符
        for (char c : invalidChars.toCharArray()) {
            sheetname = sheetname.replace(c, '_');
        }


        // 将空格替换为下划线
        sheetname = sheetname.replace(' ', '_');


        return sheetname;
    }


    public Row createRow(Sheet sheet) {
        Row row = sheet.createRow(rows);
        rows++;
        return row;
    }

    public void formatAllColumnWidth(Sheet sheet) {

        Row row = sheet.getRow(0);

        for (int j = 0; j < row.getLastCellNum(); j++) {
            sheet.setColumnWidth((short) j, (short) (6 * 1000));
        }


    }

    public void formatColumnWidth(Sheet sheet) {
        Row row = sheet.getRow(0);

        for (int i = 0; i < row.getLastCellNum(); i++) {
            sheet.setColumnWidth((short) i, (short) (6 * 1000));
        }

    }

    public void createHeaderRow(Sheet sheet, List<String> colsNames) {
        Row row = createRow(sheet);


        for (int i = 0; i < colsNames.size(); i++) {
            row.setHeight((short) 312);
            Cell headCell = row.createCell(i);
            headCell.setCellValue(colsNames.get(i));
            headCell.setCellStyle(getCenterHeaderStyle());
        }

    }

    public CellStyle getCenterHeaderStyle() {
        if (centerheaderstyle == null) {
            centerheaderstyle = getHeaderStyle(HorizontalAlignment.CENTER);
        }

        return centerheaderstyle;
    }

    public CellStyle getLeftHeaderStyle() {
        if (leftheaderstyle == null) {
            leftheaderstyle = getHeaderStyle(HorizontalAlignment.LEFT);
        }

        return leftheaderstyle;
    }

    public CellStyle getHeaderStyle(HorizontalAlignment horizontalAlignment) {


        Font font = workbook.createFont();
        //设置字体大小
        font.setFontHeightInPoints((short) 14);
        //字体加粗
        font.setBold(true);
        //设置字体名字
        font.setFontName("宋体");
        //设置样式;
        CellStyle style = workbook.createCellStyle();
        //设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        //设置底边框颜色;
        style.setBottomBorderColor(IndexedColors.BLACK.index);
        //设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        //设置左边框颜色;
        style.setLeftBorderColor(IndexedColors.BLACK.index);
        //设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        //设置右边框颜色;
        style.setRightBorderColor(IndexedColors.BLACK.index);
        //设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        //设置顶边框颜色;
        style.setTopBorderColor(IndexedColors.BLACK.index);
        //在样式用应用设置的字体;
        style.setFont(font);
        //设置自动换行;
        style.setWrapText(false);
        //设置水平对齐的样式为居中对齐;
        style.setAlignment(horizontalAlignment);
        //设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);


        return style;
    }


    public CellStyle getBodyStyle() {

        if (bodystyle == null) {


            Font font = workbook.createFont();
//        //设置字体大小
//        font.setFontHeightInPoints((short) 14);
//        字体加粗
//        font.setBold(true);
            //设置字体名字
            font.setFontName("宋体");
            //设置样式;
            CellStyle style = workbook.createCellStyle();
            //设置底边框;
            style.setBorderBottom(BorderStyle.THIN);
            //设置底边框颜色;
            style.setBottomBorderColor(IndexedColors.BLACK.index);
            //设置左边框;
            style.setBorderLeft(BorderStyle.THIN);
            //设置左边框颜色;
            style.setLeftBorderColor(IndexedColors.BLACK.index);
            //设置右边框;
            style.setBorderRight(BorderStyle.THIN);
            //设置右边框颜色;
            style.setRightBorderColor(IndexedColors.BLACK.index);
            //设置顶边框;
            style.setBorderTop(BorderStyle.THIN);
            //设置顶边框颜色;
            style.setTopBorderColor(IndexedColors.BLACK.index);
            //在样式用应用设置的字体;
            style.setFont(font);
            //设置自动换行;
            style.setWrapText(true);
            //设置水平对齐的样式为居中对齐;
            style.setAlignment(HorizontalAlignment.CENTER);
            //设置垂直对齐的样式为居中对齐;
            style.setVerticalAlignment(VerticalAlignment.CENTER);
            bodystyle = style;
        }

        return bodystyle;
    }


    public void addMergedRegion(Sheet sheet, List<String> headerNames, String modelName) {


        Row row = createRow(sheet);

        Cell cell = row.createCell(0);
        cell.setCellValue(modelName);
        cell.setCellStyle(getLeftHeaderStyle());

        if (modelName.startsWith("查询sql替换分析") || modelName.startsWith("表单管理中关联")) {
            row.setHeightInPoints(100); // 设置行高为200像素
            CellStyle style = getLeftHeaderStyle();
            style.setWrapText(true); // 自动换行
        }


        CellRangeAddress region = new CellRangeAddress(rows - 1, rows - 1, 0, headerNames.size() - 1);
        sheet.addMergedRegion(region);


    }

    public void addNewModel(Sheet sheet, String modelName, List<String> headerNames, List<List<String>> values) {

        allheet.remove(sheet.getSheetName());//有内容就把sheet页删了

        validsheet.put(sheet.getSheetName(), sheet.getSheetName());// 有内容就是需要保留 加链接的

        addMergedRegion(sheet, headerNames, modelName);

        createHeaderRow(sheet, headerNames);

        for (List<String> list : values) {
            Row row = createRow(sheet);
            for (int i1 = 0; i1 < list.size(); i1++) {

                Cell cell = row.createCell(i1);
                cell.setCellStyle(getBodyStyle());
                String value = Util.null2String(list.get(i1));
                if (value.length() > 32767) {
                    largeContent(value, row, cell, i1);
                    continue;
                }
                cell.setCellValue(value);
                if (sheet.getSheetName().equalsIgnoreCase("自定义浏览按钮信息统计")) {
                    sheet.autoSizeColumn(i1);
                }
            }
        }


        for (int i = 0; i < rowLen; i++) {
            createRow(sheet);//放空行
        }

    }

    public void largeContent(String cellContent, Row row, Cell _cell, int index) {
        int maxCellLength = 32767;

        // 如果内容太长，拆分成多个单元格或行
        List<String> parts = splitContent(cellContent, maxCellLength);

        // 将拆分后的内容写入多个单元格或行
        for (int i = 0; i < parts.size(); i++) {
            String part = parts.get(i);
            Cell cell = null;
            if (i == 0) {
                cell = _cell;
            } else {
                cell = row.createCell(++index);// 在对应新行列新建一格
            }

            cell.setCellStyle(getBodyStyle());
            cell.setCellValue(part);
        }

    }

    public static List<String> splitContent(String content, int maxLength) {
        List<String> parts = new ArrayList<>();
        int length = content.length();

        for (int i = 0; i < length; i += maxLength) {
            int end = Math.min(i + maxLength, length);
            parts.add(content.substring(i, end));
        }

        return parts;
    }

    private void insertDesc(List<String> headerNames, List<List<String>> values) {
        headerNames.add("业务");
        headerNames.add("e10");
        headerNames.add("是否已处理/无需处理");


        for (List<String> value : values) {

            value.add("");
            value.add("");
            value.add("");
        }

    }


    public CellStyle getLinkStyle(HorizontalAlignment horizontalAlignment) {


        Font font = workbook.createFont();
        //设置字体大小
//        font.setFontHeightInPoints((short) 14);


        font.setUnderline((byte) 1);
        font.setColor(IndexedColors.BLUE.index);
        //设置样式;
        CellStyle style = workbook.createCellStyle();
        //在样式用应用设置的字体;
        style.setFont(font);
        //设置底边框;
        style.setBorderBottom(BorderStyle.THIN);
        //设置底边框颜色;
        style.setBottomBorderColor(IndexedColors.BLACK.index);
        //设置左边框;
        style.setBorderLeft(BorderStyle.THIN);
        //设置左边框颜色;
        style.setLeftBorderColor(IndexedColors.BLACK.index);
        //设置右边框;
        style.setBorderRight(BorderStyle.THIN);
        //设置右边框颜色;
        style.setRightBorderColor(IndexedColors.BLACK.index);
        //设置顶边框;
        style.setBorderTop(BorderStyle.THIN);
        //设置顶边框颜色;
        style.setTopBorderColor(IndexedColors.BLACK.index);
        //设置水平对齐的样式为居中对齐;
        style.setAlignment(horizontalAlignment);
        //设置垂直对齐的样式为居中对齐;
        style.setVerticalAlignment(VerticalAlignment.CENTER);

        return style;
    }


    public void formatSheet(Sheet sheet) {

        allheet.add(sheet.getSheetName());
        formatColumnWidth(sheet);
        resetRows();
        Row row = createRow(sheet);//用来放返回目录
        Cell cell = row.createCell(0);

        getLinkCell(cell, "目录", "返回目录");

        row.setHeight((short) 312);

    }


    public Sheet getSeet(String getSheetName) {
        getSheetName = removeSpecialChar(getSheetName);
        Sheet sheet = workbook.createSheet(getSheetName);

        new BaseBean().writeLog(getSheetName);

        formatSheet(sheet);


        return sheet;
    }


    public void resetRows() {
        rows = 0;
    }

    public String writeFile() throws Exception {

        //生成一张表(IO流)，03版本就是使用xlsx结尾
        String fileName = this.MODULE + ".xlsx";
        String filePath = PATH + fileName;
        FileOutputStream fos = new FileOutputStream(filePath);
        //输出
        workbook.write(fos);
        //关闭流
        fos.close();
        new BaseBean().writeLog("文件生成完毕");
        return filePath;
    }

    public List<String> getDirectoryHeaderNames() {

        ArrayList<String> strings = new ArrayList<>();

        strings.add("链接");
        return strings;

    }

    public List<List<String>> getDirectoryvalues() {
        List<List<String>> values = new ArrayList<>();

        for (String sheetName : validsheet.keySet()) {

            List<String> strings = new ArrayList<>();
            strings.add(sheetName);
            values.add(strings);
        }

        return values;

    }


    private void dealdirectoryLInk() {

        if (funccounts != null) {
            addFuncLink();
            return;
        }

        if (getDirectoryvalues().isEmpty()) {// 不存在需要链接的 也不用加目录
            return;
        }

        Sheet sheet = workbook.getSheet("目录");

        resetRows();

        createHeaderRow(sheet, getDirectoryHeaderNames());

        for (List<String> list : getDirectoryvalues()) {

            Row row = createRow(sheet);


            for (int i1 = 0; i1 < list.size(); i1++) {

                Cell cell = row.createCell(i1);
                String value = list.get(i1);

                getLinkCell(cell, value, value);
                row.setHeight((short) 312);


            }


        }

    }


    private void addFuncLink() {

        Sheet sheet = workbook.getSheet("目录");


        resetRows();

        addLinkHeader(sheet);


        for (String sheetname : validsheet.keySet()) {

            Row row = createRow(sheet);

            Cell cell = row.createCell(0);
            Cell allcell = row.createCell(1);

            allcell.setCellStyle(getBodyStyle());
            getLinkCell(cell, sheetname, sheetname);
            row.setHeight((short) 312);

            allcell.setCellValue(add3Col(row, sheetname, 1));
        }


        formatAllColumnWidth(sheet);


    }

    public int add3Col(Row row, String sheetname, int start) {
        LinkedHashMap<String, Map<String, String>> funccount = funccounts.get(sheetname);

        if (funccount == null || funccount.isEmpty()) {
            funccount = new LinkedHashMap<>();

        }
        int handint = 0;

        for (String title : allfunctitle) {


            Map<String, String> map = funccount.get(title);

            if (map == null || map.isEmpty()) {
                map = new HashMap<String, String>();

            }

//        count.put("all", all+ "");
//        count.put("hand", hand+ "");
//        count.put("nothand", nothand + "");

            Cell all = row.createCell(++start);
            all.setCellStyle(getBodyStyle());
            all.setCellValue(Util.getIntValue(map.get("all"), 0));


            Cell hand = row.createCell(++start);
            hand.setCellStyle(getBodyStyle());
            hand.setCellValue(Util.getIntValue(map.get("hand"), 0));


            Cell nothand = row.createCell(++start);
            nothand.setCellStyle(getBodyStyle());
            nothand.setCellValue(Util.getIntValue(map.get("nothand"), 0));
            handint += Util.getIntValue(map.get("hand"), 0);
        }

        return handint;
    }

    public List<String> allfunctitle = new ArrayList<>();

    private void addLinkHeader(Sheet sheet) {


        List<String> row1 = new ArrayList<>();
        List<String> row2 = new ArrayList<>();
        row1.add("链接");
        row1.add("升级是否需要处理");
        row2.add("");
        row2.add("");

        Map<String, Integer> functitle = new HashMap<>();

        Collection<LinkedHashMap<String, Map<String, String>>> values = funccounts.values();

        for (LinkedHashMap<String, Map<String, String>> value : values) {

            for (Map.Entry<String, Map<String, String>> map : value.entrySet()) {
                String key = map.getKey();
                Map<String, String> mapValue = map.getValue();
                int all = 0;

                if (functitle.containsKey(key)) {
                    all = functitle.get(key);
                }

                all += Util.getIntValue(mapValue.get("all"), 0);
                functitle.put(key, all);
            }

        }


        // 将 Map 的 entrySet 转换为 List
        List<Map.Entry<String, Integer>> entryList = new ArrayList<>(functitle.entrySet());

        // 使用 Collections.sort 按 value 降序排序
        Collections.sort(entryList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue()); // 降序排序
            }
        });

        // 将排序后的结果放入 LinkedHashMap
        Map<String, Integer> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : entryList) {
            if (entry.getValue() == 0) {
                continue;
            }
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        // 输出排序后的 Map
        sortedMap.forEach((key, value) -> allfunctitle.add(key));

        for (String s : allfunctitle) {
            row1.add(s);
            row1.add(s);
            row1.add(s);
            row2.add("总数");
            row2.add("需要处理数量");
            row2.add("无需处理数量");

        }

        Row row = createRow(sheet);

        for (int i = 0; i < row1.size(); i++) {

            row.setHeight((short) 312);

            Cell headCell = row.createCell(i);
            headCell.setCellValue(row1.get(i));
            headCell.setCellStyle(getCenterHeaderStyle());
        }


        row = createRow(sheet);

        for (int i = 0; i < row2.size(); i++) {

            row.setHeight((short) 312);
            Cell headCell = row.createCell(i);
            headCell.setCellValue(row2.get(i));
            headCell.setCellStyle(getBodyStyle());
        }


        CellRangeAddress region = new CellRangeAddress(0, 1, 0, 0);

        sheet.addMergedRegion(region);

        region = new CellRangeAddress(0, 1, 1, 1);

        sheet.addMergedRegion(region);


        String lastfunc = row1.get(0);
        int start = 0; // 合并区域的起始列

        for (int i = 1; i < row1.size(); i++) {
            String name = row1.get(i);

            // 如果当前值与上一个值不同，合并前面的区域
            if (!name.equals(lastfunc)) {
                int end = i - 1; // 合并区域的结束列

                // 只有当 start 和 end 不同时才合并（避免单个单元格合并）
                if (start < end) {
                    region = new CellRangeAddress(0, 0, start, end);
                    sheet.addMergedRegion(region);
                }

                // 更新 lastfunc 和 start
                lastfunc = name;
                start = i; // 新的合并区域从当前列开始
            }
        }

        // 处理最后一个合并区域
        if (start < row1.size() - 1) {
            region = new CellRangeAddress(0, 0, start, row1.size() - 1);
            sheet.addMergedRegion(region);
        }


    }

    public Map<String, LinkedHashMap<String, Map<String, String>>> funccounts;

    public String excelWrite(ExcelWriteEntity entity, Map<String, LinkedHashMap<String, Map<String, String>>> funccounts) {
        this.funccounts = funccounts;

        return excelWrite(entity);
    }

    public String excelWrite(ExcelWriteEntity entity) {
        String filePath = "";
        Map<String, List<ExcelFunctionBean>> sheets = entity.getSheets();


        sheets.forEach((s, excelFunctionBeans) -> {


            for (ExcelFunctionBean excelFunctionBean : excelFunctionBeans) {


                ArrayList<String> headerNames = new ArrayList<>(excelFunctionBean.getHeaderNames());


                //List<String> headerNames = excelFunctionBean.getHeaderNames();
                int index = -1;
                for (int i = 0; i < headerNames.size(); i++) {
                    String headerName = headerNames.get(i);
                    if ("替换后sql".equals(headerName)) {
                        index = i;
                        headerNames.add(index, "复杂程度");
                        break;
                    }
                }
                excelFunctionBean.setHeaderNames(headerNames);

                //没有找到 ，过
                if (index == -1) {
                    continue;
                }
                List<List<String>> values = excelFunctionBean.getValues();
                for (List<String> value : values) {
                    if (value.size() > (index + 1)) {
                        String s1 = value.get(index + 1);
                        if (s1.indexOf("|||") > -1) {
                            String[] split = s1.split("\\|\\|\\|");
                            value.set(index + 1, split[0]);
                            value.add(index, split[1]);
                            continue;
                        }

                    }
                    value.add(index, "简单");
                }
            }
        });


        for (Map.Entry<String, List<ExcelFunctionBean>> entry : sheets.entrySet()) {
            String sheetname = entry.getKey();
            List<ExcelFunctionBean> functionBeanList = entry.getValue();
            Sheet sheet = getSeet(sheetname);
            for (ExcelFunctionBean bean : functionBeanList) {
                String functionName = bean.getFunctionName();
                if (!bean.getValues().isEmpty()) {
                    addNewModel(sheet, functionName, bean.getHeaderNames(), bean.getValues());
                }
            }
        }

        for (String name : allheet) {//没被移出的sheet名字说明没有二开
            if (name.equals("目录")) {
                continue;
            }
            workbook.removeSheetAt(workbook.getSheetIndex(name));
        }

        dealdirectoryLInk();


/*        CellStyle greenBackgroundStyle = workbook.createCellStyle();
         // 设置背景填充颜色为绿色
        greenBackgroundStyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        greenBackgroundStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);*/

        CellStyle greenBackgroundStyle = workbook.createCellStyle();
        Font redFont = workbook.createFont();
        // 设置字体颜色为红色
        redFont.setColor(IndexedColors.RED.getIndex());
        greenBackgroundStyle.setFont(redFont);


        CellStyle greenStyle = workbook.createCellStyle();
        Font greenFont = workbook.createFont();

        // 设置字体颜色为绿色
        greenFont.setColor(IndexedColors.GREEN.getIndex());
        greenStyle.setFont(greenFont);


        //增加SQL复杂程度解析


        String path = entity.getModule();
        if (path.contains("_业务维度_WorkFlowAll")) {
            Sheet sheet = workbook.getSheet("目录");
            sheet.rowIterator().forEachRemaining(row -> {
                int rowNum = 0;
                //添加标题
                if (row.getCell(rowNum++).getStringCellValue().equalsIgnoreCase("链接")) {
                    row.createCell(rowNum++).setCellValue("升级是否需要处理");
                    row.createCell(rowNum++).setCellValue("字段联动详情");
                    row.createCell(rowNum++).setCellValue("流程字段属性sql(E10流程字段联动)");


                    row.createCell(rowNum++).setCellValue("流程节点附加操作^自定义");
                    row.createCell(rowNum++).setCellValue("流程节点附加操作^标准");


                    row.createCell(rowNum++).setCellValue("流程自定义页面");


                    row.createCell(rowNum++).setCellValue("表单js详情");
                    row.createCell(rowNum++).setCellValue("流程自定义页面");
                    row.createCell(rowNum++).setCellValue("流程表单代码块");
                    row.createCell(rowNum++).setCellValue("流程老规则条件");
                    row.createCell(rowNum++).setCellValue("SAP关联流程");
                    row.createCell(rowNum++).setCellValue("流程交换列表");
                    row.createCell(rowNum++).setCellValue("流程创建日程列表");
                    row.createCell(rowNum++).setCellValue("流程节点操作组外部接口");
                    row.createCell(rowNum++).setCellValue("非html模板清单");
                    row.createCell(rowNum++).setCellValue("流程升级后需要调整的异常设置");
                } else {


                    String workflowname = row.getCell(0).getStringCellValue().replaceFirst("_", "(").replaceFirst("_", ")");

                    LinkedHashMap<String, List<ExcelFunctionBean>> flowSheet = entity.getSheets();
                    List<ExcelFunctionBean> excelFunctionBeans = flowSheet.values()
                            .stream()
                            .flatMap(List::stream)
                            .collect(Collectors.toList());


                    //List<ExcelFunctionBean> excelFunctionBeans = flowSheet.get(stringCellValue.replaceFirst("_", "\\(").replaceFirst("_", "\\)"));


                    if (flowSheet.containsKey("流程节点附加操作_自定义")) {


                        long count = flowSheet.get("流程节点附加操作_自定义").stream().filter(new Predicate<ExcelFunctionBean>() {
                            @Override
                            public boolean test(ExcelFunctionBean excelFunctionBean) {
                                int workflowIndex = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "流程名称(id)");
                                return excelFunctionBean.getValues().stream().filter(x -> x.get(workflowIndex).equals(workflowname)).count() > 0;
                            }
                        }).count();
                        if (count > 0) {
                            row.createCell(4).setCellValue("需要确认");
                            row.getCell(4).setCellStyle(greenBackgroundStyle);
                        } else {
                            row.createCell(4).setCellValue("无需处理");
                        }
                    }


                    if (flowSheet.containsKey("流程节点附加操作_标准")) {

                        long count = flowSheet.get("流程节点附加操作_标准").stream().filter(new Predicate<ExcelFunctionBean>() {
                            @Override
                            public boolean test(ExcelFunctionBean excelFunctionBean) {
                                int workflowIndex = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "流程名称(id)");
                                return excelFunctionBean.getValues().stream().filter(x -> x.get(workflowIndex).equals(workflowname)).count() > 0;
                            }
                        }).count();
                        if (count > 0) {
                            row.createCell(5).setCellValue("需要确认");
                            row.getCell(5).setCellStyle(greenBackgroundStyle);
                        } else {
                            row.createCell(5).setCellValue("无需处理");
                        }
                    }


                    if (flowSheet.containsKey("流程自定义页面")) {

                        long count = flowSheet.get("流程自定义页面").stream().filter(new Predicate<ExcelFunctionBean>() {
                            @Override
                            public boolean test(ExcelFunctionBean excelFunctionBean) {
                                int workflowIndex = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "流程名称(id)");
                                return excelFunctionBean.getValues().stream().filter(x -> x.get(workflowIndex).equals(workflowname)).count() > 0;
                            }
                        }).count();
                        if (count > 0) {
                            row.createCell(6).setCellValue(count + "个自定义页面,需要确认");
                            row.getCell(6).setCellStyle(greenBackgroundStyle);
                        } else {
                            row.createCell(6).setCellValue("无需处理");
                        }
                    }


                    if (excelFunctionBeans != null) {
                        excelFunctionBeans.forEach(excelFunctionBean -> {
                            String functionName = excelFunctionBean.getFunctionName();
                            int workflowIndex_tmp = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "流程名称(id)");
                            List<List<String>> values = excelFunctionBean.getValues().stream().filter(x -> workflowname.equals(x.get(workflowIndex_tmp))).collect(Collectors.toList());


                            if ("流程字段联动".equals(functionName)) {
                                int nameIndex = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "是否完全替换");
                                int isOpen = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "是否启用");
                                if (nameIndex > -1) {
                                    Map<Object, List<String>> collect = values.stream().filter(x -> "启用".equals(x.get(isOpen))).map(x -> x.get(nameIndex)).collect(Collectors.groupingBy((Function<Object, Object>) o -> o));
                                    List<String> relicelist = collect.getOrDefault("完全替换", new ArrayList<>());
                                    int allSize = values.size();
                                    if (allSize != relicelist.size()) {
                                        row.createCell(2).setCellValue("共" + allSize + "个字段联动，需要处理占比 " + (allSize - relicelist.size()) + "个");
                                        row.getCell(2).setCellStyle(greenBackgroundStyle);
                                    } else {
                                        row.createCell(2).setCellValue("共" + allSize + "个字段联动，迁移无需处理");
                                        //row.getCell(2).setCellStyle(greenStyle);
                                    }
                                }
                            }


                            if ("流程字段属性sql(E10流程字段联动)".equals(functionName)) {
                                int nameIndex = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "是否完全替换");
                                int isOpen = ExportWorkflowExcel.getNameIndex(excelFunctionBean, "是否启用");
                                if (nameIndex > -1) {
                                    //Map<Object, List<String>> collect = values.stream().filter(x->"启用".equals(x.get(isOpen))).map(x -> x.get(nameIndex)).collect(Collectors.groupingBy((Function<Object, Object>) o -> o));
                                    Map<Object, List<String>> collect = values.stream().map(x -> x.get(nameIndex)).collect(Collectors.groupingBy((Function<Object, Object>) o -> o));
                                    List<String> relicelist = collect.getOrDefault("完全替换", new ArrayList<>());
                                    int allSize = values.size();
                                    if (allSize != relicelist.size()) {
                                        row.createCell(3).setCellValue("共" + allSize + "个字段联动，需要处理占比 " + (allSize - relicelist.size()) + "个");
                                        row.getCell(3).setCellStyle(greenBackgroundStyle);
                                    } else {
                                        row.createCell(3).setCellValue("共" + allSize + "个字段联动，迁移无需处理");
                                        //row.getCell(2).setCellStyle(greenStyle);
                                    }
                                }
                            }

                        });

                    }


                }

            });
        }


        try {
            filePath = writeFile();
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }


        new BaseBean().writeLog("filePath = " + filePath);
        return filePath;
    }


    public void getLinkCell(Cell cell, String sheetname, String showname) {

        CreationHelper createHelper = workbook.getCreationHelper();
        XSSFHyperlink hyperlink = (XSSFHyperlink) createHelper.createHyperlink(HyperlinkType.DOCUMENT);

        // "#"表示本文档    "明细页面"表示sheet页名称  "A10"表示第几列第几行
        hyperlink.setAddress("#" + sheetname + "!A1");
        cell.setHyperlink(hyperlink);
        // 点击进行跳转
        cell.setCellValue(showname);

        cell.setCellStyle(getLinkStyle(HorizontalAlignment.CENTER));
    }


    public static String excelWriteToFile(ExcelWriteEntity writeEntity, Map<String, LinkedHashMap<String, Map<String, String>>> funccounts) {

        //  应用名称     功能名称  功能调整数

        ExcelWrite excelWrite = new ExcelWrite(writeEntity.getPath(), writeEntity.getModule());
        String filePath = excelWrite.excelWrite(writeEntity, funccounts);


        return filePath;
    }

    public static String excelWriteToFile(ExcelWriteEntity writeEntity) {


        return excelWriteToFile(writeEntity, null);
    }

    public static void addExcelModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {


        //创建一个模块 功能项
        ExcelFunctionBean functionBean = new ExcelFunctionBean(functionName);

        //功能有多少列，指定每个列的列名
        functionBean.setHeaderNames(headerNames);

        //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
        functionBean.setValues(values);

        List<ExcelFunctionBean> list = new ArrayList<>();//一个sheet页有多少功能项
        list.add(functionBean);
        LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();
        if (sheets.containsKey(sheetname)) {//包含sheet页  就补在sheet页中
            sheets.get(sheetname).addAll(list);
        } else { //不存在当前sheet页，维护新的
            sheets.put(sheetname, list);
        }
        writeEntity.setSheets(sheets);
    }


    public static void addExcelFuncModal(ExcelWriteEntity writeEntity, String sheetname, String functionName,
                                         List<String> headerNames, List<String> value) {


        LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();
        List<ExcelFunctionBean> functionBeans = new ArrayList<>();
        if (sheets.containsKey(sheetname)) {//包含sheet页  就补在sheet页中
            functionBeans = sheets.get(sheetname);
        }

        List<ExcelFunctionBean> collect = functionBeans.stream().filter(excelFunctionBean -> excelFunctionBean.getFunctionName().equals(functionName)).collect(Collectors.toList());


        if (collect.size() > 0) {

            collect.get(0).getValues().add(value);
        } else {


            //创建一个模块 功能项
            ExcelFunctionBean functionBean = new ExcelFunctionBean(functionName);

            //功能有多少列，指定每个列的列名
            functionBean.setHeaderNames(headerNames);

            //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
            functionBean.setValues(new ArrayList<List<String>>() {{
                add(value);
            }});

            functionBeans.add(functionBean);
        }


        sheets.put(sheetname, functionBeans);
        writeEntity.setSheets(sheets);
    }


    public static void addExcelFirstModal(ExcelWriteEntity writeEntity, String sheetname, String functionName
            , List<String> headerNames, List<List<String>> values) {


        //创建一个模块 功能项
        ExcelFunctionBean functionBean = new ExcelFunctionBean(functionName);

        //功能有多少列，指定每个列的列名
        functionBean.setHeaderNames(headerNames);

        //list<list> 最外层是有多少行，内层list 有多少列，必须跟headname列名顺序保持一致
        functionBean.setValues(values);

        List<ExcelFunctionBean> list = new ArrayList<>();//一个sheet页有多少功能项
        list.add(functionBean);
        LinkedHashMap<String, List<ExcelFunctionBean>> sheets = writeEntity.getSheets();

        if (sheets.containsKey(sheetname)) {//包含sheet页  就补在sheet页中
            sheets.get(sheetname).addAll(list);
        } else { //不存在当前sheet页，维护新的

            LinkedHashMap<String, List<ExcelFunctionBean>> newSheets = new LinkedHashMap<>();
            newSheets.put(sheetname, list);

            newSheets.putAll(sheets);
            sheets = newSheets;
        }


        writeEntity.setSheets(sheets);
    }


}
