package weaver.e10Migration;
import com.alibaba.fastjson.JSONObject;
import com.weaver.version.upgrade.util.sql.SQLUtil;
import com.weaver.version.upgrade.util.sql.cache.FieldMappingCache;
import com.weaver.versionupgrade.customRest.util.ConfigUtil;
import com.weaver.versionupgrade.e10Migration.modules.all.ExportAllDataExcel;
import com.weaver.versionupgrade.e10Migration.modules.cube.ExportModeExcel;
import com.weaver.versionupgrade.e10Migration.modules.cubrowser.CustomBrowserExport;
import com.weaver.versionupgrade.e10Migration.modules.edc.ExportEdcExcel;
import com.weaver.versionupgrade.e10Migration.modules.interfaces.ExportInterfacesExcel;
import com.weaver.versionupgrade.e10Migration.modules.mobilemode.ExportMobileModeExcel;
import com.weaver.versionupgrade.e10Migration.modules.portal.ExportPortalExcel;
import com.weaver.versionupgrade.e10Migration.service.ExportWorkflowExcel;
import weaver.conn.RecordSet;
import weaver.general.Util;

import java.io.File;
import java.util.*;

/**
 * @author ygd2020
 * @date 2024-7-8 10:16
 * Description:
 **/
public class MigrationOperation {
    public static File doMigrationOperation(String operation, String dimension, String checkvaule) {

        List<String> list = Arrays.asList(checkvaule.split(","));
        List<String> fileList = new ArrayList<String>();

        String PATH = ConfigUtil.getExcelDir() + File.separatorChar;

        File file = new File(PATH);
        if (!file.exists()) {
            file.mkdirs();
        }

        //清除一下缓存信息
        FieldMappingCache.tableCount.clear();
        FieldMappingCache.FieldCount.clear();
        FieldMappingCache.errorList.clear();
        SQLUtil.totalReplacement = 0;
        SQLUtil.fullReplacement = 0;
        SQLUtil.partialUnreplacedSQL = 0;
        SQLUtil.incorrectSQL = 0;
        SQLUtil.nonStandardFields = 0;

        RecordSet rs = new RecordSet();
        int version = 9;
        rs.executeQuery("select  * from license");
        if (rs.next()) {
            String cversion = Util.null2String(rs.getString("CVERSION"));
            if (cversion.startsWith("7")) {
                version = 7;
            } else if (cversion.startsWith("8")) {
                version = 8;
            }else if (cversion.startsWith("9")){
                version = 9 ;
            }
        }
        com.weaver.versionupgrade.customRest.util.SourceDBUtil.version= String.valueOf(version);

        //建模,流程分为功能维度和应用维度,集成一个维度就可以了  ,总共生成3篇文档
        if ("0".equals(dimension)) {//功能维度

            try {
                if (list.contains("workflow")) {

                    ExportWorkflowExcel exportWorkflowExcel = new ExportWorkflowExcel();
                    String filepath = exportWorkflowExcel.doWorkFlowAll(PATH, version);
                    if (!"".equals(filepath)) {
                        fileList.add(filepath);
                    }
                }
            } catch (Exception e) {
                new weaver.general.BaseBean().writeLog("", e);
            }
            if (list.contains("cube")) {
                try {
                    fileList.addAll(new ExportModeExcel().doCubeAll(PATH));

                } catch (Exception e) {
                    new weaver.general.BaseBean().writeLog("", e);
                }
            }
        } else if ("1".equals(dimension)) {//流程,应用维度
            if (list.contains("workflow")) {
//            ExportWorkflowExcel exportWorkflowExcel = new ExportWorkflowExcel();
//            String filepath = exportModeExcel.doall(PATH);
//            if (!"".equals(filepath)) {
//                fileList.add(filepath);
//            }
            }
            if (list.contains("cube")) {
                try {

//                    String filepath = new ExportModeExcel().doCubeApp(PATH);
//                    if (!"".equals(filepath)) {
//                        fileList.add(filepath);
//                    }
                } catch (Exception e) {
                    new weaver.general.BaseBean().writeLog("", e);
                }
            }
        }

        try {
            //集成模块的清单
            if (list.contains("interfaces")) {

                ExportInterfacesExcel exportInterfacesExcel = new ExportInterfacesExcel();
                String filepath = exportInterfacesExcel.doInterFaceAll(PATH);
                if (!"".equals(filepath)) {
                    fileList.add(filepath);
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }
        try {
            //门户的清单
            if (list.contains("portal")) {
                ExportPortalExcel exportPortalExcel = new ExportPortalExcel();

                String filepath = exportPortalExcel.doPoralAll(PATH);
                if (!"".equals(filepath)) {
                    fileList.add(filepath);
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }
        try {
            //数据中心的清单
            if (list.contains("edc")) {
                ExportEdcExcel exportEdcExcel = new ExportEdcExcel();
                String filepath = exportEdcExcel.doExportEdcDetail(PATH);
                if (!"".equals(filepath)) {
                    fileList.add(filepath);
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }
        try {
            //移动建模的清单
            if (list.contains("mobilemode")) {
                String filepath = new ExportMobileModeExcel().domobilemodeApp(PATH);
                if (!"".equals(filepath)) {
                    fileList.add(filepath);
                }
            }
        } catch (Throwable e) {
            new weaver.general.BaseBean().writeLog("", e);
        }

        try {
            //自定义浏览按钮分析
            if (list.contains("cubrowser")) {
                CustomBrowserExport exportInterfacesExcel = new CustomBrowserExport();
                String filepath = exportInterfacesExcel.doBrowserAll(PATH);
                if (!"".equals(filepath)) {
                    fileList.add(filepath);
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }

        try {
            //总量分析
    /*
    1.获取总数据量
    2.前50数据的表
    3.所有非标准表以及非表单表的清单以及数据量
     */
         if (list.contains("all")) {
                ExportAllDataExcel exportAllDataExcel = new ExportAllDataExcel();
                String filepath = exportAllDataExcel.doDataAll(PATH);
                if (!"".equals(filepath)) {
                    fileList.add(0, filepath);
                }
            }
        } catch (Exception e) {
            new weaver.general.BaseBean().writeLog("", e);
        }

        File allecologyzip = new File(PATH + weaver.general.Util.getCustomName() + "_" + ConfigUtil.excelname);
        if (allecologyzip.exists() && allecologyzip.isFile()) {
            allecologyzip.delete();
        }

        //开始打包
        wscheck.ZipUtils PackFileUtil = new wscheck.ZipUtils();
        wscheck.ZipUtils.execute(fileList, PATH + weaver.general.Util.getCustomName() + "_" + ConfigUtil.excelname, PATH, "ecology");

        // 从抓取结束之后清除
        ConfigUtil.errorsqllist.clear();

        return allecologyzip;
    }

    public static void main(String[] args) {



        JSONObject jsonObject=new JSONObject();
        jsonObject.put("wef",null);


        System.out.println(jsonObject.toJSONString());
    }
}
