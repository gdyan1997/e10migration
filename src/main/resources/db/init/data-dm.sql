-- 创建表 javascript_trans
CREATE TABLE JAVASCRIPT_TRANS (
                                  ID NUMBER(38,0) ,
                                  LAYOUTID INT,
    FROMMODULE     VARCHAR(100),
    OTHERPROPERTY1 VARCHAR(255),
    OTHERPROPERTY2 VARCHAR(255),
    OTHERPROPERTY3 VARCHAR(255),
    WORKFLOWID     INT,
                                  WORKFLOWNAME CLOB,
                                  FORMID INT,
                                  FORMNAME CLOB,
                                  ISBILL INT,
                                  NODEID INT,
                                  NODENAME VARCHAR(480),
                                  TYPE INT,
                                  TYPENAME VARCHAR(100),
                                  LAYOUTNAME VARCHAR(1000),
                                  scriptstr CLOB,
                                  TRANFERCONTENT CLOB,
                                  TRANFERSTATUS CLOB,
                                  TRANSFERFLAG VARCHAR(100),
                                  HASCSSSTYLE VARCHAR(10),
                                  HASSRCSCRIPT VARCHAR(10),
                                  ISACTIVE INT,
                                  CREATETIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                  UPDATETIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
/
-- 创建序列
CREATE SEQUENCE "JAVASCRIPT_TRANS_ID"
INCREMENT BY 1
START WITH 42
MAXVALUE 9223372036854775807
MINVALUE 1
CACHE 20
/
-- 创建触发器
CREATE  OR REPLACE TRIGGER "JAVASCRIPT_TRANS_TRIGGER"
BEFORE  INSERT
ON "JAVASCRIPT_TRANS"
referencing OLD ROW AS "OLD" NEW ROW AS "NEW"
for each row
begin select JAVASCRIPT_TRANS_ID.nextval into :new.id from dual; end;
/

-- 添加索引
CREATE INDEX JSTRAN_FROM_MODULE ON JAVASCRIPT_TRANS (FROMMODULE)
/