-- 创建表 javascript_trans
CREATE TABLE javascript_trans (
    id NUMBER,
    LAYOUTID NUMBER,
    FROMMODULE     VARCHAR2(100),
    OTHERPROPERTY1 VARCHAR2(255),
    OTHERPROPERTY2 VARCHAR2(255),
    OTHERPROPERTY3 VARCHAR2(255),
    WORKFLOWID     NUMBER,
    WORKFLOWNAME CLOB,
    FORMID NUMBER,
    FORMNAME CLOB,
    ISBILL NUMBER,
    NODEID NUMBER,
    NODENAME VARCHAR2(480),
    TYPE NUMBER,
    TYPENAME VARCHAR2(100),
    LAYOUTNAME VARCHAR2(1000),
    scriptstr CLOB,
    TRANFERCONTENT CLOB,
    TRANFERSTATUS CLOB,
    TRANSFERFLAG VARCHAR2(100),
    HASCSSSTYLE VARCHAR2(10),
    HASSRCSCRIPT VARCHAR2(10),
    ISACTIVE NUMBER,
    CREATETIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    UPDATETIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
/
create sequence javascript_trans_id
    start with 1
    increment by 1
    nomaxvalue
nocycle
/
create or replace trigger javascript_trans_Trigger
before insert on javascript_trans
for each row
begin
select javascript_trans_id.nextval INTO :new.id from dual;
end;
/
-- 添加索引
CREATE INDEX jstran_from_module ON javascript_trans (FROMMODULE)
/