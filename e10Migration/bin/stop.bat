@echo off
set port=4398
for /f "tokens=1-5" %%i in ('netstat -a -n -o ^|findstr ":%port%"') do (
    echo kill the process %%m who use the port %port%
    taskkill /pid %%m -t -f
	goto q
)
:q
pause