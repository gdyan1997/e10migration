 #!/bin/bash

# 指定要查找的端口号
PORT=4398

# 查找运行在指定端口上的Java进程
PID=$(lsof -ti:$PORT)

if [ -z "$PID" ]; then
    echo "未找到运行在端口 $PORT 上的Java进程。"
else
    echo "找到运行在端口 $PORT 上的Java进程，PID为 $PID。"

    # 停止Java进程
    echo "正在停止进程..."
    kill $PID

   # 循环检查进程是否还在运行  
	while ps -p $PID > /dev/null; do  
		echo "等待进程停止..."  
		sleep 2 # 等待2秒再次检查  
	done  
  
	# 如果循环结束，说明进程已经停止  
	echo "进程已成功停止。"
fi
