使用说明：
工具是一个独立的小服务工具，部署后启动即可使用  (此工具部署在E9服务器上)

E10升级评估阶段，只需要在拉取清单页面，生成清单即可。

 

 

环境运行在4398 端口
http://localhost:4398
拉取完成后请停止服务。

linux 平台
################################################################################
Linux 部署 root用户操作

1.上传目录weaver同级解压zip包

## 上传目录同级执行命令解压zip包
2. unzip e10Migration.zip

3.进行目录赋权。

   chmod -R  777  e10Migration
4.##指定Ecology数据库（下列三种方式，任选一种即可）
     1. 将e10Migration解压ecology的同级目录,系统会自动寻找weaver.properties（优先此种方式）
     2. 或复制oa weaver.properties -> e10Migration/conf
     3. 或在e10Migration\conf\database.properties 中配置 weaver.properties 地址。

## 进入执行目录  e10Migration\bin 目录下  （ cd e10Migration/bin）
5.  sh start.sh   或    ./start.sh    或   nohup ../jdk/linux/jdk-8u151/jre/bin/java -Dfile.encoding=utf-8 -Dserver.port=4398 -jar ../lib/e10Migration-0.0.1-SNAPSHOT.jar > /dev/null 2>&1 &   （此命令为后台启动命令，适用于客户堡垒机，长时间拉取时间过长导致堡垒机自动断开，拉取失败）

6.访问 http://部署服务ip:4398 拉取清单。

如果客户开放端口困难，可在服务启动后，重新打开一个与服务器连接，

控制台输入  wget 'http://127.0.0.1:4398/e10Migration/migrationOperation.jsp?dimension=0&checkvaule=all,workflow,cube,interfaces,portal,edc,mobilemode'  命令（此命令默认拉取所有模块）

等待拉取程序 e10Migration\excel目录下出现  xxxxx_ecologyForE10List.zip(xxxx代表客户名称)文件，即拉取结束下载此文件即可。

## 停止服务 e10Migration\bin 目录下
7.  ./stop.sh


window 平台
################################################################################
1.上传目录weaver同级解压zip包
2.##指定Ecology数据库（下列三种方式，任选一种即可）
     1. 将e10Migration解压ecology的同级目录,系统会自动寻找weaver.properties（优先此种方式）
     2. 或复制oa weaver.properties -> e10Migration/conf
     3. 或在e10Migration\conf\database.properties 中配置 weaver.properties 地址。
3.e10Migration\bin 目录下 start.bat 启动项目(以管理员身份运行)。
4.访问 http:///部署服务ip:4398 拉取清单。
5. e10Migration\bin stop.bat 停止服务

 

注意！！！解压到ecology同级！！！而不是解压到ecology内部！！！